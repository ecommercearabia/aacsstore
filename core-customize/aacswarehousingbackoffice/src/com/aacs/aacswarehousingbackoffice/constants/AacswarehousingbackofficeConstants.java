/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.aacs.aacswarehousingbackoffice.constants;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
public final class AacswarehousingbackofficeConstants extends GeneratedAacswarehousingbackofficeConstants
{
	public static final String EXTENSIONNAME = "aacswarehousingbackoffice";

	private AacswarehousingbackofficeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
