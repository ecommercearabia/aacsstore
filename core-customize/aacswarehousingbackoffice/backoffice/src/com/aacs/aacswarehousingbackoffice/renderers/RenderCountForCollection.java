/**
 *
 */
package com.aacs.aacswarehousingbackoffice.renderers;

import java.util.Collection;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zul.Listcell;

import com.hybris.cockpitng.core.config.impl.jaxb.listview.ListColumn;
import com.hybris.cockpitng.dataaccess.facades.type.DataType;
import com.hybris.cockpitng.dataaccess.facades.type.TypeFacade;
import com.hybris.cockpitng.dataaccess.services.PropertyValueService;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.labels.LabelService;
import com.hybris.cockpitng.widgets.common.WidgetComponentRenderer;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class RenderCountForCollection implements WidgetComponentRenderer<Listcell, ListColumn, Object>
{
	private static final Logger LOG = LoggerFactory.getLogger(RenderCountForCollection.class);

	@Resource(name = "typeFacade")
	private TypeFacade typeFacade;

	@Resource(name = "labelService")
	private LabelService labelService;

	@Resource(name = "propertyValueService")
	private PropertyValueService propertyValueService;

	@Override
	public void render(final Listcell listcell, final ListColumn columnConfiguration, final Object object, final DataType dataType,
			final WidgetInstanceManager widgetInstanceManager)
	{
		final String qualifier = columnConfiguration.getQualifier();

		try
		{
			final Object value = this.getPropertyValueService().readValue(object, qualifier);
			if (!(value instanceof Collection))
			{
				return;
			}
			final Collection col = (Collection) value;
			listcell.setLabel(String.valueOf(col.size()));
		}
		catch (final Exception var12)
		{
			LOG.info("Could not render row.", var12);
		}

	}

	/**
	 * @return the propertyValueService
	 */
	public PropertyValueService getPropertyValueService()
	{
		return propertyValueService;
	}



}
