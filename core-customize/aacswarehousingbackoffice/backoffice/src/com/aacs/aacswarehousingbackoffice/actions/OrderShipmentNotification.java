/**
 *
 */
package com.aacs.aacswarehousingbackoffice.actions;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zul.Messagebox;

import com.aacs.aacsfulfillment.context.FulfillmentProviderContext;
import com.aacs.aacsfulfillment.model.FulfillmentProviderModel;
import com.aacs.ordermanagement.model.OrderTrackingIdEmailProcessModel;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware;


/**
 * @author Monzer
 *
 */
public class OrderShipmentNotification extends AbstractComponentWidgetAdapterAware
		implements CockpitAction<ConsignmentModel, ConsignmentModel>
{
	/**
	 *
	 */
	private static final String PICKUP_IN_STORE_MODE = "pickup";
	private static final Logger LOG = LoggerFactory.getLogger(OrderShipmentNotification.class);
	private static final String TRACKING_ID_NOT_FOUND = "Tracking id not found";

	@Resource(name = "modelService")
	private ModelService modelService;

	/** The business process service. */
	@Resource(name = "businessProcessService")
	private BusinessProcessService businessProcessService;

	/** The process code generator. */
	@Resource(name = "processCodeGenerator")
	private KeyGenerator processCodeGenerator;

	@Resource(name = "fulfillmentProviderContext")
	private FulfillmentProviderContext fulfillmentProviderContext;

	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> ctx)
	{
		ActionResult<ConsignmentModel> actionResult = new ActionResult<>(ActionResult.SUCCESS);
		final ConsignmentModel consignment = ctx.getData();
		if (ctx.getData() == null)
		{
			Messagebox.show("No consignment found for this action");
			actionResult = new ActionResult<>(ActionResult.ERROR);
		}
		if (consignment == null || StringUtils.isBlank(consignment.getTrackingID()) || consignment.getCarrierDetails() == null)
		{
			Messagebox.show("Make sure that you have created a shipment for this consignmet");
			actionResult = new ActionResult<>(ActionResult.ERROR);
		}
		// Sending the order tracking email
		final OrderTrackingIdEmailProcessModel sendingOrderTrackingIdProcessModel = (OrderTrackingIdEmailProcessModel) getBusinessProcessService()
				.createProcess("sendingOrderTrackingIdEmail-process-" + consignment.getCode() + "-"
						+ processCodeGenerator.generate().toString(), "sendingOrderTrackingIdEmail-process");
		sendingOrderTrackingIdProcessModel.setConsignment(consignment);
		getModelService().save(sendingOrderTrackingIdProcessModel);
		getBusinessProcessService().startProcess(sendingOrderTrackingIdProcessModel);

		modelService.refresh(consignment);

		return actionResult;
	}

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> ctx)
	{
		final ConsignmentModel consignment = ctx.getData();

		if (consignment == null || consignment.getOrder() == null || consignment.getOrder().getStore() == null)
		{
			return false;
		}

		final AbstractOrderModel order = consignment.getOrder();

		if (order.getDeliveryMode() == null || PICKUP_IN_STORE_MODE.equalsIgnoreCase(order.getDeliveryMode().getCode()))
		{
			return false;
		}

		if (StringUtils.isBlank(consignment.getTrackingID()) || consignment.getCarrierDetails() == null)
		{
			return false;
		}

		final Optional<FulfillmentProviderModel> provider = getFulfillmentProviderContext()
				.getProvider(consignment.getOrder().getStore());

		return (provider.isPresent() && Boolean.TRUE.equals(provider.get().getActive()))
				&& order.getStore().isSendOrderTrackingEmailActionEnabled()
				&& order.getStore().isSendOrderTrackingEmailOnCreateShipmentEnabled();
	}

	@Override
	public boolean needsConfirmation(final ActionContext<ConsignmentModel> ctx)
	{
		return true;
	}

	@Override
	public String getConfirmationMessage(final ActionContext<ConsignmentModel> ctx)
	{
		return "Do you want to send the shipment notification?";
	}

	/**
	 * @return the businessProcessService
	 */
	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the processCodeGenerator
	 */
	public KeyGenerator getProcessCodeGenerator()
	{
		return processCodeGenerator;
	}

	/**
	 * @return the fulfillmentProviderContext
	 */
	protected FulfillmentProviderContext getFulfillmentProviderContext()
	{
		return fulfillmentProviderContext;
	}

}
