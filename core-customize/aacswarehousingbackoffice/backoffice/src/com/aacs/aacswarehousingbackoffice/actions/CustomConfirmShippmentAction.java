/**
 *
 */
package com.aacs.aacswarehousingbackoffice.actions;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.warehousingbackoffice.actions.ship.ConfirmShippedConsignmentAction;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zul.Messagebox;

import com.aacs.aacsloyaltyprogramprovider.context.LoyaltyProgramContext;
import com.aacs.aacsscpiservices.exception.SCPIException;
import com.aacs.aacsscpiservices.service.SCPIService;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CustomConfirmShippmentAction extends ConfirmShippedConsignmentAction
{

	private static final Logger LOG = LoggerFactory.getLogger(CustomConfirmShippmentAction.class);

	@Resource(name = "defaultSCPIService")
	private SCPIService defaultSCPIService;

	@Resource(name = "loyaltyProgramContext")
	private LoyaltyProgramContext loyaltyProgramContext;

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> actionContext)
	{
		return super.canPerform(actionContext) && checkShipment(actionContext.getData());
	}

	/**
	 * @param data
	 * @return
	 */
	private boolean checkShipment(final ConsignmentModel consignment)
	{
		return consignment != null && consignment.getCarrierDetails() != null
				&& StringUtils.isNotBlank(consignment.getTrackingID());
	}

	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> actionContext)
	{
		if (actionContext == null || actionContext.getData() == null)
		{
			Messagebox.show("Error While getting the Consignment");
			return new ActionResult<>(ActionResult.ERROR);
		}

		final ConsignmentModel data = actionContext.getData();
		final AbstractOrderModel order = data.getOrder();
		if (!validateConsignment(data))
		{
			return new ActionResult<>(ActionResult.ERROR);
		}

		// Send consignment to SCPI
		if (SCPIStatus.ERROR.equals(handleSCPI(data)))
		{
			return new ActionResult<>(ActionResult.ERROR);
		}

		return super.perform(actionContext);

	}

	/**
	 * @param data
	 * @return
	 */
	private boolean validateConsignment(final ConsignmentModel data)
	{
		if (data.getOrder() == null)
		{
			Messagebox.show("Please Make sure that the order is not null on the consignment. Aborting");
			return false;
		}

		if (data.getOrder().getStore() == null)
		{
			Messagebox.show("Please Make sure that the store us not null on the consignment order. Aborting");
			return false;
		}
		return true;

	}

	/**
	 * @param data
	 */
	private SCPIStatus handleSCPI(final ConsignmentModel data)
	{
		if (!data.getOrder().getStore().isEnableSCPISalesOrder())
		{
			return SCPIStatus.NOT_ENABLED;
		}

		try
		{
			defaultSCPIService.sendSCPISalesOrder(data);
			return SCPIStatus.SENT;
		}
		catch (final SCPIException scpi)
		{
			Messagebox.show("Error While sending data to SCPI, Please check the history on the Order . Aborting");
			LOG.error(scpi.getMessage());
			return SCPIStatus.ERROR;
		}
		catch (final Exception e)
		{
			Messagebox.show("Error While sending data to SCPI, Please check the log." + e.getMessage() + " ,Aborting");
			LOG.error(e.getMessage());
			return SCPIStatus.ERROR;
		}
	}

	private enum SCPIStatus
	{
		SENT, ERROR, NOT_ENABLED;
	}


	private enum LoyaltyStatus
	{
		CAPTURED, ERROR, NOT_ENABLED;
	}

	/**
	 * @return the defaultSCPIService
	 */
	protected SCPIService getDefaultSCPIService()
	{
		return defaultSCPIService;
	}

	/**
	 * @return the loyaltyProgramContext
	 */
	public LoyaltyProgramContext getLoyaltyProgramContext()
	{
		return loyaltyProgramContext;
	}

}
