package com.aacs.aacswarehousingbackoffice.actions;

import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.warehousingbackoffice.actions.printpacklabel.PrintPackLabelAction;

import java.util.Optional;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zul.Messagebox;

import com.aacs.aacsfulfillment.service.CustomConsignmentService;
import com.aacs.aacsloyaltyprogramprovider.context.LoyaltyProgramContext;
import com.aacs.aacspayment.context.PaymentContext;
import com.aacs.aacspayment.entry.PaymentResponseData;
import com.aacs.aacspayment.enums.PaymentOperationType;
import com.aacs.aacspayment.enums.PaymentResponseStatus;
import com.aacs.aacspayment.exception.PaymentException;
import com.aacs.aacspayment.exception.type.PaymentExceptionType;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CustomPrintPackLabelAction extends PrintPackLabelAction
{
	private static final Logger LOG = LoggerFactory.getLogger(CustomPrintPackLabelAction.class);

	@Resource(name = "customConsignmentService")
	private CustomConsignmentService customConsignmentService;

	@Resource(name = "paymentContext")
	private PaymentContext paymentContext;
	@Resource(name = "loyaltyProgramContext")
	private LoyaltyProgramContext loyaltyProgramContext;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{

		Optional<PaymentResponseData> response;
		final ConsignmentModel consignment = consignmentModelActionContext.getData();

		if (!customConsignmentService.matchOrderConsignmentEntries(consignment))
		{
			Messagebox.show("Consignment entries do no match order entries!");
			return new ActionResult<>(ActionResult.ERROR);
		}

		if (consignment.getOrder() == null)
		{
			Messagebox.show("consignment order is null!");
			return new ActionResult<>(ActionResult.ERROR);
		}
		if (consignment.getOrder().getPaymentInfo() == null)
		{
			Messagebox.show("order PaymentInfo is null!");
			return new ActionResult<>(ActionResult.ERROR);
		}

		if (!(consignment.getOrder().getPaymentInfo() instanceof CreditCardPaymentInfoModel)
				|| paymentContext.isCaptured(consignment.getOrder()))
		{
			return super.perform(consignmentModelActionContext);
		}

		try
		{
			response = paymentContext.captureOrder(consignment);
		}
		catch (final PaymentException e)
		{
			final StringBuffer msg = new StringBuffer();

			msg.append("An error has occured during capturing consignment. Exception Type [");
			msg.append(e.getExceptionType());
			msg.append("] ,Exception Message [");
			msg.append(e.getMessage());
			msg.append("]");

			if (PaymentExceptionType.THE_ORDER_IS_ALLREADY_CANCELED.equals(e.getExceptionType()))
			{
				msg.append("\n");
				msg.append("The order is already canceled on payment provider side, please cancel this order.");
			}
			Messagebox.show(msg.toString());
			return new ActionResult<>(ActionResult.ERROR);
		}

		if (response.get().getStatus().equals(PaymentResponseStatus.FAILURE))
		{
			Messagebox.show("A problem has occured during capturing consignment! Status is: " + response.get().getStatus());
			return new ActionResult<>(ActionResult.ERROR);
		}

		return super.perform(consignmentModelActionContext);

	}

	@Override
	public boolean needsConfirmation(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{

		final ConsignmentModel consignment = consignmentModelActionContext.getData();

		return isCreditCardPaymentAndDisplayMsg(consignment);
	}

	/**
	 * @param consignment
	 * @return
	 */
	private boolean isCreditCardPaymentAndDisplayMsg(final ConsignmentModel consignment)
	{
		return consignment != null && consignment.getOrder() != null && consignment.getOrder().getPaymentInfo() != null
				&& (consignment.getOrder().getPaymentInfo() instanceof CreditCardPaymentInfoModel)
				&& !PaymentOperationType.CAPTURE.equals(consignment.getOrder().getPaymentOperationType());
	}

	@Override
	public String getConfirmationMessage(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		return "Are you sure you want to capture this order?";
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public LoyaltyProgramContext getLoyaltyProgramContext()
	{
		return loyaltyProgramContext;
	}
}
