/**
 *
 */
package com.aacs.aacswarehousingbackoffice.actions;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.warehousing.taskassignment.services.WarehousingConsignmentWorkflowService;
import de.hybris.platform.workflow.enums.WorkflowActionStatus;
import de.hybris.platform.workflow.model.WorkflowActionModel;

import java.util.EnumSet;
import java.util.Optional;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zul.Messagebox;

import com.aacs.aacsfulfillment.context.FulfillmentContext;
import com.aacs.aacsfulfillment.context.FulfillmentProviderContext;
import com.aacs.aacsfulfillment.exception.FulfillmentException;
import com.aacs.aacsfulfillment.model.FulfillmentProviderModel;
import com.aacs.core.service.WarehousingDeliveryService;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.ActionResult.StatusFlag;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class UpdateDeliveryStatusAction extends AbstractComponentWidgetAdapterAware
		implements CockpitAction<ConsignmentModel, ConsignmentModel>
{
	private static final String PICKUP_IN_STORE_MODE = "pickup";
	private static final Logger LOG = LoggerFactory.getLogger(UpdateDeliveryStatusAction.class);
	private static final String COULD_NOT_UPDATE_SHIPMENT = "Could not update the Shipment status";
	protected static final String CONFIRM_CONSIGNMENT_DELIVERY_CHOICE = "confirmConsignmentDelivery";
	protected static final String DELIVERING_TEMPLATE_CODE = "NPR_Delivering";

	@Resource(name = "fulfillmentContext")
	private FulfillmentContext fulfillmentContext;

	@Resource(name = "fulfillmentProviderContext")
	private FulfillmentProviderContext fulfillmentProviderContext;

	@Resource(name = "warehousingDeliveryService")
	private WarehousingDeliveryService warehousingDeliveryService;

	@Resource
	private WarehousingConsignmentWorkflowService warehousingConsignmentWorkflowService;

	/**
	 * @return the fulfillmentContext
	 */
	protected FulfillmentContext getFulfillmentContext()
	{
		return fulfillmentContext;
	}

	/**
	 * @return the fulfillmentProviderContext
	 */
	public FulfillmentProviderContext getFulfillmentProviderContext()
	{
		return fulfillmentProviderContext;
	}

	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> ctx)
	{
		final ActionResult<ConsignmentModel> actionResult = new ActionResult(ActionResult.SUCCESS);
		final ConsignmentModel consignment = ctx.getData();

		Optional<ConsignmentStatus> updatedStatus = Optional.empty();
		try
		{
			updatedStatus = fulfillmentContext.updateStatusByCurrentStore(consignment);
		}
		catch (final FulfillmentException ex)
		{
			LOG.error(ex.getMessage());
			actionResult.setResultCode(ActionResult.ERROR);
			Messagebox.show(ex.getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
			return actionResult;
		}

		if (updatedStatus.isEmpty())
		{
			actionResult.setResultCode(ActionResult.ERROR);
			Messagebox.show("Could not fetch the shipment status from the fulfillment provider", "Error", Messagebox.OK,
					Messagebox.ERROR);
			return actionResult;
		}

		final WorkflowActionModel packWorkflowAction = this.getWarehousingConsignmentWorkflowService()
				.getWorkflowActionForTemplateCode(DELIVERING_TEMPLATE_CODE, consignment);
		if (packWorkflowAction != null && !WorkflowActionStatus.COMPLETED.equals(packWorkflowAction.getStatus())
				&& ConsignmentStatus.DELIVERY_COMPLETED.equals(updatedStatus.get()))
		{
			getWarehousingDeliveryService().confirmConsignmentDelivery(consignment);
		}

		final EnumSet<StatusFlag> statusFlags = actionResult.getStatusFlags();
		statusFlags.add(StatusFlag.OBJECT_MODIFIED);
		actionResult.setStatusFlags(statusFlags);
		return actionResult;
	}

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> ctx)
	{
		if (ctx == null)
		{
			return false;
		}
		final ConsignmentModel consignment = ctx.getData();
		if (consignment == null || consignment.getOrder() == null)
		{
			return false;
		}
		if (consignment.getCarrierDetails() == null)
		{
			return false;
		}

		final AbstractOrderModel order = consignment.getOrder();

		if (order.getDeliveryMode() == null || PICKUP_IN_STORE_MODE.equalsIgnoreCase(order.getDeliveryMode().getCode()))
		{
			return false;
		}
		final Optional<FulfillmentProviderModel> provider = getFulfillmentProviderContext()
				.getProvider(consignment.getOrder().getStore());

		return consignment.isShipped() && provider.isPresent() && provider.get().getActive()
				&& org.apache.commons.lang3.StringUtils.isNotBlank(consignment.getTrackingID());
	}

	@Override
	public boolean needsConfirmation(final ActionContext<ConsignmentModel> ctx)
	{
		return true;
	}

	@Override
	public String getConfirmationMessage(final ActionContext<ConsignmentModel> ctx)
	{
		return "Do you want to update shipment details for this consignment?";
	}

	/**
	 * @return the warehousingDeliveryService
	 */
	public WarehousingDeliveryService getWarehousingDeliveryService()
	{
		return warehousingDeliveryService;
	}

	/**
	 * @return the warehousingConsignmentWorkflowService
	 */
	public WarehousingConsignmentWorkflowService getWarehousingConsignmentWorkflowService()
	{
		return warehousingConsignmentWorkflowService;
	}
}
