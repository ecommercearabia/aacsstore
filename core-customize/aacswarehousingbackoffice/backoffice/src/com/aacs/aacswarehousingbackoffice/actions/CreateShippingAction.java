/**
 *
 */
package com.aacs.aacswarehousingbackoffice.actions;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.EnumSet;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zul.Messagebox;

import com.aacs.aacsfulfillment.context.FulfillmentContext;
import com.aacs.aacsfulfillment.context.FulfillmentProviderContext;
import com.aacs.aacsfulfillment.exception.FulfillmentException;
import com.aacs.aacsfulfillment.model.FulfillmentProviderModel;
import com.aacs.ordermanagement.model.OrderTrackingIdEmailProcessModel;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.ActionResult.StatusFlag;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class CreateShippingAction extends AbstractComponentWidgetAdapterAware
		implements CockpitAction<ConsignmentModel, ConsignmentModel>
{
	/**
	 *
	 */
	private static final String PICKUP_IN_STORE_MODE = "pickup";
	private static final Logger LOG = LoggerFactory.getLogger(CreateShippingAction.class);
	private static final String TRACKING_ID_NOT_FOUND = "Tracking id not found";

	@Resource(name = "modelService")
	private ModelService modelService;

	/** The business process service. */
	@Resource(name = "businessProcessService")
	private BusinessProcessService businessProcessService;

	/** The process code generator. */
	@Resource(name = "processCodeGenerator")
	private KeyGenerator processCodeGenerator;

	@Resource(name = "fulfillmentContext")
	private FulfillmentContext fulfillmentContext;

	/**
	 * @return the fulfillmentProviderContext
	 */
	protected FulfillmentProviderContext getFulfillmentProviderContext()
	{
		return fulfillmentProviderContext;
	}

	@Resource(name = "fulfillmentProviderContext")
	private FulfillmentProviderContext fulfillmentProviderContext;

	/**
	 * @return the fulfillmentContext
	 */
	protected FulfillmentContext getFulfillmentContext()
	{
		return fulfillmentContext;
	}


	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> ctx)
	{
		ActionResult<ConsignmentModel> actionResult = new ActionResult(ActionResult.SUCCESS);
		if (ctx == null || ctx.getData() == null)
		{
			Messagebox.show("No consignment found for this action");
			actionResult = new ActionResult(ActionResult.ERROR);
			return actionResult;
		}
		final ConsignmentModel consignment = ctx.getData();
		try
		{
			final Optional<String> trackingId = getFulfillmentContext().createShipmentByCurrentStore(consignment);

			final EnumSet<StatusFlag> statusFlags = actionResult.getStatusFlags();
			statusFlags.add(StatusFlag.OBJECT_MODIFIED);
			actionResult.setStatusFlags(statusFlags);
			if (trackingId.isPresent())
			{
				Messagebox.show(String.format("Tracking ID : %s", trackingId.get()));
				// Sending the order tracking email
				sendShipmentNotification(consignment);
			}
			else
			{
				Messagebox.show(TRACKING_ID_NOT_FOUND);
			}
		}
		catch (final FulfillmentException ex)
		{
			LOG.error(ex.getMessage());
			actionResult.setResultCode(ActionResult.ERROR);
			Messagebox.show(ex.getMessage());
		}

		return actionResult;
	}


	private void sendShipmentNotification(final ConsignmentModel consignment)
	{
		if (consignment.getOrder() != null && consignment.getOrder().getStore() != null
				&& consignment.getOrder().getStore().isSendOrderTrackingEmailOnCreateShipmentEnabled())
		{
			final OrderTrackingIdEmailProcessModel sendingOrderTrackingIdProcessModel = (OrderTrackingIdEmailProcessModel) getBusinessProcessService()
					.createProcess("sendingOrderTrackingIdEmail-process-" + consignment.getCode() + "-"
							+ processCodeGenerator.generate().toString(), "sendingOrderTrackingIdEmail-process");
			sendingOrderTrackingIdProcessModel.setConsignment(consignment);
			getModelService().save(sendingOrderTrackingIdProcessModel);
			getBusinessProcessService().startProcess(sendingOrderTrackingIdProcessModel);

			modelService.refresh(consignment);
		}
	}

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> ctx)
	{
		final ConsignmentModel consignment = ctx.getData();

		if (consignment == null || consignment.getOrder() == null)
		{
			return false;
		}

		final AbstractOrderModel order = consignment.getOrder();

		if (order.getDeliveryMode() == null || PICKUP_IN_STORE_MODE.equalsIgnoreCase(order.getDeliveryMode().getCode()))
		{
			return false;
		}

		final Optional<FulfillmentProviderModel> provider = getFulfillmentProviderContext()
				.getProvider(consignment.getOrder().getStore());

		return (provider.isPresent() && Boolean.TRUE.equals(provider.get().getActive()))
				&& checkRequiredStatusesForShipment(consignment);
	}

	/**
	 * To proceed with the fulfillment process in the correct sequence
	 *
	 * @param consignment
	 */
	protected boolean checkRequiredStatusesForShipment(final ConsignmentModel consignment)
	{
		if (consignment == null)
		{
			return false;
		}
		if (consignment.getStatus() == null)
		{
			return false;
		}
		final ConsignmentStatus status = consignment.getStatus();
		return ConsignmentStatus.READY_FOR_PICKUP.equals(status) || ConsignmentStatus.READY_FOR_SHIPPING.equals(status);
	}

	@Override
	public boolean needsConfirmation(final ActionContext<ConsignmentModel> ctx)
	{
		return true;
	}

	@Override
	public String getConfirmationMessage(final ActionContext<ConsignmentModel> ctx)
	{
		if (StringUtils.isNotBlank(ctx.getData().getTrackingID()))
		{
			return "Do you want to create new shipment?";
		}
		else
		{
			return "Do you want to create shipment?";
		}
	}

	/**
	 * @return the businessProcessService
	 */
	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the processCodeGenerator
	 */
	public KeyGenerator getProcessCodeGenerator()
	{
		return processCodeGenerator;
	}

}
