/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacssmarteditcustomaddon.constants;

@SuppressWarnings({"deprecation","PMD","squid:CallToDeprecatedMethod"})
public class AacssmarteditcustomaddonConstants extends GeneratedAacssmarteditcustomaddonConstants
{
	public static final String EXTENSIONNAME = "aacssmarteditcustomaddon";
	
	private AacssmarteditcustomaddonConstants()
	{
		//empty
	}
}
