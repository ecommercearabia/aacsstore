/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscustomercouponsamplescustomaddon.jalo;

import com.aacs.aacscustomercouponsamplescustomaddon.constants.AacscustomercouponsamplescustomaddonConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class AacscustomercouponsamplescustomaddonManager extends GeneratedAacscustomercouponsamplescustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( AacscustomercouponsamplescustomaddonManager.class.getName() );
	
	public static final AacscustomercouponsamplescustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AacscustomercouponsamplescustomaddonManager) em.getExtension(AacscustomercouponsamplescustomaddonConstants.EXTENSIONNAME);
	}
	
}
