/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscustomercouponsamplescustomaddon.constants;

/**
 * Aacscustomercouponsamplescustomaddon constants
 */
public final class AacscustomercouponsamplescustomaddonConstants
{
	public static final String EXTENSIONNAME = "aacscustomercouponsamplescustomaddon";

	private AacscustomercouponsamplescustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
