/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscustomercouponsamplescustomaddon.constants;

/**
 * Global class for all Aacscustomercouponsamplescustomaddon web constants. You can add global constants for your extension into this class.
 */
public final class AacscustomercouponsamplescustomaddonWebConstants // NOSONAR
{
	private AacscustomercouponsamplescustomaddonWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
