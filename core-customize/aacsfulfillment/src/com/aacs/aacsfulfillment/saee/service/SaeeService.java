package com.aacs.aacsfulfillment.saee.service;

import java.util.Optional;

import com.aacs.aacsfulfillment.saee.exceptions.SaeeException;
import com.aacs.aacsfulfillment.saee.model.SaeeOrderData;
import com.aacs.aacsfulfillment.saee.model.responses.Details;


public interface SaeeService
{

	public Optional<String> createShipment(SaeeOrderData param, String baseURL) throws SaeeException;

	public Optional<byte[]> getPDF(String waybill, String baseURL) throws SaeeException;

	public Optional<Details> getStatus(String waybill, String baseURL) throws SaeeException;
}
