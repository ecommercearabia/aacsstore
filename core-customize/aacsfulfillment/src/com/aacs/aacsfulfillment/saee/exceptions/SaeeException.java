package com.aacs.aacsfulfillment.saee.exceptions;

import com.aacs.aacsfulfillment.exception.FulfillmentException;
import com.aacs.aacsfulfillment.exception.enums.FulfillmentExceptionType;



public class SaeeException extends FulfillmentException
{

	private static final long serialVersionUID = 1L;
	private final FulfillmentExceptionType exceptionType;

	public SaeeException(final FulfillmentExceptionType exceptionType, final String message, final String value)
	{
		super(exceptionType, message);
		this.exceptionType = exceptionType;
	}

	public static long getSerialversionuid()
	{
		return serialVersionUID;
	}

	@Override
	public FulfillmentExceptionType getType()
	{
		return exceptionType;
	}

}
