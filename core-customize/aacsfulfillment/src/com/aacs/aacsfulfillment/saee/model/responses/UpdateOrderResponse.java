package com.aacs.aacsfulfillment.saee.model.responses;

/**
 * @author Husam Dababneh
 */
public class UpdateOrderResponse extends Response
{
	private String message;

	public UpdateOrderResponse()
	{
		super();
	}

	public UpdateOrderResponse(final boolean success, final int error_code, final String error, final String message)
	{
		super(success, error_code, error);
		this.message = message;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(final String message)
	{
		this.message = message;
	}

}
