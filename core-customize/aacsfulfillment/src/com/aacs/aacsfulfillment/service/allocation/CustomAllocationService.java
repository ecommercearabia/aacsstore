/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsfulfillment.service.allocation;

import de.hybris.platform.warehousing.allocation.AllocationService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface CustomAllocationService extends AllocationService
{

}
