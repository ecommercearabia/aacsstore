/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsfulfillment.dao;

import java.util.List;
import java.util.Optional;

import com.aacs.aacsfulfillment.model.SaeeUpdateNotificationRecordModel;


/**
 * The Interface SaeeUpdateNotificationRecordDao.
 *
 * @author monzer
 */
public interface SaeeUpdateNotificationRecordDao
{

	/**
	 * Creates the notification record.
	 *
	 * @required @param secretKey the secret key
	 * @required @param response the response
	 * @required @param trackingId the tracking id
	 * @required @param requestStatus the request status
	 * @required @param done the done
	 */
	void createNotificationRecord(String requestBody, String secretKey, String response, String trackingId, String requestStatus,
			boolean done,
			Boolean authorized);

	/**
	 * Find all notification records.
	 *
	 * @return the optional
	 */
	Optional<List<SaeeUpdateNotificationRecordModel>> findAllNotificationRecords();

	/**
	 * Find all notification records by query.
	 *
	 * @param secretKey
	 *           the secret key
	 * @param trackingId
	 *           the tracking id
	 * @param requestStatus
	 *           the request status
	 * @required @param done the done
	 * @return the optional
	 */
	Optional<List<SaeeUpdateNotificationRecordModel>> findAllNotificationRecordsByQuery(String secretKey,
			String trackingId, String requestStatus, boolean done, Boolean authorized);

}
