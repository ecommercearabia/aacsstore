/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsfulfillment.dao.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.consignmenttrackingservices.model.CarrierModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import com.google.common.base.Preconditions;
import com.aacs.aacsfulfillment.dao.CustomConsignmentDao;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultCustomConsignmentDao extends DefaultGenericDao<ConsignmentModel> implements CustomConsignmentDao
{
	private static final String FQL = "SELECT {c:" + ConsignmentModel.PK + "} FROM {" + ConsignmentModel._TYPECODE
			+ " AS c} WHERE {c:"
			+ ConsignmentModel.STATUS + "} != ?status" + " AND {c:" + ConsignmentModel.CARRIERDETAILS + "} = ?carrier" + " AND {c:"
			+ ConsignmentModel.TRACKINGID + "} IS NOT NULL";

	private static final String FQL2 = "SELECT {c:" + ConsignmentModel.PK + "} FROM {" + ConsignmentModel._TYPECODE
			+ " AS c} WHERE {c:" + ConsignmentModel.STATUS + "} != ?status" + " AND {c:" + ConsignmentModel.TRACKINGID
			+ "} IS NOT NULL";

	private static final String QUERY = "SELECT {c:" + ConsignmentModel.PK + "} FROM {" + ConsignmentModel._TYPECODE
			+ " AS c} WHERE {c:" + ConsignmentModel.STATUS + "} != ?status";

	private static final String TRACKING_ID_QUERY = "SELECT {c:" + ConsignmentModel.PK + "} FROM {" + ConsignmentModel._TYPECODE
			+ " AS c} WHERE {c:" + ConsignmentModel.TRACKINGID + "} = ?trackingId";

	private static final String CARRIER_MUST_NOT_BE_NULL = "CarrierModel must not be null";

	private static final String STATUS_MUST_NOT_BE_NULL = "ConsignmentStatus must not be null";

	private static final String STATUS = "status";

	private static final String CARRIER = "carrier";

	private static final String TRACKING_ID = "trackingId";
	/**
	 *
	 */
	public DefaultCustomConsignmentDao()
	{
		super(ConsignmentModel._TYPECODE);
	}

	@Override
	public List<ConsignmentModel> findByCarrierAndNotStatus(final CarrierModel carrier, final ConsignmentStatus status)
	{
		Preconditions.checkArgument(carrier != null, CARRIER_MUST_NOT_BE_NULL);
		Preconditions.checkArgument(status != null, STATUS_MUST_NOT_BE_NULL);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FQL);
		query.addQueryParameter(STATUS, status);
		query.addQueryParameter(CARRIER, carrier);

		final List<ConsignmentModel> result = getFlexibleSearchService().<ConsignmentModel> search(query).getResult();

		if (CollectionUtils.isEmpty(result))
		{
			Collections.emptyList();
		}
		return result;
	}

	@Override
	public List<ConsignmentModel> findByNotStatus(final ConsignmentStatus status)
	{
		Preconditions.checkArgument(status != null, STATUS_MUST_NOT_BE_NULL);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FQL2);
		query.addQueryParameter(STATUS, status);

		final List<ConsignmentModel> result = getFlexibleSearchService().<ConsignmentModel> search(query).getResult();

		if (CollectionUtils.isEmpty(result))
		{
			Collections.emptyList();
		}
		return result;
	}

	@Override
	public List<ConsignmentModel> findByStatus(final ConsignmentStatus status)
	{
		Preconditions.checkArgument(status != null, STATUS_MUST_NOT_BE_NULL);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY);
		query.addQueryParameter(STATUS, status);

		final List<ConsignmentModel> result = getFlexibleSearchService().<ConsignmentModel> search(query).getResult();

		if (CollectionUtils.isEmpty(result))
		{
			Collections.emptyList();
		}
		return result;
	}

	@Override
	public Optional<ConsignmentModel> findByTackingId(final String trackingId, final String baseStoreId)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(trackingId), "Tracking id must not be null");
		final FlexibleSearchQuery query = new FlexibleSearchQuery(TRACKING_ID_QUERY);
		query.addQueryParameter(TRACKING_ID, trackingId);

		final List<ConsignmentModel> result = getFlexibleSearchService().<ConsignmentModel> search(query).getResult();

		if (CollectionUtils.isEmpty(result))
		{
			return Optional.empty();
		}
		return result.stream().filter(consignment -> consignment.getOrder() != null && consignment.getOrder().getStore() != null
				&& baseStoreId.equals(consignment.getOrder().getStore().getUid())).findFirst();
	}

}