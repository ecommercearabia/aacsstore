/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsfulfillment.dao.impl;

import com.aacs.aacsfulfillment.model.SaeeFulfillmentProviderModel;


/**
 * @author Husam Dababneh
 */
public class DefaultSaeeFulfillmentProviderDao extends DefaultFulfillmentProviderDao
{

	public DefaultSaeeFulfillmentProviderDao()
	{
		super(SaeeFulfillmentProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return SaeeFulfillmentProviderModel._TYPECODE;
	}

}
