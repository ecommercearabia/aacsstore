/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsfulfillment.dao.impl;

import com.aacs.aacsfulfillment.dao.FulfillmentProviderDao;
import com.aacs.aacsfulfillment.model.ShipaFulfillmentProviderModel;

/**
 *
 */
public class DefaultShipaFulfillmentProviderDao extends DefaultFulfillmentProviderDao
		implements FulfillmentProviderDao
{

	/**
	 *
	 */
	public DefaultShipaFulfillmentProviderDao()
	{
		super(ShipaFulfillmentProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return ShipaFulfillmentProviderModel._TYPECODE;
	}

}
