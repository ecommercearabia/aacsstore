/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsfulfillment.dao.impl;

import com.aacs.aacsfulfillment.dao.FulfillmentProviderDao;
import com.aacs.aacsfulfillment.model.ShipoxFulfillmentProviderModel;

/**
 *
 */
public class DefaultShipoxFulfillmentProviderDao extends DefaultFulfillmentProviderDao
		implements FulfillmentProviderDao
{

	/**
	 *
	 */
	public DefaultShipoxFulfillmentProviderDao()
	{
		super(ShipoxFulfillmentProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return ShipoxFulfillmentProviderModel._TYPECODE;
	}

}
