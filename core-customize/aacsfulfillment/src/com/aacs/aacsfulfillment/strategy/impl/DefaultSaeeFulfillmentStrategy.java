/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsfulfillment.strategy.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.aacs.aacsfulfillment.exception.FulfillmentException;
import com.aacs.aacsfulfillment.model.FulfillmentProviderModel;
import com.aacs.aacsfulfillment.service.FulfillmentService;
import com.aacs.aacsfulfillment.strategy.FulfillmentStrategy;


/**
 *
 */
public class DefaultSaeeFulfillmentStrategy implements FulfillmentStrategy
{
	@Resource(name = "saeeFulfillmentService")
	private FulfillmentService saeeFulfillmentService;

	/**
	 * @return the saeeFulfillmentService
	 */
	public FulfillmentService getSaeeFulfillmentService()
	{
		return saeeFulfillmentService;
	}

	@Override
	public Optional<String> createShipment(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		return getSaeeFulfillmentService().createShipment(consignmentModel, fulfillmentProviderModel);
	}

	@Override
	public Optional<byte[]> printAWB(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		return getSaeeFulfillmentService().printAWB(consignmentModel, fulfillmentProviderModel);
	}

	@Override
	public Optional<String> getStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		return getSaeeFulfillmentService().getStatus(consignmentModel, fulfillmentProviderModel);
	}


	@Override
	public Optional<ConsignmentStatus> updateStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		return getSaeeFulfillmentService().updateStatus(consignmentModel, fulfillmentProviderModel);
	}

}
