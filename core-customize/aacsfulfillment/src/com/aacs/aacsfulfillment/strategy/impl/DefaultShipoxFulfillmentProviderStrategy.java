/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsfulfillment.strategy.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.aacs.aacsfulfillment.model.FulfillmentProviderModel;
import com.aacs.aacsfulfillment.model.ShipoxFulfillmentProviderModel;
import com.aacs.aacsfulfillment.service.FulfillmentProviderService;
import com.aacs.aacsfulfillment.strategy.FulfillmentProviderStrategy;

/**
 *
 */
public class DefaultShipoxFulfillmentProviderStrategy implements FulfillmentProviderStrategy
{
	@Resource(name = "fulfillmentProviderService")
	private FulfillmentProviderService fulfillmentProviderService;

	protected FulfillmentProviderService getFulfillmentProviderService()
	{
		return fulfillmentProviderService;
	}

	@Override
	public Optional<FulfillmentProviderModel> getActiveProvider(final String baseStoreUid)
	{
		return fulfillmentProviderService.getActive(baseStoreUid, ShipoxFulfillmentProviderModel.class);
	}

	@Override
	public Optional<FulfillmentProviderModel> getActiveProvider(final BaseStoreModel baseStoreModel)
	{
		return getFulfillmentProviderService().getActive(baseStoreModel, ShipoxFulfillmentProviderModel.class);
	}

	@Override
	public Optional<FulfillmentProviderModel> getActiveProviderByCurrentBaseStore()
	{
		return getFulfillmentProviderService().getActiveProviderByCurrentBaseStore(ShipoxFulfillmentProviderModel.class);
	}

}
