package com.aacs.aacsfulfillment.strategy.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.aacs.aacsfulfillment.model.FulfillmentProviderModel;
import com.aacs.aacsfulfillment.model.SaeeFulfillmentProviderModel;
import com.aacs.aacsfulfillment.service.FulfillmentProviderService;
import com.aacs.aacsfulfillment.strategy.FulfillmentProviderStrategy;


/**
 *
 */
public class DefaultSaeeFulfillmentProviderStrategy implements FulfillmentProviderStrategy
{
	@Resource(name = "fulfillmentProviderService")
	private FulfillmentProviderService fulfillmentProviderService;

	protected FulfillmentProviderService getFulfillmentProviderService()
	{
		return fulfillmentProviderService;
	}

	@Override
	public Optional<FulfillmentProviderModel> getActiveProvider(final String baseStoreUid)
	{
		return getFulfillmentProviderService().getActive(baseStoreUid, SaeeFulfillmentProviderModel.class);
	}

	@Override
	public Optional<FulfillmentProviderModel> getActiveProvider(final BaseStoreModel baseStoreModel)
	{
		return getFulfillmentProviderService().getActive(baseStoreModel, SaeeFulfillmentProviderModel.class);
	}

	@Override
	public Optional<FulfillmentProviderModel> getActiveProviderByCurrentBaseStore()
	{
		return getFulfillmentProviderService().getActiveProviderByCurrentBaseStore(SaeeFulfillmentProviderModel.class);
	}
}

