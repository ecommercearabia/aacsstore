/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsfulfillment.strategy;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.Optional;

import com.aacs.aacsfulfillment.exception.FulfillmentException;
import com.aacs.aacsfulfillment.model.FulfillmentProviderModel;


/**
 *
 */
public interface FulfillmentStrategy
{
	public Optional<String> createShipment(ConsignmentModel consignmentModel, FulfillmentProviderModel fulfillmentProviderModel)
			throws FulfillmentException;

	public Optional<byte[]> printAWB(ConsignmentModel consignmentModel, FulfillmentProviderModel fulfillmentProviderModel)
			throws FulfillmentException;

	public Optional<String> getStatus(ConsignmentModel consignmentModel, FulfillmentProviderModel fulfillmentProviderModel)
			throws FulfillmentException;

	public Optional<ConsignmentStatus> updateStatus(ConsignmentModel consignmentModel,
			FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException;
}
