package com.aacs.aacsfulfillment.context;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.aacs.aacsfulfillment.enums.FulfillmentProviderType;
import com.aacs.aacsfulfillment.model.FulfillmentProviderModel;


/**
 *
 */
public interface FulfillmentProviderContext
{
	public Optional<FulfillmentProviderModel> getProvider(Class<?> providerClass);

	public Optional<FulfillmentProviderModel> getProvider(String baseStoreUid, Class<?> providerClass);

	public Optional<FulfillmentProviderModel> getProvider(BaseStoreModel baseStoreModel, Class<?> providerClass);

	public Optional<FulfillmentProviderModel> getProvider(BaseStoreModel baseStoreModel);

	public Optional<FulfillmentProviderModel> getProvider(BaseStoreModel baseStoreModel, final FulfillmentProviderType type);


}