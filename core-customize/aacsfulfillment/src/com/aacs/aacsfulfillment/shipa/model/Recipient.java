package com.aacs.aacsfulfillment.shipa.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 *
 * @author mohammad-abu-muhasien
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Recipient
{
	private String address;
	private String city;
	private String name;
	private String phone;

	public Recipient(final String address, final String city, final String name, final String phone)
	{
		super();
		this.address = address;
		this.city = city;
		this.name = name;
		this.phone = phone;
	}

	public Recipient()
	{

	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(final String address)
	{
		this.address = address;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(final String city)
	{
		this.city = city;
	}

	public String getName()
	{
		return name;
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	public String getPhone()
	{
		return phone;
	}

	public void setPhone(final String phone)
	{
		this.phone = phone;
	}

	@Override
	public String toString()
	{
		return "Recipient [address=" + address + ", city=" + city + ", name=" + name + ", phone=" + phone + "]";
	}

}
