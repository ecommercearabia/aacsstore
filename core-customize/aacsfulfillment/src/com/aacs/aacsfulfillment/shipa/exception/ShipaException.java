package com.aacs.aacsfulfillment.shipa.exception;

import com.aacs.aacsfulfillment.exception.FulfillmentException;
import com.aacs.aacsfulfillment.exception.enums.FulfillmentExceptionType;


public class ShipaException extends FulfillmentException
{


	/**
	 *
	 */
	public ShipaException(final FulfillmentExceptionType type)
	{
		super(type);
	}

	/**
	 *
	 */
	public ShipaException(final FulfillmentExceptionType type, final Throwable throwable)
	{
		super(type, throwable);
	}

	private static final long serialVersionUID = 1L;



}