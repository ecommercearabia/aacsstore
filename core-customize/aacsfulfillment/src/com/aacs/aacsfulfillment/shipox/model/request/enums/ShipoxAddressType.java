/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsfulfillment.shipox.model.request.enums;

/**
 *
 */
public enum ShipoxAddressType
{

	BUSINESS("business"), RESIDENTIAL("residential");

	private String code;


	private ShipoxAddressType(final String code)
	{
		this.code = code;
	}

	/**
	 * @return the code
	 */
	public String getCode()
	{
		return code;
	}
}
