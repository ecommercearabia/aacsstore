package com.aacs.aacsfulfillment.shipox.model.response;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class TrackOrderResponse implements Serializable {
	private String status;
	private List<Order> ordersResponse;
	private Map<String, Object> responseMap;

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public List<Order> getOrdersResponse() {
		return ordersResponse;
	}

	public void setOrdersResponse(final List<Order> ordersResponse) {
		this.ordersResponse = ordersResponse;
	}

	public Map<String, Object> getResponseMap() {
		return responseMap;
	}

	public void setResponseMap(final Map<String, Object> responseMap) {
		this.responseMap = responseMap;
	}

	@Override
	public String toString() {
		return "TrackOrderResponse [status=" + status + ", ordersResponse=" + ordersResponse + ", responseMap="
				+ responseMap + "]";
	}
}