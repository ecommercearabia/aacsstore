package com.aacs.aacsfulfillment.shipox.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class UserCredentials {
	private String username;
	private String password;
	private boolean remember_me;

	public UserCredentials() {
		// TODO Auto-generated constructor stub
	}

	public UserCredentials(final String username, final String password, final boolean remember_me) {
		super();
		this.username = username;
		this.password = password;
		this.remember_me = remember_me;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	public boolean isRemember_me() {
		return remember_me;
	}

	public void setRemember_me(final boolean remember_me) {
		this.remember_me = remember_me;
	}

	@Override
	public String toString() {
		return "UserCredentials [username=" + username + ", password=" + password + ", remember_me=" + remember_me
				+ "]";
	}

}
