package com.aacs.aacsfulfillment.shipox.model.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class ShipoxUserAuthentication implements Serializable{

	private String username;
	private String password;
	private String remember_me;

	public String getUsername() {
		return username;
	}
	public void setUsername(final String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(final String password) {
		this.password = password;
	}
	public String getRemember_me() {
		return remember_me;
	}
	public void setRemember_me(final String remember_me) {
		this.remember_me = remember_me;
	}

	@Override
	public String toString() {
		return "UserAuthenticationData [username=" + username + ", password=" + password + ", remember_me="
				+ remember_me + "]";
	}

}
