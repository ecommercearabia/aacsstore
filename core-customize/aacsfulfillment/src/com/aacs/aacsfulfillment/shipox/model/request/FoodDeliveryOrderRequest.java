package com.aacs.aacsfulfillment.shipox.model.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class FoodDeliveryOrderRequest {
	private ShipoxUserBean sender;
	private ShipoxUserBean recipient;
	private String reference_id;
	List<PickupMetaData> pickup_meta_data;
	private double payment_total;
	private String payment_type;
	private String pickup_datetime;
	private PaymentMetaData payment_meta_data;
	private long courier_id;
	private String deliver_datetime;
	private String description;
	private double driver_tip;
	private double external_driver_fee;

	public ShipoxUserBean getSender() {
		return sender;
	}

	public void setSender(final ShipoxUserBean sender) {
		this.sender = sender;
	}

	public ShipoxUserBean getRecipient() {
		return recipient;
	}

	public void setRecipient(final ShipoxUserBean recipient) {
		this.recipient = recipient;
	}

	public String getReference_id() {
		return reference_id;
	}

	public void setReference_id(final String reference_id) {
		this.reference_id = reference_id;
	}

	public List<PickupMetaData> getPickup_meta_data() {
		return pickup_meta_data;
	}

	public void setPickup_meta_data(final List<PickupMetaData> pickup_meta_data) {
		this.pickup_meta_data = pickup_meta_data;
	}

	public double getPayment_total() {
		return payment_total;
	}

	public void setPayment_total(final double payment_total) {
		this.payment_total = payment_total;
	}

	public String getPayment_type() {
		return payment_type;
	}

	public void setPayment_type(final String payment_type) {
		this.payment_type = payment_type;
	}

	public String getPickup_datetime() {
		return pickup_datetime;
	}

	public void setPickup_datetime(final String pickup_datetime) {
		this.pickup_datetime = pickup_datetime;
	}

	public PaymentMetaData getPayment_meta_data() {
		return payment_meta_data;
	}

	public void setPayment_meta_data(final PaymentMetaData payment_meta_data) {
		this.payment_meta_data = payment_meta_data;
	}

	public long getCourier_id() {
		return courier_id;
	}

	public void setCourier_id(final long courier_id) {
		this.courier_id = courier_id;
	}

	public String getDeliver_datetime() {
		return deliver_datetime;
	}

	public void setDeliver_datetime(final String deliver_datetime) {
		this.deliver_datetime = deliver_datetime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public double getDriver_tip() {
		return driver_tip;
	}

	public void setDriver_tip(final double driver_tip) {
		this.driver_tip = driver_tip;
	}

	public double getExternal_driver_fee() {
		return external_driver_fee;
	}

	public void setExternal_driver_fee(final double external_driver_fee) {
		this.external_driver_fee = external_driver_fee;
	}

}
