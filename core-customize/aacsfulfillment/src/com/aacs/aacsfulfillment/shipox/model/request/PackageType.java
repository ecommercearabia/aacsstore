package com.aacs.aacsfulfillment.shipox.model.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class PackageType implements Serializable {

	private String courier_type;

	public String getCourier_type() {
		return courier_type;
	}

	public void setCourier_type(final String courier_type) {
		this.courier_type = courier_type;
	}

	@Override
	public String toString() {
		return "PackageType [couriwe_type=" + courier_type + "]";
	}

}
