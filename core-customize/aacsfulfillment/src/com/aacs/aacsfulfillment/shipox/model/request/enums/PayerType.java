package com.aacs.aacsfulfillment.shipox.model.request.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum PayerType {
	@JsonProperty("recipient")RECIPIENT, @JsonProperty("sender")SENDER;
}
