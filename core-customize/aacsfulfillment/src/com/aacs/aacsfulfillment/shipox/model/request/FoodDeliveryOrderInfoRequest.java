package com.aacs.aacsfulfillment.shipox.model.request;

import com.aacs.aacsfulfillment.shipox.model.request.enums.PaymentType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class FoodDeliveryOrderInfoRequest {
	private int page;
	private PaymentType paymentType;
	private String search;
	private int size;
	private String status;

	public int getPage() {
		return page;
	}
	public void setPage(final int page) {
		this.page = page;
	}
	public PaymentType getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(final PaymentType paymentType) {
		this.paymentType = paymentType;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(final String search) {
		this.search = search;
	}
	public int getSize() {
		return size;
	}
	public void setSize(final int size) {
		this.size = size;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(final String status) {
		this.status = status;
	}

}
