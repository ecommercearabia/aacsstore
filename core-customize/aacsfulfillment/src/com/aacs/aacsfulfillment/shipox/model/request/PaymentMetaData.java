package com.aacs.aacsfulfillment.shipox.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class PaymentMetaData {
	private String key;
	private String value;

	public String getKey() {
		return key;
	}

	public void setKey(final String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(final String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "PaymentMetaData [key=" + key + ", value=" + value + "]";
	}

}
