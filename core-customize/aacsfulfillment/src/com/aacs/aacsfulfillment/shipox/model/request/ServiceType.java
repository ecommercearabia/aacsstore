package com.aacs.aacsfulfillment.shipox.model.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class ServiceType implements Serializable{

	private String id;
	private String code;
	private String name;
	private String description;
	public String getId() {
		return id;
	}
	public void setId(final String id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(final String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(final String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(final String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "ServiceType [id=" + id + ", code=" + code + ", name=" + name + ", description=" + description + "]";
	}

}
