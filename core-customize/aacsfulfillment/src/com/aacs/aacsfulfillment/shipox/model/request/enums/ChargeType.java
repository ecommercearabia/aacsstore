package com.aacs.aacsfulfillment.shipox.model.request.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public enum ChargeType {

    @JsonProperty("COD")
COD("cod"), @JsonProperty("SERVICE_CUSTOM") SERVICE_CUSTOM("service_custom");

	private String value;

	private ChargeType(final String value) {
		this.value = value;
	}
	@JsonCreator
	public String getValue() {
		return value;
	}

}
