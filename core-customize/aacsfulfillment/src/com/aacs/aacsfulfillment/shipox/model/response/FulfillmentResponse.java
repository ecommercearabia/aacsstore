package com.aacs.aacsfulfillment.shipox.model.response;
import java.io.Serializable;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * @author monzer
 *
 */
@JsonIgnoreProperties
public class FulfillmentResponse implements Serializable
{
	private Map<String, Object> responseMap;
	private String status;
	private String trackingId;
	private String orderStatus;
	private byte[] awbFile;
	private String id;
	private String orderNumber;
	public Map<String, Object> getResponseMap() {
		return responseMap;
	}
	public void setResponseMap(final Map<String, Object> responseMap) {
		this.responseMap = responseMap;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(final String status) {
		this.status = status;
	}
	public String getTrackingId() {
		return trackingId;
	}
	public void setTrackingId(final String trackingId) {
		this.trackingId = trackingId;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(final String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public byte[] getAwbFile() {
		return awbFile;
	}
	public void setAwbFile(final byte[] awbFile) {
		this.awbFile = awbFile;
	}
	public String getId() {
		return id;
	}
	public void setId(final String id) {
		this.id = id;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(final String orderNumber) {
		this.orderNumber = orderNumber;
	}
	@Override
	public String toString() {
		return "ShipoxResponse [responseMap=" + responseMap + ", status=" + status + ", trackingId=" + trackingId
				+ ", orderStatus=" + orderStatus + ", id=" + id + ", orderNumber=" + orderNumber + "]";
	}
}