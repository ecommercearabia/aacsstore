package com.aacs.aacsfulfillment.shipox.model.request;

import java.io.Serializable;
import java.util.Arrays;

import com.aacs.aacsfulfillment.shipox.model.request.enums.DeliveryNote;
import com.aacs.aacsfulfillment.shipox.model.request.enums.PayerType;
import com.aacs.aacsfulfillment.shipox.model.request.enums.PaymentType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties
public class ShipoxOrderRequest implements Serializable
{

	private ShipoxUserBean sender_data;
	private ShipoxUserBean recipient_data;
	private Dimension dimensions;
	private PackageType package_type;
	private ChargeItem[] charge_items;

	private DeliveryNote recipient_not_available;
	private PaymentType payment_type;
	private PayerType payer;
	private Double parcel_value;
	private Boolean fragile;
	private String note;
	private Integer piece_count;
	private Boolean force_create;
	private String reference_id;

	public ShipoxUserBean getSender_data()
	{
		return sender_data;
	}

	public void setSender_data(final ShipoxUserBean sender_data)
	{
		this.sender_data = sender_data;
	}

	public ShipoxUserBean getRecipient_data()
	{
		return recipient_data;
	}

	public void setRecipient_data(final ShipoxUserBean recipient_data)
	{
		this.recipient_data = recipient_data;
	}

	public PackageType getPackage_type()
	{
		return package_type;
	}

	public void setPackage_type(final PackageType package_type)
	{
		this.package_type = package_type;
	}

	public ChargeItem[] getCharge_items()
	{
		return charge_items;
	}

	public void setCharge_items(final ChargeItem[] charge_items)
	{
		this.charge_items = charge_items;
	}

	public DeliveryNote getRecipient_not_available()
	{
		return recipient_not_available;
	}

	public void setRecipient_not_available(final DeliveryNote recipient_not_available)
	{
		this.recipient_not_available = recipient_not_available;
	}

	public PaymentType getPayment_type()
	{
		return payment_type;
	}

	public void setPayment_type(final PaymentType payment_type)
	{
		this.payment_type = payment_type;
	}

	public PayerType getPayer()
	{
		return payer;
	}

	public void setPayer(final PayerType payer)
	{
		this.payer = payer;
	}

	public Double getParcel_value()
	{
		return parcel_value;
	}

	public Dimension getDimensions()
	{
		return dimensions;
	}

	public void setDimensions(final Dimension dimensions)
	{
		this.dimensions = dimensions;
	}

	public void setParcel_value(final Double parcel_value)
	{
		this.parcel_value = parcel_value;
	}

	public Boolean getFragile()
	{
		return fragile;
	}

	public void setFragile(final Boolean fragile)
	{
		this.fragile = fragile;
	}

	public String getNote()
	{
		return note;
	}

	public void setNote(final String note)
	{
		this.note = note;
	}

	public Integer getPiece_count()
	{
		return piece_count;
	}

	public void setPiece_count(final Integer piece_count)
	{
		this.piece_count = piece_count;
	}

	public Boolean getForce_create()
	{
		return force_create;
	}

	public void setForce_create(final Boolean force_create)
	{
		this.force_create = force_create;
	}

	public String getReference_id()
	{
		return reference_id;
	}

	public void setReference_id(final String reference_id)
	{
		this.reference_id = reference_id;
	}

	@Override
	public String toString()
	{
		return "OrderData [sender_data=" + sender_data + ", recipient_data=" + recipient_data + ", dimentions=" + dimensions
				+ ", package_type=" + package_type + ", charge_items=" + Arrays.toString(charge_items) + ", recipient_not_available="
				+ recipient_not_available + ", payment_type=" + payment_type + ", payer=" + payer + ", parcel_value=" + parcel_value
				+ ", fragile=" + fragile + ", note=" + note + ", piece_count=" + piece_count + ", force_create=" + force_create
				+ ", reference_id=" + reference_id + "]";
	}

}
