package com.aacs.aacsfulfillment.shipox.exceptions;

import com.aacs.aacsfulfillment.shipox.exceptions.types.ShipoxExceptionType;

public class ShipoxException extends Exception {
	private final ShipoxExceptionType type;

	public ShipoxException(final String message, final ShipoxExceptionType type, final Throwable cause) {
		super(message, cause);
		this.type = type;
	}

	public ShipoxException(final String message, final ShipoxExceptionType type) {
		super(message);
		this.type = type;
	}

	public ShipoxExceptionType getType() {
		return type;
	}

}
