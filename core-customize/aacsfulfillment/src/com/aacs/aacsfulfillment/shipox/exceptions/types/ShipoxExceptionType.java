package com.aacs.aacsfulfillment.shipox.exceptions.types;

public enum ShipoxExceptionType {

	ACCESS_DENIED("Access Denied", "Doesn’t have token or expired", 401),
	FORBIDDEN("Forbidden", "Doesn’t have permission", 403),
	AMOUNT("amount", "Value can not be lower than 0", 400),
	LOCATIONS1("locations", "Can not create order without sender or recipient locations", 400),
	LOCATIONS2("locations", "There can be only one pickup and drop-off location", 400),
	LOCATIONS3("locations", "Provide either lat and lon coordinates or city for pickup location", 400),
	LOCATIONS4("locations", "Provide either lat and lon coordinates or city for drop-off location", 400),
	LOCATIONS5("locations", "Provided sender city code and/or country id is not valid (Check Cities\\Countries API)", 400),
	LOCATIONS6("locations", "Provided recipient city code and/or country id is not valid (Check Cities\\Countries API)", 400),
	PAYER("payer", "Only sender can be a payer when payment type is credit card", 400),
	RECIPIENT_NOT_AVAILABLE("recipient_not_available", "We can’t leave the item to anyone if there is Cash to collect", 400),
	CREDIT_BALANCE_NOT_ACTIVE("credit_balance_not_active", "credit_balance payment type is not activated", 400),
	CREDIT_BALANCE("credit_balance", "Sorry, you have reached credit_balance limit, select another payment type", 400),
	PACKAGE1("package", "Same Day delivery is available for pickup between 10:00 and 16:00. Please try to select another service type or schedule parcel pickup to another date", 400),
	PACKAGE2("package", "Package price with provided parameters not found (Check Service Types and Starting From API)", 400),
	STATUS1("status", "Status can not be Unassigned when driver is selected", 400),
	STATUS2("status", "Status can not be Assigned To Courier when driver is selected", 400),
	CHARGE_ITEM1("charge_item", "You cannot provide more than two charge items", 400),
	CHARGE_ITEM2("charge_item", "You cannot provide two charge items with the same charge types", 400),
	REFERENCE_ID("reference_id", "You cannot provide existing reference id", 400),
	BAD_REQUEST("bad_request", "Bad Request ...", 400),
	INVALID_RESPONSE_FORMAT("invalid_response_format", "Invalid Response Format ..", 400),
	INVALID_ORDER_NUMBER("invalid_order_number", "invalid_order_number ..", 400),
	YOU_EXCEED_THE_MAX_LIMIT_OF_SIZE("invalid_order_number", "you exceed the max limit of size ..", 400),
	INVALID_USER_NAME("invalid_user_name", "invalid_order_number ..", 400),
	INVALID_PASSWORD("invalid_password", "invalid_order_number ..", 400),
	INVALID_URL("invalid_url", "invalid_order_number ..", 400),
	AUTHENTICATION_FAILED("Authentication failed", "Authentication failed", 400);

	private String label;
	private String message;
	private int errorCode;

	private ShipoxExceptionType(final String label, final String message, final int errorCode) {
		this.label = label;
		this.message = message;
		this.errorCode = errorCode;
	}

	public String getLabel() {
		return label;
	}

	public String getMessage() {
		return message;
	}

	public int getErrorCode() {
		return errorCode;
	}

}
