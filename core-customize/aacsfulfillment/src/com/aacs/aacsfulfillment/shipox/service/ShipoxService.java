package com.aacs.aacsfulfillment.shipox.service;

import com.aacs.aacsfulfillment.shipox.exceptions.ShipoxException;
import com.aacs.aacsfulfillment.shipox.model.request.ShipoxOrderRequest;
import com.aacs.aacsfulfillment.shipox.model.request.UserCredentials;
import com.aacs.aacsfulfillment.shipox.model.response.FulfillmentResponse;
import com.aacs.aacsfulfillment.shipox.model.response.TrackOrderResponse;


public interface ShipoxService
{



	public FulfillmentResponse getOrderDetails(long orderId, String baseURL, String username, String password)
			throws ShipoxException;

	public FulfillmentResponse cancelOrder(long orderId, String baseURL, String username, String password) throws ShipoxException;

	public TrackOrderResponse trackOrder(String orderNumber, String baseURL, String username, String password)
			throws ShipoxException;

	public FulfillmentResponse printAWBLabel(String orderId, String baseUrl, String username, String password)
			throws ShipoxException;

	public String authorize(UserCredentials userCredentials, String authURL) throws ShipoxException;

	public FulfillmentResponse createOrder(ShipoxOrderRequest orderData, String baseURL, String username, String password)
			throws ShipoxException;


}
