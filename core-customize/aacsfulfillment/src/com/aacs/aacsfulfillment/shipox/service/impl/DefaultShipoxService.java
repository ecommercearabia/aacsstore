package com.aacs.aacsfulfillment.shipox.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.aacs.aacsfulfillment.shipox.exceptions.ShipoxException;
import com.aacs.aacsfulfillment.shipox.exceptions.types.ShipoxExceptionType;
import com.aacs.aacsfulfillment.shipox.model.request.ChargeItem;
import com.aacs.aacsfulfillment.shipox.model.request.Dimension;
import com.aacs.aacsfulfillment.shipox.model.request.PackageType;
import com.aacs.aacsfulfillment.shipox.model.request.ShipoxOrderRequest;
import com.aacs.aacsfulfillment.shipox.model.request.ShipoxUserBean;
import com.aacs.aacsfulfillment.shipox.model.request.UserCredentials;
import com.aacs.aacsfulfillment.shipox.model.response.FulfillmentResponse;
import com.aacs.aacsfulfillment.shipox.model.response.Order;
import com.aacs.aacsfulfillment.shipox.model.response.TrackOrderResponse;
import com.aacs.aacsfulfillment.shipox.service.ShipoxService;
import com.aacs.aacswebserviceapi.util.WebServiceApiUtil;
import com.google.common.base.Preconditions;


/**
 * @author mohammad-abumuhasien
 * @author monzer The Class DefaultShipoxService.
 */
@Service
public class DefaultShipoxService implements ShipoxService
{

	private static final String AUTHENTICATION_FAILED = ", authentication failed!";
	private static final String COULD_NOT_EXTRACT_THE_RESPONSE = "Could not extract the response ";
	private static final String REQUEST_URL = "Request url: {}";
	//Response attribute names
	private static final String VALUE = "value";
	private static final String ID_TOKEN = "id_token";
	private static final String ORDER_NUMBER = "order_number";
	private static final String ID = "id";
	private static final String DATA = "data";
	private static final String STATUS = "status";
	private static final String BEARER = "Bearer ";

	// Request Paths
	private static final String HISTORY_ITEMS = "/history_items";
	private static final String MOBILE = "/mobile";
	private static final String STATUS_STATUS_CANCELLED = "/status?status=cancelled";
	private static final String CREATE_ORDER_PATH = "/api/v1/customer/order";
	private static final String CREATE_ORDER_V2_PATH = "/api/v2/customer/order";

	private static final String AUTHORIZATION_PATH = "/api/v1/customer/authenticate";
	private static final String PRINT_AWB_PATH = "/api/v1/customer/order/";

	// Authorization header
	private static final String AUTHORIZATION = "Authorization";

	private static final Logger LOG = LoggerFactory.getLogger(DefaultShipoxService.class);

	@Override
	public FulfillmentResponse getOrderDetails(final long orderId, final String baseURL, final String username,
			final String password) throws ShipoxException
	{
		if (StringUtils.isBlank(username))
		{
			throw new ShipoxException(ShipoxExceptionType.INVALID_USER_NAME.getMessage(), ShipoxExceptionType.INVALID_USER_NAME);
		}
		if (StringUtils.isBlank(password))
		{
			throw new ShipoxException(ShipoxExceptionType.INVALID_PASSWORD.getMessage(), ShipoxExceptionType.INVALID_PASSWORD);
		}
		if (StringUtils.isBlank(baseURL))
		{
			throw new ShipoxException(ShipoxExceptionType.INVALID_URL.getMessage(), ShipoxExceptionType.INVALID_URL);
		}
		LOG.info("Getting Order Details in progress");
		final String url = baseURL + CREATE_ORDER_PATH + "/" + orderId + MOBILE;
		LOG.info(REQUEST_URL, url);

		final MultiValueMap<String, String> headers = getAuthorizationHeader(username, password, baseURL);
		ResponseEntity<?> response = null;
		try
		{
			response = WebServiceApiUtil.httpGet(url, headers, null, Map.class);
		}
		catch (final Exception e)
		{
			throw new ShipoxException(e.getMessage(), ShipoxExceptionType.BAD_REQUEST);
		}
		// Validate Response
		validateResponse(response);

		// Fetching and mapping the response into a proper class
		LOG.info("Request has been sent successfulty.");
		final Map<String, Object> responseMap = (Map<String, Object>) response.getBody();
		if (responseMap == null)
		{
			throw new ShipoxException(COULD_NOT_EXTRACT_THE_RESPONSE + response.getStatusCodeValue() + AUTHENTICATION_FAILED,
					ShipoxExceptionType.AUTHENTICATION_FAILED);
		}

		final FulfillmentResponse responseData = new FulfillmentResponse();
		final Map<String, Object> data = (Map<String, Object>) responseMap.get(DATA);
		responseData.setStatus(String.valueOf(responseMap.get(STATUS)));
		responseData.setId(String.valueOf(data.get(ID)));
		responseData.setOrderNumber(String.valueOf(data.get(ORDER_NUMBER)));
		responseData.setOrderStatus(String.valueOf(data.get(STATUS)));
		responseData.setResponseMap(responseMap);
		return responseData;
	}

	private void validateResponse(final ResponseEntity<?> response) throws ShipoxException
	{
		if (response == null)
		{
			throw new ShipoxException(ShipoxExceptionType.INVALID_RESPONSE_FORMAT.getMessage(),
					ShipoxExceptionType.INVALID_RESPONSE_FORMAT);
		}
		if (!response.getStatusCode().is2xxSuccessful())
		{
			throw new ShipoxException("Response: " + response.getStatusCodeValue() + AUTHENTICATION_FAILED,
					ShipoxExceptionType.AUTHENTICATION_FAILED);
		}

		if (response.getBody() == null)
		{
			throw new ShipoxException(COULD_NOT_EXTRACT_THE_RESPONSE + response.getStatusCodeValue() + AUTHENTICATION_FAILED,
					ShipoxExceptionType.AUTHENTICATION_FAILED);
		}

		if (!(response.getBody() instanceof LinkedHashMap))
		{
			throw new ShipoxException(COULD_NOT_EXTRACT_THE_RESPONSE + response.getStatusCodeValue() + AUTHENTICATION_FAILED,
					ShipoxExceptionType.AUTHENTICATION_FAILED);
		}
	}

	@Override
	public FulfillmentResponse cancelOrder(final long orderId, final String baseURL, final String username, final String password)
			throws ShipoxException
	{
		if (StringUtils.isBlank(username))
		{
			throw new ShipoxException(ShipoxExceptionType.INVALID_USER_NAME.getMessage(), ShipoxExceptionType.INVALID_USER_NAME);
		}
		if (StringUtils.isBlank(password))
		{
			throw new ShipoxException(ShipoxExceptionType.INVALID_PASSWORD.getMessage(), ShipoxExceptionType.INVALID_PASSWORD);
		}
		if (StringUtils.isBlank(baseURL))
		{
			throw new ShipoxException(ShipoxExceptionType.INVALID_URL.getMessage(), ShipoxExceptionType.INVALID_URL);
		}
		final String url = baseURL + CREATE_ORDER_PATH + "/" + orderId + STATUS_STATUS_CANCELLED;

		LOG.info("canceling Order  in progress");
		LOG.info(REQUEST_URL, url);
		final MultiValueMap<String, String> headers = getAuthorizationHeader(username, password, baseURL);
		ResponseEntity<?> response = null;
		try
		{
			response = WebServiceApiUtil.httpPut(url, null, headers, Map.class);
			LOG.info("Your order canceling Request has been sent successfulty.");
		}
		catch (final Exception e)
		{
			throw new ShipoxException(e.getMessage(), ShipoxExceptionType.BAD_REQUEST);
		}

		// Validate Response
		validateResponse(response);

		// Fetching and mapping the response into a proper class
		final Map<String, Object> responseMap = (Map<String, Object>) response.getBody();
		if (responseMap == null)
		{
			throw new ShipoxException(COULD_NOT_EXTRACT_THE_RESPONSE + response.getStatusCodeValue() + AUTHENTICATION_FAILED,
					ShipoxExceptionType.AUTHENTICATION_FAILED);
		}


		final FulfillmentResponse responseData = new FulfillmentResponse();
		responseData.setStatus(String.valueOf(responseMap.get(STATUS)));
		return responseData;
	}

	private MultiValueMap<String, String> getAuthorizationHeader(final String username, final String password,
			final String baseURL) throws ShipoxException
	{
		final UserCredentials user = new UserCredentials();
		user.setUsername(username);
		user.setPassword(password);
		final String authToken = this.authorize(user, baseURL);
		final MultiValueMap<String, String> authorizationHeader = new LinkedMultiValueMap<>();
		authorizationHeader.add(AUTHORIZATION, BEARER + authToken);
		return authorizationHeader;
	}

	@Override
	public TrackOrderResponse trackOrder(final String orderNumber, final String baseURL, final String username,
			final String password) throws ShipoxException
	{
		if (StringUtils.isBlank(orderNumber))
		{
			throw new ShipoxException(ShipoxExceptionType.INVALID_ORDER_NUMBER.getMessage(),
					ShipoxExceptionType.INVALID_ORDER_NUMBER);
		}
		if (StringUtils.isBlank(username))
		{
			throw new ShipoxException(ShipoxExceptionType.INVALID_USER_NAME.getMessage(), ShipoxExceptionType.INVALID_USER_NAME);
		}
		if (StringUtils.isBlank(password))
		{
			throw new ShipoxException(ShipoxExceptionType.INVALID_PASSWORD.getMessage(), ShipoxExceptionType.INVALID_PASSWORD);
		}
		if (StringUtils.isBlank(baseURL))
		{
			throw new ShipoxException(ShipoxExceptionType.INVALID_URL.getMessage(), ShipoxExceptionType.INVALID_URL);
		}
		LOG.info("Send Track Order in progress");
		final String url = baseURL + CREATE_ORDER_PATH + "/" + orderNumber + HISTORY_ITEMS;
		LOG.info(REQUEST_URL, url);
		final MultiValueMap<String, String> headers = getAuthorizationHeader(username, password, baseURL);
		ResponseEntity<?> response = null;
		try
		{
			response = WebServiceApiUtil.httpGet(url, headers, null, Map.class);
		}
		catch (final Exception e)
		{
			throw new ShipoxException(e.getMessage(), ShipoxExceptionType.BAD_REQUEST);
		}
		// Validate Response
		validateResponse(response);

		// Fetching and mapping the response into a proper class
		LOG.info("Your Track Order Request has been sent successfulty.");
		final Map<String, Object> responseMap = (Map<String, Object>) response.getBody();
		if (responseMap == null)
		{
			throw new ShipoxException(COULD_NOT_EXTRACT_THE_RESPONSE + response.getStatusCodeValue() + AUTHENTICATION_FAILED,
					ShipoxExceptionType.AUTHENTICATION_FAILED);
		}


		final TrackOrderResponse responseData = new TrackOrderResponse();
		responseData.setStatus(String.valueOf(responseMap.get(STATUS)));
		final Map<String, Object> data = (Map<String, Object>) responseMap.get(DATA);
		final Object object = data.get("list");
		final List<Order> orders = new ArrayList<>();
		if (object instanceof ArrayList<?>)
		{
			final List<Map<String, Object>> o = (ArrayList<Map<String, Object>>) object;
			o.stream().forEach(order -> {
				final Order ord = new Order();
				ord.setId(String.valueOf(order.get(ID)));
				ord.setOrderStatus(String.valueOf(order.get(STATUS)));
				orders.add(ord);
			});
		}
		responseData.setOrdersResponse(orders);
		responseData.setResponseMap(responseMap);
		return responseData;
	}

	@Override
	public FulfillmentResponse printAWBLabel(final String orderId, final String baseURL, final String username,
			final String password) throws ShipoxException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(baseURL), "Base URL cannot be empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(orderId), "IDs list cannot be empty");
		final MultiValueMap<String, String> authorizationHeader = getAuthorizationHeader(username, password, baseURL);
		final String fullURL = buildFullURL(orderId, baseURL, PRINT_AWB_PATH);
		ResponseEntity<?> responseEntity = null;
		try
		{
			responseEntity = WebServiceApiUtil.httpGet(fullURL, authorizationHeader, null, LinkedHashMap.class);
		}
		catch (final Exception e)
		{
			throw new ShipoxException(e.getMessage(), ShipoxExceptionType.BAD_REQUEST);
		}
		// Validate Response
		validateResponse(responseEntity);

		// Fetching and mapping the response into a proper class
		final Map<String, Map<String, Object>> objectToMap = (Map<String, Map<String, Object>>) responseEntity.getBody();
		final String awbURL = String
				.valueOf(objectToMap == null || objectToMap.get(DATA) == null ? null : objectToMap.get(DATA).get(VALUE));

		ResponseEntity<byte[]> awbFile = null;
		try
		{
			awbFile = WebServiceApiUtil.httpGetPdf(awbURL);
		}
		catch (final Exception e)
		{
			throw new ShipoxException(e.getMessage(), ShipoxExceptionType.BAD_REQUEST);
		}
		if (awbFile == null)
		{
			throw new ShipoxException(ShipoxExceptionType.INVALID_RESPONSE_FORMAT.getMessage(),
					ShipoxExceptionType.INVALID_RESPONSE_FORMAT);
		}

		final Map<String, Object> responsemMap = (LinkedHashMap<String, Object>) responseEntity.getBody();
		if (responsemMap == null)
		{
			throw new ShipoxException(COULD_NOT_EXTRACT_THE_RESPONSE + responseEntity.getStatusCodeValue() + AUTHENTICATION_FAILED,
					ShipoxExceptionType.AUTHENTICATION_FAILED);
		}
		final FulfillmentResponse response = new FulfillmentResponse();
		response.setStatus(String.valueOf(responsemMap.get(STATUS)));
		response.setAwbFile(awbFile.getBody());
		response.setResponseMap(responsemMap);
		return response;
	}

	private String buildFullURL(final String orderID, final String baseURL, final String path)
	{
		final StringBuilder builder = new StringBuilder();
		builder.append(baseURL).append(path).append(orderID).append("/airwaybill");
		return builder.toString();
	}

	@Override
	public String authorize(final UserCredentials userCredentials, final String baseURL) throws ShipoxException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(baseURL), "Base URL cannot be empty");
		Preconditions.checkArgument(userCredentials != null, "User credentials cannot be null for authentication");
		Preconditions.checkArgument(StringUtils.isNotBlank(userCredentials.getUsername()),
				"User name cannot be null for authentication");
		Preconditions.checkArgument(StringUtils.isNotBlank(userCredentials.getPassword()),
				"User Password cannot be null for authentication");
		final String fullURL = baseURL + AUTHORIZATION_PATH;

		ResponseEntity<?> responseEntity = null;
		try
		{
			responseEntity = WebServiceApiUtil.httPOST(fullURL, userCredentials, null, LinkedHashMap.class);
		}
		catch (final Exception e)
		{
			throw new ShipoxException(e.getMessage(), ShipoxExceptionType.BAD_REQUEST);
		}
		// Validate Response
		validateResponse(responseEntity);
		final Map<String, Object> body = (Map<String, Object>) responseEntity.getBody();
		final boolean isNull = body == null;
		final Map<String, Object> objectToMap = isNull ? null : (Map<String, Object>) body.get(DATA);
		if (objectToMap == null)
		{
			throw new ShipoxException(COULD_NOT_EXTRACT_THE_RESPONSE + responseEntity.getStatusCodeValue() + AUTHENTICATION_FAILED,
					ShipoxExceptionType.AUTHENTICATION_FAILED);
		}


		return String.valueOf(objectToMap.get(ID_TOKEN));
	}

	@Override
	public FulfillmentResponse createOrder(final ShipoxOrderRequest orderData, final String baseURL, final String username,
			final String password) throws ShipoxException
	{
		Preconditions.checkArgument(orderData != null, "Order data cannot be null for create shipment");
		Preconditions.checkArgument(validateUserDataInOrder(orderData.getSender_data()), "Order sender data not valid");
		Preconditions.checkArgument(validateUserDataInOrder(orderData.getRecipient_data()), "Order Recipient data not valid");
		Preconditions.checkArgument(validateDimensionDataOnOrder(orderData.getDimensions()), "Order Dimension data not valid");
		Preconditions.checkArgument(validatePackageTypeDataInOrder(orderData.getPackage_type()),
				"Order Package Type data not valid");
		Preconditions.checkArgument(validateChargeItemsDataInOrder(orderData.getCharge_items()),
				"Order Charge Items data not valid");
		Preconditions.checkArgument(orderData.getRecipient_not_available() != null, "Order Recipient_not_available data not valid");
		Preconditions.checkArgument(orderData.getPayment_type() != null, "Order Payment type data not valid");
		final String fullURL = baseURL + CREATE_ORDER_V2_PATH;
		final MultiValueMap<String, String> authorizationHeader = getAuthorizationHeader(username, password, baseURL);

		ResponseEntity<?> responseEntity = null;
		try
		{
			responseEntity = WebServiceApiUtil.httPOST(fullURL, orderData, authorizationHeader, LinkedHashMap.class);
		}
		catch (final Exception e)
		{
			throw new ShipoxException(e.getMessage(), ShipoxExceptionType.BAD_REQUEST);
		}
		// Validate Response
		validateResponse(responseEntity);

		// Fetching and mapping the response into a proper class
		final Map<String, Object> responsemMap = (LinkedHashMap<String, Object>) responseEntity.getBody();
		final Map<String, Map<String, Object>> body = (Map<String, Map<String, Object>>) responseEntity.getBody();
		final Map<String, Object> objectToMap = body != null ? body.get(DATA) : null;

		if (objectToMap == null || responsemMap == null)
		{
			throw new ShipoxException(COULD_NOT_EXTRACT_THE_RESPONSE + responseEntity.getStatusCodeValue() + AUTHENTICATION_FAILED,
					ShipoxExceptionType.AUTHENTICATION_FAILED);
		}

		final FulfillmentResponse response = new FulfillmentResponse();
		response.setStatus(String.valueOf(responsemMap.get(STATUS)));
		response.setId(String.valueOf(objectToMap.get(ID)));
		response.setOrderNumber(String.valueOf(objectToMap.get(ORDER_NUMBER)));
		response.setTrackingId(String.valueOf(objectToMap.get(ORDER_NUMBER)));
		response.setResponseMap(responsemMap);
		return response;
	}

	protected boolean validateUserDataInOrder(final ShipoxUserBean userData)
	{
		if (userData == null)
		{
			return false;
		}
		if (StringUtils.isBlank(userData.getAddress_type()))
		{
			return false;
		}
		if (StringUtils.isBlank(userData.getName()))
		{
			return false;
		}
		if (userData.getCountry() == null || StringUtils.isBlank(userData.getCountry().getId()))
		{
			return false;
		}
		if (userData.getCity() == null || StringUtils.isBlank(userData.getCity().getId())) // NOSONAR
		{
			return false;
		}
		return true;
	}

	protected boolean validateDimensionDataOnOrder(final Dimension dimension)
	{
		if (dimension == null)
		{
			return false;
		}
		if (dimension.getWeight() == null || dimension.getWeight().doubleValue() < 0.0) // NOSONAR
		{
			return false;
		}
		return true;
	}

	protected boolean validatePackageTypeDataInOrder(final PackageType packageType)
	{
		if (packageType == null)
		{
			return false;
		}
		if (packageType.getCourier_type() == null)
		{
			return false;
		}
		if (StringUtils.isBlank(packageType.getCourier_type())) // NOSONAR
		{
			return false;
		}
		return true;
	}

	protected boolean validateChargeItemsDataInOrder(final ChargeItem[] chargeItems)
	{
		if (chargeItems == null || chargeItems.length <= 0)
		{
			return false;
		}
		boolean valid = true;
		for (final ChargeItem item : chargeItems)
		{
			if (item.getCharge() == null || item.getCharge_type() == null || StringUtils.isBlank(item.getPayer()))
			{
				valid = false;
			}
		}
		return valid;
	}

	protected LinkedMultiValueMap buildShipoxSupportedFileHeader()
	{
		final LinkedMultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add("X-Amz-Algorithm", "AWS4-HMAC-SHA256");
		headers.add("X-Amz-Date", "20210317T162247Z");
		headers.add("X-Amz-SignedHeaders", "host");
		headers.add("X-Amz-Expires", "86400");
		headers.add("X-Amz-Credential", "AKIAX23LK5UTSTI6A7LR%2F20210317%2Fus-east-1%2Fs3%2Faws4_request");
		headers.add("X-Amz-Signature", "af3957009e031dcfa6ee50a55aa4cd46c3d74f1755ff3eb8e730125e74df1bda");
		return headers;
	}

}
