/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsfulfillment.shipox.populator;

import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.IntStream;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.aacs.aacsfulfillment.model.ShipoxFulfillmentProviderModel;
import com.aacs.aacsfulfillment.shipox.model.ShipoxOrderContainer;
import com.aacs.aacsfulfillment.shipox.model.request.ChargeItem;
import com.aacs.aacsfulfillment.shipox.model.request.Dimension;
import com.aacs.aacsfulfillment.shipox.model.request.PackageType;
import com.aacs.aacsfulfillment.shipox.model.request.ShipoxCity;
import com.aacs.aacsfulfillment.shipox.model.request.ShipoxCountry;
import com.aacs.aacsfulfillment.shipox.model.request.ShipoxNeighborhood;
import com.aacs.aacsfulfillment.shipox.model.request.ShipoxOrderRequest;
import com.aacs.aacsfulfillment.shipox.model.request.ShipoxUserBean;
import com.aacs.aacsfulfillment.shipox.model.request.enums.ChargeType;
import com.aacs.aacsfulfillment.shipox.model.request.enums.DeliveryNote;
import com.aacs.aacsfulfillment.shipox.model.request.enums.PayerType;
import com.aacs.aacsfulfillment.shipox.model.request.enums.PaymentType;
import com.aacs.aacsfulfillment.shipox.model.request.enums.ShipoxAddressType;
import com.aacs.core.model.AreaModel;
import com.aacs.core.model.CityModel;


/**
 *
 */
public class ShipoxRequestPopulator implements Populator<ShipoxOrderContainer, ShipoxOrderRequest>
{
	private static final Logger LOG = LoggerFactory.getLogger(ShipoxRequestPopulator.class);

	@Resource(name = "categoryService")
	private CategoryService categoryService;

	@Override
	public void populate(final ShipoxOrderContainer source, final ShipoxOrderRequest target) throws ConversionException
	{
		final ConsignmentModel consignment = source.getConsignment();
		final ShipoxFulfillmentProviderModel provider = source.getProvider();

		final AbstractOrderModel order = consignment.getOrder();
		final CustomerModel user = (CustomerModel) order.getUser();

		populateDimensions(consignment, target);
		populateReceiverData(order, user, target);
		populateSenderData(provider, consignment, target);

		final PackageType packageType = new PackageType();
		packageType.setCourier_type("EXPRESS_DELIVERY");
		target.setPackage_type(packageType);

		populateChargeItems(order, provider, target);

		populateBoxContent(order, provider, target);

		target.setRecipient_not_available(DeliveryNote.DO_NOT_DELIVER);
		target.setPayment_type(PaymentType.CASH);
		target.setPayer(PayerType.RECIPIENT);
		target.setReference_id(order.getCode());
	}

	/**
	 *
	 */
	private void populateBoxContent(final AbstractOrderModel order, final ShipoxFulfillmentProviderModel provider,
			final ShipoxOrderRequest target)
	{
		final StringBuilder builder = new StringBuilder();

		try
		{

			order.getEntries().stream().forEach(e -> {
				final List<CategoryModel> categoryPathForProduct = categoryService.getCategoryPathForProduct(e.getProduct());
				if (!CollectionUtils.isEmpty(categoryPathForProduct))
				{
					getMappedBoxCategory(builder, provider, categoryPathForProduct);
				}
			});

		}
		catch (final Exception e)
		{
			LOG.error("ShipoxRequestPopulator -> getCategoryPathForProduct to getMappedBoxCategory: " + e.getMessage());
		}
		target.setNote(builder.toString());
	}

	/**
	 *
	 */
	private void getMappedBoxCategory(final StringBuilder builder, final ShipoxFulfillmentProviderModel provider,
			final List<CategoryModel> pathsForCategory)
	{
		final OptionalInt indexOfOpenCategory = IntStream.range(0, pathsForCategory.size())
				.filter(c -> "1".equals(pathsForCategory.get(c).getCode())).findFirst();
		Optional<String> mapped = Optional.empty();
		if (indexOfOpenCategory.isPresent())
		{
			final CategoryModel mainCategory = pathsForCategory.get(indexOfOpenCategory.getAsInt() + 1);
			mapped = provider.getShipoxContentBoxMappings().keySet().stream().filter(bc -> bc.equals(mainCategory.getCode()))
					.map(key -> provider.getShipoxContentBoxMappings().get(key)).findAny();
		}
		if (mapped.isPresent() && !builder.toString().contains(mapped.get()))
		{
			builder.append(mapped.get()).append(",");
		}

	}

	/**
	 *
	 */
	private void populateChargeItems(final AbstractOrderModel order, final ShipoxFulfillmentProviderModel provider,
			final ShipoxOrderRequest target)
	{
		final ChargeItem orderCharge = new ChargeItem();
		if ("card".equalsIgnoreCase(order.getPaymentMode().getCode()))
		{

			orderCharge.setPaid(true);
		}
		else
		{
			orderCharge.setPaid(false);
		}
		orderCharge.setCharge_type(ChargeType.COD);
		orderCharge.setCharge(order.getTotalPrice());
		orderCharge.setPayer(PayerType.RECIPIENT.name());
		target.setCharge_items(new ChargeItem[]
		{ orderCharge });
	}

	/**
	 *
	 */
	private void populateSenderData(final ShipoxFulfillmentProviderModel provider, final ConsignmentModel consignment,
			final ShipoxOrderRequest target)
	{
		final ShipoxUserBean sender = new ShipoxUserBean();

		final boolean pickUp = consignment.getDeliveryPointOfService() == null;

		final WarehouseModel warehouse = consignment.getWarehouse();

		final CountryModel deliveryCountry = pickUp ? warehouse.getDeliveryCountry()
				: consignment.getDeliveryPointOfService().getAddress().getCountry();

		final CityModel deliveryCity = pickUp ? warehouse.getDeliveryCity()
				: consignment.getDeliveryPointOfService().getAddress().getCity();

		final AreaModel deliveryArea = pickUp ? warehouse.getDeliveryArea()
				: consignment.getDeliveryPointOfService().getAddress().getArea();

		sender.setAddress_type(ShipoxAddressType.RESIDENTIAL.getCode());
		sender.setEmail(provider.getSenderMail());
		sender.setName(pickUp ? provider.getSenderName() : consignment.getDeliveryPointOfService().getName());
		sender.setPhone(provider.getSenderPhone());

		final ShipoxCountry country = new ShipoxCountry();
		country.setId(deliveryCountry.getShipoxCountryCode());
		sender.setCountry(country);

		final ShipoxCity city = new ShipoxCity();
		city.setId(deliveryCity.getShipoxCity());
		sender.setCity(city);

		final ShipoxNeighborhood neighborhoodData = new ShipoxNeighborhood();
		neighborhoodData.setId(deliveryArea.getShipoxNeighborhoodCode());
		sender.setNeighborhood(neighborhoodData);
		target.setSender_data(sender);
	}

	/**
	 *
	 */
	private void populateReceiverData(final AbstractOrderModel order, final CustomerModel user, final ShipoxOrderRequest target)
	{
		final ShipoxUserBean receiver = new ShipoxUserBean();
		receiver.setEmail(user.getContactEmail());
		receiver.setName(user.getDisplayName());
		receiver.setPhone(user.getMobileNumber());

		final AddressModel deliveryAddress = order.getDeliveryAddress();
		fetchAddressDataIntoReceiver(deliveryAddress, receiver);
		target.setRecipient_data(receiver);
	}

	/**
	 *
	 */
	private void fetchAddressDataIntoReceiver(final AddressModel deliveryAddress, final ShipoxUserBean receiver)
	{
		final ShipoxCity city = new ShipoxCity();
		city.setId(deliveryAddress.getCity().getShipoxCity());
		receiver.setCity(city);
		// TODO
		final ShipoxCountry country = new ShipoxCountry();
		country.setId(deliveryAddress.getCountry().getShipoxCountryCode());
		receiver.setCountry(country);

		final ShipoxNeighborhood neighborhoodData = new ShipoxNeighborhood();
		neighborhoodData.setId(deliveryAddress.getArea().getShipoxNeighborhoodCode());
		receiver.setNeighborhood(neighborhoodData);

		receiver.setAddress_type(ShipoxAddressType.BUSINESS.getCode());
		receiver.setBuilding(deliveryAddress.getBuildingName());
		receiver.setApartment(deliveryAddress.getApartmentNumber());
		receiver.setLandmark(deliveryAddress.getNearestLandmark());
	}

	/**
	 *
	 */
	private void populateDimensions(final ConsignmentModel consignment, final ShipoxOrderRequest target)
	{
		final double totalWeight = consignment.getConsignmentEntries().stream().mapToDouble(entry -> entry.getWeight()).sum();
		final Dimension dimension = new Dimension();
		dimension.setWeight(totalWeight);
		target.setDimensions(dimension);
	}

}
