/**
 *
 */
package com.aacs.aacsorderconfirmation.dao;

import java.util.List;

import com.aacs.aacsorderconfirmation.model.OrderConfirmationEmailModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface OrderEmailDao
{
	/**
	 * @param storeUid
	 * @return
	 */
	List<OrderConfirmationEmailModel> findByStoreUid(String storeUid);
}
