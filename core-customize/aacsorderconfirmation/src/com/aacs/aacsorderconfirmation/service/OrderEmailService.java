/**
 *
 */
package com.aacs.aacsorderconfirmation.service;

import java.util.List;

import com.aacs.aacsorderconfirmation.model.OrderConfirmationEmailModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface OrderEmailService
{
	List<OrderConfirmationEmailModel> findByStoreUid(String storeUid);
}
