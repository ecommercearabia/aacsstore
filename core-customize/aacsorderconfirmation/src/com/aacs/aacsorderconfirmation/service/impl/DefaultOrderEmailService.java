/**
 *
 */
package com.aacs.aacsorderconfirmation.service.impl;

import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import javax.annotation.Resource;

import com.aacs.aacsorderconfirmation.dao.OrderEmailDao;
import com.aacs.aacsorderconfirmation.model.OrderConfirmationEmailModel;
import com.aacs.aacsorderconfirmation.service.OrderEmailService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultOrderEmailService implements OrderEmailService
{
	@Resource(name = "orderEmailDao")
	private OrderEmailDao orderEmailDao;

	public OrderEmailDao getOrderEmailDao()
	{
		return orderEmailDao;
	}

	@Override
	public List<OrderConfirmationEmailModel> findByStoreUid(final String storeUid)
	{
		ServicesUtil.validateParameterNotNull(storeUid, "storeUid must not be null");

		return getOrderEmailDao().findByStoreUid(storeUid);
	}
}
