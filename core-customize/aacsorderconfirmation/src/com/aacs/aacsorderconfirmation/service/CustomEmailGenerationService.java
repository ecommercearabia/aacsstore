/**
 *
 */
package com.aacs.aacsorderconfirmation.service;

import de.hybris.platform.acceleratorservices.email.EmailGenerationService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface CustomEmailGenerationService extends EmailGenerationService
{

}
