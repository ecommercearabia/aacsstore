/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.wishlist.constants;

/**
 * Global class for all Aacswishlistwishlist constants. You can add global constants for your extension into this class.
 */
public final class AacswishlistwishlistConstants extends GeneratedAacswishlistwishlistConstants
{
	public static final String EXTENSIONNAME = "aacswishlist";

	private AacswishlistwishlistConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "aacswishlistPlatformLogo";
}
