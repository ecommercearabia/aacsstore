/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacseventtrackingwscustomaddon.constants;

/**
 * Global class for all Aacseventtrackingwscustomaddon constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class AacseventtrackingwscustomaddonConstants extends GeneratedAacseventtrackingwscustomaddonConstants  //NOSONAR
{
	public static final String EXTENSIONNAME = "aacseventtrackingwscustomaddon";

	private AacseventtrackingwscustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
