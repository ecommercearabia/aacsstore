/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacseventtrackingwscustomaddon.controllers;

/**
 */
public interface AacseventtrackingwscustomaddonControllerConstants
{
	// implement here controller constants used by this extension
}
