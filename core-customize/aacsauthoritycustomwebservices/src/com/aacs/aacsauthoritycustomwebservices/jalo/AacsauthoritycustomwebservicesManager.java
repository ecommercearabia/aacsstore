/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsauthoritycustomwebservices.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;
import com.aacs.aacsauthoritycustomwebservices.constants.AacsauthoritycustomwebservicesConstants;

public class AacsauthoritycustomwebservicesManager extends GeneratedAacsauthoritycustomwebservicesManager
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger( AacsauthoritycustomwebservicesManager.class.getName() );
	
	public static final AacsauthoritycustomwebservicesManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AacsauthoritycustomwebservicesManager) em.getExtension(AacsauthoritycustomwebservicesConstants.EXTENSIONNAME);
	}
	
}
