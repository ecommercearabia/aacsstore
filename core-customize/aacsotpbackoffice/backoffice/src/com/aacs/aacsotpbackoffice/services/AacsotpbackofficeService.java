/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.aacs.aacsotpbackoffice.services;

/**
 * Hello World AacsotpbackofficeService
 */
public class AacsotpbackofficeService
{
	public String getHello()
	{
		return "Hello";
	}
}
