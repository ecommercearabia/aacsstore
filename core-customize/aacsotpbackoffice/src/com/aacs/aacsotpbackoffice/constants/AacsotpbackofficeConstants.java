/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.aacs.aacsotpbackoffice.constants;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
public final class AacsotpbackofficeConstants extends GeneratedAacsotpbackofficeConstants
{
	public static final String EXTENSIONNAME = "aacsotpbackoffice";

	private AacsotpbackofficeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
