/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsnotificationcustomaddon.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.aacs.aacsnotificationcustomaddon.constants.AacsnotificationcustomaddonConstants;
import org.apache.log4j.Logger;

public class AacsnotificationcustomaddonManager extends GeneratedAacsnotificationcustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( AacsnotificationcustomaddonManager.class.getName() );
	
	public static final AacsnotificationcustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AacsnotificationcustomaddonManager) em.getExtension(AacsnotificationcustomaddonConstants.EXTENSIONNAME);
	}
	
}
