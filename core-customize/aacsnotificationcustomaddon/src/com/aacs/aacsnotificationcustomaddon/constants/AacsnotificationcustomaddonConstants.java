/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsnotificationcustomaddon.constants;

/**
 * Global class for all Aacsnotificationcustomaddon constants. You can add global constants for your extension into this class.
 */
public final class AacsnotificationcustomaddonConstants extends GeneratedAacsnotificationcustomaddonConstants
{
	public static final String EXTENSIONNAME = "aacsnotificationcustomaddon";

	private AacsnotificationcustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
