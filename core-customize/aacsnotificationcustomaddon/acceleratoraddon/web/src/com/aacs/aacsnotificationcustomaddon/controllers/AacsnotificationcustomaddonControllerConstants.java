/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsnotificationcustomaddon.controllers;

/**
 */
public interface AacsnotificationcustomaddonControllerConstants
{
	// implement here controller constants used by this extension
}
