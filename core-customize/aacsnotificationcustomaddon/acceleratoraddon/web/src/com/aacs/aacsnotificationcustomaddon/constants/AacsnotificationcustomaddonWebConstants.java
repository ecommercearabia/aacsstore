/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsnotificationcustomaddon.constants;

/**
 * Global class for all Aacsnotificationcustomaddon web constants. You can add global constants for your extension into this class.
 */
public final class AacsnotificationcustomaddonWebConstants // NOSONAR
{
	private AacsnotificationcustomaddonWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
