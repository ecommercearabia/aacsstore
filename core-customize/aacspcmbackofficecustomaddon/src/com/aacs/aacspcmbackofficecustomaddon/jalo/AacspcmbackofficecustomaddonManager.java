package com.aacs.aacspcmbackofficecustomaddon.jalo;

import com.aacs.aacspcmbackofficecustomaddon.constants.AacspcmbackofficecustomaddonConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class AacspcmbackofficecustomaddonManager extends GeneratedAacspcmbackofficecustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( AacspcmbackofficecustomaddonManager.class.getName() );
	
	public static final AacspcmbackofficecustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AacspcmbackofficecustomaddonManager) em.getExtension(AacspcmbackofficecustomaddonConstants.EXTENSIONNAME);
	}
	
}
