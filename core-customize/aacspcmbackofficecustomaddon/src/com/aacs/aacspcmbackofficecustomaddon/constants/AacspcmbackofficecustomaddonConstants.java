/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.aacs.aacspcmbackofficecustomaddon.constants;

/**
 * Global class for all Aacspcmbackofficecustomaddon constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class AacspcmbackofficecustomaddonConstants extends GeneratedAacspcmbackofficecustomaddonConstants
{
	public static final String EXTENSIONNAME = "aacspcmbackofficecustomaddon";

	private AacspcmbackofficecustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
