/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsassistedservicepromotioncustomaddon.jalo;

import com.aacs.aacsassistedservicepromotioncustomaddon.constants.AacsassistedservicepromotioncustomaddonConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class AacsassistedservicepromotioncustomaddonManager extends GeneratedAacsassistedservicepromotioncustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( AacsassistedservicepromotioncustomaddonManager.class.getName() );
	
	public static final AacsassistedservicepromotioncustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AacsassistedservicepromotioncustomaddonManager) em.getExtension(AacsassistedservicepromotioncustomaddonConstants.EXTENSIONNAME);
	}
	
}
