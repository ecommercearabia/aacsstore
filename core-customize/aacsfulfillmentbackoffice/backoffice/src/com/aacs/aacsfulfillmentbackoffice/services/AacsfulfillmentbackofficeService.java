/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.aacs.aacsfulfillmentbackoffice.services;

/**
 * Hello World AacsfulfillmentbackofficeService
 */
public class AacsfulfillmentbackofficeService
{
	public String getHello()
	{
		return "Hello";
	}
}
