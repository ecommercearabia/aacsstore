/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.aacs.aacsstorecreditbackoffice.constants;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
public final class AacsstorecreditbackofficeConstants extends GeneratedAacsstorecreditbackofficeConstants
{
	public static final String EXTENSIONNAME = "aacsstorecreditbackoffice";

	private AacsstorecreditbackofficeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
