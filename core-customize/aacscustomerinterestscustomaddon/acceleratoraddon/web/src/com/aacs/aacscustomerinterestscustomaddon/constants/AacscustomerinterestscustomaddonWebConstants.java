/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscustomerinterestscustomaddon.constants;

/**
 * Global class for all Aacscustomerinterestscustomaddon web constants. You can add global constants for your extension into this class.
 */
public final class AacscustomerinterestscustomaddonWebConstants // NOSONAR
{
	private AacscustomerinterestscustomaddonWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
