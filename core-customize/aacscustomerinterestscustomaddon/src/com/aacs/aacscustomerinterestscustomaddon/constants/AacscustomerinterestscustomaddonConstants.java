/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscustomerinterestscustomaddon.constants;

/**
 * Global class for all Aacscustomerinterestscustomaddon constants. You can add global constants for your extension into this class.
 */
public final class AacscustomerinterestscustomaddonConstants extends GeneratedAacscustomerinterestscustomaddonConstants
{
	public static final String EXTENSIONNAME = "aacscustomerinterestscustomaddon";

	private AacscustomerinterestscustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
