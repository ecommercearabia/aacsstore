/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscustomerinterestscustomaddon.jalo;

import com.aacs.aacscustomerinterestscustomaddon.constants.AacscustomerinterestscustomaddonConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class AacscustomerinterestscustomaddonManager extends GeneratedAacscustomerinterestscustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( AacscustomerinterestscustomaddonManager.class.getName() );
	
	public static final AacscustomerinterestscustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AacscustomerinterestscustomaddonManager) em.getExtension(AacscustomerinterestscustomaddonConstants.EXTENSIONNAME);
	}
	
}
