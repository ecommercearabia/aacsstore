/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class PaymentTransactionOrderEntryBean
{

	@Expose
	@SerializedName("productCode")
	String productCode;

	@Expose
	@SerializedName("orderNumber")
	String orderNumber;

	@Expose
	@SerializedName("quantity")
	String quantity;

	@Expose
	@SerializedName("unit")
	String unit;

	@Expose
	@SerializedName("basePrice")
	String basePrice;

	@Expose
	@SerializedName("totalPrice")
	String totalPrice;

	@Expose
	@SerializedName("discountValue")
	String discountValue;

	@Expose
	@SerializedName("taxValue")
	String taxValue;

	/**
	 * @return the productCode
	 */
	public String getProductCode()
	{
		return productCode;
	}

	/**
	 * @param productCode
	 *           the productCode to set
	 */
	public void setProductCode(final String productCode)
	{
		this.productCode = productCode;
	}

	/**
	 * @return the orderNumber
	 */
	public String getOrderNumber()
	{
		return orderNumber;
	}

	/**
	 * @param orderNumber
	 *           the orderNumber to set
	 */
	public void setOrderNumber(final String orderNumber)
	{
		this.orderNumber = orderNumber;
	}

	/**
	 * @return the quantity
	 */
	public String getQuantity()
	{
		return quantity;
	}

	/**
	 * @param quantity
	 *           the quantity to set
	 */
	public void setQuantity(final String quantity)
	{
		this.quantity = quantity;
	}

	/**
	 * @return the unit
	 */
	public String getUnit()
	{
		return unit;
	}

	/**
	 * @param unit
	 *           the unit to set
	 */
	public void setUnit(final String unit)
	{
		this.unit = unit;
	}

	/**
	 * @return the basePrice
	 */
	public String getBasePrice()
	{
		return basePrice;
	}

	/**
	 * @param basePrice
	 *           the basePrice to set
	 */
	public void setBasePrice(final String basePrice)
	{
		this.basePrice = basePrice;
	}

	/**
	 * @return the totalPrice
	 */
	public String getTotalPrice()
	{
		return totalPrice;
	}

	/**
	 * @param totalPrice
	 *           the totalPrice to set
	 */
	public void setTotalPrice(final String totalPrice)
	{
		this.totalPrice = totalPrice;
	}

	/**
	 * @return the discountValue
	 */
	public String getDiscountValue()
	{
		return discountValue;
	}

	/**
	 * @param discountValue
	 *           the discountValue to set
	 */
	public void setDiscountValue(final String discountValue)
	{
		this.discountValue = discountValue;
	}

	/**
	 * @return the taxValue
	 */
	public String getTaxValue()
	{
		return taxValue;
	}

	/**
	 * @param taxValue
	 *           the taxValue to set
	 */
	public void setTaxValue(final String taxValue)
	{
		this.taxValue = taxValue;
	}

}
