/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.beans;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class PaymentTransactionInfoBean
{
	@Expose
	@SerializedName("code")
	private String code;

	@Expose
	@SerializedName("orderCode")
	private String orderCode;

	@Expose
	@SerializedName("deliveryCost")
	private String deliveryCost;

	@Expose
	@SerializedName("totalPrice")
	private String totalPrice;

	@Expose
	@SerializedName("totalTax")
	private String totalTax;

	@Expose
	@SerializedName("mpgsSuccessIndicator")
	private String mpgsSuccessIndicator;

	@Expose
	@SerializedName("redeemedLoyaltyPoints")
	private String redeemedLoyaltyPoints;

	@Expose
	@SerializedName("giiftValidateId")
	private String giiftValidateId;

	@Expose
	@SerializedName("giiftTransactionId")
	private String giiftTransactionId;

	@Expose
	@SerializedName("totalPriceAfterRedeem")
	private String totalPriceAfterRedeem;

	@Expose
	@SerializedName("loyaltyPaymentMode")
	private String loyaltyPaymentMode;

	@Expose
	@SerializedName("loyaltyAmountSelected")
	private String loyaltyAmountSelected;

	@Expose
	@SerializedName("expectedLoyaltyRedeemAmount")
	private String expectedLoyaltyRedeemAmount;

	@Expose
	@SerializedName("expectedAddedPoint")
	private String expectedAddedPoint;

	@Expose
	@SerializedName("usableLoyaltyRedeemPoints")
	private String usableLoyaltyRedeemPoints;

	@Expose
	@SerializedName("usableLoyaltyRedeemAmount")
	private String usableLoyaltyRedeemAmount;

	@Expose
	@SerializedName("loyaltyPaymentStatus")
	private String loyaltyPaymentStatus;

	@Expose
	@SerializedName("loyaltyPaymentType")
	private String loyaltyPaymentType;

	@Expose
	@SerializedName("loyaltyFailureReason")
	private String loyaltyFailureReason;

	@Expose
	@SerializedName("loyaltyExchangeRate")
	private String loyaltyExchangeRate;

	@Expose
	@SerializedName("loyaltyAmount")
	private String loyaltyAmount;

	@Expose
	@SerializedName("shouldCalculateLoyalty")
	private String shouldCalculateLoyalty;

	@Expose
	@SerializedName("globalDiscountValues")
	private String globalDiscountValues;

	@Expose
	@SerializedName("sessionId")
	private String sessionId;

	@Expose
	@SerializedName("operationType")
	private String operationType;

	@Expose
	@SerializedName("storeCreditAmount")
	private String storeCreditAmount;

	@Expose
	@SerializedName("storeCreditAmountSelected")
	private String storeCreditAmountSelected;

	@Expose
	@SerializedName("storeCreditMode")
	private String storeCreditMode;

	@Expose
	@SerializedName("totalPriceWithTax")
	private String totalPriceWithTax;

	@Expose
	@SerializedName("entries")
	private List<PaymentTransactionOrderEntryBean> entries;

	@Expose
	@SerializedName("merchantID")
	private String merchantID;

	@Expose
	@SerializedName("apiKey")
	private String apiKey;

	/**
	 * @return the storeCreditAmount
	 */
	public String getStoreCreditAmount()
	{
		return storeCreditAmount;
	}

	/**
	 * @return the totalPriceWithTax
	 */
	public String getTotalPriceWithTax()
	{
		return totalPriceWithTax;
	}

	/**
	 * @param totalPriceWithTax
	 *           the totalPriceWithTax to set
	 */
	public void setTotalPriceWithTax(final String totalPriceWithTax)
	{
		this.totalPriceWithTax = totalPriceWithTax;
	}

	/**
	 * @param storeCreditAmount
	 *           the storeCreditAmount to set
	 */
	public void setStoreCreditAmount(final String storeCreditAmount)
	{
		this.storeCreditAmount = storeCreditAmount;
	}

	/**
	 * @return the storeCreditAmountSelected
	 */
	public String getStoreCreditAmountSelected()
	{
		return storeCreditAmountSelected;
	}

	/**
	 * @param storeCreditAmountSelected
	 *           the storeCreditAmountSelected to set
	 */
	public void setStoreCreditAmountSelected(final String storeCreditAmountSelected)
	{
		this.storeCreditAmountSelected = storeCreditAmountSelected;
	}

	/**
	 * @return the storeCreditMode
	 */
	public String getStoreCreditMode()
	{
		return storeCreditMode;
	}

	/**
	 * @param storeCreditMode
	 *           the storeCreditMode to set
	 */
	public void setStoreCreditMode(final String storeCreditMode)
	{
		this.storeCreditMode = storeCreditMode;
	}

	/**
	 * @return the code
	 */
	public String getCode()
	{
		return code;
	}

	/**
	 * @param code
	 *           the code to set
	 */
	public void setCode(final String code)
	{
		this.code = code;
	}

	/**
	 * @return the orderCode
	 */
	public String getOrderCode()
	{
		return orderCode;
	}

	/**
	 * @param orderCode
	 *           the orderCode to set
	 */
	public void setOrderCode(final String orderCode)
	{
		this.orderCode = orderCode;
	}

	/**
	 * @return the deliveryCost
	 */
	public String getDeliveryCost()
	{
		return deliveryCost;
	}

	/**
	 * @param deliveryCost
	 *           the deliveryCost to set
	 */
	public void setDeliveryCost(final String deliveryCost)
	{
		this.deliveryCost = deliveryCost;
	}

	/**
	 * @return the totalPrice
	 */
	public String getTotalPrice()
	{
		return totalPrice;
	}

	/**
	 * @param totalPrice
	 *           the totalPrice to set
	 */
	public void setTotalPrice(final String totalPrice)
	{
		this.totalPrice = totalPrice;
	}

	/**
	 * @return the totalTax
	 */
	public String getTotalTax()
	{
		return totalTax;
	}

	/**
	 * @param totalTax
	 *           the totalTax to set
	 */
	public void setTotalTax(final String totalTax)
	{
		this.totalTax = totalTax;
	}

	/**
	 * @return the mpgsSuccessIndicator
	 */
	public String getMpgsSuccessIndicator()
	{
		return mpgsSuccessIndicator;
	}

	/**
	 * @param mpgsSuccessIndicator
	 *           the mpgsSuccessIndicator to set
	 */
	public void setMpgsSuccessIndicator(final String mpgsSuccessIndicator)
	{
		this.mpgsSuccessIndicator = mpgsSuccessIndicator;
	}

	/**
	 * @return the redeemedLoyaltyPoints
	 */
	public String getRedeemedLoyaltyPoints()
	{
		return redeemedLoyaltyPoints;
	}

	/**
	 * @param redeemedLoyaltyPoints
	 *           the redeemedLoyaltyPoints to set
	 */
	public void setRedeemedLoyaltyPoints(final String redeemedLoyaltyPoints)
	{
		this.redeemedLoyaltyPoints = redeemedLoyaltyPoints;
	}

	/**
	 * @return the giiftValidateId
	 */
	public String getGiiftValidateId()
	{
		return giiftValidateId;
	}

	/**
	 * @param giiftValidateId
	 *           the giiftValidateId to set
	 */
	public void setGiiftValidateId(final String giiftValidateId)
	{
		this.giiftValidateId = giiftValidateId;
	}

	/**
	 * @return the giiftTransactionId
	 */
	public String getGiiftTransactionId()
	{
		return giiftTransactionId;
	}

	/**
	 * @param giiftTransactionId
	 *           the giiftTransactionId to set
	 */
	public void setGiiftTransactionId(final String giiftTransactionId)
	{
		this.giiftTransactionId = giiftTransactionId;
	}

	/**
	 * @return the totalPriceAfterRedeem
	 */
	public String getTotalPriceAfterRedeem()
	{
		return totalPriceAfterRedeem;
	}

	/**
	 * @param totalPriceAfterRedeem
	 *           the totalPriceAfterRedeem to set
	 */
	public void setTotalPriceAfterRedeem(final String totalPriceAfterRedeem)
	{
		this.totalPriceAfterRedeem = totalPriceAfterRedeem;
	}

	/**
	 * @return the loyaltyPaymentMode
	 */
	public String getLoyaltyPaymentMode()
	{
		return loyaltyPaymentMode;
	}

	/**
	 * @param loyaltyPaymentMode
	 *           the loyaltyPaymentMode to set
	 */
	public void setLoyaltyPaymentMode(final String loyaltyPaymentMode)
	{
		this.loyaltyPaymentMode = loyaltyPaymentMode;
	}

	/**
	 * @return the loyaltyAmountSelected
	 */
	public String getLoyaltyAmountSelected()
	{
		return loyaltyAmountSelected;
	}

	/**
	 * @param loyaltyAmountSelected
	 *           the loyaltyAmountSelected to set
	 */
	public void setLoyaltyAmountSelected(final String loyaltyAmountSelected)
	{
		this.loyaltyAmountSelected = loyaltyAmountSelected;
	}

	/**
	 * @return the expectedLoyaltyRedeemAmount
	 */
	public String getExpectedLoyaltyRedeemAmount()
	{
		return expectedLoyaltyRedeemAmount;
	}

	/**
	 * @param expectedLoyaltyRedeemAmount
	 *           the expectedLoyaltyRedeemAmount to set
	 */
	public void setExpectedLoyaltyRedeemAmount(final String expectedLoyaltyRedeemAmount)
	{
		this.expectedLoyaltyRedeemAmount = expectedLoyaltyRedeemAmount;
	}

	/**
	 * @return the expectedAddedPoint
	 */
	public String getExpectedAddedPoint()
	{
		return expectedAddedPoint;
	}

	/**
	 * @param expectedAddedPoint
	 *           the expectedAddedPoint to set
	 */
	public void setExpectedAddedPoint(final String expectedAddedPoint)
	{
		this.expectedAddedPoint = expectedAddedPoint;
	}

	/**
	 * @return the usableLoyaltyRedeemPoints
	 */
	public String getUsableLoyaltyRedeemPoints()
	{
		return usableLoyaltyRedeemPoints;
	}

	/**
	 * @param usableLoyaltyRedeemPoints
	 *           the usableLoyaltyRedeemPoints to set
	 */
	public void setUsableLoyaltyRedeemPoints(final String usableLoyaltyRedeemPoints)
	{
		this.usableLoyaltyRedeemPoints = usableLoyaltyRedeemPoints;
	}

	/**
	 * @return the usableLoyaltyRedeemAmount
	 */
	public String getUsableLoyaltyRedeemAmount()
	{
		return usableLoyaltyRedeemAmount;
	}

	/**
	 * @param usableLoyaltyRedeemAmount
	 *           the usableLoyaltyRedeemAmount to set
	 */
	public void setUsableLoyaltyRedeemAmount(final String usableLoyaltyRedeemAmount)
	{
		this.usableLoyaltyRedeemAmount = usableLoyaltyRedeemAmount;
	}

	/**
	 * @return the loyaltyPaymentStatus
	 */
	public String getLoyaltyPaymentStatus()
	{
		return loyaltyPaymentStatus;
	}

	/**
	 * @param loyaltyPaymentStatus
	 *           the loyaltyPaymentStatus to set
	 */
	public void setLoyaltyPaymentStatus(final String loyaltyPaymentStatus)
	{
		this.loyaltyPaymentStatus = loyaltyPaymentStatus;
	}

	/**
	 * @return the loyaltyPaymentType
	 */
	public String getLoyaltyPaymentType()
	{
		return loyaltyPaymentType;
	}

	/**
	 * @param loyaltyPaymentType
	 *           the loyaltyPaymentType to set
	 */
	public void setLoyaltyPaymentType(final String loyaltyPaymentType)
	{
		this.loyaltyPaymentType = loyaltyPaymentType;
	}

	/**
	 * @return the loyaltyFailureReason
	 */
	public String getLoyaltyFailureReason()
	{
		return loyaltyFailureReason;
	}

	/**
	 * @param loyaltyFailureReason
	 *           the loyaltyFailureReason to set
	 */
	public void setLoyaltyFailureReason(final String loyaltyFailureReason)
	{
		this.loyaltyFailureReason = loyaltyFailureReason;
	}

	/**
	 * @return the loyaltyExchangeRate
	 */
	public String getLoyaltyExchangeRate()
	{
		return loyaltyExchangeRate;
	}

	/**
	 * @param loyaltyExchangeRate
	 *           the loyaltyExchangeRate to set
	 */
	public void setLoyaltyExchangeRate(final String loyaltyExchangeRate)
	{
		this.loyaltyExchangeRate = loyaltyExchangeRate;
	}

	/**
	 * @return the loyaltyAmount
	 */
	public String getLoyaltyAmount()
	{
		return loyaltyAmount;
	}

	/**
	 * @param loyaltyAmount
	 *           the loyaltyAmount to set
	 */
	public void setLoyaltyAmount(final String loyaltyAmount)
	{
		this.loyaltyAmount = loyaltyAmount;
	}

	/**
	 * @return the shouldCalculateLoyalty
	 */
	public String getShouldCalculateLoyalty()
	{
		return shouldCalculateLoyalty;
	}

	/**
	 * @param shouldCalculateLoyalty
	 *           the shouldCalculateLoyalty to set
	 */
	public void setShouldCalculateLoyalty(final String shouldCalculateLoyalty)
	{
		this.shouldCalculateLoyalty = shouldCalculateLoyalty;
	}

	/**
	 * @return the globalDiscountValues
	 */
	public String getGlobalDiscountValues()
	{
		return globalDiscountValues;
	}

	/**
	 * @param globalDiscountValues
	 *           the globalDiscountValues to set
	 */
	public void setGlobalDiscountValues(final String globalDiscountValues)
	{
		this.globalDiscountValues = globalDiscountValues;
	}

	/**
	 * @return the sessionId
	 */
	public String getSessionId()
	{
		return sessionId;
	}

	/**
	 * @param sessionId
	 *           the sessionId to set
	 */
	public void setSessionId(final String sessionId)
	{
		this.sessionId = sessionId;
	}

	/**
	 * @return the operationType
	 */
	public String getOperationType()
	{
		return operationType;
	}

	/**
	 * @param operationType
	 *           the operationType to set
	 */
	public void setOperationType(final String operationType)
	{
		this.operationType = operationType;
	}

	/**
	 * @return the entries
	 */
	public List<PaymentTransactionOrderEntryBean> getEntries()
	{
		return entries;
	}

	/**
	 * @param entries
	 *           the entries to set
	 */
	public void setEntries(final List<PaymentTransactionOrderEntryBean> entries)
	{
		this.entries = entries;
	}

	/**
	 * @return the merchantID
	 */
	public String getMerchantID()
	{
		return merchantID;
	}

	/**
	 * @param merchantID
	 *           the merchantID to set
	 */
	public void setMerchantID(final String merchantID)
	{
		this.merchantID = merchantID;
	}

	/**
	 * @return the apiKey
	 */
	public String getApiKey()
	{
		return apiKey;
	}

	/**
	 * @param apiKey
	 *           the apiKey to set
	 */
	public void setApiKey(final String apiKey)
	{
		this.apiKey = apiKey;
	}


}
