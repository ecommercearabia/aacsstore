/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.dao.records;

import java.util.List;

import com.aacs.aacspayment.model.PaymentTransactionRecordsModel;


/**
 *
 */
public interface PaymentTransactionRecordsDao
{

	void createPaymentRecord(PaymentTransactionRecordsModel model);

	List<PaymentTransactionRecordsModel> getAllPaymentRecords();

	List<PaymentTransactionRecordsModel> getAllPaymentRecordsForQuery(PaymentTransactionRecordsModel model);

}
