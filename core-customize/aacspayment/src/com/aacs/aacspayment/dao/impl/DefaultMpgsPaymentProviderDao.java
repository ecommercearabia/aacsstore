/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.dao.impl;

import com.aacs.aacspayment.dao.PaymentProviderDao;
import com.aacs.aacspayment.model.MpgsPaymentProviderModel;


/**
 * @author mnasro
 *
 *         The Class DefaultHyperpayPaymentProviderDao.
 */
public class DefaultMpgsPaymentProviderDao extends DefaultPaymentProviderDao implements PaymentProviderDao
{

	/**
	 * Instantiates a new default CC avenue payment provider dao.
	 */
	public DefaultMpgsPaymentProviderDao()
	{
		super(MpgsPaymentProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return MpgsPaymentProviderModel._TYPECODE;
	}

}
