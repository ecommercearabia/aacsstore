/**
 *
 */
package com.aacs.aacspayment.hyperpay.dao;

import com.aacs.aacspayment.model.HyperpayCheckoutIdModel;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public interface HyperpayCheckoutIdDao
{
	HyperpayCheckoutIdModel getCheckoutIdById(final String id);
}
