/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.hyperpay.exception;

import com.aacs.aacspayment.exception.PaymentException;
import com.aacs.aacspayment.exception.type.PaymentExceptionType;

/**
 *
 * @author monzer
 *
 */
public class HyperpayPaymentException extends PaymentException
{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private final String message;


	public HyperpayPaymentException(final PaymentExceptionType type, final String message)
	{
		super(message, type);
		this.message = message;
	}


}
