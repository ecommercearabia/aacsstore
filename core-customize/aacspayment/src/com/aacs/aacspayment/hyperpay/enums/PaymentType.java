/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.hyperpay.enums;

/**
 *
 * @author monzer
 *
 */
public enum PaymentType {

	PA("PA"),
	DB("DB"),
	CD("CD"),
	CP("PA.CP"),
	RV("RV"),
	RF("RF");

	private String paymentCode;

	private PaymentType(final String paymentCode) {
		this.paymentCode = paymentCode;
	}

	public String getPaymentCode() {
		return paymentCode;
	}

}
