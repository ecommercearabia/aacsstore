/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.hyperpay.enums;

/**
 *
 * @author monzer
 *
 */
public enum CustomerSex {

	M, F;

}
