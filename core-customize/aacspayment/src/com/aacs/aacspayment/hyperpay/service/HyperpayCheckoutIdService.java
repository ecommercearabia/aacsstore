/**
 *
 */
package com.aacs.aacspayment.hyperpay.service;

import com.aacs.aacspayment.model.HyperpayCheckoutIdModel;


/**
 * @author husam.dababneh@eraba.com
 *
 */
public interface HyperpayCheckoutIdService
{
	HyperpayCheckoutIdModel getCheckoutIdById(final String id);


	HyperpayCheckoutIdModel createHyperpayCheckoutId(final String id, final String orderCode, final String cartCode);
}
