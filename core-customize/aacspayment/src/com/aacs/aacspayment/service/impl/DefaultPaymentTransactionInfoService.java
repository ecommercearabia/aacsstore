/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.service.impl;

import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.aacs.aacspayment.beans.PaymentTransactionInfoBean;
import com.aacs.aacspayment.beans.PaymentTransactionOrderEntryBean;
import com.aacs.aacspayment.model.PaymentTransactionInfoModel;
import com.aacs.aacspayment.model.PaymentTransactionInfoOrderEntryModel;
import com.aacs.aacspayment.mpgs.service.impl.DefaultMpgsServiceImpl;
import com.aacs.aacspayment.service.PaymentTransactionInfoService;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 *
 */
public class DefaultPaymentTransactionInfoService implements PaymentTransactionInfoService
{

	private static final String SESSION_ID_CAN_NOT_BE_NULL = "sessionID can not be null";
	private static final String SESSION_INFO_CAN_NOT_BE_NULL = "sessionInfo can not be null";

	private static final String PAYMENT_TRANSACTION_INFO_CAN_NOT_BE_NULL = "paymentTransactionInfoModel can not be null";



	private static final String MERCHANT_ID_CAN_NOT_BE_NULL = "merchantID can not be null";
	private static final String API_KEY_CAN_NOT_BE_NULL = "apiKey can not be null";
	private static final String ORDER_CAN_NOT_BE_NULL = "order can not be null";
	private static final GsonBuilder _builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
	private static final Gson GSON = _builder.setPrettyPrinting().create();
	private static final Logger LOG = LoggerFactory.getLogger(DefaultMpgsServiceImpl.class);

	static
	{
		_builder.serializeNulls();
	}

	/** The common I 18 N service. */
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "modelService")
	private ModelService modelService;



	@Override
	public PaymentTransactionInfoModel updatePaymentTransactionInfo(final PaymentTransactionInfoModel paymentTransactionInfoModel,
			final String sessionInfo, final double sessionInfoAmount, final String sessionInfoError)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(sessionInfo), SESSION_INFO_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(paymentTransactionInfoModel != null, PAYMENT_TRANSACTION_INFO_CAN_NOT_BE_NULL);


		paymentTransactionInfoModel.setSessionIdInfo(sessionInfo);
		paymentTransactionInfoModel.setSessionInfoAmount(String.valueOf(sessionInfoAmount));
		paymentTransactionInfoModel.setSessionInfoError(sessionInfoError);

		getModelService().save(paymentTransactionInfoModel);
		getModelService().refresh(paymentTransactionInfoModel);

		return paymentTransactionInfoModel;
	}

	@Override
	public PaymentTransactionInfoModel savePaymentTransactionInfo(final String operationType, final String sessionID,
			final String merchantID, final String apiKey, final AbstractOrderModel order)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(sessionID), SESSION_ID_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(merchantID), MERCHANT_ID_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(StringUtils.isNotBlank(apiKey), API_KEY_CAN_NOT_BE_NULL);
		Preconditions.checkArgument(order != null, ORDER_CAN_NOT_BE_NULL);

		final PaymentTransactionInfoBean bean = populatePaymentTransactionInfoBean(operationType, sessionID, merchantID, apiKey,
				order);

		return generatPaymentTransactionInfo(bean, order);

	}

	/**
	 *
	 */
	private PaymentTransactionInfoModel generatPaymentTransactionInfo(final PaymentTransactionInfoBean bean,
			final AbstractOrderModel order)
	{
		final PaymentTransactionInfoModel paymentTransactionInfo = getModelService().create(PaymentTransactionInfoModel.class);

		paymentTransactionInfo.setCode(bean.getCode());
		paymentTransactionInfo.setOrderCode(bean.getOrderCode());
		paymentTransactionInfo.setDeliveryCost(bean.getDeliveryCost());
		paymentTransactionInfo.setTotalPrice(bean.getTotalPrice());
		paymentTransactionInfo.setTotalTax(bean.getTotalTax());
		paymentTransactionInfo.setMpgsSuccessIndicator(bean.getMpgsSuccessIndicator());
		paymentTransactionInfo.setRedeemedLoyaltyPoints(bean.getRedeemedLoyaltyPoints());
		paymentTransactionInfo.setGiiftValidateId(bean.getGiiftValidateId());
		paymentTransactionInfo.setGiiftTransactionId(bean.getGiiftTransactionId());
		paymentTransactionInfo.setTotalPriceAfterRedeem(bean.getTotalPriceAfterRedeem());
		paymentTransactionInfo.setLoyaltyPaymentMode(bean.getLoyaltyPaymentMode());
		paymentTransactionInfo.setLoyaltyAmountSelected(bean.getLoyaltyAmountSelected());
		paymentTransactionInfo.setExpectedLoyaltyRedeemAmount(bean.getExpectedLoyaltyRedeemAmount());
		paymentTransactionInfo.setExpectedAddedPoint(bean.getExpectedAddedPoint());
		paymentTransactionInfo.setUsableLoyaltyRedeemPoints(bean.getUsableLoyaltyRedeemPoints());
		paymentTransactionInfo.setUsableLoyaltyRedeemAmount(bean.getUsableLoyaltyRedeemAmount());
		paymentTransactionInfo.setLoyaltyPaymentStatus(bean.getLoyaltyPaymentStatus());
		paymentTransactionInfo.setLoyaltyPaymentType(bean.getLoyaltyPaymentType());
		paymentTransactionInfo.setLoyaltyFailureReason(bean.getLoyaltyFailureReason());
		paymentTransactionInfo.setLoyaltyExchangeRate(bean.getLoyaltyExchangeRate());
		paymentTransactionInfo.setLoyaltyAmount(bean.getLoyaltyAmount());
		paymentTransactionInfo.setShouldCalculateLoyalty(bean.getShouldCalculateLoyalty());
		paymentTransactionInfo.setOperationType(bean.getOperationType());
		paymentTransactionInfo.setSessionId(bean.getSessionId());
		paymentTransactionInfo.setMerchantID(bean.getMerchantID());
		paymentTransactionInfo.setApiKey(bean.getApiKey());
		paymentTransactionInfo.setGlobalDiscountValues(bean.getGlobalDiscountValues());
		paymentTransactionInfo.setStoreCreditAmount(bean.getStoreCreditAmount());
		paymentTransactionInfo.setStoreCreditAmountSelected(bean.getStoreCreditAmountSelected());
		paymentTransactionInfo.setStoreCreditMode(bean.getStoreCreditMode());

		paymentTransactionInfo.setEntries(generatPaymentTransactionInfoEntries(bean.getEntries()));
		paymentTransactionInfo.setData(getJSONData(bean));
		paymentTransactionInfo.setOrder(order);
		paymentTransactionInfo.setTotalPriceWithTax(bean.getTotalPriceWithTax());
		getModelService().saveAll(paymentTransactionInfo);
		getModelService().refresh(paymentTransactionInfo);
		return paymentTransactionInfo;
	}

	/**
	 * @param bean
	 *
	 */
	private String getJSONData(final PaymentTransactionInfoBean bean)
	{

		return GSON.toJson(bean, PaymentTransactionInfoBean.class);

	}

	/**
	 *
	 */
	private List<PaymentTransactionInfoOrderEntryModel> generatPaymentTransactionInfoEntries(
			final List<PaymentTransactionOrderEntryBean> entries)
	{
		if (CollectionUtils.isEmpty(entries))
		{
			return null;
		}


		final List<PaymentTransactionInfoOrderEntryModel> list = new ArrayList<>();

		for (final PaymentTransactionOrderEntryBean entry : entries)
		{
			list.add(generatPaymentTransactionInfoEntry(entry));
		}
		return list;

	}

	/**
	 *
	 */
	private PaymentTransactionInfoOrderEntryModel generatPaymentTransactionInfoEntry(final PaymentTransactionOrderEntryBean entry)
	{

		final PaymentTransactionInfoOrderEntryModel entryModel = getModelService()
				.create(PaymentTransactionInfoOrderEntryModel.class);

		entryModel.setProductCode(entry.getProductCode());
		entryModel.setOrderNumber(entry.getOrderNumber());
		entryModel.setQuantity(entry.getQuantity());
		entryModel.setUnit(entry.getUnit());
		entryModel.setBasePrice(entry.getBasePrice());
		entryModel.setTotalPrice(entry.getTotalPrice());
		entryModel.setDiscountValue(entry.getDiscountValue());
		entryModel.setTaxValue(entry.getTaxValue());
		return entryModel;
	}

	/**
	 *
	 */
	private PaymentTransactionInfoBean populatePaymentTransactionInfoBean(final String operationType, final String sessionID,
			final String merchantID, final String apiKey, final AbstractOrderModel order)
	{
		final PaymentTransactionInfoBean bean = new PaymentTransactionInfoBean();

		bean.setCode(order.getCode());
		bean.setOrderCode(order.getOrderCode());
		bean.setDeliveryCost(objectToString(order.getDeliveryCost()));
		bean.setTotalPrice(String.valueOf(order.getTotalPrice()));
		bean.setTotalTax(String.valueOf(order.getTotalTax()));
		bean.setMpgsSuccessIndicator(order.getMpgsSuccessIndicator());
		bean.setRedeemedLoyaltyPoints(String.valueOf(order.getRedeemedLoyaltyPoints()));
		bean.setGiiftValidateId(order.getGiiftValidateId());
		bean.setGiiftTransactionId(order.getGiiftTransactionId());
		bean.setTotalPriceAfterRedeem(String.valueOf(order.getTotalPriceAfterRedeem()));
		bean.setLoyaltyPaymentMode(order.getLoyaltyPaymentMode() == null ? "" : order.getLoyaltyPaymentMode().getName());
		bean.setLoyaltyAmountSelected(String.valueOf(order.getLoyaltyAmountSelected()));
		bean.setExpectedLoyaltyRedeemAmount(objectToString(order.getExpectedLoyaltyRedeemAmount()));
		bean.setExpectedAddedPoint(objectToString(order.getExpectedAddedPoint()));
		bean.setUsableLoyaltyRedeemPoints(objectToString(order.getUsableLoyaltyRedeemPoints()));
		bean.setUsableLoyaltyRedeemAmount(objectToString(order.getUsableLoyaltyRedeemAmount()));
		bean.setLoyaltyPaymentStatus(hybrisEnumValueToString(order.getLoyaltyPaymentStatus()));
		bean.setLoyaltyPaymentType(hybrisEnumValueToString(order.getLoyaltyPaymentType()));
		bean.setLoyaltyFailureReason(order.getLoyaltyFailureReason());
		bean.setLoyaltyExchangeRate(String.valueOf(order.getLoyaltyExchangeRate()));
		bean.setLoyaltyAmount(String.valueOf(order.getLoyaltyAmount()));

		bean.setEntries(getPaymentTransactionOrderEntries(order));
		bean.setShouldCalculateLoyalty(String.valueOf(order.isShouldCalculateLoyalty()));

		bean.setOperationType(operationType);
		bean.setSessionId(sessionID);
		bean.setMerchantID(merchantID);
		bean.setApiKey(apiKey);
		bean.setGlobalDiscountValues(String.valueOf(getGlobalDiscountValues(order)));

		bean.setStoreCreditAmount(objectToString(order.getStoreCreditAmount()));
		bean.setStoreCreditAmountSelected(objectToString(order.getStoreCreditAmountSelected()));
		bean.setStoreCreditMode(order.getStoreCreditMode() != null ? order.getStoreCreditMode().getName() : "");
		bean.setTotalPriceWithTax(objectToString(getOrderTotalWithTax(order)));

		return bean;
	}

	/**
	 *
	 */
	private List<PaymentTransactionOrderEntryBean> getPaymentTransactionOrderEntries(final AbstractOrderModel order)
	{
		if (order == null || CollectionUtils.isEmpty(order.getEntries()))
		{
			return Collections.emptyList();
		}

		final List<PaymentTransactionOrderEntryBean> list = new ArrayList<>();

		for (final AbstractOrderEntryModel orderEntry : order.getEntries())
		{
			list.add(getPaymentTransactionOrderEntry(orderEntry));
		}
		return list;
	}

	/**
	 *
	 */
	private PaymentTransactionOrderEntryBean getPaymentTransactionOrderEntry(final AbstractOrderEntryModel entry)
	{
		final PaymentTransactionOrderEntryBean bean = new PaymentTransactionOrderEntryBean();

		bean.setProductCode(entry.getProduct() != null ? entry.getProduct().getCode() : "");
		bean.setQuantity(objectToString(entry.getQuantity()));
		bean.setUnit(entry.getUnit() != null ? entry.getUnit().getCode() : "");
		bean.setBasePrice(objectToString(entry.getBasePrice()));
		bean.setTotalPrice(objectToString(entry.getTotalPrice()));
		bean.setDiscountValue(String.valueOf(getEntryDiscountValues(entry)));
		bean.setTaxValue(String.valueOf(entry.getTaxValues()));
		return bean;
	}

	private double getEntryDiscountValues(final AbstractOrderEntryModel entry)
	{

		if (entry == null || CollectionUtils.isEmpty(entry.getDiscountValues()))
		{
			return 0d;
		}

		double entryDiscountValues = 0d;
		for (final DiscountValue discountValue : entry.getDiscountValues())
		{
			entryDiscountValues += discountValue.getValue();
		}

		return entryDiscountValues;
	}

	/**
	 *
	 */
	private double getGlobalDiscountValues(final AbstractOrderModel order)
	{

		if (order == null || CollectionUtils.isEmpty(order.getDiscounts()))
		{
			return 0d;
		}

		double globalDiscountValues = 0d;
		for (final DiscountModel discountModel : order.getDiscounts())
		{
			globalDiscountValues += discountModel.getValue() == null ? 0d : discountModel.getValue().doubleValue();
		}

		return globalDiscountValues;
	}


	protected Double getOrderTotalWithTax(final AbstractOrderModel abstractOrderModel)
	{
		final CurrencyModel cartCurrency = abstractOrderModel.getCurrency();
		final CurrencyModel baseCurrency = commonI18NService.getBaseCurrency();
		final double calcTotalWithTax = calcTotalWithTax(abstractOrderModel);

		return commonI18NService.convertAndRoundCurrency(cartCurrency.getConversion().doubleValue(),
				baseCurrency.getConversion().doubleValue(), cartCurrency.getDisplayDigits(), calcTotalWithTax);
	}

	private double calcTotalWithTax(final AbstractOrderModel source)
	{
		if (source == null)
		{
			throw new IllegalArgumentException("source order must not be null");
		}
		if (source.getTotalPrice() == null)
		{
			return 0.0d;
		}

		BigDecimal totalPrice = BigDecimal.valueOf(source.getTotalPrice().doubleValue());

		// Add the taxes to the total price if the cart is net; if the total was null taxes should be null as well
		if (Boolean.TRUE.equals(source.getNet()) && totalPrice.compareTo(BigDecimal.ZERO) != 0 && source.getTotalTax() != null)
		{
			totalPrice = totalPrice.add(BigDecimal.valueOf(source.getTotalTax().doubleValue()));
		}
		return totalPrice.doubleValue();
	}

	/**
	 *
	 */
	private String objectToString(final Object object)
	{
		return object != null ? object.toString() : "";
	}

	private String hybrisEnumValueToString(final HybrisEnumValue object)
	{
		return object != null ? object.getCode() : "";
	}


	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}





}
