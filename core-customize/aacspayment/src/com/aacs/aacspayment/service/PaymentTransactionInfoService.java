/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import com.aacs.aacspayment.model.PaymentTransactionInfoModel;


/**
 *
 */
public interface PaymentTransactionInfoService
{

	public PaymentTransactionInfoModel savePaymentTransactionInfo(final String operationType, final String sessionID,
			final String merchantID, String apiKey, AbstractOrderModel order);

	public PaymentTransactionInfoModel updatePaymentTransactionInfo(final PaymentTransactionInfoModel paymentTransactionInfoModel,
			final String sessionInfo, final double sessionInfoAmount, final String sessionInfoError);

}
