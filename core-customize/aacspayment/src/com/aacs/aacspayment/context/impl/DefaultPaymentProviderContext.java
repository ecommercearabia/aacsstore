/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.context.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.util.Strings;

import com.aacs.aacspayment.context.PaymentProviderContext;
import com.aacs.aacspayment.enums.CardPaymentProvider;
import com.aacs.aacspayment.model.CCAvenuePaymentProviderModel;
import com.aacs.aacspayment.model.HyperpayPaymentProviderModel;
import com.aacs.aacspayment.model.MpgsPaymentProviderModel;
import com.aacs.aacspayment.model.PaymentProviderModel;
import com.aacs.aacspayment.strategy.PaymentProviderStrategy;
import com.google.common.base.Preconditions;


/**
 * @author mnasro
 *
 *         The Class DefaultPaymentProviderContext.
 */
public class DefaultPaymentProviderContext implements PaymentProviderContext
{

	/** The Constant PROVIDER_CLASS_MUSTN_T_BE_NULL. */
	private static final String PROVIDER_CLASS_MUSTN_T_BE_NULL = "strategy mustn't be null";

	/** The Constant PROVIDER_CLASS_MUSTN_T_BE_NULL. */
	private static final String BASESTORE_MUSTN_T_BE_NULL = "baseStoreModel mustn't be null";

	/** The Constant PROVIDER_STRATEGY_NOT_FOUND. */
	private static final String PROVIDER_STRATEGY_NOT_FOUND = "strategy not found";

	/** The payment provider map. */
	@Resource(name = "paymentProviderMap")
	private Map<Class<?>, PaymentProviderStrategy> paymentProviderMap;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	/**
	 * Gets the provider.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the provider
	 */
	@Override
	public Optional<PaymentProviderModel> getProvider(final Class<?> providerClass, final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);

		final Optional<PaymentProviderStrategy> strategy = getStrategy(providerClass);

		if (!strategy.isPresent())
		{
			return Optional.empty();
		}
		return strategy.get().getActiveProvider(baseStoreModel);
	}

	/**
	 * Gets the strategy.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the strategy
	 */
	protected Optional<PaymentProviderStrategy> getStrategy(final Class<?> providerClass)
	{
		final PaymentProviderStrategy strategy = getPaymentProviderMap().get(providerClass);
		Preconditions.checkArgument(strategy != null, PROVIDER_STRATEGY_NOT_FOUND);

		return Optional.ofNullable(strategy);
	}

	/**
	 * Gets the payment provider map.
	 *
	 * @return the payment provider map
	 */
	protected Map<Class<?>, PaymentProviderStrategy> getPaymentProviderMap()
	{
		return paymentProviderMap;
	}

	@Override
	public Optional<PaymentProviderModel> getProvider(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_MUSTN_T_BE_NULL);

		if (StringUtils.isBlank(baseStoreModel.getPaymentProvider()))
		{
			return Optional.empty();
		}
		switch (baseStoreModel.getPaymentProvider().toUpperCase())
		{
			case "CCAVENUEPAYMENTPROVIDER":
				return getProvider(CCAvenuePaymentProviderModel.class, baseStoreModel);
			case "HYPERPAYPAYMENTPROVIDER":
				return getProvider(HyperpayPaymentProviderModel.class, baseStoreModel);
			case "MPGSPAYMENTPROVIDER":
				return getProvider(MpgsPaymentProviderModel.class, baseStoreModel);
			default:
				return Optional.empty();
		}

	}

	@Override
	public Optional<PaymentProviderModel> getProviderByCurrentStore()
	{
		return getProvider(getBaseStoreService().getCurrentBaseStore());
	}

	@Override
	public Optional<PaymentProviderModel> getProviderByPaymentMode(final PaymentModeModel paymentModeModel,
			final BaseStoreModel baseStore)

	{
		Preconditions.checkArgument(paymentModeModel != null, "Payment mode is null");

		if (Strings.isBlank(paymentModeModel.getCode()))
		{
			return Optional.empty();
		}
		switch (paymentModeModel.getCode().toUpperCase())
		{
			case "MADA":
				return getMadaProvider(baseStore);
			case "CARD":
				return getCardProvider(baseStore);
			case "APPLEPAY":
				return getApplePayProvider(baseStore);
			case "APPLE":
				return getCardProvider(baseStore);
			default:
				return Optional.empty();
		}
	}


	/**
	 *
	 */
	private Optional<PaymentProviderModel> getCardProvider(final BaseStoreModel baseStore)
	{
		Preconditions.checkArgument(baseStore != null, BASESTORE_MUSTN_T_BE_NULL);
		if (baseStore.getDefaultCardPaymentProvider() == null)
		{
			return Optional.empty();
		}

		final CardPaymentProvider defaultCardPaymentProvider = baseStore.getDefaultCardPaymentProvider();
		if (defaultCardPaymentProvider == CardPaymentProvider.MPGS)
		{
			return getProvider(MpgsPaymentProviderModel.class, baseStore);
		}

		return Optional.empty();
	}

	/**
	 *
	 */
	private Optional<PaymentProviderModel> getMadaProvider(final BaseStoreModel baseStore)
	{
		throw new UnsupportedOperationException("Mada provider is not yet configured");
	}

	/**
	 *
	 */
	private Optional<PaymentProviderModel> getApplePayProvider(final BaseStoreModel baseStore)
	{
		throw new UnsupportedOperationException("Mada provider is not yet configured");
	}


	@Override
	public Optional<PaymentProviderModel> getProvider(final AbstractOrderModel order)
	{
		Preconditions.checkArgument(order != null, "order is null");
		Preconditions.checkArgument(order.getStore() != null, BASESTORE_MUSTN_T_BE_NULL);

		return getProviderByPaymentMode(order.getPaymentMode(), order.getStore());
	}

}
