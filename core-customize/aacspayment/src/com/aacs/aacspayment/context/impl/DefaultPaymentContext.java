/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.context.impl;

import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aacs.aacspayment.context.PaymentContext;
import com.aacs.aacspayment.context.PaymentProviderContext;
import com.aacs.aacspayment.entry.PaymentRequestData;
import com.aacs.aacspayment.entry.PaymentResponseData;
import com.aacs.aacspayment.enums.PaymentResponseStatus;
import com.aacs.aacspayment.exception.PaymentException;
import com.aacs.aacspayment.exception.type.PaymentExceptionType;
import com.aacs.aacspayment.model.HyperpayPaymentProviderModel;
import com.aacs.aacspayment.model.PaymentProviderModel;
import com.aacs.aacspayment.strategy.PaymentStrategy;
import com.google.common.base.Preconditions;


/**
 * The Class DefaultPaymentContext.
 *
 * @author mnasro
 * @author abu-muhasien
 */
public class DefaultPaymentContext implements PaymentContext
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultPaymentContext.class);
	/** The Constant DATA_CAN_NOT_BE_NULL. */
	private static final String DATA_CAN_NOT_BE_NULL = "data can not be null";

	/** The Constant PAYMENT_PROVIDER_MODEL_MUST_NOT_BE_NULL. */
	private static final String PAYMENT_PROVIDER_MODEL_MUST_NOT_BE_NULL = "paymentProviderModel must not be null";

	/** The Constant PAYMENT_PROVIDER_MODEL_MUST_NOT_BE_NULL. */
	private static final String ORDER_MODEL_MUST_NOT_BE_NULL = "abstractOrderModel must not be null";

	private static final String CONSIGNMENT_MODEL_MUST_NOT_BE_NULL = "consignmentModel must not be null";


	/** The Constant PAYMENT_STRATEGY_NOT_FOUND. */
	private static final String PAYMENT_STRATEGY_NOT_FOUND = "strategy not found";

	/** The payment provider context. */
	@Resource(name = "paymentProviderContext")
	private PaymentProviderContext paymentProviderContext;

	/** The payment strategy map. */
	@Resource(name = "paymentStrategyMap")
	private Map<Class<?>, PaymentStrategy> paymentStrategyMap;

	/**
	 * Gets the payment strategy map.
	 *
	 * @return the payment strategy map
	 */
	protected Map<Class<?>, PaymentStrategy> getPaymentStrategyMap()
	{
		return paymentStrategyMap;
	}

	/**
	 * Gets the strategy.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the strategy
	 */
	protected Optional<PaymentStrategy> getStrategy(final Class<?> providerClass)
	{
		final PaymentStrategy strategy = getPaymentStrategyMap().get(providerClass);
		Preconditions.checkArgument(strategy != null, PAYMENT_STRATEGY_NOT_FOUND);

		return Optional.ofNullable(strategy);
	}

	/**
	 * Gets the payment data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order
	 * @return the payment data
	 */
	@Override
	public Optional<PaymentRequestData> getPaymentData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MODEL_MUST_NOT_BE_NULL);
		Preconditions.checkArgument(abstractOrderModel != null, ORDER_MODEL_MUST_NOT_BE_NULL);

		if (!("card".equalsIgnoreCase(abstractOrderModel.getPaymentMode().getCode())
				|| "apple".equalsIgnoreCase(abstractOrderModel.getPaymentMode().getCode())
				|| "google".equalsIgnoreCase(abstractOrderModel.getPaymentMode().getCode())))
		{
			return Optional.empty();
		}

		final Optional<PaymentStrategy> paymentStrategy = getStrategy(paymentProviderModel.getClass());
		if (paymentStrategy.isPresent())
		{
			return paymentStrategy.get().buildPaymentRequestData(paymentProviderModel, abstractOrderModel);
		}
		return Optional.empty();

	}

	/**
	 * Gets the payment data.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param abstractOrderModel
	 *           the abstract order
	 * @return the payment data
	 */
	@Override
	public Optional<PaymentRequestData> getPaymentData(final BaseStoreModel baseStoreModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{
		final Optional<PaymentProviderModel> paymentProvider = paymentProviderContext.getProvider(baseStoreModel);
		if (!paymentProvider.isPresent())
		{
			return Optional.empty();

		}
		return getPaymentData(paymentProvider.get(), abstractOrderModel);
	}


	/**
	 * Gets the payment data by current store.
	 *
	 * @param abstractOrderModel
	 *           the abstract order
	 * @return the payment data by current store
	 */
	@Override
	public Optional<PaymentRequestData> getPaymentDataByCurrentStore(final AbstractOrderModel abstractOrderModel)
			throws PaymentException
	{
		Preconditions.checkArgument(abstractOrderModel != null, ORDER_MODEL_MUST_NOT_BE_NULL);
		Preconditions.checkArgument(abstractOrderModel.getPaymentMode() != null, "No payment mode selected for this cart");

		if (!("card".equalsIgnoreCase(abstractOrderModel.getPaymentMode().getCode())
				|| "apple".equalsIgnoreCase(abstractOrderModel.getPaymentMode().getCode())
				|| "google".equalsIgnoreCase(abstractOrderModel.getPaymentMode().getCode())))
		{
			return Optional.empty();
		}

		final Optional<PaymentProviderModel> paymentProvider = paymentProviderContext.getProviderByCurrentStore();
		if (!paymentProvider.isPresent())
		{
			return Optional.empty();
		}

		return getPaymentData(paymentProvider.get(), abstractOrderModel);
	}

	/**
	 * Interpret response.
	 *
	 * @param responseParams
	 *           the response params
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @return the optional
	 * @throws PaymentException
	 */
	@Override
	public Optional<CreateSubscriptionResult> interpretResponse(final Map<String, Object> responseParams,
			final PaymentProviderModel paymentProviderModel)
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MODEL_MUST_NOT_BE_NULL);
		final Optional<PaymentStrategy> strategy = getStrategy(paymentProviderModel.getClass());
		if (!strategy.isPresent())
		{
			return Optional.empty();
		}
		return strategy.get().interpretResponse(responseParams, paymentProviderModel);


	}

	/**
	 * Interpret response.
	 *
	 * @param responseParams
	 *           the response params
	 * @param baseStoreModel
	 *           the base store model
	 * @return the optional
	 * @throws PaymentException
	 */
	@Override
	public Optional<CreateSubscriptionResult> interpretResponse(final Map<String, Object> responseParams,
			final BaseStoreModel baseStoreModel) throws PaymentException
	{
		final Optional<PaymentProviderModel> paymentProvider = paymentProviderContext.getProvider(baseStoreModel);
		if (paymentProvider.isPresent())
		{
			return interpretResponse(responseParams, paymentProvider.get());
		}
		return Optional.empty();
	}

	/**
	 * Interpret response by current store.
	 *
	 * @param responseParams
	 *           the response params
	 * @return the optional
	 * @throws PaymentException
	 */
	@Override
	public Optional<CreateSubscriptionResult> interpretResponseByCurrentStore(final Map<String, Object> responseParams)
			throws PaymentException
	{
		final Optional<PaymentProviderModel> paymentProvider = paymentProviderContext.getProviderByCurrentStore();
		if (paymentProvider.isPresent())
		{
			return interpretResponse(responseParams, paymentProvider.get());
		}
		return Optional.empty();
	}

	/**
	 * Gets the response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param data
	 *           the data
	 * @return the response data
	 * @throws PaymentException
	 */
	@Override
	public Optional<PaymentResponseData> getResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data)
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MODEL_MUST_NOT_BE_NULL);
		Preconditions.checkArgument(data != null, DATA_CAN_NOT_BE_NULL);

		final Optional<PaymentStrategy> strategy = getStrategy(paymentProviderModel.getClass());
		if (!strategy.isPresent())
		{
			return Optional.empty();
		}
		return strategy.get().buildPaymentResponseData(paymentProviderModel, abstractOrderModel, data);
	}

	/**
	 * Gets the response data.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param data
	 *           the data
	 * @return the response data
	 * @throws PaymentException
	 */
	@Override
	public Optional<PaymentResponseData> getResponseData(final BaseStoreModel baseStoreModel,
			final AbstractOrderModel abstractOrderModel, final Object data) throws PaymentException
	{
		final Optional<PaymentProviderModel> paymentProvider = paymentProviderContext.getProvider(baseStoreModel);
		if (!paymentProvider.isPresent())
		{
			return Optional.empty();
		}
		return getResponseData(paymentProvider.get(), abstractOrderModel, data);

	}

	/**
	 * Gets the response data by current store.
	 *
	 * @param data
	 *           the data
	 * @return the response data by current store
	 * @throws PaymentException
	 */
	@Override
	public Optional<PaymentResponseData> getResponseDataByCurrentStore(final AbstractOrderModel abstractOrderModel,
			final Object data) throws PaymentException
	{
		final Optional<PaymentProviderModel> paymentProvider = paymentProviderContext.getProviderByCurrentStore();
		if (!paymentProvider.isPresent())
		{
			return Optional.empty();
		}
		return getResponseData(paymentProvider.get(), abstractOrderModel, data);

	}

	/**
	 * Checks if is successful paid order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return true, if is successful paid order
	 * @throws PaymentException
	 */
	@Override
	public boolean isSuccessfulPaidOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data) throws PaymentException
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MODEL_MUST_NOT_BE_NULL);
		final Optional<PaymentStrategy> strategy = getStrategy(paymentProviderModel.getClass());
		if (!strategy.isPresent())
		{
			return false;
		}
		return strategy.get().isSuccessfulPaidOrder(paymentProviderModel, abstractOrderModel, data);
	}

	/**
	 * Checks if is successful paid order.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return true, if is successful paid order
	 * @throws PaymentException
	 */
	@Override
	public boolean isSuccessfulPaidOrder(final BaseStoreModel baseStoreModel, final AbstractOrderModel abstractOrderModel,
			final Object data) throws PaymentException
	{
		final Optional<PaymentProviderModel> paymentProvider = paymentProviderContext.getProvider(baseStoreModel);
		if (!paymentProvider.isPresent())
		{
			return false;
		}
		return isSuccessfulPaidOrder(paymentProvider.get(), abstractOrderModel, data);

	}

	/**
	 * Checks if is successful paid order by current store.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return true, if is successful paid order by current store
	 * @throws PaymentException
	 */
	@Override
	public boolean isSuccessfulPaidOrderByCurrentStore(final AbstractOrderModel abstractOrderModel, final Object data)
			throws PaymentException
	{
		final Optional<PaymentProviderModel> paymentProvider = paymentProviderContext.getProviderByCurrentStore();
		if (!paymentProvider.isPresent())
		{
			return false;
		}
		return isSuccessfulPaidOrder(paymentProvider.get(), abstractOrderModel, data);

	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseDataByCurrentStore(final Object data,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{
		final Optional<PaymentProviderModel> paymentProviderModel = paymentProviderContext.getProviderByCurrentStore();
		if (!paymentProviderModel.isPresent())
		{
			return Optional.empty();
		}

		return getPaymentOrderStatusResponseData(paymentProviderModel.get(), abstractOrderModel, data);
	}


	@Override
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseData(final BaseStoreModel baseStoreModel,
			final AbstractOrderModel abstractOrderModel, final Object data) throws PaymentException
	{
		final Optional<PaymentProviderModel> paymentProvider = paymentProviderContext.getProvider(baseStoreModel);
		if (paymentProvider.isEmpty())
		{
			return Optional.empty();
		}
		return getPaymentOrderStatusResponseData(paymentProvider.get(), abstractOrderModel, data);
	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data) throws PaymentException
	{
		final Optional<PaymentStrategy> strategy = getStrategy(paymentProviderModel.getClass());

		if (!strategy.isPresent())
		{
			return Optional.empty();
		}
		return strategy.get().getPaymentOrderStatusResponseData(paymentProviderModel, abstractOrderModel, data);
	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderConfirmedResponseData(final BaseStoreModel baseStoreModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{
		final Optional<PaymentProviderModel> paymentProvider = paymentProviderContext.getProvider(baseStoreModel);
		if (!paymentProvider.isPresent())
		{

			return Optional.empty();
		}
		return getPaymentOrderConfirmedResponseData(paymentProvider.get(), abstractOrderModel);
	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderConfirmedResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{

		final Optional<PaymentStrategy> strategy = getStrategy(paymentProviderModel.getClass());

		if (!strategy.isPresent())
		{
			return Optional.empty();
		}
		return strategy.get().captureOrder(paymentProviderModel, abstractOrderModel);
	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderCancelResponseData(final BaseStoreModel baseStoreModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{
		final Optional<PaymentProviderModel> paymentProvider = paymentProviderContext.getProvider(baseStoreModel);
		if (!paymentProvider.isPresent())
		{
			return Optional.empty();
		}
		return getPaymentOrderCancelResponseData(paymentProvider.get(), abstractOrderModel);
	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderCancelResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{

		final Optional<PaymentStrategy> strategy = getStrategy(paymentProviderModel.getClass());

		if (!strategy.isPresent())
		{
			return Optional.empty();
		}
		return strategy.get().cancelOrder(paymentProviderModel, abstractOrderModel);
	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderRefundResponseData(final BaseStoreModel baseStoreModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{
		final Optional<PaymentProviderModel> paymentProvider = paymentProviderContext.getProvider(baseStoreModel);
		if (!paymentProvider.isPresent())
		{
			return Optional.empty();
		}
		return getPaymentOrderRefundResponseData(paymentProvider.get(), abstractOrderModel);
	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderRefundResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{

		final Optional<PaymentStrategy> strategy = getStrategy(paymentProviderModel.getClass());

		if (!strategy.isPresent())
		{
			return Optional.empty();
		}
		return strategy.get().refundOrder(paymentProviderModel, abstractOrderModel);
	}

	@Override
	public Optional<PaymentRequestData> getPaymentData(final AbstractOrderModel abstractOrderModel) throws PaymentException
	{
		return getPaymentData(abstractOrderModel.getStore(), abstractOrderModel);
	}

	@Override
	public Optional<PaymentResponseData> initiate3DSecureCheckByCurrentStore(final AbstractOrderModel abstractOrderModel,
			final Object data) throws PaymentException
	{
		final Optional<PaymentProviderModel> paymentProviderModel = paymentProviderContext.getProviderByCurrentStore();
		if (paymentProviderModel.isEmpty())
		{
			return Optional.empty();
		}
		return initiate3DSecureCheck(paymentProviderModel.get(), abstractOrderModel, data);
	}

	@Override
	public Optional<PaymentResponseData> initiate3DSecureCheck(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data) throws PaymentException
	{
		final Optional<PaymentStrategy> strategy = getStrategy(paymentProviderModel.getClass());

		if (strategy.isEmpty())
		{
			return Optional.empty();
		}
		return strategy.get().initiate3DSecureCheck(paymentProviderModel, abstractOrderModel, data);
	}

	@Override
	public Optional<PaymentResponseData> authenticate3DSecurePayerByCurrentStore(final AbstractOrderModel abstractOrderModel,
			final Object data, final String secureId) throws PaymentException
	{
		final Optional<PaymentProviderModel> paymentProviderModel = paymentProviderContext.getProviderByCurrentStore();
		if (paymentProviderModel.isEmpty())
		{
			return Optional.empty();
		}
		return authenticate3DSecurePayer(paymentProviderModel.get(), abstractOrderModel, data, secureId);
	}

	@Override
	public Optional<PaymentResponseData> authenticate3DSecurePayer(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data, final String secureId) throws PaymentException
	{
		final Optional<PaymentStrategy> strategy = getStrategy(paymentProviderModel.getClass());

		if (strategy.isEmpty())
		{
			return Optional.empty();
		}
		return strategy.get().authenticate3DSecurePayer(paymentProviderModel, abstractOrderModel, data, secureId);
	}

	@Override
	public Optional<PaymentResponseData> payOrderByCurrentStore(final AbstractOrderModel abstractOrderModel,
			final Object sessionId, final Object authenticatedTransactionId, final String threeDSResponse) throws PaymentException
	{
		final Optional<PaymentProviderModel> paymentProviderModel = paymentProviderContext.getProviderByCurrentStore();
		if (!paymentProviderModel.isPresent())
		{
			return Optional.empty();
		}
		return payOrder(paymentProviderModel.get(), abstractOrderModel, sessionId, authenticatedTransactionId, threeDSResponse);
	}

	@Override
	public Optional<PaymentResponseData> payOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object sessionId, final Object authenticatedTransactionId,
			final String threeDSResponse) throws PaymentException
	{
		final Optional<PaymentStrategy> strategy = getStrategy(paymentProviderModel.getClass());

		if (strategy.isEmpty())
		{
			return Optional.empty();
		}
		return strategy.get().payOrder(paymentProviderModel, abstractOrderModel, sessionId, authenticatedTransactionId,
				threeDSResponse);
	}

	@Override
	public boolean isWebHookNotificationPending(final HyperpayPaymentProviderModel paymentProvider,
			final Map<String, Object> payload) throws PaymentException
	{
		final PaymentStrategy strategy = getStrategy(paymentProvider.getClass()).orElse(null);
		if (strategy == null)
		{
			throw new PaymentException(String.format("Cannot find strategy for PaymentProvider: Pk[%s], Code[%s]",
					paymentProvider.getPk(), paymentProvider.getCode()), PaymentExceptionType.CLIENT_ERROR);
		}


		return strategy.isWebHookNotificationPending(payload);
	}

	private Optional<PaymentProviderModel> getProvider(final AbstractOrderModel order)
	{
		if (Objects.isNull(order) || Objects.isNull(order.getStore()))
		{
			return Optional.empty();
		}

		return paymentProviderContext.getProvider(order.getStore());
	}

	@Override
	public PaymentResponseStatus getTransactionStatus(final Map<String, Object> response,
			final AbstractOrderModel abstractorderModel)
	{
		final Optional<PaymentProviderModel> provider = getProvider(abstractorderModel);
		Preconditions.checkArgument(provider.isPresent(), PAYMENT_PROVIDER_MODEL_MUST_NOT_BE_NULL);

		final Optional<PaymentStrategy> strategy = getStrategy(provider.get().getClass());
		if (!strategy.isPresent())
		{
			return null;
		}

		return strategy.get().getTransactionStatus(response);
	}


	@Override
	public Optional<CreateSubscriptionResult> interpretResponse(final AbstractOrderModel order)
	{
		final Optional<PaymentProviderModel> provider = getProvider(order);
		return interpretResponse(order, provider.orElse(null));
	}

	protected Optional<CreateSubscriptionResult> interpretResponse(final AbstractOrderModel order,
			final PaymentProviderModel paymentProviderModel)
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MODEL_MUST_NOT_BE_NULL);
		final Optional<PaymentStrategy> strategy = getStrategy(paymentProviderModel.getClass());
		if (!strategy.isPresent())
		{
			return Optional.empty();
		}
		return strategy.get().interpretResponse(order, paymentProviderModel);


	}

	@Override
	public Optional<CreateSubscriptionResult> interpretResponseUsingWebhook(final Map<String, Object> orderInfoMap,
			final AbstractOrderModel order)
	{
		final Optional<PaymentProviderModel> provider = getProvider(order);
		return interpretResponse(orderInfoMap, provider.orElse(null));
	}

	@Override
	public Map<String, Object> getCheckoutMap(final AbstractOrderModel abstractOrderModel)
	{
		final Optional<PaymentProviderModel> supportedPaymentProvider = paymentProviderContext.getProvider(abstractOrderModel);

		if (supportedPaymentProvider.isEmpty() || Objects.isNull(abstractOrderModel))
		{
			return MapUtils.EMPTY_MAP;
		}

		final PaymentProviderModel paymentProviderModel = supportedPaymentProvider.get();

		if (paymentProviderModel instanceof HyperpayPaymentProviderModel)
		{
			return abstractOrderModel.getHyperpayCheckoutMap();
		}
		LOG.warn("Could not get checkoutmap from order");
		//		else if (paymentProviderModel instanceof PaymentProviderModel)
		//		{
		//			return abstractOrderModel.getPaymentResponseMap();
		//		}
		return MapUtils.EMPTY_MAP;
	}

	@Override
	public Optional<PaymentResponseData> getResponseData(final AbstractOrderModel abstractOrderModel, final Object data)
	{
		final Optional<PaymentProviderModel> paymentProvider = getProvider(abstractOrderModel);
		if (paymentProvider.isEmpty())
		{
			return Optional.empty();
		}
		return getResponseData(paymentProvider.get(), abstractOrderModel, data);
	}

	@Override
	public Optional<PaymentResponseData> captureOrder(final AbstractOrderModel abstractOrderModel) throws PaymentException
	{
		Preconditions.checkArgument(abstractOrderModel != null, ORDER_MODEL_MUST_NOT_BE_NULL);

		final Optional<PaymentProviderModel> provider = getProvider(abstractOrderModel);
		Preconditions.checkArgument(provider.isPresent(), PAYMENT_PROVIDER_MODEL_MUST_NOT_BE_NULL);

		final Optional<PaymentStrategy> strategy = getStrategy(provider.get().getClass());
		if (!strategy.isPresent())
		{
			LOG.error(PaymentExceptionType.PAYMENT_PROVIDER_DOES_NOT_EXIST.getMessage());
			throw new PaymentException(PaymentExceptionType.PAYMENT_PROVIDER_DOES_NOT_EXIST.getMessage(),
					PaymentExceptionType.PAYMENT_PROVIDER_DOES_NOT_EXIST);
		}
		return strategy.get().captureOrder(provider.get(), abstractOrderModel);
	}

	@Override
	public Optional<PaymentResponseData> captureOrder(final ConsignmentModel consignmentModel) throws PaymentException
	{
		Preconditions.checkArgument(consignmentModel != null, CONSIGNMENT_MODEL_MUST_NOT_BE_NULL);

		Preconditions.checkArgument(consignmentModel.getOrder() != null, ORDER_MODEL_MUST_NOT_BE_NULL);

		final Optional<PaymentProviderModel> provider = getProvider(consignmentModel.getOrder());
		Preconditions.checkArgument(provider.isPresent(), PAYMENT_PROVIDER_MODEL_MUST_NOT_BE_NULL);

		final Optional<PaymentStrategy> strategy = getStrategy(provider.get().getClass());
		if (!strategy.isPresent())
		{
			LOG.error(PaymentExceptionType.PAYMENT_PROVIDER_DOES_NOT_EXIST.getMessage());
			throw new PaymentException(PaymentExceptionType.PAYMENT_PROVIDER_DOES_NOT_EXIST.getMessage(),
					PaymentExceptionType.PAYMENT_PROVIDER_DOES_NOT_EXIST);
		}
		return strategy.get().captureOrder(provider.get(), consignmentModel);
	}

	@Override
	public boolean isCaptured(final AbstractOrderModel orderModel)
	{
		Preconditions.checkArgument(orderModel != null, ORDER_MODEL_MUST_NOT_BE_NULL);


		final Optional<PaymentProviderModel> provider = getProvider(orderModel);
		Preconditions.checkArgument(provider.isPresent(), PAYMENT_PROVIDER_MODEL_MUST_NOT_BE_NULL);

		final Optional<PaymentStrategy> strategy = getStrategy(provider.get().getClass());
		if (!strategy.isPresent())
		{
			LOG.error(PaymentExceptionType.PAYMENT_PROVIDER_DOES_NOT_EXIST.getMessage());
			return false;
		}

		return strategy.get().isCaptured(provider.get(), orderModel);
	}


}
