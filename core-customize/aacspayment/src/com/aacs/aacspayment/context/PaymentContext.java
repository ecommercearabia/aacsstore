/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.context;

import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Map;
import java.util.Optional;

import com.aacs.aacspayment.entry.PaymentRequestData;
import com.aacs.aacspayment.entry.PaymentResponseData;
import com.aacs.aacspayment.enums.PaymentResponseStatus;
import com.aacs.aacspayment.exception.PaymentException;
import com.aacs.aacspayment.model.HyperpayPaymentProviderModel;
import com.aacs.aacspayment.model.PaymentProviderModel;


/**
 * The Interface PaymentContext.
 *
 *
 * @author mnasro
 * @author abu-muhasien
 *
 */
public interface PaymentContext
{

	/**
	 * Gets the payment data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order
	 * @return the payment data
	 */
	public Optional<PaymentRequestData> getPaymentData(PaymentProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrderModel) throws PaymentException;

	/**
	 * Gets the payment data.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param abstractOrderModel
	 *           the abstract order
	 * @return the payment data
	 */
	public Optional<PaymentRequestData> getPaymentData(BaseStoreModel baseStoreModel, AbstractOrderModel abstractOrderModel)
			throws PaymentException;

	/**
	 * Gets the payment data by current store.
	 *
	 * @param abstractOrderModel
	 *           the abstract order
	 * @return the payment data by current store
	 */
	public Optional<PaymentRequestData> getPaymentDataByCurrentStore(AbstractOrderModel abstractOrderModel)
			throws PaymentException;

	/**
	 * Gets the payment data by current store.
	 *
	 * @param abstractOrderModel
	 *           the abstract order
	 * @return the payment data by current store
	 */
	public Optional<PaymentRequestData> getPaymentData(AbstractOrderModel abstractOrderModel) throws PaymentException;

	/**
	 * Gets the response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return the response data
	 */
	public Optional<PaymentResponseData> getResponseData(PaymentProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrderModel, Object data) throws PaymentException;

	/**
	 * Gets the response data.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return the response data
	 */
	public Optional<PaymentResponseData> getResponseData(BaseStoreModel baseStoreModel, AbstractOrderModel abstractOrderModel,
			Object data) throws PaymentException;

	/**
	 * Gets the response data by current store.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return the response data by current store
	 */
	public Optional<PaymentResponseData> getResponseDataByCurrentStore(AbstractOrderModel abstractOrderModel, Object data)
			throws PaymentException;

	/**
	 * Interpret response.
	 *
	 * @param responseParams
	 *           the response params
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @return the optional
	 */
	public Optional<CreateSubscriptionResult> interpretResponse(final Map<String, Object> responseParams,
			PaymentProviderModel paymentProviderModel) throws PaymentException;

	/**
	 * Interpret response.
	 *
	 * @param responseParams
	 *           the response params
	 * @param baseStoreModel
	 *           the base store model
	 * @return the optional
	 */
	public Optional<CreateSubscriptionResult> interpretResponse(final Map<String, Object> responseParams,
			BaseStoreModel baseStoreModel) throws PaymentException;

	/**
	 * Interpret response by current store.
	 *
	 * @param responseParams
	 *           the response params
	 * @return the optional
	 */
	public Optional<CreateSubscriptionResult> interpretResponseByCurrentStore(final Map<String, Object> responseParams)
			throws PaymentException;

	public Optional<CreateSubscriptionResult> interpretResponse(final AbstractOrderModel order);

	public Optional<CreateSubscriptionResult> interpretResponseUsingWebhook(final Map<String, Object> orderInfoMap,
			final AbstractOrderModel order);

	/**
	 * Checks if is successful paid order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return true, if is successful paid order
	 */
	public boolean isSuccessfulPaidOrder(PaymentProviderModel paymentProviderModel, AbstractOrderModel abstractOrderModel,
			Object data) throws PaymentException;

	/**
	 * Checks if is successful paid order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return true, if is successful paid order
	 */
	public boolean isSuccessfulPaidOrder(BaseStoreModel paymentProviderModel, AbstractOrderModel abstractOrderModel, Object data)
			throws PaymentException;

	/**
	 * Checks if is successful paid order by current store.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return true, if is successful paid order by current store
	 */
	public boolean isSuccessfulPaidOrderByCurrentStore(AbstractOrderModel abstractOrderModel, Object data) throws PaymentException;

	/**
	 * Gets the payment order status response data by current store.
	 *
	 * @param data
	 *           the data
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the payment order status response data by current store
	 * @throws PaymentException
	 *            the payment exception
	 */
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseDataByCurrentStore(Object data,
			final AbstractOrderModel abstractOrderModel) throws PaymentException;

	/**
	 * Gets the payment order status response data.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return the payment order status response data
	 * @throws PaymentException
	 *            the payment exception
	 */
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseData(BaseStoreModel baseStoreModel,
			AbstractOrderModel abstractOrderModel, Object data) throws PaymentException;

	/**
	 * Gets the payment order status response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return the payment order status response data
	 * @throws PaymentException
	 *            the payment exception
	 */
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseData(PaymentProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrderModel, Object data) throws PaymentException;



	/**
	 * Gets the payment order confirmed response data.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the payment order confirmed response data
	 * @throws PaymentException
	 *            the payment exception
	 */
	public Optional<PaymentResponseData> getPaymentOrderConfirmedResponseData(BaseStoreModel baseStoreModel,
			AbstractOrderModel abstractOrderModel) throws PaymentException;

	/**
	 * Gets the payment order confirmed response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the payment order confirmed response data
	 * @throws PaymentException
	 *            the payment exception
	 */
	public Optional<PaymentResponseData> getPaymentOrderConfirmedResponseData(PaymentProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrderModel) throws PaymentException;




	/**
	 * Gets the payment order cancel response data.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the payment order cancel response data
	 * @throws PaymentException
	 *            the payment exception
	 */
	public Optional<PaymentResponseData> getPaymentOrderCancelResponseData(BaseStoreModel baseStoreModel,
			AbstractOrderModel abstractOrderModel) throws PaymentException;

	/**
	 * Gets the payment order cancel response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the payment order cancel response data
	 * @throws PaymentException
	 *            the payment exception
	 */
	public Optional<PaymentResponseData> getPaymentOrderCancelResponseData(PaymentProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrderModel) throws PaymentException;



	/**
	 * Gets the payment order refund response data.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the payment order refund response data
	 * @throws PaymentException
	 *            the payment exception
	 */
	public Optional<PaymentResponseData> getPaymentOrderRefundResponseData(BaseStoreModel baseStoreModel,
			AbstractOrderModel abstractOrderModel) throws PaymentException;

	/**
	 * Gets the payment order refund response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the payment order refund response data
	 * @throws PaymentException
	 *            the payment exception
	 */
	public Optional<PaymentResponseData> getPaymentOrderRefundResponseData(PaymentProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrderModel) throws PaymentException;

	public Optional<PaymentResponseData> initiate3DSecureCheckByCurrentStore(AbstractOrderModel abstractOrderModel, Object data)
			throws PaymentException;

	public Optional<PaymentResponseData> initiate3DSecureCheck(final PaymentProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrderModel, Object data) throws PaymentException;

	public Optional<PaymentResponseData> authenticate3DSecurePayerByCurrentStore(AbstractOrderModel abstractOrderModel,
			Object data, String redirectUrl) throws PaymentException;

	public Optional<PaymentResponseData> authenticate3DSecurePayer(final PaymentProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrderModel, Object data, final String redirectUrl) throws PaymentException;

	public Optional<PaymentResponseData> payOrderByCurrentStore(AbstractOrderModel abstractOrderModel, Object sessionId,
			Object transactionId, String threeDSResponse) throws PaymentException;

	public Optional<PaymentResponseData> payOrder(final PaymentProviderModel paymentProviderModel,
			AbstractOrderModel abstractOrderModel, Object sessionId, Object transactionId, String threeDSResponse)
			throws PaymentException;

	public Optional<PaymentResponseData> captureOrder(AbstractOrderModel abstractOrderModel) throws PaymentException;

	public Optional<PaymentResponseData> captureOrder(ConsignmentModel consignmentModel) throws PaymentException;

	public boolean isCaptured(AbstractOrderModel orderModel);

	public boolean isWebHookNotificationPending(HyperpayPaymentProviderModel paymentProvider, Map<String, Object> payload)
			throws PaymentException;

	public PaymentResponseStatus getTransactionStatus(final Map<String, Object> response,
			final AbstractOrderModel abstractorderModel);

	public Map<String, Object> getCheckoutMap(final AbstractOrderModel abstractOrderModel);

	public Optional<PaymentResponseData> getResponseData(final AbstractOrderModel abstractOrderModel, final Object data);

}
