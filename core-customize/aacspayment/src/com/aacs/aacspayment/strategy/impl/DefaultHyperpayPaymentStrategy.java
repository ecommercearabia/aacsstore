/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.strategy.impl;

import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.util.CollectionUtils;

import com.aacs.aacspayment.entry.PaymentRequestData;
import com.aacs.aacspayment.entry.PaymentResponseData;
import com.aacs.aacspayment.enums.PaymentResponseStatus;
import com.aacs.aacspayment.exception.PaymentException;
import com.aacs.aacspayment.exception.type.PaymentExceptionType;
import com.aacs.aacspayment.hyperpay.enums.ShippingMethod;
import com.aacs.aacspayment.hyperpay.enums.TestMode;
import com.aacs.aacspayment.hyperpay.enums.TransactionStatus;
import com.aacs.aacspayment.hyperpay.exception.HyperpayPaymentException;
import com.aacs.aacspayment.hyperpay.model.HyperpayPaymentData;
import com.aacs.aacspayment.hyperpay.response.ResultCode;
import com.aacs.aacspayment.hyperpay.service.HyperpayService;
import com.aacs.aacspayment.model.HyperpayPaymentProviderModel;
import com.aacs.aacspayment.model.PaymentProviderModel;
import com.aacs.aacspayment.strategy.CustomPaymentTransactionStrategy;
import com.aacs.aacspayment.strategy.PaymentStrategy;
import com.google.common.base.Preconditions;


/**
 * The Class DefaultHyperpayPaymentStrategy.
 *
 * @author mnasro
 * @author abu-muhasien
 *
 *         The Class DefaultHyperpayPaymentStrategy.
 */
public class DefaultHyperpayPaymentStrategy implements PaymentStrategy
{

	/** The Constant PAYMENT_ID. */
	private static final String PAYMENT_ID = "Payment Id= [";

	/** The Constant CITY_STATE. */
	private static final String CITY_STATE = "Riyadh";

	/** The Constant STREET_1. */
	private static final String STREET_1 = "Prince Sultan Road - Al Muhammadiyah District 5";

	/** The Constant POSTCODE. */
	private static final String POSTCODE = "11564";

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultHyperpayPaymentStrategy.class);
	/** The Constant THE_PROVIDER_MODER_IS_NOT_A_CC_AVENUE_PAYMENT_PROVIDER_MODEL. */
	private static final String THE_PROVIDER_MODER_IS_NOT_A_CC_AVENUE_PAYMENT_PROVIDER_MODEL = "the provider moder is not a HyperpayPaymentProviderModel ";

	/** The Constant PAYMENT_PROVIDER_MUSTN_T_BE_NULL. */
	private static final String PAYMENT_PROVIDER_MUSTN_T_BE_NULL = "paymentProviderModel mustn't be null";

	/** The Constant ABSTRACTORDER_MUSTN_T_BE_NULL. */
	private static final String ABSTRACTORDER_MUSTN_T_BE_NULL = "abstractOrder mustn't be null";
	/** The Constant RESPONSE_PARAMS_MUSTN_T_BE_NULL. */
	private static final String RESPONSE_PARAMS_MUSTN_T_BE_NULL = "responseParams mustn't be null";

	/** The Constant DATA_CAN_NOT_BE_NULL. */
	private static final String DATA_CAN_NOT_BE_NULL = "data mustn't be null";

	/** The Constant ID_CAN_NOT_BE_NULL. */
	private static final String ID_CAN_NOT_BE_NULL = "id mustn't be null";

	private static final String CODE = "code";
	private static final String RESULT = "result";

	/** The common I 18 N service. */
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;
	/** The payment provider service. */
	@Resource(name = "hyperpayService")
	private HyperpayService hyperpayService;

	/** The cart service. */
	@Resource(name = "cartService")
	private CartService cartService;

	/** The model service. */
	@Resource(name = "modelService")
	private ModelService modelService;

	/** The create subscription result converter. */
	@Resource(name = "hyperpayCreateSubscriptionResultConverter")
	private Converter<Map<String, Object>, CreateSubscriptionResult> createSubscriptionResultConverter;

	@Resource(name = "hyperpayCreateSubscriptionResultForAbstractOrderConverter")
	private Converter<AbstractOrderModel, CreateSubscriptionResult> createSubscriptionResultAbstractOrderConverter;

	/** The payment transaction strategy. */
	@Resource(name = "paymentTransactionStrategy")
	private CustomPaymentTransactionStrategy paymentTransactionStrategy;

	private static final String ORDER_MUSTN_T_BE_NULL = "order mustn't be null";

	/** The key generator. */
	@Resource(name = "orderCodeGenerator")
	private KeyGenerator keyGenerator;

	/**
	 * Gets the creates the subscription result converter.
	 *
	 * @return the createSubscriptionResultConverter
	 */
	protected Converter<Map<String, Object>, CreateSubscriptionResult> getCreateSubscriptionResultConverter()
	{
		return createSubscriptionResultConverter;
	}

	/**
	 * Builds the payment request data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrder
	 *           the abstract order
	 * @return the optional
	 */
	@Override
	public Optional<PaymentRequestData> buildPaymentRequestData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrder)
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrder != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(paymentProviderModel instanceof HyperpayPaymentProviderModel, ABSTRACTORDER_MUSTN_T_BE_NULL);

		try
		{
			abstractOrder.setOrderCode(generateOrderCode());
			modelService.save(abstractOrder);

			final HyperpayPaymentProviderModel hyperpayPaymentProviderModel = (HyperpayPaymentProviderModel) paymentProviderModel;

			final HyperpayPaymentData requestData = getRequestData(paymentProviderModel, abstractOrder);

			final Map<String, Object> prepareCheckout = hyperpayService.prepareCheckout(
					hyperpayPaymentProviderModel.getAccessToken(), hyperpayPaymentProviderModel.getEntityId(),
					hyperpayPaymentProviderModel.getCheckoutsURL(), requestData);
			if (prepareCheckout != null && prepareCheckout.get("id") != null)
			{
				final String id = (String) prepareCheckout.get("id");
				modelService.refresh(abstractOrder);
				abstractOrder.setRequestPaymentBody(requestData.toString());
				abstractOrder.setResponsePaymentBody(prepareCheckout.toString());
				modelService.save(abstractOrder);
				final PaymentRequestData paymentRequestData = new PaymentRequestData(hyperpayPaymentProviderModel.getSrcForm() + id,
						HyperpayPaymentProviderModel._TYPECODE, hyperpayPaymentProviderModel, null);
				return Optional.ofNullable(paymentRequestData);

			}
		}
		catch (final PaymentException e)
		{
			return Optional.empty();
		}

		return Optional.empty();
	}

	/**
	 * Generate order code.
	 *
	 * @return the string
	 */
	protected String generateOrderCode()
	{
		final Object generatedValue = keyGenerator.generate();
		if (generatedValue instanceof String)
		{
			return (String) generatedValue;
		}
		else
		{
			return String.valueOf(generatedValue);
		}
	}


	/**
	 * Interpret response.
	 *
	 * @param responseParams
	 *           the response params
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @return the optional
	 */
	@Override
	public Optional<CreateSubscriptionResult> interpretResponse(final Map<String, Object> responseParams,
			final PaymentProviderModel paymentProviderModel)
	{
		Preconditions.checkArgument(responseParams != null, RESPONSE_PARAMS_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		final CreateSubscriptionResult createSubscriptionResult = getCreateSubscriptionResultConverter().convert(responseParams);
		return createSubscriptionResult == null ? Optional.empty() : Optional.ofNullable(createSubscriptionResult);
	}


	@Override
	public Optional<CreateSubscriptionResult> interpretResponse(final AbstractOrderModel order,
			final PaymentProviderModel paymentProviderModel)
	{
		Preconditions.checkArgument(order != null, ORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		final CreateSubscriptionResult createSubscriptionResult = getCreateSubscriptionResultAbstractOrderConverter()
				.convert(order);
		return createSubscriptionResult == null ? Optional.empty() : Optional.ofNullable(createSubscriptionResult);
	}

	/**
	 * Builds the payment response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrder
	 *           the abstract order
	 * @param data
	 *           the data
	 * @return the optional
	 */
	@Override
	public Optional<PaymentResponseData> buildPaymentResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrder, final Object data)
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrder != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(data != null, DATA_CAN_NOT_BE_NULL);
		final String id = (String) data;

		final HyperpayPaymentProviderModel hyperpayPaymentProviderModel = (HyperpayPaymentProviderModel) paymentProviderModel;
		Map<String, Object> checkoutStatus = null;
		try
		{
			checkoutStatus = hyperpayService.getCheckoutStatus(hyperpayPaymentProviderModel.getAccessToken(),
					hyperpayPaymentProviderModel.getEntityId(), hyperpayPaymentProviderModel.getCheckoutsURL(), id);
		}
		catch (final HyperpayPaymentException e)
		{
			final String requestPaymentBody = PAYMENT_ID + id + "]";
			final String responsePaymentBody = String.valueOf(checkoutStatus);
			paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrder, abstractOrder.getPaymentReferenceId(),
					PaymentTransactionType.REVIEW_DECISION, de.hybris.platform.payment.dto.TransactionStatus.ERROR, e.getMessage(),
					requestPaymentBody, responsePaymentBody, null);
			return Optional.empty();
		}
		if (!CollectionUtils.isEmpty(checkoutStatus) && checkoutStatus.get("id") != null)
		{
			abstractOrder.setPaymentReferenceId((String) checkoutStatus.get("id"));
			modelService.save(abstractOrder);
			final PaymentResponseStatus paymentResponseStatus = getResponseStatus(hyperpayPaymentProviderModel.getAccessToken(),
					hyperpayPaymentProviderModel.getEntityId(), hyperpayPaymentProviderModel.getQueryURL(),
					abstractOrder.getPaymentReferenceId());

			final de.hybris.platform.payment.dto.TransactionStatus transactionStatus = paymentResponseStatus == null
					|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus)
							? de.hybris.platform.payment.dto.TransactionStatus.REJECTED
							: de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED;

			final TransactionStatusDetails transactionStatusDetails = paymentResponseStatus == null
					|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus) ? TransactionStatusDetails.INVALID_REQUEST
							: TransactionStatusDetails.REVIEW_NEEDED;
			final String requestPaymentBody = PAYMENT_ID + abstractOrder.getPaymentReferenceId() + "]";
			final String responsePaymentBody = checkoutStatus.toString();
			paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrder, abstractOrder.getPaymentReferenceId(),
					PaymentTransactionType.REVIEW_DECISION, transactionStatus, transactionStatusDetails, requestPaymentBody,
					responsePaymentBody, null);

			modelService.refresh(abstractOrder);
			abstractOrder.setResponsePaymentBody(checkoutStatus.toString());
			modelService.save(abstractOrder);
			final PaymentResponseData paymentResponseData = new PaymentResponseData(checkoutStatus,
					HyperpayPaymentProviderModel._TYPECODE, null);
			return Optional.ofNullable(paymentResponseData);
		}
		else
		{
			final String requestPaymentBody = PAYMENT_ID + id + "]";
			final String responsePaymentBody = String.valueOf(checkoutStatus);
			LOG.error("Response is: " + responsePaymentBody);
			paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrder, abstractOrder.getPaymentReferenceId(),
					PaymentTransactionType.REVIEW_DECISION, de.hybris.platform.payment.dto.TransactionStatus.ERROR,
					"checkoutStatus Map is null", requestPaymentBody, responsePaymentBody, null);

			return Optional.empty();
		}
	}

	/**
	 * To digits.
	 *
	 * @param d
	 *           the d
	 * @param currency
	 *           the currency
	 * @return the string
	 */
	protected String toDigits(final double d, final CurrencyModel currency)
	{
		final int digits = currency.getDisplayDigits();
		return d + digits + "f";
	}

	/**
	 * Round.
	 *
	 * @param num
	 *           the num
	 * @param currency
	 *           the currency
	 * @return the string
	 */
	protected String round(final double num, final CurrencyModel currency)
	{
		if (currency == null)
		{
			return "" + num;
		}
		final int digits = currency.getDisplayDigits();
		BigDecimal decimal = BigDecimal.valueOf(num);
		decimal = decimal.setScale(digits, RoundingMode.HALF_UP);
		return decimal.toString();
	}

	/**
	 * Gets the response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the order
	 * @return the response data
	 */
	protected HyperpayPaymentData getRequestData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel)
	{
		Preconditions.checkArgument(paymentProviderModel instanceof HyperpayPaymentProviderModel,
				THE_PROVIDER_MODER_IS_NOT_A_CC_AVENUE_PAYMENT_PROVIDER_MODEL);
		Preconditions.checkArgument(commonI18NService.getCurrentLanguage() != null, "CurrentLanguage is null");

		final HyperpayPaymentProviderModel providerModel = (HyperpayPaymentProviderModel) paymentProviderModel;
		final AddressModel paymentAddress = abstractOrderModel.getPaymentAddress();
		final AddressModel deliveryAddress = abstractOrderModel.getDeliveryAddress();
		final HyperpayPaymentData data = new HyperpayPaymentData();

		final Double totalPrice = getTotalPrice(abstractOrderModel);
		final Double taxAmount = getTaxAmountPrice(abstractOrderModel);
		if (providerModel.isRound())
		{
			data.setAmount(round(totalPrice.doubleValue(), providerModel.getCurrency()));
			data.setTaxAmount(round(taxAmount.doubleValue(), providerModel.getCurrency()));
		}
		else
		{
			data.setAmount((totalPrice == null ? null : toDigits(totalPrice.doubleValue(), providerModel.getCurrency())));
			data.setTaxAmount((taxAmount == null ? null : toDigits(taxAmount.doubleValue(), providerModel.getCurrency())));
		}
		data.setMerchantTransactionId(abstractOrderModel.getOrderCode());
		final CustomerModel customerModel = (de.hybris.platform.core.model.user.CustomerModel) abstractOrderModel.getUser();
		data.setEntityId(providerModel.getEntityId());
		data.setCurrency(providerModel.getCurrency().getIsocode());
		data.setPaymentType(providerModel.getPaymentType());
		final boolean round = providerModel.isRound();
		if (round)
		{
			data.setTestMode(TestMode.INTERNAL.toString().equalsIgnoreCase(providerModel.getTestMode()) ? TestMode.INTERNAL
					: TestMode.EXTERNAL);
		}
		data.setCustomerMerchantCustomerId(customerModel.getCustomerID());
		data.setCustomerGivenName(
				StringUtils.isNotBlank(customerModel.getDisplayName()) ? customerModel.getDisplayName().toUpperCase().charAt(0) + ""
						: null);
		data.setCustomerSurname(
				StringUtils.isNotBlank(customerModel.getDisplayName()) && customerModel.getDisplayName().length() >= 2
						&& customerModel.getDisplayName().charAt(1) != ' ' ? customerModel.getDisplayName().toUpperCase().charAt(1) + ""
								: customerModel.getDisplayName().toUpperCase().charAt(0) + "");

		data.setCustomerPhone(customerModel.getMobileNumber());
		data.setCustomerMobile(customerModel.getMobileNumber());
		data.setCustomerWorkPhone(customerModel.getMobileNumber());
		data.setCustomerEmail(customerModel.getContactEmail());

		if (paymentAddress != null)
		{
			data.setBillingPostCode(POSTCODE);
			data.setBillingCountry(paymentAddress.getCountry().getIsocode());
			data.setBillingStreet1(STREET_1);
			data.setBillingStreet2(paymentAddress.getLine2());
			data.setBillingCity(CITY_STATE);
			data.setBillingState(CITY_STATE);
		}
		if (deliveryAddress != null)
		{
			data.setShippingStreet1(deliveryAddress.getLine1());
			data.setShippingStreet2(deliveryAddress.getLine1());
			data.setShippingPostCode("00000");
			data.setShippingCountry(deliveryAddress.getCountry().getIsocode());
			data.setShippingMethod(ShippingMethod.OTHER);
			if (providerModel.isRound())
			{
				data.setShippingCost(round(abstractOrderModel.getDeliveryCost(), providerModel.getCurrency()));
			}
			else
			{
				data.setShippingCost((abstractOrderModel.getDeliveryCost() == null ? null
						: toDigits(abstractOrderModel.getDeliveryCost().doubleValue(), providerModel.getCurrency())));
			}
		}
		return data;
	}

	/**
	 * Gets the total price.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the total price
	 */
	protected Double getTotalPrice(final AbstractOrderModel abstractOrderModel)
	{
		final CurrencyModel cartCurrency = abstractOrderModel.getCurrency();
		final CurrencyModel baseCurrency = commonI18NService.getBaseCurrency();
		Double amount = null;
		if (baseCurrency.getIsocode().equalsIgnoreCase(cartCurrency.getIsocode()))
		{
			amount = abstractOrderModel.getTotalPrice();
		}
		else
		{
			amount = commonI18NService.convertAndRoundCurrency(cartCurrency.getConversion().doubleValue(),
					baseCurrency.getConversion().doubleValue(), cartCurrency.getDisplayDigits(), abstractOrderModel.getTotalPrice());
		}
		return amount;
	}

	/**
	 * Gets the total price.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the total price
	 */
	protected Double getTaxAmountPrice(final AbstractOrderModel abstractOrderModel)
	{
		final CurrencyModel cartCurrency = abstractOrderModel.getCurrency();
		final CurrencyModel baseCurrency = commonI18NService.getBaseCurrency();
		Double amount = null;
		if (baseCurrency.getIsocode().equalsIgnoreCase(cartCurrency.getIsocode()))
		{
			amount = abstractOrderModel.getTotalTax();
		}
		else
		{
			amount = commonI18NService.convertAndRoundCurrency(cartCurrency.getConversion().doubleValue(),
					baseCurrency.getConversion().doubleValue(), cartCurrency.getDisplayDigits(), abstractOrderModel.getTotalPrice());
		}
		return amount;
	}



	/**
	 * Checks if is successful paid order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param data
	 *           the data
	 * @return true, if is successful paid order
	 */
	@Override
	public boolean isSuccessfulPaidOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data)
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		final HyperpayPaymentProviderModel hyperpayPaymentProviderModel = (HyperpayPaymentProviderModel) paymentProviderModel;
		final String id = (String) data;
		final PaymentResponseStatus responseStatus = getResponseStatus(hyperpayPaymentProviderModel.getAccessToken(),
				hyperpayPaymentProviderModel.getEntityId(), hyperpayPaymentProviderModel.getQueryURL(), id);

		return PaymentResponseStatus.SUCCESS.equals(responseStatus);
	}

	/**
	 * Gets the payment order status response data.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrder
	 *           the abstract order
	 * @param data
	 *           the data
	 * @return the payment order status response data
	 * @throws PaymentException
	 *            the payment exception
	 */
	@Override
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrder, final Object data) throws PaymentException
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrder != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		final String id = data == null ? abstractOrder.getPaymentReferenceId() : (String) data;
		Preconditions.checkArgument(id != null, ID_CAN_NOT_BE_NULL);

		final HyperpayPaymentProviderModel hyperpayPaymentProviderModel = (HyperpayPaymentProviderModel) paymentProviderModel;
		Map<String, Object> checkoutStatus = null;
		try
		{
			checkoutStatus = hyperpayService.searchForTransactionById(hyperpayPaymentProviderModel.getAccessToken(),
					hyperpayPaymentProviderModel.getEntityId(), hyperpayPaymentProviderModel.getQueryURL(), id);
		}
		catch (final HyperpayPaymentException e)
		{
			final String requestPaymentBody = PAYMENT_ID + id + "]";
			final String responsePaymentBody = String.valueOf(checkoutStatus);
			paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrder, abstractOrder.getPaymentReferenceId(),
					PaymentTransactionType.REVIEW_DECISION, de.hybris.platform.payment.dto.TransactionStatus.ERROR, e.getMessage(),
					requestPaymentBody, responsePaymentBody, null);
			return Optional.empty();
		}
		if (!CollectionUtils.isEmpty(checkoutStatus) && checkoutStatus.get("id") != null)
		{
			abstractOrder.setPaymentReferenceId((String) checkoutStatus.get("id"));
			modelService.save(abstractOrder);
			final PaymentResponseStatus paymentResponseStatus = getResponseStatus(hyperpayPaymentProviderModel.getAccessToken(),
					hyperpayPaymentProviderModel.getEntityId(), hyperpayPaymentProviderModel.getQueryURL(),
					abstractOrder.getPaymentReferenceId());

			final de.hybris.platform.payment.dto.TransactionStatus transactionStatus = paymentResponseStatus == null
					|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus)
							? de.hybris.platform.payment.dto.TransactionStatus.REJECTED
							: de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED;

			final TransactionStatusDetails transactionStatusDetails = paymentResponseStatus == null
					|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus) ? TransactionStatusDetails.INVALID_REQUEST
							: TransactionStatusDetails.REVIEW_NEEDED;
			final String requestPaymentBody = PAYMENT_ID + abstractOrder.getPaymentReferenceId() + "]";
			final String responsePaymentBody = checkoutStatus.toString();
			paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrder, abstractOrder.getPaymentReferenceId(),
					PaymentTransactionType.REVIEW_DECISION, transactionStatus, transactionStatusDetails, requestPaymentBody,
					responsePaymentBody, null);

			modelService.refresh(abstractOrder);
			abstractOrder.setResponsePaymentBody(checkoutStatus.toString());
			modelService.save(abstractOrder);
			final PaymentResponseData paymentResponseData = new PaymentResponseData(checkoutStatus,
					HyperpayPaymentProviderModel._TYPECODE, null);
			return Optional.ofNullable(paymentResponseData);
		}
		else
		{
			final String requestPaymentBody = PAYMENT_ID + id + "]";
			final String responsePaymentBody = String.valueOf(checkoutStatus);
			paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrder, abstractOrder.getPaymentReferenceId(),
					PaymentTransactionType.REVIEW_DECISION, de.hybris.platform.payment.dto.TransactionStatus.ERROR,
					"checkoutStatus Map is null", requestPaymentBody, responsePaymentBody, null);

			return Optional.empty();
		}
	}

	/**
	 * Capture order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the optional
	 * @throws PaymentException
	 *            the payment exception
	 */
	@Override
	public Optional<PaymentResponseData> captureOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{

		return Optional.empty();
	}

	/**
	 * Cancel order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the optional
	 * @throws PaymentException
	 *            the payment exception
	 */
	@Override
	public Optional<PaymentResponseData> cancelOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{

		return Optional.empty();
	}

	/**
	 * Gets the response status.
	 *
	 * @param accessToken
	 *           the access token
	 * @param entityId
	 *           the entity id
	 * @param queryURL
	 *           the query URL
	 * @param paymentId
	 *           the payment id
	 * @return the response status
	 */
	private PaymentResponseStatus getResponseStatus(final String accessToken, final String entityId, final String queryURL,
			final String paymentId)
	{
		return TransactionStatus.SUCCESS.equals(hyperpayService.getTransactionStatus(accessToken, entityId, queryURL, paymentId))
				? PaymentResponseStatus.SUCCESS
				: PaymentResponseStatus.FAILURE;
	}


	/**
	 * Refund order.
	 *
	 * @param paymentProviderModel
	 *           the payment provider model
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the optional
	 * @throws PaymentException
	 *            the payment exception
	 */
	@Override
	public Optional<PaymentResponseData> refundOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{
		return Optional.empty();
	}

	@Override
	public Optional<PaymentResponseData> initiate3DSecureCheck(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data) throws PaymentException
	{
		throw new PaymentException("3DSecure check is not supported by Hyperpay", null);
	}

	@Override
	public Optional<PaymentResponseData> payOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object sessionId, final Object authenticatedTransactionId,
			final String threeDSResponse) throws PaymentException
	{
		return Optional.empty();
	}

	@Override
	public Optional<PaymentResponseData> authenticate3DSecurePayer(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data, final String redirectUrl) throws PaymentException
	{
		throw new PaymentException("3DSecure check is not supported by Hyperpay", null);
	}

	@Override
	public boolean isWebHookNotificationPending(final Map<String, Object> payload) throws PaymentException
	{
		Preconditions.checkArgument(payload != null, "Payload Must not be null");
		final String resultString = RESULT;
		final String codeString = CODE;


		final Map<String, Object> result = (Map<String, Object>) payload.getOrDefault(resultString, null);
		if (result == null)
		{
			throw new HyperpayPaymentException(PaymentExceptionType.BAD_REQUEST, "Pay Load does not contain 'result' object");
		}

		final String code = (String) result.getOrDefault(codeString, Strings.EMPTY);

		return ResultCode.TRANSACTION_PENDING.equals(ResultCode.getResultByCode(code));
	}

	private PaymentResponseStatus isSuccessfullPaymnetOrder(final Map<String, Object> response)
	{
		final TransactionStatus transactionStatus = hyperpayService.getTransactionStatus(response);

		return TransactionStatus.SUCCESS.equals(transactionStatus) ? PaymentResponseStatus.SUCCESS : PaymentResponseStatus.FAILURE;
	}

	@Override
	public PaymentResponseStatus getTransactionStatus(final Map<String, Object> response)
	{
		return isSuccessfullPaymnetOrder(response);
	}

	/**
	 * @return the createSubscriptionResultAbstractOrderConverter
	 */
	public Converter<AbstractOrderModel, CreateSubscriptionResult> getCreateSubscriptionResultAbstractOrderConverter()
	{
		return createSubscriptionResultAbstractOrderConverter;
	}

	@Override
	public Optional<PaymentResponseData> captureOrder(final PaymentProviderModel paymentProviderModel,
			final ConsignmentModel consignmentModel) throws PaymentException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isCaptured(final PaymentProviderModel paymentProviderModel, final AbstractOrderModel abstractOrderModel)
	{
		throw new UnsupportedOperationException();
	}


}
