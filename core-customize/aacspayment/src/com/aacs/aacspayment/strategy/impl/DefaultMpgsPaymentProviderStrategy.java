/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.strategy.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.aacs.aacspayment.model.MpgsPaymentProviderModel;
import com.aacs.aacspayment.model.PaymentProviderModel;
import com.aacs.aacspayment.service.PaymentProviderService;
import com.aacs.aacspayment.strategy.PaymentProviderStrategy;


/**
 * @author husam.dababneh@erabia.com
 *
 *         The Class DefaultMpgsPaymentProviderStrategy.
 */
public class DefaultMpgsPaymentProviderStrategy implements PaymentProviderStrategy
{

	/** The payment provider service. */
	@Resource(name = "paymentProviderService")
	private PaymentProviderService paymentProviderService;

	/**
	 * Gets the payment provider service.
	 *
	 * @return the payment provider service
	 */
	protected PaymentProviderService getPaymentProviderService()
	{
		return paymentProviderService;
	}

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreUid
	 *           the base store uid
	 * @return the active provider
	 */
	@Override
	public Optional<PaymentProviderModel> getActiveProvider(final String baseStoreUid)
	{
		return getPaymentProviderService().getActive(baseStoreUid, MpgsPaymentProviderModel.class);
	}

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @return the active provider
	 */
	@Override
	public Optional<PaymentProviderModel> getActiveProvider(final BaseStoreModel baseStoreModel)
	{
		return getPaymentProviderService().getActive(baseStoreModel, MpgsPaymentProviderModel.class);
	}

	/**
	 * Gets the active provider by current base store.
	 *
	 * @return the active provider by current base store
	 */
	@Override
	public Optional<PaymentProviderModel> getActiveProviderByCurrentBaseStore()
	{
		return getPaymentProviderService().getActiveProviderByCurrentBaseStore(MpgsPaymentProviderModel.class);
	}
}

