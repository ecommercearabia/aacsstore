/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.populator.hyperpay;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.enums.DecisionsEnum;

import java.util.Map;

import javax.annotation.Resource;

import com.aacs.aacspayment.hyperpay.HyperpayStatusReason;
import com.aacs.aacspayment.hyperpay.enums.TransactionStatus;
import com.aacs.aacspayment.hyperpay.service.HyperpayService;


/**
 * @author amjad.shati@erabia.com
 */
public class CreateSubscriptionResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{
	@Resource(name = "hyperpayService")
	private HyperpayService hyperpayService;
	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(source, "Parameter source (Map<String, String>) cannot be null");
		validateParameterNotNull(target, "Parameter target (CreateSubscriptionResult) cannot be null");

		final TransactionStatus transactionStatus = hyperpayService.getTransactionStatus(source);

		if (TransactionStatus.SUCCESS.equals(transactionStatus))
		{
			target.setDecision(DecisionsEnum.ACCEPT.toString());

		}
		else
		{
			target.setDecision(DecisionsEnum.ERROR.toString());

		}
		target.setDecisionPublicSignature(getResultCode(source));
		target.setTransactionSignatureVerified(null);
		target.setReasonCode(1);
		target.setRequestId(String.valueOf(source.get(HyperpayStatusReason.ID.getKey())));

	}

	private String getResultCode(final Map<String, Object> source)
	{
		final Object object = source.get(HyperpayStatusReason.RESULT.getKey());
		if (object == null || !(object instanceof Map)
				|| ((Map<String, Object>) object).get(HyperpayStatusReason.RESULT_CODE.getKey()) == null)
		{
			return null;
		}
		final Map<String, Object> cardMap = (Map<String, Object>) object;

		return String.valueOf(cardMap.get(HyperpayStatusReason.RESULT_CODE.getKey()));

	}
}

