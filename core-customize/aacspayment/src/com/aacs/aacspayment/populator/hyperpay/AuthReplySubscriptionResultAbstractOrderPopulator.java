/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.populator.hyperpay;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.Map;

import javax.annotation.Resource;

import com.aacs.aacspayment.context.PaymentContext;


/**
 * @author mnasro
 */
public class AuthReplySubscriptionResultAbstractOrderPopulator
		extends AbstractResultPopulator<AbstractOrderModel, CreateSubscriptionResult>
{
	@Resource(name = "hyperpayAuthReplyResultPopulator")
	private Populator<Map<String, Object>, CreateSubscriptionResult> hyperpayAuthReplyResultPopulator;

	@Resource(name = "paymentContext")
	private PaymentContext paymentContext;

	@Override
	public void populate(final AbstractOrderModel source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(source, "Parameter [AbstractOrderModel] source cannot be null");
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");

		getHyperpayAuthReplyResultPopulator().populate(getPaymentContext().getCheckoutMap(source), target);
	}

	/**
	 * @return the hyperpayAuthReplyResultPopulator
	 */
	protected Populator<Map<String, Object>, CreateSubscriptionResult> getHyperpayAuthReplyResultPopulator()
	{
		return hyperpayAuthReplyResultPopulator;
	}

	/**
	 * @return the paymentContext
	 */
	protected PaymentContext getPaymentContext()
	{
		return paymentContext;
	}


}