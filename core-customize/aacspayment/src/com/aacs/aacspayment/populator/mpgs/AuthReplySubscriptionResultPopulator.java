/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.populator.mpgs;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.AuthReplyData;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;

import java.math.BigDecimal;
import java.util.Map;

import com.aacs.aacspayment.mpgs.beans.incoming.OuterTransactionResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * @author husam.dababneh@erabia.com
 */
public class AuthReplySubscriptionResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{
	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{

		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");
		validateParameterNotNull(source, "source [Map<String, Object>] source cannot be null");

		final GsonBuilder _builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
		final Gson gson = _builder.setPrettyPrinting().create();
		final OuterTransactionResponse transaction = gson.fromJson(gson.toJson(source), OuterTransactionResponse.class);
		validateParameterNotNull(transaction, "transaction of type OuterTransactionResponse cannot be null");

		final AuthReplyData data = new AuthReplyData();

		final Integer ccAuthReplyReasonCode = 1;
		final String ccAuthReplyAuthorizationCode = transaction.getResponse().getAcquirerMessage();
		final String ccAuthReplyCvCode = null;
		final Boolean cvnDecision = Boolean.TRUE;
		final String ccAuthReplyAvsCodeRaw = "YES";
		final String ccAuthReplyAvsCode = "YES";
		final BigDecimal ccAuthReplyAmount = new BigDecimal(transaction.getTransaction().getAmount());
		final String ccAuthReplyProcessorResponse = null;
		final String ccAuthReplyAuthorizedDateTime = transaction.getTimeOfRecord();


		data.setCcAuthReplyReasonCode(ccAuthReplyReasonCode);
		data.setCcAuthReplyAuthorizationCode(ccAuthReplyAuthorizationCode);
		data.setCcAuthReplyCvCode(ccAuthReplyCvCode);
		data.setCvnDecision(cvnDecision);
		data.setCcAuthReplyAvsCodeRaw(ccAuthReplyAvsCodeRaw);
		data.setCcAuthReplyAvsCode(ccAuthReplyAvsCode);
		data.setCcAuthReplyAmount(ccAuthReplyAmount);
		data.setCcAuthReplyProcessorResponse(ccAuthReplyProcessorResponse);
		data.setCcAuthReplyAuthorizedDateTime(ccAuthReplyAuthorizedDateTime);

		target.setAuthReplyData(data);
	}

}
