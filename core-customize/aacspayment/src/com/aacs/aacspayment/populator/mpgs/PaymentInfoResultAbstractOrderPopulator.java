/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.populator.mpgs;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.cybersource.enums.CardTypeEnum;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import javax.annotation.Resource;

import com.aacs.aacspayment.context.PaymentProviderContext;
import com.aacs.aacspayment.model.MpgsPaymentProviderModel;
import com.aacs.aacspayment.mpgs.beans.incoming.OuterTransactionResponse;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Card;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.SourceOfFunds;
import com.aacs.aacspayment.mpgs.constants.MpgsConstants;
import com.aacs.aacspayment.mpgs.exception.MpgsException;
import com.aacs.aacspayment.mpgs.service.MpgsService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * @author husam.dababneh@erabia.com
 */
public class PaymentInfoResultAbstractOrderPopulator extends AbstractResultPopulator<AbstractOrderModel, CreateSubscriptionResult>
{
	@Resource(name = "mpgsService")
	private MpgsService mpgsService;

	@Resource(name = "paymentProviderContext")
	private PaymentProviderContext paymentProviderContext;

	private MpgsConstants getMpgsConstants(final AbstractOrderModel order)
	{
		final MpgsPaymentProviderModel provider = (MpgsPaymentProviderModel) paymentProviderContext.getProvider(order).orElse(null);
		return new MpgsConstants(provider.getMerchantId(), provider.getApiKey(), provider.getApiVersion(),
				provider.getJavaScriptCodeUrl(), provider.getBaseURL());
	}


	@Override
	public void populate(final AbstractOrderModel order, final CreateSubscriptionResult target)
	{
		OuterTransactionResponse source;
		try
		{
			source = mpgsService.getLastTransaction(order.getPaymentTransactionCode(), getMpgsConstants(order)).orElse(null);
		}
		catch (final MpgsException e)
		{
			throw new IllegalStateException("Could not retrive last transaction");
		}

		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");
		validateParameterNotNull(source, "Parameter [AbstractOrderModel] source cannot be null");

		final Gson gson = new GsonBuilder().create();
		final OuterTransactionResponse transaction = gson.fromJson(gson.toJson(source), OuterTransactionResponse.class);
		validateParameterNotNull(transaction, "transaction of type OuterTransactionResponse cannot be null");
		String cardAccountNumber = null;
		String cardCardType = null;
		Integer cardExpirationMonth = null;
		Integer cardExpirationYear = null;
		String cardAccountHolderName = null;



		final SourceOfFunds sourceOfFunds = transaction.getSourceOfFunds();

		if (sourceOfFunds != null && sourceOfFunds.getProvided() != null && sourceOfFunds.getProvided().getCard() != null)
		{
			final Card card = sourceOfFunds.getProvided().getCard();

			cardAccountNumber = card.getNumber();
			cardCardType = getCardType(card.getBrand());


			cardExpirationMonth = card.getExpiry() != null ? Integer.parseInt(card.getExpiry().getMonth()) : 0;
			cardExpirationYear = card.getExpiry() != null ? Integer.parseInt(card.getExpiry().getYear()) : 0;

			cardAccountHolderName = card.getNameOnCard();
		}

		final String cardCvNumber = null;

		final String cardIssueNumber = "0000";
		final String cardStartMonth = "01";
		final String cardStartYear = "30";
		final String paymentOption = "CARD";


		final PaymentInfoData data = new PaymentInfoData();
		data.setCardAccountNumber(cardAccountNumber);
		data.setCardCardType(cardCardType);
		data.setCardCvNumber(cardCvNumber);
		data.setCardExpirationMonth(cardExpirationMonth);
		data.setCardExpirationYear(cardExpirationYear);
		data.setCardIssueNumber(cardIssueNumber);
		data.setCardStartMonth(cardStartMonth);
		data.setCardStartYear(cardStartYear);
		data.setPaymentOption(paymentOption);
		data.setCardAccountHolderName(cardAccountHolderName);

		target.setPaymentInfoData(data);
	}


	private String getCardType(String brandType)
	{
		if (brandType.toLowerCase().contains("mastercard"))
		{
			brandType = String.valueOf(CardTypeEnum.master.getStringValue());
		}
		else if (brandType.toLowerCase().contains("visa"))
		{
			brandType = String.valueOf(CardTypeEnum.visa.getStringValue());
		}
		else
		{
			brandType = String.valueOf(CardTypeEnum.master.getStringValue());
		}
		return brandType;
	}

}
