/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.populator.mpgs;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.CustomerInfoData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;


/**
 * @author husam.dababneh@erabia.com
 */
public class CustomerInfoResultAbstractOrderPopulator
		extends AbstractResultPopulator<AbstractOrderModel, CreateSubscriptionResult>
{


	@Override
	public void populate(final AbstractOrderModel order, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");

		final CustomerInfoData data = new CustomerInfoData();

		populateBillingAddressData(order, data);
		populateDeliveryAddressData(order, data);
		target.setCustomerInfoData(data);
	}

	/**
	 *
	 */
	private void populateDeliveryAddressData(final AbstractOrderModel order, final CustomerInfoData data)
	{
		final AddressModel deliveryAddress = order.getDeliveryAddress();
		if (deliveryAddress != null)
		{
			data.setShipToCity(deliveryAddress.getCity() == null ? deliveryAddress.getTown() : deliveryAddress.getCity().getName());
			data.setShipToCompany(deliveryAddress.getCompany());
			data.setShipToCountry(deliveryAddress.getCountry() == null ? "" : deliveryAddress.getCountry().getIsocode());
			data.setShipToFirstName(deliveryAddress.getFirstname());
			data.setShipToLastName(deliveryAddress.getLastname());
			data.setShipToPhoneNumber(deliveryAddress.getMobile());
			data.setShipToPostalCode(deliveryAddress.getPostalcode());
			data.setShipToShippingMethod(order.getDeliveryMode() == null ? "" : order.getDeliveryMode().getName());
			data.setShipToState(deliveryAddress.getRegion() == null ? "" : deliveryAddress.getRegion().getIsocode());
			data.setShipToStreet1(deliveryAddress.getLine1());
			data.setShipToStreet2(deliveryAddress.getLine2());
		}

	}

	/**
	 *
	 */
	private void populateBillingAddressData(final AbstractOrderModel order, final CustomerInfoData data)
	{
		AddressModel billingAddress = order.getPaymentAddress();
		if (billingAddress == null)
		{
			billingAddress = order.getDeliveryAddress();
		}
		final String contactEmail = ((CustomerModel) order.getUser()).getContactEmail();

		if (billingAddress != null)
		{
			data.setBillToCity(billingAddress.getCity() == null ? billingAddress.getTown() : billingAddress.getCity().getName());
			data.setBillToCompany(billingAddress.getCompany());
			data.setBillToCountry(billingAddress.getCountry() == null ? "" : billingAddress.getCountry().getIsocode());
			data.setBillToCustomerIdRef(order.getUser().getUid());
			data.setBillToEmail(contactEmail);
			data.setBillToTitleCode(billingAddress.getTitle() == null ? "" : billingAddress.getTitle().getCode());
			data.setBillToFirstName(billingAddress.getFirstname());
			data.setBillToLastName(billingAddress.getLastname());
			data.setBillToPhoneNumber(billingAddress.getMobile());
			data.setBillToPostalCode(billingAddress.getPostalcode());
			data.setBillToState(billingAddress.getRegion() == null ? "" : billingAddress.getRegion().getIsocode());
			data.setBillToStreet1(billingAddress.getLine1());
			data.setBillToStreet2(billingAddress.getLine2());
		}

	}
}
