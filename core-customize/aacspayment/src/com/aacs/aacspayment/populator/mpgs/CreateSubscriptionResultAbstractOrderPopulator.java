/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.populator.mpgs;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.enums.DecisionsEnum;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import javax.annotation.Resource;

import com.aacs.aacspayment.context.PaymentProviderContext;
import com.aacs.aacspayment.hyperpay.enums.TransactionStatus;
import com.aacs.aacspayment.model.MpgsPaymentProviderModel;
import com.aacs.aacspayment.mpgs.beans.incoming.OuterTransactionResponse;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Response;
import com.aacs.aacspayment.mpgs.constants.MpgsConstants;
import com.aacs.aacspayment.mpgs.exception.MpgsException;
import com.aacs.aacspayment.mpgs.service.MpgsService;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * @author husam.dababneh@erabia.com
 */
public class CreateSubscriptionResultAbstractOrderPopulator
		extends AbstractResultPopulator<AbstractOrderModel, CreateSubscriptionResult>
{
	@Resource(name = "mpgsService")
	private MpgsService mpgsService;

	@Resource(name = "paymentProviderContext")
	private PaymentProviderContext paymentProviderContext;

	private MpgsConstants getMpgsConstants(final AbstractOrderModel order)
	{
		final MpgsPaymentProviderModel provider = (MpgsPaymentProviderModel) paymentProviderContext.getProvider(order).orElse(null);
		return new MpgsConstants(provider.getMerchantId(), provider.getApiKey(), provider.getApiVersion(),
				provider.getJavaScriptCodeUrl(), provider.getBaseURL());
	}

	@Override
	public void populate(final AbstractOrderModel order, final CreateSubscriptionResult target)
	{
		OuterTransactionResponse source;
		try
		{
			source = mpgsService.getLastTransaction(order.getPaymentTransactionCode(), getMpgsConstants(order)).orElse(null);
		}
		catch (final MpgsException e)
		{
			throw new IllegalStateException("Could not retrive last transaction");
		}

		validateParameterNotNull(source, "Parameter source (Map<String, String>) cannot be null");
		validateParameterNotNull(target, "Parameter target (CreateSubscriptionResult) cannot be null");

		final Gson gson = new GsonBuilder().create();
		final OuterTransactionResponse transaction = gson.fromJson(gson.toJson(source), OuterTransactionResponse.class);
		validateParameterNotNull(transaction, "transaction of type OuterTransactionResponse cannot be null");

		final Response response = transaction.getResponse();
		validateParameterNotNull(response, "Response cannot be null");

		final String gatewayCode = response.getGatewayCode();

		final TransactionStatus transactionStatus = getPaymentResponseStatus(gatewayCode);

		if (TransactionStatus.SUCCESS.equals(transactionStatus))
		{
			target.setDecision(DecisionsEnum.ACCEPT.toString());

		}
		else
		{
			target.setDecision(DecisionsEnum.ERROR.toString());

		}
		target.setDecisionPublicSignature(transaction.getResponse().getAcquirerMessage());
		target.setTransactionSignatureVerified(null);
		target.setReasonCode(1);
		target.setRequestId(transaction.getTransaction().getId());

	}

	private TransactionStatus getPaymentResponseStatus(final String responseGatewayCode)
	{
		if (Strings.isNullOrEmpty(responseGatewayCode))
		{
			return TransactionStatus.FAILED;
		}

		switch (responseGatewayCode)
		{
			case "APPROVED":
			case "APPROVED_AUTO":
				return TransactionStatus.SUCCESS;
			default:
				return TransactionStatus.FAILED;
		}

	}

}

