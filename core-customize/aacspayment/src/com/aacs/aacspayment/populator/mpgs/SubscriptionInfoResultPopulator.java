/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.populator.mpgs;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.SubscriptionInfoData;

import java.util.Map;

import com.aacs.aacspayment.mpgs.beans.incoming.OuterTransactionResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * @author husam.dababneh@erabia.com
 */
public class SubscriptionInfoResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{
	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(source, "Parameter [Map<String, String>] source cannot be null");
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");
		final GsonBuilder _builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
		final Gson gson = _builder.setPrettyPrinting().create();
		final OuterTransactionResponse transaction = gson.fromJson(gson.toJson(source), OuterTransactionResponse.class);
		validateParameterNotNull(transaction, "transaction of type OuterTransactionResponse cannot be null");


		final SubscriptionInfoData data = new SubscriptionInfoData();
		final String subscriptionID = transaction.getTransaction() != null ? transaction.getTransaction().getId() : null;
		final String subscriptionIDPublicSignature = null;
		final String subscriptionSignedValue = null;
		data.setSubscriptionID(subscriptionID);
		data.setSubscriptionIDPublicSignature(subscriptionIDPublicSignature);
		data.setSubscriptionSignedValue(subscriptionSignedValue);

		target.setSubscriptionInfoData(data);
	}
}
