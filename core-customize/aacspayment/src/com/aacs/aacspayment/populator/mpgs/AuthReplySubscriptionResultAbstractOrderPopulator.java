/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.populator.mpgs;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.AuthReplyData;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.math.BigDecimal;

import javax.annotation.Resource;

import com.aacs.aacspayment.context.PaymentProviderContext;
import com.aacs.aacspayment.model.MpgsPaymentProviderModel;
import com.aacs.aacspayment.mpgs.beans.incoming.OuterTransactionResponse;
import com.aacs.aacspayment.mpgs.constants.MpgsConstants;
import com.aacs.aacspayment.mpgs.exception.MpgsException;
import com.aacs.aacspayment.mpgs.service.MpgsService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * @author husam.dababneh@erabia.com
 */
public class AuthReplySubscriptionResultAbstractOrderPopulator
		extends AbstractResultPopulator<AbstractOrderModel, CreateSubscriptionResult>
{

	@Resource(name = "mpgsService")
	private MpgsService mpgsService;

	@Resource(name = "paymentProviderContext")
	private PaymentProviderContext paymentProviderContext;

	private MpgsConstants getMpgsConstants(final AbstractOrderModel order)
	{
		final MpgsPaymentProviderModel provider = (MpgsPaymentProviderModel) paymentProviderContext.getProvider(order).orElse(null);
		return new MpgsConstants(provider.getMerchantId(), provider.getApiKey(), provider.getApiVersion(),
				provider.getJavaScriptCodeUrl(), provider.getBaseURL());
	}

	@Override
	public void populate(final AbstractOrderModel order, final CreateSubscriptionResult target)
	{
		OuterTransactionResponse source;
		try
		{
			source = mpgsService.getLastTransaction(order.getPaymentTransactionCode(), getMpgsConstants(order)).orElse(null);
		}
		catch (final MpgsException e)
		{
			throw new IllegalStateException("Could not retrive last transaction");
		}

		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");
		validateParameterNotNull(source, "Parameter [AbstractOrderModel] source cannot be null");

		final Gson gson = new GsonBuilder().create();
		final OuterTransactionResponse transaction = gson.fromJson(gson.toJson(source), OuterTransactionResponse.class);
		validateParameterNotNull(transaction, "transaction of type OuterTransactionResponse cannot be null");

		final AuthReplyData data = new AuthReplyData();

		final Integer ccAuthReplyReasonCode = 1;
		final String ccAuthReplyAuthorizationCode = transaction.getResponse().getAcquirerMessage();
		final String ccAuthReplyCvCode = null;
		final Boolean cvnDecision = Boolean.TRUE;
		final String ccAuthReplyAvsCodeRaw = "YES";
		final String ccAuthReplyAvsCode = "YES";
		final BigDecimal ccAuthReplyAmount = new BigDecimal(transaction.getTransaction().getAmount());
		final String ccAuthReplyProcessorResponse = null;
		final String ccAuthReplyAuthorizedDateTime = transaction.getTimeOfRecord();


		data.setCcAuthReplyReasonCode(ccAuthReplyReasonCode);
		data.setCcAuthReplyAuthorizationCode(ccAuthReplyAuthorizationCode);
		data.setCcAuthReplyCvCode(ccAuthReplyCvCode);
		data.setCvnDecision(cvnDecision);
		data.setCcAuthReplyAvsCodeRaw(ccAuthReplyAvsCodeRaw);
		data.setCcAuthReplyAvsCode(ccAuthReplyAvsCode);
		data.setCcAuthReplyAmount(ccAuthReplyAmount);
		data.setCcAuthReplyProcessorResponse(ccAuthReplyProcessorResponse);
		data.setCcAuthReplyAuthorizedDateTime(ccAuthReplyAuthorizedDateTime);

		target.setAuthReplyData(data);
	}

}
