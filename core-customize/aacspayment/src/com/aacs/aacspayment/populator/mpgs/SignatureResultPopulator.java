/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.populator.mpgs;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.SignatureData;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Map;

import com.aacs.aacspayment.mpgs.beans.incoming.OuterTransactionResponse;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Order;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * @author amjad.shati@erabia.com
 */
public class SignatureResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{

	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(source, "Parameter [Map<String, String>] source cannot be null");
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");
		final GsonBuilder _builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
		final Gson gson = _builder.setPrettyPrinting().create();
		final OuterTransactionResponse transaction = gson.fromJson(gson.toJson(source), OuterTransactionResponse.class);
		validateParameterNotNull(transaction, "transaction of type OuterTransactionResponse cannot be null");

		BigDecimal amount = null;
		String currency = null;
		String currencyPublicSignature = null;
		final Order orderBean = transaction.getOrder();
		if (orderBean != null)
		{


			amount = new BigDecimal(orderBean.getAmount(), MathContext.DECIMAL64);
			currency = orderBean.getCurrency();
			currencyPublicSignature = orderBean.getCurrency();
		}



		final String merchantID = transaction.getMerchant();
		final String orderPageSerialNumber = null;
		final String sharedSecret = null;
		final String orderPageVersion = "1.1";
		final String amountPublicSignature = null;
		final String signedFields = null;

		final String transactionSignature = transaction.getTransaction() != null ? transaction.getTransaction().getId() : null;


		final SignatureData data = new SignatureData();

		data.setAmount(amount);
		data.setCurrency(currency);
		data.setMerchantID(merchantID);
		data.setOrderPageSerialNumber(orderPageSerialNumber);
		data.setSharedSecret(sharedSecret);
		data.setOrderPageVersion(orderPageVersion);
		data.setAmountPublicSignature(amountPublicSignature);
		data.setCurrencyPublicSignature(currencyPublicSignature);
		data.setTransactionSignature(transactionSignature);
		data.setSignedFields(signedFields);

		target.setSignatureData(data);
	}

}
