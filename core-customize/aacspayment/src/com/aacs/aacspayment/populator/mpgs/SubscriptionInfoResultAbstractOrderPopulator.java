/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.populator.mpgs;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.SubscriptionInfoData;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import javax.annotation.Resource;

import com.aacs.aacspayment.context.PaymentProviderContext;
import com.aacs.aacspayment.model.MpgsPaymentProviderModel;
import com.aacs.aacspayment.mpgs.beans.incoming.OuterTransactionResponse;
import com.aacs.aacspayment.mpgs.constants.MpgsConstants;
import com.aacs.aacspayment.mpgs.exception.MpgsException;
import com.aacs.aacspayment.mpgs.service.MpgsService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * @author husam.dababneh@erabia.com
 */
public class SubscriptionInfoResultAbstractOrderPopulator
		extends AbstractResultPopulator<AbstractOrderModel, CreateSubscriptionResult>
{

	@Resource(name = "mpgsService")
	private MpgsService mpgsService;

	@Resource(name = "paymentProviderContext")
	private PaymentProviderContext paymentProviderContext;

	private MpgsConstants getMpgsConstants(final AbstractOrderModel order)
	{
		final MpgsPaymentProviderModel provider = (MpgsPaymentProviderModel) paymentProviderContext.getProvider(order).orElse(null);
		return new MpgsConstants(provider.getMerchantId(), provider.getApiKey(), provider.getApiVersion(),
				provider.getJavaScriptCodeUrl(), provider.getBaseURL());
	}


	@Override
	public void populate(final AbstractOrderModel order, final CreateSubscriptionResult target)
	{
		OuterTransactionResponse source;
		try
		{
			source = mpgsService.getLastTransaction(order.getPaymentTransactionCode(), getMpgsConstants(order)).orElse(null);
		}
		catch (final MpgsException e)
		{
			throw new IllegalStateException("Could not retrive last transaction");
		}

		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");
		validateParameterNotNull(source, "Parameter [AbstractOrderModel] source cannot be null");

		final Gson gson = new GsonBuilder().create();
		final OuterTransactionResponse transaction = gson.fromJson(gson.toJson(source), OuterTransactionResponse.class);
		validateParameterNotNull(transaction, "transaction of type OuterTransactionResponse cannot be null");


		final SubscriptionInfoData data = new SubscriptionInfoData();
		final String subscriptionID = transaction.getTransaction() != null ? transaction.getTransaction().getId() : null;
		final String subscriptionIDPublicSignature = null;
		final String subscriptionSignedValue = null;
		data.setSubscriptionID(subscriptionID);
		data.setSubscriptionIDPublicSignature(subscriptionIDPublicSignature);
		data.setSubscriptionSignedValue(subscriptionSignedValue);

		target.setSubscriptionInfoData(data);
	}
}
