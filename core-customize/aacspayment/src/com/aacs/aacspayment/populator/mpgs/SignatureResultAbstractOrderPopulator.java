/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.populator.mpgs;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.SignatureData;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.math.BigDecimal;
import java.math.MathContext;

import javax.annotation.Resource;

import com.aacs.aacspayment.context.PaymentProviderContext;
import com.aacs.aacspayment.model.MpgsPaymentProviderModel;
import com.aacs.aacspayment.mpgs.beans.incoming.OuterTransactionResponse;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Order;
import com.aacs.aacspayment.mpgs.constants.MpgsConstants;
import com.aacs.aacspayment.mpgs.exception.MpgsException;
import com.aacs.aacspayment.mpgs.service.MpgsService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * @author amjad.shati@erabia.com
 */
public class SignatureResultAbstractOrderPopulator extends AbstractResultPopulator<AbstractOrderModel, CreateSubscriptionResult>
{
	@Resource(name = "mpgsService")
	private MpgsService mpgsService;

	@Resource(name = "paymentProviderContext")
	private PaymentProviderContext paymentProviderContext;

	private MpgsConstants getMpgsConstants(final AbstractOrderModel order)
	{
		final MpgsPaymentProviderModel provider = (MpgsPaymentProviderModel) paymentProviderContext.getProvider(order).orElse(null);
		return new MpgsConstants(provider.getMerchantId(), provider.getApiKey(), provider.getApiVersion(),
				provider.getJavaScriptCodeUrl(), provider.getBaseURL());
	}

	@Override
	public void populate(final AbstractOrderModel order, final CreateSubscriptionResult target)
	{
		OuterTransactionResponse source;
		try
		{
			source = mpgsService.getLastTransaction(order.getPaymentTransactionCode(), getMpgsConstants(order)).orElse(null);
		}
		catch (final MpgsException e)
		{
			throw new IllegalStateException("Could not retrive last transaction");
		}

		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");
		validateParameterNotNull(source, "Parameter [AbstractOrderModel] source cannot be null");

		final Gson gson = new GsonBuilder().create();
		final OuterTransactionResponse transaction = gson.fromJson(gson.toJson(source), OuterTransactionResponse.class);
		validateParameterNotNull(transaction, "transaction of type OuterTransactionResponse cannot be null");

		BigDecimal amount = null;
		String currency = null;
		String currencyPublicSignature = null;
		final Order orderBean = transaction.getOrder();
		if (orderBean != null)
		{


			amount = new BigDecimal(orderBean.getAmount(), MathContext.DECIMAL64);
			currency = orderBean.getCurrency();
			currencyPublicSignature = orderBean.getCurrency();
		}



		final String merchantID = transaction.getMerchant();
		final String orderPageSerialNumber = null;
		final String sharedSecret = null;
		final String orderPageVersion = "1.1";
		final String amountPublicSignature = null;
		final String signedFields = null;

		final String transactionSignature = transaction.getTransaction() != null ? transaction.getTransaction().getId() : null;


		final SignatureData data = new SignatureData();

		data.setAmount(amount);
		data.setCurrency(currency);
		data.setMerchantID(merchantID);
		data.setOrderPageSerialNumber(orderPageSerialNumber);
		data.setSharedSecret(sharedSecret);
		data.setOrderPageVersion(orderPageVersion);
		data.setAmountPublicSignature(amountPublicSignature);
		data.setCurrencyPublicSignature(currencyPublicSignature);
		data.setTransactionSignature(transactionSignature);
		data.setSignedFields(signedFields);

		target.setSignatureData(data);
	}

}
