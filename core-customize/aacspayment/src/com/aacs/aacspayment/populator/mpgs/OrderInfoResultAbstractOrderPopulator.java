/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.populator.mpgs;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.CartService;

import javax.annotation.Resource;


/**
 * @author husam.dababneh@erabia.com
 */
public class OrderInfoResultAbstractOrderPopulator extends AbstractResultPopulator<AbstractOrderModel, CreateSubscriptionResult>
{

	@Resource(name = "cartService")
	private CartService cartService;

	@Override
	public void populate(final AbstractOrderModel order, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(order, "Parameter [AbstractOrderModel] source cannot be null");
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");
		final OrderInfoData data = new OrderInfoData();

		final String comments = null;
		final String orderNumber = order.getOrderCode();
		final Boolean orderPageIgnoreAVS = null;
		final Boolean orderPageIgnoreCVN = null;
		final String orderPageRequestToken = null;
		final String orderPageTransactionType = "capture";
		final String recurringSubscriptionInfoPublicSignature = null;
		final String subscriptionTitle = null;
		final String taxAmount = null;

		data.setComments(comments);
		data.setOrderNumber(orderNumber);
		data.setOrderPageIgnoreAVS(orderPageIgnoreAVS);
		data.setOrderPageIgnoreCVN(orderPageIgnoreCVN);
		data.setOrderPageRequestToken(orderPageRequestToken);
		data.setOrderPageTransactionType(orderPageTransactionType);
		data.setRecurringSubscriptionInfoPublicSignature(recurringSubscriptionInfoPublicSignature);
		data.setSubscriptionTitle(subscriptionTitle);
		data.setTaxAmount(taxAmount);

		target.setOrderInfoData(data);
	}
}
