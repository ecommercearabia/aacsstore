/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.populator.mpgs;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.cybersource.enums.CardTypeEnum;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;

import java.util.Map;

import com.aacs.aacspayment.mpgs.beans.incoming.OuterTransactionResponse;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Card;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.SourceOfFunds;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * @author husam.dababneh@erabia.com
 */
public class PaymentInfoResultPopulator extends AbstractResultPopulator<Map<String, Object>, CreateSubscriptionResult>
{
	@Override
	public void populate(final Map<String, Object> source, final CreateSubscriptionResult target)
	{
		validateParameterNotNull(source, "Parameter [Map<String, String>] source cannot be null");
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");
		final GsonBuilder _builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
		final Gson gson = _builder.setPrettyPrinting().create();
		final OuterTransactionResponse transaction = gson.fromJson(gson.toJson(source), OuterTransactionResponse.class);
		validateParameterNotNull(transaction, "transaction of type OuterTransactionResponse cannot be null");

		String cardAccountNumber = null;
		String cardCardType = null;
		Integer cardExpirationMonth = null;
		Integer cardExpirationYear = null;
		String cardAccountHolderName = null;



		final SourceOfFunds sourceOfFunds = transaction.getSourceOfFunds();

		if (sourceOfFunds != null && sourceOfFunds.getProvided() != null && sourceOfFunds.getProvided().getCard() != null)
		{
			final Card card = sourceOfFunds.getProvided().getCard();

			cardAccountNumber = card.getNumber();
			cardCardType = getCardType(card.getBrand());


			cardExpirationMonth = card.getExpiry() != null ? Integer.parseInt(card.getExpiry().getMonth()) : 0;
			cardExpirationYear = card.getExpiry() != null ? Integer.parseInt(card.getExpiry().getYear()) : 0;

			cardAccountHolderName = card.getNameOnCard();
		}

		final String cardCvNumber = null;

		final String cardIssueNumber = "0000";
		final String cardStartMonth = "01";
		final String cardStartYear = "30";
		final String paymentOption = "CARD";


		final PaymentInfoData data = new PaymentInfoData();
		data.setCardAccountNumber(cardAccountNumber);
		data.setCardCardType(cardCardType);
		data.setCardCvNumber(cardCvNumber);
		data.setCardExpirationMonth(cardExpirationMonth);
		data.setCardExpirationYear(cardExpirationYear);
		data.setCardIssueNumber(cardIssueNumber);
		data.setCardStartMonth(cardStartMonth);
		data.setCardStartYear(cardStartYear);
		data.setPaymentOption(paymentOption);
		data.setCardAccountHolderName(cardAccountHolderName);

		target.setPaymentInfoData(data);
	}


	private String getCardType(String brandType)
	{
		if (brandType.toLowerCase().contains("mastercard"))
		{
			brandType = String.valueOf(CardTypeEnum.master.getStringValue());
		}
		else if (brandType.toLowerCase().contains("visa"))
		{
			brandType = String.valueOf(CardTypeEnum.visa.getStringValue());
		}
		else
		{
			brandType = String.valueOf(CardTypeEnum.master.getStringValue());
		}
		return brandType;
	}

}
