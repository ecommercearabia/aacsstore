/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.entry;

import java.io.Serializable;


/**
 * The Class PaymentRequestData.
 *
 * @author abu-muhasien
 * @author husam.dababneh@erabia.com
 *
 */
public class PaymentRequestData implements Serializable
{

	/** The script src. */
	/* In MPGS This stores the session ID */
	private String scriptSrc;

	/** The payment provider. */
	private String paymentProvider;
	/** The payment provider Data. */
	private Object paymentProviderData;

	private String version;

	/**
	 *
	 */
	public PaymentRequestData(final String scriptSrc, final String paymentProvider, final Object paymentProviderData,
			final String version)
	{
		super();
		this.scriptSrc = scriptSrc;
		this.paymentProvider = paymentProvider;
		this.paymentProviderData = paymentProviderData;
		this.version = version;
	}

	/**
	 * Instantiates a new payment request data.
	 *
	 * @param scriptSrc
	 *           the script src
	 * @param paymentProvider
	 *           the payment provider
	 */
	public PaymentRequestData(final String scriptSrc, final String paymentProvider)
	{
		super();
		this.scriptSrc = scriptSrc;
		this.paymentProvider = paymentProvider;
	}

	/**
	 * @return the paymentProviderData
	 */
	public Object getPaymentProviderData()
	{
		return paymentProviderData;
	}

	/**
	 * @param paymentProviderData
	 *           the paymentProviderData to set
	 */
	public void setPaymentProviderData(final Object paymentProviderData)
	{
		this.paymentProviderData = paymentProviderData;
	}

	/**
	 * Sets the payment provider.
	 *
	 * @param paymentProvider
	 *           the new payment provider
	 */
	public void setPaymentProvider(final String paymentProvider)
	{
		this.paymentProvider = paymentProvider;
	}

	/**
	 * Gets the payment provider.
	 *
	 * @return the payment provider
	 */
	public String getPaymentProvider()
	{
		return paymentProvider;
	}

	/**
	 * Gets the script src.
	 *
	 * @return the scriptSrc
	 */
	public String getScriptSrc()
	{
		return scriptSrc;
	}

	/**
	 * Sets the script src.
	 *
	 * @param scriptSrc
	 *           the scriptSrc to set
	 */
	public void setScriptSrc(final String scriptSrc)
	{
		this.scriptSrc = scriptSrc;
	}

}
