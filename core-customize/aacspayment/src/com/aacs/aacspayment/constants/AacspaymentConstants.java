/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.constants;

/**
 * Global class for all Aacspayment constants. You can add global constants for your extension into this class.
 */
public final class AacspaymentConstants extends GeneratedAacspaymentConstants
{
	public static final String EXTENSIONNAME = "aacspayment";

	private AacspaymentConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "aacspaymentPlatformLogo";
}
