/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.ccavenue.enums;

/**
 * @author ashati
 *
 */
public enum CCAvenueConstants
{
	ACCESS_CODE("AVUQ03GK18BR25QURB"), WORKING_KEY("DB52ABCF09144F1F145607BEEF3C349A");

	private String value;

	private CCAvenueConstants(final String value)
	{
		this.value = value;
	}

	public String getValue()
	{
		return value;
	}

}
