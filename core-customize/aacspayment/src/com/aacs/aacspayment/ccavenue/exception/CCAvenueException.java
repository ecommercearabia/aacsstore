/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.ccavenue.exception;

import com.aacs.aacspayment.exception.PaymentException;
import com.aacs.aacspayment.exception.type.PaymentExceptionType;

/**
 *
 */
public class CCAvenueException extends PaymentException
{
	

	public CCAvenueException(final PaymentExceptionType type)
	{
		super(type.getMessage(), type);
	}

	public CCAvenueException(final String message, final PaymentExceptionType type)
	{
		super(message, type);
	}



}
