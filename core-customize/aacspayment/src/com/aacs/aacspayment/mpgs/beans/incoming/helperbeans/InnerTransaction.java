package com.aacs.aacspayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InnerTransaction {
	@Expose
	@SerializedName("acquirer")
	private Acquirer acquirer;
	@Expose
	@SerializedName("amount")
	private String amount;
	@Expose
	@SerializedName("authorizationCode")
	private String authorizationCode;
	@Expose
	@SerializedName("currency")
	private String currency;
	@Expose
	@SerializedName("id")
	private String id;
	@Expose
	@SerializedName("receipt")
	private String receipt;
	@Expose
	@SerializedName("source")
	private String source;
	@Expose
	@SerializedName("stan")
	private String stan;
	@Expose
	@SerializedName("terminal")
	private String terminal;
	@Expose
	@SerializedName("type")
	private String type;

	public InnerTransaction() {
		super();
	}

	public Acquirer getAcquirer() {
		return acquirer;
	}

	public void setAcquirer(Acquirer acquirer) {
		this.acquirer = acquirer;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getAuthorizationCode() {
		return authorizationCode;
	}

	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getReceipt() {
		return receipt;
	}

	public void setReceipt(String receipt) {
		this.receipt = receipt;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getStan() {
		return stan;
	}

	public void setStan(String stan) {
		this.stan = stan;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
