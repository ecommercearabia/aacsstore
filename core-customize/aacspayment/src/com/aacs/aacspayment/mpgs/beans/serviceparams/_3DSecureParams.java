/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.mpgs.beans.serviceparams;

/**
 *
 */
public class _3DSecureParams extends PayServiceParams
{

	private String redirectUrl;

	/**
	 *
	 */
	public _3DSecureParams()
	{
	}

	/**
	 * @return the redirectUrl
	 */
	public String getRedirectUrl()
	{
		return redirectUrl;
	}

	/**
	 * @param redirectUrl
	 *           the redirectUrl to set
	 */
	public void setRedirectUrl(final String redirectUrl)
	{
		this.redirectUrl = redirectUrl;
	}

}
