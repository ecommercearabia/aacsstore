/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.mpgs.beans.serviceparams;

/**
 * @author monzer
 */
public class OrderServiceParams
{
	private String orderId;
	private double amount;
	private String currency;
	private String uniqueTransactionId;

	/**
	 *
	 */
	public OrderServiceParams()
	{
	}

	/**
	 * @return the orderId
	 */
	public String getOrderId()
	{
		return orderId;
	}

	/**
	 * @param orderId
	 *           the orderId to set
	 */
	public void setOrderId(final String orderId)
	{
		this.orderId = orderId;
	}

	/**
	 * @return the amount
	 */
	public double getAmount()
	{
		return amount;
	}

	/**
	 * @param amount
	 *           the amount to set
	 */
	public void setAmount(final double amount)
	{
		this.amount = amount;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency()
	{
		return currency;
	}

	/**
	 * @param currency
	 *           the currency to set
	 */
	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	/**
	 * @return the uniiqueTransactionId
	 */
	public String getUniqueTransactionId()
	{
		return uniqueTransactionId;
	}

	/**
	 * @param uniiqueTransactionId
	 *           the uniiqueTransactionId to set
	 */
	public void setUniqueTransactionId(final String uniqueTransactionId)
	{
		this.uniqueTransactionId = uniqueTransactionId;
	}

}
