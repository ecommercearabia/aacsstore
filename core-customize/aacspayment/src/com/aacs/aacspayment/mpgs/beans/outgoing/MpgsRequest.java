package com.aacs.aacspayment.mpgs.beans.outgoing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class MpgsRequest
{

	@Expose
	@SerializedName("apiOperation")
	private String apiOperation;

	public String getApiOperation()
	{
		return apiOperation;
	}

	protected void setApiOperation(final String apiOperation)
	{
		this.apiOperation = apiOperation;
	}

}
