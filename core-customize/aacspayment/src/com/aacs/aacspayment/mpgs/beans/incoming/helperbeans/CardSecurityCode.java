package com.aacs.aacspayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CardSecurityCode {
	@Expose
	@SerializedName("acquirerCode")
	private String acquirerCode;
	@Expose
	@SerializedName("gatewayCode")
	private String gatewayCode;

	public CardSecurityCode() {
	}

	public String getAcquirerCode() {
		return acquirerCode;
	}

	public void setAcquirerCode(String acquirerCode) {
		this.acquirerCode = acquirerCode;
	}

	public String getGatewayCode() {
		return gatewayCode;
	}

	public void setGatewayCode(String gatewayCode) {
		this.gatewayCode = gatewayCode;
	}

}