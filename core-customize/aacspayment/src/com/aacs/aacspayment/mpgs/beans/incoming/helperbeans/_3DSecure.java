package com.aacs.aacspayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class _3DSecure
{
	@Expose
	@SerializedName("acsEci")
	private String acsEci;
	@Expose
	@SerializedName("authenticationToken")
	private String authenticationToken;
	@Expose
	@SerializedName("paResStatus")
	private String paResStatus;
	@Expose
	@SerializedName("veResEnrolled")
	private String veResEnrolled;
	@Expose
	@SerializedName("xid")
	private String xid;
	@Expose
	@SerializedName("authenticationRedirect")
	private String authenticationRedirect;
	@Expose
	@SerializedName("transactionId")
	private String transactionId;
	@Expose
	@SerializedName("transactionStatus")
	private String transactionStatus;

	public _3DSecure()
	{
	}

	public String getAcsEci()
	{
		return acsEci;
	}

	public void setAcsEci(final String acsEci)
	{
		this.acsEci = acsEci;
	}

	public String getAuthenticationToken()
	{
		return authenticationToken;
	}

	public void setAuthenticationToken(final String authenticationToken)
	{
		this.authenticationToken = authenticationToken;
	}

	public String getPaResStatus()
	{
		return paResStatus;
	}

	public void setPaResStatus(final String paResStatus)
	{
		this.paResStatus = paResStatus;
	}

	public String getVeResEnrolled()
	{
		return veResEnrolled;
	}

	public void setVeResEnrolled(final String veResEnrolled)
	{
		this.veResEnrolled = veResEnrolled;
	}

	public String getXid()
	{
		return xid;
	}

	public void setXid(final String xid)
	{
		this.xid = xid;
	}

	/**
	 * @return the authenticationRedirect
	 */
	public String getAuthenticationRedirect()
	{
		return authenticationRedirect;
	}

	/**
	 * @param authenticationRedirect
	 *           the authenticationRedirect to set
	 */
	public void setAuthenticationRedirect(final String authenticationRedirect)
	{
		this.authenticationRedirect = authenticationRedirect;
	}

	/**
	 * @return the transactionId
	 */
	public String getTransactionId()
	{
		return transactionId;
	}

	/**
	 * @param transactionId
	 *           the transactionId to set
	 */
	public void setTransactionId(final String transactionId)
	{
		this.transactionId = transactionId;
	}

}