package com.aacs.aacspayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class _3DSecure1
{
	@Expose()
	@SerializedName("paResStatus")
	private String paResStatus;

	@Expose()
	@SerializedName("veResEnrolled")
	private String veResEnrolled;



	/**
	 *
	 */
	public _3DSecure1()
	{
		super();
	}

	/**
	 * @return the paResStatus
	 */
	public String getPaResStatus()
	{
		return paResStatus;
	}

	/**
	 * @param paResStatus
	 *           the paResStatus to set
	 */
	public void setPaResStatus(final String paResStatus)
	{
		this.paResStatus = paResStatus;
	}

	/**
	 * @return the veResEnrolled
	 */
	public String getVeResEnrolled()
	{
		return veResEnrolled;
	}

	/**
	 * @param veResEnrolled
	 *           the veResEnrolled to set
	 */
	public void setVeResEnrolled(final String veResEnrolled)
	{
		this.veResEnrolled = veResEnrolled;
	}




}