/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * @author monzer
 */
public class RedirectBean
{

	@Expose
	@SerializedName("customized")
	private CustomizedBean customized;
	@Expose
	@SerializedName("domainName")
	private String domainName;

	/**
	 *
	 */
	public RedirectBean()
	{
	}

	/**
	 * @return the customized
	 */
	public CustomizedBean getCustomized()
	{
		return customized;
	}

	/**
	 * @param customized
	 *           the customized to set
	 */
	public void setCustomized(final CustomizedBean customized)
	{
		this.customized = customized;
	}

}
