/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.mpgs.beans;

import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Interaction;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Order;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.SessionBean;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class RetrieveSessionBean
{

	@Expose
	@SerializedName("order")
	private Order order;

	@Expose
	@SerializedName("merchant")
	private String merchant;

	@Expose
	@SerializedName("session")
	private SessionBean session;

	@Expose
	@SerializedName("interaction")
	private Interaction interaction;

	@Expose
	@SerializedName("version")
	private String version;

	/**
	 * @return the order
	 */
	public Order getOrder()
	{
		return order;
	}

	/**
	 * @param order
	 *           the order to set
	 */
	public void setOrder(final Order order)
	{
		this.order = order;
	}

	/**
	 * @return the merchant
	 */
	public String getMerchant()
	{
		return merchant;
	}

	/**
	 * @param merchant
	 *           the merchant to set
	 */
	public void setMerchant(final String merchant)
	{
		this.merchant = merchant;
	}

	/**
	 * @return the session
	 */
	public SessionBean getSession()
	{
		return session;
	}

	/**
	 * @param session
	 *           the session to set
	 */
	public void setSession(final SessionBean session)
	{
		this.session = session;
	}

	/**
	 * @return the interaction
	 */
	public Interaction getInteraction()
	{
		return interaction;
	}

	/**
	 * @param interaction
	 *           the interaction to set
	 */
	public void setInteraction(final Interaction interaction)
	{
		this.interaction = interaction;
	}

	/**
	 * @return the version
	 */
	public String getVersion()
	{
		return version;
	}

	/**
	 * @param version
	 *           the version to set
	 */
	public void setVersion(final String version)
	{
		this.version = version;
	}

}
