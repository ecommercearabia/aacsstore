package com.aacs.aacspayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class _3DTransaction
{

	@Expose
	@SerializedName("amount")
	private String amount;
	@Expose
	@SerializedName("authenticationStatus")
	private MpgsTransactionAuthenticaStatus authenticationStatus;
	@Expose
	@SerializedName("currency")
	private String currency;
	@Expose
	@SerializedName("id")
	private String id;
	@Expose
	@SerializedName("type")
	private MpgsTransactionType type;
	@Expose
	@SerializedName("acquirer")
	private Acquirer acquirer;


	public _3DTransaction()
	{
	}

	public String getAmount()
	{
		return amount;
	}

	public void setAmount(final String amount)
	{
		this.amount = amount;
	}

	public MpgsTransactionAuthenticaStatus getAuthenticationStatus()
	{
		return authenticationStatus;
	}

	public void setAuthenticationStatus(final MpgsTransactionAuthenticaStatus authenticationStatus)
	{
		this.authenticationStatus = authenticationStatus;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	public MpgsTransactionType getType()
	{
		return type;
	}

	public void setType(final MpgsTransactionType type)
	{
		this.type = type;
	}

	/**
	 * @return the acquirer
	 */
	public Acquirer getAcquirer()
	{
		return acquirer;
	}

	/**
	 * @param acquirer
	 *           the acquirer to set
	 */
	public void setAcquirer(final Acquirer acquirer)
	{
		this.acquirer = acquirer;
	}

}
