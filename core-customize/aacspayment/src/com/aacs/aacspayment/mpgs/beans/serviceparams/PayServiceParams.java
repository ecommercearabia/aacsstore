/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.mpgs.beans.serviceparams;

/**
 *
 */
public class PayServiceParams extends OrderServiceParams
{

	private String sessionId;
	private String authenticatedTransactioinId;

	/**
	 *
	 */
	public PayServiceParams()
	{

	}

	/**
	 * @return the sessionId
	 */
	public String getSessionId()
	{
		return this.sessionId;
	}

	/**
	 * @param sessionId
	 *           the sessionId to set
	 */
	public void setSessionId(final String sessionId)
	{
		this.sessionId = sessionId;
	}

	/**
	 * @return the authenticatedTransactioinId
	 */
	public String getAuthenticatedTransactioinId()
	{
		return authenticatedTransactioinId;
	}

	/**
	 * @param authenticatedTransactioinId
	 *           the authenticatedTransactioinId to set
	 */
	public void setAuthenticatedTransactioinId(final String authenticatedTransactioinId)
	{
		this.authenticatedTransactioinId = authenticatedTransactioinId;
	}
}
