package com.aacs.aacspayment.mpgs.beans.incoming;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public abstract class MpgsResponse {

	public enum MpgsResultEnum {
		FAILURE("FAILURE", "The operation was declined or rejected by the gateway, acquirer or issuer"),
		PENDING("PENDING", "The operation is currently in progress or pending processing"),
		SUCCESS("SUCCESS", "The operation was successfully processed"),
		ERROR("ERROR", "Internal Server Error"),
		UNKNOWN("UNKNOWN", "The result of the operation is unknown");

		private final String result;
		private final String message;

		private MpgsResultEnum(final String result, final String message) {
			this.message = message;
			this.result = result;
		}

		public String getMessage() {
			return message;
		}

		public String getResult() {
			return result;
		}

	}

	@Expose
	@SerializedName("result")
	private String result;

	@Expose
	@SerializedName("error")
	private MpgsErrorResponse error;

	public String getResult() {
		return result;
	}

	public void setResult(final String result) {
		this.result = result;
	}

	public MpgsErrorResponse getError() {
		return error;
	}

	public void setError(final MpgsErrorResponse error) {
		this.error = error;
	}

}
