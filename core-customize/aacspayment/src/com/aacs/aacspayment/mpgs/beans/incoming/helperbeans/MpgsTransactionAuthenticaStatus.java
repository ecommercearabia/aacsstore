/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.mpgs.beans.incoming.helperbeans;

/**
 *
 */
public enum MpgsTransactionAuthenticaStatus
{
	// @formatter:off
	AUTHENTICATION_ATTEMPTED("AUTHENTICATION_ATTEMPTED", "Payer authentication was attempted and a proof of authentication attempt was obtained."),
	AUTHENTICATION_AVAILABLE("AUTHENTICATION_AVAILABLE", "Payer authentication is available for the payment method provided."),
	AUTHENTICATION_EXEMPT("AUTHENTICATION_EXEMPT", "Exemption from the Regulatory Technical Standards (RTS) requirements for Strong Customer Authentication (SCA) under the Payment Services Directive 2 (PSD2) regulations in the European Economic Area has been claimed or granted."),
	AUTHENTICATION_FAILED("AUTHENTICATION_FAILED", "The payer was not authenticated. You should not proceed with this transaction."),
	AUTHENTICATION_NOT_IN_EFFECT("AUTHENTICATION_NOT_IN_EFFECT", "There is no authentication information associated with this transaction."),
	AUTHENTICATION_NOT_SUPPORTED("AUTHENTICATION_NOT_SUPPORTED", "The requested authentication method is not supported for this payment method."),
	AUTHENTICATION_PENDING("AUTHENTICATION_PENDING", "Payer authentication is pending completion of a challenge process."),
	AUTHENTICATION_REJECTED("AUTHENTICATION_REJECTED", "The issuer rejected the authentication request and requested that you do not attempt authorization of a payment."),
	AUTHENTICATION_REQUIRED("AUTHENTICATION_REQUIRED", "Payer authentication is required for this payment, but was not provided."),
	AUTHENTICATION_SUCCESSFUL("AUTHENTICATION_SUCCESSFUL", "The payer was successfully authenticated."),
	AUTHENTICATION_UNAVAILABLE("AUTHENTICATION_UNAVAILABLE", "The payer was not able to be authenticated due to a technical or other issue.");
	// @formatter:on

	private String code;
	private String desc;

	/**
	 *
	 */
	private MpgsTransactionAuthenticaStatus(final String code, final String desc)
	{
		this.code = code;
		this.desc = desc;
	}

	/**
	 * @return the code
	 */
	public String getCode()
	{
		return code;
	}

	/**
	 * @return the desc
	 */
	public String getDesc()
	{
		return desc;
	}

	public static boolean isFailedAuthentication(final MpgsTransactionAuthenticaStatus status)
	{

		switch (status)
		{
			case AUTHENTICATION_FAILED:
			case AUTHENTICATION_REJECTED:
			case AUTHENTICATION_NOT_IN_EFFECT:
			case AUTHENTICATION_NOT_SUPPORTED:
			case AUTHENTICATION_UNAVAILABLE:
				return true;

			default:
				return false;
		}
	}

}
