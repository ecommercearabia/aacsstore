package com.aacs.aacspayment.mpgs.beans.outgoing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderInfoBean {

	@Expose
	@SerializedName("id")
	private String id;

	@Expose
	@SerializedName("currency")
	private String currency;

	@Expose
	@SerializedName("description")
	private String description;

	@Expose
	@SerializedName("amount")
	private String amount;

	@Expose
	@SerializedName("notification")
	private String notification;

	@Expose
	@SerializedName("reference")
	private String reference;



	/**
	 * @return the notification
	 */
	public String getNotification()
	{
		return notification;
	}

	/**
	 * @param notification
	 *           the notification to set
	 */
	public void setNotification(final String notification)
	{
		this.notification = notification;
	}

	/**
	 * @return the reference
	 */
	public String getReference()
	{
		return reference;
	}

	/**
	 * @param reference
	 *           the reference to set
	 */
	public void setReference(final String reference)
	{
		this.reference = reference;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(final String currency) {
		this.currency = currency;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(final String amount) {
		this.amount = amount;
	}

}
