/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.mpgs.beans.incoming;

import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Device;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class AuthenticatePayerResponseBean extends Initiate3DAuthenticationResponseBean
{

	@Expose
	@SerializedName("device")
	private Device device;

	/**
	 *
	 */
	public AuthenticatePayerResponseBean()
	{
	}

	/**
	 * @return the device
	 */
	public Device getDevice()
	{
		return device;
	}

	/**
	 * @param device
	 *           the device to set
	 */
	public void setDevice(final Device device)
	{
		this.device = device;
	}

}
