package com.aacs.aacspayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class _3DSecure2
{
	@Expose()
	@SerializedName("acsTransactionId")
	private String acsTransactionId;

	@Expose()
	@SerializedName("directoryServerId")
	private String directoryServerId;

	@Expose()
	@SerializedName("dsTransactionId")
	private String dsTransactionId;

	@Expose()
	@SerializedName("methodCompleted")
	private String methodCompleted;

	@Expose()
	@SerializedName("methodSupported")
	private String methodSupported;

	@Expose()
	@SerializedName("protocolVersion")
	private String protocolVersion;

	@Expose()
	@SerializedName("requestorId")
	private String requestorId;

	@Expose()
	@SerializedName("requestorName")
	private String requestorName;

	@Expose()
	@SerializedName("sdk")
	private Sdk sdk;

	@Expose()
	@SerializedName("statusReasonCode")
	private String statusReasonCode;

	@Expose()
	@SerializedName("transactionStatus")
	private String transactionStatus;



	/**
	 *
	 */
	public _3DSecure2()
	{
		super();
	}

	/**
	 * @return the acsTransactionId
	 */
	public String getAcsTransactionId()
	{
		return acsTransactionId;
	}

	/**
	 * @param acsTransactionId
	 *           the acsTransactionId to set
	 */
	public void setAcsTransactionId(final String acsTransactionId)
	{
		this.acsTransactionId = acsTransactionId;
	}

	/**
	 * @return the directoryServerId
	 */
	public String getDirectoryServerId()
	{
		return directoryServerId;
	}

	/**
	 * @param directoryServerId
	 *           the directoryServerId to set
	 */
	public void setDirectoryServerId(final String directoryServerId)
	{
		this.directoryServerId = directoryServerId;
	}

	/**
	 * @return the dsTransactionId
	 */
	public String getDsTransactionId()
	{
		return dsTransactionId;
	}

	/**
	 * @param dsTransactionId
	 *           the dsTransactionId to set
	 */
	public void setDsTransactionId(final String dsTransactionId)
	{
		this.dsTransactionId = dsTransactionId;
	}

	/**
	 * @return the methodCompleted
	 */
	public String getMethodCompleted()
	{
		return methodCompleted;
	}

	/**
	 * @param methodCompleted
	 *           the methodCompleted to set
	 */
	public void setMethodCompleted(final String methodCompleted)
	{
		this.methodCompleted = methodCompleted;
	}

	/**
	 * @return the methodSupported
	 */
	public String getMethodSupported()
	{
		return methodSupported;
	}

	/**
	 * @param methodSupported
	 *           the methodSupported to set
	 */
	public void setMethodSupported(final String methodSupported)
	{
		this.methodSupported = methodSupported;
	}

	/**
	 * @return the protocolVersion
	 */
	public String getProtocolVersion()
	{
		return protocolVersion;
	}

	/**
	 * @param protocolVersion
	 *           the protocolVersion to set
	 */
	public void setProtocolVersion(final String protocolVersion)
	{
		this.protocolVersion = protocolVersion;
	}

	/**
	 * @return the requestorId
	 */
	public String getRequestorId()
	{
		return requestorId;
	}

	/**
	 * @param requestorId
	 *           the requestorId to set
	 */
	public void setRequestorId(final String requestorId)
	{
		this.requestorId = requestorId;
	}

	/**
	 * @return the requestorName
	 */
	public String getRequestorName()
	{
		return requestorName;
	}

	/**
	 * @param requestorName
	 *           the requestorName to set
	 */
	public void setRequestorName(final String requestorName)
	{
		this.requestorName = requestorName;
	}

	/**
	 * @return the sdk
	 */
	public Sdk getSdk()
	{
		return sdk;
	}

	/**
	 * @param sdk
	 *           the sdk to set
	 */
	public void setSdk(final Sdk sdk)
	{
		this.sdk = sdk;
	}

	/**
	 * @return the statusReasonCode
	 */
	public String getStatusReasonCode()
	{
		return statusReasonCode;
	}

	/**
	 * @param statusReasonCode
	 *           the statusReasonCode to set
	 */
	public void setStatusReasonCode(final String statusReasonCode)
	{
		this.statusReasonCode = statusReasonCode;
	}

	/**
	 * @return the transactionStatus
	 */
	public String getTransactionStatus()
	{
		return transactionStatus;
	}

	/**
	 * @param transactionStatus
	 *           the transactionStatus to set
	 */
	public void setTransactionStatus(final String transactionStatus)
	{
		this.transactionStatus = transactionStatus;
	}



}