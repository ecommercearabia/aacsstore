package com.aacs.aacspayment.mpgs.beans.outgoing;

import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Device;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class AuthenticatePayerRequestBean extends Initiate3DAuthenticationBean
{
	@Expose
	@SerializedName("device")
	private Device device;

	// Same as the Initiate3DAuthenticationBean attributes
	public AuthenticatePayerRequestBean()
	{
		super();
		this.setApiOperation(ApiOperationEnum.AUTHENTICATE_PAYER.getOperation());
	}

	/**
	 * @return the device
	 */
	public Device getDevice()
	{
		return device;
	}

	/**
	 * @param device
	 *           the device to set
	 */
	public void setDevice(final Device device)
	{
		this.device = device;
	}

}
