package com.aacs.aacspayment.mpgs.beans.outgoing;

import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Billing;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Shipping;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CreateCheckoutSessionBean extends MpgsRequest
{

	@Expose
	@SerializedName("order")
	private OrderInfoBean order;

	@Expose
	@SerializedName("interaction")
	private InteractionBean interaction;

	@Expose
	@SerializedName("shipping")
	private Shipping shipping;

	@Expose
	@SerializedName("billing")
	private Billing billing;

	public CreateCheckoutSessionBean()
	{
		super();
		this.setApiOperation(ApiOperationEnum.CREATE_CHECKOUT_SESSION.getOperation());
	}

	public OrderInfoBean getOrder()
	{
		return order;
	}

	public void setOrder(final OrderInfoBean order)
	{
		this.order = order;
	}

	public InteractionBean getInteraction()
	{
		return interaction;
	}

	public void setInteraction(final InteractionBean interaction)
	{
		this.interaction = interaction;
	}

	public Shipping getShipping()
	{
		return shipping;
	}

	public void setShipping(final Shipping shipping)
	{
		this.shipping = shipping;
	}

	public Billing getBilling()
	{
		return billing;
	}

	public void setBilling(final Billing billing)
	{
		this.billing = billing;
	}

}
