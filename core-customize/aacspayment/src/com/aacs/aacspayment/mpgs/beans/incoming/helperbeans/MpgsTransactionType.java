/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.mpgs.beans.incoming.helperbeans;

/**
 *
 */
public enum MpgsTransactionType
{
	// @formatter:off
	AUTHENTICATION("AUTHENTICATION", "Authentication"),
	AUTHORIZATION("AUTHORIZATION", "Authorization"),
	AUTHORIZATION_UPDATE("AUTHORIZATION_UPDATE", "Authorization Update"),
	CAPTURE("CAPTURE", "Capture"),
	CHARGEBACK("CHARGEBACK", "Chargeback"),
	FUNDING("FUNDING", "The transaction transfers money to or from the merchant, without the involvement of a payer. For example, recording monthly merchant service fees from your payment service provider."),
	PAYMENT("PAYMENT", "Payment (Purchase)"),
	REFUND("REFUND", "Refund"),
	REFUND_REQUEST("REFUND_REQUEST", "Refund Request"),
	VERIFICATION("VERIFICATION", "Verification"),
	VOID_AUTHORIZATION("VOID_AUTHORIZATION", "Void Authorization"),
	VOID_CAPTURE("VOID_CAPTURE", "Void Capture"),
	VOID_PAYMENT("VOID_PAYMENT", "Void Payment"),
	VOID_REFUND("VOID_REFUND", "Void Refund");


	//@formatter:on
	private String code;
	private String desc;

	/**
	 *
	 */
	private MpgsTransactionType(final String code, final String desc)
	{
		this.code = code;
		this.desc = desc;
	}

	/**
	 * @return the code
	 */
	public String getCode()
	{
		return code;
	}

	/**
	 * @return the desc
	 */
	public String getDesc()
	{
		return desc;
	}


}
