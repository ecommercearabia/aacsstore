package com.aacs.aacspayment.mpgs.beans.incoming;

import java.util.Date;
import java.util.List;

import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Billing;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Chargeback;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Customer;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Device;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.SourceOfFunds;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans._3DSecure;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderStatusResponse extends MpgsResponse {

	@Expose
	@SerializedName("3DSecure")
	private _3DSecure _3DSecure;
	@Expose
	@SerializedName("3DSecureId")
	private String _3DSecureId;
	@Expose
	@SerializedName("amount")
	private double amount;
	@Expose
	@SerializedName("billing")
	private Billing billing;
	@Expose
	@SerializedName("certainty")
	private String certainty;
	@Expose
	@SerializedName("chargeback")
	private Chargeback chargeback;
	@Expose
	@SerializedName("creationTime")
	private Date creationTime;
	@Expose
	@SerializedName("currency")
	private String currency;
	@Expose
	@SerializedName("customer")
	private Customer customer;
	@Expose
	@SerializedName("description")
	private String description;
	@Expose
	@SerializedName("device")
	private Device device;
	@Expose
	@SerializedName("id")
	private String id;
	@Expose
	@SerializedName("lastUpdatedTime")
	private Date lastUpdatedTime;
	@Expose
	@SerializedName("merchant")
	private String merchant;
	@Expose
	@SerializedName("merchantAmount")
	private double merchantAmount;
	@Expose
	@SerializedName("merchantCategoryCode")
	private String merchantCategoryCode;
	@Expose
	@SerializedName("merchantCurrency")
	private String merchantCurrency;
	@Expose
	@SerializedName("sourceOfFunds")
	private SourceOfFunds sourceOfFunds;
	@Expose
	@SerializedName("status")
	private String status;
	@Expose
	@SerializedName("totalAuthorizedAmount")
	private double totalAuthorizedAmount;
	@Expose
	@SerializedName("totalCapturedAmount")
	private double totalCapturedAmount;
	@Expose
	@SerializedName("totalRefundedAmount")
	private double totalRefundedAmount;
	@Expose
	@SerializedName("transaction")
	private List<OuterTransactionResponse> transaction;

	public OrderStatusResponse() {
		super();
	}

	public _3DSecure get_3DSecure() {
		return _3DSecure;
	}

	public void set_3DSecure(_3DSecure _3dSecure) {
		_3DSecure = _3dSecure;
	}

	public String get_3DSecureId() {
		return _3DSecureId;
	}

	public void set_3DSecureId(String _3dSecureId) {
		_3DSecureId = _3dSecureId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public String getCertainty() {
		return certainty;
	}

	public void setCertainty(String certainty) {
		this.certainty = certainty;
	}

	public Chargeback getChargeback() {
		return chargeback;
	}

	public void setChargeback(Chargeback chargeback) {
		this.chargeback = chargeback;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(Date lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public double getMerchantAmount() {
		return merchantAmount;
	}

	public void setMerchantAmount(double merchantAmount) {
		this.merchantAmount = merchantAmount;
	}

	public String getMerchantCategoryCode() {
		return merchantCategoryCode;
	}

	public void setMerchantCategoryCode(String merchantCategoryCode) {
		this.merchantCategoryCode = merchantCategoryCode;
	}

	public String getMerchantCurrency() {
		return merchantCurrency;
	}

	public void setMerchantCurrency(String merchantCurrency) {
		this.merchantCurrency = merchantCurrency;
	}

	public SourceOfFunds getSourceOfFunds() {
		return sourceOfFunds;
	}

	public void setSourceOfFunds(SourceOfFunds sourceOfFunds) {
		this.sourceOfFunds = sourceOfFunds;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getTotalAuthorizedAmount() {
		return totalAuthorizedAmount;
	}

	public void setTotalAuthorizedAmount(double totalAuthorizedAmount) {
		this.totalAuthorizedAmount = totalAuthorizedAmount;
	}

	public double getTotalCapturedAmount() {
		return totalCapturedAmount;
	}

	public void setTotalCapturedAmount(double totalCapturedAmount) {
		this.totalCapturedAmount = totalCapturedAmount;
	}

	public double getTotalRefundedAmount() {
		return totalRefundedAmount;
	}

	public void setTotalRefundedAmount(double totalRefundedAmount) {
		this.totalRefundedAmount = totalRefundedAmount;
	}

	public List<OuterTransactionResponse> getTransactions() {
		return transaction;
	}

	public void setTransactions(List<OuterTransactionResponse> transactions) {
		this.transaction = transactions;
	}

}
