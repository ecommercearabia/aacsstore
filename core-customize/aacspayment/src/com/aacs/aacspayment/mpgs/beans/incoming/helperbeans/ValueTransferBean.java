/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class ValueTransferBean
{

	@Expose
	@SerializedName("accountType")
	private String accountType;

	/**
	 *
	 */
	public ValueTransferBean()
	{
	}

	/**
	 * @return the accountType
	 */
	public String getAccountType()
	{
		return accountType;
	}

	/**
	 * @param accountType
	 *           the accountType to set
	 */
	public void setAccountType(final String accountType)
	{
		this.accountType = accountType;
	}

}
