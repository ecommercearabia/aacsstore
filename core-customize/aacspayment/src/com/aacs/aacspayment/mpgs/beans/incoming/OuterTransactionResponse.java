package com.aacs.aacspayment.mpgs.beans.incoming;

import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Billing;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Customer;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Device;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.InnerTransaction;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Order;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Response;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.SourceOfFunds;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans._3DSecure;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OuterTransactionResponse extends MpgsResponse {
	@Expose
	@SerializedName("3DSecure")
	private _3DSecure _3DSecure;
	@Expose
	@SerializedName("3DSecureId")
	private String _3DSecureId;
	@Expose
	@SerializedName("authorizationResponse")
	private AuthorizationResponse authorizationResponse;
	@Expose
	@SerializedName("billing")
	private Billing billing;
	@Expose
	@SerializedName("customer")
	private Customer customer;
	@Expose
	@SerializedName("device")
	private Device device;
	@Expose
	@SerializedName("gatewayEntryPoint")
	private String gatewayEntryPoint;
	@Expose
	@SerializedName("merchant")
	private String merchant;
	@Expose
	@SerializedName("order")
	private Order order;
	@Expose
	@SerializedName("response")
	private Response response;
	@Expose
	@SerializedName("sourceOfFunds")
	private SourceOfFunds sourceOfFunds;
	@Expose
	@SerializedName("timeOfLastUpdate")
	private String timeOfLastUpdate;
	@Expose
	@SerializedName("timeOfRecord")
	private String timeOfRecord;
	@Expose
	@SerializedName("transaction")
	private InnerTransaction transaction;
	@Expose
	@SerializedName("version")
	private String version;

	public OuterTransactionResponse() {
		super();
	}

	public _3DSecure get_3DSecure() {
		return _3DSecure;
	}

	public void set_3DSecure(_3DSecure _3dSecure) {
		_3DSecure = _3dSecure;
	}

	public String get_3DSecureId() {
		return _3DSecureId;
	}

	public void set_3DSecureId(String _3dSecureId) {
		_3DSecureId = _3dSecureId;
	}

	public AuthorizationResponse getAuthorizationResponse() {
		return authorizationResponse;
	}

	public void setAuthorizationResponse(AuthorizationResponse authorizationResponse) {
		this.authorizationResponse = authorizationResponse;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public String getGatewayEntryPoint() {
		return gatewayEntryPoint;
	}

	public void setGatewayEntryPoint(String gatewayEntryPoint) {
		this.gatewayEntryPoint = gatewayEntryPoint;
	}

	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public SourceOfFunds getSourceOfFunds() {
		return sourceOfFunds;
	}

	public void setSourceOfFunds(SourceOfFunds sourceOfFunds) {
		this.sourceOfFunds = sourceOfFunds;
	}

	public String getTimeOfLastUpdate() {
		return timeOfLastUpdate;
	}

	public void setTimeOfLastUpdate(String timeOfLastUpdate) {
		this.timeOfLastUpdate = timeOfLastUpdate;
	}

	public String getTimeOfRecord() {
		return timeOfRecord;
	}

	public void setTimeOfRecord(String timeOfRecord) {
		this.timeOfRecord = timeOfRecord;
	}

	public InnerTransaction getTransaction() {
		return transaction;
	}

	public void setTransaction(InnerTransaction transaction) {
		this.transaction = transaction;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}
