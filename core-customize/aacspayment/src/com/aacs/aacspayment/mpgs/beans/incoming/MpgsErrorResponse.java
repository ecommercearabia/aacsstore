package com.aacs.aacspayment.mpgs.beans.incoming;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MpgsErrorResponse extends MpgsResponse {

	@Expose
	@SerializedName("cause")
	private String cause;

	@Expose
	@SerializedName("explanation")
	private String explanation;

	@Expose
	@SerializedName("field")
	private String field;
	
	@Expose
	@SerializedName("supportCode")
	private String supportCode;
	
	@Expose
	@SerializedName("validationType")
	private String validationType;

	public MpgsErrorResponse() {
		super();
	}

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	public String getExplanation() {
		return explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}

}
