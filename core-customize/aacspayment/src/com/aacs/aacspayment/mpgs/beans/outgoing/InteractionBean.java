package com.aacs.aacspayment.mpgs.beans.outgoing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InteractionBean {
	@SerializedName("operation")
	@Expose
	private String operation;

	@SerializedName("returnUrl")
	@Expose
	private String returnUrl;

	@SerializedName("cancelUrl")
	@Expose
	private String cancelUrl;

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	public String getCancelUrl() {
		return cancelUrl;
	}

	public void setCancelUrl(String cancelUrl) {
		this.cancelUrl = cancelUrl;
	}

}
