package com.aacs.aacspayment.mpgs.beans.outgoing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class AuthorizeOrderBean extends MpgsRequest
{

	private class Transaction
	{
		@Expose
		@SerializedName("amount")
		private double amount;

		@Expose
		@SerializedName("currency")
		private String currency;

		public Transaction()
		{
			super();
		}

		public double getAmount()
		{
			return amount;
		}

		public void setAmount(final double amount)
		{
			this.amount = amount;
		}

		public String getCurrency()
		{
			return currency;
		}

		public void setCurrency(final String currency)
		{
			this.currency = currency;
		}

	}

	@Expose
	@SerializedName("transaction")
	private final Transaction transaction;

	public AuthorizeOrderBean()
	{
		super();
		this.transaction = new Transaction();
		this.setApiOperation(ApiOperationEnum.AUTHORIZE.getOperation());
	}

	public double getTransactionAmount()
	{
		return transaction.getAmount();
	}

	public void setTransactionAmount(final double amount)
	{
		this.transaction.setAmount(amount);
	}

	public String getTransactionCurrency()
	{
		return transaction.getCurrency();
	}

	public void setTransactionCurrency(final String currency)
	{
		this.transaction.setCurrency(currency);
	}

}
