package com.aacs.aacspayment.mpgs.beans.outgoing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VoidTransactionBean extends MpgsRequest {

	private class Transaction {
		@Expose
		@SerializedName("targetTransactionId")
		private String transactionId;

		public String getTransactionId() {
			return transactionId;
		}

		public void setTransactionId(String transactionId) {
			this.transactionId = transactionId;
		}

	}

	@Expose
	@SerializedName("transaction")
	private Transaction transaction;

	public VoidTransactionBean() {
		this.transaction = new Transaction();
		this.setApiOperation(ApiOperationEnum.VOID.getOperation());
	}

	public String getTransactionId() {
		return transaction.getTransactionId();
	}

	public void setTransactionId(String id) {
		this.transaction.setTransactionId(id);
	}

}
