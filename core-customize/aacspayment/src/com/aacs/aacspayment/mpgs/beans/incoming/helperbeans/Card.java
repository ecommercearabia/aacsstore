package com.aacs.aacspayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Card {
	@Expose
	@SerializedName("brand")
	private String brand;
	@Expose
	@SerializedName("expiry")
	private Expiry expiry;
	@Expose
	@SerializedName("fundingMethod")
	private String fundingMethod;
	@Expose
	@SerializedName("issuer")
	private String issuer;
	@Expose
	@SerializedName("nameOnCard")
	private String nameOnCard;
	@Expose
	@SerializedName("number")
	private String number;
	@Expose
	@SerializedName("scheme")
	private String scheme;
	@Expose
	@SerializedName("storedOnFile")
	private String storedOnFile;

	public Card() {
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Expiry getExpiry() {
		return expiry;
	}

	public void setExpiry(Expiry expiry) {
		this.expiry = expiry;
	}

	public String getFundingMethod() {
		return fundingMethod;
	}

	public void setFundingMethod(String fundingMethod) {
		this.fundingMethod = fundingMethod;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public String getNameOnCard() {
		return nameOnCard;
	}

	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getScheme() {
		return scheme;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	public String getStoredOnFile() {
		return storedOnFile;
	}

	public void setStoredOnFile(String storedOnFile) {
		this.storedOnFile = storedOnFile;
	}

}
