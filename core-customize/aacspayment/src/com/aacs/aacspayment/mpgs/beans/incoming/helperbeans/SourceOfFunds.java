package com.aacs.aacspayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SourceOfFunds {
	@Expose
	@SerializedName("provided")
	private Provided provided;
	@Expose
	@SerializedName("type")
	private String type;

	public SourceOfFunds() {
	}

	public Provided getProvided() {
		return provided;
	}

	public void setProvided(Provided provided) {
		this.provided = provided;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
