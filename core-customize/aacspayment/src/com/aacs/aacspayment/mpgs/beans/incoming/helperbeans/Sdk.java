/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class Sdk
{
	@Expose
	@SerializedName("inferinterface")
	private String inferinterface;

	@Expose
	@SerializedName("timeout")
	private int timeout;

	@Expose
	@SerializedName("uiType")
	private String uiType;



	/**
	 *
	 */
	public Sdk()
	{
		super();
	}

	/**
	 * @return the inferinterface
	 */
	public String getInferinterface()
	{
		return inferinterface;
	}

	/**
	 * @param inferinterface
	 *           the inferinterface to set
	 */
	public void setInferinterface(final String inferinterface)
	{
		this.inferinterface = inferinterface;
	}

	/**
	 * @return the timeout
	 */
	public int getTimeout()
	{
		return timeout;
	}

	/**
	 * @param timeout
	 *           the timeout to set
	 */
	public void setTimeout(final int timeout)
	{
		this.timeout = timeout;
	}

	/**
	 * @return the uiType
	 */
	public String getUiType()
	{
		return uiType;
	}

	/**
	 * @param uiType
	 *           the uiType to set
	 */
	public void setUiType(final String uiType)
	{
		this.uiType = uiType;
	}



}
