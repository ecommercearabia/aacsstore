package com.aacs.aacspayment.mpgs.beans.outgoing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CaptureOrderBean extends MpgsRequest {

	private class Transaction {
		@Expose
		@SerializedName("amount")
		private double amount;

		@Expose
		@SerializedName("currency")
		private String currency;

		public Transaction() {
			super();
		}

		public double getAmount() {
			return amount;
		}

		public void setAmount(double amount) {
			this.amount = amount;
		}

		public String getCurrency() {
			return currency;
		}

		public void setCurrency(String currency) {
			this.currency = currency;
		}

	}

	@Expose
	@SerializedName("transaction")
	private Transaction transaction;

	public CaptureOrderBean() {
		super();
		this.transaction = new Transaction();
		this.setApiOperation(ApiOperationEnum.CAPTURE.getOperation());
	}

	public double getTransactionAmount() {
		return transaction.getAmount();
	}

	public void setTransactionAmount(double amount) {
		this.transaction.setAmount(amount);
	}

	public String getTransactionCurrency() {
		return transaction.getCurrency();
	}

	public void setTransactionCurrency(String currency) {
		this.transaction.setCurrency(currency);
	}

}
