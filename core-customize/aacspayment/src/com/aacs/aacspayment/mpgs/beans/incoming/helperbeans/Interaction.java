/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class Interaction
{
	@Expose
	@SerializedName("cancelUrl")
	private String cancelUrl;

	@Expose
	@SerializedName("operation")
	private String operation;

	@Expose
	@SerializedName("returnUrl")
	private String returnUrl;

	@Expose
	@SerializedName("successIndicator")
	private String successIndicator;

	/**
	 * @return the cancelUrl
	 */
	public String getCancelUrl()
	{
		return cancelUrl;
	}

	/**
	 * @param cancelUrl
	 *           the cancelUrl to set
	 */
	public void setCancelUrl(final String cancelUrl)
	{
		this.cancelUrl = cancelUrl;
	}

	/**
	 * @return the operation
	 */
	public String getOperation()
	{
		return operation;
	}

	/**
	 * @param operation
	 *           the operation to set
	 */
	public void setOperation(final String operation)
	{
		this.operation = operation;
	}

	/**
	 * @return the returnUrl
	 */
	public String getReturnUrl()
	{
		return returnUrl;
	}

	/**
	 * @param returnUrl
	 *           the returnUrl to set
	 */
	public void setReturnUrl(final String returnUrl)
	{
		this.returnUrl = returnUrl;
	}

	/**
	 * @return the successIndicator
	 */
	public String getSuccessIndicator()
	{
		return successIndicator;
	}

	/**
	 * @param successIndicator
	 *           the successIndicator to set
	 */
	public void setSuccessIndicator(final String successIndicator)
	{
		this.successIndicator = successIndicator;
	}



}
