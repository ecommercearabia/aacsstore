package com.aacs.aacspayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Shipping {
	@Expose
	@SerializedName("address")
	private Address address;

	public Shipping() {
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}