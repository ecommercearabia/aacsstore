/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.mpgs.beans.incoming.helperbeans;

/**
 *
 */
public enum MpgsOrderStatus
{
	// @formatter:off
	AUTHENTICATED("AUTHENTICATED", "The payer was successfully authenticated."),
	AUTHENTICATION_INITIATED("AUTHENTICATION_INITIATED", "Payer authentication has been initiated but not completed."),
	AUTHENTICATION_NOT_NEEDED("AUTHENTICATION_NOT_NEEDED", "Payer authentication was not performed as it was not needed."),
	AUTHENTICATION_UNSUCCESSFUL("AUTHENTICATION_UNSUCCESSFUL", "Payer authentication was not able to be successfully completed."),
	AUTHORIZED("AUTHORIZED", "The payment has been authorized successfully but the authorized amount has not yet been captured, in part, full, or excess."),
	CANCELLED("CANCELLED", "The initial transaction for this order has been voided successfully."),
	CAPTURED("CAPTURED", "The authorized amount for this order, in full or excess, has been captured successfully."),
	CHARGEBACK_PROCESSED("CHARGEBACK_PROCESSED", "A Chargeback has been processed against this order."),
	DISPUTED("DISPUTED", "The payment has been disputed and is under investigation. A request for information has been received or a chargeback is pending."),
	EXCESSIVELY_REFUNDED("EXCESSIVELY_REFUNDED", "The payment has been captured in part, full, or excess, but the captured amount in excess has been refunded successfully."),
	FAILED("FAILED", "The payment has not been successful."),
	FUNDING("FUNDING", "The order transfers money to or from the merchant, without the involvement of a payer. For example, recording monthly merchant service fees from your payment service provider."),
	INITIATED("INITIATED", "A browser payment that has successfully been initiated for this order. No payment has yet been made."),
	PARTIALLY_CAPTURED("PARTIALLY_CAPTURED", "The authorized amount for this order, in part, has been captured successfully."),
	PARTIALLY_REFUNDED("PARTIALLY_REFUNDED", "The payment has been captured in part, full, or excess, but the captured amount in part has been refunded successfully."),
	REFUNDED("REFUNDED", "The payment has been captured in part, full, or excess, but the captured amount in full has been refunded successfully."),
	REFUND_REQUESTED("REFUND_REQUESTED", "A refund against captured amounts on this order has been requested but not executed. Requires further action to approve the refund."),
	VERIFIED("VERIFIED", "The card details for this order have successfully been verified. No payment has yet been initiated or made.");
	// @formatter:on

	private String code;
	private String desc;

	/**
	 *
	 */
	private MpgsOrderStatus(final String code, final String desc)
	{
		this.code = code;
		this.desc = desc;
	}

	/**
	 * @return the code
	 */
	public String getCode()
	{
		return code;
	}

	/**
	 * @return the desc
	 */
	public String getDesc()
	{
		return desc;
	}

	public static boolean isValueOneOf(final MpgsOrderStatus value, final MpgsOrderStatus... statuses)
	{
		for (int a = 0; a < statuses.length; a++)
		{
			if (statuses[a].equals(value))
			{
				return true;
			}
		}
		return false;
	}

}
