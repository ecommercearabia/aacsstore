/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.mpgs.beans.incoming;

import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Authentication;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Order;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Response;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.SourceOfFunds;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans._3DTransaction;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class Initiate3DAuthenticationResponseBean extends MpgsResponse
{

	@Expose
	@SerializedName("authentication")
	private Authentication authentication;

	@Expose
	@SerializedName("merchant")
	private String merchant;

	@Expose
	@SerializedName("order")
	private Order order;

	@Expose
	@SerializedName("response")
	private Response response;

	@Expose
	@SerializedName("sourceOfFunds")
	private SourceOfFunds sourceOfFunds;

	@Expose
	@SerializedName("timeOfLastUpdate")
	private String timeOfLastUpdate;

	@Expose
	@SerializedName("timeOfRecord")
	private String timeOfRecord;

	@Expose
	@SerializedName("transaction")
	private _3DTransaction transaction;

	@Expose
	@SerializedName("version")
	private String version;

	public Initiate3DAuthenticationResponseBean()
	{
	}

	public Authentication getAuthentication()
	{
		return authentication;
	}

	public void setAuthentication(final Authentication authentication)
	{
		this.authentication = authentication;
	}

	public String getMerchant()
	{
		return merchant;
	}

	public void setMerchant(final String merchant)
	{
		this.merchant = merchant;
	}

	public Order getOrder()
	{
		return order;
	}

	public void setOrder(final Order order)
	{
		this.order = order;
	}

	public Response getResponse()
	{
		return response;
	}

	public void setResponse(final Response response)
	{
		this.response = response;
	}

	public SourceOfFunds getSourceOfFunds()
	{
		return sourceOfFunds;
	}

	public void setSourceOfFunds(final SourceOfFunds sourceOfFunds)
	{
		this.sourceOfFunds = sourceOfFunds;
	}

	public String getTimeOfLastUpdate()
	{
		return timeOfLastUpdate;
	}

	public void setTimeOfLastUpdate(final String timeOfLastUpdate)
	{
		this.timeOfLastUpdate = timeOfLastUpdate;
	}

	public String getTimeOfRecord()
	{
		return timeOfRecord;
	}

	public void setTimeOfRecord(final String timeOfRecord)
	{
		this.timeOfRecord = timeOfRecord;
	}

	public _3DTransaction getTransaction()
	{
		return transaction;
	}

	public void setTransaction(final _3DTransaction transaction)
	{
		this.transaction = transaction;
	}

	public String getVersion()
	{
		return version;
	}

	public void setVersion(final String version)
	{
		this.version = version;
	}

}
