package com.aacs.aacspayment.mpgs.service.impl;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.aacs.aacspayment.mpgs.beans.RetrieveSessionBean;
import com.aacs.aacspayment.mpgs.beans.incoming.AuthenticatePayerResponseBean;
import com.aacs.aacspayment.mpgs.beans.incoming.CreateCheckoutSessionResponseBean;
import com.aacs.aacspayment.mpgs.beans.incoming.Initiate3DAuthenticationResponseBean;
import com.aacs.aacspayment.mpgs.beans.incoming.MpgsResponse.MpgsResultEnum;
import com.aacs.aacspayment.mpgs.beans.incoming.OrderStatusResponse;
import com.aacs.aacspayment.mpgs.beans.incoming.OuterTransactionResponse;
import com.aacs.aacspayment.mpgs.beans.incoming.PayOrderResponseBean;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Authentication;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.BrowserDetails;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Device;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.Order;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.SessionBean;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.SourceOfFunds;
import com.aacs.aacspayment.mpgs.beans.outgoing.AuthenticatePayerRequestBean;
import com.aacs.aacspayment.mpgs.beans.outgoing.CaptureOrderBean;
import com.aacs.aacspayment.mpgs.beans.outgoing.CreateCheckoutSessionBean;
import com.aacs.aacspayment.mpgs.beans.outgoing.Initiate3DAuthenticationBean;
import com.aacs.aacspayment.mpgs.beans.outgoing.PayOrderRequestBean;
import com.aacs.aacspayment.mpgs.beans.outgoing.RefundTransactionBean;
import com.aacs.aacspayment.mpgs.beans.outgoing.VoidTransactionBean;
import com.aacs.aacspayment.mpgs.beans.serviceparams.PayServiceParams;
import com.aacs.aacspayment.mpgs.beans.serviceparams._3DSecureParams;
import com.aacs.aacspayment.mpgs.constants.MpgsConstants;
import com.aacs.aacspayment.mpgs.exception.MpgsException;
import com.aacs.aacspayment.mpgs.exception.type.MpgsExceptionType;
import com.aacs.aacspayment.mpgs.service.MpgsService;
import com.aacs.aacspayment.mpgs.utils.WebServiceApiUtil;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class DefaultMpgsServiceImpl implements MpgsService
{
	private static final String TRANSACTION_STRING = "/transaction/";
	private static final String CANCELLED = "CANCELLED";
	private static final String ORDER_STRING = "/order/";
	private static final GsonBuilder _builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
	private static final Gson GSON = _builder.setPrettyPrinting().create();
	private static final Logger LOG = LoggerFactory.getLogger(DefaultMpgsServiceImpl.class);

	static
	{
		_builder.serializeNulls();
	}

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Override
	public Optional<RetrieveSessionBean> getSessionInfo(final String sessionId, final MpgsConstants constants) throws MpgsException
	{
		final String url = constants.getBaseURL() + "/session/" + sessionId;

		final MultiValueMap<String, String> headers = createAuthHeader(constants);

		ResponseEntity<String> response = null;
		response = WebServiceApiUtil.httpGet(url, headers, String.class);


		if (!response.getStatusCode().is2xxSuccessful())
		{
			throw new MpgsException(MpgsExceptionType.UNKNOWN, response.getBody(), url);
		}

		final RetrieveSessionBean result = GSON.fromJson(response.getBody(), RetrieveSessionBean.class);


		System.out.println(response.getBody() + "/n" + result.toString());

		return Optional.ofNullable(result);

	}


	@Override
	public Optional<CreateCheckoutSessionResponseBean> createCheckoutSession(
			final CreateCheckoutSessionBean createCheckoutSessionBean, final MpgsConstants constants) throws MpgsException
	{
		createCheckoutSessionBean.getInteraction().setOperation("AUTHORIZE");
		final String url = constants.getBaseURL() + "/session";

		final MultiValueMap<String, String> headers = createAuthHeader(constants);

		final String requestBody = GSON.toJson(createCheckoutSessionBean).toString();
		ResponseEntity<String> response = null;
		response = WebServiceApiUtil.httpPost(url, requestBody, headers, String.class);
		if (!response.getStatusCode().is2xxSuccessful())
		{
			throw new MpgsException(MpgsExceptionType.UNKNOWN, response.getBody(), requestBody);
		}

		final CreateCheckoutSessionResponseBean result = GSON.fromJson(response.getBody(), CreateCheckoutSessionResponseBean.class);
		if (!MpgsResultEnum.SUCCESS.getResult().equals(result.getResult()))
		{
			throw MpgsException.getExceptionFromCode(result.getError().getCause(), response.getBody(), null);
		}

		return Optional.ofNullable(result);

	}

	@Override
	public Optional<OuterTransactionResponse> voidOrder(final String orderId, final MpgsConstants constants) throws MpgsException
	{

		// Get order first
		final Optional<OrderStatusResponse> orderOpt = getOrder(orderId, constants);
		if (orderOpt.isEmpty())
		{
			throw new MpgsException(MpgsExceptionType.UNKNOWN, "Order Doesn't Exist", null);
		}

		final OrderStatusResponse order = orderOpt.get();
		if (CANCELLED.equals(order.getStatus()))
		{
			return Optional.empty();
		}


		final VoidTransactionBean voidData = new VoidTransactionBean();
		voidData.setTransactionId("1");

		final String url = constants.getBaseURL() + ORDER_STRING + order.getId() + "/transaction/voidtransaction_"
				+ order.getTransactions().get(0).getTransaction().getId();

		final MultiValueMap<String, String> headers = createAuthHeader(constants);

		final String requestBody = GSON.toJson(voidData).toString();
		ResponseEntity<String> response = null;

		response = WebServiceApiUtil.httpPut(url, requestBody, headers, String.class);
		if (!response.getStatusCode().is2xxSuccessful())
		{
			throw new MpgsException(MpgsExceptionType.UNKNOWN, response.getBody(), requestBody);
		}

		final OuterTransactionResponse transaction = GSON.fromJson(response.getBody(), OuterTransactionResponse.class);
		if (!MpgsResultEnum.SUCCESS.getResult().equals(transaction.getResult()))
		{
			throw MpgsException.getExceptionFromCode(transaction.getError().getCause(), response.getBody(), null);
		}

		return Optional.ofNullable(transaction);
	}

	@Override
	public Optional<OuterTransactionResponse> authorizeOrder(final String orderId, final double amount, final String currency,
			final MpgsConstants constants) throws MpgsException
	{
		final Optional<OrderStatusResponse> orderOpt = getOrder(orderId, constants);
		if (orderOpt.isEmpty())
		{
			return Optional.empty();
		}

		final OrderStatusResponse order = orderOpt.get();
		if (CANCELLED.equals(order.getStatus()))
		{
			return Optional.empty();
		}

		final String url = constants.getBaseURL() + ORDER_STRING + order.getId() + "/transaction/captureOrder_"
				+ order.getTransactions().get(0).getTransaction().getId();

		final CaptureOrderBean captureBody = new CaptureOrderBean();
		captureBody.setTransactionAmount(amount);
		captureBody.setTransactionCurrency(currency);

		final MultiValueMap<String, String> headers = createAuthHeader(constants);

		final String requestBody = GSON.toJson(captureBody).toString();
		ResponseEntity<String> response = null;

		response = WebServiceApiUtil.httpPut(url, requestBody, headers, String.class);
		if (!response.getStatusCode().is2xxSuccessful())
		{
			throw new MpgsException(MpgsExceptionType.UNKNOWN, response.getBody(), requestBody);
		}

		final OuterTransactionResponse transaction = GSON.fromJson(response.getBody(), OuterTransactionResponse.class);
		if (!MpgsResultEnum.SUCCESS.getResult().equals(transaction.getResult()))
		{
			throw MpgsException.getExceptionFromCode(transaction.getError().getCause(), response.getBody(), null);
		}

		return Optional.ofNullable(transaction);

	}

	@Override
	public Optional<OuterTransactionResponse> captureOrder(final String orderId, final double amount, final String currency,
			final MpgsConstants constants) throws MpgsException
	{
		// Get order first
		final Optional<OrderStatusResponse> orderOpt = getOrder(orderId, constants);
		if (orderOpt.isEmpty())
		{
			return Optional.empty();
		}

		final OrderStatusResponse order = orderOpt.get();
		if (CANCELLED.equals(order.getStatus()))
		{
			throw new MpgsException(MpgsExceptionType.ORDER_CANCELLED, MpgsExceptionType.ORDER_CANCELLED.getMessage(),
					GSON.toJson(order).toString());
		}

		final String url = constants.getBaseURL() + ORDER_STRING + order.getId() + "/transaction/captureOrder_"
				+ order.getTransactions().get(0).getTransaction().getId();

		final CaptureOrderBean captureBody = new CaptureOrderBean();
		captureBody.setTransactionAmount(amount);
		captureBody.setTransactionCurrency(currency);

		final MultiValueMap<String, String> headers = createAuthHeader(constants);

		final String requestBody = GSON.toJson(captureBody).toString();
		ResponseEntity<String> response = null;

		try
		{
			response = WebServiceApiUtil.httpPut(url, requestBody, headers, String.class);
		}
		catch (final Exception e)
		{
			throw new MpgsException(MpgsExceptionType.INVALID_REQUEST, e.getMessage(), requestBody);
		}
		if (!response.getStatusCode().is2xxSuccessful())
		{
			throw new MpgsException(MpgsExceptionType.UNKNOWN, response.getBody(), requestBody);
		}

		final OuterTransactionResponse transaction = GSON.fromJson(response.getBody(), OuterTransactionResponse.class);
		if (!MpgsResultEnum.SUCCESS.getResult().equals(transaction.getResult()))
		{
			throw MpgsException.getExceptionFromCode(transaction.getError().getCause(), response.getBody(), null);
		}

		return Optional.ofNullable(transaction);

	}


	private MultiValueMap<String, String> createAuthHeader(final MpgsConstants constants)
	{
		final String header = WebServiceApiUtil.getBasicAuthHeader(constants.getAuthUsername(), constants.getPassword());
		final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add("Authorization", header);
		return headers;
	}


	@Override
	public Optional<OrderStatusResponse> getOrder(final String orderId, final MpgsConstants constants) throws MpgsException
	{
		final String url = constants.getBaseURL() + ORDER_STRING + orderId;
		final MultiValueMap<String, String> headers = createAuthHeader(constants);
		ResponseEntity<String> response = null;
		LOG.info("getOrder for order id {} , url{} , headers{}", orderId, url, GSON.toJson(headers));

		try
		{
			response = WebServiceApiUtil.httpGet(url, headers, String.class);
		}
		catch (final Exception e)
		{
			LOG.error("getOrder [{}] is faild! : INVALID_REQUEST  Message [{}]", orderId, e.getMessage());
			throw new MpgsException(MpgsExceptionType.INVALID_REQUEST, e.getMessage(), url);
		}
		if (!response.getStatusCode().is2xxSuccessful())
		{
			LOG.error("getOrder is not Successful for order id {}", orderId);
			LOG.error("GetOrder Error: [{}]", response.getBody());
			throw new MpgsException(MpgsExceptionType.ORDER_NOT_FOUND, response.getBody(), response.getBody());
		}
		final OrderStatusResponse orderStatusResponse = GSON.fromJson(response.getBody(), OrderStatusResponse.class);

		LOG.info("getOrder for order id {} , url{} , headers{} ,  Response :[{}]", orderId, url, GSON.toJson(headers),
				GSON.toJson(headers));

		if (!MpgsResultEnum.SUCCESS.getResult().equals(orderStatusResponse.getResult()))
		{
			LOG.error("getOrder [{}] is faild!  , url{} , headers{} ,  Response :[{}]", orderId, url, GSON.toJson(headers),
					GSON.toJson(headers));
			throw MpgsException.getExceptionFromCode(orderStatusResponse.getError().getCause(), response.getBody(), null);
		}
		return Optional.ofNullable(orderStatusResponse);

	}

	@Override
	public Optional<OuterTransactionResponse> getTransaction(final String orderId, final String transactonId,
			final MpgsConstants constants) throws MpgsException
	{
		final String url = constants.getBaseURL() + ORDER_STRING + orderId + TRANSACTION_STRING + transactonId;
		final MultiValueMap<String, String> headers = createAuthHeader(constants);
		ResponseEntity<String> response = null;

		response = WebServiceApiUtil.httpGet(url, headers, String.class);
		if (!response.getStatusCode().is2xxSuccessful())
		{
			throw new MpgsException(MpgsExceptionType.UNKNOWN, response.getBody(), url);
		}
		final OuterTransactionResponse transaction = GSON.fromJson(response.getBody(), OuterTransactionResponse.class);

		if (!MpgsResultEnum.SUCCESS.getResult().equals(transaction.getResult()))
		{
			throw MpgsException.getExceptionFromCode(transaction.getError().getCause(), response.getBody(), null);
		}
		return Optional.ofNullable(transaction);

	}

	@Override
	public Optional<OuterTransactionResponse> refundOrder(final String orderId, final double amount, final String currency,
			final MpgsConstants constants) throws MpgsException
	{
		// Get order first
		final Optional<OrderStatusResponse> orderOpt = getOrder(orderId, constants);
		if (orderOpt.isEmpty())
		{
			return Optional.empty();
		}

		final OrderStatusResponse order = orderOpt.get();
		final String url = constants.getBaseURL() + ORDER_STRING + order.getId() + "/transaction/refundOrder_"
				+ order.getTransactions().get(0).getTransaction().getId();

		final RefundTransactionBean refundBody = new RefundTransactionBean();
		refundBody.setTransactionAmount(amount);
		refundBody.setTransactionCurrency(currency);

		final MultiValueMap<String, String> headers = createAuthHeader(constants);

		final String requestBody = GSON.toJson(refundBody).toString();
		ResponseEntity<String> response = null;

		response = WebServiceApiUtil.httpPut(url, requestBody, headers, String.class);
		if (!response.getStatusCode().is2xxSuccessful())
		{
			throw new MpgsException(MpgsExceptionType.UNKNOWN, response.getBody(), requestBody);
		}

		final OuterTransactionResponse transaction = GSON.fromJson(response.getBody(), OuterTransactionResponse.class);

		if (!MpgsResultEnum.SUCCESS.getResult().equals(transaction.getResult()))
		{
			throw MpgsException.getExceptionFromCode(transaction.getError().getCause(), response.getBody(), null);
		}

		return Optional.ofNullable(transaction);


	}

	@Override
	public Optional<OuterTransactionResponse> getLastTransaction(final String orderId, final MpgsConstants constants)
			throws MpgsException
	{
		final Optional<OrderStatusResponse> order = this.getOrder(orderId, constants);
		if (order.isEmpty())
		{
			return Optional.empty();
		}
		final List<OuterTransactionResponse> transactions = order.get().getTransactions();
		return Optional.ofNullable(transactions.get(transactions.size() - 1));
	}

	@Override
	public Optional<List<OuterTransactionResponse>> getTransactions(final String orderId, final MpgsConstants constants)
			throws MpgsException
	{
		final Optional<OrderStatusResponse> order = this.getOrder(orderId, constants);
		if (order.isEmpty())
		{
			return Optional.empty();
		}
		return Optional.ofNullable(order.get().getTransactions());
	}

	@Override
	public Optional<OuterTransactionResponse> getLastTransaction(final OrderStatusResponse order) throws MpgsException
	{
		final List<OuterTransactionResponse> transactions = order.getTransactions();
		if (transactions == null || transactions.size() == 0)
		{
			return Optional.empty();
		}
		return Optional.ofNullable(transactions.get(transactions.size() - 1));
	}

	@Override
	public Optional<Initiate3DAuthenticationResponseBean> initiate3DSecureAuthentication(final _3DSecureParams params,
			final MpgsConstants constants) throws MpgsException
	{
		// no need to get the order since the order has not been created yet on MPGS
		Preconditions.checkArgument(params != null, "Initiate 3DSecure parameters are empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(params.getCurrency()), "Initiate 3DSecure currency parameter is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(params.getSessionId()),
				"Initiate 3DSecure session id parameter is empty");
		Preconditions.checkArgument(constants != null, "Initiate 3DSecure Mpgs Constants parameters are empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(constants.getAuthUsername()),
				"Initiate 3DSecure Mpgs Constants Auth username parameter is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(constants.getPassword()),
				"Initiate 3DSecure Mpgs Constants Auth password parameter is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(constants.getBaseURL()),
				"Initiate 3DSecure Mpgs Constants base URL parameter is empty");

		final String url = constants.getBaseURL() + ORDER_STRING + params.getOrderId() + TRANSACTION_STRING
				+ params.getUniqueTransactionId();

		final Initiate3DAuthenticationBean request = populateInitiate3DSecureRequest(params);

		final MultiValueMap<String, String> headers = createAuthHeader(constants);

		final String requestBody = GSON.toJson(request).toString();
		ResponseEntity<String> response = null;
		response = WebServiceApiUtil.httpPut(url, requestBody, headers, String.class);
		if (!response.getStatusCode().is2xxSuccessful())
		{
			throw new MpgsException(MpgsExceptionType.UNKNOWN, response.getBody(), requestBody);
		}

		final Initiate3DAuthenticationResponseBean transaction = GSON.fromJson(response.getBody(),
				Initiate3DAuthenticationResponseBean.class);

		return Optional.ofNullable(transaction);
	}

	@Override
	public Optional<AuthenticatePayerResponseBean> authenticate3DSecurePayer(final _3DSecureParams params,
			final MpgsConstants constants) throws MpgsException
	{
		// no need to get the order since the order has not been created yet on MPGS
		Preconditions.checkArgument(params != null, "Initiate 3DSecure parameters are empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(params.getOrderId()), "Initiate 3DSecure order ID parameter is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(params.getCurrency()), "Initiate 3DSecure currency parameter is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(params.getUniqueTransactionId()),
				"Initiate 3DSecure transaction ID parameter is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(params.getUniqueTransactionId()),
				"Initiate 3DSecure transaction ID parameter is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(params.getSessionId()),
				"Initiate 3DSecure session id parameter is empty");
		Preconditions.checkArgument(constants != null, "Initiate 3DSecure Mpgs Constants parameters are empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(constants.getAuthUsername()),
				"Initiate 3DSecure Mpgs Constants Auth username parameter is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(constants.getPassword()),
				"Initiate 3DSecure Mpgs Constants Auth password parameter is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(constants.getBaseURL()),
				"Initiate 3DSecure Mpgs Constants base URL parameter is empty");

		final String url = constants.getBaseURL() + ORDER_STRING + params.getOrderId() + TRANSACTION_STRING
				+ params.getUniqueTransactionId();

		final AuthenticatePayerRequestBean request = populate3DSecureAuthRequest(params);

		final MultiValueMap<String, String> headers = createAuthHeader(constants);

		final String requestBody = GSON.toJson(request).toString();
		ResponseEntity<String> response = null;
		response = WebServiceApiUtil.httpPut(url, requestBody, headers, String.class);
		if (!response.getStatusCode().is2xxSuccessful())
		{
			throw new MpgsException(MpgsExceptionType.UNKNOWN, response.getBody(), requestBody);
		}

		final AuthenticatePayerResponseBean transaction = GSON.fromJson(response.getBody(), AuthenticatePayerResponseBean.class);

		return Optional.ofNullable(transaction);
	}

	/**
	 *
	 */
	private Initiate3DAuthenticationBean populateInitiate3DSecureRequest(final _3DSecureParams params)
	{
		// populate authentication param
		final Authentication authentication = new Authentication();
		authentication.setAcceptVersions("3DS1,3DS2");
		authentication.setChannel("PAYER_BROWSER");
		authentication.setPurpose("PAYMENT_TRANSACTION");

		// populating order param
		final Order order = new Order();
		order.setCurrency(params.getCurrency());

		// populate session param
		final SessionBean session = new SessionBean();
		session.setId(params.getSessionId());

		final Initiate3DAuthenticationBean request = new Initiate3DAuthenticationBean();

		request.setAuthentication(authentication);
		request.setOrder(order);
		request.setSession(session);

		return request;
	}

	private AuthenticatePayerRequestBean populate3DSecureAuthRequest(final _3DSecureParams params)
	{
		// populate authentication param
		final Authentication authentication = new Authentication();
		authentication.setRedirectResponseUrl(params.getRedirectUrl());

		// populating order param
		final Order order = new Order();
		order.setCurrency(params.getCurrency());
		order.setAmount(String.valueOf(params.getAmount()));

		// populate session param
		final SessionBean session = new SessionBean();
		session.setId(params.getSessionId());

		final Device device = new Device();
		device.setBrowser("PHONE");
		device.setBrowserDetails(new BrowserDetails());

		final AuthenticatePayerRequestBean request = new AuthenticatePayerRequestBean();
		request.setOrder(order);
		request.setSession(session);
		request.setDevice(device);
		request.setAuthentication(authentication);
		return request;
	}

	@Override
	public Optional<PayOrderResponseBean> pay(final PayServiceParams params, final MpgsConstants constants) throws MpgsException
	{

		if (!StringUtils.isBlank(params.getAuthenticatedTransactioinId()))
		{
			// Get order first
			final Optional<OrderStatusResponse> orderOpt = getOrder(params.getOrderId(), constants);
			if (orderOpt.isEmpty())
			{
				LOG.error("Order Status Response is Empty for order: [{}]", params.getOrderId());
				throw new MpgsException(MpgsExceptionType.INVALID_REQUEST,
						"Order Status Response is Empty for order: [" + params.getOrderId() + "]", null);
			}
			LOG.info("get Order [{}] is successfully  OrderStatusResponse : [{}]", params.getOrderId(), GSON.toJson(orderOpt.get()));

		}

		// Prepare Pay Request
		LOG.info("Prepare Pay Request for order is {}", params.getOrderId());

		final PayOrderRequestBean request = createPayOrderRequest(params);

		final String url = constants.getBaseURL() + ORDER_STRING + params.getOrderId() + TRANSACTION_STRING
				+ params.getUniqueTransactionId();

		LOG.info("URL: {} ", url);

		final MultiValueMap<String, String> headers = createAuthHeader(constants);

		final String requestBody = GSON.toJson(request).toString();

		LOG.info("Transaction request Body: {}", requestBody);

		final String headresJson = GSON.toJson(headers);
		LOG.info("createPayOrderRequest for order[{}] :  url[{}], headers[{}] , requestBody [{}]", params.getOrderId(), url,
				headresJson, requestBody);


		ResponseEntity<String> response = null;
		response = WebServiceApiUtil.httpPut(url, requestBody, headers, String.class);

		LOG.info("Response Body: {}", response.getBody());


		if (!response.getStatusCode().is2xxSuccessful() || !response.hasBody())
		{

			LOG.error(
					"createPayOrderRequest for order[{}] is faild! MpgsExceptionType [UNKNOWN] :  url[{}], headers[{}] , requestBody , response[{}]",
					params.getOrderId(), url, headresJson, requestBody, response.hasBody() ? response.getBody() : "null", requestBody);
			throw new MpgsException(MpgsExceptionType.UNKNOWN, response.hasBody() ? response.getBody() : "null", requestBody);
		}

		LOG.info("createPayOrderRequest for order[{}] :  url[{}], headers[{}] , requestBody , response[{}]", params.getOrderId(),
				url, headresJson, requestBody, response.getBody());

		final PayOrderResponseBean payResponse = GSON.fromJson(response.getBody(), PayOrderResponseBean.class); // NOSONAR Checked above

		LOG.info("Pay Order Respons Body: {}", payResponse);

		if (payResponse.getError() != null || payResponse.getResponse() == null
				|| !"APPROVED".equals(payResponse.getResponse().getGatewayCode()))
		{
			final String errorMessage = getErrorMessageFromPayResponse(payResponse);
			throw MpgsException.getExceptionFromCode(errorMessage, response.getBody(), null);
		}

		return Optional.ofNullable(payResponse);
	}

	private String getErrorMessageFromPayResponse(final PayOrderResponseBean payResponse)
	{
		final StringBuilder sb = new StringBuilder();

		if (payResponse.getError() != null)
		{
			sb.append(payResponse.getError().getCause());
			return sb.toString();
		}

		sb.append("Transaction status was ");
		if (payResponse.getResponse() != null)
		{
			sb.append(payResponse.getResponse().getGatewayCode());
		}
		else
		{
			sb.append("Empty");
		}

		return sb.toString();
	}

	/**
	 *
	 */
	private PayOrderRequestBean createPayOrderRequest(final PayServiceParams params)
	{
		Authentication authentication = null;
		if (!StringUtils.isBlank(params.getAuthenticatedTransactioinId()))
		{
			authentication = new Authentication();
			authentication.setTransactionId(params.getAuthenticatedTransactioinId());
		}

		final Order orderParam = new Order();
		orderParam.setAmount(String.valueOf(params.getAmount()));
		orderParam.setCurrency(params.getCurrency());

		final SessionBean session = new SessionBean();
		session.setId(params.getSessionId());

		final SourceOfFunds source = new SourceOfFunds();
		source.setType("CARD");

		final PayOrderRequestBean payRequest = new PayOrderRequestBean();
		payRequest.setAuthentication(authentication);
		payRequest.setOrder(orderParam);
		payRequest.setSession(session);
		payRequest.setSourceOfFunds(source);

		return payRequest;
	}



}

