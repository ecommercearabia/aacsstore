package com.aacs.aacspayment.mpgs.service;

import java.util.List;
import java.util.Optional;

import com.aacs.aacspayment.mpgs.beans.RetrieveSessionBean;
import com.aacs.aacspayment.mpgs.beans.incoming.AuthenticatePayerResponseBean;
import com.aacs.aacspayment.mpgs.beans.incoming.CreateCheckoutSessionResponseBean;
import com.aacs.aacspayment.mpgs.beans.incoming.Initiate3DAuthenticationResponseBean;
import com.aacs.aacspayment.mpgs.beans.incoming.OrderStatusResponse;
import com.aacs.aacspayment.mpgs.beans.incoming.OuterTransactionResponse;
import com.aacs.aacspayment.mpgs.beans.incoming.PayOrderResponseBean;
import com.aacs.aacspayment.mpgs.beans.outgoing.CreateCheckoutSessionBean;
import com.aacs.aacspayment.mpgs.beans.serviceparams.PayServiceParams;
import com.aacs.aacspayment.mpgs.beans.serviceparams._3DSecureParams;
import com.aacs.aacspayment.mpgs.constants.MpgsConstants;
import com.aacs.aacspayment.mpgs.exception.MpgsException;


public interface MpgsService
{

	Optional<RetrieveSessionBean> getSessionInfo(String sessionId, MpgsConstants constants) throws MpgsException;


	Optional<CreateCheckoutSessionResponseBean> createCheckoutSession(CreateCheckoutSessionBean createCheckoutSessionBean,
			MpgsConstants constants) throws MpgsException;

	Optional<OuterTransactionResponse> voidOrder(String orderId, MpgsConstants constants) throws MpgsException;

	Optional<OuterTransactionResponse> getTransaction(String orderId, String transactonId, MpgsConstants constants)
			throws MpgsException;

	Optional<OuterTransactionResponse> getLastTransaction(String orderId, MpgsConstants constants) throws MpgsException;

	Optional<List<OuterTransactionResponse>> getTransactions(String orderId, MpgsConstants constants) throws MpgsException;

	Optional<OrderStatusResponse> getOrder(String orderId, MpgsConstants constants) throws MpgsException;

	Optional<OuterTransactionResponse> authorizeOrder(String orderId, double amount, String currency, MpgsConstants constants)
			throws MpgsException;

	Optional<OuterTransactionResponse> captureOrder(String orderId, double amount, String currency, MpgsConstants constants)
			throws MpgsException;

	Optional<OuterTransactionResponse> refundOrder(String orderId, double amount, String currency, MpgsConstants constants)
			throws MpgsException;

	Optional<OuterTransactionResponse> getLastTransaction(OrderStatusResponse order) throws MpgsException;

	Optional<Initiate3DAuthenticationResponseBean> initiate3DSecureAuthentication(_3DSecureParams params,
			final MpgsConstants constants) throws MpgsException;

	Optional<AuthenticatePayerResponseBean> authenticate3DSecurePayer(_3DSecureParams params, final MpgsConstants constants)
			throws MpgsException;

	Optional<PayOrderResponseBean> pay(final PayServiceParams params, final MpgsConstants constants) throws MpgsException;

}
