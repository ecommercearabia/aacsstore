package com.aacs.aacspayment.mpgs.constants;

public class MpgsConstants
{

	// TODO: Provider Model
	//	public static final String MERCHANTID = "TEST999999345";
	//	public static final String OPERATORID = "merchadmin";
	//	public static final String PASSWORD = "22d5880f940c3a348b2f306e460d3140";
	//	public static final String AUTH_USER_NAME = "merchant." + MERCHANTID;
	//	public static final String MASTER_CART_API_VERSION = "57";
	//	public static final String BASE_URL = "https://ap-gateway.mastercard.com/api/rest/version/"	+ MASTER_CART_API_VERSION + "/merchant/" + MERCHANTID;


	public final String merchantID;
	public final String password;
	public final String authUsername;
	public final String mpgsApiVersion;
	public final String baseURL;
	public final String javaScriptCode;

	/**
	 *
	 */
	public MpgsConstants(final String merchantid, final String password, final String mpgsApiVersion, final String javaScriptCode,
			final String baseURL)
	{
		super();
		this.merchantID = merchantid;
		this.password = password;
		this.authUsername = "merchant." + merchantid;
		this.mpgsApiVersion = mpgsApiVersion;
		//this.baseURL = String.format("https://ap-gateway.mastercard.com/api/rest/version/%s/merchant/%s", mpgsApiVersion , merchantID);
		this.baseURL = String.format(baseURL, mpgsApiVersion, merchantID);
		//this.javaScriptCode = String.format("https://ap-gateway.mastercard.com/checkout/version/%s/checkout.js", mpgsApiVersion);
		this.javaScriptCode = String.format(javaScriptCode, mpgsApiVersion);
	}

	/**
	 * @return the javaScriptCode
	 */
	public String getJavaScriptCode()
	{
		return javaScriptCode;
	}

	/**
	 * @return the merchantID
	 */
	public String getMerchantID()
	{
		return merchantID;
	}

	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @return the authUsername
	 */
	public String getAuthUsername()
	{
		return authUsername;
	}

	/**
	 * @return the mpgsApiVersion
	 */
	public String getMpgsApiVersion()
	{
		return mpgsApiVersion;
	}

	/**
	 * @return the baseURL
	 */
	public String getBaseURL()
	{
		return baseURL;
	}


}
