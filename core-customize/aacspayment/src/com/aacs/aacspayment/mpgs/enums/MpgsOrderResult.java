/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.mpgs.enums;

/**
 *
 */
public enum MpgsOrderResult
{
	CAPTURED("captured"), AUTHORIZED("authorized"), CANCELLED("cancelled");

	private final String code;

	/**
	 *
	 */
	private MpgsOrderResult(final String code)
	{
		this.code = code;
	}

	/**
	 * @return the code
	 */
	public String getCode()
	{
		return code;
	}

}
