/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacspayment.exception.type;

public enum PaymentExceptionType
{
	// @formatter:off
	BAD_REQUEST("Bad Request"),
	INVALID_RESPONSE_SYNTAX_EXPECTED_JSON("Invalid response syntax,bad json syntax"),
	THE_ORDER_IS_ALLREADY_CANCELED("The order is allready canceled"),
	ORDER_IS_AUTHORIZED("The order is allready authorized"),
	TRACKING_NUMBER_DOES_NOT_EXIST("Tracking number does not exist"),
	THE_ORDER_IS_ALLREADY_CAPTUARED("The order is allready captuared"),
	CAN_NOT_CANCEL_CAPTUARED_ORDER("Can not cancel captuared order please refund first"),
	THE_ORDER_IS_ALLREADY_REFUNDED("The order is allready refunded or refund limit exceeds"),
	THE_ORDER_IS_NOT_AUTHORIZED("The order is not authorized"),
	THE_ORDER_IS_NOT_CAPTUARED("The order is not captuared yet."),
	UNKNOWN_ORDER_STATUS("The order status  not unknown"),
	INVALID_ORDER_STATUS("Invalid order status "),
   INVALID_SESSION_ID("Invalid session id"),
   INVALID_SESSION_AMOUNT("Invalid session amount"),
	INVALID_PARAMETER_FORMAT("Invalid Parameter Format"),
	SERVER_ERROR("Server Error"),
	CLIENT_ERROR("Client Error"),
	INVALID_CONSIGNMENT_ENTRIES("Invalid consginment entries do not match order entries"),
	ORDER_IS_ALREADY_PAID("Order Is Already Paid"),	PAYMENT_PROVIDER_DOES_NOT_EXIST("Payment Provider does not exist"),
	ORDER_STATUS_IS_CANCELLED("the order is cancelled");
;
	// @formatter:on

	String message;


	private PaymentExceptionType(final String message)
	{
		this.message = message;

	}


	public String getMessage()
	{
		return message;
	}



}