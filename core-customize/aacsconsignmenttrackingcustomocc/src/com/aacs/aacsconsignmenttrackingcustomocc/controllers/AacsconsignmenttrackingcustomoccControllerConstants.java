/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsconsignmenttrackingcustomocc.controllers;

/**
 */
public interface AacsconsignmenttrackingcustomoccControllerConstants
{
	// implement here controller constants used by this extension
}
