/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsconsignmenttrackingcustomocc.jalo;

import com.aacs.aacsconsignmenttrackingcustomocc.constants.AacsconsignmenttrackingcustomoccConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class AacsconsignmenttrackingcustomoccManager extends GeneratedAacsconsignmenttrackingcustomoccManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( AacsconsignmenttrackingcustomoccManager.class.getName() );
	
	public static final AacsconsignmenttrackingcustomoccManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AacsconsignmenttrackingcustomoccManager) em.getExtension(AacsconsignmenttrackingcustomoccConstants.EXTENSIONNAME);
	}
	
}
