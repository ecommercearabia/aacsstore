/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsconsignmenttrackingcustomocc.constants;

/**
 * Global class for all aacsconsignmenttrackingcustomocc constants. You can add global constants for your extension into this
 * class.
 */
@SuppressWarnings(
{ "deprecation", "squid:CallToDeprecatedMethod" })
public final class AacsconsignmenttrackingcustomoccConstants extends GeneratedAacsconsignmenttrackingcustomoccConstants
{
	public static final String EXTENSIONNAME = "aacsconsignmenttrackingcustomocc"; //NOSONAR

	private AacsconsignmenttrackingcustomoccConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
