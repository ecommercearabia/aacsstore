/**
 *
 */
package com.aacs.core.context;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.aacs.core.enums.SerialNumberSource;


/**
 * @author monzer
 *
 */
public interface SerialNumberConfigurationContext
{


	/**
	 * Generate invoice number by current store.
	 *
	 * @return the optional
	 */
	Optional<String> generateSerialNumberByCurrentStore(SerialNumberSource source);

	/**
	 * Generate invoice number for base store.
	 *
	 * @param baseStore
	 *           the base store
	 * @return the optional
	 */
	Optional<String> generateSerialNumberForBaseStore(BaseStoreModel baseStore, SerialNumberSource source);

}
