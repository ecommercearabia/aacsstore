/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.core.service;

import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.io.ByteArrayOutputStream;
import java.io.IOException;


/**
 * The Interface BarcodeGenaratorService.
 * @author monzer
 */
public interface BarcodeGenaratorService
{

	/**
	 * Genarate code 128 barcode.
	 *
	 * @param value the value
	 * @return ByteArrayOutputStream representing the image.
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public ByteArrayOutputStream genarateCode128Barcode(String value) throws IOException;

	/**
	 * Method creates MediaModel object for storing barcode.
	 *
	 * @param consignment           - to use trackingID and display it under the barcode.
	 * @return created MediaModel object
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public MediaModel generateBarcodeAsMedia(ConsignmentModel consignment) throws IOException;
}
