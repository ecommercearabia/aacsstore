/**
 *
 */
package com.aacs.core.service;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import com.aacs.aacsfacades.units.UnitFactorRangeData;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public interface UnitFactorRangeDataFactory
{


	List<UnitFactorRangeData> create(final ProductModel model);
}
