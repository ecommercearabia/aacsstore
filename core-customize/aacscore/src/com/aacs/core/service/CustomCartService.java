/**
 *
 */
package com.aacs.core.service;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.CartService;

import java.util.List;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface CustomCartService extends CartService
{
	CartModel createCartFromAbstractOrder(final OrderModel order);

	/**
	 * @param id
	 * @return
	 */
	List<CartModel> getCartByMerchantTransactionId(String id);
}
