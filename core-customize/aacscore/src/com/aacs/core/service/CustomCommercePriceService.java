/**
 *
 */
package com.aacs.core.service;

import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;


/**
 * @author mbaker
 *
 */
public interface CustomCommercePriceService extends CommercePriceService
{

	PriceInformation getWebPriceForProductWithTax(ProductModel productModel);

	PriceInformation getWebPriceForProductWithTax(ProductModel productModel, boolean withTax);

}
