/**
 *
 */
package com.aacs.core.service;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.PriceService;

import java.util.List;


/**
 * @author mbaker
 *
 */
public interface CustomPriceService extends PriceService
{

	public List<PriceInformation> getPriceInformationsForProductWithTax(final ProductModel model);

}
