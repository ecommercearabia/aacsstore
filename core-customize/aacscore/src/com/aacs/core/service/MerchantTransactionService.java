/**
 *
 */
package com.aacs.core.service;

import com.aacs.core.model.MerchantTransactionModel;


/**
 * @author husam.dababneh@eraba.com
 *
 */
public interface MerchantTransactionService
{
	MerchantTransactionModel getMerchantTransactionById(final String id);

	void removeMerchantTransaction(final MerchantTransactionModel model);
}
