/**
 *
 */
package com.aacs.core.service;

import de.hybris.platform.core.model.order.OrderModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface CustomOrderService
{
	OrderModel getOrderForCode(final String orderCode);
}
