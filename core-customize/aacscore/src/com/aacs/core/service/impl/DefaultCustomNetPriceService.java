/**
 *
 */
package com.aacs.core.service.impl;

import de.hybris.platform.commerceservices.price.impl.NetPriceService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;

import java.util.List;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class DefaultCustomNetPriceService extends NetPriceService
{
	@Override
	public List<PriceInformation> getPriceInformationsForProduct(final ProductModel model)
	{
		return super.getPriceInformationsForProduct(model);
	}


}
