/**
 *
 */
package com.aacs.core.service.impl;

import de.hybris.platform.acceleratorservices.order.impl.DefaultCartServiceForAccelerator;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.type.TypeService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.aacs.core.dao.CustomCartDao;
import com.aacs.core.service.CustomCartService;
import com.google.common.base.Preconditions;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultCustomCartServiceForAccelerator extends DefaultCartServiceForAccelerator implements CustomCartService
{
	private TypeService typeService;

	protected KeyGenerator keyGenerator;

	@Resource(name = "customCartDao")
	private CustomCartDao customCartDao;

	@Resource(name = "commerceCheckoutService")
	private CommerceCheckoutService commerceCheckoutService;


	/**
	 * @return the commerceCheckoutService
	 */
	protected CommerceCheckoutService getCommerceCheckoutService()
	{
		return commerceCheckoutService;
	}

	@Override
	public List<CartModel> getCartByMerchantTransactionId(final String merchantTransactionId)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(merchantTransactionId), "merchantTransactionId must not be null");

		final String[] split = merchantTransactionId.split("_");
		final String orderCode = split[0];
		return getCustomCartDao().getCartByOrderCode(orderCode);
	}

	@Override
	public CartModel createCartFromAbstractOrder(final OrderModel order)
	{
		return super.clone(getTypeService().getComposedTypeForClass(CartModel.class),
				getTypeService().getComposedTypeForClass(CartEntryModel.class), order, getKeyGenerator().generate().toString());
	}

	@Override
	protected CartModel internalGetSessionCart()
	{
		final CartModel cart = super.internalGetSessionCart();
		if (isSameDate(cart))
		{
			return cart;
		}
		return refreshCartCalculation(cart);
	}


	/**
	 * @param cart
	 * @return
	 */
	private boolean isSameDate(final CartModel cart)
	{
		final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
		final String currentDate = dateFormatter.format(new Date());
		final String cartDate = dateFormatter.format(cart.getDate());
		return cartDate.equals(currentDate);
	}

	private CartModel refreshCartCalculation(final CartModel cartModel)
	{
		cartModel.setDate(new Date());
		cartModel.setCalculated(Boolean.FALSE);
		getModelService().save(cartModel);
		getCommerceCheckoutService().calculateCart(cartModel);
		getModelService().save(cartModel);
		getModelService().refresh(cartModel);
		return cartModel;

	}


	public TypeService getTypeService()
	{
		return typeService;
	}

	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}

	public KeyGenerator getKeyGenerator()
	{
		return keyGenerator;
	}

	public void setKeyGenerator(final KeyGenerator keyGenerator)
	{
		this.keyGenerator = keyGenerator;
	}

	/**
	 * @return the customCartDao
	 */
	public CustomCartDao getCustomCartDao()
	{
		return customCartDao;
	}


}
