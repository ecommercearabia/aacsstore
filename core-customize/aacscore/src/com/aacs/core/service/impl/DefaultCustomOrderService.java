/**
 *
 */
package com.aacs.core.service.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.impl.DefaultOrderService;

import org.springframework.beans.factory.annotation.Autowired;

import com.aacs.core.dao.CustomOrderDAO;
import com.aacs.core.service.CustomOrderService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultCustomOrderService extends DefaultOrderService implements CustomOrderService
{
	private CustomOrderDAO customOrderDAO;

	@Override
	public OrderModel getOrderForCode(final String orderCode)
	{
		return getCustomOrderDAO().findOrderByCode(orderCode);
	}

	public CustomOrderDAO getCustomOrderDAO()
	{
		return customOrderDAO;
	}

	@Autowired
	public void setCustomOrderDAO(final CustomOrderDAO customOrderDAO)
	{
		this.customOrderDAO = customOrderDAO;
	}

}
