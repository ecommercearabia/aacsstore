/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.core.service.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.aacs.core.service.BarcodeGenaratorService;
import com.onbarcode.barcode.Code128;


/**
 * The Class DefaultBarcodeGenaratorService.
 *
 * @author monzer
 */
@Service
public class DefaultBarcodeGenaratorService implements BarcodeGenaratorService
{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultBarcodeGenaratorService.class);

	/** The model service. */
	@Resource(name = "modelService")
	private ModelService modelService;

	/** The media service. */
	@Resource(name = "mediaService")
	private MediaService mediaService;

	/** The base site service. */
	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	/** The catalog version service. */
	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	/**
	 * Genarate code 128 barcode.
	 *
	 * @param value
	 *           the value
	 * @return the byte array output stream
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 */
	@Override
	public ByteArrayOutputStream genarateCode128Barcode(final String value) throws IOException
	{
		if (value == null || value.isEmpty())
		{
			throw new IllegalArgumentException("The value can not be null");
		}

		final Code128 barcode = new Code128();

		// Set barcode data text to encode
		barcode.setData(value);
		barcode.setLeftMargin(7f);
		barcode.setRightMargin(7f);
		barcode.setBottomMargin(7f);
		barcode.setTopMargin(7);
		barcode.setBarcodeWidth(100f);
		barcode.setBarcodeHeight(60f);
		barcode.setAutoResize(true);

		final ByteArrayOutputStream out = new ByteArrayOutputStream();

		try
		{
			barcode.drawBarcode(out);
		}
		catch (final Exception e)
		{
			throw new IOException(e.getMessage());
		}

		return out;
	}

	/**
	 * Generate barcode as media.
	 *
	 * @param consignment
	 *           the consignment
	 * @return the media model
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 */
	@Override
	public MediaModel generateBarcodeAsMedia(final ConsignmentModel consignment) throws IOException
	{
		if (consignment == null)
		{
			throw new IllegalArgumentException("The consignment can not be null");
		}
		ByteArrayOutputStream byteArray = null;
		try
		{
			byteArray = genarateCode128Barcode(consignment.getTrackingID());
		}
		catch (final IllegalArgumentException e)
		{
			byteArray = genarateCode128Barcode(" ");
		}


		final MediaModel media = modelService.create(MediaModel.class);
		final String mediaName = consignment.getTrackingID() + "-" + LocalDateTime.now().toString();
		media.setCode(mediaName);
		media.setMime("image/x-png");
		media.setRealFileName(mediaName);
		final AbstractOrderModel order = consignment.getOrder();
		modelService.refresh(order);

		try
		{
			media.setCatalogVersion(catalogVersionService.getCatalogVersion("Default", "Online"));
			modelService.save(media);
			modelService.refresh(media);
			modelService.refresh(consignment);
			consignment.setBarcode(media);
			modelService.save(consignment);
		}
		catch (final Exception e)
		{
			LOG.error("Could not set CatalogVersionModel on MediaModel", e);
			return null;
		}

		final MediaFolderModel mediaFolderModel = getDocumentMediaFolder();

		try (InputStream dataStream = new ByteArrayInputStream(byteArray.toByteArray());)
		{
			mediaService.setStreamForMedia(media, dataStream, mediaName, "image/x-png", mediaFolderModel);
		}
		catch (final Exception e)
		{
			LOG.error("could not generate barcode from byteArray", e);
		}

		return media;
	}


	/**
	 * Gets the {@link MediaFolderModel} to save the generated Media.
	 *
	 * @return the {@link MediaFolderModel}
	 */
	protected MediaFolderModel getDocumentMediaFolder()
	{
		return mediaService.getFolder("documents");
	}
}
