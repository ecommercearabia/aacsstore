/**
 *
 */
package com.aacs.core.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commerceservices.price.impl.DefaultCommercePriceService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;

import java.util.List;

import org.springframework.util.CollectionUtils;

import com.aacs.core.service.CustomCommercePriceService;
import com.aacs.core.service.CustomPriceService;

/**
 * @author mbaker
 *
 */
public class DefaultCustomCommercePriceService extends DefaultCommercePriceService implements CustomCommercePriceService
{
	private CustomPriceService priceService;

	@Override
	public PriceInformation getWebPriceForProduct(final ProductModel product)
	{
		validateParameterNotNull(product, "Product model cannot be null");
		final List<PriceInformation> prices = getPriceService().getPriceInformationsForProductWithTax(product);
		if (!CollectionUtils.isEmpty(prices))
		{
			PriceInformation minPriceForLowestQuantity = null;
			for (final PriceInformation price : prices)
			{
				if (minPriceForLowestQuantity == null || (((Long) minPriceForLowestQuantity.getQualifierValue("minqtd"))
						.longValue() > ((Long) price.getQualifierValue("minqtd")).longValue()))
				{
					minPriceForLowestQuantity = price;
				}
			}
			return minPriceForLowestQuantity;
		}
		return null;
	}

	@Override
	public PriceInformation getWebPriceForProductWithTax(final ProductModel productModel)
	{
		return this.getWebPriceForProduct(productModel);
	}

	@Override
	public PriceInformation getWebPriceForProductWithTax(final ProductModel productModel, final boolean withTax)
	{
		if (withTax)
		{
			return this.getWebPriceForProductWithTax(productModel);
		}
		else
		{
			return super.getWebPriceForProduct(productModel);
		}
	}

	/**
	 * @return the priceService
	 */
	@Override
	public CustomPriceService getPriceService()
	{
		return priceService;
	}

	/**
	 * @param priceService
	 *           the priceService to set
	 */
	public void setPriceService(final CustomPriceService priceService)
	{
		this.priceService = priceService;
	}
}
