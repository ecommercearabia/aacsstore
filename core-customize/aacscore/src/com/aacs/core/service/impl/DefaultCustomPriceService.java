/**
 *
 */
package com.aacs.core.service.impl;

import de.hybris.platform.commerceservices.price.impl.NetPriceService;
import de.hybris.platform.commerceservices.strategies.NetGrossStrategy;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;

import java.util.List;

import com.aacs.core.service.CustomPriceService;


/**
 * @author mbaker
 *
 */
public class DefaultCustomPriceService extends NetPriceService implements CustomPriceService
{

	private NetGrossStrategy netGrossStrategy;
	private TimeService timeService;
	private ModelService modelService;

	@Override
	public List<PriceInformation> getPriceInformationsForProductWithTax(final ProductModel model)
	{
		final boolean net = false;
		final Product product = (Product) getModelService().getSource(model);
		try
		{
			return product.getPriceInformations(timeService.getCurrentTime(), net);
		}
		catch (final JaloPriceFactoryException e)
		{
			throw new SystemException(e.getMessage(), e);
		}
	}

	/**
	 * @return the netGrossStrategy
	 */
	@Override
	public NetGrossStrategy getNetGrossStrategy()
	{
		return netGrossStrategy;
	}

	/**
	 * @param netGrossStrategy
	 *           the netGrossStrategy to set
	 */
	@Override
	public void setNetGrossStrategy(final NetGrossStrategy netGrossStrategy)
	{
		this.netGrossStrategy = netGrossStrategy;
	}

	/**
	 * @return the timeService
	 */
	public TimeService getTimeService()
	{
		return timeService;
	}

	/**
	 * @param timeService
	 *           the timeService to set
	 */
	@Override
	public void setTimeService(final TimeService timeService)
	{
		this.timeService = timeService;
	}

	/**
	 * @return the modelService
	 */
	@Override
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Override
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

}