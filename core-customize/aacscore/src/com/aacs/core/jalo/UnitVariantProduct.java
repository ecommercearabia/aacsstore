package com.aacs.core.jalo;

import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.jalo.order.price.PriceFactory;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.servicelayer.time.TimeService;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.aacs.aacscustompricefactory.catalog.jalo.CustomEurope1PriceFactory;


public class UnitVariantProduct extends GeneratedUnitVariantProduct
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(UnitVariantProduct.class.getName());

	@Resource(name = "timeService")
	private TimeService timeService;

	@Override
	protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
			throws JaloBusinessException
	{
		// business code placed here will be executed before the item is created
		// then create the item
		final Item item = super.createItem(ctx, type, allAttributes);
		// business code placed here will be executed after the item was created
		// and return the item
		return item;
	}


	public List<PriceRowModel> getPriceRowModels(final Date date, final boolean isNet) throws JaloPriceFactoryException
	{
		final PriceFactory priceFactory = this.getSession().getOrderManager().getPriceFactory();

		if (priceFactory instanceof CustomEurope1PriceFactory)
		{
			final CustomEurope1PriceFactory aacsPriceFactory = (CustomEurope1PriceFactory) priceFactory;
			return aacsPriceFactory.getPriceRowModels(this.getSession().getSessionContext(), this, isNet, date);
		}
		return Collections.emptyList();
	}

	public PriceRowModel getSelectedPriceRowModel(final Date date, final boolean isNet) throws JaloPriceFactoryException
	{
		final List<PriceRowModel> priceRowModels = getPriceRowModels(date, isNet);

		PriceRowModel result = null;
		for (final PriceRowModel priceRow : priceRowModels)
		{
			if (result == null || result.getMinqtd() > priceRow.getMinqtd())
			{
				result = priceRow;
			}
		}

		return result;
	}

}
