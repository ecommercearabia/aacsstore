/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.core.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.aacs.core.constants.AacsCoreConstants;
import com.aacs.core.setup.CoreSystemSetup;


/**
 * Do not use, please use {@link CoreSystemSetup} instead.
 * 
 */
public class AacsCoreManager extends GeneratedAacsCoreManager
{
	public static final AacsCoreManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AacsCoreManager) em.getExtension(AacsCoreConstants.EXTENSIONNAME);
	}
}
