/**
 *
 */
package com.aacs.core.job;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.model.synchronization.CatalogVersionSyncCronJobModel;
import de.hybris.platform.catalog.model.synchronization.CatalogVersionSyncJobModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.helper.ProductAndCategoryHelper;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.JobModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aacs.core.model.ProductReferencesCronJobModel;


/**
 * @author mbaker
 *
 */
public class ProductReferencesJob extends AbstractJobPerformable<ProductReferencesCronJobModel>
{

	private static final String STAGED = "Staged";
	private static final String ONLINE = "Online";

	protected static final Logger LOG = LoggerFactory.getLogger(ProductReferencesJob.class);

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	@Resource(name = "productAndCategoryHelper")
	private ProductAndCategoryHelper productAndCategoryHelper;

	@Resource(name = "cronJobService")
	private CronJobService cronJobService;

	@Override
	public PerformResult perform(final ProductReferencesCronJobModel cronjob)
	{

		if (Objects.isNull(cronjob))
		{
			LOG.error("Cronjob model is empty!");
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}

		if (Objects.isNull(cronjob.getProductCatalogVersion()))
		{
			LOG.error("ProductCatalogVersion  is empty!");
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}
		if (Objects.isNull(cronjob.getProductReferenceType()))
		{
			LOG.error("ProductReferenceType  is empty!");
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}

		getCatalogVersionService().setSessionCatalogVersion(cronjob.getProductCatalogVersion().getCatalog().getId(), STAGED);

		final Set<ProductModel> products = getProducts(cronjob);

		if (CollectionUtils.isEmpty(products))
		{
			LOG.error("No Products !");
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}


		for (final ProductModel product : products)
		{
			changeProductReference(cronjob, product);
		}

		synchronizeCatalog(cronjob);

		LOG.info("Done");

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	/**
	 * @param cronjob
	 * @param product
	 */
	private void changeProductReference(final ProductReferencesCronJobModel cronjob, final ProductModel product)
	{
		final CategoryModel lastCategory = getLastCategory(product);

		if (Objects.isNull(lastCategory))
		{
			LOG.warn("Couldn't get the last category for product {}", product.getCode());
			return;
		}
		final List<ProductModel> productsForCategory = getProductService().getProductsForCategory(lastCategory);

		if (CollectionUtils.isEmpty(productsForCategory))
		{
			LOG.warn("No Products found for category {}", lastCategory.getCode());
			return;
		}
		final List<ProductModel> approvedProducts = productsForCategory.stream()
				.filter(e -> ArticleApprovalStatus.APPROVED.equals(e.getApprovalStatus())).collect(Collectors.toList());
		final List<ProductModel> possibleProducts = new ArrayList<>(approvedProducts);
		possibleProducts.remove(product);
		int size = possibleProducts.size();
		final long maxNumberOfProducts = cronjob.getMaxNumberOfProducts();

		if (maxNumberOfProducts <= 0)
		{
			LOG.warn("Max number of products to add is 0");
			return;
		}

		if (size <= maxNumberOfProducts)
		{
			createProductReferences(possibleProducts, product, cronjob.getProductReferenceType());
			return;
		}

		final List<ProductModel> refernecedProducts = new LinkedList<>();

		final Random rand = new Random(System.nanoTime());
		while (refernecedProducts.size() < maxNumberOfProducts)
		{
			LOG.warn("Selecting references for product {} from category {} --> name {}, possible options: {}", product.getCode(),
					lastCategory.getCode(), lastCategory.getName(), size);
			final int num = rand.nextInt(size - 1);
			final ProductModel randomProduct = possibleProducts.get(num);
			if (!product.equals(randomProduct) && !refernecedProducts.contains(randomProduct))
			{
				refernecedProducts.add(randomProduct);
				possibleProducts.remove(randomProduct);
				size--;
			}
		}

		createProductReferences(refernecedProducts, product, cronjob.getProductReferenceType());

	}

	/**
	 * @param productsForCategory
	 * @param product
	 * @param productReferenceType
	 */
	private void createProductReferences(final List<ProductModel> refernecedProducts, final ProductModel product,
			final ProductReferenceTypeEnum productReferenceType)
	{
		final Collection<ProductReferenceModel> productReferences = product.getProductReferences();
		List<ProductReferenceModel> referncesToRemove = Collections.emptyList();

		if (CollectionUtils.isNotEmpty(productReferences))
		{
			referncesToRemove = productReferences.stream().filter(e -> productReferenceType.equals(e.getReferenceType()))
					.collect(Collectors.toList());
		}

		final List<ProductReferenceModel> newList = new LinkedList<>(productReferences);
		if (CollectionUtils.isNotEmpty(referncesToRemove))
		{
			LOG.info("Remove Product References for product {} with type {}", product.getCode(), productReferenceType);
			newList.removeAll(referncesToRemove);
			getModelService().removeAll(referncesToRemove);
		}
		getModelService().refresh(product);
		createNewProductReferences(refernecedProducts, productReferenceType, product);
		LOG.info("Product References created for product {} with type {}", product.getCode(), productReferenceType);

	}

	/**
	 * @param refernecedProducts
	 * @param product
	 * @param productReferenceType
	 * @return
	 */
	private List<ProductReferenceModel> createNewProductReferences(final List<ProductModel> refernecedProducts,
			final ProductReferenceTypeEnum productReferenceType, final ProductModel originalProduct)
	{

		final List<ProductReferenceModel> productReferenceModels = new LinkedList<>();
		for (final ProductModel productModel : refernecedProducts)
		{
			final ProductReferenceModel productReferenceModel = getModelService().create(ProductReferenceModel.class);
			productReferenceModel.setActive(true);
			productReferenceModel.setReferenceType(productReferenceType);
			productReferenceModel.setTarget(productModel);
			productReferenceModel.setPreselected(true);
			productReferenceModel.setSource(originalProduct);
			productReferenceModels.add(productReferenceModel);
			getModelService().save(productReferenceModel);
		}

		return productReferenceModels;
	}

	/**
	 * @param cronjob
	 * @return
	 */
	private Set<ProductModel> getProducts(final ProductReferencesCronJobModel cronjob)
	{

		final List<ProductModel> baseProducts = getProductService()
				.getAllProductsForCatalogVersion(cronjob.getProductCatalogVersion());

		if (CollectionUtils.isEmpty(baseProducts))
		{
			return Collections.emptySet();
		}

		final List<ProductModel> variantProducts = new LinkedList<>();

		for (final ProductModel productModel : baseProducts)
		{

			getVariantProduct(productModel, variantProducts);
		}

		return variantProducts.stream().collect(Collectors.toSet());
	}

	/**
	 * @param productModel
	 * @param variantProducts
	 * @return
	 */
	private void getVariantProduct(final ProductModel productModel, final List<ProductModel> variantProducts)
	{
		if (CollectionUtils.isEmpty(productModel.getVariants()))
		{
			variantProducts.add(productModel);
			return;
		}

		for (final ProductModel product : productModel.getVariants())
		{
			getVariantProduct(product, variantProducts);
		}

	}

	/**
	 * @return the productService
	 */
	protected ProductService getProductService()
	{
		return productService;
	}

	/**
	 * @return the catalogVersionService
	 */
	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}


	protected CategoryModel getLastCategory(final ProductModel productModel)
	{
		final List<CategoryModel> categoryModels = new ArrayList<>();
		final ProductModel baseProductModel = getProductAndCategoryHelper().getBaseProduct(productModel);
		categoryModels.addAll(baseProductModel.getSupercategories());

		while (!categoryModels.isEmpty())
		{
			CategoryModel toDisplay = null;
			toDisplay = processCategoryModels(categoryModels, toDisplay);
			categoryModels.clear();
			if (toDisplay != null)
			{
				return toDisplay;
			}
		}
		return null;
	}

	/**
	 * @return the productAndCategoryHelper
	 */
	protected ProductAndCategoryHelper getProductAndCategoryHelper()
	{
		return productAndCategoryHelper;
	}

	protected CategoryModel processCategoryModels(final Collection<CategoryModel> categoryModels, final CategoryModel toDisplay)
	{
		CategoryModel categoryToDisplay = toDisplay;

		for (final CategoryModel categoryModel : categoryModels)
		{
			if (getProductAndCategoryHelper().isValidProductCategory(categoryModel) && Objects.isNull(categoryToDisplay))
			{
				categoryToDisplay = categoryModel;
				break;

			}
		}
		return categoryToDisplay;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	private void synchronizeCatalog(final ProductReferencesCronJobModel cronJobModel)
	{
		final Boolean isSync = cronJobModel.getIsSync();
		if (!Boolean.TRUE.equals(isSync))
		{
			LOG.info("Cronjob is not syncable, enable isSync property to sync the catalog version");
			return;
		}

		final CatalogVersionModel sourceVersion = getCatalogVersionService()
				.getCatalogVersion(cronJobModel.getProductCatalogVersion().getCatalog().getId(), STAGED);
		final CatalogVersionModel targetVersion = getCatalogVersionService()
				.getCatalogVersion(cronJobModel.getProductCatalogVersion().getCatalog().getId(), ONLINE);
		getCatalogVersionService().setSessionCatalogVersion(targetVersion.getCatalog().getId(), ONLINE);
		synchronizeCatalog(sourceVersion, targetVersion);
		LOG.info("Finished synchronization of product catalog: {}", targetVersion.getCatalog().getId());
	}

	private CatalogVersionSyncJobModel createCatalogVersionSyncJobModel(final CatalogVersionModel sourceVersion,
			final CatalogVersionModel targetVersion)
	{
		final String jobCode = "sync " + sourceVersion.getCatalog().getId() + ":Staged->Online";
		final JobModel job = getCronJobService().getJob(jobCode);
		CatalogVersionSyncJobModel syncJob = null;
		if (job == null)
		{
			syncJob = getModelService().create(CatalogVersionSyncJobModel.class);
			syncJob.setCode(jobCode);
			syncJob.setSourceVersion(sourceVersion);
			syncJob.setTargetVersion(targetVersion);
			getModelService().save(syncJob);

			getModelService().refresh(syncJob);

			return syncJob;
		}
		syncJob = (CatalogVersionSyncJobModel) job;
		return syncJob;
	}

	private CatalogVersionSyncCronJobModel createSyncCronJob(final CatalogVersionSyncJobModel catalogVersionSyncJobModel)
	{
		final CatalogVersionSyncCronJobModel cronJobModel = getModelService().create(CatalogVersionSyncCronJobModel.class);
		cronJobModel.setJob(catalogVersionSyncJobModel);
		cronJobModel.setCode("update_product_Job_" + UUID.randomUUID().toString());

		getModelService().save(cronJobModel);
		return cronJobModel;
	}

	protected void synchronizeCatalog(final CatalogVersionModel sourceVersion, final CatalogVersionModel targetVersion)
	{
		cronJobService.performCronJob(createSyncCronJob(createCatalogVersionSyncJobModel(sourceVersion, targetVersion)), true);
	}

	/**
	 * @return the cronJobService
	 */
	protected CronJobService getCronJobService()
	{
		return cronJobService;
	}



}
