/**
 *
 */
package com.aacs.core.hook;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.hook.CommerceAddToCartMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;

import java.util.Objects;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aacs.aacsloyaltyprogramprovider.context.LoyaltyProgramContext;


/**
 * @author mbaker
 *
 */
public class LoyaltyAddToCartMethodHook implements CommerceAddToCartMethodHook
{
	private static final Logger LOG = LoggerFactory.getLogger(LoyaltyAddToCartMethodHook.class);


	@Resource(name = "loyaltyProgramContext")
	private LoyaltyProgramContext loyaltyProgramContext;

	@Override
	public void beforeAddToCart(final CommerceCartParameter parameters) throws CommerceCartModificationException
	{
		//DO-NOTHING

	}

	@Override
	public void afterAddToCart(final CommerceCartParameter parameters, final CommerceCartModification result)
			throws CommerceCartModificationException
	{
		validateParameterNotNullStandardMessage("parameter", parameters);
		final CartModel cart = parameters.getCart();
		if (Objects.isNull(cart))
		{
			LOG.warn("cart is null");
			return;
		}

		loyaltyProgramContext.resetLoyalty(cart);

	}

}
