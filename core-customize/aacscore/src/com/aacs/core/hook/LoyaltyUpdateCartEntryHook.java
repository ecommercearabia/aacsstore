/**
 *
 */
package com.aacs.core.hook;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.hook.CommerceUpdateCartEntryHook;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;

import java.util.Objects;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aacs.aacsloyaltyprogramprovider.context.LoyaltyProgramContext;


/**
 * @author mbaker
 *
 */
public class LoyaltyUpdateCartEntryHook implements CommerceUpdateCartEntryHook
{
	private static final Logger LOG = LoggerFactory.getLogger(LoyaltyUpdateCartEntryHook.class);


	@Resource(name = "loyaltyProgramContext")
	private LoyaltyProgramContext loyaltyProgramContext;

	@Override
	public void afterUpdateCartEntry(final CommerceCartParameter parameter, final CommerceCartModification result)
	{
		validateParameterNotNullStandardMessage("parameter", parameter);
		final CartModel cart = parameter.getCart();
		if (Objects.isNull(cart))
		{
			LOG.warn("cart is null");
			return;
		}

		loyaltyProgramContext.resetLoyalty(cart);
	}

	@Override
	public void beforeUpdateCartEntry(final CommerceCartParameter parameter)
	{
		//DO-NOTHING

	}

}
