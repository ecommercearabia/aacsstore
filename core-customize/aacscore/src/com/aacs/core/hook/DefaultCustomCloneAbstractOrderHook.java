/**
 *
 */
package com.aacs.core.hook;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.strategies.ordercloning.CloneAbstractOrderHook;

import java.util.List;

import org.springframework.util.CollectionUtils;


/**
 * @author user2
 *
 */
public class DefaultCustomCloneAbstractOrderHook implements CloneAbstractOrderHook
{

	@Override
	public void beforeClone(final AbstractOrderModel original, final Class abstractOrderClassResult)
	{
		// XXX Auto-generated method stub

	}

	@Override
	public <T extends AbstractOrderModel> void afterClone(final AbstractOrderModel original, final T clone,
			final Class abstractOrderClassResult)
	{


		final AbstractOrderModel target = clone;

		copyPaymentTransactionsInfos(original, target);

		checkZeroQty(original, target.getEntries());

	}

	/**
	 * @param original
	 * @param target
	 */
	private void copyPaymentTransactionsInfos(final AbstractOrderModel original, final AbstractOrderModel target)
	{
		target.setPaymentTransactionsInfos(original.getPaymentTransactionsInfos());
	}

	@Override
	public void beforeCloneEntries(final AbstractOrderModel original)
	{
		// XXX Auto-generated method stub

	}

	@Override
	public <T extends AbstractOrderEntryModel> void afterCloneEntries(final AbstractOrderModel original,
			final List<T> clonedEntries)
	{
		checkZeroQty(original, (List<AbstractOrderEntryModel>) clonedEntries);
	}

	public void checkZeroQty(final AbstractOrderModel source, final List<AbstractOrderEntryModel> target)
	{
		if (CollectionUtils.isEmpty(target))
		{
			return;
		}

		for (final AbstractOrderEntryModel abstractOrderEntryModel : target)
		{
			if (abstractOrderEntryModel.getQuantity() == null || abstractOrderEntryModel.getQuantity().longValue() == 0)
			{
				abstractOrderEntryModel.setQuantity(1l);
			}
		}

	}

}
