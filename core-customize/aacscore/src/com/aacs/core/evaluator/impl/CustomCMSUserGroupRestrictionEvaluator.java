package com.aacs.core.evaluator.impl;

import de.hybris.platform.cms2.model.restrictions.CMSUserGroupRestrictionModel;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.evaluator.impl.CMSUserGroupRestrictionEvaluator;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


public class CustomCMSUserGroupRestrictionEvaluator extends CMSUserGroupRestrictionEvaluator
{
	private static final Logger LOG = Logger.getLogger(CustomCMSUserGroupRestrictionEvaluator.class);

	@Override
	public boolean evaluate(final CMSUserGroupRestrictionModel cmsUserGroupRestriction, final RestrictionData context)
	{
		final Collection<UserGroupModel> groups = cmsUserGroupRestriction.getUserGroups();
		final Collection<UserGroupModel> userGroupsNotAllowed = cmsUserGroupRestriction.getUserGroupsNotAllowed();
		final UserModel currentUserModel = getUserService().getCurrentUser();
		final Set<PrincipalGroupModel> userGroups = new HashSet<>(currentUserModel.getGroups());

		if (cmsUserGroupRestriction.isIncludeSubgroups())
		{
			userGroups.addAll(getSubgroups(userGroups));
		}


		final List<String> restrGroupNotAllowedNames = new ArrayList<>();
		for (final UserGroupModel group : userGroupsNotAllowed)
		{
			restrGroupNotAllowedNames.add(group.getUid());
		}

		final List<String> restrGroupNames = new ArrayList<>();
		for (final UserGroupModel group : groups)
		{
			restrGroupNames.add(group.getUid());
		}


		final List<String> currentGroupNames = new ArrayList<>();
		for (final PrincipalGroupModel group : userGroups)
		{
			currentGroupNames.add(group.getUid());
		}


		if (LOG.isDebugEnabled())
		{

			LOG.debug("Current UserGroups: " + StringUtils.join(currentGroupNames, "; "));
			LOG.debug("Restricted UserGroups: " + StringUtils.join(restrGroupNames, "; "));
			LOG.debug("Not Allowed UserGroups: " + StringUtils.join(restrGroupNotAllowedNames, "; "));


		}

		for (final String group : restrGroupNotAllowedNames)
		{
			if (currentGroupNames.contains(group))
			{
				return false;
			}
		}

		for (final String group : restrGroupNames)
		{

			if (currentGroupNames.contains(group))
			{
				return true;
			}

		}
		return false;
	}





}
