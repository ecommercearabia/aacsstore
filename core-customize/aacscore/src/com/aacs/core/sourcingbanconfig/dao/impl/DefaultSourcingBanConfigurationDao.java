/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.core.sourcingbanconfig.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import javax.annotation.Resource;

import com.aacs.core.model.SourcingBanConfigModel;
import com.aacs.core.sourcingbanconfig.dao.SourcingBanConfigurationDao;
import com.aacs.core.sourcingbanconfig.exception.AmbiguousSourcingBanConfigurationException;


/**
 * @author monzer
 */
public class DefaultSourcingBanConfigurationDao implements SourcingBanConfigurationDao
{

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Override
	public SourcingBanConfigModel getSourcingBanConfiguration()
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery("SELECT {PK} FROM {SourcingBanConfig}");
		final SearchResult<SourcingBanConfigModel> result = flexibleSearchService.<SourcingBanConfigModel> search(query);
		if (result.getTotalCount() > 1)
		{
			throw new AmbiguousSourcingBanConfigurationException("Only one Sourcing Ban Configuration is Allowed");
		}
		else
		{
			return result.getTotalCount() == 0 ? null : result.getResult().get(0);
		}
	}

}
