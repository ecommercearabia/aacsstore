/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.core.sourcingbanconfig.dao;

import com.aacs.core.model.SourcingBanConfigModel;


/**
 * @author monzer
 */
public interface SourcingBanConfigurationDao
{
	SourcingBanConfigModel getSourcingBanConfiguration();
}
