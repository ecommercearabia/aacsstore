/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.core.sourcingbanconfig.exception;

/**
 * @author monzer
 *
 */
public class AmbiguousSourcingBanConfigurationException extends RuntimeException
{
	public AmbiguousSourcingBanConfigurationException(final String message)
	{
		super(message);
	}
}