/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.core.sourcingbanconfig.service;

import de.hybris.platform.warehousing.sourcing.ban.service.SourcingBanService;

import com.aacs.core.model.SourcingBanConfigModel;

/**
 * The Interface CustomSourcingBanService.
 *
 * @author monzer
 */
public interface CustomSourcingBanService extends SourcingBanService
{

	/**
	 * Gets the sourcing ban configuration.
	 *
	 * @return the sourcing ban configuration
	 */
	SourcingBanConfigModel getSourcingBanConfiguration();
}
