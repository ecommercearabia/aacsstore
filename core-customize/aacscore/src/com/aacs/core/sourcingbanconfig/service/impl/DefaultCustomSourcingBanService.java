/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.core.sourcingbanconfig.service.impl;

import de.hybris.platform.warehousing.sourcing.ban.service.impl.DefaultSourcingBanService;

import javax.annotation.Resource;

import com.aacs.core.model.SourcingBanConfigModel;
import com.aacs.core.sourcingbanconfig.dao.SourcingBanConfigurationDao;
import com.aacs.core.sourcingbanconfig.service.CustomSourcingBanService;


/**
 * The Class DefaultCustomSourcingBanService.
 *
 * @author monzer
 */
public class DefaultCustomSourcingBanService extends DefaultSourcingBanService implements CustomSourcingBanService
{

	/** The sourcing ban config dao. */
	@Resource(name = "sourcingBanConfigurationDao")
	private SourcingBanConfigurationDao sourcingBanConfigDao;

	/**
	 * Gets the sourcing ban configuration.
	 *
	 * @return the sourcing ban configuration
	 */
	@Override
	public SourcingBanConfigModel getSourcingBanConfiguration()
	{
		return sourcingBanConfigDao.getSourcingBanConfiguration();
	}

}
