/**
 *
 */
package com.aacs.core.user.nationality.service;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.List;
import java.util.Optional;

import com.aacs.core.model.NationalityModel;


/**
 * The Interface NationalityService.
 *
 * @author mnasro
 */

public interface NationalityService
{

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	public List<NationalityModel> getAll();

	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	public Optional<NationalityModel> get(final String code);

	/**
	 * Gets the by site.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return the by site
	 */
	public List<NationalityModel> getBySite(CMSSiteModel cmsSiteModel);

	/**
	 * Gets the by current site.
	 *
	 * @return the by current site
	 */
	public List<NationalityModel> getByCurrentSite();

}
