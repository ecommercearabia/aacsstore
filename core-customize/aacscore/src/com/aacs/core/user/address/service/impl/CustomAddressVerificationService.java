/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.core.user.address.service.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.address.AddressErrorCode;
import de.hybris.platform.commerceservices.address.AddressFieldType;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import de.hybris.platform.commerceservices.address.AddressVerificationService;
import de.hybris.platform.commerceservices.address.data.AddressFieldErrorData;
import de.hybris.platform.commerceservices.address.data.AddressVerificationResultData;
import de.hybris.platform.commerceservices.address.util.AddressVerificationResultUtils;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.aacs.core.user.service.MobilePhoneService;


/**
 * The mock implementation of AddressVerificationService to be used in the absence of an external service.
 */
public class CustomAddressVerificationService implements
		AddressVerificationService<AddressVerificationDecision, AddressFieldErrorData<AddressFieldType, AddressErrorCode>>
{
	private BaseStoreService baseStoreService;

	public static final String ACCEPT = "accept";
	public static final String REJECT = "reject";

	public static final String TITLE_CODE = "titleCode";
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String ADDRESS_LINE_1 = "addressline1";
	public static final String ADDRESS_LINE_2 = "addressline2";
	public static final String REGION = "region";
	public static final String ZIP_CODE = "zipcode";
	public static final String CITY = "city";
	public static final String COUNTRY = "country";
	public static final String MOBILE_COUNTRY = "mobileCountry";
	public static final String MOBILE = "mobile";
	public static final String ADDRESS_NAME = "addressName";
	public static final String VERIFIED = "verified";

	public static final String MISSING = "missing";
	public static final String INVALID = "invalid";

	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "userService")
	private UserService userService;

	@Override
	public AddressVerificationResultData<AddressVerificationDecision, AddressFieldErrorData<AddressFieldType, AddressErrorCode>> verifyAddress(
			final AddressModel addressModel)
	{
		final AddressVerificationResultData<AddressVerificationDecision, AddressFieldErrorData<AddressFieldType, AddressErrorCode>> acceptedResult = createVerificationResult();

		validateAddressFields(acceptedResult, addressModel);
		if (AddressVerificationResultUtils.requiresErrorHandling(acceptedResult))
		{
			acceptedResult.setDecision(AddressVerificationDecision.lookup(REJECT));
		}
		else
		{
			acceptedResult.setDecision(AddressVerificationDecision.lookup(ACCEPT));
		}
		return acceptedResult;
	}

	@Override
	public boolean isCustomerAllowedToIgnoreSuggestions()
	{
		final BaseStoreModel baseStore = getBaseStoreService().getCurrentBaseStore();
		return baseStore != null && baseStore.isCustomerAllowedToIgnoreSuggestions();
	}

	/**
	 * Validates each field input in an AddressForm. Field validation is usually left up to the external address
	 * verification service so the mock must perform this function.
	 */
	protected void validateAddressFields(
			final AddressVerificationResultData<AddressVerificationDecision, AddressFieldErrorData<AddressFieldType, AddressErrorCode>> result,
			final AddressModel address)
	{

		final List<AddressFieldErrorData<AddressFieldType, AddressErrorCode>> errorList = new ArrayList<>();

		validateAddressUserData(address, errorList);

		validateAddressData(address, errorList);

		validateAddressCountryData(address, errorList);

		validateMobileNumberData(address, errorList);

		validateOTPOnAddress(address, errorList);

		result.setFieldErrors(errorList);
	}

	/**
	 * @param address
	 * @param errorList
	 */
	private void validateOTPOnAddress(final AddressModel address,
			final List<AddressFieldErrorData<AddressFieldType, AddressErrorCode>> errorList)
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		if (currentSite == null)
		{
			return;
		}
		final UserModel currentUser = userService.getCurrentUser();
		if (!(currentUser instanceof CustomerModel))
		{
			return;
		}
		final CustomerModel currentCustomer = (CustomerModel) userService.getCurrentUser();

		if (CustomerType.GUEST.equals(currentCustomer.getType()) && currentSite.isEnableOTPVerificationOnGuestAddress()
				&& !address.isVerified())
		{
			addErrorToVerificationResult(VERIFIED, INVALID, errorList);
		}
		if (CustomerType.REGISTERED.equals(currentCustomer.getType()) && currentSite.isEnableOTPVerificationOnCustomerAddress()
				&& !address.isVerified())
		{
			addErrorToVerificationResult(VERIFIED, INVALID, errorList);
		}
		if (CustomerType.THIRD_PARTY.equals(currentCustomer.getType()) && currentSite.isEnableOTPVerificationOnCustomerAddress()
				&& !address.isVerified())
		{
			addErrorToVerificationResult(VERIFIED, INVALID, errorList);
		}
	}

	/**
	 * @param address
	 * @param errorList
	 */
	private void validateAddressCountryData(final AddressModel address,
			final List<AddressFieldErrorData<AddressFieldType, AddressErrorCode>> errorList)
	{
		if (address.getCountry() == null || (address.getCountry() != null && address.getCountry().getIsocode() == null))
		{
			addErrorToVerificationResult(COUNTRY, MISSING, errorList);
		}
		else if (StringUtils.length(address.getCountry().getIsocode()) > 255)
		{
			addErrorToVerificationResult(COUNTRY, INVALID, errorList);
		}

		if (address.getMobileCountry() == null
				|| (address.getMobileCountry() != null && address.getMobileCountry().getIsocode() == null))
		{
			addErrorToVerificationResult(MOBILE_COUNTRY, MISSING, errorList);
		}
		else if (StringUtils.length(address.getCountry().getIsocode()) > 255)
		{
			addErrorToVerificationResult(MOBILE_COUNTRY, INVALID, errorList);
		}

	}

	/**
	 * @param address
	 * @param errorList
	 */
	private void validateMobileNumberData(final AddressModel address,
			final List<AddressFieldErrorData<AddressFieldType, AddressErrorCode>> errorList)
	{
		if (StringUtils.isEmpty(address.getMobile()))
		{
			addErrorToVerificationResult(MOBILE, MISSING, errorList);
		}
		else if (StringUtils.length(address.getMobile()) > 255)
		{
			addErrorToVerificationResult(MOBILE, INVALID, errorList);
		}

		if (address.getMobileCountry() != null && !StringUtils.isEmpty(address.getMobile()))
		{
			final Optional<String> normalizedPhoneNumber = mobilePhoneService
					.validateAndNormalizePhoneNumberByIsoCode(address.getMobileCountry().getIsocode(), address.getMobile());
			if (normalizedPhoneNumber.isEmpty())
			{
				addErrorToVerificationResult(MOBILE_COUNTRY, INVALID, errorList);
				addErrorToVerificationResult(MOBILE, INVALID, errorList);
			}
			else
			{
				address.setMobile(normalizedPhoneNumber.get());
			}
		}

	}

	/**
	 * @param address
	 * @param errorList
	 */
	private void validateAddressData(final AddressModel address,
			final List<AddressFieldErrorData<AddressFieldType, AddressErrorCode>> errorList)
	{
		if (StringUtils.isEmpty(address.getLine1()))
		{
			addErrorToVerificationResult(ADDRESS_LINE_1, MISSING, errorList);
		}
		else if (StringUtils.length(address.getLine1()) > 255)
		{
			addErrorToVerificationResult(ADDRESS_LINE_1, INVALID, errorList);
		}

		if (StringUtils.length(address.getLine2()) > 255)
		{
			addErrorToVerificationResult(ADDRESS_LINE_2, INVALID, errorList);
		}

		if (!StringUtils.isEmpty(address.getTown()) && StringUtils.length(address.getTown()) > 255)
		{
			addErrorToVerificationResult(CITY, INVALID, errorList);
		}

		if (address.getRegion() != null && address.getRegion().getIsocode() == null)
		{
			addErrorToVerificationResult(REGION, MISSING, errorList);
		}
		else if (address.getRegion() != null && StringUtils.length(address.getRegion().getIsocode()) > 255)
		{
			addErrorToVerificationResult(REGION, INVALID, errorList);
		}
	}

	/**
	 * @param address
	 */
	private void validateAddressUserData(final AddressModel address,
			final List<AddressFieldErrorData<AddressFieldType, AddressErrorCode>> errorList)
	{
		if (address.getTitle() != null && StringUtils.isNotBlank(address.getTitle().getCode())
				&& StringUtils.length(address.getTitle().getCode()) > 255)
		{
			addErrorToVerificationResult(TITLE_CODE, INVALID, errorList);
		}

		if (StringUtils.isEmpty(address.getFirstname()))
		{
			addErrorToVerificationResult(FIRST_NAME, MISSING, errorList);
		}
		else if (StringUtils.length(address.getFirstname()) > 255)
		{
			addErrorToVerificationResult(FIRST_NAME, INVALID, errorList);
		}

		//		if (StringUtils.isEmpty(address.getLastname()))
		//		{
		//			addErrorToVerificationResult(LAST_NAME, MISSING, errorList);
		//		}
		//		else if (StringUtils.length(address.getLastname()) > 255)
		//		{
		//			addErrorToVerificationResult(LAST_NAME, INVALID, errorList);
		//		}
		//		if (StringUtils.isEmpty(address.getAddressName()))
		//		{
		//			addErrorToVerificationResult(ADDRESS_NAME, MISSING, errorList);
		//		}
		//		else if (StringUtils.length(address.getAddressName()) > 255)
		//		{
		//			addErrorToVerificationResult(ADDRESS_NAME, INVALID, errorList);
		//		}

	}

	protected void addErrorToVerificationResult(final String titleCode, final String missing,
			final List<AddressFieldErrorData<AddressFieldType, AddressErrorCode>> errors)
	{
		final AddressFieldErrorData<AddressFieldType, AddressErrorCode> errorData = createFieldError();
		errorData.setFieldType(AddressFieldType.lookup(titleCode));
		errorData.setErrorCode(AddressErrorCode.lookup(missing));
		errors.add(errorData);
	}

	protected AddressFieldErrorData<AddressFieldType, AddressErrorCode> createFieldError()
	{
		return new AddressFieldErrorData<>();
	}

	protected AddressVerificationResultData<AddressVerificationDecision, AddressFieldErrorData<AddressFieldType, AddressErrorCode>> createVerificationResult()
	{
		return new AddressVerificationResultData<>();
	}

	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}


	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}
}
