/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.core.order.service.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import com.aacs.core.model.OrderNoteEntryModel;
import com.aacs.core.model.OrderNoteModel;
import com.aacs.core.order.service.OrderNoteService;
import com.google.common.base.Preconditions;


/**
 * The Class DefaultOrderNoteService.
 *
 * @author amjad.shati@erabia.com
 */
public class DefaultOrderNoteService implements OrderNoteService
{

	/** The Constant CMSSITE_IS_NULL. */
	private static final String CMSSITE_IS_NULL = "CMSSite must not be null";

	/** The cms site service. */
	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	/** The model service. */
	@Resource(name = "modelService")
	private ModelService modelService;

	/**
	 * Gets the order notes by current site.
	 *
	 * @return the order notes by current site
	 */
	@Override
	public List<OrderNoteModel> getOrderNotesByCurrentSite()
	{
		return getOrderNotes(cmsSiteService.getCurrentSite());
	}

	/**
	 * Gets the order notes.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return the order notes
	 */
	@Override
	public List<OrderNoteModel> getOrderNotes(final CMSSiteModel cmsSiteModel)
	{
		Preconditions.checkArgument(cmsSiteModel != null, CMSSITE_IS_NULL);
		return cmsSiteModel.getOrderNotes() == null ? Collections.emptyList() : new ArrayList<>(cmsSiteModel.getOrderNotes());
	}

	/**
	 * Save order note entry.
	 *
	 * @param noteModel
	 *           the note model
	 * @param cart
	 *           the cart
	 */
	@Override
	public void saveOrderNoteEntry(final OrderNoteEntryModel noteModel, final CartModel cart)
	{
		modelService.refresh(cart);
		modelService.save(noteModel);
		modelService.refresh(noteModel);
		cart.setOrderNoteEntry(noteModel);
		modelService.save(cart);
	}

}