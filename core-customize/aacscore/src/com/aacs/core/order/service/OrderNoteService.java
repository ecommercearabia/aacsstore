/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.core.order.service;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.order.CartModel;

import java.util.List;

import com.aacs.core.model.OrderNoteEntryModel;
import com.aacs.core.model.OrderNoteModel;


/**
 * The Interface OrderNoteService.
 *
 * @author amjad.shati@erabia.com
 */
public interface OrderNoteService
{

	/**
	 * Gets the order notes by current site.
	 *
	 * @return the order notes by current site
	 */
	public List<OrderNoteModel> getOrderNotesByCurrentSite();

	/**
	 * Gets the order notes.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return the order notes
	 */
	public List<OrderNoteModel> getOrderNotes(final CMSSiteModel cmsSiteModel);

	/**
	 * Save order note entry.
	 *
	 * @param noteModel
	 *           the note model
	 * @param cart
	 *           the cart
	 */
	public void saveOrderNoteEntry(final OrderNoteEntryModel noteModel, final CartModel cart);
}
