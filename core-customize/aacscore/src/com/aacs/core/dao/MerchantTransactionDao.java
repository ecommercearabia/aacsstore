/**
 *
 */
package com.aacs.core.dao;

import com.aacs.core.model.MerchantTransactionModel;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public interface MerchantTransactionDao
{
	MerchantTransactionModel getMerchantTransactionById(final String id);
}
