/**
 *
 */
package com.aacs.core.dao.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.aacs.core.dao.CustomOrderDAO;
import com.google.common.base.Preconditions;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultCustomOrderDAO extends DefaultGenericDao<OrderModel> implements CustomOrderDAO
{

	public DefaultCustomOrderDAO()
	{
		super(OrderModel._TYPECODE);
	}

	@Override
	public OrderModel findOrderByCode(final String code)
	{
		Preconditions.checkArgument(code != null, "code must not be null.");
		final Map<String, Object> attr = new HashMap<String, Object>(1);
		attr.put(OrderModel.CODE, code);
		final StringBuilder sql = new StringBuilder();
		sql.append("SELECT {o:pk} from { ").append(OrderModel._TYPECODE).append(" as o} WHERE {o:code} = ?code ")
				.append("AND {o:" + OrderModel.VERSIONID + "} IS NULL");
		final FlexibleSearchQuery query = new FlexibleSearchQuery(sql.toString());
		query.getQueryParameters().putAll(attr);
		final SearchResult<OrderModel> result = getFlexibleSearchService().search(query);
		final List<OrderModel> orders = result.getResult();
		return orders.isEmpty() ? null : orders.get(0);
	}

}
