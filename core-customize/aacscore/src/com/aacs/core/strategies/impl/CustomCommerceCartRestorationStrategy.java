/**
 *
 */
package com.aacs.core.strategies.impl;

import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartRestorationStrategy;
import de.hybris.platform.core.model.order.CartModel;


/**
 * @author core
 *
 */
public class CustomCommerceCartRestorationStrategy extends DefaultCommerceCartRestorationStrategy
{

	@Override
	protected void clearPaymentTransactionsOnCart(final CartModel cartModel)
	{
		//		getModelService().removeAll(cartModel.getPaymentTransactions());
		//		cartModel.setPaymentTransactions(Collections.emptyList());
	}
}
