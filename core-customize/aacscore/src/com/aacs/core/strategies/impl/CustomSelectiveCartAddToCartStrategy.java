/**
 *
 */
package com.aacs.core.strategies.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.selectivecartservices.strategies.SelectiveCartAddToCartStrategy;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import javax.annotation.Resource;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CustomSelectiveCartAddToCartStrategy extends SelectiveCartAddToCartStrategy
{
	@Resource(name = "customCommerceAddToCartStrategy")
	private CustomCommerceAddToCartStrategy customCommerceAddToCartStrategy;

	/**
	 * @return the customCommerceAddToCartStrategy
	 */
	protected CustomCommerceAddToCartStrategy getCustomCommerceAddToCartStrategy()
	{
		return customCommerceAddToCartStrategy;
	}

	@Override
	protected long getAllowedCartAdjustmentForProduct(final CartModel cartModel, final ProductModel productModel,
			final long quantityToAdd, final PointOfServiceModel pointOfServiceModel)
	{
		return getCustomCommerceAddToCartStrategy().getAllowedCartAdjustmentForProduct(cartModel, productModel, quantityToAdd,
				pointOfServiceModel);
	}

	@Override
	public synchronized CommerceCartModification addToCart(final CommerceCartParameter parameter)
			throws CommerceCartModificationException
	{
		final CommerceCartModification modification = doAddToCart(parameter);
		afterAddToCart(parameter, modification);
		// Here the entry is fully populated, so we can search for a similar one and merge.
		mergeEntry(modification, parameter);
		getCommerceCartCalculationStrategy().calculateCart(parameter);
		return modification;
	}


}
