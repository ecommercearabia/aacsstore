/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.aacs.aacsscpiservicesbackoffice.services;

/**
 * Hello World AacsscpiservicesbackofficeService
 */
public class AacsscpiservicesbackofficeService
{
	public String getHello()
	{
		return "Hello";
	}
}
