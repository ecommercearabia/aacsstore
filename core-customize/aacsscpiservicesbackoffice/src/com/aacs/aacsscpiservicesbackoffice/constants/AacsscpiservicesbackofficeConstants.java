/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.aacs.aacsscpiservicesbackoffice.constants;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
public final class AacsscpiservicesbackofficeConstants extends GeneratedAacsscpiservicesbackofficeConstants
{
	public static final String EXTENSIONNAME = "aacsscpiservicesbackoffice";

	private AacsscpiservicesbackofficeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
