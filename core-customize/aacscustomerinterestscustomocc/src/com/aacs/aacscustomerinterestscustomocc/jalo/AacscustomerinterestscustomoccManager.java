/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscustomerinterestscustomocc.jalo;

import com.aacs.aacscustomerinterestscustomocc.constants.AacscustomerinterestscustomoccConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class AacscustomerinterestscustomoccManager extends GeneratedAacscustomerinterestscustomoccManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( AacscustomerinterestscustomoccManager.class.getName() );
	
	public static final AacscustomerinterestscustomoccManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AacscustomerinterestscustomoccManager) em.getExtension(AacscustomerinterestscustomoccConstants.EXTENSIONNAME);
	}
	
}
