/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscustomerinterestscustomocc.constants;

/**
 * Global class for all aacscustomerinterestscustomocc constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings(
{ "deprecation", "squid:CallToDeprecatedMethod" })
public final class AacscustomerinterestscustomoccConstants extends GeneratedAacscustomerinterestscustomoccConstants
{
	public static final String EXTENSIONNAME = "aacscustomerinterestscustomocc"; //NOSONAR

	private AacscustomerinterestscustomoccConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
