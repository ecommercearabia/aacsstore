/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscomponents.constants;

/**
 * Global class for all Aacscomponents constants. You can add global constants for your extension into this class.
 */
public final class AacscomponentsConstants extends GeneratedAacscomponentsConstants
{
	public static final String EXTENSIONNAME = "aacscomponents";

	private AacscomponentsConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "aacscomponentsPlatformLogo";
}
