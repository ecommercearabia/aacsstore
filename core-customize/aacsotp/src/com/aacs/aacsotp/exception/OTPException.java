package com.aacs.aacsotp.exception;

import com.aacs.aacsotp.exception.enums.OTPExceptionType;


/**
 * @author mnasro
 *
 *         The Class OTPException.
 */
public class OTPException extends Exception
{

	/** The otp exception type. */
	private final OTPExceptionType type;

	private String errorResponse;

	/**
	 * Instantiates a new OTP exception.
	 *
	 * @param type
	 *           the otp exception type
	 * @param message
	 *           the message
	 */
	public OTPException(final OTPExceptionType type, final String message)
	{
		super(message);
		this.type = type;
	}

	public OTPException(final OTPExceptionType type, final String message, final String errorResponse)
	{
		super(message);
		this.type = type;
		this.errorResponse = errorResponse;
	}


	/**
	 * Gets the OTP exception type.
	 *
	 * @return the OTPExceptionType
	 */
	public OTPExceptionType geType()
	{
		return type;
	}



	/**
	 * @return the errorResponse
	 */
	public String getErrorResponse()
	{
		return errorResponse;
	}



	/**
	 * @param errorResponse
	 *           the errorResponse to set
	 */
	public void setErrorResponse(final String errorResponse)
	{
		this.errorResponse = errorResponse;
	}



}
