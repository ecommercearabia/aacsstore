/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsotp.dao.impl;

import com.aacs.aacsotp.dao.OTPProviderDao;
import com.aacs.aacsotp.model.BotSocietyProviderModel;


/**
 * @author monzer
 *
 *         The Class DefaultWhatsappProviderDao.
 */
public class DefaultBotSocietyProviderDao extends DefaultOTPProviderDao implements OTPProviderDao
{

	/**
	 * Instantiates a new default BotSocietyProvider provider dao.
	 */
	public DefaultBotSocietyProviderDao()
	{
		super(BotSocietyProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return BotSocietyProviderModel._TYPECODE;
	}

}
