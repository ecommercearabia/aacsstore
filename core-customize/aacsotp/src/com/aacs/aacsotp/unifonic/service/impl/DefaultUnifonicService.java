package com.aacs.aacsotp.unifonic.service.impl;

import java.util.HashMap;
import java.util.Map;

import com.aacs.aacsotp.unifonic.beans.UnifonicResponse;
import com.aacs.aacsotp.unifonic.exception.UnifonicException;
import com.aacs.aacsotp.unifonic.exception.type.UnifonicExceptionType;
import com.aacs.aacsotp.unifonic.service.UnifonicService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;


public class DefaultUnifonicService implements UnifonicService
{

	private static final String API_UNIFONIC_END_POINT_GETCODE = "GetCode";
	private static final String API_UNIFONIC_END_POINT_VERIFYNUMBER = "VerifyNumber";
	private static Gson gson = null;

	static
	{
		final GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting();
		gson = builder.create();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.erabia.otpservices.service.VerificationService#sendVerificationCode(java. lang.String, java.lang.String)
	 */
	@Override
	public boolean sendVerificationCode(final String baseUrl, final String appSid, final String message, final String countryCode,
			final String messageTo) throws UnifonicException
	{
		final String URL = baseUrl + API_UNIFONIC_END_POINT_GETCODE;
		final Map<String, Object> body = new HashMap<>(3);
		body.put("AppSid", appSid);
		body.put("Recipient", messageTo);
		body.put("Body", message);
		String response = null;

		try
		{
			response = Unirest.post(URL).header("Content-Type", "application/x-www-form-urlencoded").fields(body).asString()
					.getBody();
			final UnifonicResponse responseObject = gson.fromJson(response, UnifonicResponse.class);
			if (!"true".equals(responseObject.getSuccess()))
			{
				throw new UnifonicException(responseObject.getMessage(),
						UnifonicExceptionType.getExceptionFromCode(responseObject.getErrorCode()), response);
			}
			return true;
		}
		catch (final UnirestException e)
		{
			throw new UnifonicException(e.getMessage(), UnifonicExceptionType.INTERNAL_ERROR, response);
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.erabia.otpservices.service.VerificationService#verifyCode(java.lang. String, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public boolean verifyCode(final String baseUrl, final String appSid, final String countryCode, final String messageTo,
			final String code) throws UnifonicException
	{
		final String URL = baseUrl + API_UNIFONIC_END_POINT_VERIFYNUMBER;
		final Map<String, Object> body = new HashMap<>(3);
		body.put("AppSid", appSid);
		body.put("Recipient", messageTo);
		body.put("PassCode", code);
		String response = null;

		try
		{
			response = Unirest.post(URL).header("Content-Type", "application/x-www-form-urlencoded").fields(body).asString()
					.getBody();

			final UnifonicResponse responseObject = gson.fromJson(response, UnifonicResponse.class);
			if (!"true".equals(responseObject.getSuccess()))
			{
				throw new UnifonicException(responseObject.getMessage(),
						UnifonicExceptionType.getExceptionFromCode(responseObject.getErrorCode()), response);
			}
			return true;

		}
		catch (final UnirestException e)
		{
			throw new UnifonicException(e.getMessage(), UnifonicExceptionType.INTERNAL_ERROR, response);
		}

	}

}
