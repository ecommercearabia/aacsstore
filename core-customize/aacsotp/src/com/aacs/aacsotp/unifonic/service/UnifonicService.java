package com.aacs.aacsotp.unifonic.service;

import com.aacs.aacsotp.unifonic.exception.UnifonicException;


public interface UnifonicService
{
	boolean sendVerificationCode(final String baseUrl, final String appSid, final String message,
			final String countryCode, final String messageTo) throws UnifonicException;

	boolean verifyCode(final String baseUrl, final String appSid, final String countryCode,
			final String messageTo, final String code) throws UnifonicException;
}
