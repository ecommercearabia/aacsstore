package com.aacs.aacsotp.unifonic.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VerifiyCodeResponse extends UnifonicResponse {

	@SerializedName("data")
	@Expose
	private VerifiyCodeResponseData data;

	public VerifiyCodeResponseData getData() {
		return data;
	}

	public void setData(final VerifiyCodeResponseData data) {
		this.data = data;
	}

}
