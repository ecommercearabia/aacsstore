package com.aacs.aacsotp.unifonic.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class SendVerificationCodeResponse extends UnifonicResponse
{

	@SerializedName("data")
	@Expose
	private SendVerificationCodeResponseData data;

	public SendVerificationCodeResponseData getData()
	{
		return data;
	}

	public void setData(final SendVerificationCodeResponseData data)
	{
		this.data = data;
	}
}
