package com.aacs.aacsotp.strategy.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.aacs.aacsotp.model.OTPProviderModel;
import com.aacs.aacsotp.model.BotSocietyProviderModel;
import com.aacs.aacsotp.service.OTPProviderService;
import com.aacs.aacsotp.strategy.OTPProviderStrategy;


/**
 * @author monzer
 *
 *         The Class DefaultWhatsappProviderStrategy.
 */
public class DefaultBotSocietyProviderStrategy implements OTPProviderStrategy
{

	/** The otp provider service. */
	@Resource(name = "otpProviderService")
	private OTPProviderService otpProviderService;

	/**
	 * Gets the OTP provider service.
	 *
	 * @return the OTP provider service
	 */
	protected OTPProviderService getOTPProviderService()
	{
		return otpProviderService;
	}

	/**
	 * Gets the active provider.
	 *
	 * @param cmsSiteUid
	 *           the cms site uid
	 * @return the active provider
	 */
	@Override
	public Optional<OTPProviderModel> getActiveProvider(final String cmsSiteUid)
	{
		return getOTPProviderService().getActive(cmsSiteUid, BotSocietyProviderModel.class);
	}

	/**
	 * Gets the active provider.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return the active provider
	 */
	@Override
	public Optional<OTPProviderModel> getActiveProvider(final CMSSiteModel cmsSiteModel)
	{
		return getOTPProviderService().getActive(cmsSiteModel, BotSocietyProviderModel.class);
	}

	/**
	 * Gets the active provider by current site.
	 *
	 * @return the active provider by current site
	 */
	@Override
	public Optional<OTPProviderModel> getActiveProviderByCurrentSite()
	{
		return getOTPProviderService().getActiveProviderByCurrentSite(BotSocietyProviderModel.class);
	}
}

