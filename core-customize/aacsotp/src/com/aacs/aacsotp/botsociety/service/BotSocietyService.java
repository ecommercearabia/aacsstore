/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsotp.botsociety.service;

/**
 *
 */
public interface BotSocietyService
{

	String sendOrderConfirmationMessage(String campignId, String orderId, String mobileNumber);

	String sendOrderDeliveredMessage(String campignId, String orderId, String mobileNumber);

	String sendOrderShipmentMessage(String campignId, String orderId, String mobileNumber);

	String sendOrderCancellationMessage(String campignId, String orderId, String mobileNumber);

}
