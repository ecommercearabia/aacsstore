/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.facades.price.enums;

/**
 * @author amjad.shati@erabia.com
 *
 */
public enum RuleConditionChildrenDefinitionType
{
	PRODUCTS("y_qualifying_products"), CATEGORIES("y_qualifying_categories");

	private String name;

	/**
	 *
	 */
	private RuleConditionChildrenDefinitionType(final String name)
	{
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

}
