/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.facades.labels.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.DocumentPageModel;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.warehousingfacades.order.WarehousingConsignmentFacade;

import java.io.IOException;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import com.aacs.core.service.BarcodeGenaratorService;


public class CustomPackContext extends de.hybris.platform.warehousing.labels.context.PackContext
{

	private static final Logger LOG = Logger.getLogger(CustomPackContext.class);

	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;

	@Resource(name = "warehousingConsignmentFacade")
	private WarehousingConsignmentFacade warehousingConsignmentFacade;

	@Resource(name = "productFacade")
	private ProductFacade productFacade;

	@Resource(name = "barcodeGenaratorService")
	private BarcodeGenaratorService barcodeGenaratorService;

	private OrderData orderData;

	private ConsignmentData consignmentData;

	private MediaModel barcode;

	/**
	 * @return the orderData
	 */
	public OrderData getOrderData()
	{
		return orderData;
	}


	/**
	 * @param orderData
	 *           the orderData to set
	 */
	public void setOrderData(final OrderData orderData)
	{
		this.orderData = orderData;
	}


	@Override
	public void init(final ConsignmentProcessModel businessProcessModel, final DocumentPageModel documentPageModel)
	{
		super.init(businessProcessModel, documentPageModel);
		this.orderData = orderFacade.getOrderDetailsForCode(getOrder().getCode());
		this.consignmentData = warehousingConsignmentFacade.getConsignmentForCode(getConsignment().getCode());

		if (getConsignment().getBarcode() == null)
		{
			try
			{
				barcode = barcodeGenaratorService.generateBarcodeAsMedia(businessProcessModel.getConsignment());

			}
			catch (final IOException e)
			{
				LOG.error(e.getMessage());
			}
		}

		barcode = getConsignment().getBarcode();
	}

	public String getCustomProductImageURL(final ProductData product)
	{
		String path = null;

		if (product.getImages() != null && !product.getImages().isEmpty())
		{
			final Optional<ImageData> findFirst = product.getImages().stream()
					.filter(p -> p.getFormat().equals("thumbnail") && p.getImageType().equals(ImageDataType.PRIMARY)).findFirst();

			if (findFirst.isPresent())
			{
				path = findFirst.get().getUrl();
			}
		}
		return path;
	}

	public String getBasePrice(final OrderEntryData orderEntry)
	{
		final String path = null;

		if (orderEntry == null)
		{
			return "";
		}
		if (orderEntry.getProduct() == null)
		{
			return orderEntry.getBasePrice() != null ? orderEntry.getBasePrice().getFormattedValue() : null;
		}

		if (!CollectionUtils.isEmpty(orderEntry.getProduct().getUnitFactorRangeData()))
		{
			final String unit = orderEntry.getProduct().getUnit() != null ? "/" + orderEntry.getProduct().getUnit().getUnit() : "";
			return orderEntry.getProduct().getUnitPrice().getFormattedValue() + unit;
		}
		else
		{
			return orderEntry.getBasePrice() != null ? orderEntry.getBasePrice().getFormattedValue() : null;
		}

	}

	/**
	 * @return the consignmentData
	 */
	public ConsignmentData getConsignmentData()
	{
		return consignmentData;
	}


	/**
	 * @param consignmentData
	 *           the consignmentData to set
	 */
	public void setConsignmentData(final ConsignmentData consignmentData)
	{
		this.consignmentData = consignmentData;
	}

	/**
	 * @return the barcode
	 */
	public MediaModel getBarcode()
	{
		return barcode;
	}

	/**
	 * @param barcode
	 *           the barcode to set
	 */
	public void setBarcode(final MediaModel barcode)
	{
		this.barcode = barcode;
	}

}
