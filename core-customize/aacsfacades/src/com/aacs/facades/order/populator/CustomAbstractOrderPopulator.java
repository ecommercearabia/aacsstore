package com.aacs.facades.order.populator;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.math.BigDecimal;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;


/**
 *
 * The Class CustomAbstractOrderPopulator.
 *
 * @author mnasro
 *
 * @param <S>
 *           SOURCE, the generic type extends AbstractOrderModel
 * @param <T>
 *           TARGET, the generic type extends AbstractOrderData
 */
public class CustomAbstractOrderPopulator<S extends AbstractOrderModel, T extends AbstractOrderData> implements Populator<S, T>
{
	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	/**
	 * @return the priceDataFactory
	 */
	protected PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	@Override
	public void populate(final AbstractOrderModel source, final AbstractOrderData target)
	{
		if (source != null && target != null)
		{
			populateAllEntriesCount(source, target);
			target.setOrderRefId(source.getOrderRefId());

			populateRemainingAmount(source, target);
		}

	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateRemainingAmount(final AbstractOrderModel source, final AbstractOrderData target)
	{

		final PriceData remainingAmount = createPrice(source, source.getRemainingAmount());
		target.setRemainingAmount(remainingAmount);
	}

	/**
	 * @param source
	 * @param target
	 */
	protected void populateAllEntriesCount(final AbstractOrderModel source, final AbstractOrderData target)
	{
		if (target instanceof CartData)
		{
			final int allEntriesCount = CollectionUtils.isEmpty(source.getEntries()) ? 0 : source.getEntries().size();
			((CartData) target).setAllEntriesCount(allEntriesCount);
		}
	}

	protected PriceData createPrice(final AbstractOrderModel orderModel, final double val)
	{
		return getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(val), orderModel.getCurrency());
	}

}
