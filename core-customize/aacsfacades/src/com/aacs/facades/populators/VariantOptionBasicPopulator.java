/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.facades.populators;

import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.variants.model.VariantProductModel;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.aacs.wishlist.exception.WishlistException;
import com.aacs.wishlist.service.WishlistService;


/**
 * @author amjad.shati@erabia.com
 * @author monzer
 *
 */
public class VariantOptionBasicPopulator implements Populator<VariantProductModel, VariantOptionData>
{
	@Resource(name = "wishlistService")
	private WishlistService wishlistService;

	@Override
	public void populate(final VariantProductModel source, final VariantOptionData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setName(source.getName());
		target.setDescription(source.getDescription());
		target.setSummary(source.getSummary());

		boolean isProductInWishList;
		try
		{
			isProductInWishList = wishlistService.isProductInWishList(source.getCode());
		}
		catch (final WishlistException e)
		{
			isProductInWishList = false;
		}
		target.setInWishList(isProductInWishList);
	}

}
