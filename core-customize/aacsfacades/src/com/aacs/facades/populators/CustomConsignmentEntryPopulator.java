/**
 *
 */
package com.aacs.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.ConsignmentEntryPopulator;
import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.warehousing.inventoryevent.service.InventoryEventService;
import de.hybris.platform.warehousing.model.AllocationEventModel;

import java.util.Collection;
import java.util.Objects;

import javax.annotation.Resource;

import org.springframework.util.Assert;


/**
 * @author mohammed-baker
 *
 */
public class CustomConsignmentEntryPopulator extends ConsignmentEntryPopulator
{

	@Resource(name = "inventoryEventService")
	private InventoryEventService inventoryEventService;

	@Override
	public void populate(final ConsignmentEntryModel source, final ConsignmentEntryData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setQuantity(source.getQuantity());
		target.setShippedQuantity(source.getShippedQuantity());
		if (!Objects.isNull(source.getOrderEntry()))
		{
			target.setOrderEntry(getOrderEntryConverter().convert(source.getOrderEntry()));
		}

		if (source.getOrderEntry() != null && source.getOrderEntry().getProduct() != null
				&& source.getOrderEntry().getProduct().getUnitFactorRange() != null
				&& source.getOrderEntry().getProduct().isEnableUnitFactorRange())
		{
			target.setWieghtedQuantity(source.getQuantity() / 1000d);
		}
		target.setStockBinCode(extractBin(source));
	}

	public String extractBin(final ConsignmentEntryModel consignmentEntryModel)
	{
		String binLocation = "";
		final StringBuilder bins = new StringBuilder();

		final Collection<AllocationEventModel> events = this.getInventoryEventService()
				.getAllocationEventsForConsignmentEntry(consignmentEntryModel);
		events.stream().filter(e -> (e.getStockLevel() != null && e.getStockLevel().getBin() != null)).forEach(e -> {
			bins.append(e.getStockLevel().getBin()).append(",");
		});
		if (bins.length() > 0)
		{
			binLocation = bins.substring(0, bins.length() - 1);
		}


		return binLocation;
	}

	/**
	 * @return the inventoryEventService
	 */
	public InventoryEventService getInventoryEventService()
	{
		return inventoryEventService;
	}

}
