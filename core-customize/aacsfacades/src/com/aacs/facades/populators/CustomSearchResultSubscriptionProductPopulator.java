/**
 *
 */
package com.aacs.facades.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.subscriptionfacades.converters.populator.SearchResultSubscriptionProductPopulator;
import de.hybris.platform.subscriptionfacades.data.BillingPlanData;
import de.hybris.platform.subscriptionfacades.data.BillingTimeData;
import de.hybris.platform.subscriptionfacades.data.SubscriptionTermData;
import de.hybris.platform.subscriptionfacades.data.TermOfServiceFrequencyData;
import de.hybris.platform.subscriptionservices.enums.TermOfServiceRenewal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author monzer
 *
 */
public class CustomSearchResultSubscriptionProductPopulator
		extends SearchResultSubscriptionProductPopulator<SearchResultValueData, ProductData>
{

	private static final Logger LOG = LoggerFactory.getLogger(CustomSearchResultSubscriptionProductPopulator.class);

	@Override
	public void populate(final SearchResultValueData source, final ProductData target)
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		final String billingTimeAsString = this.getValue(source, "billingTime");
		final BillingTimeData billingTime = new BillingTimeData();
		billingTime.setName(billingTimeAsString);

		if (target.getSubscriptionTerm() == null)
		{
			target.setSubscriptionTerm(new SubscriptionTermData());
			target.getSubscriptionTerm().setBillingPlan(new BillingPlanData());
		}

		if (target.getSubscriptionTerm().getBillingPlan() == null)
		{
			target.getSubscriptionTerm().setBillingPlan(new BillingPlanData());
		}

		target.getSubscriptionTerm().getBillingPlan().setBillingTime(billingTime);

		final String termOfServiceFrequencyAsString = this.getValue(source, "termLimit");
		final TermOfServiceFrequencyData termOfServiceFrequencyData = new TermOfServiceFrequencyData();
		termOfServiceFrequencyData.setName(termOfServiceFrequencyAsString);
		target.getSubscriptionTerm().setTermOfServiceFrequency(termOfServiceFrequencyData);
		ProductModel productModel = null;
		try
		{
			productModel = getProductService().getProductForCode(target.getCode());
		}
		catch (final UnknownIdentifierException ex)
		{
			LOG.warn("product not found or not Approved");
			return;
		}
		if (productModel == null)
		{
			return;
		}
		if (getSubscriptionProductService().isSubscription(productModel))
		{
			final PriceData oldPrice = target.getPrice();
			getSubscriptionPricePlanPopulator().populate(productModel, target);

			final TermOfServiceRenewal termOfServiceRenewal = productModel.getSubscriptionTerm().getTermOfServiceRenewal();
			target.getSubscriptionTerm().setTermOfServiceRenewal(getTermOfServiceRenewalConverter().convert(termOfServiceRenewal));
			// restore values from old price as they may have been overridden by the SubscriptionPricePlanPopulator
			if (oldPrice != null)
			{
				target.getPrice().setValue(oldPrice.getValue());
				target.getPrice().setCurrencyIso(oldPrice.getCurrencyIso());
				target.getPrice().setFormattedValue(oldPrice.getFormattedValue());
				target.getPrice().setMaxQuantity(oldPrice.getMaxQuantity());
				target.getPrice().setMinQuantity(oldPrice.getMinQuantity());
				target.getPrice().setPriceType(oldPrice.getPriceType());
			}
		}

	}

}
