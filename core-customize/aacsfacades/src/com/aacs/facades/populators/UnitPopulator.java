/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.UnitModel;

import org.springframework.util.Assert;

import com.aacs.aacsfacades.units.UnitData;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class UnitPopulator implements Populator<UnitModel, UnitData>
{


	@Override
	public void populate(final UnitModel source, final UnitData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setConversion(source.getConversion() == null ? source.getConversion().doubleValue() : 1);
		target.setUnit(source.getName());
		target.setUnitType(source.getUnitType());
	}

}
