/**
 *
 */
package com.aacs.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.GroupOrderConsignmentEntryPopulator;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;
import java.util.stream.Collectors;


/**
 * @author monzer
 *
 */
public class CustomGroupOrderConsignmentEntryPopulator extends GroupOrderConsignmentEntryPopulator
{

	@Override
	public void populate(final OrderModel source, final OrderData target) throws ConversionException
	{

		final List<ConsignmentData> consignments = target.getConsignments();
		for (final ConsignmentData consignment : consignments)
		{
			consignment.setEntries(groupConsignmentEntries(getConsignmentEntriesFiltered(consignment), target));
		}

		target.setUnconsignedEntries(super.groupEntries(target.getUnconsignedEntries(), target));
	}

	protected List<ConsignmentEntryData> getConsignmentEntriesFiltered(final ConsignmentData consignment)
	{
		return consignment.getEntries().stream()
				.filter(e -> e != null && e.getOrderEntry() != null && e.getOrderEntry().getProduct() != null)
				.collect(Collectors.toList());
	}



}
