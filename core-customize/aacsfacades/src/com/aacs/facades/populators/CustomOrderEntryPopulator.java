/**
 *
 */
package com.aacs.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.OrderEntryPopulator;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;


/**
 * @author monzer
 *
 */
public class CustomOrderEntryPopulator extends OrderEntryPopulator
{

	@Override
	protected void addCommon(final AbstractOrderEntryModel orderEntry, final OrderEntryData entry)
	{
		super.addCommon(orderEntry, entry);
		if (orderEntry.getProduct() != null && orderEntry.getProduct().getUnitFactorRange() != null
				&& orderEntry.getProduct().isEnableUnitFactorRange() && orderEntry.getQuantity() != null)
		{
			entry.setWieghtedQuantity(orderEntry.getQuantity() / 1000d);
		}

		if (entry != null && orderEntry instanceof OrderEntryModel)
		{
			final OrderEntryModel source = (OrderEntryModel) orderEntry;
			entry.setQuantityAllocated(source.getQuantityAllocated());
			entry.setQuantityUnallocated(source.getQuantityUnallocated());
			entry.setQuantityCancelled(source.getQuantityCancelled());
			entry.setQuantityPending(source.getQuantityPending());
			entry.setQuantityShipped(source.getQuantityShipped());
			entry.setQuantityReturned(source.getQuantityReturned());
		}

	}

	@Override
	protected void addTotals(final AbstractOrderEntryModel orderEntry, final OrderEntryData entry)
	{
		if (orderEntry.getBasePrice() != null && orderEntry.getProduct() != null
				&& orderEntry.getProduct().getUnitFactorRange() != null && orderEntry.getProduct().isEnableUnitFactorRange())
		{
			entry.setBasePrice(createPrice(orderEntry, orderEntry.getBasePrice() * orderEntry.getQuantity()));
		}
		else if (orderEntry.getBasePrice() != null)
		{
			entry.setBasePrice(createPrice(orderEntry, orderEntry.getBasePrice()));
		}
		if (orderEntry.getTotalPrice() != null)
		{
			entry.setTotalPrice(createPrice(orderEntry, orderEntry.getTotalPrice()));
		}
	}

}
