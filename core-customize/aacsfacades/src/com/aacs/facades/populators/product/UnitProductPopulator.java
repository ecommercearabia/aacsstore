package com.aacs.facades.populators.product;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import com.aacs.aacsfacades.units.UnitData;


/**
 * @author mnasro
 *
 */
public class UnitProductPopulator implements Populator<ProductModel, ProductData>
{

	@Resource(name = "unitConverter")
	private Converter<UnitModel, UnitData> unitConverter;

	/**
	 * @return the unitConverter
	 */
	protected Converter<UnitModel, UnitData> getUnitConverter()
	{
		return unitConverter;
	}
	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		if (source == null || target == null)
		{
			return;
		}
		if (source.getUnit() != null)
		{
			target.setUnit(getUnitConverter().convert(source.getUnit()));
		}

	}
}
