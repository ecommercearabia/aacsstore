/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.facades.populators.product;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;



/**
 * @author mnasro
 *
 */
public class SearchResultGeneralProductPopulator implements Populator<SearchResultValueData, ProductData>
{

	private static final String EXPRESS_DELIVERY_SOLR_KEY = "expressDelivery";

	private static final Logger LOG = Logger.getLogger(SearchResultGeneralProductPopulator.class);

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "unitProductPopulator")
	private Populator<ProductModel, ProductData> unitProductPopulator;

	@Resource(name = "productUnitPricePopulator")
	private Populator<ProductModel, ProductData> productUnitPricePopulator;

	@Override
	public void populate(final SearchResultValueData source, final ProductData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		populateExpressDelivery(source, target);

		final ProductModel product = getProduct(source);
		if (product == null)
		{
			return;
		}
		populateUnit(product, target);
		populateUnitPrice(product, target);
	}

	/**
	 * @param product
	 * @param target
	 */
	private void populateUnitPrice(final ProductModel product, final ProductData target)
	{
		getProductUnitPricePopulator().populate(product, target);
	}

	/**
	 * @param product
	 * @param target
	 */
	private void populateUnit(final ProductModel product, final ProductData target)
	{
		getUnitProductPopulator().populate(product, target);
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateExpressDelivery(final SearchResultValueData source, final ProductData target)
	{
		final Boolean value = this.<Boolean> getValue(source, EXPRESS_DELIVERY_SOLR_KEY);
		if (value == null)
		{
			return;
		}
		target.setExpressDelivery(value.booleanValue());
	}

	protected <T> T getValue(final SearchResultValueData source, final String propertyName)
	{
		if (source.getValues() == null)
		{
			return null;
		}
		// DO NOT REMOVE the cast (T) below, while it should be unnecessary it is required by the javac compiler
		return (T) source.getValues().get(propertyName);
	}


	private ProductModel getProduct(final SearchResultValueData source)
	{
		ProductModel productModel = null;
		final String productCode = (String) source.getValues().get(ProductModel.CODE);
		try
		{
			productModel = productService.getProductForCode(productCode);
			if (productModel == null)
			{
				LOG.warn("product code [" + productCode + "] not found");
			}
		}
		catch (final UnknownIdentifierException ex)
		{
			LOG.warn("product code [" + productCode + "] not found");
		}

		return productModel;
	}

	/**
	 * @return the unitProductPopulator
	 */
	protected Populator<ProductModel, ProductData> getUnitProductPopulator()
	{
		return unitProductPopulator;
	}

	/**
	 * @return the productUnitPricePopulator
	 */
	protected Populator<ProductModel, ProductData> getProductUnitPricePopulator()
	{
		return productUnitPricePopulator;
	}

}
