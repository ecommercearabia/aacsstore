/**
 *
 */
package com.aacs.facades.populators.product;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.converters.populator.SearchResultProductPopulator;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class CustomSearchResultProductImagePopulator extends SearchResultProductPopulator
{
	@Override
	protected List<ImageData> createImageData(final SearchResultValueData source)
	{
		final List<ImageData> result = new ArrayList<>();
		addImageData(source, "zoom", result);
		addImageData(source, "thumbnail", result);
		addImageData(source, "product", result);
		addImageData(source, "store", result);

		return result;
	}

	@Override
	public void populate(final SearchResultValueData source, final ProductData target)
	{
		final List<ImageData> images = createImageData(source);
		if (CollectionUtils.isNotEmpty(images))
		{
			target.setImages(images);
		}
	}
}
