/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.facades.populators.product;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.DiscountPriceData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;
import java.util.Collection;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;


/**
 * The Class ProductUnitPricePopulator.
 *
 * @author mnasro
 */
public class ProductUnitPricePopulator implements Populator<ProductModel, ProductData>
{
	/** The commerce price service. */
	@Resource(name = "commercePriceService")
	private CommercePriceService commercePriceService;

	/** The product price populator. */
	@Resource(name = "productPricePopulator")
	private Populator<ProductModel, ProductData> productPricePopulator;

	/** The product discount populator. */
	@Resource(name = "productDiscountPopulator")
	private Populator<ProductModel, ProductData> productDiscountPopulator;

	/** The price data factory. */
	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	/** The common I 18 N service. */
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	/**
	 * @return the productPricePopulator
	 */
	protected Populator<ProductModel, ProductData> getProductPricePopulator()
	{
		return productPricePopulator;
	}

	/**
	 * Gets the product discount populator.
	 *
	 * @return the productDiscountPopulator
	 */
	protected Populator<ProductModel, ProductData> getProductDiscountPopulator()
	{
		return productDiscountPopulator;
	}

	/**
	 * Gets the commerce price service.
	 *
	 * @return the commercePriceService
	 */
	protected CommercePriceService getCommercePriceService()
	{
		return commercePriceService;
	}

	/**
	 * Gets the price data factory.
	 *
	 * @return the priceDataFactory
	 */
	protected PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	/**
	 * Gets the common I 18 N service.
	 *
	 * @return the commonI18NService
	 */
	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	/**
	 * Populate.
	 *
	 * @param productModel
	 *           the product model
	 * @param productData
	 *           the product data
	 */
	@Override
	public void populate(final ProductModel productModel, final ProductData productData)
	{
		if (productModel == null || productData == null || !productModel.isEnableUnitFactorRange()
				|| productModel.getEurope1Prices() == null)
		{
			return;
		}

		getProductPricePopulator().populate(productModel, productData);


		if (productData.getPrice() == null || productData.getPrice().getValue() == null)
		{
			return;
		}
		final double priceVal = productData.getPrice().getValue().doubleValue();

		final CurrencyModel currentCurrency = commonI18NService.getCurrentCurrency();
		final PriceRowModel findPriceRowByCurrency = findPriceRowByCurrency(productModel.getEurope1Prices(),
				currentCurrency.getIsocode());

		if (findPriceRowByCurrency == null || findPriceRowByCurrency.getUnitFactor() == null)
		{
			return;
		}
		final PriceDataType priceType;
		final PriceInformation info;
		if (CollectionUtils.isEmpty(productModel.getVariants()))
		{
			priceType = PriceDataType.BUY;
			info = getCommercePriceService().getWebPriceForProduct(productModel);
		}
		else
		{
			priceType = PriceDataType.FROM;
			info = getCommercePriceService().getFromPriceForProduct(productModel);
		}
		if (info == null)
		{
			return;
		}
		final String currencyIso = info.getPriceValue().getCurrencyIso();
		final int unitFactor = findPriceRowByCurrency.getUnitFactor().intValue();
		final PriceData unitPrice = populateUnitPrice(priceVal, unitFactor, priceType, currencyIso);

		productData.setUnitPrice(unitPrice);

		getProductDiscountPopulator().populate(productModel, productData);

		if (productData.getDiscount() == null || productData.getDiscount().getPrice() == null
				|| productData.getDiscount().getPrice().getValue() == null || productData.getDiscount().getDiscountPrice() == null
				|| productData.getDiscount().getDiscountPrice().getValue() == null || productData.getDiscount().getSaving() == null
				|| productData.getDiscount().getSaving().getValue() == null || productData.getDiscount().getPercentage() == null)

		{
			return;
		}

		final double price = productData.getDiscount().getPrice().getValue().doubleValue();
		final double discountPrice = productData.getDiscount().getDiscountPrice().getValue().doubleValue();
		final double savingPrice = productData.getDiscount().getSaving().getValue().doubleValue();
		final double percentage = productData.getDiscount().getPercentage();


		final DiscountPriceData discountUnitPriceData = populateDiscountUnitPrice(price, discountPrice, savingPrice, percentage,
				unitFactor, priceType, currencyIso);
		productData.setDiscountUnitPrice(discountUnitPriceData);

	}

	/**
	 * Populate unit price.
	 *
	 * @param priceVal
	 *           the price val
	 * @param unitFactor
	 *           the unit factor
	 * @param priceType
	 *           the price type
	 * @param currencyIso
	 *           the currency iso
	 * @return the price data
	 */
	private PriceData populateUnitPrice(final double priceVal, final int unitFactor, final PriceDataType priceType,
			final String currencyIso)
	{
		return getPriceData(priceVal * unitFactor, priceType, currencyIso);
	}

	/**
	 * Populate discount unit price.
	 *
	 * @param price
	 *           the price
	 * @param discountPrice
	 *           the discount price
	 * @param savingPrice
	 *           the saving price
	 * @param percentage
	 *           the percentage
	 * @param unitFactor
	 *           the unit factor
	 * @param priceType
	 *           the price type
	 * @param currencyIso
	 *           the currency iso
	 * @return the discount price data
	 */
	private DiscountPriceData populateDiscountUnitPrice(final double price, final double discountPrice, final double savingPrice,
			final double percentage, final int unitFactor, final PriceDataType priceType, final String currencyIso)
	{
		final DiscountPriceData discountUnitPriceData = new DiscountPriceData();

		final PriceData priceData = getPriceData(price * unitFactor, priceType, currencyIso);
		final PriceData discountPriceData = getPriceData(discountPrice * unitFactor, priceType, currencyIso);
		final PriceData savingPriceData = getPriceData(savingPrice * unitFactor, priceType, currencyIso);

		discountUnitPriceData.setPrice(priceData);
		discountUnitPriceData.setDiscountPrice(discountPriceData);
		discountUnitPriceData.setSaving(savingPriceData);

		discountUnitPriceData.setPercentage(Double.valueOf(percentage));

		return discountUnitPriceData;

	}

	/**
	 * Find price row by currency.
	 *
	 * @param priceRowModels
	 *           the price row models
	 * @param isocodeCurrency
	 *           the isocode currency
	 * @return the price row model
	 */
	private PriceRowModel findPriceRowByCurrency(final Collection<PriceRowModel> priceRowModels, final String isocodeCurrency)
	{
		if (CollectionUtils.isEmpty(priceRowModels) || !StringUtils.isNoneEmpty(isocodeCurrency))
		{
			return null;
		}
		for (final PriceRowModel priceRowModel : priceRowModels)
		{
			if (priceRowModel.getCurrency() != null && isocodeCurrency.equals(priceRowModel.getCurrency().getIsocode()))
			{
				return priceRowModel;
			}
		}
		return null;
	}

	/**
	 * Gets the price data.
	 *
	 * @param value
	 *           the value
	 * @param priceType
	 *           the price type
	 * @param currencyIso
	 *           the currency iso
	 * @return the price data
	 */
	private PriceData getPriceData(final double value, final PriceDataType priceType, final String currencyIso)
	{
		return getPriceDataFactory().create(priceType, BigDecimal.valueOf(value), currencyIso);
	}

}
