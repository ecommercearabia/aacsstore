/**
 *
 */
package com.aacs.facades.populators.product;


import de.hybris.platform.commercefacades.product.converters.populator.ProductPrimaryImagePopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Collections;


/**
 * @author Tuqa
 *
 */
public class CustomProductPrimaryImagePopulator<SOURCE extends ProductModel, TARGET extends ProductData>
		extends ProductPrimaryImagePopulator<SOURCE, TARGET>
{
	@Override
	public void populate(final SOURCE productModel, final TARGET productData) throws ConversionException
	{
		try
		{
			super.populate(productModel, productData);

		}
		catch (final Exception e)
		{
			productData.setImages(Collections.emptyList());
		}

	}
}
