/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.facades.populators.product;

import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.ConfigurablePopulator;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;


/**
 * @author tuqa
 *
 */
public class SearchResultProductVariantOptionsPopulator implements Populator<SearchResultValueData, ProductData>
{
	private static final Logger LOG = Logger.getLogger(SearchResultProductVariantOptionsPopulator.class);

	protected static final List<ProductOption> PRODUCT_OPTIONS = Arrays.asList(ProductOption.VARIANT_FULL,
			ProductOption.VARIANT_MATRIX, ProductOption.VARIANT_FIRST_VARIANT, ProductOption.VARIANT_MATRIX_BASE,
			ProductOption.VARIANT_MATRIX_ALL_OPTIONS, ProductOption.VARIANT_MATRIX_URL, ProductOption.VARIANT_MATRIX_MEDIA,
			ProductOption.VARIANT_MATRIX_PRICE, ProductOption.VARIANT_MATRIX_STOCK);


	@Resource(name = "productConfiguredPopulator")
	private ConfigurablePopulator<ProductModel, ProductData, ProductOption> productConfiguredPopulator;

	/**
	 * @return the productConfiguredPopulator
	 */
	protected ConfigurablePopulator<ProductModel, ProductData, ProductOption> getProductConfiguredPopulator()
	{
		return productConfiguredPopulator;
	}

	/** The product service. */
	@Resource(name = "productService")
	private ProductService productService;

	@Override
	public void populate(final SearchResultValueData source, final ProductData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		LOG.debug("Search Result Product Variant Options Populator .............");
		final String productCode = (String) source.getValues().get(ProductModel.CODE);
		try
		{
			final ProductModel productModel = productService.getProductForCode(productCode);
			if (productModel != null)
			{
				getProductConfiguredPopulator().populate(productModel, target, PRODUCT_OPTIONS);
			}
			else
			{
				LOG.warn("product code [" + productCode + "] not found");
			}

		}
		catch (final UnknownIdentifierException ex)
		{
			LOG.warn("product code [" + productCode + "] not found");
		}
	}

}
