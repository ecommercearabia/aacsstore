/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.facades.populators;


import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.DiscountPriceData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.OptionalInt;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import com.aacs.aacsfacades.units.UnitFactorRangeData;
import com.aacs.core.service.UnitFactorRangeDataFactory;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class UnitFactorRangeProductPopulator implements Populator<ProductModel, ProductData>
{

	@Resource(name = "unitFactorRangeDataFactory")
	private UnitFactorRangeDataFactory unitFactorRangeDataFactory;

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	@Resource(name = "commercePriceService")
	private CommercePriceService commercePriceService;

	@Resource(name = "productUnitPricePopulator")
	private Populator<ProductModel, ProductData> productUnitPricePopulator;

	@Resource(name = "cartService")
	private CartService cartService;

	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		if (source == null || target == null)
		{
			return;
		}
		final List<UnitFactorRangeData> unitFactorRangeData = getUnitFactorRangeDataFactory().create(source);
		target.setEnableUnitFactorRangeData(!CollectionUtils.isEmpty(unitFactorRangeData));
		target.setUnitFactorRangeData(unitFactorRangeData);


		productUnitPricePopulator.populate(source, target);


		if (!CollectionUtils.isEmpty(unitFactorRangeData))
		{
			for (final UnitFactorRangeData d : unitFactorRangeData)
			{
				populatePriceData(source, d, target.getDiscountUnitPrice());
			}
		}
		populateSelectedUnit(unitFactorRangeData, source.getCode(), target);
	}

	/**
	 * @param unitFactorRangeData
	 * @param productCode
	 * @param target
	 */
	private void populateSelectedUnit(final List<UnitFactorRangeData> unitFactorRangeData, final String productCode,
			final ProductData target)
	{
		if (!getCartService().hasSessionCart() || StringUtils.isBlank(productCode)
				|| CollectionUtils.isEmpty(getCartService().getSessionCart().getEntries()))
		{
			return;
		}

		final int selectedQTY = getEntryQty(getCartService().getSessionCart().getEntries(), productCode, target);
		target.setSelectedQuantity(selectedQTY);
		unitFactorRangeData.stream().forEach(unit -> unit.setSelected(unit.getQuantity() == selectedQTY));
	}

	/**
	 * @param entries
	 * @param productCode
	 * @param target
	 */
	private int getEntryQty(final List<AbstractOrderEntryModel> entries, final String productCode, final ProductData target)
	{
		final OptionalInt qty = entries.stream().filter(entry -> !Objects.isNull(entry) && !Objects.isNull(entry.getProduct())
				&& productCode.equals(entry.getProduct().getCode())).mapToInt(entry -> {
					target.setEntryId(String.valueOf(entry.getEntryNumber()));
					return entry.getQuantity() == null ? 0 : entry.getQuantity().intValue();
				}).findFirst();
		return qty.isEmpty() ? 0 : qty.getAsInt();
	}

	public void populatePriceData(final ProductModel productModel, final UnitFactorRangeData data,
			final DiscountPriceData discountPriceData) throws ConversionException
	{
		final PriceDataType priceType;
		final PriceInformation info;
		if (CollectionUtils.isEmpty(productModel.getVariants()))
		{
			priceType = PriceDataType.BUY;
			info = getCommercePriceService().getWebPriceForProduct(productModel);
		}
		else
		{
			priceType = PriceDataType.FROM;
			info = getCommercePriceService().getFromPriceForProduct(productModel);
		}

		if (info != null)
		{

			PriceData priceAfterSaving = null;
			final PriceData priceData = getPriceDataFactory().create(priceType,
					BigDecimal.valueOf(info.getPriceValue().getValue() * data.getQuantity()), info.getPriceValue().getCurrencyIso());
			priceAfterSaving = priceData;
			if (discountPriceData != null && discountPriceData.getDiscountPrice() != null
					&& discountPriceData.getDiscountPrice().getValue() != null)
			{
				final int unitFactor = getUnitFactorByPriceRowByCurrency(productModel, info.getPriceValue().getCurrencyIso());
				if (unitFactor == 0)
				{
					throw new ArithmeticException("unitFactor is 0");
				}
				final double price = discountPriceData.getPrice().getValue().doubleValue() / unitFactor;
				final double discountPrice = discountPriceData.getDiscountPrice().getValue().doubleValue() / unitFactor;
				final double savingPrice = discountPriceData.getSaving().getValue().doubleValue() / unitFactor;
				final double percentage = discountPriceData.getPercentage() == null ? 0
						: discountPriceData.getPercentage().doubleValue();

				final DiscountPriceData populateDiscountUnitPrice = populateDiscountUnitPrice(price, discountPrice, savingPrice,
						percentage, data.getQuantity(), priceType, info.getPriceValue().getCurrencyIso());

				data.setDiscountUnitPrice(populateDiscountUnitPrice);
				priceAfterSaving = populateDiscountUnitPrice.getDiscountPrice();

			}

			data.setPriceAfterSaving(priceAfterSaving);
			data.setPrice(priceData);
			data.setPurchasable(true);
		}
		else
		{
			data.setPurchasable(false);
		}
	}

	private int getUnitFactorByPriceRowByCurrency(final ProductModel productModel, final String isocodeCurrency)
	{
		final Collection<PriceRowModel> priceRowModels = productModel.getEurope1Prices();
		if (CollectionUtils.isEmpty(priceRowModels) || !StringUtils.isNoneEmpty(isocodeCurrency))
		{
			return 0;
		}
		for (final PriceRowModel priceRowModel : priceRowModels)
		{
			if (priceRowModel.getCurrency() != null && isocodeCurrency.equals(priceRowModel.getCurrency().getIsocode()))
			{
				return priceRowModel.getUnitFactor() == null ? 0 : priceRowModel.getUnitFactor().intValue();
			}
		}
		return 0;
	}

	/**
	 * Populate discount unit price.
	 *
	 * @param price
	 *           the price
	 * @param discountPrice
	 *           the discount price
	 * @param savingPrice
	 *           the saving price
	 * @param percentage
	 *           the percentage
	 * @param unitFactor
	 *           the unit factor
	 * @param priceType
	 *           the price type
	 * @param currencyIso
	 *           the currency iso
	 * @return the discount price data
	 */
	private DiscountPriceData populateDiscountUnitPrice(final double price, final double discountPrice, final double savingPrice,
			final double percentage, final int unitFactor, final PriceDataType priceType, final String currencyIso)
	{
		final DiscountPriceData discountUnitPriceData = new DiscountPriceData();

		final PriceData priceData = getPriceData(price * unitFactor, priceType, currencyIso);
		final PriceData discountPriceData = getPriceData(discountPrice * unitFactor, priceType, currencyIso);
		final PriceData savingPriceData = getPriceData(savingPrice * unitFactor, priceType, currencyIso);

		discountUnitPriceData.setPrice(priceData);
		discountUnitPriceData.setDiscountPrice(discountPriceData);
		discountUnitPriceData.setSaving(savingPriceData);

		discountUnitPriceData.setPercentage(Double.valueOf(percentage));

		return discountUnitPriceData;

	}

	private PriceData getPriceData(final double value, final PriceDataType priceType, final String currencyIso)
	{
		return getPriceDataFactory().create(priceType, BigDecimal.valueOf(value), currencyIso);
	}

	/**
	 * @return the unitFactorRangeDataFactory
	 */
	public UnitFactorRangeDataFactory getUnitFactorRangeDataFactory()
	{
		return unitFactorRangeDataFactory;
	}

	/**
	 * @return the priceDataFactory
	 */
	public PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	/**
	 * @return the commercePriceService
	 */
	public CommercePriceService getCommercePriceService()
	{
		return commercePriceService;
	}

	/**
	 * @return the cartFacade
	 */
	public CartService getCartService()
	{
		return cartService;
	}

}
