/**
 *
 */
package com.aacs.facades.populators;

import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.consignmenttrackingfacades.populators.ConsignmentForTrackingPopulator;
import de.hybris.platform.consignmenttrackingservices.delivery.data.ConsignmentEventData;
import de.hybris.platform.consignmenttrackingservices.model.CarrierModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;


/**
 * @author mnasro
 *
 */
public class CustomConsignmentForTrackingPopulator extends ConsignmentForTrackingPopulator
{
	@Override
	public void populate(final ConsignmentModel source, final ConsignmentData target)
	{
		final CarrierModel carrierModel = source.getCarrierDetails();
		if (carrierModel != null)
		{
			target.setCarrierDetails(getCarrierConverter().convert(carrierModel));
		}

		final List<ConsignmentEventData> events = getConsignmentTrackingService().getConsignmentEvents(source);
		if (StringUtils.isNotBlank(source.getTrackingID()) && source.getCarrierDetails() != null
				&& CollectionUtils.isNotEmpty(events))
		{
			target.setTrackingEvents(sortEvents(events));
		}
		else
		{
			target.setTrackingEvents(Collections.emptyList());
		}

		target.setTargetArrivalDate(getTargetArrivalDate(source, events));
		target.setStatusDate(source.getShippingDate());
		target.setCreateDate(source.getOrder() == null ? null : source.getOrder().getDate());
		target.setTargetShipDate(source.getShippingDate());
	}
}
