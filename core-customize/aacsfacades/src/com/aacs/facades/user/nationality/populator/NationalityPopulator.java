/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.facades.user.nationality.populator;

import de.hybris.platform.commercefacades.user.data.NationalityData;
import de.hybris.platform.converters.Populator;

import org.springframework.util.Assert;

import com.aacs.core.model.NationalityModel;


/**
 * The Class NationalityPopulator.
 *
 * @author mnasro
 */
public class NationalityPopulator implements Populator<NationalityModel, NationalityData>
{

	/**
	 * Fill the source to target.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	@Override
	public void populate(final NationalityModel source, final NationalityData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setCode(source.getCode());
		target.setName(source.getName());
	}

}
