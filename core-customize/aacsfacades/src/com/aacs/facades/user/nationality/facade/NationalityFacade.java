/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.facades.user.nationality.facade;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commercefacades.user.data.NationalityData;

import java.util.List;
import java.util.Optional;


/**
 * The Interface NationalityFacade.
 *
 * @author mnasro
 */
public interface NationalityFacade
{

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	public List<NationalityData> getAll();

	/**
	 * Gets the by site.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return the by site
	 */
	public List<NationalityData> getBySite(CMSSiteModel cmsSiteModel);

	/**
	 * Gets the by current site.
	 *
	 * @return the by current site
	 */
	public List<NationalityData> getByCurrentSite();

	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	public Optional<NationalityData> get(final String code);
}
