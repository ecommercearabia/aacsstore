/**
 *
 */
package com.aacs.facades.user.delivery;

import java.util.Optional;

import com.aacs.aacsfacades.customer.DeliveryCityAreaData;
import com.aacs.aacsfacades.customer.TimeDeliveryLocationData;
import com.aacs.aacstimeslotfacades.exception.TimeSlotException;


/**
 * @author mohammad-abumuhasien
 *
 */
public interface DeliveryLocationService
{
	public Optional<TimeDeliveryLocationData> saveCustomerDeliveryLocation(final String AreaCode, final String cityCode)
			throws TimeSlotException;

	public Optional<TimeDeliveryLocationData> getTimeDeliveryLocationData(final String areaCode, final String cityCode)
			throws TimeSlotException;

	public Optional<TimeDeliveryLocationData> getTimeDeliveryLocationDataForCurrentCustomer() throws TimeSlotException;

	Optional<DeliveryCityAreaData> getSelectedDeliveryCityAndArea();
}
