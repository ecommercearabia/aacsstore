/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.facades.user.customer.facade;

import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;

import com.aacs.facades.customer.data.MaritalStatusData;


/**
 *
 */
public interface CustomCustomerFacade extends CustomerFacade
{

	CustomerData getCustomerByCustomerId(String id);

	boolean isCustomerExists(String customerId);

	List<MaritalStatusData> getAllMaritailStatuses();

	void updateSignatureIdCurrentCustomer(String signatureId);

	public void registerCustomerInLoyalty(CustomerModel customer);

	public void unregisterCustomerInLoyalty(CustomerModel customer);

	public void setLoyalty(CustomerModel customer, boolean isInvolvedInLoyalty);

	public void setLoyaltyByCurrentCustomer(boolean isInvolvedInLoyalty);


	public void registerCustomerInLoyaltyByCurrentCustomer();

	public void unregisterCustomerInLoyaltyByCurrentCustomer();


}
