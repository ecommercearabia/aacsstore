/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.facades.user.customer.populator;

import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.NationalityData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.aacs.core.enums.MaritalStatus;
import com.aacs.core.model.NationalityModel;
import com.aacs.facades.customer.data.MaritalStatusData;


/**
 * The Class CustomerPopulator.
 *
 * @author mnasro
 *
 *         Converter implementation for {@link de.hybris.platform.core.model.user.UserModel} as source and
 *         {@link de.hybris.platform.commercefacades.user.data.CustomerData} as target type.
 */


public class CustomerPopulator implements Populator<CustomerModel, CustomerData>
{

	/** The country converter. */
	@Resource(name = "countryConverter")
	private Converter<CountryModel, CountryData> countryConverter;

	/** The country converter. */
	@Resource(name = "nationalityConverter")
	private Converter<NationalityModel, NationalityData> nationalityConverter;

	@Resource(name = "maritalStatusDataConverter")
	private Converter<MaritalStatus, MaritalStatusData> maritalStatusDataConverter;

	/**
	 * Fill the source to target.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	@Override
	public void populate(final CustomerModel source, final CustomerData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setMobileCountry(
				source.getMobileCountry() == null ? null : getCountryConverter().convert(source.getMobileCountry()));

		target.setMobileNumber(source.getMobileNumber());
		target.setCustomerType(source.getType() == null ? null : source.getType().toString());
		target.setNationality(source.getNationality() == null ? null : getNationalityConverter().convert(source.getNationality()));
		target.setNationalityID(source.getNationalityID());
		target.setBirthDate(source.getBirthOfDate());
		if (source.getMaritalStatus() != null)
		{
			target.setMaritalStatus(maritalStatusDataConverter.convert(source.getMaritalStatus()));
		}
		target.setInvolvedInLoyaltyProgram(source.isInvolvedInLoyaltyProgram());
	}



	/**
	 * @return the nationalityConverter
	 */
	public Converter<NationalityModel, NationalityData> getNationalityConverter()
	{
		return nationalityConverter;
	}



	/**
	 * Gets the country converter.
	 *
	 * @return the countryConverter
	 */
	public Converter<CountryModel, CountryData> getCountryConverter()
	{
		return countryConverter;
	}

}
