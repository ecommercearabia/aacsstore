/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.facades.user.customer.facade.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.customer.impl.DefaultCustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.CustomerService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.event.UpdatedProfileEvent;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import com.aacs.aacsloyaltyprogramprovider.context.LoyaltyProgramContext;
import com.aacs.core.enums.MaritalStatus;
import com.aacs.core.model.NationalityModel;
import com.aacs.core.user.nationality.service.NationalityService;
import com.aacs.core.user.service.MobilePhoneService;
import com.aacs.core.user.service.SingatureIdEncoderService;
import com.aacs.facades.customer.data.MaritalStatusData;
import com.aacs.facades.user.customer.facade.CustomCustomerFacade;
import com.google.common.collect.Lists;


/**
 * @author mnasro
 *
 *         Default implementation for the {@link CustomerFacade}.
 */
public class DefaultCustomCustomerFacade extends DefaultCustomerFacade implements CustomCustomerFacade
{
	/**
	 *
	 */
	private static final String CUSTOMER_NULL_MSG = "customer cannot be null";

	private static final Logger LOG = Logger.getLogger(DefaultCustomCustomerFacade.class);

	/** The customer converter. */
	@Resource(name = "customerConverter")
	private Converter<UserModel, CustomerData> customerConverter;

	/** The customer reverse converter. */
	@Resource(name = "customerReverseConverter")
	private Converter<CustomerData, CustomerModel> customerReverseConverter;

	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Resource(name = "customerService")
	private CustomerService customerService;
	@Resource(name = "nationalityService")
	private NationalityService nationalityService;
	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "maritalStatusDataConverter")
	private Converter<MaritalStatus, MaritalStatusData> maritalStatusDataConverter;

	@Resource(name = "singatureIdEncoderService")
	private SingatureIdEncoderService singatureIdEncoderService;

	@Resource(name = "loyaltyProgramContext")
	private LoyaltyProgramContext loyaltyProgramContext;

	/**
	 * Sets the common properties for register.
	 *
	 * @param registerData
	 *           the register data
	 * @param customerModel
	 *           the customer model
	 */
	@Override
	protected void setCommonPropertiesForRegister(final RegisterData registerData, final CustomerModel customerModel)
	{
		customerModel.setName(getCustomerNameStrategy().getName(registerData.getFirstName(), registerData.getLastName()));
		setTitleForRegister(registerData, customerModel);
		setUidForRegister(registerData, customerModel);
		setMobileNumberForRegister(registerData, customerModel);
		customerModel.setMobileNumber(registerData.getMobileNumber());
		customerModel.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
		customerModel.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
		customerModel.setUid(customerModel.getUid());
		customerModel.setNationalityID(registerData.getNationalityId());
		customerModel.setBirthOfDate(registerData.getBirthDate());
		customerModel.setCreatedFrom(registerData.getSalesApp());
		if (StringUtils.isNotBlank(registerData.getNationality()))
		{
			final Optional<NationalityModel> nationalityModel = nationalityService.get(registerData.getNationality());
			if (nationalityModel.isPresent())
			{
				customerModel.setNationality(nationalityModel.get());

			}
		}
		if (StringUtils.isNotBlank(registerData.getMaritalStatusCode()))
		{
			customerModel.setMaritalStatus(MaritalStatus.valueOf(registerData.getMaritalStatusCode()));
		}
		customerModel.setInvolvedInLoyaltyProgram(registerData.isInvolvedInLoyaltyProgram());
	}



	/**
	 * Update profile.
	 *
	 * @param customerData
	 *           the customer data
	 * @throws DuplicateUidException
	 *            the duplicate uid exception
	 */
	@Override
	public void updateProfile(final CustomerData customerData) throws DuplicateUidException
	{
		final String name = getCustomerNameStrategy().getName(customerData.getFirstName(), customerData.getLastName());
		final CustomerModel customer = getCurrentSessionCustomer();
		customer.setOriginalUid(customerData.getDisplayUid());
		customerReverseConverter.convert(customerData, customer);
		getCustomerAccountService().updateProfile(customer, customerData.getTitleCode(), name, customerData.getUid());
		LOG.info("Customer data has been updated successfully");
	}

	/**
	 * Sets the mobile number for register.
	 *
	 * @param registerData
	 *           the register data
	 * @param customerModel
	 *           the customer model
	 */
	protected void setMobileNumberForRegister(final RegisterData registerData, final CustomerModel customerModel)
	{
		if (StringUtils.isNotBlank(registerData.getMobileCountry()) && StringUtils.isNotBlank(registerData.getMobileNumber()))
		{
			final Optional<String> normalizedPhoneNumber = mobilePhoneService
					.validateAndNormalizePhoneNumberByIsoCode(registerData.getMobileCountry(), registerData.getMobileNumber());

			if (normalizedPhoneNumber.isPresent())
			{
				customerModel.setMobileNumber(normalizedPhoneNumber.get());
			}
			else
			{
				customerModel.setMobileNumber(registerData.getMobileNumber());
			}

			customerModel.setMobileCountry(getCommonI18NService().getCountry(registerData.getMobileCountry()));
		}
	}

	@Override
	public void updateFullProfile(final CustomerData customerData) throws DuplicateUidException
	{
		validateDataBeforeUpdate(customerData);

		final CustomerModel customer = getCurrentSessionCustomer();
		customerReverseConverter.convert(customerData, customer);
		if (customer.getDefaultPaymentAddress() != null)
		{
			getModelService().save(customer.getDefaultPaymentAddress());
		}

		if (customer.getDefaultShipmentAddress() != null)
		{
			getModelService().save(customer.getDefaultShipmentAddress());
		}

		getModelService().save(customer);
		getEventService().publishEvent(initializeCommerceEvent(new UpdatedProfileEvent(), customer));
	}

	/**
	 * Gets the current customer.
	 *
	 * @return the current customer
	 */
	@Override
	public CustomerData getCurrentCustomer()
	{
		return customerConverter.convert(getCurrentUser());
	}

	@Override
	public CustomerData getCustomerByCustomerId(final String id)
	{
		if (id != null)
		{
			final CustomerModel customerModel = customerService.getCustomerByCustomerId(id);
			if (customerModel != null)
			{
				return customerConverter.convert(customerModel);
			}
		}
		return null;
	}

	@Override
	public boolean isCustomerExists(final String customerId)
	{
		if (customerId != null)
		{
			return getCustomerByCustomerId(customerId) != null;
		}
		return false;
	}

	public List<MaritalStatusData> getAllMaritailStatuses()
	{

		return maritalStatusDataConverter.convertAll(Lists.newArrayList(MaritalStatus.values()));
	}

	@Override
	public void updateSignatureIdCurrentCustomer(final String signatureId)
	{
		final UserModel currentUser = getCurrentUser();
		if (currentUser == null || !(currentUser instanceof CustomerModel))
		{
			return;
		}
		final CustomerModel customer = (CustomerModel) currentUser;
		singatureIdEncoderService.setSignatureIdForCustomer(customer, signatureId);
		getModelService().save(customer);
	}

	@Override
	public void register(final RegisterData registerData) throws DuplicateUidException
	{
		validateParameterNotNullStandardMessage("registerData", registerData);
		Assert.hasText(registerData.getFirstName(), "The field [FirstName] cannot be empty");
		Assert.hasText(registerData.getLastName(), "The field [LastName] cannot be empty");
		Assert.hasText(registerData.getLogin(), "The field [Login] cannot be empty");

		final CustomerModel newCustomer = getModelService().create(CustomerModel.class);
		setCommonPropertiesForRegister(registerData, newCustomer);
		getCustomerAccountService().register(newCustomer, registerData.getPassword());
		getModelService().refresh(newCustomer);
		registerForLoyaltyProgram(newCustomer);


	}

	protected void registerForLoyaltyProgram(final CustomerModel customer)
	{
		if (!customer.isInvolvedInLoyaltyProgram())
		{
			return;
		}

		try
		{
			loyaltyProgramContext.registerCustomerByCurrentBaseStore(customer);
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage());
		}
	}


	@Override
	public void updateCartWithGuestForAnonymousCheckout(final CustomerData guestCustomerData)
	{

		// First thing to do is to try to change the user on the session cart
		if (getCartService().hasSessionCart())
		{
			getCartService().changeCurrentCartUser(getUserService().getUserForUID(guestCustomerData.getUid()));
		}

		// Update the session currency (which might change the cart currency)
		if (!updateSessionCurrency(guestCustomerData.getCurrency(), getStoreSessionFacade().getDefaultCurrency()))
		{
			// Update the user
			getUserFacade().syncSessionCurrency();
		}

		if (!updateSessionLanguage(guestCustomerData.getLanguage(), getStoreSessionFacade().getDefaultLanguage()))
		{
			// Update the user
			getUserFacade().syncSessionLanguage();
		}

		// Calculate the cart after setting everything up
		if (getCartService().hasSessionCart())
		{
			final CartModel sessionCart = getCartService().getSessionCart();

			// Clear the delivery address, delivery mode, payment info before starting the guest checkout.
			sessionCart.setDeliveryAddress(null);
			sessionCart.setDeliveryMode(null);
			sessionCart.setPaymentInfo(null);
			sessionCart.setTimeSlotInfo(null);
			sessionCart.setPaymentMode(null);
			getCartService().saveOrder(sessionCart);

			try
			{
				final CommerceCartParameter parameter = new CommerceCartParameter();
				parameter.setEnableHooks(true);
				parameter.setCart(sessionCart);
				getCommerceCartService().recalculateCart(parameter);
			}
			catch (final CalculationException ex)
			{
				LOG.error("Failed to recalculate order [" + sessionCart.getCode() + "]", ex);
			}
		}

	}



	@Override
	public void registerCustomerInLoyalty(final CustomerModel customer)
	{
		if (Objects.isNull(customer))
		{
			throw new IllegalArgumentException(CUSTOMER_NULL_MSG);
		}
		customer.setInvolvedInLoyaltyProgram(true);
		getModelService().save(customer);
		getModelService().refresh(customer);
		registerForLoyaltyProgram(customer);

	}


	@Override
	public void unregisterCustomerInLoyalty(final CustomerModel customer)
	{
		if (Objects.isNull(customer))
		{
			throw new IllegalArgumentException(CUSTOMER_NULL_MSG);
		}
		customer.setInvolvedInLoyaltyProgram(false);
		getModelService().save(customer);
		getModelService().refresh(customer);
	}


	@Override
	public void registerCustomerInLoyaltyByCurrentCustomer()
	{
		registerCustomerInLoyalty(getCurrentSessionCustomer());

	}

	@Override
	public void unregisterCustomerInLoyaltyByCurrentCustomer()
	{
		unregisterCustomerInLoyalty(getCurrentSessionCustomer());
	}

	@Override
	public void setLoyalty(final CustomerModel customer, final boolean isInvolvedInLoyalty)
	{
		if (Objects.isNull(customer))
		{
			throw new IllegalArgumentException(CUSTOMER_NULL_MSG);
		}

		customer.setInvolvedInLoyaltyProgram(isInvolvedInLoyalty);
		getModelService().save(customer);
		if (isInvolvedInLoyalty)
		{
			registerForLoyaltyProgram(customer);
		}

	}

	@Override
	public void setLoyaltyByCurrentCustomer(final boolean isInvolvedInLoyalty)
	{
		setLoyalty(getCurrentSessionCustomer(), isInvolvedInLoyalty);
	}



}
