/**
 *
 */
package com.aacs.facades.taskassignment.actions;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.warehousing.taskassignment.actions.AbstractTaskAssignmentActions;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;

import java.util.Optional;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class TaskAssignmentConsignmentDeliveryAction extends AbstractTaskAssignmentActions
{
	protected static final String CONSIGNMENT_ACTION_EVENT_NAME = "ConsignmentActionEvent";
	protected static final String CONFIRM_DELIVERY_CONSIGNMENT_CHOICE = "confirmConsignmentDelivery";
	protected static final String DELIVERING_TEMPLATE_CODE = "NPR_Delivering";

	public WorkflowDecisionModel perform(final WorkflowActionModel workflowAction)
	{
		WorkflowDecisionModel result = null;
		final Optional<ItemModel> attachedConsignment = getAttachedConsignment(workflowAction);
		if (attachedConsignment.isPresent() && attachedConsignment.get() instanceof ConsignmentModel)
		{
			final ConsignmentModel consignment = (ConsignmentModel) attachedConsignment.get();
			getWorkflowActionAndAssignPrincipal(DELIVERING_TEMPLATE_CODE, workflowAction, consignment, "delivered");
			getConsignmentBusinessProcessService().triggerChoiceEvent(consignment,
					CONSIGNMENT_ACTION_EVENT_NAME,
					CONFIRM_DELIVERY_CONSIGNMENT_CHOICE);
			result = workflowAction.getDecisions().isEmpty() ? null : workflowAction.getDecisions().iterator().next();
		}
		return result;
	}
}
