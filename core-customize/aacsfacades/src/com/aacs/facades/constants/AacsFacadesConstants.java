/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.facades.constants;

/**
 * Global class for all AacsFacades constants.
 */
public class AacsFacadesConstants extends GeneratedAacsFacadesConstants
{
	public static final String EXTENSIONNAME = "aacsfacades";

	private AacsFacadesConstants()
	{
		//empty
	}
}
