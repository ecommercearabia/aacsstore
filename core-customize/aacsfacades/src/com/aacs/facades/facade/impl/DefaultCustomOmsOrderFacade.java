/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.facades.facade.impl;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commerceservices.search.dao.impl.DefaultPagedGenericDao;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordermanagementfacades.order.impl.DefaultOmsOrderFacade;
import de.hybris.platform.servicelayer.internal.dao.SortParameters;
import de.hybris.platform.servicelayer.internal.dao.SortParameters.SortOrder;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.aacs.core.dao.impl.OrderByNullVersionIdWithCodePagedDao;
import com.aacs.facades.facade.CustomOmsOrderFacade;


/**
 *
 */
public class DefaultCustomOmsOrderFacade extends DefaultOmsOrderFacade implements CustomOmsOrderFacade
{

	@Resource(name = "orderByNullVersionIdWithCodePagedDao")
	private OrderByNullVersionIdWithCodePagedDao orderByNullVersionIdWithCodePagedDao;

	/**
	 * @return the orderByNullVersionIdWithCodePagedDao
	 */
	protected OrderByNullVersionIdWithCodePagedDao getOrderByNullVersionIdWithCodePagedDao()
	{
		return orderByNullVersionIdWithCodePagedDao;
	}

	@Resource(name = "orderPagedGenericDao")
	private DefaultPagedGenericDao defaultPagedGenericDao;


	/**
	 * @return the defaultPagedGenericDao
	 */
	public DefaultPagedGenericDao getDefaultPagedGenericDao()
	{
		return defaultPagedGenericDao;
	}

	@Override
	public SearchPageData<OrderData> searchOrderByCode(final PageableData pageableData, final String code)
	{
		if (!StringUtils.isBlank(code))
		{
			final Map<String, String> param = new HashMap<>();
			param.put("code", code);
			final SearchPageData<OrderModel> orderSearchPageData = getOrderByNullVersionIdWithCodePagedDao().findCustom(param,
					pageableData);
			return convertSearchPageData(orderSearchPageData, getOrderConverter());
		}
		else
		{
			return getOrders(pageableData);
		}

	}



	@Override
	public SearchPageData<OrderData> searchOrderByCode(final PageableData pageableData, final String code, final String sort)
	{
		if (!StringUtils.isBlank(code))
		{
			final Map<String, String> param = new HashMap<>();
			param.put("code", code);

			final SearchPageData<OrderModel> orderSearchPageData = getOrderByNullVersionIdWithCodePagedDao().findCustom(param,
					pageableData);
			return convertSearchPageData(orderSearchPageData, getOrderConverter());
		}
		else
		{
			return getOrders(pageableData, sort);
		}
	}


	public SearchPageData<OrderData> getOrders(final PageableData pageableData, final String sort)
	{
		final SortParameters sortParameter = new SortParameters();
		switch (sort)
		{
			case "asc":
				sortParameter.addSortParameter("code", SortOrder.ASCENDING);
				break;
			case "desc":
				sortParameter.addSortParameter("code", SortOrder.DESCENDING);
				break;

			default:
				sortParameter.addSortParameter("code", SortOrder.ASCENDING);
				break;
		}

		@SuppressWarnings("removal")
		final SearchPageData<OrderModel> orderSearchPageData = getDefaultPagedGenericDao().find(new HashMap<String, Object>(),
				sortParameter, pageableData);
		return convertSearchPageData(orderSearchPageData, getOrderConverter());
	}
}
