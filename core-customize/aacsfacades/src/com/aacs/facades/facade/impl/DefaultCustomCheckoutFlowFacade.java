/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.facades.facade.impl;

import de.hybris.platform.acceleratorfacades.flow.CheckoutFlowFacade;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import com.aacs.aacstimeslotfacades.exception.TimeSlotException;
import com.aacs.aacstimeslotfacades.facade.TimeSlotFacade;
import com.aacs.aacstimeslotfacades.validation.TimeSlotValidationService;
import com.aacs.core.order.cart.exception.CartValidationException;
import com.aacs.core.order.cart.service.CartValidationService;
import com.aacs.facades.facade.CustomAcceleratorCheckoutFacade;
import com.aacs.facades.facade.CustomCheckoutFlowFacade;




/**
 * @author mnasro
 *
 *         The Class DefaultCustomCheckoutFlowFacade.
 */
public class DefaultCustomCheckoutFlowFacade implements CustomCheckoutFlowFacade
{

	/** The checkout flow facade. */
	@Resource(name = "checkoutFlowFacade")
	private CheckoutFlowFacade checkoutFlowFacade;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "acceleratorCheckoutFacade")
	private CustomAcceleratorCheckoutFacade checkoutFacade;

	@Resource(name = "cartValidationService")
	private CartValidationService cartValidationService;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "timeSlotFacade")
	private TimeSlotFacade timeSlotFacade;

	@Resource(name = "timeSlotValidationService")
	private TimeSlotValidationService timeSlotValidationService;

	@Resource(name = "userService")
	private UserService userService;

	@Override
	public boolean hasValidCart()
	{
		try
		{
			return checkoutFlowFacade.hasValidCart() && cartService.hasSessionCart()
					&& cartValidationService.validateCartMaxAmount(cartService.getSessionCart())
					&& cartValidationService.validateCartMinAmount(cartService.getSessionCart())
					&& cartValidationService.validateCartDeliveryType(cartService.getSessionCart());
		}
		catch (final CartValidationException e)
		{
			return false;
		}
	}

	/**
	 * Gets the checkout flow facade.
	 *
	 * @return the checkoutFlowFacade
	 */
	public de.hybris.platform.acceleratorfacades.flow.CheckoutFlowFacade getCheckoutFlowFacade()
	{
		return checkoutFlowFacade;
	}

	/**
	 * Checks for no payment info.
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean hasNoPaymentInfo()
	{
		final CartData cartData = getCheckoutFlowFacade().getCheckoutCart();
		return cartData == null || (cartData.getPaymentInfo() == null && cartData.getNoCardPaymentInfo() == null);
	}

	@Override
	public boolean hasNoPaymentMode()
	{
		final CartData cartData = getCheckoutFlowFacade().getCheckoutCart();
		return cartData == null || cartData.getPaymentMode() == null;
	}

	@Override
	public boolean hasPaymentProvider()
	{
		if (!"card".equalsIgnoreCase(getCheckoutFacade().getCheckoutCart().getPaymentMode().getCode()))
		{
			return false;
		}

		return getCheckoutFacade().getSupportedPaymentProvider().isPresent();
	}

	/**
	 * @return the checkoutFacade
	 */
	public CustomAcceleratorCheckoutFacade getCheckoutFacade()
	{
		return checkoutFacade;
	}

	@Override
	public boolean hasNoTimeSlot() throws TimeSlotException
	{
		final CartData cartData = getCheckoutFlowFacade().getCheckoutCart();
		return timeSlotFacade.isTimeSlotEnabledByCurrentSite()
				&& !timeSlotValidationService.validate(cartData.getTimeSlotInfoData());
	}

	@Override
	public boolean isTimeSlotEnabledByCurrentSite() throws TimeSlotException
	{
		return timeSlotFacade.isTimeSlotEnabledByCurrentSite();
	}

	@Override
	public boolean isAddressVerified()
	{
		if (!getCheckoutFlowFacade().hasCheckoutCart())
		{
			return false;
		}
		final CartData cart = getCheckoutFlowFacade().getCheckoutCart();
		if (cart.getDeliveryAddress() == null)
		{
			return false;
		}

		return cart.getDeliveryAddress().isVerified();
	}

	@Override
	public boolean shouldVerifyAddress()
	{
		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();
		if (currentSite == null)
		{
			return false;
		}
		final boolean anonymousUser = getUserService().isAnonymousUser(getUserService().getCurrentUser());
		final boolean forCustomer = !anonymousUser && currentSite.isEnableOTPVerificationOnCustomerAddress();

		final boolean forGuest = anonymousUser && currentSite.isEnableOTPVerificationOnGuestAddress();
		return forCustomer || forGuest;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @return the cmsSiteService
	 */
	public CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

}
