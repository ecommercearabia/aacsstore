/*
 *
 */
package com.aacs.facades.facade.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.acceleratorfacades.order.impl.DefaultAcceleratorCheckoutFacade;
import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.acceleratorservices.model.payment.CCPaySubValidationModel;
import de.hybris.platform.acceleratorservices.payment.dao.CreditCardPaymentSubscriptionDao;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.PaymentSubscriptionResultItem;
import de.hybris.platform.acceleratorservices.payment.enums.DecisionsEnum;
import de.hybris.platform.acceleratorservices.payment.strategies.CreditCardPaymentInfoCreateStrategy;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.NoCardPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.PaymentModeData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.NoCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.payment.dto.BillingInfo;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.store.BaseStoreModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.aacs.aacsloyaltyprogramfacades.data.LoyaltyBalanceData;
import com.aacs.aacsloyaltyprogramfacades.data.LoyaltyPaymentModeData;
import com.aacs.aacsloyaltyprogramfacades.data.LoyaltyUsablePointData;
import com.aacs.aacsloyaltyprogramfacades.facades.LoyaltyPaymentFacade;
import com.aacs.aacsloyaltyprogramfacades.facades.LoyaltyPaymentModeFacade;
import com.aacs.aacsloyaltyprogramprovider.exception.AACSLoyaltyException;
import com.aacs.aacsloyaltyprogramprovider.service.LoyaltyPaymentModeService;
import com.aacs.aacspayment.context.PaymentContext;
import com.aacs.aacspayment.context.PaymentProviderContext;
import com.aacs.aacspayment.customer.service.CustomCustomerAccountService;
import com.aacs.aacspayment.entry.PaymentRequestData;
import com.aacs.aacspayment.entry.PaymentResponseData;
import com.aacs.aacspayment.enums.PaymentOperationType;
import com.aacs.aacspayment.enums.PaymentResponseStatus;
import com.aacs.aacspayment.exception.PaymentException;
import com.aacs.aacspayment.exception.type.PaymentExceptionType;
import com.aacs.aacspayment.model.PaymentProviderModel;
import com.aacs.aacspayment.model.PaymentTransactionInfoModel;
import com.aacs.aacspayment.mpgs.beans.ReturnCallbackResponseBean;
import com.aacs.aacspayment.service.PaymentTransactionInfoService;
import com.aacs.aacspayment.strategy.CustomPaymentTransactionStrategy;
import com.aacs.aacsstorecredit.service.StoreCreditModeService;
import com.aacs.aacsstorecredit.service.StoreCreditService;
import com.aacs.aacsstorecreditfacades.data.StoreCreditModeData;
import com.aacs.aacsstorecreditfacades.facade.StoreCreditFacade;
import com.aacs.aacsstorecreditfacades.facade.StoreCreditModeFacade;
import com.aacs.aacstimeslot.model.TimeSlotInfoModel;
import com.aacs.aacstimeslot.service.TimeSlotService;
import com.aacs.aacstimeslotfacades.TimeSlotData;
import com.aacs.aacstimeslotfacades.TimeSlotInfoData;
import com.aacs.aacstimeslotfacades.exception.TimeSlotException;
import com.aacs.aacstimeslotfacades.facade.TimeSlotFacade;
import com.aacs.core.context.SerialNumberConfigurationContext;
import com.aacs.core.enums.SerialNumberSource;
import com.aacs.core.service.CustomCartService;
import com.aacs.core.service.CustomOrderService;
import com.aacs.core.service.MerchantTransactionService;
import com.aacs.facades.facade.CustomAcceleratorCheckoutFacade;
import com.aacs.facades.user.facade.CustomUserFacade;
import com.google.common.base.Preconditions;


/**
 *
 * /** The Class DefaultCustomAcceleratorCheckoutFacade.
 *
 * @author mnasro
 *
 */
public class DefaultCustomAcceleratorCheckoutFacade extends DefaultAcceleratorCheckoutFacade
		implements CustomAcceleratorCheckoutFacade
{

	private static final String ACQUIRER_CODE = "acquirerCode";

	private static final String CONTINUE = "CONTINUE";

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultCustomAcceleratorCheckoutFacade.class);

	/** The Constant SUBSCRIPTION_INFO_DATA_CANNOT_BE_NULL_MSG. */
	private static final String SUBSCRIPTION_INFO_DATA_CANNOT_BE_NULL_MSG = "SubscriptionInfoData cannot be null";

	/** The Constant SIGNATURE_DATA_CANNOT_BE_NULL_MSG. */
	private static final String SIGNATURE_DATA_CANNOT_BE_NULL_MSG = "SignatureData cannot be null";

	/** The Constant PAYMENT_INFO_DATA_CANNOT_BE_NULL_MSG. */
	private static final String PAYMENT_INFO_DATA_CANNOT_BE_NULL_MSG = "PaymentInfoData cannot be null";

	/** The Constant ORDER_INFO_DATA_CANNOT_BE_NULL_MSG. */
	private static final String ORDER_INFO_DATA_CANNOT_BE_NULL_MSG = "OrderInfoData cannot be null";

	/** The Constant CUSTOMER_INFO_DATA_CANNOT_BE_NULL_MSG. */
	private static final String CUSTOMER_INFO_DATA_CANNOT_BE_NULL_MSG = "CustomerInfoData cannot be null";

	/** The Constant AUTH_REPLY_DATA_CANNOT_BE_NULL_MSG. */
	private static final String AUTH_REPLY_DATA_CANNOT_BE_NULL_MSG = "AuthReplyData cannot be null";

	/** The Constant DECISION_CANNOT_BE_NULL_MSG. */
	private static final String DECISION_CANNOT_BE_NULL_MSG = "Decision cannot be null";

	/** The Constant CREATE_SUBSCRIPTION_RESULT_CANNOT_BE_NULL_MSG. */
	private static final String CREATE_SUBSCRIPTION_RESULT_CANNOT_BE_NULL_MSG = "CreateSubscriptionResult cannot be null";

	public static final String REDIRECT_PREFIX = "redirect:";

	protected static final String REDIRECT_URL_ERROR = REDIRECT_PREFIX + "/checkout/multi/hop/error";

	protected static final String REDIRECT_URL_ORDER_CONFIRMATION = REDIRECT_PREFIX + "/checkout/orderConfirmation/";

	private static final String ABSTRACTORDERMODEL_MUSTN_T_BE_NULL = "abstractOrderModel mustn't be null or empty";

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	/** The payment mode converter. */
	@Resource(name = "paymentModeConverter")
	private Converter<PaymentModeModel, PaymentModeData> paymentModeConverter;

	/** The customer account service. */
	@Resource(name = "customerAccountService")
	private CustomCustomerAccountService customerAccountService;

	/** The no card payment info converter. */
	@Resource(name = "noCardPaymentInfoConverter")
	private Converter<NoCardPaymentInfoModel, NoCardPaymentInfoData> noCardPaymentInfoConverter;

	/** The payment context. */
	@Resource(name = "paymentContext")
	private PaymentContext paymentContext;

	/** The payment provider context. */
	@Resource(name = "paymentProviderContext")
	private PaymentProviderContext paymentProviderContext;

	/** The payment transaction strategy. */
	@Resource(name = "defaultCustomPaymentTransactionStrategy")
	private CustomPaymentTransactionStrategy paymentTransactionStrategy;

	/** The credit card payment info create strategy. */
	@Resource(name = "creditCardPaymentInfoCreateStrategy")
	private CreditCardPaymentInfoCreateStrategy creditCardPaymentInfoCreateStrategy;

	/** The credit card payment subscription dao. */
	@Resource(name = "creditCardPaymentSubscriptionDao")
	private CreditCardPaymentSubscriptionDao creditCardPaymentSubscriptionDao;

	/** The payment subscription result data converter. */
	@Resource(name = "paymentSubscriptionResultDataConverter")
	private Converter<PaymentSubscriptionResultItem, PaymentSubscriptionResultData> paymentSubscriptionResultDataConverter;

	@Resource(name = "paymentTransactionInfoService")
	private PaymentTransactionInfoService paymentTransactionInfoService;



	/** The payment mode service. */
	@Resource(name = "paymentModeService")
	private PaymentModeService paymentModeService;

	/** The address reverse converter. */
	@Resource(name = "addressReverseConverter")
	private Converter<AddressData, AddressModel> addressReverseConverter;

	@Resource(name = "storeCreditModeService")
	private StoreCreditModeService storeCreditModeService;
	@Resource(name = "storeCreditService")
	private StoreCreditService storeCreditService;

	@Resource(name = "storeCreditModeFacade")
	private StoreCreditModeFacade storeCreditModeFacade;

	@Resource(name = "storeCreditFacade")
	private StoreCreditFacade storeCreditFacade;

	@Resource(name = "timeSlotFacade")
	private TimeSlotFacade timeSlotFacade;

	@Resource(name = "timeSlotInfoReverseConverter")
	private Converter<TimeSlotInfoData, TimeSlotInfoModel> timeSlotInfoReverseConverter;

	@Resource(name = "timeSlotService")
	private TimeSlotService timeSlotService;

	@Resource(name = "customOrderService")
	private CustomOrderService customOrderService;

	@Resource(name = "customCartService")
	private CustomCartService customCartService;

	@Resource(name = "commerceCartService")
	private CommerceCartService commerceCartService;



	@Resource(name = "loyaltyPaymentModeFacade")
	private LoyaltyPaymentModeFacade loyaltyPaymentModeFacade;

	@Resource(name = "loyaltyPaymentModeService")
	private LoyaltyPaymentModeService loyaltyPaymentModeService;

	@Resource(name = "loyaltyPaymentFacade")
	private LoyaltyPaymentFacade loyaltyPaymentFacade;

	@Resource(name = "userFacade")
	private CustomUserFacade userFacade;

	@Resource(name = "serialNumberConfigurationContext")
	private SerialNumberConfigurationContext serialNumberConfigurationContext;

	@Resource(name = "merchantTransactionService")
	private MerchantTransactionService merchantTransactionService;

	/**
	 * Gets the payment mode service.
	 *
	 * @return the payment mode service
	 */
	protected PaymentModeService getPaymentModeService()
	{
		return paymentModeService;
	}

	/**
	 * Checks for no payment info.
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean hasNoPaymentInfo()
	{
		final CartData cartData = getCheckoutCart();
		return cartData == null || (cartData.getPaymentInfo() == null && cartData.getNoCardPaymentInfo() == null);
	}

	/**
	 * Gets the supported payment modes.
	 *
	 * @return the supported payment modes
	 */
	@Override
	public Optional<List<PaymentModeData>> getSupportedPaymentModes()
	{
		final CartModel cartModel = getCart();
		if (cartModel == null)
		{
			return Optional.empty();
		}
		if (cartModel.getDeliveryMode() == null)
		{
			LOG.error("No DeliveryMode select For current cart code : " + cartModel.getCode());
			return Optional.empty();
		}

		if (CollectionUtils.isEmpty(cartModel.getDeliveryMode().getSupportedPaymentModes()))
		{
			LOG.error("No Payment Modes Defined For delivery Mode Code : " + cartModel.getDeliveryMode().getCode());
			return Optional.empty();
		}
		if (cartModel.getTotalPrice() == 0)
		{
			final List<PaymentModeModel> supportedPaymentModes = cartModel.getDeliveryMode().getSupportedPaymentModes().stream()
					.filter(Objects::nonNull).filter(p -> Boolean.TRUE.equals(p.getActive()) && CONTINUE.equalsIgnoreCase(p.getCode()))
					.collect(Collectors.toList());

			return Optional.ofNullable(paymentModeConverter.convertAll(supportedPaymentModes));
		}
		final List<PaymentModeModel> supportedPaymentModes = cartModel.getDeliveryMode().getSupportedPaymentModes().stream()
				.filter(Objects::nonNull).filter(p -> Boolean.TRUE.equals(p.getActive()) && !CONTINUE.equalsIgnoreCase(p.getCode()))
				.collect(Collectors.toList());

		return Optional.ofNullable(paymentModeConverter.convertAll(supportedPaymentModes));
	}

	/**
	 * Gets the default payment mode.
	 *
	 * @return the supported payment modes
	 */
	public Optional<PaymentModeData> getDefaultPaymentModesForStore(final BaseStoreModel baseStore)
	{
		if (baseStore == null || baseStore.getDefaultPaymentMode() == null)
		{
			return Optional.empty();
		}

		return Optional.ofNullable(paymentModeConverter.convert(baseStore.getDefaultPaymentMode()));
	}

	/**
	 * Creates the payment subscription.
	 *
	 * @param paymentInfoData
	 *           the payment info data
	 * @return the optional
	 */
	@Override
	public Optional<NoCardPaymentInfoData> createPaymentSubscription(final NoCardPaymentInfoData paymentInfoData)
	{
		validateParameterNotNullStandardMessage("paymentInfoData", paymentInfoData);
		final AddressData billingAddressData = paymentInfoData.getBillingAddress();
		validateParameterNotNullStandardMessage("billingAddressData", billingAddressData);
		if (checkIfCurrentUserIsTheCartUser())
		{
			final BillingInfo billingInfo = new BillingInfo();
			billingInfo.setCity(billingAddressData.getTown());
			billingInfo.setCountry(billingAddressData.getCountry() == null ? null : billingAddressData.getCountry().getIsocode());
			billingInfo.setRegion(billingAddressData.getRegion() == null ? null : billingAddressData.getRegion().getIsocode());
			billingInfo.setFirstName(billingAddressData.getFirstName());
			billingInfo.setLastName(billingAddressData.getLastName());
			billingInfo.setEmail(billingAddressData.getEmail());
			billingInfo.setPhoneNumber(billingAddressData.getPhone());
			billingInfo.setPostalCode(billingAddressData.getPostalCode());
			billingInfo.setStreet1(billingAddressData.getLine1());
			billingInfo.setStreet2(billingAddressData.getLine2());

			final Optional<NoCardPaymentInfoModel> noCardPaymentInfo = getCustomerAccountService().createPaymentSubscription(
					getCurrentUserForCheckout(), billingInfo, billingAddressData.getTitleCode(), getPaymentProvider(),
					paymentInfoData.isSaved(),
					paymentInfoData.getNoCardTypeData() == null ? null : paymentInfoData.getNoCardTypeData().getCode());

			return noCardPaymentInfo.isPresent()
					? Optional.ofNullable(getNoCardPaymentInfoConverter().convert(noCardPaymentInfo.get()))
					: Optional.empty();
		}
		return Optional.empty();
	}

	/**
	 * Sets the general payment details.
	 *
	 * @param paymentInfoId
	 *           the payment info id
	 * @return true, if successful
	 */
	@Override
	public boolean setGeneralPaymentDetails(final String paymentInfoId)
	{
		validateParameterNotNullStandardMessage("paymentInfoId", paymentInfoId);

		if (checkIfCurrentUserIsTheCartUser() && StringUtils.isNotBlank(paymentInfoId))
		{
			final CustomerModel currentUserForCheckout = getCurrentUserForCheckout();
			final Optional<PaymentInfoModel> paymentInfo = getCustomerAccountService().getPaymentInfoForCode(currentUserForCheckout,
					paymentInfoId);

			final CartModel cartModel = getCart();
			if (paymentInfo.isPresent())
			{
				final CommerceCheckoutParameter parameter = createCommerceCheckoutParameter(cartModel, true);
				parameter.setPaymentInfo(paymentInfo.get());
				return getCommerceCheckoutService().setPaymentInfo(parameter);
			}
			LOG.warn(String.format(
					"Did not find paymentInfoModel for user: %s, cart: %s &  paymentInfoId: %s. PaymentInfo Will not get set.",
					currentUserForCheckout, cartModel, paymentInfoId));
		}
		return false;
	}

	/**
	 * Gets the payment mode converter.
	 *
	 * @return the paymentModeConverter
	 */
	public Converter<PaymentModeModel, PaymentModeData> getPaymentModeConverter()
	{
		return paymentModeConverter;
	}

	/**
	 * Gets the no card payment info converter.
	 *
	 * @return the noCardPaymentInfoConverter
	 */
	public Converter<NoCardPaymentInfoModel, NoCardPaymentInfoData> getNoCardPaymentInfoConverter()
	{
		return noCardPaymentInfoConverter;
	}



	/**
	 * Gets the customer account service.
	 *
	 * @return the customer account service
	 */
	@Override
	protected CustomCustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}


	/**
	 * Sets the payment mode.
	 *
	 * @param paymentMode
	 *           the new payment mode
	 */
	@SuppressWarnings("removal")
	@Override
	public void setPaymentMode(final String paymentMode)
	{
		if (getCart() != null)
		{
			validateParameterNotNullStandardMessage("paymentMode", paymentMode);
			final PaymentModeModel paymentModeModel = getPaymentModeService().getPaymentModeForCode(paymentMode);
			getCart().setPaymentMode(paymentModeModel);
			if ("CARD".equalsIgnoreCase(paymentModeModel.getCode()) || CONTINUE.equalsIgnoreCase(paymentModeModel.getCode())
					|| "APPLE".equalsIgnoreCase(paymentModeModel.getCode()))
			{
				getCart().setPaymentStatus(PaymentStatus.PAID);
			}
			else
			{
				getCart().setPaymentStatus(PaymentStatus.NOTPAID);
			}
			if (!("CARD".equalsIgnoreCase(paymentModeModel.getCode()) || "APPLE".equalsIgnoreCase(paymentModeModel.getCode())))
			{
				getCart().setPaymentOperationType(PaymentOperationType.NONE);

			}

			getCart().setCalculated(Boolean.FALSE);

			getCommerceCheckoutService().calculateCart(getCart());
			getModelService().save(getCart());
		}
	}

	/**
	 * Gets the supported payment data.
	 *
	 * @return the supported payment data
	 */
	@Override
	public Pair<Optional<PaymentRequestData>, Boolean> getSupportedPaymentData()
	{

		try
		{
			return Pair.of(getPaymentContext().getPaymentDataByCurrentStore(getCart()), false);
		}
		catch (final PaymentException e)
		{
			if (PaymentExceptionType.ORDER_IS_ALREADY_PAID.equals(e.getExceptionType()))
			{
				return Pair.of(null, true);
			}


			return Pair.of(Optional.empty(), null);
		}
	}

	/**
	 * Save billing address.
	 *
	 * @param addressData
	 *           the address data
	 */
	@Override
	public void saveBillingAddress(final AddressData addressData)
	{
		if (getCart() != null)
		{
			final CartModel sessionCart = getCart();
			final AddressData deliveryAddressData = getDeliveryAddress();
			if (addressData.getId() != null && deliveryAddressData != null
					&& addressData.getId().equals(deliveryAddressData.getId()))
			{
				final AddressModel deliveryAddress = sessionCart.getDeliveryAddress();
				deliveryAddress.setBillingAddress(true);
				sessionCart.setPaymentAddress(deliveryAddress);
				getModelService().save(deliveryAddress);
			}
			else
			{
				final AddressModel convertReverse = addressReverseConverter.convert(addressData);
				convertReverse.setOwner(sessionCart.getUser());
				sessionCart.setPaymentAddress(convertReverse);
				getModelService().save(convertReverse);
				getModelService().save(sessionCart);
			}


		}
	}

	/**
	 * Gets the supported payment provider.
	 *
	 * @return the supported payment provider
	 */
	@Override
	public Optional<PaymentProviderModel> getSupportedPaymentProvider()
	{
		return getPaymentProviderContext().getProviderByCurrentStore();
	}

	/**
	 * Checks if is successful paid order.
	 *
	 * @param data
	 *           the data
	 * @return true, if is successful paid order
	 */
	@Override
	public boolean isSuccessfulPaidOrder(final Object data)
	{
		try
		{
			return getPaymentContext().isSuccessfulPaidOrderByCurrentStore(getCart(), data);
		}
		catch (final PaymentException e)
		{
			return false;
		}
	}

	/**
	 * Gets the payment response data.
	 *
	 * @param data
	 *           the data
	 * @return the payment response data
	 */
	@Override
	public Optional<PaymentResponseData> getPaymentResponseData(final Object data)
	{
		try
		{
			return getPaymentContext().getResponseDataByCurrentStore(getCart(), data);
		}
		catch (final PaymentException e)
		{
			return Optional.empty();
		}
	}

	/**
	 * Complete payment create subscription.
	 *
	 * @param orderInfoMap
	 *           the order info map
	 * @param saveInAccount
	 *           the save in account
	 * @return the optional
	 */
	@Override
	public Optional<PaymentSubscriptionResultData> completePaymentCreateSubscription(final Map<String, Object> orderInfoMap,
			final boolean saveInAccount)
	{

		CreateSubscriptionResult createSubscriptionResult = null;
		final Optional<CreateSubscriptionResult> response;
		try
		{
			response = getPaymentContext().interpretResponseByCurrentStore(orderInfoMap);
			if (response.isEmpty())
			{
				throw new IllegalArgumentException("Empty Response");
			}

			createSubscriptionResult = response.get();
		}
		catch (final Exception e)
		{
			throw new IllegalArgumentException("Empty Response");
		}

		ServicesUtil.validateParameterNotNull(createSubscriptionResult, CREATE_SUBSCRIPTION_RESULT_CANNOT_BE_NULL_MSG);
		Assert.notNull(createSubscriptionResult.getDecision(), DECISION_CANNOT_BE_NULL_MSG);
		Assert.notNull(createSubscriptionResult.getAuthReplyData(), AUTH_REPLY_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(createSubscriptionResult.getCustomerInfoData(), CUSTOMER_INFO_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(createSubscriptionResult.getOrderInfoData(), ORDER_INFO_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(createSubscriptionResult.getPaymentInfoData(), PAYMENT_INFO_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(createSubscriptionResult.getSignatureData(), SIGNATURE_DATA_CANNOT_BE_NULL_MSG);
		Assert.notNull(createSubscriptionResult.getSubscriptionInfoData(), SUBSCRIPTION_INFO_DATA_CANNOT_BE_NULL_MSG);

		final PaymentSubscriptionResultItem paymentSubscriptionResult = new PaymentSubscriptionResultItem();
		paymentSubscriptionResult.setSuccess(DecisionsEnum.ACCEPT.name().equalsIgnoreCase(response.get().getDecision()));
		paymentSubscriptionResult.setDecision(String.valueOf(response.get().getDecision()));
		paymentSubscriptionResult.setResultCode(String.valueOf(response.get().getReasonCode()));


		if (DecisionsEnum.ACCEPT.name().equalsIgnoreCase(response.get().getDecision()))
		{
			final CustomerModel customerModel = (CustomerModel) getCart().getUser();
			final PaymentTransactionEntryModel savePaymentTransactionEntry = getPaymentTransactionStrategy()
					.savePaymentTransactionEntry(customerModel, response.get().getRequestId(), response.get().getOrderInfoData());
			final CreditCardPaymentInfoModel cardPaymentInfoModel = getCreditCardPaymentInfoCreateStrategy().saveSubscription(
					customerModel, response.get().getCustomerInfoData(), response.get().getSubscriptionInfoData(),
					response.get().getPaymentInfoData(), saveInAccount);
			paymentSubscriptionResult.setStoredCard(cardPaymentInfoModel);

			// Check if the subscription has already been validated
			final CCPaySubValidationModel subscriptionValidation = getCreditCardPaymentSubscriptionDao()
					.findSubscriptionValidationBySubscription(cardPaymentInfoModel.getSubscriptionId());
			if (subscriptionValidation != null)
			{
				cardPaymentInfoModel.setSubscriptionValidated(true);
				getModelService().save(cardPaymentInfoModel);
				getModelService().remove(subscriptionValidation);
				getModelService().refresh(cardPaymentInfoModel);
			}

			if (savePaymentTransactionEntry != null && savePaymentTransactionEntry.getPaymentTransaction() != null)
			{
				final PaymentTransactionModel paymentTransaction = savePaymentTransactionEntry.getPaymentTransaction();
				paymentTransaction.setInfo(cardPaymentInfoModel);
				savePaymentTransactionEntry.setPaymentTransaction(paymentTransaction);
				final CartModel sessionCart = getCart();
				sessionCart.setPaymentTransactions(Arrays.asList(paymentTransaction));
				getModelService().save(sessionCart);
			}
		}
		else
		{
			final String logData = String.format("Cannot create subscription. Decision: %s - Reason Code: %s",
					response.get().getDecision(), response.get().getReasonCode());
			LOG.error(logData);
		}
		return Optional.ofNullable(getPaymentSubscriptionResultDataConverter().convert(paymentSubscriptionResult));
	}

	/**
	 * Gets the payment subscription result data converter.
	 *
	 * @return the payment subscription result data converter
	 */
	protected Converter<PaymentSubscriptionResultItem, PaymentSubscriptionResultData> getPaymentSubscriptionResultDataConverter()
	{
		return paymentSubscriptionResultDataConverter;
	}

	/**
	 * Gets the credit card payment subscription dao.
	 *
	 * @return the credit card payment subscription dao
	 */
	protected CreditCardPaymentSubscriptionDao getCreditCardPaymentSubscriptionDao()
	{
		return creditCardPaymentSubscriptionDao;
	}

	/**
	 * Gets the credit card payment info create strategy.
	 *
	 * @return the credit card payment info create strategy
	 */
	protected CreditCardPaymentInfoCreateStrategy getCreditCardPaymentInfoCreateStrategy()
	{
		return creditCardPaymentInfoCreateStrategy;
	}

	/**
	 * Gets the payment transaction strategy.
	 *
	 * @return the payment transaction strategy
	 */
	protected CustomPaymentTransactionStrategy getPaymentTransactionStrategy()
	{
		return paymentTransactionStrategy;
	}

	/**
	 * Gets the payment provider context.
	 *
	 * @return the payment provider context
	 */
	protected PaymentProviderContext getPaymentProviderContext()
	{
		return paymentProviderContext;
	}

	/**
	 * Gets the payment context.
	 *
	 * @return the payment context
	 */
	protected PaymentContext getPaymentContext()
	{
		return paymentContext;
	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseData(final Object data) throws PaymentException
	{
		return getPaymentContext().getPaymentOrderStatusResponseDataByCurrentStore(data, getCart());
	}

	@Override
	protected void afterPlaceOrder(final CartModel cartModel, final OrderModel orderModel)
	{
		redeemStoreCreditAmount(orderModel);
		if (getCartService().hasSessionCart())
		{
			getCartService().removeSessionCart();
		}
		else
		{
			getModelService().remove(cartModel);
		}

		if (orderModel != null)
		{
			final Optional<String> orderNumber = generateOrderNumber(orderModel);
			if (orderNumber.isPresent())
			{
				orderModel.setOrderRefId(orderNumber.get());
				getModelService().save(orderModel);
				getModelService().refresh(orderModel);
			}
		}
	}

	@Override
	public OrderData placeOrder(final SalesApplication salesApplication) throws InvalidCartException
	{

		final CartModel cartModel = getCart();

		final PaymentTransactionInfoModel savePaymentTransactionInfo = paymentTransactionInfoService.savePaymentTransactionInfo(
				"place order - before", cartModel.getSessionId() == null ? "sessionID is null" : cartModel.getSessionId(),
				"mgps null", "apikey null", cartModel);
		paymentTransactionInfoService.updatePaymentTransactionInfo(savePaymentTransactionInfo, "session info null", 0, "true");

		if (cartModel != null
				&& (cartModel.getUser().equals(getCurrentUserForCheckout()) || getCheckoutCustomerStrategy().isAnonymousCheckout()))
		{
			beforePlaceOrder(cartModel);
			final OrderModel orderModel = placeOrder(cartModel);
			final PaymentTransactionInfoModel savePaymentTransactionAfterInfo = paymentTransactionInfoService
					.savePaymentTransactionInfo("place order - after",
							cartModel.getSessionId() == null ? "sessionID is null" : cartModel.getSessionId(), "mgps null",
							"apikey null", orderModel);
			afterPlaceOrder(cartModel, orderModel);
			if (orderModel != null)
			{
				paymentTransactionInfoService.updatePaymentTransactionInfo(savePaymentTransactionAfterInfo, "session info null", 0,
						"true");
				orderModel.setSalesApplication(salesApplication);
				getModelService().save(orderModel);
				getModelService().refresh(orderModel);
				return getOrderConverter().convert(orderModel);
			}
		}
		paymentTransactionInfoService.updatePaymentTransactionInfo(savePaymentTransactionInfo, "session info null", 0,
				"an error occurred while placing order");
		return null;
	}

	@Override
	public void resetPaymentInfo()
	{
		final CartModel cartModel = getCart();
		if (cartModel != null)
		{
			cartModel.setPaymentInfo(null);
			getModelService().save(cartModel);
			getModelService().refresh(cartModel);
		}
	}

	@Override
	public Optional<List<StoreCreditModeData>> getSupportedStoreCreditModes()
	{
		return storeCreditModeFacade.getSupportedStoreCreditModesCurrentBaseStore();
	}

	@SuppressWarnings("removal")
	@Override
	public void setStoreCreditMode(final String StoreCreditTypeCode, final Double storeCreditAmaountSelected)
	{
		if (getCart() != null)
		{
			getCart().setStoreCreditMode(storeCreditModeService.getStoreCreditMode(StoreCreditTypeCode));
			getCart().setStoreCreditAmountSelected(storeCreditAmaountSelected);
			getCart().setCalculated(Boolean.FALSE);
			getCommerceCheckoutService().calculateCart(getCart());
			getModelService().save(getCart());
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.sacc.facades.facade.CustomAcceleratorCheckoutFacade#redeemStoreCreditAmount(de.hybris.platform.core.model.
	 * order.AbstractOrderModel)
	 */
	@Override
	public void redeemStoreCreditAmount(final AbstractOrderModel orderModel)
	{
		if (orderModel != null && orderModel.getStoreCreditAmount() != null && orderModel.getStoreCreditAmount().doubleValue() > 0)
		{
			storeCreditService.redeemStoreCreditAmount(orderModel);
		}
	}


	@Override
	public boolean isStoreCreditModeSupported(final String StoreCreditTypeCode)
	{
		return storeCreditModeFacade.isStoreCreditModeSupportedByCurrentBaseStore(StoreCreditTypeCode);
	}

	@Override
	public Optional<PriceData> getAvailableBalanceStoreCreditAmount()
	{
		if (getCart() == null)
		{
			return Optional.empty();
		}
		return storeCreditFacade.getStoreCreditAmountByCurrentUserAndCurrentBaseStore();
	}


	@Override
	public Optional<PriceData> getStoreCreditAmountFullRedeem()
	{
		if (getCart() == null)
		{
			return Optional.empty();
		}
		return storeCreditFacade.getStoreCreditAmountFullRedeem(getCart());
	}

	@Override
	public Optional<TimeSlotData> getSupportedTimeSlot()
	{
		final CartModel cartModel = getCart();
		Optional<TimeSlotData> timeSlot = Optional.empty();
		try
		{
			switch (cmsSiteService.getCurrentSite().getTimeSlotConfigType())
			{
				case BY_AREA:
					if (cartModel.getDeliveryAddress() == null || cartModel.getDeliveryAddress().getArea() == null
							|| StringUtils.isBlank(cartModel.getDeliveryAddress().getArea().getCode()))
					{
						return Optional.empty();
					}
					timeSlot = timeSlotFacade.getTimeSlotDataByArea(cartModel.getDeliveryAddress().getArea().getCode());

					break;
				case BY_DELIVERYMODE:
					if (cartModel.getDeliveryMode() == null || cartModel.getDeliveryMode().getCode() == null)
					{
						return Optional.empty();
					}
					timeSlot = timeSlotFacade.getTimeSlotData(cartModel.getDeliveryMode().getCode());

					break;

				default:
					timeSlot = timeSlotFacade.getTimeSlotData(cartModel.getDeliveryMode().getCode());

					break;
			}
		}
		catch (final TimeSlotException e)
		{
			LOG.error(e.getMessage(), e);
			return Optional.empty();
		}
		return timeSlot;
	}

	@Override
	public void setTimeSlot(final TimeSlotInfoData timeSlotInfoData)
	{
		validateParameterNotNullStandardMessage("timeSlotInfoData", timeSlotInfoData);
		final TimeSlotInfoModel infoModel = timeSlotInfoReverseConverter.convert(timeSlotInfoData);
		if (getCart() != null)
		{
			final CartModel sessionCart = getCart();
			timeSlotService.saveTimeSlotInfo(infoModel, sessionCart, timeSlotInfoData.getDate(), timeSlotInfoData.getStart());
		}
	}

	@SuppressWarnings("removal")
	@Override
	public void createCartFromOrder(final String orderCode)
	{

		final OrderModel order = customOrderService.getOrderForCode(orderCode);
		if (order == null || !order.getUser().equals(getUserService().getCurrentUser()))
		{
			throw new IllegalArgumentException("Order doesn't exist nor belong to this user.");
		}
		AddressModel originalDeliveryAddress = order.getDeliveryAddress();
		if (originalDeliveryAddress != null)
		{
			originalDeliveryAddress = originalDeliveryAddress.getOriginal();
		}

		AddressModel originalPaymentAddress = order.getPaymentAddress();
		if (originalPaymentAddress != null)
		{
			originalPaymentAddress = originalPaymentAddress.getOriginal();
		}

		final PaymentInfoModel paymentInfoModel = order.getPaymentInfo();

		// detach the order and null the attribute that is not available on the cart to avoid cloning errors.
		getModelService().detach(order);

		order.setOriginalVersion(null);
		order.setStatus(OrderStatus.CREATED);
		order.setPaymentAddress(null);
		order.setDeliveryAddress(null);
		order.setHistoryEntries(null);
		order.setPaymentInfo(null);
		order.setMpgsSuccessIndicator(null);
		order.setOrderCode(null);
		order.setPaymentReferenceId(null);
		order.setPaymentTransactions(null);
		order.setRequestPaymentBody(null);
		order.setResponsePaymentBody(null);
		order.setBillingTime(null);
		resetLoyaltyAttributes(order);
		order.setPaymentMode(null);
		order.setPaymentStatus(null);
		order.setPaymentOperationType(PaymentOperationType.NONE);
		order.setStoreCreditMode(null);
		order.setStoreCreditAmount(null);
		order.setStoreCreditAmountSelected(null);
		order.setMpgs3dsTransactionId(Strings.EMPTY);
		order.setPaymentTransactionCode(Strings.EMPTY);
		order.setPaymentRefundedTransactions(Collections.emptySet());
		order.setMpgsLastTransactionId(Strings.EMPTY);
		order.setMpgsTransactionIdCounter(0);
		order.setDeliveryMode(null);
		order.setCartOperationHistory(null);
		order.setLastOperationSource(null);
		order.setPaymentTransactionsInfos(null);

		// create cart from the order object.
		final CartModel cart = customCartService.createCartFromAbstractOrder(order);
		if (cart != null)
		{
			cart.setDeliveryAddress(originalDeliveryAddress);
			cart.setPaymentAddress(originalPaymentAddress);
			cart.setPaymentInfo(paymentInfoModel);
			getModelService().save(cart);
			getCartService().removeSessionCart();
			commerceCartService.calculateCart(cart);
			getModelService().refresh(cart);
			getCartService().setSessionCart(cart);
		}
	}

	/**
	 * @param order
	 */
	private void resetLoyaltyAttributes(final OrderModel order)
	{
		order.setUsableLoyaltyRedeemAmount(null);
		order.setUsableLoyaltyRedeemPoints(null);
		order.setLoyaltyExchangeRate(0);
		order.setLoyaltyAmount(0);
		order.setLoyaltyFailureReason(null);
		order.setLoyaltyAmountSelected(0);
		order.setLoyaltyPaymentMode(null);
		order.setLoyaltyPaymentType(null);
		order.setGiiftValidateId(null);
		order.setRedeemedLoyaltyPoints(0);
		order.setTotalPriceAfterRedeem(0);
		order.setExpectedAddedPoint(null);
		order.setExpectedLoyaltyRedeemAmount(null);
		order.setShouldCalculateLoyalty(true);
		order.setGiiftTransactionId(null);
		order.setLoyaltyPaymentStatus(null);

	}

	@Override

	public List<LoyaltyPaymentModeData> getLoyaltyPaymentModes()
	{
		return loyaltyPaymentModeFacade.getSupportedLoyaltyPaymentModesCurrentBaseStore();
	}


	@SuppressWarnings("removal")
	@Override
	public void setLoyaltyPaymentMode(final String mode, final double loyaltyPointSelected)
	{
		if (getCart() != null)
		{
			getCart().setLoyaltyPaymentMode(loyaltyPaymentModeService.getLoyaltyPaymentMode(mode).orElse(null));
			getCart().setLoyaltyAmountSelected(loyaltyPointSelected);
			getCart().setCalculated(Boolean.FALSE);
			getCart().setShouldCalculateLoyalty(true);
			getCommerceCheckoutService().calculateCart(getCart());
			getModelService().save(getCart());
		}
	}

	@Override
	public Optional<LoyaltyUsablePointData> getLoyaltyUsablePoints()
	{
		if (getCart() == null)
		{
			return Optional.empty();
		}

		try
		{
			return loyaltyPaymentFacade.getLoyaltyUsablePoints(getCart());
		}
		catch (final AACSLoyaltyException ex)
		{
			LOG.error(ex.getMessage());
		}

		return Optional.empty();

	}

	public PaymentSubscriptionResultData completePaymentTransactionForStorefront(final String resultIndicator,
			final String sessionVersion) throws PaymentException, InvalidCartException
	{
		this.getPaymentResponseData(new ReturnCallbackResponseBean(resultIndicator, sessionVersion));
		final Optional<PaymentResponseData> paymentOrderStatusResponseData = this.getPaymentOrderStatusResponseData(null);

		final Map<String, Object> orderInfoMap = paymentOrderStatusResponseData.isEmpty() ? MapUtils.EMPTY_MAP
				: paymentOrderStatusResponseData.get().getResponseData();
		if (!this.isSuccessfulPaidOrder(null))
		{
			LOG.warn("NOT_SUCCESSFUL_PAYMENT STOREFRONT : " + orderInfoMap);
			final Map<String, Object> response = ((Map<String, Object>) orderInfoMap.get("response"));
			return buildErrorPaymentSubscription(response);
		}
		final Optional<PaymentSubscriptionResultData> paymentSubscriptionResultData = this
				.completePaymentCreateSubscription(orderInfoMap, true);
		if (paymentSubscriptionResultData.isPresent() && paymentSubscriptionResultData.get().isSuccess()
				&& paymentSubscriptionResultData.get().getStoredCard() != null
				&& StringUtils.isNotBlank(paymentSubscriptionResultData.get().getStoredCard().getSubscriptionId()))
		{
			setCardPaymentDetails(paymentSubscriptionResultData.get());
			final OrderData orderData = this.placeOrder();
			paymentSubscriptionResultData.get().setOrderId(orderData.getCode());
		}
		else
		{
			LOG.error("Failed to create subscription. Please check the log files for more information");
		}
		return paymentSubscriptionResultData.orElse(null);
	}

	@Override
	public PaymentSubscriptionResultData completePaymentTransactionForOCC(final Object data)
			throws PaymentException, InvalidCartException
	{
		final Optional<PaymentResponseData> paymentOrderStatusResponseData = this.getPaymentOrderStatusResponseData(data);
		final Map<String, Object> responseMap = paymentOrderStatusResponseData.isEmpty() ? MapUtils.EMPTY_MAP
				: paymentOrderStatusResponseData.get().getResponseData();
		//		if (!this.isSuccessfulPaidOrder(null))
		//		{
		//			LOG.warn("NOT_SUCCESSFUL_PAYMENT OCC : " + responseMap);
		//			final Map<String, Object> response = ((Map<String, Object>) responseMap.get("response"));
		//			if (response != null && response.get(ACQUIRER_CODE) != null)
		//			{
		//				throw new PaymentException("Not successfull payment due to " + (String) response.get(ACQUIRER_CODE), null);
		//			}
		//			else
		//			{
		//				throw new PaymentException("Not successfull payment", null);
		//			}
		//		}
		final Optional<PaymentSubscriptionResultData> paymentSubscriptionResultData = this
				.completePaymentCreateSubscription(responseMap, true);
		if (paymentSubscriptionResultData.isPresent() && paymentSubscriptionResultData.get().isSuccess()
				&& paymentSubscriptionResultData.get().getStoredCard() != null
				&& StringUtils.isNotBlank(paymentSubscriptionResultData.get().getStoredCard().getSubscriptionId()))
		{
			final String orderCode = setCardPaymentDetails(paymentSubscriptionResultData.get());
			paymentSubscriptionResultData.get().setOrderId(orderCode);
		}
		else
		{
			LOG.error("Failed to create subscription. Please check the log files for more information");
		}
		return paymentSubscriptionResultData.orElse(null);
	}

	private String setCardPaymentDetails(final PaymentSubscriptionResultData paymentSubscriptionResultData)
			throws InvalidCartException
	{
		final CCPaymentInfoData newPaymentSubscription = paymentSubscriptionResultData.getStoredCard();

		if (getUserFacade().getCCPaymentInfos(true).size() <= 1)
		{
			getUserFacade().setDefaultPaymentInfo(newPaymentSubscription);
		}
		this.setPaymentDetails(newPaymentSubscription.getId());

		return (getCheckoutCustomerStrategy().isAnonymousCheckout() ? getCart().getGuid() : getCart().getCode());
	}

	private PaymentSubscriptionResultData buildErrorPaymentSubscription(final Map<String, Object> response)
	{
		final PaymentSubscriptionResultData result = new PaymentSubscriptionResultData();
		result.setSuccess(false);
		if (response != null && response.get(ACQUIRER_CODE) != null)
		{
			result.setResultCode((String) response.get(ACQUIRER_CODE));
			result.setDecision("1");
		}
		else
		{
			result.setResultCode("0000");
			result.setDecision("0");
		}
		return result;
	}

	/**
	 * @return the userFacade
	 */
	public CustomUserFacade getUserFacade()
	{
		return userFacade;
	}


	@Override
	public Optional<LoyaltyBalanceData> getLoyaltyBalance()
	{
		try
		{
			return loyaltyPaymentFacade.getBalanceByCurrentBaseStoreAndCustomer();

		}
		catch (final AACSLoyaltyException ex)
		{
			return Optional.empty();
		}

	}

	@Override
	public boolean isLoyaltyEnabled() throws AACSLoyaltyException
	{

		return loyaltyPaymentFacade.isEnabledForCustomerByCurrentBaseStoreAndCurrentCustomer();
	}

	protected Optional<String> generateOrderNumber(final AbstractOrderModel abstractOrder)
	{
		return getSerialNumberConfigurationContext().generateSerialNumberForBaseStore(abstractOrder.getStore(),
				SerialNumberSource.ABSTRACT_ORDER);
	}

	/**
	 * @return the serialNumberConfigurationContext
	 */
	public SerialNumberConfigurationContext getSerialNumberConfigurationContext()
	{
		return serialNumberConfigurationContext;
	}

	/**
	 * @param orderInfoMap
	 * @param cartModel
	 * @param b
	 * @return
	 */
	private Optional<PaymentSubscriptionResultData> completePaymentCreateSubscriptionUsingWebhook(
			final Map<String, Object> orderInfoMap, final AbstractOrderModel abstractOrderModel, final boolean saveInAccount)
	{
		Preconditions.checkArgument(abstractOrderModel != null, ABSTRACTORDERMODEL_MUSTN_T_BE_NULL);

		if (abstractOrderModel.getPaymentMode() == null)
		{
			return Optional.empty();
		}
		final CreateSubscriptionResult response = getPaymentContext().interpretResponse(abstractOrderModel).orElse(null);

		Preconditions.checkNotNull(response, CREATE_SUBSCRIPTION_RESULT_CANNOT_BE_NULL_MSG);
		Preconditions.checkNotNull(response.getDecision(), DECISION_CANNOT_BE_NULL_MSG);
		Preconditions.checkNotNull(response.getAuthReplyData(), AUTH_REPLY_DATA_CANNOT_BE_NULL_MSG);
		Preconditions.checkNotNull(response.getCustomerInfoData(), CUSTOMER_INFO_DATA_CANNOT_BE_NULL_MSG);
		Preconditions.checkNotNull(response.getOrderInfoData(), ORDER_INFO_DATA_CANNOT_BE_NULL_MSG);
		Preconditions.checkNotNull(response.getPaymentInfoData(), PAYMENT_INFO_DATA_CANNOT_BE_NULL_MSG);
		Preconditions.checkNotNull(response.getSignatureData(), SIGNATURE_DATA_CANNOT_BE_NULL_MSG);
		Preconditions.checkNotNull(response.getSubscriptionInfoData(), SUBSCRIPTION_INFO_DATA_CANNOT_BE_NULL_MSG);

		final PaymentSubscriptionResultItem paymentSubscriptionResult = new PaymentSubscriptionResultItem();
		paymentSubscriptionResult.setSuccess(DecisionsEnum.ACCEPT.name().equalsIgnoreCase(response.getDecision()));
		paymentSubscriptionResult.setDecision(String.valueOf(response.getDecision()));
		paymentSubscriptionResult.setResultCode(String.valueOf(response.getReasonCode()));



		if (DecisionsEnum.ACCEPT.name().equalsIgnoreCase(response.getDecision()))
		{
			final CustomerModel customerModel = (CustomerModel) abstractOrderModel.getUser();
			final PaymentTransactionEntryModel savePaymentTransactionEntry = getPaymentTransactionStrategy()
					.savePaymentTransactionEntry(abstractOrderModel, response.getRequestId(), response.getOrderInfoData());
			final CreditCardPaymentInfoModel cardPaymentInfoModel = getCreditCardPaymentInfoCreateStrategy().saveSubscription(
					customerModel, response.getCustomerInfoData(), response.getSubscriptionInfoData(), response.getPaymentInfoData(),
					saveInAccount);
			paymentSubscriptionResult.setStoredCard(cardPaymentInfoModel);

			// Check if the subscription has already been validated
			final CCPaySubValidationModel subscriptionValidation = getCreditCardPaymentSubscriptionDao()
					.findSubscriptionValidationBySubscription(cardPaymentInfoModel.getSubscriptionId());
			if (subscriptionValidation != null)
			{
				cardPaymentInfoModel.setSubscriptionValidated(true);
				getModelService().save(cardPaymentInfoModel);
				getModelService().remove(subscriptionValidation);
				getModelService().refresh(cardPaymentInfoModel);
			}

			if (savePaymentTransactionEntry != null && savePaymentTransactionEntry.getPaymentTransaction() != null)
			{
				final PaymentTransactionModel paymentTransaction = savePaymentTransactionEntry.getPaymentTransaction();
				paymentTransaction.setInfo(cardPaymentInfoModel);
				savePaymentTransactionEntry.setPaymentTransaction(paymentTransaction);
				addPaymentTransactionOnCart(abstractOrderModel, paymentTransaction);
				getModelService().save(abstractOrderModel);
			}
		}
		else
		{
			final String logData = String.format("Cannot create subscription. Decision: %s - Reason Code: %s",
					response.getDecision(), response.getReasonCode());
			LOG.error(logData);
		}

		return Optional.ofNullable(getPaymentSubscriptionResultDataConverter().convert(paymentSubscriptionResult));
	}

	/**
	 * @param cart
	 * @param paymentTransaction
	 */
	private void addPaymentTransactionOnCart(final AbstractOrderModel cart, final PaymentTransactionModel paymentTransaction)
	{
		final List<PaymentTransactionModel> paymentTransactions = cart.getPaymentTransactions();
		final List<PaymentTransactionModel> paymentTransactionsList = new LinkedList<>(paymentTransactions);
		paymentTransactionsList.add(paymentTransaction);
		cart.setPaymentTransactions(paymentTransactionsList);

	}

	@Override
	public OrderModel completeCardOrderModelUsingWebhook(final CartModel cartModel, final Map<String, Object> payload)
			throws InvalidCartException
	{
		if (cartModel == null)
		{
			throw new InvalidCartException("cart is empty!");
		}
		if (cartModel.getPaymentMode() == null || StringUtils.isBlank(cartModel.getPaymentMode().getCode()))
		{
			throw new InvalidCartException("PaymentMode on the cart[" + cartModel.getCode() + "] is empty!");
		}
		final String paymentModeCode = cartModel.getPaymentMode().getCode();
		if (!StringUtils.equalsAnyIgnoreCase(paymentModeCode, "mada", "card", "applepay", "apple"))
		{
			throw new InvalidCartException("PaymentMode [" + paymentModeCode + "] on the cart[" + cartModel.getCode()
					+ "] is none of these options [mada,card,applepay]");
		}


		final PaymentResponseStatus transactionStatus = getPaymentContext().getTransactionStatus(payload, cartModel);

		if (!PaymentResponseStatus.SUCCESS.equals(transactionStatus))
		{
			throw new InvalidCartException("Payment Transaction status is [" + transactionStatus + "] on the cart["
					+ cartModel.getCode() + "]. Please check the log files for more information");

		}
		final Optional<PaymentSubscriptionResultData> paymentSubscriptionResultData = completePaymentCreateSubscriptionUsingWebhook(
				payload, cartModel, true);

		if (paymentSubscriptionResultData.isPresent() && paymentSubscriptionResultData.get().isSuccess()
				&& paymentSubscriptionResultData.get().getStoredCard() != null
				&& StringUtils.isNotBlank(paymentSubscriptionResultData.get().getStoredCard().getSubscriptionId()))
		{
			final CCPaymentInfoData newPaymentSubscription = paymentSubscriptionResultData.get().getStoredCard();
			final CustomerModel customer = (CustomerModel) cartModel.getUser();

			if (getUserFacade().getCCPaymentInfos(customer, true).size() <= 1)
			{
				getUserFacade().setDefaultPaymentInfo(customer, newPaymentSubscription);
			}
			setPaymentDetails(cartModel, newPaymentSubscription.getId());
		}
		else
		{

			final String decision = paymentSubscriptionResultData.isPresent() ? paymentSubscriptionResultData.get().getDecision()
					: null;
			final String resultCode = paymentSubscriptionResultData.isPresent() ? paymentSubscriptionResultData.get().getResultCode()
					: null;

			getMerchantTransactionService().getMerchantTransactionById(cartModel.getOrderCode());

			LOG.error("Failed to create subscription : Decision[" + decision + "], ResultCode[" + resultCode
					+ "] ,Do not have valid payment Subscription Result Data ! .  Please check the log files for more information");

			throw new InvalidCartException("Failed to create subscription : Decision[" + decision + "], ResultCode[" + resultCode
					+ "] ,Do not have valid payment Subscription Result Data ! .  Please check the log files for more information");
		}

		return checkAndPlaceOrderModel(cartModel);
	}

	private OrderModel checkAndPlaceOrderModel(final CartModel cartModel) throws InvalidCartException
	{
		if (cartModel != null && (cartModel.getUser().equals(getUserForCheckout(cartModel)) || isAnonymousCheckout(cartModel)))
		{

			beforePlaceOrder(cartModel);
			final OrderModel orderModel = placeOrder(cartModel);
			afterPlaceOrder(cartModel, orderModel);
			if (orderModel != null)
			{
				return orderModel;
			}
		}
		return null;
	}

	private CustomerModel getUserForCheckout(final AbstractOrderModel abstractOrderModel)
	{
		if (!isAnonymousCheckout(abstractOrderModel))
		{
			final CustomerModel checkoutCustomer = (CustomerModel) getUserService()
					.getUserForUID(abstractOrderModel.getUser().getUid());

			Assert.state(!getUserService().isAnonymousUser(checkoutCustomer), "Checkout user must not be the anonymous user");

			return checkoutCustomer;
		}
		return (CustomerModel) getUserService().getCurrentUser();
	}

	protected boolean checkIfUserIsTheCartUser(final AbstractOrderModel abstractOrderModel)
	{
		return abstractOrderModel != null && abstractOrderModel.getUser().equals(getUserForCheckout(abstractOrderModel));
	}

	protected boolean setPaymentDetails(final CartModel cartModel, final String paymentInfoId)
	{
		validateParameterNotNullStandardMessage("paymentInfoId", paymentInfoId);
		validateParameterNotNullStandardMessage("cartModel", cartModel);


		if (checkIfUserIsTheCartUser(cartModel) && StringUtils.isNotBlank(paymentInfoId))
		{
			final CustomerModel currentUserForCheckout = (CustomerModel) cartModel.getUser();
			final CreditCardPaymentInfoModel ccPaymentInfoModel = getCustomerAccountService()
					.getCreditCardPaymentInfoForCode(currentUserForCheckout, paymentInfoId);
			if (ccPaymentInfoModel != null)
			{

				final CommerceCheckoutParameter parameter = createCommerceCheckoutParameter(cartModel, true);
				parameter.setPaymentInfo(ccPaymentInfoModel);
				return getCommerceCheckoutService().setPaymentInfo(parameter);
			}
			LOG.warn(String.format(
					"Did not find CreditCardPaymentInfoModel for user: %s, cart: %s &  paymentInfoId: %s. PaymentInfo Will not get set.",
					currentUserForCheckout, cartModel, paymentInfoId));
		}
		return false;
	}

	public boolean isAnonymousCheckout(final AbstractOrderModel abstractOrderModel)
	{
		return getUserService().isAnonymousUser(abstractOrderModel.getUser());
	}

	/**
	 * @return the merchantTransactionService
	 */
	public MerchantTransactionService getMerchantTransactionService()
	{
		return merchantTransactionService;
	}

	@Override
	public boolean isSuccessfulPaidOrderByOrder()
	{
		// XXX Auto-generated method stub
		return false;
	}

	@Override
	public Optional<PaymentResponseData> getOrderPaymentResponseData(final Object data)
	{
		final CartModel cart = getCart();
		if (cart.getPaymentMode() == null)
		{
			return Optional.empty();
		}
		return getPaymentContext().getResponseData(cart, data);
	}

	@Override
	public List<PaymentModeData> getSupportedPaymentModes(final boolean forOCC, final boolean forStoreFront,
			final SalesApplication deviceType)
	{
		final Optional<List<PaymentModeData>> supportedPaymentModes = this.getSupportedPaymentModes();
		if (supportedPaymentModes.isEmpty())
		{
			return Collections.emptyList();
		}
		checkPaymentModeDataForStorFront(supportedPaymentModes.get().get(0), forStoreFront);
		return supportedPaymentModes.get().stream()
				.filter(pm -> Boolean.TRUE.equals(pm.isActive()) && checkPaymentModeDataForStorFront(pm, forStoreFront)
						&& checkPaymentModeDataForOCC(pm, forOCC) && checkPaymentModeDataForSalesApplication(pm, deviceType))
				.collect(Collectors.toList());
	}

	/**
	 * @param pm
	 * @param deviceType
	 * @return
	 */
	private boolean checkPaymentModeDataForSalesApplication(final PaymentModeData pm, final SalesApplication deviceType)
	{
		if (deviceType == null)
		{
			return true;
		}
		return pm.getSupportedDevices().contains(deviceType);
	}

	/**
	 * @param paymentModeData
	 * @param forStoreFront
	 */
	private boolean checkPaymentModeDataForStorFront(final PaymentModeData paymentModeData, final boolean forStoreFront)
	{
		if (!forStoreFront)
		{
			return true;
		}
		if (paymentModeData == null)
		{
			return false;
		}
		return paymentModeData.isActive() && paymentModeData.isActiveForStorefront();
	}

	/**
	 * @param paymentModeData
	 * @param forOCC
	 */
	private boolean checkPaymentModeDataForOCC(final PaymentModeData paymentModeData, final boolean forOCC)
	{
		if (!forOCC)
		{
			return true;
		}
		if (paymentModeData == null)
		{
			return false;
		}
		return paymentModeData.isActive() && paymentModeData.isActiveForOCC();
	}

	@Override
	public boolean validateCartTimeSlot()
	{

		final CartModel cart = getCart();

		if (cart == null)
		{
			return false;
		}

		if (cart.getTimeSlotInfo() == null || isValidateCartTimeSlot(cart))
		{
			return true;
		}


		cart.setTimeSlotInfo(null);
		getModelService().save(getCart());
		return false;
	}

	private boolean isValidateCartTimeSlot(final CartModel cart)
	{

		try
		{
			final TimeSlotInfoModel timeslot = cart.getTimeSlotInfo();
			final Date currentDate = new Date();
			final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
			final Date timesSlotDate = dateFormatter.parse(timeslot.getDate());
			final String[] hoursAndMinutes = timeslot.getEnd().split(":");
			timesSlotDate.setHours(Integer.valueOf(hoursAndMinutes[0]));
			timesSlotDate.setMinutes(Integer.valueOf(hoursAndMinutes[1]));
			return currentDate.before(timesSlotDate);
		}
		catch (final ParseException e)
		{
			//TODO add log
			return false;

		}

	}
}
