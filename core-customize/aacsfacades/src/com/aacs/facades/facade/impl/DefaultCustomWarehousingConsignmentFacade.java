/**
 *
 */
package com.aacs.facades.facade.impl;


import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commerceservices.search.dao.impl.DefaultPagedGenericDao;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.internal.dao.SortParameters;
import de.hybris.platform.servicelayer.internal.dao.SortParameters.SortOrder;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.warehousingfacades.order.impl.DefaultWarehousingConsignmentFacade;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.aacs.aacsfulfillment.context.FulfillmentContext;
import com.aacs.aacsfulfillment.exception.FulfillmentException;
import com.aacs.core.dao.impl.DefaultConsignmentByCodePagedDao;
import com.aacs.facades.facade.CustomWarehousingConsignmentFacade;
import com.google.common.base.Preconditions;


/**
 * @author monzer
 *
 */
public class DefaultCustomWarehousingConsignmentFacade extends DefaultWarehousingConsignmentFacade
		implements CustomWarehousingConsignmentFacade
{

	@Resource(name = "fulfillmentContext")
	private FulfillmentContext fulfillmentContext;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "consignmentByCodePagedDao")
	private DefaultConsignmentByCodePagedDao consignmentByCodePagedDao;

	/**
	 * @return the orderByNullVersionIdWithCodePagedDao
	 */
	protected DefaultConsignmentByCodePagedDao getConsignmentByCodePagedDao()
	{
		return consignmentByCodePagedDao;
	}

	@Resource(name = "consignmentPagedGenericDao")
	private DefaultPagedGenericDao defaultPagedGenericDao;


	/**
	 * @return the defaultPagedGenericDao
	 */
	public DefaultPagedGenericDao getDefaultPagedGenericDao()
	{
		return defaultPagedGenericDao;
	}

	@Override
	public Optional<String> createShipment(final String consignmentCode) throws FulfillmentException
	{
		final ConsignmentModel consignmentModel = getAndValidateConsignmentForCode(consignmentCode);
		final Optional<String> trackingId = fulfillmentContext.createShipmentByCurrentStore(consignmentModel);
		return trackingId;
	}

	@Override
	public Optional<byte[]> printConsignmentAWB(final String consignmentCode) throws FulfillmentException
	{
		final ConsignmentModel consignmentModel = getAndValidateConsignmentForCode(consignmentCode);
		final Optional<byte[]> awb = fulfillmentContext.printAWBByCurrentStore(consignmentModel);
		return awb;
	}

	@Override
	public void updateConsignmentTrackingId(final String consignmentCode, final String trackingId) throws FulfillmentException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(trackingId), "Tracking id cannot be null");
		final ConsignmentModel consignmentModel = getAndValidateConsignmentForCode(consignmentCode);
		consignmentModel.setTrackingID(trackingId);
		modelService.save(consignmentModel);
	}

	private ConsignmentModel getAndValidateConsignmentForCode(final String consignmentCode) throws FulfillmentException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(consignmentCode), "Cannot proceed with an empty consignment code");
		final ConsignmentModel consignmentModel = getConsignmentModelForCode(consignmentCode);
		Preconditions.checkArgument(consignmentModel != null, "Cannot proceed without consignment model");
		return consignmentModel;
	}



	public SearchPageData<ConsignmentData> searchConsignmentByCode(final PageableData pageableData, final String code,
			final String sort)
	{
		if (!StringUtils.isBlank(code))
		{
			final Map<String, String> param = new HashMap<>();
			param.put("code", code);
			final SearchPageData<ConsignmentModel> consignmentSearchPageData = getConsignmentByCodePagedDao().findCustom(param,
					pageableData);

			return convertSearchPageData(consignmentSearchPageData, getConsignmentConverter());
		}
		else
		{
			return getConsignments(pageableData, sort);
		}

	}

	public SearchPageData<ConsignmentData> getConsignments(final PageableData pageableData, final String sort)
	{
		final SortParameters sortParameter = new SortParameters();
		switch (sort.toLowerCase())
		{
			case "asc":
				sortParameter.addSortParameter("code", SortOrder.ASCENDING);
				break;
			case "desc":
				sortParameter.addSortParameter("code", SortOrder.DESCENDING);
				break;

			default:
				sortParameter.addSortParameter("code", SortOrder.ASCENDING);
				break;
		}

		return convertSearchPageData(getDefaultPagedGenericDao().find(new HashMap<>(), sortParameter, pageableData),
				getConsignmentConverter());
	}

}
