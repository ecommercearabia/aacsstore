package com.aacs.facades.facade;

import de.hybris.platform.commercefacades.product.data.CategoryData;

import java.util.Optional;


/**
 * @author mnasro
 *
 */
public interface CategoryFacade
{
	public Optional<CategoryData> getCategoryForCode(final String code);
}
