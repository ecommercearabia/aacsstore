/**
 *
 */
package com.aacs.facades.facade;

import java.util.List;

import com.aacs.aacsfacades.authority.AuthorityGroupData;


/**
 * @author monzer
 *
 */
public interface WarehousingAndOMSAuthorityFacade
{

	AuthorityGroupData getAllAuthorityGroupsForCurrentUser();

	AuthorityGroupData getAllAuthorityGroupsForUser(String userUid);

	List<String> getAllViewsAvailabelForUser(String userUid);

}
