/**
 *
 */
package com.aacs.facades.facade;

import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.warehousingfacades.order.WarehousingConsignmentFacade;

import java.util.Optional;

import com.aacs.aacsfulfillment.exception.FulfillmentException;


/**
 * @author monzer
 *
 */
public interface CustomWarehousingConsignmentFacade extends WarehousingConsignmentFacade
{
	Optional<String> createShipment(String consignmentCode) throws FulfillmentException;

	Optional<byte[]> printConsignmentAWB(String consignmentCode) throws FulfillmentException;

	void updateConsignmentTrackingId(String consignmentCode, String trackingId) throws FulfillmentException;

	public SearchPageData<ConsignmentData> searchConsignmentByCode(final PageableData pageableData, final String code,
			final String sort);

	public SearchPageData<ConsignmentData> getConsignments(final PageableData pageableData, final String sort);

}
