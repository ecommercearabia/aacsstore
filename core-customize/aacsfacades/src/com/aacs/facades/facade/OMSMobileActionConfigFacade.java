/**
 *
 */
package com.aacs.facades.facade;

import com.aacs.aacsfacades.dto.config.OMSActionConfigData;

/**
 * @author monzer
 *
 */
public interface OMSMobileActionConfigFacade
{
	OMSActionConfigData getOMSActionConfigForOrder(String orderCode);
}
