/**
 *
 */
package com.aacs.facades.facade;

import com.aacs.aacsfacades.dto.config.WarehousingActionConfigData;


/**
 * @author monzer
 *
 */
public interface WarehousingMobileActionConfigFacade
{

	WarehousingActionConfigData getWarehousingActionsConfigForConsignment(String consignmentCode);

}
