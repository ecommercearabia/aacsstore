/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.facades.facade;

import com.aacs.aacstimeslotfacades.exception.TimeSlotException;


/**
 * The Interface CustomCheckoutFlowFacade.
 *
 * @author mnasro The Interface CheckoutFlowFacade.
 */
public interface CustomCheckoutFlowFacade
{

	/**
	 * Checks if there is no payment info.
	 *
	 * @return true if there is no payment info
	 */
	boolean hasNoPaymentInfo();

	/**
	 * Checks for no payment mode.
	 *
	 * @return true, if successful
	 */
	boolean hasNoPaymentMode();

	/**
	 * Checks for payment provider.
	 *
	 * @return true, if successful
	 */
	boolean hasPaymentProvider();

	boolean hasValidCart();

	/**
	 * Checks for no payment mode.
	 *
	 * @return true, if successful
	 */
	boolean hasNoTimeSlot() throws TimeSlotException;

	boolean isTimeSlotEnabledByCurrentSite() throws TimeSlotException;

	boolean shouldVerifyAddress();

	boolean isAddressVerified();
}
