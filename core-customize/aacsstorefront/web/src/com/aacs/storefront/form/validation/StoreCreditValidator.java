/**
 *
 */
package com.aacs.storefront.form.validation;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.aacs.aacsstorecredit.enums.StoreCreditModeType;
import com.aacs.storefront.form.StoreCreditForm;


/**
 * @author amjad.shati@erabia.com
 *
 */
@Component("storeCreditValidator")
public class StoreCreditValidator implements Validator
{

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return StoreCreditForm.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final StoreCreditForm form = (StoreCreditForm) object;
		final String sctCode = form.getSctCode();
		final String scAmount = form.getScAmount();
		if (StringUtils.isEmpty(sctCode))
		{
			errors.rejectValue("sctCode", "storeCredit.sctCode.invalid");
		}
		StoreCreditModeType storeCreditModeType = null;
		try
		{
			storeCreditModeType = StoreCreditModeType.valueOf(sctCode);
		}
		catch (final Exception e)
		{
			errors.rejectValue("sctCode", "storeCredit.sctCode.invalid");
		}

		if (StoreCreditModeType.REDEEM_SPECIFIC_AMOUNT.getCode().equals(sctCode))
		{
			if (StringUtils.isEmpty(scAmount))
			{
				errors.rejectValue("scAmount", "storeCredit.scAmount.invalid");
			}

			double scAmountValue = 0.0;
			try
			{
				scAmountValue = Double.parseDouble(scAmount);
			}
			catch (final Exception ex)
			{
				errors.rejectValue("scAmount", "storeCredit.scAmount.invalid");
			}
			if (scAmountValue < 0)
			{
				errors.rejectValue("scAmount", "storeCredit.scAmount.invalid");
			}
		}
	}

}
