package com.aacs.storefront.form.validation;

import org.springframework.stereotype.Component;

import com.aacs.storefront.form.OTPForm;


/**
 * The Class OTPValidator.
 *
 * @author mnasro
 */
@Component("otpRegisterValidator")
public class OTPRegisterValidator extends OTPValidator
{

	@Override
	public boolean supports(final Class<?> form)
	{
		return form.getClass().equals(OTPForm.class);
	}

}
