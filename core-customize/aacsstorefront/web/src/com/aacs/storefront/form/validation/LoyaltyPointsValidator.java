/**
 *
 */
package com.aacs.storefront.form.validation;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.aacs.aacsloyaltyprogramprovider.enums.LoyaltyPaymentModeType;
import com.aacs.storefront.form.LoyaltyPointsForm;


/**
 * @author amjad.shati@erabia.com
 *
 */
@Component("loyaltyPointsValidator")
public class LoyaltyPointsValidator implements Validator
{

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return LoyaltyPointsForm.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final LoyaltyPointsForm form = (LoyaltyPointsForm) object;
		final String loyaltyPaymentCode = form.getLoyaltyPaymentCode();
		final String loyaltyPaymentAmount = form.getLoyaltyPaymentAmount();
		if (StringUtils.isEmpty(loyaltyPaymentCode))
		{
			errors.rejectValue("loyaltyPaymentCode", "loyaltypoints.loyaltyPaymentCode.invalid");
		}
		LoyaltyPaymentModeType loyaltyPaymentModeType = null;
		try
		{
			loyaltyPaymentModeType = LoyaltyPaymentModeType.valueOf(loyaltyPaymentCode);
		}
		catch (final Exception e)
		{

			errors.rejectValue("loyaltyPaymentCode", "loyaltypoints.loyaltyPaymentCode.invalid");
		}

		if (LoyaltyPaymentModeType.REDEEM_SPECIFIC_AMOUNT.getCode().equals(loyaltyPaymentCode))
		{
			if (StringUtils.isEmpty(loyaltyPaymentAmount))
			{
				errors.rejectValue("loyaltyPaymentAmount", "loyaltypoints.loyaltyPaymentAmount.invalid");
			}

			double scAmountValue = 0.0;
			try
			{
				scAmountValue = Double.parseDouble(loyaltyPaymentAmount);
			}
			catch (final Exception ex)
			{
				errors.rejectValue("loyaltyPaymentAmount", "loyaltypoints.loyaltyPaymentAmount.invalid");
			}
			if (scAmountValue < 0)
			{
				errors.rejectValue("loyaltyPaymentAmount", "loyaltypoints.loyaltyPaymentAmount.invalid");
			}
		}
	}

}
