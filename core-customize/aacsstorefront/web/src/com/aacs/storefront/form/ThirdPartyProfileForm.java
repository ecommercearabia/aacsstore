package com.aacs.storefront.form;

/**
 * @author monzer
 *
 */
public class ThirdPartyProfileForm
{

	private String firstName;
	private String lastName;
	private String email;
	private String titleCode;
	private String mobileNumber;
	private String mobileCountry;
	private String nationality;
	private String nationalityId;
	private String birthDate;
	private boolean hasNationalId;
	private boolean involvedInLoyaltyProgram;
	private String maritalStatusCode;



	/**
	 * @return the involvedInLoyaltyProgram
	 */
	public boolean isInvolvedInLoyaltyProgram()
	{
		return involvedInLoyaltyProgram;
	}

	/**
	 * @param involvedInLoyaltyProgram
	 *           the involvedInLoyaltyProgram to set
	 */
	public void setInvolvedInLoyaltyProgram(final boolean involvedInLoyaltyProgram)
	{
		this.involvedInLoyaltyProgram = involvedInLoyaltyProgram;
	}

	/**
	 * @return the maritalStatusCode
	 */
	public String getMaritalStatusCode()
	{
		return maritalStatusCode;
	}

	/**
	 * @param maritalStatusCode
	 *           the maritalStatusCode to set
	 */
	public void setMaritalStatusCode(final String maritalStatusCode)
	{
		this.maritalStatusCode = maritalStatusCode;
	}

	/**
	 * @return the hasNationalId
	 */
	public boolean isHasNationalId()
	{
		return hasNationalId;
	}

	/**
	 * @param hasNationalId
	 *           the hasNationalId to set
	 */
	public void setHasNationalId(final boolean hasNationalId)
	{
		this.hasNationalId = hasNationalId;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @return the nationality
	 */
	public String getNationality()
	{
		return nationality;
	}

	/**
	 * @return the birthDate
	 */
	public String getBirthDate()
	{
		return birthDate;
	}

	/**
	 * @param birthDate
	 *           the birthDate to set
	 */
	public void setBirthDate(final String birthDate)
	{
		this.birthDate = birthDate;
	}

	/**
	 * @param nationality
	 *           the nationality to set
	 */
	public void setNationality(final String nationality)
	{
		this.nationality = nationality;
	}

	/**
	 * @param firstName
	 *           the firstName to set
	 */
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * @return the nationalityId
	 */
	public String getNationalityId()
	{
		return nationalityId;
	}

	/**
	 * @param nationalityId
	 *           the nationalityId to set
	 */
	public void setNationalityId(final String nationalityId)
	{
		this.nationalityId = nationalityId;
	}

	/**
	 * @param lastName
	 *           the lastName to set
	 */
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *           the email to set
	 */
	public void setEmail(final String email)
	{
		this.email = email;
	}

	/**
	 * @return the titleCode
	 */
	public String getTitleCode()
	{
		return titleCode;
	}

	/**
	 * @param titleCode
	 *           the titleCode to set
	 */
	public void setTitleCode(final String titleCode)
	{
		this.titleCode = titleCode;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber()
	{
		return mobileNumber;
	}

	/**
	 * @param mobileNumber
	 *           the mobileNumber to set
	 */
	public void setMobileNumber(final String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @param mobileCountry
	 *           the mobileCountry to set
	 */
	public void setMobileCountry(final String mobileCountry)
	{
		this.mobileCountry = mobileCountry;
	}

	/**
	 * @return the mobileCountry
	 */
	public String getMobileCountry()
	{
		return mobileCountry;
	}

}
