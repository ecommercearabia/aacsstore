/**
 *
 */
package com.aacs.storefront.form.validation;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.commercefacades.user.data.CustomerData;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.aacs.storefront.form.OTPForm;



/**
 * @author monzer
 *
 */
@Component("otpLoginValidator")
public class OTPLoginValidator extends OTPValidator
{

	/**
	 *
	 */
	private static final String EMAIL = "email";

	@Override
	public boolean supports(final Class<?> form)
	{
		return form.getClass().equals(OTPForm.class);
	}

	@Override
	public void validateSend(final Object object, final Errors errors)
	{
		final OTPForm otpForm = (OTPForm) object;
		validateEmail(otpForm, errors);
	}

	public void validateEmail(final OTPForm otpForm, final Errors errors)
	{
		if (StringUtils.isEmpty(otpForm.getEmail()))
		{
			errors.rejectValue(EMAIL, "otp.email.format.invalid");
			return;
		}
		final String email = otpForm.getEmail();

		if (StringUtils.length(email) > 255 || !validateEmailAddress(email))
		{
			errors.rejectValue(EMAIL, "otp.email.format.invalid");
			return;
		}
		final CustomerData user = getCustomerFacade().getUserForUID(email);
		if (user == null)
		{
			errors.rejectValue(EMAIL, "registration.error.account.exists.title");
			return;
		}
		otpForm.setMobileCountry(user.getMobileCountry().getIsocode());
		otpForm.setMobileNumber(user.getMobileNumber());

		validateStandardFields(otpForm, errors);
	}

	protected boolean validateEmailAddress(final String email)
	{
		final Matcher matcher = Pattern.compile(getConfigurationService().getConfiguration().getString(WebConstants.EMAIL_REGEX))
				.matcher(email);
		return matcher.matches();
	}


}
