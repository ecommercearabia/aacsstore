/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.storefront.controllers.cms;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.aacs.aacscomponents.model.BannerLinksComponentModel;
import com.aacs.aacscomponents.model.BannerLinksListComponentModel;
import com.aacs.storefront.controllers.ControllerConstants;


/**
 * Controller for home page
 */

@Controller("BannerLinksListComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.BannerLinksListComponent)
public class BannerLinksListComponentController extends AbstractAcceleratorCMSComponentController<BannerLinksListComponentModel>
{


	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final BannerLinksListComponentModel component)
	{
		if (!Boolean.TRUE.equals(component.getVisible()))
		{
			return;
		}

		final List<BannerLinksComponentModel> visiableBannerLinksComponent = component.getBannerLinksComponents().stream()
				.filter(c -> Boolean.TRUE.equals(c.getVisible())).collect(Collectors.toList());
		component.setBannerLinksComponents(visiableBannerLinksComponent);
		model.addAttribute("bannerLinksList", component);
	}




}
