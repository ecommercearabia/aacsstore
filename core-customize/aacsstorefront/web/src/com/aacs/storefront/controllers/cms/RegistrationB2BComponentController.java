/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.storefront.controllers.cms;

import de.hybris.platform.acceleratorfacades.device.ResponsiveMediaFacade;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.aacs.aacscomponents.model.RegistrationB2BComponentModel;
import com.aacs.storefront.controllers.ControllerConstants;


/**
 * Controller for home page
 */
@Controller("RegistrationB2BComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.RegistrationB2BComponent)
public class RegistrationB2BComponentController extends AbstractAcceleratorCMSComponentController<RegistrationB2BComponentModel>
{
	@Resource(name = "responsiveMediaFacade")
	private ResponsiveMediaFacade responsiveMediaFacade;
	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final RegistrationB2BComponentModel component)
	{
		if (Boolean.TRUE.equals(component.getVisible()))
		{
			model.addAttribute("title", component.getTitle());
			model.addAttribute("content", component.getContent());
			model.addAttribute("link", component.getLink());
		}
	}




}
