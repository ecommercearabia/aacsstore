/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.storefront.controllers.cms;

import de.hybris.platform.acceleratorfacades.device.ResponsiveMediaFacade;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.aacs.aacscomponents.model.QuickOrderComponentModel;
import com.aacs.storefront.controllers.ControllerConstants;


/**
 * Controller for home page
 */
@Controller("QuickOrderComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.QuickOrderComponent)
public class QuickOrderComponentController extends AbstractAcceleratorCMSComponentController<QuickOrderComponentModel>
{
	@Resource(name = "responsiveMediaFacade")
	private ResponsiveMediaFacade responsiveMediaFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final QuickOrderComponentModel component)
	{
		if (Boolean.TRUE.equals(component.getVisible()))
		{
			model.addAttribute("title", component.getTitle());
			model.addAttribute("backgroundImage", component.getBackgroundImage());
			model.addAttribute("content", component.getContent());
		}
	}




}
