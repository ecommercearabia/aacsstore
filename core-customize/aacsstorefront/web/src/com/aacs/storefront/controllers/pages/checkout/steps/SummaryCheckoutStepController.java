/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.storefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.acceleratorservices.enums.CheckoutPciOptionEnum;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateQuoteCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.PlaceOrderForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.aacs.aacspayment.entry.PaymentRequestData;
import com.aacs.aacspayment.exception.PaymentException;
import com.aacs.aacspayment.hyperpay.service.HyperpayCheckoutIdService;
import com.aacs.core.model.MerchantTransactionModel;
import com.aacs.core.service.CustomCartService;
import com.aacs.core.service.CustomOrderService;
import com.aacs.core.service.MerchantTransactionService;
import com.aacs.facades.facade.CustomCheckoutFlowFacade;
import com.aacs.storefront.checkout.steps.CheckoutStep;
import com.aacs.storefront.controllers.ControllerConstants;


@Controller
@RequestMapping(value = "/checkout/multi/summary")
public class SummaryCheckoutStepController extends AbstractCheckoutStepController
{

	private static final Logger LOG = LoggerFactory.getLogger(SummaryCheckoutStepController.class);

	private static final String FAILED_TO_CREATE_SUBSCRIPTION = "Failed to create subscription.  Please check the log files for more information";
	private static final String SUMMARY = "summary";
	private static final String CART = "/cart";
	private static final String REDIRECT_URL_ORDER_CONFIRMATION = REDIRECT_PREFIX + "/checkout/orderConfirmation/";
	private static final String REDIRECIT_TO_CHECKOUT_ORDER_PENDING = REDIRECT_PREFIX + "/checkout/orderPending/";
	private static final String FAILED_TO_PLACE_ORDER = "Failed to place Order";
	private static final String CHECKOUT_PLACE_ORDER_FAILED = "checkout.placeOrder.failed";


	@Resource(name = "customCheckoutFlowFacade")
	private CustomCheckoutFlowFacade customCheckoutFlowFacade;

	@Resource(name = "customCartService")
	private CustomCartService customCartService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "merchantTransactionService")
	private MerchantTransactionService merchantTransactionService;

	@Resource(name = "hyperpayCheckoutIdService")
	private HyperpayCheckoutIdService hyperpayCheckoutIdService;

	@Resource(name = "customOrderService")
	private CustomOrderService customOrderService;

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	@PreValidateQuoteCheckoutStep
	@PreValidateCheckoutStep(checkoutStep = SUMMARY)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException, // NOSONAR
			CommerceCartModificationException
	{
		if (validateCart(redirectAttributes))
		{
			// Invalid cart. Bounce back to the cart page.
			return REDIRECT_PREFIX + CART;
		}

		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		if (cartData.getEntries() != null && !cartData.getEntries().isEmpty())
		{
			for (final OrderEntryData entry : cartData.getEntries())
			{
				final String productCode = entry.getProduct().getCode();
				final ProductData product = getProductFacade().getProductForCodeAndOptions(productCode, Arrays.asList(
						ProductOption.BASIC, ProductOption.PRICE, ProductOption.VARIANT_MATRIX_BASE, ProductOption.PRICE_RANGE));
				entry.setProduct(product);
			}
		}

		if ("card".equalsIgnoreCase(getCheckoutFacade().getCheckoutCart().getPaymentMode().getCode()))
		{
			final Pair<Optional<PaymentRequestData>, Boolean> supportedPaymentData = getCheckoutFacade().getSupportedPaymentData();

			final CartModel sessionCart = getCustomCartService().getSessionCart();
			if (Boolean.TRUE.equals(supportedPaymentData.getRight()) && sessionCart != null)
			{
				try
				{
					return completeOrder(model, sessionCart.getMpgsSuccessIndicator(), Strings.EMPTY);
				}
				catch (final Exception e)
				{
					LOG.error(FAILED_TO_PLACE_ORDER, e);
				}
			}

			if (!supportedPaymentData.getLeft().isPresent())
			{
				return back(redirectAttributes);
			}
			model.addAttribute("supportedPaymentData", supportedPaymentData.getLeft().get());
		}

		model.addAttribute("cartData", cartData);
		model.addAttribute("allItems", cartData.getEntries());
		model.addAttribute("deliveryAddress", cartData.getDeliveryAddress());
		model.addAttribute("deliveryMode", cartData.getDeliveryMode());
		model.addAttribute("paymentInfo", cartData.getPaymentInfo());

		// Only request the security code if the SubscriptionPciOption is set to Default.
		final boolean requestSecurityCode = CheckoutPciOptionEnum.DEFAULT
				.equals(getCheckoutFlowFacade().getSubscriptionPciOption());
		model.addAttribute("requestSecurityCode", Boolean.valueOf(requestSecurityCode));

		model.addAttribute(new PlaceOrderForm());

		final ContentPageModel multiCheckoutSummaryPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, multiCheckoutSummaryPage);
		setUpMetaDataForContentPage(model, multiCheckoutSummaryPage);

		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.summary.breadcrumb"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		setCheckoutStepLinksForModel(model, getCheckoutStep());
		return ControllerConstants.Views.Pages.MultiStepCheckout.CheckoutSummaryPage;
	}


	@RequestMapping(value = "/placeOrder")
	@PreValidateQuoteCheckoutStep
	@RequireHardLogIn
	public String placeOrder(@ModelAttribute("placeOrderForm")
	final PlaceOrderForm placeOrderForm, final Model model, final HttpServletRequest request,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException, // NOSONAR
			InvalidCartException, CommerceCartModificationException
	{
		if (validateOrderForm(placeOrderForm, model))
		{
			return enterStep(model, redirectModel);
		}

		//Validate the cart
		if (validateCart(redirectModel))
		{
			// Invalid cart. Bounce back to the cart page.
			return REDIRECT_PREFIX + CART;
		}

		final OrderData orderData;
		try
		{
			orderData = getCheckoutFacade().placeOrder();
		}
		catch (final Exception e)
		{
			LOG.error(FAILED_TO_PLACE_ORDER, e);
			GlobalMessages.addErrorMessage(model, CHECKOUT_PLACE_ORDER_FAILED);
			return enterStep(model, redirectModel);
		}

		return redirectToOrderConfirmationPage(orderData);
	}



	private String getDecisionAndReasonCodeString(String decision, String reason)
	{
		final String formatString = "/?decision=%s&reasonCode=%s";
		decision = StringUtils.isBlank(decision) ? "0" : decision;
		reason = StringUtils.isBlank(decision) ? "0000" : reason;
		return String.format(formatString, decision, reason);
	}



	private Pair<MerchantTransactionModel, Boolean> trySaveMerchantTransaction(final String merchantTransactionId)
	{
		try
		{
			final MerchantTransactionModel merchantTrasnaction = getModelService().create(MerchantTransactionModel.class);
			final String orderCode = merchantTransactionId.split("_")[0];
			merchantTrasnaction.setId(orderCode);
			merchantTrasnaction.setSource("STOREFRONT");
			getModelService().save(merchantTrasnaction);
			return Pair.of(merchantTrasnaction, true);
		}
		catch (final ModelSavingException e)
		{
			LOG.info(e.toString());
			LOG.warn("MerchantTransactionModel already exist for {}", merchantTransactionId);
			return Pair.of(getMerchantTransactionService().getMerchantTransactionById(merchantTransactionId), false);
		}
	}

	private String completeOrder(final Model model, final String resultIndicator, final String sessionVersion)
			throws InvalidCartException
	{
		try
		{
			final PaymentSubscriptionResultData result = getCheckoutFacade().completePaymentTransactionForStorefront(resultIndicator,
					sessionVersion);

			if (result == null)
			{
				GlobalMessages.addErrorMessage(model, CHECKOUT_PLACE_ORDER_FAILED);
				return getCheckoutStep().currentStep();
			}
			if (!result.isSuccess())
			{
				return REDIRECT_URL_ERROR + "/?decision=" + result.getDecision() + "&reasonCode=" + result.getResultCode();
			}

			return REDIRECT_URL_ORDER_CONFIRMATION + result.getOrderId();
		}
		catch (final PaymentException e)
		{
			GlobalMessages.addErrorMessage(model, CHECKOUT_PLACE_ORDER_FAILED);
			return getCheckoutStep().currentStep();
		}
	}

	@RequestMapping(value = "/callback/mpgs/cancel/{orderCode}", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getMpgsCancelCallback(final Model model, final HttpServletRequest httpServletRequest, @PathVariable
	final String orderCode) throws CMSItemNotFoundException, InvalidCartException, CommerceCartModificationException
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/callback/mpgs/return/{orderCode}", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getMpgsReturnCallback(final Model model, final HttpServletRequest httpServletRequest, @PathVariable
	final String orderCode, @RequestParam(required = false)
	final String resultIndicator, @RequestParam(required = false)
	final String sessionVersion) throws InvalidCartException
	{
		final String merchantTransactionId = orderCode;
		//		return REDIRECIT_TO_CHECKOUT_ORDER_PENDING + merchantTransactionId;

		final Pair<MerchantTransactionModel, Boolean> merchantTransaction = trySaveMerchantTransaction(merchantTransactionId);
		final MerchantTransactionModel transaction = merchantTransaction.getLeft();
		if (!merchantTransaction.getRight())
		{
			// This code should run only if the WEBHOOK is handling the payment
			final OrderModel orderForCode = getCustomOrderService().getOrderForCode(merchantTransactionId);
			if (orderForCode != null)
			{
				return REDIRECT_URL_ORDER_CONFIRMATION + merchantTransactionId;
			}

			if (transaction != null && Strings.isNotBlank(transaction.getDecision())
					&& Strings.isNotBlank(transaction.getResultCode()))
			{
				getMerchantTransactionService().removeMerchantTransaction(transaction);
				// Redirect to error page
				return REDIRECT_URL_ERROR + getDecisionAndReasonCodeString(transaction.getDecision(), transaction.getResultCode());
			}

			return REDIRECIT_TO_CHECKOUT_ORDER_PENDING + merchantTransactionId;
		}


		// If error we should delete the merchant transaction id object so we can handle another request if there is one
		final List<CartModel> cartByMerchantTransactionId = getCustomCartService()
				.getCartByMerchantTransactionId(merchantTransactionId);

		if (CollectionUtils.isEmpty(cartByMerchantTransactionId))
		{
			LOG.error("Could not find cart with order code: {}", orderCode);
			return REDIRECT_URL_ERROR + getDecisionAndReasonCodeString(transaction.getDecision(), transaction.getResultCode());
		}

		final CartModel cartModel = cartByMerchantTransactionId.get(0);

		if (!resultIndicator.equals(cartModel.getMpgsSuccessIndicator()))
		{
			getMerchantTransactionService().removeMerchantTransaction(transaction);
			LOG.warn("Success Indicators did not match");
			return getCheckoutStep().previousStep();
		}


		cartModel.setOrderPlacedByChannel("STOREFRONT");
		getModelService().save(cartModel);
		return completeOrder(model, resultIndicator, sessionVersion);
	}



	protected String completeOrderForOrderCode(final Map<String, Object> orderInfoMap, final String orderCode)
			throws InvalidCartException
	{
		final PaymentSubscriptionResultData paymentSubscriptionResultData = getCheckoutFacade()
				.completePaymentCreateSubscription(orderInfoMap, true).orElse(null);


		if (paymentSubscriptionResultData == null)
		{
			LOG.error(FAILED_TO_CREATE_SUBSCRIPTION);
			return REDIRECT_URL_ERROR + getDecisionAndReasonCodeString(null, null);
		}

		if (!paymentSubscriptionResultData.isSuccess() || paymentSubscriptionResultData.getStoredCard() == null
				|| !StringUtils.isNotBlank(paymentSubscriptionResultData.getStoredCard().getSubscriptionId()))
		{
			LOG.error(FAILED_TO_CREATE_SUBSCRIPTION);
			return REDIRECT_URL_ERROR + getDecisionAndReasonCodeString(paymentSubscriptionResultData.getDecision(),
					paymentSubscriptionResultData.getResultCode());
		}

		final CCPaymentInfoData newPaymentSubscription = paymentSubscriptionResultData.getStoredCard();

		if (getUserFacade().getCCPaymentInfos(true).size() <= 1)
		{
			getUserFacade().setDefaultPaymentInfo(newPaymentSubscription);
		}
		getCheckoutFacade().setPaymentDetails(newPaymentSubscription.getId());

		final OrderData orderData = getCheckoutFacade().placeOrder();

		return orderData == null ? REDIRECIT_TO_CHECKOUT_ORDER_PENDING + orderCode : redirectToOrderConfirmationPage(orderData);
	}

	protected String completeOrder(final Map<String, Object> orderInfoMap) throws InvalidCartException
	{
		final PaymentSubscriptionResultData paymentSubscriptionResultData = getCheckoutFacade()
				.completePaymentCreateSubscription(orderInfoMap, true).orElseGet(null);


		if (paymentSubscriptionResultData == null)
		{
			LOG.error(FAILED_TO_CREATE_SUBSCRIPTION);
			return REDIRECT_URL_ERROR + getDecisionAndReasonCodeString(null, null);
		}


		if (!paymentSubscriptionResultData.isSuccess() || paymentSubscriptionResultData.getStoredCard() == null
				|| !StringUtils.isNotBlank(paymentSubscriptionResultData.getStoredCard().getSubscriptionId()))
		{
			LOG.error(FAILED_TO_CREATE_SUBSCRIPTION);
			return REDIRECT_URL_ERROR + getDecisionAndReasonCodeString(paymentSubscriptionResultData.getDecision(),
					paymentSubscriptionResultData.getResultCode());
		}

		final CCPaymentInfoData newPaymentSubscription = paymentSubscriptionResultData.getStoredCard();

		if (getUserFacade().getCCPaymentInfos(true).size() <= 1)
		{
			getUserFacade().setDefaultPaymentInfo(newPaymentSubscription);
		}
		getCheckoutFacade().setPaymentDetails(newPaymentSubscription.getId());

		final OrderData orderData = getCheckoutFacade().placeOrder();
		return redirectToOrderConfirmationPage(orderData);

	}

	/**
	 * Validates the order form before to filter out invalid order states
	 *
	 * @param placeOrderForm
	 *           The spring form of the order being submitted
	 * @param model
	 *           A spring Model
	 * @return True if the order form is invalid and false if everything is valid.
	 */
	protected boolean validateOrderForm(final PlaceOrderForm placeOrderForm, final Model model)
	{
		final String securityCode = placeOrderForm.getSecurityCode();
		boolean invalid = false;

		if (getCheckoutFlowFacade().hasNoDeliveryAddress())
		{
			GlobalMessages.addErrorMessage(model, "checkout.deliveryAddress.notSelected");
			invalid = true;
		}

		if (getCheckoutFlowFacade().hasNoDeliveryMode())
		{
			GlobalMessages.addErrorMessage(model, "checkout.deliveryMethod.notSelected");
			invalid = true;
		}

		if (getCustomCheckoutFlowFacade().hasNoPaymentInfo())
		{
			GlobalMessages.addErrorMessage(model, "checkout.paymentMethod.notSelected");
			invalid = true;
		}
		else
		{
			// Only require the Security Code to be entered on the summary page if the SubscriptionPciOption is set to Default.
			if (CheckoutPciOptionEnum.DEFAULT.equals(getCheckoutFlowFacade().getSubscriptionPciOption())
					&& StringUtils.isBlank(securityCode))
			{
				GlobalMessages.addErrorMessage(model, "checkout.paymentMethod.noSecurityCode");
				invalid = true;
			}
		}

		if (!placeOrderForm.isTermsCheck())
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.terms.not.accepted");
			invalid = true;
			return invalid;
		}
		final CartData cartData = getCheckoutFacade().getCheckoutCart();

		if (!getCheckoutFacade().containsTaxValues())
		{
			final String format = String.format(
					"Cart %s does not have any tax values, which means the tax cacluation was not properly done, placement of order can't continue",
					cartData.getCode());
			LOG.error(format);
			GlobalMessages.addErrorMessage(model, "checkout.error.tax.missing");
			invalid = true;
		}

		if (!cartData.isCalculated())
		{
			final String format = String.format("Cart %s has a calculated flag of FALSE, placement of order can't continue",
					cartData.getCode());
			LOG.error(format);
			GlobalMessages.addErrorMessage(model, "checkout.error.cart.notcalculated");
			invalid = true;
		}

		return invalid;
	}

	@GetMapping(value = "/back")
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@GetMapping(value = "/next")
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(SUMMARY);
	}


	@Override
	public CustomCheckoutFlowFacade getCustomCheckoutFlowFacade()
	{
		return customCheckoutFlowFacade;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the merchantTransactionService
	 */
	public MerchantTransactionService getMerchantTransactionService()
	{
		return merchantTransactionService;
	}


	/**
	 * @return the hyperpayCheckoutIdService
	 */
	public HyperpayCheckoutIdService getHyperpayCheckoutIdService()
	{
		return hyperpayCheckoutIdService;
	}


	/**
	 * @return the customOrderService
	 */
	public CustomOrderService getCustomOrderService()
	{
		return customOrderService;
	}


	/**
	 * @return the customCartService
	 */
	public CustomCartService getCustomCartService()
	{
		return customCartService;
	}

}
