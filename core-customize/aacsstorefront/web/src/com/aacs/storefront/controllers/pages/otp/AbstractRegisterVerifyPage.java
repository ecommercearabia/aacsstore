/**
 *
 */
package com.aacs.storefront.controllers.pages.otp;

import de.hybris.platform.commercefacades.user.data.RegisterData;

import javax.annotation.Resource;

import com.aacs.aacsotp.model.OTPVerificationTokenModel;
import com.aacs.storefront.form.OTPForm;
import com.aacs.storefront.form.validation.OTPRegisterValidator;
import com.aacs.storefront.form.validation.OTPValidator;


/**
 * @author mnasro
 *
 */
public abstract class AbstractRegisterVerifyPage extends AbstractOTPVerifyPage
{


	/** The otp validator. */
	@Resource(name = "otpRegisterValidator")
	private OTPRegisterValidator otpRegisterValidator;


	/**
	 * @return the otpValidator
	 */
	@Override
	public OTPValidator getOTPValidator()
	{
		return otpRegisterValidator;
	}


	@Override
	protected String getPageLabelOrId()
	{
		return "/verify";
	}

	@Override
	public boolean isEmailCheck()
	{
		return false;
	}

	@Override
	protected Object updateTokenData(final Object data, final OTPForm otpForm)
	{
		return data;
	}

	@Override
	protected OTPForm updateOTPForm(final OTPForm otpForm, final OTPVerificationTokenModel otpVerificationToken)
	{
		if (otpForm == null || otpVerificationToken == null || otpVerificationToken.getData() == null
				|| !(otpVerificationToken.getData() instanceof RegisterData))
		{
			return otpForm;
		}

		final RegisterData otpData = (RegisterData) otpVerificationToken.getData();
		otpForm.setEmail(otpData.getLogin());
		otpForm.setMobileCountry(otpData.getMobileCountry());
		otpForm.setMobileNumber(otpData.getMobileNumber());
		return otpForm;
	}

}
