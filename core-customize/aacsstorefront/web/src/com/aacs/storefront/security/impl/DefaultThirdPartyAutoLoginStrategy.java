/**
 *
 */
package com.aacs.storefront.security.impl;

import de.hybris.platform.acceleratorstorefrontcommons.security.GUIDCookieStrategy;
import de.hybris.platform.commercefacades.customer.CustomerFacade;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.RememberMeServices;

import com.aacs.storefront.security.ThirdPartyAutoLoginStrategy;
import com.aacs.storefront.security.authentication.token.ThirdPartyAuthenticationToken;



/**
 * @author monzer
 *
 */
public class DefaultThirdPartyAutoLoginStrategy implements ThirdPartyAutoLoginStrategy
{

	private static final Logger LOG = Logger.getLogger(DefaultThirdPartyAutoLoginStrategy.class);

	private CustomerFacade customerFacade;
	private GUIDCookieStrategy guidCookieStrategy;
	private RememberMeServices rememberMeServices;

	@Resource(name = "authenticationManager")
	private AuthenticationManager authManager;

	@Override
	public boolean login(final String username, final Object token, final String provider, final HttpServletRequest request,
			final HttpServletResponse response)
	{
		final String callbackUrl = request.getScheme() + request.getServerName();
		final Authentication authenticationToken = new ThirdPartyAuthenticationToken(username, token, provider, callbackUrl);
		try
		{
			final Authentication authentication = getAuthenticationManager().authenticate(authenticationToken);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			getCustomerFacade().loginSuccess();
			getGuidCookieStrategy().setCookie(request, response);
			getRememberMeServices().loginSuccess(request, response, authenticationToken);
			return true;
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage());
			SecurityContextHolder.getContext().setAuthentication(null);
			return false;
		}
	}



	protected AuthenticationManager getAuthenticationManager()
	{
		return authManager;
	}

	public void setAuthenticationManager(final AuthenticationManager authenticationManager)
	{
		this.authManager = authenticationManager;
	}

	protected CustomerFacade getCustomerFacade()
	{
		return customerFacade;
	}

	@Required
	public void setCustomerFacade(final CustomerFacade customerFacade)
	{
		this.customerFacade = customerFacade;
	}

	protected GUIDCookieStrategy getGuidCookieStrategy()
	{
		return guidCookieStrategy;
	}

	@Required
	public void setGuidCookieStrategy(final GUIDCookieStrategy guidCookieStrategy)
	{
		this.guidCookieStrategy = guidCookieStrategy;
	}

	protected RememberMeServices getRememberMeServices()
	{
		return rememberMeServices;
	}

	@Required
	public void setRememberMeServices(final RememberMeServices rememberMeServices)
	{
		this.rememberMeServices = rememberMeServices;
	}

}
