/**
 *
 */
package com.aacs.storefront.security.authentication.token;

import de.hybris.platform.commercefacades.user.data.OTPData;

import org.springframework.security.authentication.AbstractAuthenticationToken;


/**
 * @author monzer
 *
 */
public class OTPAuthenticationToken extends AbstractAuthenticationToken
{

	Object otpLoginForm;

	/**
	 * @param arg0
	 */
	public OTPAuthenticationToken(final Object otpLoginForm)
	{
		super(null);
		this.otpLoginForm = otpLoginForm;
		setAuthenticated(false);
	}

	@Override
	public Object getCredentials()
	{
		final OTPData form = (OTPData) otpLoginForm;
		return form.getOtpCode();
	}

	@Override
	public Object getPrincipal()
	{
		final OTPData form = (OTPData) otpLoginForm;
		return form.getUid();
	}

	@Override
	public Object getDetails()
	{
		final OTPData form = (OTPData) otpLoginForm;
		return form.getMobileNumber();
	}

}
