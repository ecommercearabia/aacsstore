/**
 *
 */
package com.aacs.storefront.filters.exception;

import org.springframework.security.core.AuthenticationException;


/**
 * @author monzer
 *
 */
public class ReCaptchaFailedException extends AuthenticationException
{
	public ReCaptchaFailedException(final String message)
	{
		super(message);
	}
}
