/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.storefront.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.aacs.aacstimeslotfacades.exception.TimeSlotException;
import com.aacs.facades.facade.CustomCheckoutFlowFacade;
import com.aacs.storefront.checkout.steps.validation.AbstractCheckoutStepValidator;
import com.aacs.storefront.checkout.steps.validation.ValidationResults;


public class ResponsivePaymentCheckoutStepValidator extends AbstractCheckoutStepValidator
{
	private static final Logger LOGGER = Logger.getLogger(ResponsivePaymentCheckoutStepValidator.class);

	@Resource(name = "customCheckoutFlowFacade")
	private CustomCheckoutFlowFacade customCheckoutFlowFacade;

	@Override
	public CustomCheckoutFlowFacade getCustomCheckoutFlowFacade()
	{
		return customCheckoutFlowFacade;
	}

	@Override
	public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
	{
		if (!getCustomCheckoutFlowFacade().hasValidCart())
		{
			return ValidationResults.REDIRECT_TO_CART;
		}

		if (getCheckoutFlowFacade().hasNoDeliveryAddress())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.deliveryAddress.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}

		if (getCustomCheckoutFlowFacade().shouldVerifyAddress() && !getCustomCheckoutFlowFacade().isAddressVerified())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.multi.deliveryAddress.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}

		if (getCheckoutFlowFacade().hasNoDeliveryMode())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.deliveryMethod.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_METHOD;
		}

		try
		{
			if (getCustomCheckoutFlowFacade().isTimeSlotEnabledByCurrentSite() && getCustomCheckoutFlowFacade().hasNoTimeSlot())
			{
				return ValidationResults.REDIRECT_TO_TIME_SLOT;
			}
		}
		catch (final TimeSlotException e)
		{
			switch (e.getTimeSlotExceptionType())
			{
				case NO_DELIVERY_AREA_SELECTED:
				case NO_DELIVERY_METHOD_SELECTED:
				case NO_TIMESLOT_CONFIGURATIONS_AVAILABLE:
					return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
				case INVALID_CHOSEN_TIMESLOT:
					return ValidationResults.REDIRECT_TO_TIME_SLOT;
			}
		}

		return ValidationResults.SUCCESS;
	}
}
