<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<product:addToCartTitle/>

<c:choose>
	<c:when test="${not empty suggestions and component.maximumNumberProducts > 0}">


<div class="carousel__component">
			<div class="carousel__component--headline">${fn:escapeXml(component.title)}</div>

			
					<div class="carousel__component--carousel js-owl-carousel owl-carousel js-owl-default owl-theme">
						<c:forEach end="${component.maximumNumberProducts}" items="${suggestions}" var="product">

							<c:url value="${product.url}" var="productUrl"/>












						<div class="carousel__item  ${product.code}"  >
							
				
			
								
									<div class="carousel__item--thumb">
										<c:if test="${not empty product.discount.percentage}">
	<div class="promo_label hidden">-<fmt:formatNumber type="NUMBER" value="${product.discount.percentage}" maxFractionDigits="0"/>%</div>
	</c:if>
									
									
									 <c:url value="javascript:;" var="link"></c:url>
									 <c:set value="" var="PopupWishlist"></c:set>
<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
<c:set value="js-popup-wishlist" var="PopupWishlist"></c:set>
<c:url value="javascript:;" var="link"></c:url>
</sec:authorize>
<c:set value="" var="isout"></c:set>
<c:set value="hidden" var="isin"></c:set>
	<c:if test="${product.inWishlist}">
	<c:set value="" var="isin"></c:set>
	<c:set value="hidden" var="isout"></c:set>
	</c:if>
	<c:if test="${!product.inWishlist}">
	<c:set value="hidden" var="isin"></c:set>
	<c:set value="" var="isout"></c:set>
	</c:if>
	

        <span class="wishlist_icon">
	<a href="${link}" title="wishlist" class="removeWishlistEntry ${PopupWishlist} wishlistbtn  ${isin} " data-productcode="${product.code}" data-pk="8796093055677"><i class="fas fa-heart"></i></a>
	<a href="${link}" title="wishlist" class="addWishlistEntry ${PopupWishlist} wishlistbtn  ${isout}" data-productcode="${product.code}" data-pk="8796093055677"><i class="fas fa-heart"></i></a>
				
				</span>
									<a href="${productUrl}">
									<c:if test="${not empty product.productLabel}"><div class="carousel__item--label">${product.productLabel}</div></c:if>
										<a href="${productUrl}"><product:productPrimaryImage product="${product}" format="zoom"/></a>
										
										
										</a>
									</div>
									
									<div class="cont_detail_carousel">
<%-- 									<a href="${productUrl}"> --%>
									<div class="cont_href_div">
									<div class="carousel__item--name">
									<a href="${productUrl}">
									<c:choose>
						<c:when test="${fn:length(product.name) > 44}">
							<c:out value="${fn:substring(product.name, 0, 44)}..."/>
						</c:when>
						<c:otherwise>
						${fn:escapeXml(product.name)}
						</c:otherwise>
						</c:choose>
									</a>
									</div>
									
									
									<div class="carousel__item--countryoforigin">
									<c:if test="${not empty product.countryOfOrigin}">
									${product.countryOfOrigin}
									<c:set value="${fn:replace(product.countryOfOriginIsocode, ' ', '-')}" var="countryOfOrigin"></c:set>
									<div class="carousel__item--countryoforiginisocode hidden"><i class="flagicon ${fn:toLowerCase(countryOfOrigin)}"></i>${product.countryOfOrigin} / </div>
									</c:if></div>
									</div></a>
									<div class="price_counter">
									<span class="stockLevel">
									<c:if test="${product.stock.stockLevelStatus.code eq 'outOfStock'}"><span class="OutOfStock"><i class="fas fa-times-circle"></i><spring:theme code='product.variants.out.of.stock'/></span></c:if>
									</span>
									<div class="carousel__item--price price">
									<c:choose>
						<c:when test="${not empty product.discount}">
							<span class="scratched"><format:fromPrice priceData="${product.discount.price}"/><span class="line_dis"></span></span>
							<span class="price"><format:fromPrice priceData="${product.discount.discountPrice}"/></span>
						
						</c:when>
						<c:otherwise>
						
						<p class="price"><format:fromPrice priceData="${product.price}"/></p>
							<span class="scratched"></span>	
						</c:otherwise>
					</c:choose>
									</div>
										
										<product:addtocartcarousel showQuantityBox="true" product="${product}" />
										</div>
										</div>
<!-- 								</a> -->
						
						
							</div>


							
						</c:forEach>
					</div>
		</div>




<!-- 		<div class="carousel__component"> -->
<%-- 			<div class="carousel__component--headline">${fn:escapeXml(component.title)}</div> --%>
<!-- 			<div class="carousel__component--carousel js-owl-carousel owl-carousel js-owl-default owl-theme">				 -->
<%-- 				<c:forEach end="${component.maximumNumberProducts}" items="${suggestions}" var="suggestion"> --%>
<%-- 					<c:url value="${suggestion.url}" var="productQuickViewUrl"/> --%>
<%-- 					<div class="carousel__item  ${product.code}"> --%>
<%-- 						<a href="${fn:escapeXml(productQuickViewUrl)}"> --%>
<!--                             <div class="carousel__item--thumb"> -->
<%--                                 <product:productPrimaryReferenceImage product="${suggestion}" format="product"/> --%>
<!--                             </div> -->
<%-- 							<c:if test="${component.displayProductTitles}"> --%>
<%-- 								<div class="carousel__item--name">${fn:escapeXml(suggestion.name)}</div> --%>
<%-- 							</c:if> --%>
<%-- 							<c:if test="${component.displayProductPrices}"> --%>
<%-- 								<div class="carousel__item--price"><format:fromPrice priceData="${suggestion.price}"/></div> --%>
<%-- 							</c:if> --%>
<!-- 						</a> -->
<!-- 					</div> -->
<%-- 				</c:forEach> --%>
<!-- 			</div> -->
<!-- 		</div> -->
	</c:when>
	<c:otherwise>
		<component:emptyComponent/>
	</c:otherwise>
</c:choose>
