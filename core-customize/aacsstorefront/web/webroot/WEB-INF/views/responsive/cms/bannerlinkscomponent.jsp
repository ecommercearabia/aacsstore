<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>


<c:url value="${urlLink}" var="simpleBannerUrl" />
<div class="box">
<div class="row">

<div class=" col-xs-12 box_con">
<div class="box_links">
	<h6 class="titleBox">${title}</h6>
	<ul>
	<c:forEach items="${links }" var="tempLink">
		<li><cms:component component="${tempLink}"  /></li>
	</c:forEach>
	</ul>
	
	<cms:component component="${link}"  />
</div>


<div class="banner__component simple-banner box_img text-center">
	<c:if test="${ycommerce:validateUrlScheme(media.url)}">
		<c:choose>
			<c:when test="${empty simpleBannerUrl || simpleBannerUrl eq '#' || !ycommerce:validateUrlScheme(simpleBannerUrl)}">
				<img title="${fn:escapeXml(media.altText)}" alt="${fn:escapeXml(media.altText)}"
					src="${fn:escapeXml(media.url)}">
			</c:when>
			<c:otherwise>
				<a href="${fn:escapeXml(simpleBannerUrl)}"><img title="${fn:escapeXml(media.altText)}"
					alt="${fn:escapeXml(media.altText)}" src="${fn:escapeXml(media.url)}"></a>
			</c:otherwise>
		</c:choose>
	</c:if>
</div>
</div>


</div>
</div>