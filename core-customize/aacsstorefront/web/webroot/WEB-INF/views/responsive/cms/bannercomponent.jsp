<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:url value="${component.urlLink}" var="bannerUrl" />
<c:url value="${component.external}" var="external" />

<div class="banner__component simple-banner mega-hover">

	<c:if test="${ycommerce:validateUrlScheme(component.media.url)}">
		<c:choose>
			<c:when test="${empty bannerUrl || bannerUrl eq '#' || !ycommerce:validateUrlScheme(bannerUrl)}">
				<img title="${fn:escapeXml(component.media.altText)}" alt="${fn:escapeXml(component.media.altText)}"
					src="${fn:escapeXml(component.media.url)}">
			</c:when>
			<c:otherwise>
				<a href="${fn:escapeXml(bannerUrl)}"><img title="${fn:escapeXml(component.media.altText)}"
					alt="${fn:escapeXml(component.media.altText)}" src="${fn:escapeXml(component.media.url)}"></a>
			</c:otherwise>
		</c:choose>
	</c:if>
</div>
<c:if test="${not empty component.content}">
<h3>${component.content}</h3>
</c:if>