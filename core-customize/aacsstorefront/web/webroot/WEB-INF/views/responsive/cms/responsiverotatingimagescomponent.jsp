<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/responsive/component"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="responsiveRotatingImagesComponent"
	tagdir="/WEB-INF/tags/responsive/component/responsiveRotatingImagesComponent"%>

<c:if test="${component.visible && not empty banners}">
	<c:choose>
		<c:when test="${component.theme eq 'BANNERS'}">
			<responsiveRotatingImagesComponent:banners
				banners="${banners}" />
		</c:when>
		<c:when test="${component.theme eq 'TOP_BANNER'}">
			<responsiveRotatingImagesComponent:topBanner 
				banners="${banners}" />
		</c:when>
	</c:choose>
</c:if>




