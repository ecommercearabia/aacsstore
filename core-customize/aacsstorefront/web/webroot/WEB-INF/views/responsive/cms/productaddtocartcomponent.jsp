<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:set var="isForceInStock"
       value="${product.stock.stockLevelStatus.code eq 'inStock' and empty product.stock.stockLevel}" />
<c:choose>
    <c:when test="${isForceInStock}">
        <c:set var="maxQty" value="FORCE_IN_STOCK" />
    </c:when>
    <c:otherwise>
        <c:set var="maxQty" value="${product.stock.stockLevel}" />
    </c:otherwise>
</c:choose>

<c:set value="" var="disableMaxQty"></c:set>
<c:set value="" var="disableMaxQtyInput"></c:set>
<c:if test="${!isForceInStock}" >
	<c:if test="${product.selectedQuantity eq maxQty}">
	
	    <c:set value="disabled" var="disableMaxQty"></c:set>
	    <c:set value="readonly" var="disableMaxQtyInput"></c:set>
	</c:if>
</c:if>

<c:set var="qtyMinus"
       value="${not empty product.unitFactorRangeData ? product.unitFactorRangeData[0].quantity : '1' }" />

<c:set value="" var="unitFactorRangeData"></c:set><c:set value="hidden" var="QTY"></c:set>
<c:if test="${not empty product.unitFactorRangeData}">
    <c:set value="" var="QTY"></c:set>
    <c:set value="hidden" var="unitFactorRangeData"></c:set> </c:if>
<div class="addtocart-component ">
    <c:if test="${empty showAddToCart ? true : showAddToCart}">
		<span class="${QTY} selectUnit">
		<c:if test="${not empty product.unitFactorRangeData}">
		<select id="SelectproductQtyItem">
			<c:forEach items="${product.unitFactorRangeData}" var="item">
			
			<c:choose>
				<c:when test="${not empty item.discountUnitPrice}">
				<option class="itemSelectQty" data-value="${item.formatedValue}" data-Qty="${item.quantity}"
                         data-stock="${item.inStock}" data-price='<format:fromPrice	priceData="${item.discountUnitPrice.price}"/>'
                         data-priceAfterSaving='<format:fromPrice	priceData="${item.priceAfterSaving}"/>'
                         data-discountedPrice='<format:fromPrice	priceData="${item.discountUnitPrice.discountPrice}"/>'
                         data-saving='<format:fromPrice	priceData="${item.discountUnitPrice.saving}"/>'
                         data-percentage='${item.discountUnitPrice.percentage}'
                         data-selected='${item.selected}'>${item.formatedValue} </option>
				
				</c:when>
				<c:otherwise>
				<option class="itemSelectQty" data-value="${item.formatedValue}" data-Qty="${item.quantity}"
                         data-stock="${item.inStock}" data-price='<format:fromPrice	priceData="${item.price}"/>'
                         data-priceAfterSaving='<format:fromPrice	priceData="${item.priceAfterSaving}"/>'
                         data-discountedPrice='<format:fromPrice	priceData="${item.discountUnitPrice.discountPrice}"/>'
                         data-saving='<format:fromPrice	priceData="${item.discountUnitPrice.saving}"/>'
                         data-percentage='${item.discountUnitPrice.percentage}'
                         data-selected='${item.selected}'>${item.formatedValue} </option>
				
				</c:otherwise>
			
			</c:choose>
				 
            </c:forEach>
		</select>
        </c:if>
		</span>
        <span hidden> ${product.selectedQuantity} </span>
        <span hidden> ${product.entryId} </span>

        <div class="qty-selector input-group js-qty-selector ${unitFactorRangeData}">
			<span class="input-group-btn">
				<button class="btn js-qty-selector-minus" ${disableMaxQty} type="button"  <c:if
                        test="${qtyMinus <= 1}"><c:out value="disabled='disabled'" /></c:if> ><span class="fas fa-minus"
                                                                                                    aria-hidden="true"></span></button>
			</span>
            <input type="text" maxlength="3" class="form-control js-qty-selector-input" size="1"
                   value="${fn:escapeXml(qtyMinus)}" data-max="${fn:escapeXml(maxQty)}" data-min="1"
                   name="pdpAddtoCartInput" id="pdpAddtoCartInput" ${disableMaxQtyInput} />
            <span class="input-group-btn">
				<button class="btn js-qty-selector-plus" type="button" ${disableMaxQty}><span class="fas fa-plus"
                                                                                              aria-hidden="true"></span></button>
			</span>
        </div>
    </c:if>

    <div class="actions">
        <c:if test="${multiDimensionalProduct}">
            <c:url value="${product.url}/orderForm" var="productOrderFormUrl" />
            <a href="${productOrderFormUrl}" class="btn btn-default btn-block btn-icon js-add-to-cart fa-list-alt">
                <spring:theme code="order.form" />
            </a>
        </c:if>
        <action:actions element="div" parentComponent="${component}" />
    </div>


</div>
<div class="addedtocart hidden">
    <spring:theme code="basket.added.to.basket" />
</div>
<div class="addedtocarterror hidden">

</div>
<div class="addedtocartinfo hidden">

</div>

<c:if test="${product.stock.stockLevel gt 0}">
    <c:set var="productStockLevel">
        <i class="fal fa-check-circle"></i> &nbsp;<spring:theme code="product.variants.in.stock" />
    </c:set>
</c:if>

<c:if test="${product.stock.stockLevel eq 0}">
    <c:set var="productStockLevel">
        <i class="fas fa-times-circle"></i>
        <spring:theme code="product.variants.out.of.stock" />
    </c:set>

</c:if>

<c:if test="${product.stock.stockLevelStatus.code eq 'lowStock'}">
    <c:set var="productStockLevel">
        <i class="fal fa-check-circle"></i> <spring:theme code="product.variants.only.left"
                                                          arguments="${product.stock.stockLevel}" />
    </c:set>
</c:if>


<c:if test="${isForceInStock}">
    <c:set var="productStockLevel">
        <i class="fal fa-check-circle"></i><spring:theme code="product.variants.available" />
    </c:set>
</c:if>
<div class="stock-wrapper">

    <c:choose>
        <c:when test="${empty product.unitFactorRangeData}">
            ${productStockLevel}
        </c:when>
        <c:otherwise>
            <c:choose>
                <c:when test="${product.unitFactorRangeData[0].inStock}">
                    <span class="InStock"> <i class="fal fa-check-circle"></i><spring:theme
                            code="product.variants.in.stock" /></span>
                    <span class="OutOfStock hidden"><i class="fas fa-times-circle"></i><spring:theme
                            code="product.variants.out.of.stock" /></span>
                </c:when>

                <c:otherwise>
                    <span class="InStock hidden"><i class="fal fa-check-circle"></i><spring:theme
                            code="product.variants.in.stock" /></span>
                    <span class="OutOfStock "><i class="fas fa-times-circle"></i><spring:theme
                            code="product.variants.out.of.stock" /></span>

                </c:otherwise>
            </c:choose>


        </c:otherwise>
    </c:choose>


</div>

