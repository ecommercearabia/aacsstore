<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">

	<div class="row sec1">
	
		
		<div class="col-lg-8  col-md-10 col-sm-8 col-xs-10 col-md-offset-1 col-sm-offset-2 col-xs-offset-1 col-lg-offset-2 ">
			<cms:pageSlot position="OTPSection1" var="feature" >
				<cms:component component="${feature}" />
			</cms:pageSlot>
		</div>
		
	</div>

	<cms:pageSlot position="OTPSection2" var="feature">
		<cms:component component="${feature}" />
	</cms:pageSlot>

</template:page>