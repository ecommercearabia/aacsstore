<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<div class="account-section-header">

	<spring:theme code="text.account.profile.updatePasswordForm"/>

</div>
<div class="row">
	<div class="container-lg col-md-8 col-md-offset-2">
		<div class="account-section-content">
			<div class="account-section-form card">
				<form:form action="${action}" method="post" modelAttribute="updatePasswordForm">

					<div class="currentPassword newpass"><formElement:formPasswordBox idKey="currentPassword"
																			  labelKey="profile.currentPassword" path="currentPassword" inputCSS="form-control"
																			  mandatory="true" /></div>
					<div class="newPassword newpass"><formElement:formPasswordBox idKey="newPassword"
																		  labelKey="profile.newPassword" path="newPassword" inputCSS="form-control"
																		  mandatory="true" /></div>
					<div class="checkNewPassword newpass">	<formElement:formPasswordBox idKey="checkNewPassword"
																				   labelKey="profile.checkNewPassword" path="checkNewPassword" inputCSS="form-control"
																				   mandatory="true" />
					</div>

					<div class="row">
						<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 col-sm-offset-2 col-md-offset-6 col-lg-offset-6 accountButtons">
							<div class="accountActions">
								<button type="submit" class="btn btn-primary btn-block">
									<spring:theme code="updatePwd.submit" text="Update Password" />
								</button>
							</div>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 accountButtons">
							<div class="accountActions">
								<button type="button" class="btn btn-default btn-block backToHome">
									<spring:theme code="text.button.cancel" text="Cancel" />
								</button>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>