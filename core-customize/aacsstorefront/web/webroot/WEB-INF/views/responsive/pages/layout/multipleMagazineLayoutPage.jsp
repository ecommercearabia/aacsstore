<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}">

<cms:pageSlot position="Section1" var="feature" element="div"   class="row fixMar  firstBanner">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
</cms:pageSlot>

   <cms:pageSlot position="Section2" var="feature" element="div"   class="row fixMar bannarImagepaddingdowndots">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-3 col-sm-4 col-xs-12"/>
</cms:pageSlot>

   <cms:pageSlot position="Section3" var="feature" element="div"   class="row fixMar bannarImagepaddingdowndots paddCarExpress">
        <cms:component component="${feature}" element="div" class="yComponentWrapper col-md-12 col-sm-12 col-xs-12"/>
</cms:pageSlot>


</template:page>
