<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/responsive/store" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="account-section-header"><spring:theme code="storeFinder.find.a.store" /></div>
<div class="row">
<div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
<store:storeSearch errorNoResults="${errorNoResults}"/>
</div>
<div class="col-xs-12 col-md-12">
<store:storeListForm searchPageData="${searchPageData}" locationQuery="${locationQuery}" numberPagesShown="${numberPagesShown}" geoPoint="${geoPoint}"/>

</div>
</div>