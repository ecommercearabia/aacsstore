<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<%@ attribute name="productData" required="true" type="java.util.List" %>
<%@ attribute name="title" required="false" type="java.lang.String"%>

<div class="carousel__component">
			<div class="carousel__component--headline">${fn:escapeXml(title)}</div>

			
					<div class="carousel__component--carousel js-owl-carousel owl-carousel js-owl-default owl-theme">
						<c:forEach items="${productData}" var="productReference">
							<c:set var="product" value="${productReference.target}"/>
							<c:url value="${product.url}" var="productUrl"/>

							<div class="carousel__item  ${product.code}"  >
							
				
			
								
									<div class="carousel__item--thumb">
										<c:if test="${not empty product.discount.percentage}">
	<div class="promo_label hidden">-<fmt:formatNumber type="NUMBER" value="${product.discount.percentage}" maxFractionDigits="0"/>%</div>
	</c:if>
									
									
<c:url value="javascript:;" var="link"></c:url>
									 <c:set value="" var="PopupWishlist"></c:set>
<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
<c:set value="js-popup-wishlist" var="PopupWishlist"></c:set>
<c:url value="javascript:;" var="link"></c:url>
</sec:authorize>
<c:set value="" var="isout"></c:set>
<c:set value="hidden" var="isin"></c:set>
	<c:if test="${product.inWishlist}">
	<c:set value="" var="isin"></c:set>
	<c:set value="hidden" var="isout"></c:set>
	</c:if>
	<c:if test="${!product.inWishlist}">
	<c:set value="hidden" var="isin"></c:set>
	<c:set value="" var="isout"></c:set>
	</c:if>
	

        <span class="wishlist_icon">
	<a href="${link}" title="wishlist" class="removeWishlistEntry wishlistbtn ${PopupWishlist} ${isin} " data-productcode="${product.code}" data-pk="8796093055677"><i class="fas fa-heart"></i></a>
	<a href="${link}" title="wishlist" class="addWishlistEntry wishlistbtn ${PopupWishlist}  ${isout}" data-productcode="${product.code}" data-pk="8796093055677"><i class="fas fa-heart"></i></a>
				
				</span>
									<a href="${productUrl}">
									<c:if test="${not empty product.productLabel}"><div class="carousel__item--label">${product.productLabel}</div></c:if>
										<product:productPrimaryImage product="${product}" format="zoom"/>
										
										
										</a>
									</div>
									
									<div class="cont_detail_carousel">
									<a href="${productUrl}">
									<div class="cont_href_div">
									<div class="carousel__item--name">
									
									<c:choose>
						<c:when test="${fn:length(product.name) > 44}">
							<c:out value="${fn:substring(product.name, 0, 44)}..."/>
						</c:when>
						<c:otherwise>
						${fn:escapeXml(product.name)}
						</c:otherwise>
						</c:choose>
									
									</div>
									
									
									
									<c:if test="${not empty product.countryOfOrigin}">
									<div class="carousel__item--countryoforigin hidden">${product.countryOfOrigin}</div>
									<c:set value="${fn:replace(product.countryOfOriginIsocode, ' ', '-')}" var="countryOfOrigin"></c:set>
									<div class="carousel__item--countryoforiginisocode"><i class="flagicon ${fn:toLowerCase(countryOfOrigin)}"></i>${product.countryOfOrigin} / </div>
									</c:if>
									</div></a>
									<div class="price_counter">
									<a href="${productUrl}" class="stockLevel">
									<c:if test="${product.stock.stockLevelStatus.code eq 'outOfStock'}"><span class="OutOfStock"><i class="fas fa-times-circle"></i><spring:theme code='product.variants.out.of.stock'/></span></c:if>
									</a>
									<div class="carousel__item--price price">
									
									
									                     <c:choose>
                            <c:when test="${not empty product.unitFactorRangeData}">


                                <c:choose>
                                    <c:when test="${not empty product.unitFactorRangeData[0].discountUnitPrice}">


                                    <span class="price">

                                           <format:fromPrice
                                                   priceData="${product.unitFactorRangeData[0].discountUnitPrice.discountPrice}"/>


                                   </span>
                                        <span class="disCountPrice">  <span class="scratched">
                                        <span class="Pricescratched">  <format:fromPrice priceData="${product.unitFactorRangeData[0].discountUnitPrice.price}"/></span>
                                    <span
                                            class="line_dis ${display_inline} "></span></span>
                                        </span>


                                    </c:when>
                                    <c:otherwise>



                                     <span class="price">
                                    <format:fromPrice priceData="${product.unitFactorRangeData[0].price}"/>
                                          <span class="disCountPrice">  <span class="scratched hidden">
                                                <span class="Pricescratched"> </span>
                                    <span
                                            class="line_dis hidden "></span></span>
                                        </span>

                                    </span>

                                    </c:otherwise>

                                </c:choose>


                            </c:when>
                            <c:otherwise>


                                <c:choose>

                                    <c:when test="${not empty product.discount}">
                                        <c:set value="display_in" var="display_inline"></c:set>
                                        <span class="price">




                                                <format:fromPrice
                                                        priceData="${product.discount.discountPrice}"/>

                                    </span>


                                        <span class="scratched"><format:fromPrice
                                                priceData="${product.discount.price}"/><span
                                                class="line_dis ${display_inline} "></span></span>
                                        <%--                                        <span class="discount_style">(${product.discount.percentage})</span> --%>

                                    </c:when>
                                    <c:otherwise>

                                        <span class="scratched"></span>
                                        <product:productPricePanel product="${product}"/>

                                    </c:otherwise>

                                </c:choose>


                            </c:otherwise>
                        </c:choose>
									
									
									
									
<%-- 									<c:choose> --%>
<%-- 						<c:when test="${not empty product.discount}"> --%>
							
<%-- 							<p class="price"><format:fromPrice priceData="${product.discount.discountPrice}"/></p> --%>
<%-- 						<span class="scratched"><format:fromPrice priceData="${product.discount.price}"/><span class="line_dis"></span></span> --%>
<%-- 						</c:when> --%>
<%-- 						<c:otherwise> --%>
						
<%-- 						<p class="price"><format:fromPrice priceData="${product.price}"/></p> --%>
<!-- 							<span class="scratched"></span>	 -->
<%-- 						</c:otherwise> --%>
<%-- 					</c:choose> --%>
									</div>
										
										<product:addtocartcarousel showQuantityBox="true" product="${product}" />
										</div>
										</div>
								</a>
						
						
							</div>
							
						</c:forEach>
					</div>
		</div>