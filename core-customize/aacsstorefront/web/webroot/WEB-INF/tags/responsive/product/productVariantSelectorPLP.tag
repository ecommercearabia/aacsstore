<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>

<c:set var="showAddToCart" value="" scope="session"/>
<spring:htmlEscape defaultHtmlEscape="true" />


            <c:if test="${not empty product.variantOptions}">
                <c:set var="variantOptions" value="${product.variantOptions}"/>
            </c:if>
            <c:if test="${not empty product.baseOptions[0].options}">
                <c:set var="variantOptions" value="${product.baseOptions[0].options}"/>
            </c:if>
            
            
        <c:if test="${not empty variantOptions}">
            <div class="variant-section">
                <div class="variant-selector">
                    <div class="variant-name">
                        <label for="Type"><spring:theme code="product.variants.type"/><span
                                class="variant-selected typeName"></span></label>
                    </div>
                    <select id="variant" class="form-control variant-select hidden" >

                        <c:forEach items="${variantOptions}" var="variantOption">
                            <c:set var="optionsStringHtml" value=""/>
                            <c:set var="nameStringHtml" value=""/>
                            <c:forEach items="${variantOption.variantOptionQualifiers}" var="variantOptionQualifier">
                                <c:set var="optionsStringHtml">${optionsStringHtml}</c:set>
                                <c:set var="nameStringHtml">${fn:escapeXml(variantOptionQualifier.value)}</c:set>
                            </c:forEach>

                            <c:if test="${(variantOption.stock.stockLevel gt 0) and (variantSize.stock.stockLevelStatus ne 'outOfStock')}">
                                <c:set var="stockLevelHtml">${fn:escapeXml(variantOption.stock.stockLevel)} <spring:theme code="product.variants.in.stock"/></c:set>
                            </c:if>
                            <c:if test="${(variantOption.stock.stockLevel le 0) and (variantSize.stock.stockLevelStatus eq 'inStock')}">
                                <c:set var="stockLevelHtml"><spring:theme code="product.variants.available"/></c:set>
                            </c:if>
                            <c:if test="${(variantOption.stock.stockLevel le 0) and (variantSize.stock.stockLevelStatus ne 'inStock')}">
                                <c:set var="stockLevelHtml"><spring:theme code="product.variants.out.of.stock"/></c:set>
                            </c:if>
                            <c:choose>
                                <c:when test="${product.purchasable and product.stock.stockLevelStatus.code ne 'outOfStock' }">
                                    <c:set var="showAddToCart" value="${true}" scope="session"/>
                                </c:when>
                                <c:otherwise>
                                    <c:set var="showAddToCart" value="${false}" scope="session"/>
                                </c:otherwise>
                            </c:choose>
                            <c:url value="${variantOption.url}" var="variantOptionUrl"/>
                            <c:if test="${(variantOption.url eq product.url)}">
                                <c:set var="showAddToCart" value="${true}" scope="session"/>
                                <c:set var="currentSizeHtml" value="${nameStringHtml}"/>
                            </c:if>
                            <option value="${fn:escapeXml(variantOptionUrl)}" ${(variantOption.url eq product.url) ? 'selected="selected"' : ''}>
                                <span class="variant-selected">${optionsStringHtml}</span>
                            </option>
                        </c:forEach>
                    </select>




                    <div class="variant-type">


                            <c:forEach items="${variantOptions}" var="variantOption">
                                <c:set var="optionsStringHtml" value=""/>

                                <c:set var="nameStringHtml" value=""/>
                            <c:forEach items="${variantOption.variantOptionQualifiers}" var="variantOptionQualifier">
                         		<c:if test="${variantOptionQualifier.qualifier eq 'unitVariant'}">
                         		 <c:set var="optionsStringHtml">${optionsStringHtml}${fn:escapeXml(variantOptionQualifier.value)}</c:set>
                                <c:set var="nameStringHtml">${fn:escapeXml(variantOptionQualifier.value)}</c:set>
                         		</c:if> 
                           
                            </c:forEach>

                            <c:if test="${(variantOption.stock.stockLevel gt 0) and (variantSize.stock.stockLevelStatus ne 'outOfStock')}">
                            <c:set var="stockLevelHtml">${fn:escapeXml(variantOption.stock.stockLevel)} <spring:theme code="product.variants.in.stock"/></c:set>
                            </c:if>
                            <c:if test="${(variantOption.stock.stockLevel le 0) and (variantSize.stock.stockLevelStatus eq 'inStock')}">
                                <c:set var="stockLevelHtml"><spring:theme code="product.variants.available"/></c:set>
                            </c:if>
                            <c:if test="${(variantOption.stock.stockLevel le 0) and (variantSize.stock.stockLevelStatus ne 'inStock')}">
                                <c:set var="stockLevelHtml"><spring:theme code="product.variants.out.of.stock"/></c:set>
                            </c:if>
                            <c:choose>
                            <c:when test="${product.purchasable and product.stock.stockLevelStatus.code ne 'outOfStock' }">
                                <c:set var="showAddToCart" value="${true}" scope="session"/>
                            </c:when>
                            <c:otherwise>
                                <c:set var="showAddToCart" value="${false}" scope="session"/>
                            </c:otherwise>
                            </c:choose>
                                <c:url value="${variantOption.url}" var="variantOptionUrl"/>
                                <c:set var="select" value=" "/>
                            <c:if test="${(variantOption.url eq product.url)}">
                                <c:set var="showAddToCart" value="${true}" scope="session"/>
                                <c:set var="currentSizeHtml" value="${nameStringHtml}"/>
                                <c:set var="select" value="active-item"/>
                            </c:if>

                            <a class="item-variant ${select}"  href="${fn:escapeXml(variantOptionUrl)}" ${(variantOption.url eq product.url) ? 'selected="selected"' : ''}>
                                <span class="variant-selected">${optionsStringHtml}</span>
                            </a>
                            </c:forEach>
                    </div>




                    <div id="currentTypeValue" data-type-value="${currentSizeHtml}"></div>
                </div>
            </div>
        </c:if>

