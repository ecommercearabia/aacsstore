<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ attribute name="provider" required="false"
	type="com.aacs.aacsthirdpartyauthentication.data.ThirdPartyAuthenticationProviderData"%>


<spring:htmlEscape defaultHtmlEscape="true" />
<c:url value="/thirdpartyauthentication/getuserdata"
	var="getUserDataURL" />
<script src="https://apis.google.com/js/platform.js" async defer></script>

<meta name="google-signin-client_id"
	content="${provider.id }" />


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>



<div  class="g-signin2" data-onsuccess="onSignIn" onclick="ClickLogin()">
</div>


<script>
	
var clicked=false;//Global Variable
function ClickLogin()
{
	console.log("clicked");
    clicked=true;
}
function onSignIn(googleUser) {
if(clicked){
		var id_token = googleUser.getAuthResponse().id_token;
		  var profile = googleUser.getBasicProfile();
	         console.log('ID: ' + profile.getId());
	         console.log('Name: ' + profile.getName());
	         console.log('Image URL: ' + profile.getImageUrl());
	         console.log('Email: ' + profile.getEmail());
	         console.log('id_token: ' + googleUser.getAuthResponse().id_token);
		console.log(id_token);
		
		var redirectUrl = ACC.config.encodedContextPath + '/thirdpartyauthentication/getuserdata';
		
		 var form = $('<form action="' + redirectUrl + '" method="get">' +
                 '<input type="text" name="data" value="' +
                  id_token + '" />' +
                  '<input type="text" name="type" value="GOOGLE" />' +
                                                       '</form>');
		$('body').append(form);
		form.submit();

		console.log("SUBMITED");
		

}

	}
	$( "#google_action" ).click(function() {
 $(".g-signin2 .abcRioButtonContentWrapper").trigger('click');
});
	
	</script>