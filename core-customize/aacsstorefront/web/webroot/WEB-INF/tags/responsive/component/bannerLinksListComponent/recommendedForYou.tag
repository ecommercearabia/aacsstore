<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ attribute name="bannerLinksList" required="false" type="com.aacs.aacscomponents.model.BannerLinksListComponentModel" %>

<div class="carousel__component categoriesSection">
	<div class="titleSection"><span class="carousel__component--headline">${bannerLinksList.title}</span> <span class="linkView"><cms:component component="${bannerLinksList.link}"  /></span></div>


	<div class="carousel__component--carousel js-owl-carousel owl-carousel js-owl-categories owl-theme">

		<c:forEach items="${bannerLinksList.bannerLinksComponents}"
				   var="bannerLinks">

			<div class="carousel__item"  >



				<c:url value="${bannerLinks.link.url}" var="simpleBannerUrl" />
					<%-- 		<cms:component component="${bannerLinks.link}"  /> --%>

				<div class="banner__component simple-banner box_img text-center">
					<c:if test="${ycommerce:validateUrlScheme(bannerLinks.media.url)}">
						<c:choose>
							<c:when test="${empty simpleBannerUrl || simpleBannerUrl eq '#' || !ycommerce:validateUrlScheme(simpleBannerUrl)}">
								<img title="${fn:escapeXml(media.altText)}" alt="${fn:escapeXml(bannerLinks.media.altText)}"
									 src="${fn:escapeXml(bannerLinks.media.url)}">
							</c:when>
							<c:otherwise>
								<a href="${fn:escapeXml(simpleBannerUrl)}"><img title="${fn:escapeXml(bannerLinks.media.altText)}"
																				alt="${fn:escapeXml(bannerLinks.media.altText)}" src="${fn:escapeXml(bannerLinks.media.url)}"></a>
							</c:otherwise>
						</c:choose>
					</c:if>

					<h2 class="titleBox">${bannerLinks.title}</h2>
				</div>
			</div>

		</c:forEach>
	</div>
</div>


