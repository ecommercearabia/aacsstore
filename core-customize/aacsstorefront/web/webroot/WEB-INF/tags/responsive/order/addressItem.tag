<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="address" required="true"
	type="de.hybris.platform.commercefacades.user.data.AddressData"%>
<%@ attribute name="storeAddress" required="false"
	type="java.lang.Boolean"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:if test="${not storeAddress }">
	<c:if test="${not empty address.title}">
	    ${fn:escapeXml(address.title)}&nbsp;
	</c:if>
	${fn:escapeXml(address.firstName)}&nbsp;${fn:escapeXml(address.lastName)}
	<br>
</c:if>
${fn:escapeXml(address.line1)}
<c:if test="${not empty address.line2}">
	<br>
	${fn:escapeXml(address.line2)}
</c:if>
<br>
${fn:escapeXml(address.city.name)}&nbsp;${fn:escapeXml(address.region.name)}
<br>
${fn:escapeXml(address.country.name)}&nbsp;${fn:escapeXml(address.postalCode)}
<br />
<c:if test="${not empty address.mobileNumber}">
	${fn:escapeXml(address.mobileNumber)}
	<br />
</c:if>
<c:if test="${not empty address.nearestLandmark}">
	${fn:escapeXml(address.nearestLandmark)}
	<br />
</c:if>
<c:if test="${not empty address.buildingName}">
	${fn:escapeXml(address.buildingName)}
	<br />
</c:if>
<c:if test="${not empty address.floorNumber}">
	${fn:escapeXml(address.floorNumber)}
	<br />
</c:if>
<c:if test="${not empty address.apartmentNumber}">
	${fn:escapeXml(address.apartmentNumber)}
	<br />
</c:if>
<c:if test="${not empty address.deliveryNotes}">
	${fn:escapeXml(address.deliveryNotes)}
	<br />
</c:if>
