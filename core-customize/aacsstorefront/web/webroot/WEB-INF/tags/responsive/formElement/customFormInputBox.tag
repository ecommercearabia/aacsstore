<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="idKey" required="true" type="java.lang.String"%>
<%@ attribute name="labelKey" required="true" type="java.lang.String"%>
<%@ attribute name="path" required="true" type="java.lang.String"%>
<%@ attribute name="mandatory" required="false" type="java.lang.Boolean"%>
<%@ attribute name="labelCSS" required="false" type="java.lang.String"%>
<%@ attribute name="inputCSS" required="false" type="java.lang.String"%>
<%@ attribute name="placeholder" required="false" type="java.lang.String"%>
<%@ attribute name="tabindex" required="false" rtexprvalue="true"%>
<%@ attribute name="autocomplete" required="false" type="java.lang.String"%>
<%@ attribute name="disabled" required="false" type="java.lang.Boolean"%>
<%@ attribute name="maxlength" required="false" type="java.lang.Integer"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<%@ attribute name="customer" required="false" type="de.hybris.platform.commercefacades.user.data.PrincipalData" %>

<%
	String sessionAlive = "";
	if(session != null && "SESSION_ALIVE".equals(session.getAttribute("SESSION_ALIVE")))
	{
		sessionAlive = (String)session.getAttribute("SESSION_ALIVE");
	}
%>


<spring:htmlEscape defaultHtmlEscape="true" />

<c:set var="sessionAlive" value='<%=sessionAlive%>' />

<template:errorSpanField path="${path}">
	<ycommerce:testId code="LoginPage_Item_${idKey}">
		<label class="control-label ${fn:escapeXml(labelCSS)}" for="${fn:escapeXml(idKey)}">
			<spring:theme code="${labelKey}" />
			<c:if test="${mandatory != null && mandatory == false}">
				<span>&nbsp;<spring:theme code="login.optional" /></span>
			</c:if>
		</label>
			
		<spring:theme code="login.optional" var="optionalField" htmlEscape="false"/>
		<spring:theme code="${placeholder}" var="placeHolderMessage" htmlEscape="false"/>

		<form:input cssClass="${fn:escapeXml(inputCSS)} form-control" id="${idKey}" path="${path}"
					tabindex="${tabindex}" autocomplete="${autocomplete}" placeholder="${placeHolderMessage}"
					disabled="${disabled}" maxlength="${maxlength}"/>
		<c:if test="${not empty customer && customer.uid ne 'anonymous' && empty sessionAlive}">
				<font color="class-error"><spring:theme code="session.timeout" text="Your session has expired" /></font>
			</c:if>				
	</ycommerce:testId>
</template:errorSpanField>
