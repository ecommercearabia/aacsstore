<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="amount" required="true" type="de.hybris.platform.commercefacades.product.data.PriceData" %>
<spring:url var="storeCreditURL" value="{contextPath}/my-account/store-credit" htmlEscape="false" >
	<spring:param name="contextPath" value="${request.contextPath}" />
</spring:url>




<div class="cont_box"><div class="left-side">
	<div class="far fa-credit-card"></div></div>
	<div class="right-side">
	<div class="headline">


	<div class="head_myaccount">
		<spring:theme code="store.credit"/>
	</div>
	
	
	<div class="order_dis">
	<div class="font-title">
	<div class="myaccount_title"><spring:theme code="myaccount.storecredit.amount"/></div> <div class="myaccount_subtitle">${amount.formattedValue}<br></div>
	</div></div>
	<div class="body">
		
	</div>

</div>
</div>
<a href="${storeCreditURL}" class="link_box_cont"></a>
</div>