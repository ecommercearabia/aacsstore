ACC.product = {

    _autoload: [
        "bindToAddToCartForm",
        "enableStorePickupButton",
        "enableVariantSelectors",
        "bindFacets"
    ],


    bindFacets: function () {
        $(document).on("click", ".js-show-facets", function (e) {
            e.preventDefault();
            var selectRefinementsTitle = $(this).data("selectRefinementsTitle");
            var colorBoxTitleHtml = ACC.common.encodeHtml(selectRefinementsTitle);


            ACC.colorbox.open(colorBoxTitleHtml, {
                href: ".js-product-facet",
                className: "filterMobile",
                inline: true, innerWidth: "100%", height: "100%",
                width: "100%",
                close: '<i class="fal fa-times"></i>',
                onComplete: function () {
                    $('body').css('overflow', 'hidden');


                },
                onClosed: function () {
                    $('body').css('overflow', 'auto');
                    $('body').css('overflow-x', 'hidden');
                    $(document).off("click", ".js-product-facet .js-facet-name");
                }
            });
        });
        enquire.register("screen and (min-width:" + screenSmMax + ")", function () {
            $("#cboxClose").click();
        });
    },


    enableAddToCartButton: function () {
        $('.js-enable-btn').each(function () {
            if (!($(this).hasClass('outOfStock') || $(this).hasClass('out-of-stock'))) {
                $(this).prop("disabled", false);
            }
        });
    },

    enableVariantSelectors: function () {
        $('.variant-select').prop("disabled", false);
    },

    bindToAddToCartForm: function () {
        var addToCartForm = $('.add_to_cart_form');
        addToCartForm.ajaxForm({
            beforeSubmit: ACC.product.showRequest,
            success: ACC.product.displayAddToCartPopup
        });
        setTimeout(function () {
            $ajaxCallEvent = true;
        }, 2000);
    },
    showRequest: function (arr, $form, options) {
        if ($ajaxCallEvent) {
            $ajaxCallEvent = false;
            return true;
        }
        return false;

    },

    bindToAddToCartStorePickUpForm: function () {
        var addToCartStorePickUpForm = $('#colorbox #add_to_cart_storepickup_form');
        addToCartStorePickUpForm.ajaxForm({ success: ACC.product.displayAddToCartPopup });
    },

    enableStorePickupButton: function () {
        $('.js-pickup-in-store-button').prop("disabled", false);
    },

    displayAddToCartPopup: function (cartResult, statusText, xhr, formElement) {
        var valueError = $('.cart_popup_error_msg').html();
        console.log(valueError)
        $('.descriptionCartToast').html(valueError);
        $ajaxCallEvent = true;
        $('#addToCartLayer').remove();
        if (typeof ACC.minicart.updateMiniCartDisplay == 'function') {
            ACC.minicart.updateMiniCartDisplay();
        }
        var titleHeader = $('#addToCartTitle').text();

//        ACC.colorbox.open(titleHeader, {
//            html: cartResult.addToCartLayer,
//            width: "460px"
//        });


        var text = cartResult.addToCartLayer;
        var textdec = $(text).find('.cart_popup_error_msg').html();
        $('.product-main-info .addedtocartinfo').html(textdec);
        $('.product-main-info .addedtocartinfo').removeClass('hidden');


        $('.product-main-info .addedtocartinfo').fadeIn("300", function () {
            $('.product-main-info .addedtocartinfo').removeClass('hidden');
        });
        setTimeout(() => $('.product-main-info .addedtocartinfo').fadeOut("slow"), 4000);


        var titleHeadertoast = $('.titletoast').val()
        $.toast({

            text: text, // Text that is to be shown in the toast
            heading: titleHeadertoast, // Optional heading to be shown on the toast
            // Type of toast icon
            showHideTransition: 'fade', // fade, slide or plain
            allowToastClose: true, // Boolean value true or false
            hideAfter: 4000, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
            stack: 1, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time
            position: 'top-right', // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values


            textAlign: 'left',  // Text alignment i.e. left, right or center
            loader: false,  // Whether to show loader or not. True by default
        });


        var productCode = $('[name=productCodePost]', formElement).val();
        var quantityField = $('[name=qty]', formElement).val();

        var quantity = 1;
        if (quantityField != undefined) {
            quantity = quantityField;
        }

        var cartAnalyticsData = cartResult.cartAnalyticsData;

        var cartData = {
            "cartCode": cartAnalyticsData.cartCode,
            "productCode": productCode, "quantity": quantity,
            "productPrice": cartAnalyticsData.productPostPrice,
            "productName": cartAnalyticsData.productName
        };
        ACC.track.trackAddToCart(productCode, quantity, cartData);
    }
};

$(document).ready(function () {
    if ($(".page-homepage").length) {
        $("input[name=CSRFToken]").val(ACC.config.CSRFToken);
    }

    $ajaxCallEvent = true;
    ACC.product.enableAddToCartButton();
});