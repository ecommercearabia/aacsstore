ACC.autocomplete = {

  _autoload: [
    "bindSearchAutocomplete",
    "bindDisableSearch"
  ],

  bindSearchAutocomplete: function () {
    // extend the default autocomplete widget, to solve issue on multiple instances of the searchbox component
    $.widget("custom.yautocomplete", $.ui.autocomplete, {
      _create: function () {

        // get instance specific options form the html data attr
        var option = this.element.data("options");
        // set the options to the widget
        this._setOptions({
          minLength: option.minCharactersBeforeRequest,
          displayProductImages: option.displayProductImages,
          delay: option.waitTimeBeforeRequest,
          autocompleteUrl: option.autocompleteUrl,
          source: this.source
        });

        // call the _super()
        $.ui.autocomplete.prototype._create.call(this);

      },
      options: {
        cache: {}, // init cache per instance
        focus: function () {
          return false;
        }, // prevent textfield value replacement on item focus
        select: function (event, ui) {
          ui.item.value = ACC.sanitizer.sanitizeSelect(ui.item.value);
          window.location.href = ui.item.url;
        }
      },
      _renderItem: function (ul, item) {

        if (item.type == "autoSuggestion") {
          var renderHtml = $("<a>").attr("href", item.url)
            .append($("<div>").addClass("name").text(item.value));
          return $("<li>")
            .data("item.autocomplete", item)
            .append(renderHtml)
            .appendTo(ul);
        } else if (item.type == "productResult") {
          var renderHtml = $("<a>").attr("href", item.url)
            .append(
              item.image
                ? $("<div>").addClass("thumb")
                  .append($("<img>").attr("src", item.image))
                : null
            )
            .append($("<div>").addClass("name").html(ACC.sanitizer.sanitize(item.value)))
            .append($("<div>").addClass("price").text(item.price));

          return $("<li>")
            .data("item.autocomplete", item)
            .append(renderHtml)
            .appendTo(ul);
        }
      },
      source: function (request, response) {

        var self = this;
        var term = request.term.toLowerCase();
        if (term in self.options.cache) {
          return response(self.options.cache[term]);
        }

        $.getJSON(self.options.autocompleteUrl, { term: request.term }, function (data) {
          var autoSearchData = [];
          if (data.suggestions != null) {
            $.each(data.suggestions, function (i, obj) {
              autoSearchData.push({
                value: obj.term,
                url: ACC.config.encodedContextPath + "/search?text=" + encodeURIComponent(obj.term),
                type: "autoSuggestion"
              });
            });
          }
          if (data.products != null) {
            $.each(data.products, function (i, obj) {
	
				var price =obj.price.formattedValue;
				if(obj.unitFactorRangeData.length){
					price= obj.unitFactorRangeData[0].price.formattedValue
				}
	
              autoSearchData.push({
                value: ACC.sanitizer.sanitize(obj.name),
                code: obj.code,
                desc: ACC.sanitizer.sanitize(obj.description),
                manufacturer: ACC.sanitizer.sanitize(obj.manufacturer),
                url: ACC.config.encodedContextPath + obj.url,
                price: price,
                type: "productResult",
                image: (obj.images != null && self.options.displayProductImages) ? obj.images[0].url : null // prevent errors if obj.images = null
              });
            });
          }
          self.options.cache[term] = autoSearchData;
          return response(autoSearchData);
        });
      }

    });


    $search = $(".js-site-search-input");
    if ($search.length > 0) {
      $search.yautocomplete()
    }

  },

  bindDisableSearch: function () {


    $('.history-cont').addClass('hidden')
    if (this.value == "") {

      $('.row.hidden-md .input-group-btn ').removeClass('right-search-btn');
      $('.row.hidden-md  .js_clear_button ').addClass('hidden');

    } else {
      $('.row.hidden-md .input-group-btn ').addClass('right-search-btn');
      $('.row.hidden-md  .js_clear_button ').removeClass('hidden');


    }






    $('.row.hidden-md .js-site-search-input').keyup(function () {

      $('.row.hidden-md .js-site-search-input').val($('.row.hidden-md .js-site-search-input').val().replace(/^\s+/gm, ''));
      $('.row.hidden-md .js_search_button').prop('disabled', this.value == "" ? true : false);
    })

    $('.navigation--top .js-site-search-input').keyup(function () {

      $('.history-cont').addClass('hidden')
      if (this.value == "") {

        $('.navigation--top .input-group-btn ').removeClass('right-search-btn');
        $('.navigation--top  .js_clear_button ').addClass('hidden');

      } else {
        $('.navigation--top .input-group-btn ').addClass('right-search-btn');
        $('.navigation--top  .js_clear_button ').removeClass('hidden');


      }
      $('.navigation--top .js-site-search-input').val($('.navigation--top .js-site-search-input').val().replace(/^\s+/gm, ''));
      $('.navigation--top .js_search_button').prop('disabled', this.value == "" ? true : false);
    })
  }
};


$('.js_clear_button').click(function (event) {
  event.preventDefault();
  $(this).closest('.input-group').find('.js-site-search-input').val('');
  $(this).addClass('hidden')
})

$('.js-site-search-input').click(function () {
  $('.history_item_list').empty()
  if (this.value == "") {
    // console.log(123456)


    var value = [];
    if ($('.language-en').length) {
      var key = ACC.config.contextPath + 'En';

      if (localStorage.getItem(key)) {
        value = JSON.parse(localStorage.getItem(key))

      }


    } else {
      var key = ACC.config.contextPath + 'Ar';
      if (localStorage.getItem(key)) {
        value = JSON.parse(localStorage.getItem(key))

      }


    }

    if (value.length) {
      var lengthItem = value.length
      if (lengthItem > 8) {
        lengthItem = 8
      }
      for (var i = 0; i < lengthItem; i++) {
        var link = $("<a>");
        link.attr("href", ACC.config.encodedContextPath + '/search/?text=' + decodeURI(value[i]));
        var text= decodeURI(value[i]);
        text =text.replace(/\+/g,' ')
        link.text(text);
        link.addClass("link");
        $(link).prependTo(".history_item_list");
      }

      $('.history-cont').removeClass('hidden')
    }
    else{
      $('.history-cont').addClass('hidden')
    }
  } else {
    // $('.history-cont').addClass('hidden')
  }
})


if (window.location.search) {
  var search = window.location.search;
  search = search.split('=');


  if(search[0]=='?text'){

    var item = search[1];
    if ($('.language-en').length) {
      var text= search[1];
      text =text.replace(/\+/g,' ')
      $('.js-site-search-input').val(text);
      $('.navigation--top  .js_clear_button ').removeClass('hidden');
      var key = ACC.config.contextPath + 'En';
      var value = [];
      if (localStorage.getItem(key)) {
        value = JSON.parse(localStorage.getItem(key))

      }
      if (value.includes(item)) {
        var index = value.indexOf(item);
        if (index > -1) {
          value.splice(index, 1);
        }

      }
      value.push(item);


      localStorage.setItem(key, JSON.stringify(value));
    } else {

      var text= decodeURI(search[1]);
      text =text.replace(/\+/g,' ')
      $('.js-site-search-input').val(text);
      $('.navigation--top  .js_clear_button ').removeClass('hidden');
      var key = ACC.config.contextPath + 'Ar';
      var value = [];
      if (localStorage.getItem(key)) {
        value = JSON.parse(localStorage.getItem(key))

      }
      if (value.includes(item)) {
        var index = value.indexOf(item);
        if (index > -1) {
          value.splice(index, 1);
        }

      }
      value.push(item);

    }
    localStorage.setItem(key, JSON.stringify(value));

  }


}


$('.clear-history').click(function () {


  var key = '';
  if ($('.language-en').length) {
    key = ACC.config.contextPath + 'En';


  } else {
    key = ACC.config.contextPath + 'Ar';


  }


  localStorage.removeItem(key);

})