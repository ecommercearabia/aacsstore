document.addEventListener("touchstart", function () {
}, false);


$('.wsmenu').append($('<a class="wsdownmenu-animated-arrow"><span></span></a>'));
$('.wsmenu').append($(''));
$('.wsdownmenu-animated-arrow').on('click', function () {
  $('.wsmenu-list').slideToggle('slow');
  $(this).toggleClass('wsdownmenu-lines');
  return false;
});

$('.wsmenu > .wsmenu-list > li').has('.sub-menu').prepend('<span class="wsmenu-click"><i class="wsmenu-arrow"></i></span>');
$('.wsmenu > .wsmenu-list > li').has('.wsmegamenu').prepend('<span class="wsmenu-click"><i class="wsmenu-arrow"></i></span>');

$('.wsmenu-click').click(function () {
  $(this).toggleClass('ws-activearrow')
    .parent().siblings().children().removeClass('ws-activearrow');
  $(".wsmenu > .wsmenu-list > li > .sub-menu, .wsmegamenu").not($(this).siblings('.wsmenu > .wsmenu-list > li > .sub-menu, .wsmegamenu')).slideUp('slow');
  $(this).siblings('.sub-menu').slideToggle('slow');
  $(this).siblings('.wsmegamenu').slideToggle('slow');
});

$('.wsmenu > .wsmenu-list > li > ul > li').has('.sub-menu').prepend('<span class="wsmenu-click02"><i class="wsmenu-arrow"></i></span>');
$('.wsmenu > .wsmenu-list > li > ul > li > ul > li').has('.sub-menu').prepend('<span class="wsmenu-click02"><i class="wsmenu-arrow"></i></span>');

$('.wsmenu-click02').click(function () {
  $(this).children('.wsmenu-arrow').toggleClass('wsmenu-rotate');
  $(this).siblings('li > .sub-menu').slideToggle('slow');
});

$(window).on('resize', function () {
  if ($(window).width() < 1023) {
  } else {
    $('.wsmenu-list').removeAttr("style");
    $('.wsdownmenu-animated-arrow').removeClass("wsdownmenu-lines");
    $('.wsmenu > .wsmenu-list > li > .wsmegamenu, .wsmenu > .wsmenu-list > li > ul.sub-menu, .wsmenu > .wsmenu-list > li > ul.sub-menu > li > ul.sub-menu, .wsmenu > .wsmenu-list > li > ul.sub-menu > li > ul.sub-menu > li > ul.sub-menu').removeAttr("style");
    $('.wsmenu-click').removeClass("ws-activearrow");
    $('.wsmenu-click02 > i').removeClass("wsmenu-rotate");
  }
});
$(window).trigger('resize');




/* global $ */
/* global document */



if ($(window).width() < 1023) {
} else {
  document.addEventListener("touchstart", function () {
  }, false);


  /* Automation Div */
  $('body').wrapInner('<div class="wsmenucontainer" />');
  $('<div class="overlapblackbg"></div>').prependTo('.wsmenu');

  $('#wsnavtoggle').click(function () {
    $('body').toggleClass('wsactive');
  });

  $('.overlapblackbg').click(function () {
    $("body").removeClass('wsactive');
  });

  /* Append and Toggle Class */
  $('.wsmenu-list> li').has('.sub-menu').prepend('<span class="wsmenu-click"><i class="wsmenu-arrow"></i></span>');
  $('.wsmenu-list > li').has('.wsshoptabing').prepend('<span class="wsmenu-click"><i class="wsmenu-arrow"></i></span>');
  $('.wsmenu-list > li').has('.wsmegamenu').prepend('<span class="wsmenu-click"><i class="wsmenu-arrow"></i></span>');
  $('.wsmenu-click').on('click', function () {
    $(this).toggleClass('ws-activearrow').parent().siblings().children().removeClass('ws-activearrow');
    $(".sub-menu, .wsshoptabing, .wsmegamenu").not($(this).siblings('.sub-menu, .wsshoptabing, .wsmegamenu')).slideUp('slow');
    $(this).siblings('.sub-menu').slideToggle('slow');
    $(this).siblings('.wsshoptabing').slideToggle('slow');
    $(this).siblings('.wsmegamenu').slideToggle('slow');
    return false;
  });

  $('.wstabitem > li').has('.wstitemright').prepend('<span class="wsmenu-click02"><i class="wsmenu-arrow"></i></span>');
  $('.wsmenu-click02').on('click', function () {
    $(this).siblings('.wstitemright').slideToggle('slow');
    $(this).toggleClass('ws-activearrow02').parent().siblings().children().removeClass('ws-activearrow02');
    $(".wstitemright").not($(this).siblings('.wstitemright')).slideUp('slow');
    return false;
  });

  $('.wstabitem02 > li').has('.wstbrandbottom').prepend('<span class="wsmenu-click03"><i class="wsmenu-arrow"></i></span>');
  $('.wsmenu-click03').on('click', function () {
    $(this).siblings('.wstbrandbottom').slideToggle('slow');
    $(this).toggleClass('ws-activearrow03').parent().siblings().children().removeClass('ws-activearrow03');
    $(".wstbrandbottom").not($(this).siblings('.wstbrandbottom')).slideUp('slow');
    return false;
  });

  /* Add Class in ever tabing menu LI When MouseEnter */
  $(window).ready(function () {
    $(".wsshoptabing.wtsdepartmentmenu > .wsshopwp > .wstabitem > li").on('mouseenter', function () {
      $(this).addClass("wsshoplink-active").siblings(this).removeClass("wsshoplink-active");
      return false;
    });
    $(".wsshoptabing.wtsbrandmenu > .wsshoptabingwp > .wstabitem02 > li").on('mouseenter', function () {
      $(this).addClass("wsshoplink-active").siblings(this).removeClass("wsshoplink-active");
      return false;
    });
  });
  setmenuheight();
  $(window).on("load resize", function () {
    var w_height = $(window).outerWidth();
    if (w_height <= 991) {
      $(".wsshopwp").css('height', '100%');
      $(".wstitemright").css('height', '100%');
    } else {
      setmenuheight();
    }
  });


  /* Calculating Height */
  function setmenuheight() {
    var TabgetHeight = 1;
    $(".wstabitem > li").each(function () {
      var forHeight = $(this).find(".wstitemright").innerHeight();
      TabgetHeight = forHeight > TabgetHeight ? forHeight : TabgetHeight;
      $(this).find(".wstitemright").css('height', 'auto');
    });
    $(".wsshopwp").css('height', TabgetHeight + 0);
  }


  /* Removing inLine Style  */
  $(document).ready(function ($) {
    function removeStyles() {
      if ($(window).outerWidth() >= 991) {
        $('.wsshoptabing, .wstitemright, .wstbrandbottom, .wsmegamenu, ul.sub-menu').css({
          'display': '',
        });
      }
    }

    removeStyles();
    $(window).resize(removeStyles);
  });


  /* Removing Class  */
  $(window).on('resize', function () {
    if ($(window).outerWidth() <= 991) {
      $('.wsmenu').css('height', $(this).height() + "px");
      $('.wsmenucontainer').css('min-width', $(this).width() + "px");
    } else {
      $('.wsmenu').removeAttr("style");
      $('.wsmenucontainer').removeAttr("style");
      $('body').removeClass("wsactive");
      $('.wsmenu-click').removeClass("ws-activearrow");
      $('.wsmenu-click02').removeClass("ws-activearrow02");
      $('.wsmenu-click03').removeClass("ws-activearrow03");
    }
  });
  $(window).trigger('resize');




  //Mobile Search Box
  $(window).on("load", function () {
    $('.wsmobileheader .wssearch').on("click", function () {
      $(this).toggleClass("wsopensearch");
    });
    $("body, .wsopensearch .wsclosesearch").on("click", function () {
      $(".wssearch").removeClass('wsopensearch');
    });
    $(".wssearch, .wssearchform form").on("click", function (e) {
      e.stopPropagation();
    });
  });
}

