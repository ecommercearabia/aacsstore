
$('#change-country').selectpicker('refresh');
$('#mcountry').selectpicker('refresh');
$('#addresscountry').selectpicker('refresh');
$('select').selectpicker();

//add checked in radio in slot time
$("input[name='selector_selector']").click(function(){
	 var data_code = $("input[name='selector_selector']:checked").data('code');
	    var data_start = $("input[name='selector_selector']:checked").data('start');
	    var data_end = $("input[name='selector_selector']:checked").data('end');
	    var data_day = $("input[name='selector_selector']:checked").data('day');
	    var data_date = $("input[name='selector_selector']:checked").data('date');
	    $("#periodCode").attr("value",data_code)
	     $("#start").attr("value",data_start)
	     $("#end").attr("value",data_end);
	    $("#day").attr("value",data_day);
	    $("#date").attr("value",data_date);
	   
});

//payment method on load 
var payment_ul= $('#selectPaymentMethodForm ul li');
for (let li of payment_ul) {
	var nameClass= $(li).hasClass( "selected" )
		if(nameClass)
			{			
			var input_pay = $("input[name='paymentmothed']").parent('label');
			for(let label_pay of input_pay)
				{
				if($(label_pay).text().trim() ==$(li).find('.text').text().trim() )
					{
					$(label_pay).find('input').attr('checked',true)
					
					
					}
				
				}
			}

}
//payment method in change value
$("input[name='paymentmothed']").click(function(){
	
	
		$("#selectPaymentMethodForm select.form-control").val($(this).val());
		
	
});



$('body').on('click', function (e) {

	 $('.bootstrap-select').removeClass('open');
	 
	});	


if($('.pageLabel-orders').length){
	var headerOrder = $('.account-section-header').text();
	var numproduct = $('.pagination-bar-results').html()
	$('.ProductNumberFound').html(numproduct)
	$('.titleotherPages').text(headerOrder)
	
}





let label_pass= $('.navmobileConetnt .password-holder .control-label').text().trim();
if($('.confirmPass').length){
	let labelCheck=$('.confirmPass label').text().trim()
$('.confirmPass').attr("placeholder", labelCheck);}
$('[id*="password"]').attr("placeholder", label_pass);

let label_passcheck= $('.checkPwd label').text().trim();
$('[id*="register\\.checkPwd"]').attr("placeholder", label_passcheck);


if($('.pwdprofile').length){
let label_passcheckprofile= $('.pwdprofile label').text().trim();
$('[id*="profile\\.pwd"]').attr("placeholder", label_passcheckprofile);
}

//new password page


if($('.currentPassword').length){
	let label_passcheckcurrent= $('.currentPassword label').text().trim();
	$('[id*="currentPassword"]').attr("placeholder", label_passcheckcurrent);
	}
if($('.newPassword').length){
	let label_passcheckNew= $('.newPassword label').text().trim();
	$('[id*="newPassword"]').attr("placeholder", label_passcheckNew);
	}
if($('.checkNewPassword').length){
	let label_passcheckcheck= $('.checkNewPassword label').text().trim();
	$('[id*="checkNewPassword"]').attr("placeholder", label_passcheckcheck);
	}


//popup form_login in home_page
$(".registration .login_action").click(function(){
	$(".darkback").fadeIn("300", function () {$('.darkback').removeClass("hidden");});
	$(".login_box").fadeIn("300", function () {
		$('.login_box').removeClass("hidden");
		
	});
	
	$('.registration').addClass("click_border");
	
	});

$('.darkback').click(function(){
	$(".login_box").fadeOut("medium").fadeOut("medium", function () {
		$('.login_box').addClass("hidden");
		$('.darkback').addClass("hidden");
		
	});
	$('.registration').removeClass("click_border");
});

if($('.page-productGrid').length){
	
	var numproduct = $('.pagination-bar-results').html()
	$('.ProductNumberFound').html(numproduct)
}
$('#register\\.dob').datepicker({
    language: 'en',
    dateFormat: 'dd/mm/yyyy'
});
if($('#profile\\.dob').length){
	
	var dateSelect =$('#profile\\.dob').val()
$('#profile\\.dob').datepicker({
    language: 'en',
    dateFormat: 'dd/mm/yyyy',
    startDate:new Date($('#profile\\.dob').val())
});

	
$(".IconDate").click(function () {

	$('#register\\.dob').datepicker("show");
});
}

var ias = $.ias({
	  container:  ".product__listing.product__grid",
	  item:       ".product-item",
	  delay: 500,
	  pagination: ".pagination-bar.top .pagination",
	  next:       ".pagination-bar.top .pagination-next a"
	});
ias.extension(new IASSpinnerExtension());
ias.extension(new IASTriggerExtension({
	text: 'Load More',
	offset: '10',
	html: '<div class="ias-trigger ias-trigger-next" ><a class="btn btn-primary btn-block">{text}</a></div>'// override text when no pages left
})); // shows a trigger after page 3
ias.extension(new IASNoneLeftExtension({
text: '',
}));
$('.js-update-entry-quantity-input').attr('autocomplete','off');
ias.on('rendered', function() {

	
	ACC.product.bindToAddToCartForm();
	ACC.product.bindToAddToCartStorePickUpForm();
	ACC.product.enableStorePickupButton();
	ACC.product.enableAddToCartButton();
	lazyloadingAddtocart=false;
  $('.addproductitem').on('submit', function (e) {

      e.preventDefault();
      var formelement = $(this)
      	var url = $(this).attr('action');
      $.ajax({
    	   url: url,
		   type: 'POST',
	
		  
		   data: formelement.serialize() ,
		   async: false,
        success: function (data) {
        
          ACC.product.displayAddToCartPopup(data,formelement)
        }
      });

    });
	$(".js-enable-btn").click(function(){
		 
	       
		var select_this_cart=$(this).parents(".actions")
		
		setTimeout(function(){
			 var html_content_add_to_car_box=$("#addedtocartno").find('.cart_popup_error_msg').text();
		 //console.log(html_content_add_to_car_box.length)
		 	 if(html_content_add_to_car_box.length === 0  ||html_content_add_to_car_box.length ==7){
		 		
		 		var add_to_cart= select_this_cart.find('.addedtocart')
		 		 
			 	$(add_to_cart).fadeIn("300", function () {$(add_to_cart).css("display","block");});  
			      setTimeout(() =>$(add_to_cart).fadeOut("medium"), 2000);
		 	 }else if (html_content_add_to_car_box.length ===49 ||html_content_add_to_car_box.length == 34 )
		 		 {
		 		var add_to_cart= select_this_cart.find(".addedtocarterror");
			 	$(add_to_cart).fadeIn("300", function () {$(add_to_cart).css("display","block");});  
			      setTimeout(() =>$(add_to_cart).fadeOut("medium"), 3000); 
		 		 
		 		 } else if (html_content_add_to_car_box.length ===86 ||html_content_add_to_car_box.length ==70){
		 			 var add_to_cart= select_this_cart.find(".addedtocartinfo");
				 	$(add_to_cart).fadeIn("300", function () {$(add_to_cart).css("display","block");});  
				      setTimeout(() =>$(add_to_cart).fadeOut("medium"), 5000); 
		 			 
		 		 }

		}, 250);
		
		
	});
//	if($('.stock-0.selected').length){
//		$('.stock-0.selected').each(function() {
//				var list = $(this).parents('.list');
//			$(list).find('.size_change').each(function(index) {
//			
//				if($(this).data('stock') > 0){
//					$(this).trigger('click');
//					return false;
//					
//					
//				}
//				
//			});
//
//		
//		});
//		}

});
ias.on('load', function(event) {
	TweenMax.staggerTo('.slice', 1, {
		opacity : 1,
		repeat : 1000
	}, 0.1);
});








$('.link_ar').click(function(){ 
    $('.lang-selector').val('ar');
    $(".lang-form").submit();
    
})
$('.link_en').click(function(){ 
	$('.lang-selector').val('en');
    $(".lang-form").submit();
 
    
})




if($('.page-update-profile').length){
	
	var valueStatus= $('#profile\\.maritalStatus').val();
	
	if(valueStatus=='MARRIED'){
		$('input[type=radio][name=radioMaritalprofile][value="MARRIED"]').prop('checked', true);
	}else if(valueStatus=='SINGLE'){
		$('input[type=radio][name=radioMaritalprofile][value="SINGLE"]').prop('checked', true);
		
	}
	 if($('#involvedInLoyaltyProgram').is(":checked")) {
	    	$('#involvedInLoyalty').prop('checked', true);
	    }
	    else{
	    	$('#involvedInLoyalty').prop('checked', false);

	    	
	    }
	
	
}











if($('.register__section').length){
	
	var textError=$('.checkboxMartal .has-error').find('.help-block').html();
	var valueStatus= $('#register\\.maritalStatus').val();
	
	if(valueStatus=='MARRIED'){
		$('input[type=radio][name=radioMarital][value="MARRIED"]').prop('checked', true);
	}else if(valueStatus=='SINGLE'){
		$('input[type=radio][name=radioMarital][value="SINGLE"]').prop('checked', true);
		
	}
	$('.checkboxMartal .help-block.custom').html(textError)
	$('.checkboxMartal .help-block.custom').removeClass('hidden')
	
	
}









$(".registration .login_action").click(function(){
	$(".darkback").fadeIn("300", function () {$('.darkback').removeClass("hidden");});
	$(".login_box,.tab_login_header").fadeIn("300", function () {
		$('.login_box').removeClass("hidden");
		$('.tab_login_header').removeClass("hidden");
		
	});
	
	$('.registration').addClass("click_border");
	
	});

$('.darkback').click(function(){
	
	$(".login_box,.tab_login_header").fadeOut("medium").fadeOut("medium", function () {
		$('.tab_login_header').addClass("hidden");
		$('.login_box').addClass("hidden");
		
		
		$('.darkback').addClass("hidden");
		
	});
	$('.registration').removeClass("click_border");
});
			




$('body').on('click', '.google_action', function() {

	 $(".abcRioButtonContentWrapper").click()
});




$('body').on('click', '.myAccountList', function() {
	
	$(".darkback").fadeIn("300", function () {$('.darkback').removeClass("hidden");});
	$(".NAVcompONENT,.accNavComponent__child-wrap,.tab_profile_header").fadeIn("300", function () {
		$('.NAVcompONENT').removeClass("display-none");
		$('.accNavComponent__child-wrap').removeClass("display-none");
		$('.tab_profile_header').removeClass("display-none");
		
	});
	
	
	
	});

$('.darkback').click(function(){
	
	$(".login_box,.tab_login_header,.NAVcompONENT,.tab_profile_header").fadeOut("medium").fadeOut("medium", function () {
		$('.tab_login_header').addClass("hidden");
		$('.NAVcompONENT').addClass("display-none");
		$('.tab_profile_header').addClass("display-none");
		$('.accNavComponent__child-wrap').addClass("display-none");
		$('.login_box').addClass("hidden");
		$('.darkback').addClass("hidden");
		
	});
	$('.registration').removeClass("click_border");
});



$('input[type=radio][name=radioMarital]').change(function() {	
	$('#register\\.maritalStatus').val(this.value)

   
   
   $('select').selectpicker();
});

$('input[type=radio][name=radioMaritalprofile]').change(function() {	
	$('#profile\\.maritalStatus').val(this.value)

   
   
   $('select').selectpicker();
});



if($('.language-en').length){
	$('input[type=radio][name=radioLang][value="EN"]').prop('checked', true);	
}
else{
	$('input[type=radio][name=radioLang][value="AR"]').prop('checked', true);	
}



$('input[type=radio][name=radioLang]').change(function() {	
if(this.value == 'EN'){
	
	 $('.lang-selector').val('en');
	    $(".lang-form").submit();
}
else if(this.value == 'AR'){
	 $('.lang-selector').val('ar');
	    $(".lang-form").submit();
	
}
});





$('#switchNationalform').change(function() {
	

    if($(this).is(":checked")) {
        $('.FormNational').css('display','inline-block')
    }
    else{
    	
    	   $('.FormNational').css('display','none')
    }
          
});


$('#involvedInLoyalty').change(function() {
	
	

    if($(this).is(":checked")) {
    	$('#involvedInLoyaltyProgram').prop('checked', true);
    }
    else{
    	$('#involvedInLoyaltyProgram').prop('checked', false);

    	
    }
          
});


$('#registerChkTermsCondition').change(function() {
	$('#registerChkTermsConditions').click();
	

    if($(this).is(":checked")) {
    	$('.registerBtn').prop("disabled", false);
    }
    else{
    	
    	$('.registerBtn').prop("disabled", 'disabled');
    	
    }
          
});







$(".mobile-action .title").click(function(){
	$(".mobile-action .footer__nav--links").removeClass('activefooter');
	$('.mobile-action').find(".title span").removeClass('fa-minus').addClass('fa-plus')
	$(this).parent('.footer__nav--container').find('.footer__nav--links').toggleClass('activefooter');
	$(this).parent('.footer__nav--container').find('.title span').removeClass('fa-plus').addClass('fa-minus')
	});

$('.changeLocation ').click(function(){
	var CityCode=$('.defaultCity').val();
	var AreaCode=$('.defaultArea').val();
	var CountryCode=$('.defaultCountry').val();
	$('.select-city').addClass('hidden')
	$('.default-view').removeClass('hidden');
	var urlArea=ACC.config.encodedContextPath+'/misc/city/'+CityCode+'/areas';
	
	   $.ajax({
		   type : "GET",
			url : urlArea,
			dataType : "json",
           success: function (res) {
              for(let i=0;i<res.data.length;i++){
            	  var li= res.data[i];
            	  
            	  $('.itemListPlaces').append("<li class='itemserach'><a  href='javaScript:;' data-code='"+li.code+"'>"+li.name +"</a></li>");
            	  
              } 
           },
           error: function (err) {
             console.log(err)
           }

       });
	
	$(".deliverypopup").fadeOut("medium").fadeOut("medium", function () {
		$(".deliverypopup").removeClass('hidden');
		
	})
});


$('.deliverypopup').click(function(){
	

	$(".deliverypopup").fadeIn("medium").fadeIn("medium", function () {
		$(".deliverypopup").addClass('hidden');
		
	})
	
})

$(".popupBoxdelivery").click(function(e) {
   // Do something
   e.stopPropagation();
});


$('#searchInputText').keyup(function(){
	 var input, filter, ul, li, a, i, txtValue;
	    input = document.getElementById("searchInputText");
	    filter = input.value.toUpperCase();
	    ul = document.getElementById("itemListdata");
	    li = ul.getElementsByTagName("li");
	    for (i = 0; i < li.length; i++) {
	        a = li[i].getElementsByTagName("a")[0];
	        txtValue = a.textContent || a.innerText;
	        if (txtValue.toUpperCase().indexOf(filter) > -1) {
	            li[i].style.display = "";
	        } else {
	            li[i].style.display = "none";
	        }
	    }
});

$('#searchInputTextCity').keyup(function(){
	 var input, filter, ul, li, a, i, txtValue;
	    input = document.getElementById("searchInputTextCity");
	    filter = input.value.toUpperCase();
	    ul = document.getElementById("itemListdataCity");
	    li = ul.getElementsByTagName("li");
	    for (i = 0; i < li.length; i++) {
	        a = li[i].getElementsByTagName("a")[0];
	        txtValue = a.textContent || a.innerText;
	        if (txtValue.toUpperCase().indexOf(filter) > -1) {
	            li[i].style.display = "";
	        } else {
	            li[i].style.display = "none";
	        }
	    }
});


$("#itemListdata").on("click", ".itemserach", function() {
	 var dataAreaCode= $(this).find('a').data().code;
	 var CityCode=$('.defaultCity').val();
	 $('.defaultArea').val(dataAreaCode);
	 var urlpost = ACC.config.encodedContextPath+'/misc/delivery-location/save';
	 $.post(urlpost, { 'cityCode': CityCode, 'areaCode' : dataAreaCode}).done(function(data) {
		 location.reload();
	  });
	 
	 

	 
	 
	 
	 
	 
	});


$('.btnChange').click(function(e) {
	$('.select-city').removeClass('hidden')
	$('.default-view').addClass('hidden');
	var CountryCode=$('.defaultCountry').val();
	 $('.itemListPlaces').empty();
		var urlcity=ACC.config.encodedContextPath+'/misc/country/'+CountryCode+'/cities';


	   $.ajax({
		   type : "GET",
			url : urlcity,
			dataType : "json",
           success: function (res) {
              for(let i=0;i<res.data.length;i++){
            	  
            	  var li= res.data[i];
            	  
            	  $('.itemListPlaces').append("<li class='itemserachcity'><a  href='javaScript:;' data-code='"+li.code+"'>"+li.name +"</a></li>");
            	  
              } 
           },
           error: function (err) {
             console.log(err)
           }

       });
});



$("#itemListdataCity").on("click", ".itemserachcity", function() {
	
	 var CityCode= $(this).find('a').data().code;
	 var CityName=$(this).find('a').text();
	 $('.title-select-value').html(CityName)
	 $('.defaultCity').val(CityName);
	 $('.select-city').addClass('hidden')
		$('.default-view').removeClass('hidden');
	 $('.itemListPlaces').empty();
	 var urlArea=ACC.config.encodedContextPath+'/misc/city/'+CityCode+'/areas';

	 
	 
	   $.ajax({
		   type : "GET",
			url : urlArea,
			dataType : "json",
           success: function (res) {
              for(let i=0;i<res.data.length;i++){
            	  var li= res.data[i];
            	  
            	  $('.itemListPlaces').append("<li class='itemserach'><a  href='javaScript:;' data-code='"+li.code+"'>"+li.name +"</a></li>");
            	  
              } 
           },
           error: function (err) {
             console.log(err)
           }

       });
	 
	 
	 
	 
	 
})
$('.closepopupdelivery').click(function(e) {
	$('.deliverypopup').click()
	
})


$('.AccountMobile_action').click(function(){
	
	
	
})













$('body').on('click', '.AccountMobile_action', function() {
	
	$(".darkback").fadeIn("300", function () {$('.darkbackMobile').removeClass("hidden");});
	$(".mobileAccountBox ,.AccountNavmobile .tab_profile_header").fadeIn("300", function () {
		$('.mobileAccountBox').removeClass("hidden");
		
		$('.AccountNavmobile .tab_profile_header').removeClass("hidden");
		
	});
	
	
	
	});

$('.darkbackMobile').click(function(){
	
	$(".mobileAccountBox ,.AccountNavmobile .tab_profile_header").fadeOut("medium").fadeOut("medium", function () {
		$('.mobileAccountBox').addClass("hidden");
		$('.AccountNavmobile .tab_profile_header').addClass("hidden");

		
	});
	
});


//filter plp

$('.js-facet-name').click(function(){
	
	if($(this).find('.clickarrowFilter .fa-plus').length){
		$(this).find('.clickarrowFilter .fa-plus').removeClass('fa-plus').addClass('fa-minus');
		
		$(this).parent().find('.facet__list').slideToggle("medium");
		
	}else{
		
		
		$(this).find('.clickarrowFilter .fa-minus').removeClass('fa-minus').addClass('fa-plus');
		$(this).parent().find('.facet__list').slideToggle("medium");

	}
	
	
	
	
})


$('.plus').click ( function () {
	
	 var div_item =$(this).closest('.item__list--item');
	 var total_data= $(this).parent().parent().parent().parent();
	
	
	var form = $(this).closest('form');
	form.find('.minus').removeClass('not_allow_a');
	var className= '.'+$(form).attr('class');
	var max_qty=form.find('input[name= quantity ]').attr('max');
	
	var productCode = form.find('input[name=quantity]').val();
	var initialCartQuantity = form.find('input[name=initialQuantity]').val();
	
	var newCartQuantity =  parseFloat(initialCartQuantity) + 1;
	
	
	if(max_qty >= newCartQuantity){
		
	form.find('input[name= quantity ]').val(newCartQuantity);
	
	
	//form.find('input[name= quantity ]').val(newCartQuantity);
	var cal_total= div_item.find('.item__price').html().trim().replace(/[^0-9.]/g, "");
	
	var totalVal=parseFloat(cal_total) * newCartQuantity
	div_item.find('.js-item-total').html('AED '+ totalVal.toFixed(2))
	form.find('input[name=initialQuantity]').val(newCartQuantity);
	if(newCartQuantity == max_qty){
		 
		   form.find('.plus').addClass('not_allow_a');

	   }
	
	
	var url = form.attr('action');
	//console.log(form.serialize())
	$.ajax({
	   url: url,
	   type: 'POST',
	   data: form.serialize() ,
	   async: true,
	   success: function (data) {
	   var total_data= $(data).find('.js-cart-totals').html();
	   
	   $('.js-cart-totals').html(total_data);
	 
	   ACC.minicart.updateMiniCartDisplay();
	   
	  
	   }
	
	
 });
}
	 
    });
$('.minus').click (function () { 
	 var div_item =$(this).closest('.item__list--item');
	 var total_data= $(this).parent().parent().parent().parent();
	var form = $(this).closest('form');
	var className= '.'+$(form).attr('class');
	 form.find('.plus').removeClass('not_allow_a');
	var min_qty =1;
	var productCode = form.find('input[name=quantity]').val();
	var initialCartQuantity = form.find('input[name=initialQuantity]').val();
	
	var newCartQuantity =  parseFloat(initialCartQuantity) - 1;
	
	if(newCartQuantity >= 1){

	form.find('input[name= quantity ]').val(newCartQuantity);
	var cal_total= div_item.find('.item__price').html().trim().replace(/[^0-9.]/g, "");
	
	var totalVal=parseFloat(cal_total) * newCartQuantity
	div_item.find('.js-item-total').html('AED '+ totalVal.toFixed(2))
	
	var url = form.attr('action');
	
	if(newCartQuantity  == min_qty){
		 
		   form.find('.minus').addClass('not_allow_a');

	   }
	form.find('input[name=initialQuantity]').val(newCartQuantity)
	//console.log(form.serialize())
	$.ajax({
	   url: url,
	   type: 'POST',
	   data: form.serialize() ,
	   async: true,
	   success: function (data) {
		   var total_data= $(data).find('.js-cart-totals').html();
		  // var qty= form.find('input[name=initialQuantity]').val();
		   //var total_item_temp= $(data).find(className).parent().parent();
		   //var total_item= total_item_temp.find('.js-item-total').html()
		  // div_item.find('.js-item-total').html(total_item)
	   $('.js-cart-totals').html(total_data);
		   ACC.minicart.updateMiniCartDisplay();
	   
	   
	   //  ACC.product.displayAddToCartPopup(data,formelement)
	   }
	
	
 });
	
	}
//    	var form = $(this).closest('form');
//    	var productCode = form.find('input[name=productCode]').val();
//    	var initialCartQuantity = form.find('input[name=initialQuantity]').val();
//    	var newCartQuantity = parseFloat(initialCartQuantity) - 1;
//    	form.find('input[name=quantity]').val(newCartQuantity);
//    	
//    	  form.submit();
    	  });

var myset_time_out;
$(".js-update-entry-quantity-input").on("keyup", function() {
	clearTimeout(myset_time_out);
	var w_check_value=true;
	var min_qty =1;
	 var div_item =$(this).closest('.item__list--item');
	
	 var total_data= $(this).parent().parent().parent().parent();
	
	var final_value=parseInt($(this).val());
	var form = $(this).closest('form');
	
	var className= '.'+$(form).attr('class');
	var max_qty=form.find('input[name= quantity ]').attr('max');
	   var value=$(this).val();
	   if(value =='0'){$(this).val(1);final_value=1}
	   var input =/^\d*$/.test(value)
	   if(!value.length){ 
		   var input =this;
		   w_check_value=false;
		 
		   myset_time_out = setTimeout(function(){
			   $(input).val(1);
				var cal_total= div_item.find('.item__price').html().trim().replace(/[^0-9.]/g, "");
				
				var totalVal=parseFloat(cal_total) * 1
				div_item.find('.js-item-total').html('AED '+ totalVal.toFixed(2))
				form.find('input[name=initialQuantity]').val(1);
			   var url = form.attr('action');
			   if(final_value-1 <= min_qty){
					 
				   form.find('.minus').addClass('not_allow_a');

			   }
			 //  form.find('input[name=initialQuantity]').val(1)
				$.ajax({
				   url: url,
				   type: 'POST',
				   data: form.serialize() ,
				   async: true,
				   success: function (data) {
					   var total_data= $(data).find('.js-cart-totals').html();
					   //var qty= 1;
					   //var total_item_temp= $(data).find(className).parent().parent();
					   //var total_item= total_item_temp.find('.js-item-total').html()
					   //div_item.find('.js-item-total').html(total_item)
				   $('.js-cart-totals').html(total_data);
					   ACC.minicart.updateMiniCartDisplay();
				 
				   
				   //  ACC.product.displayAddToCartPopup(data,formelement)
				   }
				
				
			 });
			   
			   
			   
			   
		 }, 2000);}
	   if(value.length && !input ){
		   $(this).val(1);
		   final_value=1;  }
	   
	   var value_qty = parseInt(value)
	   if(value_qty > parseInt(max_qty) ){
		   $(this).val(parseInt(max_qty));
		   final_value=parseInt(max_qty)
	   }
	   if(w_check_value){
		 //  console.log(final_value)
		  
		   
		   
		   $(input).val(final_value);
			var cal_total= div_item.find('.item__price').html().trim().replace(/[^0-9.]/g, "");
			
			var totalVal=parseFloat(cal_total) * final_value
			div_item.find('.js-item-total').html('AED '+ totalVal.toFixed(2))
			form.find('input[name=initialQuantity]').val(final_value);
			 if(final_value-1 <= min_qty){
				 
				   form.find('.minus').addClass('not_allow_a');

			   }else{
				   
				   form.find('.minus').removeClass('not_allow_a');
			   }
			   if(final_value +1 >= parseInt(max_qty)){
					 
				   form.find('.plus').addClass('not_allow_a');

			   }else{
				   
				   form.find('.plus').removeClass('not_allow_a');
			   }
		   var url = form.attr('action');
			//console.log(form.serialize())
		   form.find('input[name=initialQuantity]').val(final_value)
			$.ajax({
			   url: url,
			   type: 'POST',
			   data: form.serialize() ,
			   async: false,
			   success: function (data) {
				   var total_data= $(data).find('.js-cart-totals').html();
				   //var qty= final_value;
				   //var total_item_temp= $(data).find(className).parent().parent();
				   //var total_item= total_item_temp.find('.js-item-total').html()
				   //div_item.find('.js-item-total').html(total_item)
			   $('.js-cart-totals').html(total_data);
				   ACC.minicart.updateMiniCartDisplay();
			   //form.find('input[name=initialQuantity]').val(parseInt(qty)-1)
			  
			   //  ACC.product.displayAddToCartPopup(data,formelement)
			   }
			
			
		 });
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
	   }
	   
	   
	});


if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
	   $('select').selectpicker('mobile');
	}



//map in locationpicker

var lat=23.58238473915374;
var long=54.065219534058286;

if($('#latitude').val()){
	
	lat=$('#latitude').val()					
}
if($('#longitude').val()){
	
	long=$('#longitude').val()					
}

 $('#us2').locationpicker({
    location: {
            latitude: lat,
            longitude: long
        },
        radius: 0,
    zoom:8,
        inputBinding: {
      latitudeInput: $('#latitude'),
        longitudeInput: $('#longitude'),
            locationNameInput: $('#us2-address')
        },
        enableAutocomplete: true,
     
       
   	                    });		




 var checkarea;
	$(document).on("change",'#cityId', function (){
		$('#us2-address').val('');
		 checkarea=false;
		var val_city_change=$('#cityId').val();
		area_zoom=false;
		city_zoom=true;
		$.ajax({
			 url: "https://maps.googleapis.com/maps/api/geocode/json?address="+val_city_change+"&key=AIzaSyBDnqcqkmG9MqNZR6QKsB8V-mg-NNa71t8",
      
         success: function( data ) {
            
             
             $('#us2').locationpicker({
                 location: {
                         latitude: data.results[0].geometry.location.lat,
                         longitude: data.results[0].geometry.location.lng
                     },
                     radius: 0, zoom: 1,
                     inputBinding: {
                   latitudeInput: $('#latitude'),
                     longitudeInput: $('#longitude'),
                         radiusInput: $('#us2-radius'),
                         locationNameInput: $('#us2-address')
                     },
                     enableAutocomplete: true,
                  
                    
                	                    });	
         }
     });
	
	});
	
	
	$(document).on("change",'#areaId', function (){
		checkarea=true;
		var val_area_change=$('#areaId option:selected').text();
		area_zoom=true;
		city_zoom=false;
		$.ajax({
			 url: "https://maps.googleapis.com/maps/api/geocode/json?address="+val_area_change+ '%20'+ $('#cityId').val()+"&key=AIzaSyBDnqcqkmG9MqNZR6QKsB8V-mg-NNa71t8",
      
         success: function( data ) {
         	
             
             $('#us2').locationpicker({
                 location: {
                         latitude: data.results[0].geometry.location.lat,
                         longitude: data.results[0].geometry.location.lng
                     },
                     radius: 0, zoom: 1,
                     inputBinding: {
                   latitudeInput: $('#latitude'),
                     longitudeInput: $('#longitude'),
                         radiusInput: $('#us2-radius'),
                         locationNameInput: $('#us2-address')
                     },
                     enableAutocomplete: true,
                  
                    
                	                    });	
         }
     });
		
	
	});
	$('.reLoadOrder').click ( function () {
	location.reload();});
	
	
	
	
	
	
	
	//main menu Direction Desktop
	$(document).ready(MenuDirection);

	 $(window).resize(MenuDirection);
	   jQuery(window).on('resize', function () {
		   MenuDirection()
	   })
	 function MenuDirection(){
		 if (jQuery(window).width() > 1023){
			 
		 $(' .firstLevelMenu').each(function() {
			 var item = $(this)
				var of = item.offset(), // this will return the left and top
			    left = of.left, // this will return left 
			    right = $(window).width() - left - item.width();
				
				if($('.language-en').length){
					
					
				// you can get right by calculate
					if( left > 284){item.find('.levelTwoContainer').addClass('rightSideMenu') }else
					{item.find('.levelTwoContainer').removeClass('rightSideMenu')}
				if(right < 568){   
					
					item.find('.levelThreeContainer').addClass('leftSideMenu')
				
			}else{
				$(this).find('.levelThreeContainer').removeClass('leftSideMenu')
				
			}
				}
				
			
				})
		 }
	 }
	 $(document).on(' click', '.firstLevelItem', function(){ 
		    if($(this).parent().find('.levelTwoContainer').length){
		    	if($(this).find('.fa-chevron-down').length){
			    	$(this).find('.fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-up');}
			    	else{
			    		$(this).find('.fa-chevron-up').removeClass('fa-chevron-up').addClass('fa-chevron-down');
			    		
			    	}
		    	$(this).parent().find('.levelTwoContainer').slideToggle('slow');
		    }
		});
	
	
	 $(document).on(' click', '.ChildLevelTwoItem', function(){ 
		    if($(this).parent().find('.levelThreeContainer').length){
		    	if($(this).find('.fa-chevron-down').length){
		    	$(this).find('.fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-up');}
		    	else{
		    		$(this).find('.fa-chevron-up').removeClass('fa-chevron-up').addClass('fa-chevron-down');
		    		
		    	}
		    	$(this).parent().find('.levelThreeContainer').slideToggle('slow');
		    }
		});	
	
	 
		
		
		$('.js-popup-wishlist').click(function(){
			
			
			$(".wishlistLogin").fadeOut("medium").fadeOut("medium", function () {
				$(".wishlistLogin").removeClass('hidden');
				
			})
			
		})

	 $('').click(function(){
	

	
})
 $( 'body' ).on( 'click', '.wishlistLogin', function () {
	 
	 $('.wishlistLogin').fadeIn("medium").fadeIn("medium", function () {
			$('.wishlistLogin').addClass('hidden');
			
		})
 });


	 $( 'body' ).on( 'click', '.wishlist_popup', function (e) {
   // Do something	
	
  e.stopPropagation();
});
			$( 'body' ).on( 'click', '.Close_wishlist', function () {
			$('.wishlistLogin').click()
			
		})
	 
