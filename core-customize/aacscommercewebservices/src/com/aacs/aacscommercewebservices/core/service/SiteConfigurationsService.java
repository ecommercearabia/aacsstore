/**
 *
 */
package com.aacs.aacscommercewebservices.core.service;

import java.util.Optional;

import com.aacs.aacscommercewebservices.core.configuration.ContactUsConfigurationData;
import com.aacs.aacscommercewebservices.core.configuration.site.AddressConfigurationData;
import com.aacs.aacscommercewebservices.core.configuration.site.RegistrationConfigurationData;
import com.aacs.aacscommercewebservices.core.configuration.site.SiteConfigurationData;


/**
 * @author mbaker
 *
 */
public interface SiteConfigurationsService
{
	public Optional<RegistrationConfigurationData> getRegistrationConfigurationForCurrentSite();

	public Optional<AddressConfigurationData> getAddressConfigurationForCurrentSite();

	public Optional<SiteConfigurationData> getSiteConfigurationForCurrentSite();

	public Optional<ContactUsConfigurationData> getContactUsConfigurationForCurrentSite();

}
