/**
 *
 */
package com.aacs.aacscommercewebservices.core.service;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Optional;

import com.aacs.aacsfacades.customer.SiteDeliveryCityAreaData;
import com.aacs.facades.user.delivery.DeliveryLocationService;


/**
 * @author monzer
 *
 */
public interface DeliveryLocationOCCService extends DeliveryLocationService
{
	Optional<SiteDeliveryCityAreaData> getDeliveryLocationDataByCurrentSite();

	Optional<SiteDeliveryCityAreaData> getDeliveryLocationData(CMSSiteModel siteUid);
}
