/**
 *
 */
package com.aacs.aacscommercewebservices.core.service.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.aacs.aacscommercewebservices.core.configuration.ContactUsConfigurationData;
import com.aacs.aacscommercewebservices.core.configuration.FieldConfigurationData;
import com.aacs.aacscommercewebservices.core.configuration.site.AddressConfigurationData;
import com.aacs.aacscommercewebservices.core.configuration.site.RegistrationConfigurationData;
import com.aacs.aacscommercewebservices.core.configuration.site.SiteConfigurationData;
import com.aacs.aacscommercewebservices.core.service.SiteConfigurationsService;
import com.aacs.aacsloyaltyprogramfacades.facades.LoyaltyPaymentFacade;
import com.aacs.aacsotp.context.OTPProviderContext;


/**
 * @author mbaker
 *
 */
public class DefaultSiteConfigurationsService implements SiteConfigurationsService
{
	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "loyaltyPaymentFacade")
	private LoyaltyPaymentFacade loyaltyPaymentFacade;

	@Resource(name = "otpProviderContext")
	protected OTPProviderContext otpProviderContext;

	@Override
	public Optional<SiteConfigurationData> getSiteConfigurationForCurrentSite()
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		if (currentSite == null)
		{
			return Optional.empty();
		}
		final SiteConfigurationData siteConfigurationData = new SiteConfigurationData();
		final RegistrationConfigurationData registrationConfigurationData = getRegistrationConfiguration(currentSite);
		final AddressConfigurationData addressConfigurationData = getAddressConfiguration(currentSite);
		siteConfigurationData.setAddressConfiguration(addressConfigurationData);
		siteConfigurationData.setRegistrationConfiguration(registrationConfigurationData);
		siteConfigurationData.setLoyaltyEnabled(loyaltyPaymentFacade.isEnabledOnStoreByCurrentBaseStore());
		siteConfigurationData.setOtpEnabled(
				!CollectionUtils.isEmpty(currentSite.getOtpProviders()) && otpProviderContext.getProvider(currentSite).isPresent());
		siteConfigurationData.setShowReOrderButton(currentSite.isShowReOrderButton());
		siteConfigurationData.setEnablePickUpInStoreOnPDP(currentSite.isEnablePickUpInStoreOnPDP());
		return Optional.ofNullable(siteConfigurationData);
	}


	@Override
	public Optional<RegistrationConfigurationData> getRegistrationConfigurationForCurrentSite()
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		if (currentSite == null)
		{
			return Optional.empty();
		}


		return Optional.ofNullable(getRegistrationConfiguration(currentSite));
	}

	@Override
	public Optional<ContactUsConfigurationData> getContactUsConfigurationForCurrentSite()
	{

		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		if (currentSite == null)
		{
			return Optional.empty();
		}

		return Optional.ofNullable(getContactUsConfiguration(currentSite));
	}

	/**
	 * @param currentSite
	 * @return
	 */
	private ContactUsConfigurationData getContactUsConfiguration(final CMSSiteModel currentSite)
	{
		final ContactUsConfigurationData contactUsConfigurationData = new ContactUsConfigurationData();
		contactUsConfigurationData.setPhoneNumber(currentSite.getContactUsNumber());
		contactUsConfigurationData.setEmailAddress(currentSite.getContactUsEmailAddress());
		contactUsConfigurationData.setPoBox(currentSite.getContactUsPOBox());
		return contactUsConfigurationData;
	}


	public Optional<AddressConfigurationData> getAddressConfigurationForCurrentSite()
	{

		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		if (currentSite == null)
		{
			return Optional.empty();
		}

		return Optional.ofNullable(getAddressConfiguration(currentSite));
	}


	/**
	 * @param currentSite
	 * @return
	 */
	private AddressConfigurationData getAddressConfiguration(final CMSSiteModel currentSite)
	{
		final AddressConfigurationData addressConfigurationData = new AddressConfigurationData();
		final FieldConfigurationData areaConfigurations = getAreaConfigurations(currentSite);
		final FieldConfigurationData buildingNameConfigurations = getBuildingNameConfigurations(currentSite);
		final FieldConfigurationData apartmentNumberConfigurations = getApartmentNumberConfigurations(currentSite);
		final FieldConfigurationData cityConfigurations = getCityConfigurations(currentSite);
		final FieldConfigurationData moreInstructionConfigurations = getMoreInstructionConfigurations(currentSite);
		final FieldConfigurationData floorNumberConfigurations = getFloorNumberConfigurations(currentSite);
		final FieldConfigurationData nearestLandmarkConfigurations = getNearestLandmarkConfigurations(currentSite);

		addressConfigurationData.setAreaConfigurations(areaConfigurations);
		addressConfigurationData.setBuildingNameConfigurations(buildingNameConfigurations);
		addressConfigurationData.setApartmentNumberConfigurations(apartmentNumberConfigurations);
		addressConfigurationData.setCityConfigurations(cityConfigurations);
		addressConfigurationData.setMoreInstructionConfigurations(moreInstructionConfigurations);
		addressConfigurationData.setFloorNumberConfigurations(floorNumberConfigurations);
		addressConfigurationData.setNearestLandmarkConfigurations(nearestLandmarkConfigurations);

		return addressConfigurationData;
	}

	/**
	 * @param currentSite
	 * @return
	 */
	private FieldConfigurationData getNearestLandmarkConfigurations(final CMSSiteModel currentSite)
	{
		final FieldConfigurationData nearestLandmarkConfigurations = new FieldConfigurationData();
		nearestLandmarkConfigurations.setEnabled(currentSite.isNearestLandmarkAddressEnabled());
		nearestLandmarkConfigurations.setHidden(currentSite.isNearestLandmarkAddressHidden());
		nearestLandmarkConfigurations.setRequired(currentSite.isNearestLandmarkAddressRequired());
		return nearestLandmarkConfigurations;
	}

	/**
	 * @param currentSite
	 * @return
	 */
	private FieldConfigurationData getFloorNumberConfigurations(final CMSSiteModel currentSite)
	{
		final FieldConfigurationData floorNumberConfigurations = new FieldConfigurationData();
		floorNumberConfigurations.setEnabled(currentSite.isFloorNumberAddressEnabled());
		floorNumberConfigurations.setHidden(currentSite.isFloorNumberAddressHidden());
		floorNumberConfigurations.setRequired(currentSite.isFloorNumberAddressRequired());
		return floorNumberConfigurations;
	}

	/**
	 * @param currentSite
	 * @return
	 */
	private FieldConfigurationData getMoreInstructionConfigurations(final CMSSiteModel currentSite)
	{
		final FieldConfigurationData moreInstructionConfigurations = new FieldConfigurationData();
		moreInstructionConfigurations.setEnabled(currentSite.isMoreInstructionsAddressEnabled());
		moreInstructionConfigurations.setHidden(currentSite.isMoreInstructionsAddressHidden());
		moreInstructionConfigurations.setRequired(currentSite.isMoreInstructionsAddressRequired());
		return moreInstructionConfigurations;
	}

	/**
	 * @param currentSite
	 * @return
	 */
	private FieldConfigurationData getCityConfigurations(final CMSSiteModel currentSite)
	{
		final FieldConfigurationData cityConfigurations = new FieldConfigurationData();
		cityConfigurations.setEnabled(currentSite.isCityAddressEnabled());
		cityConfigurations.setHidden(currentSite.isCityAddressHidden());
		cityConfigurations.setRequired(currentSite.isCityAddressRequired());
		return cityConfigurations;
	}

	/**
	 * @param currentSite
	 * @return
	 */
	private FieldConfigurationData getApartmentNumberConfigurations(final CMSSiteModel currentSite)
	{
		final FieldConfigurationData apartmentNumberConfigurations = new FieldConfigurationData();
		apartmentNumberConfigurations.setEnabled(currentSite.isApartmentNumberAddressEnabled());
		apartmentNumberConfigurations.setHidden(currentSite.isApartmentNumberAddressHidden());
		apartmentNumberConfigurations.setRequired(currentSite.isApartmentNumberAddressRequired());
		return apartmentNumberConfigurations;
	}

	/**
	 * @param currentSite
	 * @return
	 */
	private FieldConfigurationData getBuildingNameConfigurations(final CMSSiteModel currentSite)
	{
		final FieldConfigurationData buildingNameConfigurations = new FieldConfigurationData();
		buildingNameConfigurations.setEnabled(currentSite.isBuildingNameAddressEnabled());
		buildingNameConfigurations.setHidden(currentSite.isBuildingNameAddressHidden());
		buildingNameConfigurations.setRequired(currentSite.isBuildingNameAddressRequired());
		return buildingNameConfigurations;
	}

	/**
	 * @param currentSite
	 * @return
	 */
	private FieldConfigurationData getAreaConfigurations(final CMSSiteModel currentSite)
	{
		final FieldConfigurationData areaConfigurations = new FieldConfigurationData();
		areaConfigurations.setEnabled(currentSite.isAreaAddressEnabled());
		areaConfigurations.setHidden(currentSite.isAreaAddressHidden());
		areaConfigurations.setRequired(currentSite.isAreaAddressRequired());
		return areaConfigurations;
	}

	/**
	 * @param currentSite
	 * @return
	 */
	private RegistrationConfigurationData getRegistrationConfiguration(final CMSSiteModel currentSite)
	{
		final RegistrationConfigurationData registrationConfigurationData = new RegistrationConfigurationData();
		final FieldConfigurationData birthDateConfig = getBirthDateConfig(currentSite);
		final FieldConfigurationData nationalityConfig = getNationalityConfig(currentSite);
		final FieldConfigurationData nationalIdConfig = getNationalIdConfig(currentSite);
		final FieldConfigurationData maritalStatusConfig = getMaritalStatusConfig(currentSite);

		registrationConfigurationData.setBirthDateConfigurations(birthDateConfig);
		registrationConfigurationData.setMaritalStatusConfigurations(maritalStatusConfig);
		registrationConfigurationData.setNationalityConfigurations(nationalityConfig);
		registrationConfigurationData.setNationalityIdConfigurations(nationalIdConfig);

		return registrationConfigurationData;
	}

	/**
	 * @param currentSite
	 * @return
	 */
	private FieldConfigurationData getBirthDateConfig(final CMSSiteModel currentSite)
	{
		final FieldConfigurationData birthDateConfig = new FieldConfigurationData();
		birthDateConfig.setEnabled(currentSite.isBirthOfDateCustomerEnabled());
		birthDateConfig.setHidden(currentSite.isBirthOfDateCustomerHidden());
		birthDateConfig.setRequired(currentSite.isBirthOfDateCustomerRequired());
		return birthDateConfig;
	}

	/**
	 * @param currentSite
	 * @return
	 */
	private FieldConfigurationData getNationalityConfig(final CMSSiteModel currentSite)
	{
		final FieldConfigurationData nationalityConfig = new FieldConfigurationData();
		nationalityConfig.setEnabled(currentSite.isNationalityCustomerEnabled());
		nationalityConfig.setHidden(currentSite.isNationalityCustomerHidden());
		nationalityConfig.setRequired(currentSite.isNationalityCustomerRequired());
		return nationalityConfig;
	}

	/**
	 *
	 * @param currentSite
	 * @return
	 */
	private FieldConfigurationData getNationalIdConfig(final CMSSiteModel currentSite)
	{
		final FieldConfigurationData nationalityIdConfig = new FieldConfigurationData();
		nationalityIdConfig.setEnabled(currentSite.isNationalityIdCustomerEnabled());
		nationalityIdConfig.setHidden(currentSite.isNationalityIdCustomerHidden());
		nationalityIdConfig.setRequired(currentSite.isNationalityIdCustomerRequired());
		return nationalityIdConfig;
	}

	/**
	 *
	 * @param currentSite
	 * @return
	 */
	private FieldConfigurationData getMaritalStatusConfig(final CMSSiteModel currentSite)
	{
		final FieldConfigurationData maritalStatusConfig = new FieldConfigurationData();
		maritalStatusConfig.setEnabled(currentSite.isCustomerMaritalStatusEnabled());
		maritalStatusConfig.setHidden(currentSite.isCustomerMaritalStatusHidden());
		maritalStatusConfig.setRequired(currentSite.isCustomerMaritalStatusRequired());
		return maritalStatusConfig;
	}
}
