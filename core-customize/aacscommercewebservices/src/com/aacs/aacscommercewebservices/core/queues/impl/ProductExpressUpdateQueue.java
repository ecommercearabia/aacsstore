/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscommercewebservices.core.queues.impl;

import com.aacs.aacscommercewebservices.core.queues.data.ProductExpressUpdateElementData;


/**
 * Queue for {@link com.aacs.aacscommercewebservices.core.queues.data.ProductExpressUpdateElementData}
 */
public class ProductExpressUpdateQueue extends AbstractUpdateQueue<ProductExpressUpdateElementData>
{
	// EMPTY - just to instantiate bean
}
