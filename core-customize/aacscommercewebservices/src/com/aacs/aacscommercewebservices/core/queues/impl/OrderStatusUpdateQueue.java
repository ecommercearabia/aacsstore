/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscommercewebservices.core.queues.impl;

import com.aacs.aacscommercewebservices.core.queues.data.OrderStatusUpdateElementData;


/**
 * Queue for {@link com.aacs.aacscommercewebservices.core.queues.data.OrderStatusUpdateElementData}
 */
public class OrderStatusUpdateQueue extends AbstractUpdateQueue<OrderStatusUpdateElementData>
{
	// EMPTY - just to instantiate bean
}
