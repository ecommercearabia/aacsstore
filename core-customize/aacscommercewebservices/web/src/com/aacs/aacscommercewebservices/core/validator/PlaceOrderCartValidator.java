/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscommercewebservices.core.validator;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercewebservicescommons.core.user.data.CartCheckOutValidatorData;

import javax.annotation.Resource;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.aacs.core.order.cart.exception.CartValidationException;
import com.aacs.core.order.cart.exception.enums.CartExceptionType;
import com.aacs.core.order.cart.service.CartValidationService;


/**
 * Default commerce web services cart validator. Checks if cart is calculated and if needed values are filled.
 */
public class PlaceOrderCartValidator implements Validator
{
	@Resource(name = "cartValidationService")
	private CartValidationService cartValidationService;

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return CartData.class.equals(clazz);
	}

	@Override
	public void validate(final Object target, final Errors errors)
	{
		final CartData cart = (CartData) target;

		if (!cart.isCalculated())
		{
			errors.reject("cart.notCalculated");
		}

		if (cart.getDeliveryMode() == null)
		{
			errors.reject("cart.deliveryModeNotSet");
		}

		if (cart.getPaymentInfo() == null && cart.getNoCardPaymentInfo() == null)
		{
			errors.reject("cart.paymentInfoNotSet");
		}

		final CartCheckOutValidatorData cartCheckOutValidatorData = new CartCheckOutValidatorData();
		cartCheckOutValidatorData.setValid(true);

		try
		{
			final boolean validateCartMaxAmount = getCartValidationService().validateCartMaxAmountByCurrentCart();
		}
		catch (final CartValidationException ex)
		{
			if (CartExceptionType.MAX_AMOUNT.equals(ex.getCartExceptionType()))
			{
				final String[] errorParm = ex.getErrorParm();
				cartCheckOutValidatorData.setValid(false);
				errors.reject("max2.cart.amount.value.error", errorParm, "Please note, The maximum order amount is {0} {1}");
			}
		}
		try
		{
			final boolean validateCartMaxAmount = getCartValidationService().validateCartMinAmountByCurrentCart();
		}
		catch (final CartValidationException ex)
		{
			if (CartExceptionType.MIN_AMOUNT.equals(ex.getCartExceptionType()))
			{
				final String[] errorParm = ex.getErrorParm();
				cartCheckOutValidatorData.setValid(false);
				errors.reject("min2.cart.amount.value.error", errorParm,
						"Min order amount is {0} {1}, please add {4} {1} more to continue with checkout.");
			}
		}
		try
		{
			final boolean validateCartDeliveryTypeByCurrentCart = getCartValidationService().validateCartDeliveryTypeByCurrentCart();
		}
		catch (final CartValidationException ex)
		{
			if (CartExceptionType.SHIPPINGMETHOD.equals(ex.getCartExceptionType()))
			{
				cartCheckOutValidatorData.setValid(false);
				errors.reject("cart.validation.contains.both.delivery.and.pickup",
						"Your cart cannot contain pick up from store and shipping items");
			}
		}
	}

	/**
	 * @return the cartValidationService
	 */
	public CartValidationService getCartValidationService()
	{
		return cartValidationService;
	}

}
