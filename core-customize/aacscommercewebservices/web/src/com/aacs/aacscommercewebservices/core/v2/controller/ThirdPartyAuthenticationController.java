/**
 *
 */
package com.aacs.aacscommercewebservices.core.v2.controller;

import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdParam;
import de.hybris.platform.webservicescommons.swagger.ApiFieldsParam;

import java.security.Principal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidClientException;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.common.exceptions.InvalidRequestException;
import org.springframework.security.oauth2.common.exceptions.UnsupportedGrantTypeException;
import org.springframework.security.oauth2.common.util.OAuth2Utils;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.OAuth2RequestValidator;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestValidator;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aacs.aacscommercewebservices.core.oauth2.ThirdPartyTokenGranter;
import com.aacs.aacscommercewebservices.core.service.UserSignatureAuthenticationService;
import com.aacs.aacsthirdpartyauthentication.data.ThirdPartyUserData;
import com.aacs.aacsthirdpartyauthentication.enums.ThirdPartyAuthenticationType;
import com.aacs.aacsthirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.aacs.aacsthirdpartyauthentication.exception.enums.ThirdPartyAuthenticationExceptionType;
import com.aacs.aacsthirdpartyauthentication.facades.facade.ThirdPartyAuthenticationProviderFacade;
import com.aacs.core.enums.MaritalStatus;
import com.aacs.facades.customer.data.MaritalStatusData;
import com.aacs.facades.user.customer.facade.CustomCustomerFacade;
import com.aacs.thirdpartyauthentication.profile.ThirdPartyProfileWsDTO;
import com.aacs.thirdpartyauthentication.provider.ThirdPartyAuthenticationProviderListData;
import com.aacs.thirdpartyauthentication.provider.ThirdPartyAuthenticationProviderListWsDTO;
import com.aacs.thirdpartyauthentication.provider.apple.AppleAuthenticationProviderData;
import com.aacs.thirdpartyauthentication.provider.apple.AppleAuthenticationProviderWsDTO;
import com.aacs.thirdpartyauthentication.provider.facebook.FacebookAuthenticationProviderData;
import com.aacs.thirdpartyauthentication.provider.facebook.FacebookAuthenticationProviderWsDTO;
import com.aacs.thirdpartyauthentication.provider.google.GoogleAuthenticationProviderData;
import com.aacs.thirdpartyauthentication.provider.google.GoogleAuthenticationProviderWsDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


/**
 * @author monzer
 *
 */
@Controller
@RequestMapping(value = "/{baseSiteId}/third-party")
@CacheControl(directive = CacheControlDirective.PUBLIC, maxAge = 360)
@Api(tags = "Third-Party Authentication")
public class ThirdPartyAuthenticationController extends BaseController
{

	@Resource(name = "thirdPartyAuthenticationProviderFacade")
	private ThirdPartyAuthenticationProviderFacade thirdPartyAuthenticationProviderFacade;

	private static final Logger LOG = LoggerFactory.getLogger(SignatureIdAuthenticationController.class);

	@Resource(name = "userSignatureAuthenticationService")
	private UserSignatureAuthenticationService userSignatureAuthenticationService;

	@Resource(name = "thirdPartyTokenGranter")
	private ThirdPartyTokenGranter thirdPartyTokenGranter;

	@Autowired
	@Qualifier(value = "openIDClientDetails")
	private ClientDetailsService clientDetailsService;

	@Resource(name = "authenticationManager")
	private AuthenticationManager authenticationManager;

	@Resource(name = "thirdPartyProfileDTOValidator")
	private Validator thirdPartyProfileDTOValidator;

	@Autowired
	private OAuth2RequestFactory oAuth2RequestFactory;

	@Resource(name = "customCustomerFacade")
	private CustomCustomerFacade customerFacade;

	@Resource(name = "maritalStatusDataConverter")
	private Converter<MaritalStatus, MaritalStatusData> maritalStatusDataConverter;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	private final OAuth2RequestValidator oAuth2RequestValidator = new DefaultOAuth2RequestValidator();

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/providers", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getThirdPartyAuthenticationProviders", value = "Third-Party Authentication Providers", notes = "Third-Party Authentication Providers")
	@ApiBaseSiteIdParam
	public ThirdPartyAuthenticationProviderListWsDTO getThirdPartyAuthenticationProviders(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final Optional<ThirdPartyAuthenticationProviderListData> thirdPartyProviders = thirdPartyAuthenticationProviderFacade
				.getAllThirdPartyProvidersByCurrentSite();

		return getDataMapper().map(thirdPartyProviders.orElse(null), ThirdPartyAuthenticationProviderListWsDTO.class);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/providers/facebook", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getFacebookAuthenticationProviders", value = "Facebook Authentication Providers", notes = "Facebook Authentication Providers")
	@ApiBaseSiteIdParam
	public FacebookAuthenticationProviderWsDTO getFacebookProvider(@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final Optional<FacebookAuthenticationProviderData> facebookProvider = thirdPartyAuthenticationProviderFacade
				.getFacebookAuthenticationProviderByCurrentSite();
		return getDataMapper().map(facebookProvider.orElse(null), FacebookAuthenticationProviderWsDTO.class);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/providers/google", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getGoogleAuthenticationProviders", value = "Google Authentication Providers", notes = "Google Authentication Providers")
	@ApiBaseSiteIdParam
	public GoogleAuthenticationProviderWsDTO getGoogleProvider(@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final Optional<GoogleAuthenticationProviderData> googleProvider = thirdPartyAuthenticationProviderFacade
				.getGoogleAuthenticationProviderByCurrentSite();
		return getDataMapper().map(googleProvider.orElse(null), GoogleAuthenticationProviderWsDTO.class);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/providers/apple", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getAppleAuthenticationProviders", value = "Apple Authentication Providers", notes = "Apple Authentication Providers")
	@ApiBaseSiteIdParam
	public AppleAuthenticationProviderWsDTO getAppleProvider(@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final Optional<AppleAuthenticationProviderData> googleProvider = thirdPartyAuthenticationProviderFacade
				.getAppleAuthenticationProviderByCurrentSite();
		return getDataMapper().map(googleProvider.orElse(null), AppleAuthenticationProviderWsDTO.class);
	}

	//==============================================================//
	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/auth", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(nickname = "thirdPartyAuthentication", value = "authenticate Using the Third-Party authentication.", notes = "authenticate Using the Third-Party authentication")
	@ApiBaseSiteIdParam
	public ResponseEntity<Map<String, Object>> authenticateThirdPartyToken(@ApiParam
	@RequestParam(required = true)
	final String thirdPartyToken, @ApiParam
	@RequestParam(required = true)
	final ThirdPartyAuthenticationType type, final Principal principal) throws ThirdPartyAuthenticationException
	{
		final Optional<ThirdPartyUserData> userData = thirdPartyAuthenticationProviderFacade
				.getThirdPartyUserDataByCurrentSite(thirdPartyToken, type);
		if (userData.isEmpty())
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					"Invalid token, User data from third party could be retreived");
		}
		if (!userData.get().isExists())
		{
			final Map<String, Object> map = new HashMap<>();
			return getResponse(null, userData.get());
		}
		final Map<String, String> parameters = new HashMap<String, String>();
		final CustomerData customer = customerFacade.getCustomerByCustomerId(userData.get().getUserData().getId());
		parameters.put("email", customer.getUid());
		parameters.put("thirdpartyToken", thirdPartyToken);
		parameters.put("thirdPartyType", type.getCode());
		parameters.put("callbackURL", "");
		parameters.put("grant_type", "third_party");
		userData.get().setUserData(null);
		return authenticate(principal, parameters, userData.get());
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/update-profile", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(nickname = "thirdPartyAuthentication", value = "authenticate Using the Third-Party authentication.", notes = "authenticate Using the Third-Party authentication")
	@ApiBaseSiteIdParam
	public ResponseEntity<Map<String, Object>> saveThirdPartyUserAndAuthenticateToken(@ApiParam
	@RequestParam(required = true)
	final String thirdPartyToken, @ApiParam
	@RequestParam(required = true)
	final ThirdPartyAuthenticationType type, @ApiParam(value = "Third-Party Profile object.", required = true)
	@RequestBody
	final ThirdPartyProfileWsDTO profile, final Principal principal)
			throws ThirdPartyAuthenticationException, DuplicateUidException
	{
		validate(profile, "profile", thirdPartyProfileDTOValidator);

		final Optional<ThirdPartyUserData> userData = thirdPartyAuthenticationProviderFacade
				.getThirdPartyUserRegistrationDataByCurrentSite(thirdPartyToken, type);
		if (userData.isEmpty())
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					"Invalid token, User data from third party could be retreived");
		}
		if (!profile.getId().equals(userData.get().getUserData().getId()))
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED,
					"The third-party user id is not valid for this third-party token");
		}

		final CustomerData customerData = populateCustomerDataFromThirdPartyProfile(type, profile);
		if (userData.get().isExists())
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					"This 3rd party user is already exists");
		}
		else
		{
			thirdPartyAuthenticationProviderFacade.saveUser(customerData);
		}

		final Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("email", customerData.getUid());
		parameters.put("thirdpartyToken", thirdPartyToken);
		parameters.put("thirdPartyType", type.getCode());
		parameters.put("callbackURL", "");
		parameters.put("grant_type", "third_party");
		userData.get().setExists(true);
		userData.get().setUserData(null);
		return authenticate(principal, parameters, userData.get());
	}

	private CustomerData populateCustomerDataFromThirdPartyProfile(final ThirdPartyAuthenticationType type,
			final ThirdPartyProfileWsDTO profile)
	{
		final CustomerData customerData = this.getDataMapper().map(profile, CustomerData.class);
		// Data could not be mapped with the mapper.
		customerData.setUid(profile.getId() + "|" + profile.getEmail());
		customerData.setThirdPartyType(type);
		customerData.setName(profile.getFirstName() + " " + profile.getLastName());
		final CountryData mobileCountry = new CountryData();
		mobileCountry.setIsocode(profile.getMobileCountry());
		customerData.setMobileCountry(mobileCountry);
		populateCustomerMaritailStatus(customerData, profile.getMaritalStatusCode());
		return customerData;
	}

	/**
	 * @param customerData
	 * @param maritalStatusCode
	 */
	private void populateCustomerMaritailStatus(final CustomerData customerData, final String maritalStatusCode)
	{
		if (StringUtils.isNotBlank(maritalStatusCode))
		{
			MaritalStatus status = null;
			try
			{
				status = MaritalStatus.valueOf(maritalStatusCode);
			}
			catch (final IllegalArgumentException e)
			{
				LOG.info("Third-Party customer " + status + " marital status is invalid");
			}
			if (status != null)
			{
				customerData.setMaritalStatus(maritalStatusDataConverter.convert(status));
			}
		}
	}

	private ResponseEntity<Map<String, Object>> authenticate(final Principal principal, final Map<String, String> parameters,
			final ThirdPartyUserData userData)
	{
		if (!(principal instanceof Authentication))
		{
			throw new InsufficientAuthenticationException(
					"There is no client authentication. Try adding an appropriate authentication filter.");
		}

		final String clientId = getClientId(principal);
		final ClientDetails authenticatedClient = clientDetailsService.loadClientByClientId(clientId);

		final TokenRequest tokenRequest = oAuth2RequestFactory.createTokenRequest(parameters, authenticatedClient);

		if (clientId != null && !clientId.equals(""))
		{
			// Only validate the client details if a client authenticated during this
			// request.
			if (!clientId.equals(tokenRequest.getClientId()))
			{
				// double check to make sure that the client ID in the token request is the same as that in the
				// authenticated client
				throw new InvalidClientException("Given client ID does not match authenticated client");
			}
		}
		if (authenticatedClient != null)
		{
			oAuth2RequestValidator.validateScope(tokenRequest, authenticatedClient);
		}
		if (!org.springframework.util.StringUtils.hasText(tokenRequest.getGrantType()))
		{
			throw new InvalidRequestException("Missing grant type");
		}
		if (tokenRequest.getGrantType().equals("implicit"))
		{
			throw new InvalidGrantException("Implicit grant type not supported from token endpoint");
		}

		if (isAuthCodeRequest(parameters))
		{
			// The scope was requested or determined during the authorization step
			if (!tokenRequest.getScope().isEmpty())
			{
				LOG.debug("Clearing scope of incoming token request");
				tokenRequest.setScope(Collections.<String> emptySet());
			}
		}

		if (isRefreshTokenRequest(parameters))
		{
			// A refresh token has its own default scopes, so we should ignore any added by the factory here.
			tokenRequest.setScope(OAuth2Utils.parseParameterList(parameters.get(OAuth2Utils.SCOPE)));
		}

		final OAuth2AccessToken token = thirdPartyTokenGranter.grant(tokenRequest.getGrantType(), tokenRequest);
		if (token == null)
		{
			throw new UnsupportedGrantTypeException("Unsupported grant type: " + tokenRequest.getGrantType());
		}

		return getResponse(token, userData);
	}

	protected String getClientId(final Principal principal)
	{
		final Authentication client = (Authentication) principal;
		if (!client.isAuthenticated())
		{
			throw new InsufficientAuthenticationException("The client is not authenticated.");
		}
		String clientId = client.getName();
		if (client instanceof OAuth2Authentication)
		{
			// Might be a client and user combined authentication
			clientId = ((OAuth2Authentication) client).getOAuth2Request().getClientId();
		}
		return clientId;
	}

	private ResponseEntity<Map<String, Object>> getResponse(final OAuth2AccessToken accessToken, final ThirdPartyUserData userData)
	{
		final HttpHeaders headers = new HttpHeaders();
		headers.set("Cache-Control", "no-store");
		headers.set("Pragma", "no-cache");
		headers.set("Content-Type", "application/json;charset=UTF-8");
		headers.set("signature-authentication", "The user has been authenticated successfully using signature code");
		final Map<String, Object> tokenBody = new HashMap<String, Object>();
		tokenBody.put("access_token", accessToken == null ? null : accessToken.getValue());
		tokenBody.put("token_type", accessToken == null ? null : accessToken.getTokenType());
		tokenBody.put("refresh_token", accessToken == null ? sessionService.getCurrentSession().getAttribute("refresh_token")
				: accessToken.getRefreshToken().getValue());
		tokenBody.put("expires_in", accessToken == null ? null : accessToken.getExpiresIn());
		tokenBody.put("scope", accessToken == null ? null : accessToken.getScope().stream().collect(Collectors.joining(", ")));
		tokenBody.put("exists", userData == null ? false : userData.isExists());
		tokenBody.put("userData", userData == null ? false : userData.getUserData());
		return new ResponseEntity<Map<String, Object>>(tokenBody, headers, HttpStatus.OK);
	}

	private boolean isRefreshTokenRequest(final Map<String, String> parameters)
	{
		return "refresh_token".equals(parameters.get("grant_type")) && parameters.get("refresh_token") != null;
	}

	private boolean isAuthCodeRequest(final Map<String, String> parameters)
	{
		return "authorization_code".equals(parameters.get("grant_type")) && parameters.get("code") != null;
	}

}
