package com.aacs.aacscommercewebservices.core.validator;

import de.hybris.platform.commercefacades.order.data.PaymentModeData;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.aacs.facades.facade.CustomAcceleratorCheckoutFacade;


/**
 * @author rflaifel
 *
 */
public class DefaultPaymentModeWsDTOValidator implements Validator
{
	@Resource(name = "acceleratorCheckoutFacade")
	private CustomAcceleratorCheckoutFacade checkoutFacade;

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(final Class<?> arg0)
	{
		// YTODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
	 */
	@Override
	public void validate(final Object paymentModeCode, final Errors errors)
	{
		final String code = (String) paymentModeCode;

		if (code == null || StringUtils.isEmpty(code))
		{
			errors.rejectValue("code", "payment.mode.value.error");

		}
		final List<PaymentModeData> supportedPaymentModes = checkoutFacade.getSupportedPaymentModes(true, false, null);
		if (CollectionUtils.isEmpty(supportedPaymentModes))
		{
			errors.rejectValue("code", "payment.mode.supported.error");
			return;
		}
		for (final PaymentModeData paymentModeData : supportedPaymentModes)
		{
			if (paymentModeData.getCode().equals(code))
			{
				return;
			}

		}
		errors.rejectValue("code", "payment.mode.value.valid.error");

	}
}
