/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscommercewebservices.core.v2.controller;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercewebservicescommons.core.user.data.LoyaltyCustomerHistoriesData;
import de.hybris.platform.commercewebservicescommons.dto.user.data.LoyaltyCustomerHistoriesWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.data.LoyaltyCustomerInfoWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.data.LoyaltyCustomerQrWsDTO;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdAndUserIdParam;
import de.hybris.platform.webservicescommons.swagger.ApiFieldsParam;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aacs.aacsloyaltyprogramfacades.data.LoyaltyCustomerHistoryData;
import com.aacs.aacsloyaltyprogramfacades.data.LoyaltyCustomerInfoData;
import com.aacs.aacsloyaltyprogramfacades.data.LoyaltyCustomerQrData;
import com.aacs.aacsloyaltyprogramfacades.data.LoyaltyPaginationData;
import com.aacs.aacsloyaltyprogramfacades.facades.LoyaltyPaymentFacade;
import com.aacs.aacsloyaltyprogramprovider.exception.AACSLoyaltyException;
import com.aacs.facades.user.customer.facade.CustomCustomerFacade;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


@Controller
@RequestMapping(value = "/{baseSiteId}/users/{userId}/loyalties")
@CacheControl(directive = CacheControlDirective.NO_CACHE)
@Api(tags = "Loyalty")
public class LoyaltyController extends BaseCommerceController
{
	private static final Logger LOG = LoggerFactory.getLogger(LoyaltyController.class);

	@Resource(name = "loyaltyPaymentFacade")
	private LoyaltyPaymentFacade loyaltyPaymentFacade;

	@Resource(name = "customCustomerFacade")
	private CustomCustomerFacade customCustomerFacade;

	@RequestMapping(value = "/info", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCustomerInfo", value = "Get Loyalty customer info.", notes = "Loyalty customer info")
	@ApiBaseSiteIdAndUserIdParam
	public LoyaltyCustomerInfoWsDTO getCustomerInfo(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields, @ApiParam(value = "Optional pagination parameter. Default value 0.")
	@RequestParam(defaultValue = DEFAULT_CURRENT_PAGE)
	final int currentPage, @ApiParam(value = "Optional {@link PaginationData} parameter. Default value 20.")
	@RequestParam(defaultValue = DEFAULT_PAGE_SIZE)
	final int pageSize)
	{
		//
		//		final LoyaltyPaginationData loyaltyPaginationData = getDataMapper().map(loyaltyPagintaion, LoyaltyPaginationData.class,
		//				"pageIndex,pageSize");

		final LoyaltyPaginationData loyaltyPaginationData = new LoyaltyPaginationData();
		loyaltyPaginationData.setPageIndex(currentPage);
		loyaltyPaginationData.setPageSize(pageSize);

		Optional<LoyaltyCustomerInfoData> customerInfoData;
		try
		{
			customerInfoData = loyaltyPaymentFacade.getLoyaltyCustomerByCurrentBaseStoreAndCurrentCustomer(loyaltyPaginationData);
		}
		catch (final AACSLoyaltyException e)
		{
			return null;
		}

		if (customerInfoData.isEmpty())
		{
			return null;
		}
		final List<LoyaltyCustomerHistoryData> histories = customerInfoData.get().getHistories();
		customerInfoData.get().setHistories(null);
		final LoyaltyCustomerInfoWsDTO infoWsDTO = getDataMapper().map(customerInfoData.get(), LoyaltyCustomerInfoWsDTO.class);
		if (!CollectionUtils.isEmpty(histories))
		{
			final LoyaltyCustomerHistoriesData historiesData = new LoyaltyCustomerHistoriesData();
			historiesData.setHistories(histories);

			final LoyaltyCustomerHistoriesWsDTO historiesWsDTO = getDataMapper().map(historiesData,
					LoyaltyCustomerHistoriesWsDTO.class);
			infoWsDTO.setHistories(historiesWsDTO);
		}


		return infoWsDTO;
	}

	@RequestMapping(value = "/qr", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCustomerInfo", value = "Get Loyalty customer info.", notes = "Loyalty customer info")
	@ApiBaseSiteIdAndUserIdParam
	public LoyaltyCustomerQrWsDTO getCustomerQrCode(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		Optional<LoyaltyCustomerQrData> customerQrCode;
		try
		{
			customerQrCode = loyaltyPaymentFacade.getLoyaltyCustomerQrCodeByCurrentBaseStoreAndCustomer();
		}
		catch (final AACSLoyaltyException e)
		{
			return null;
		}
		return customerQrCode.isPresent() ? getDataMapper().map(customerQrCode.get(), LoyaltyCustomerQrWsDTO.class) : null;

	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(nickname = "/registerCurrentCustomer", value = "Register current customer for loyalty program.", notes = "Loyalty customer info")
	@ApiBaseSiteIdAndUserIdParam
	public void registerCurrentCustomer(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final CustomerData currentCustomer = getCustomCustomerFacade().getCurrentCustomer();

		if (currentCustomer == null)
		{
			LOG.warn("Could not get Current customer");
			return;
		}

		if (currentCustomer.isInvolvedInLoyaltyProgram())
		{
			return;
		}

		getCustomCustomerFacade().registerCustomerInLoyaltyByCurrentCustomer();
	}

	@RequestMapping(value = "/unregister", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(nickname = "/unregisterCurrentCustomer", value = "Unregister current customer for loyalty program.", notes = "Loyalty customer info")
	@ApiBaseSiteIdAndUserIdParam
	public void unregisterCurrentCustomer(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final CustomerData currentCustomer = getCustomCustomerFacade().getCurrentCustomer();

		if (currentCustomer == null)
		{
			LOG.warn("Could not get Current customer");
			return;
		}

		if (!currentCustomer.isInvolvedInLoyaltyProgram())
		{
			return;
		}

		getCustomCustomerFacade().unregisterCustomerInLoyaltyByCurrentCustomer();
	}

	/**
	 * @return the customCustomerFacade
	 */
	public CustomCustomerFacade getCustomCustomerFacade()
	{
		return customCustomerFacade;
	}
}
