/**
 *
 */
package com.aacs.aacscommercewebservices.core.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CountryValidator implements Validator
{

	private String isoCode;


	@Override
	public boolean supports(final Class<?> clazz)
	{
		return true;
	}

	@Override
	public void validate(final Object obj, final Errors errors)
	{
		Assert.notNull(errors, "Errors object must not be null");

		final String iso = (String) errors.getFieldValue(this.isoCode);


		if (StringUtils.isBlank(iso))
		{
			errors.rejectValue(this.isoCode, "", new String[]
			{ this.isoCode }, "This field is not a valid country code.");
		}
	}

	/**
	 * @return the isoCode
	 */
	public String getIsoCode()
	{
		return isoCode;
	}

	/**
	 * @param isoCode
	 *           the isoCode to set
	 */
	public void setIsoCode(final String isoCode)
	{
		this.isoCode = isoCode;
	}

}
