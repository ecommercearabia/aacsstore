/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscommercewebservices.core.util.ws;

public interface SearchQueryCodec<QUERY>
{
	QUERY decodeCategoryQuery(String query);

	QUERY decodeQuery(String query);

	String encodeQuery(QUERY query);
}
