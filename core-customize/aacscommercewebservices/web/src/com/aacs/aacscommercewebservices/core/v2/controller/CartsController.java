/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscommercewebservices.core.v2.controller;

import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.SaveCartFacade;
import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.CartModificationDataList;
import de.hybris.platform.commercefacades.order.data.DeliveryModesData;
import de.hybris.platform.commercefacades.order.data.NoCardPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.NoCardTypeData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.order.data.PaymentModeData;
import de.hybris.platform.commercefacades.product.data.PromotionResultData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commercefacades.promotion.CommercePromotionRestrictionFacade;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.voucher.VoucherFacade;
import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceCartMergingException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.commerceservices.promotion.CommercePromotionRestrictionException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commercewebservicescommons.annotation.SiteChannelRestriction;
import de.hybris.platform.commercewebservicescommons.core.user.data.CartCheckOutValidatorData;
import de.hybris.platform.commercewebservicescommons.dto.order.CartCheckOutValidatorWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.CartListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.CartModificationListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.CartModificationWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.CartWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.DeliveryModeListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.DeliveryModeWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.LoyaltyPaymentDetailsWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.LoyaltyPaymentModeListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.OrderEntryListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.OrderEntryWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.PaymentDetailsWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.PaymentModeListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.PaymentModeWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.StoreCreditDetailsWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.StoreCreditModeListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.TimeSlotInfoWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.TimeSlotWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.payment.data.Payment3DSResultWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.product.PromotionResultListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.AddressWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.voucher.VoucherListWsDTO;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.CartEntryException;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.CartEntryGroupException;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.CartException;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.LowStockException;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.ProductLowStockException;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.RequestParameterException;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.StockSystemException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdAndUserIdParam;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdUserIdAndCartIdParam;
import de.hybris.platform.webservicescommons.swagger.ApiFieldsParam;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.aacs.aacsacceleratorcustomocc.dto.payment.PaymentRequestWsDTO;
import com.aacs.aacscommercewebservices.core.basesite.data.LoyaltyPaymentModesData;
import com.aacs.aacscommercewebservices.core.cart.impl.CommerceWebServicesCartFacade;
import com.aacs.aacscommercewebservices.core.exceptions.InvalidPaymentInfoException;
import com.aacs.aacscommercewebservices.core.exceptions.NoCheckoutCartException;
import com.aacs.aacscommercewebservices.core.exceptions.UnsupportedDeliveryModeException;
import com.aacs.aacscommercewebservices.core.exceptions.UnsupportedRequestException;
import com.aacs.aacscommercewebservices.core.order.data.CartDataList;
import com.aacs.aacscommercewebservices.core.order.data.OrderEntryDataList;
import com.aacs.aacscommercewebservices.core.product.data.PromotionResultDataList;
import com.aacs.aacscommercewebservices.core.request.support.impl.PaymentProviderRequestSupportedStrategy;
import com.aacs.aacscommercewebservices.core.stock.CommerceStockFacade;
import com.aacs.aacscommercewebservices.core.validation.data.CartVoucherValidationData;
import com.aacs.aacscommercewebservices.core.voucher.data.VoucherDataList;
import com.aacs.aacsloyaltyprogramfacades.data.LoyaltyPaymentModeData;
import com.aacs.aacspayment.context.PaymentContext;
import com.aacs.aacspayment.entry.PaymentRequestData;
import com.aacs.aacspayment.entry.PaymentResponseData;
import com.aacs.aacspayment.exception.PaymentException;
import com.aacs.aacspayment.exception.type.PaymentExceptionType;
import com.aacs.aacspayment.model.PaymentTransactionInfoModel;
import com.aacs.aacspayment.mpgs.beans.MpgsPaymentRequestData;
import com.aacs.aacspayment.mpgs.beans.RetrieveSessionBean;
import com.aacs.aacspayment.mpgs.beans.incoming.MpgsResponse.MpgsResultEnum;
import com.aacs.aacspayment.mpgs.constants.MpgsConstants;
import com.aacs.aacspayment.mpgs.exception.MpgsException;
import com.aacs.aacspayment.mpgs.exception.type.MpgsExceptionType;
import com.aacs.aacspayment.mpgs.service.MpgsService;
import com.aacs.aacspayment.service.PaymentTransactionInfoService;
import com.aacs.aacsstorecreditfacades.data.StoreCreditModeData;
import com.aacs.aacstimeslot.enums.TimeSlotConfigType;
import com.aacs.aacstimeslot.model.TimeSlotInfoModel;
import com.aacs.aacstimeslotfacades.PeriodData;
import com.aacs.aacstimeslotfacades.TimeSlotData;
import com.aacs.aacstimeslotfacades.TimeSlotDayData;
import com.aacs.aacstimeslotfacades.TimeSlotInfoData;
import com.aacs.aacstimeslotfacades.exception.TimeSlotException;
import com.aacs.aacstimeslotfacades.exception.type.TimeSlotExceptionType;
import com.aacs.aacstimeslotfacades.facade.TimeSlotFacade;
import com.aacs.core.order.cart.exception.CartValidationException;
import com.aacs.core.order.cart.exception.enums.CartExceptionType;
import com.aacs.core.order.cart.service.CartValidationService;
import com.aacs.facades.facade.CustomAcceleratorCheckoutFacade;
import com.aacs.facades.user.facade.CustomUserFacade;
import com.aldahra.aldahracommercewebservices.core.basesite.data.PaymentModesData;
import com.aldahra.aldahracommercewebservices.core.basesite.data.StoreCreditModesData;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;



@Controller
@RequestMapping(value = "/{baseSiteId}/users/{userId}/carts")
@CacheControl(directive = CacheControlDirective.NO_CACHE)
@Api(tags = "Carts")
public class CartsController extends BaseCommerceController
{
	private static final Logger LOG = LoggerFactory.getLogger(CartsController.class);

	private static final String ADDRESS_MAPPING = "title,titleCode,firstName,lastName,line1,line2,country(isocode),addressName,mobileCountry,mobileNumber,city,area,nearestLandmark,buildingName,apartmentNumber,floorNumber,deliveryNotes";
	private static final String OBJECT_NAME_ADDRESS = "address";
	private static final long DEFAULT_PRODUCT_QUANTITY = 1;
	private static final String ENTRY = "entry";
	private static final String ENTRY_NUMBER = "entryNumber";
	private static final String PICKUP_STORE = "pickupStore";
	private static final String COUPON_STATUS_CODE = "couponNotValid";
	private static final String VOUCHER_STATUS_CODE = "voucherNotValid";

	@Resource(name = "commerceCheckoutService")
	private CommerceCheckoutService commerceCheckoutService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "paymentModeWsDTOValidator")
	private Validator paymentModeValidator;
	@Resource(name = "commercePromotionRestrictionFacade")
	private CommercePromotionRestrictionFacade commercePromotionRestrictionFacade;
	@Resource(name = "commerceStockFacade")
	private CommerceStockFacade commerceStockFacade;
	@Resource(name = "customerFacade")
	private CustomerFacade customerFacade;
	@Resource(name = "pointOfServiceValidator")
	private Validator pointOfServiceValidator;
	@Resource(name = "orderEntryCreateValidator")
	private Validator orderEntryCreateValidator;
	@Resource(name = "addToCartEntryGroupValidator")
	private Validator addToCartEntryGroupValidator;
	@Resource(name = "orderEntryUpdateValidator")
	private Validator orderEntryUpdateValidator;
	@Resource(name = "orderEntryReplaceValidator")
	private Validator orderEntryReplaceValidator;
	@Resource(name = "greaterThanZeroValidator")
	private Validator greaterThanZeroValidator;
	@Resource(name = "paymentProviderRequestSupportedStrategy")
	private PaymentProviderRequestSupportedStrategy paymentProviderRequestSupportedStrategy;
	@Resource(name = "saveCartFacade")
	private SaveCartFacade saveCartFacade;
	@Resource(name = "voucherFacade")
	private VoucherFacade voucherFacade;
	@Resource(name = "cartValidationService")
	private CartValidationService cartValidationService;
	@Resource(name = "messageSource")
	private MessageSource messageSource;

	@Resource(name = "timeSlotInfoWsDTOValidator")
	private Validator timeSlotInfoValidator;
	@Resource(name = "userFacade")
	private CustomUserFacade customUserFacade;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "addressConverter")
	private Converter<AddressModel, AddressData> addressConverter;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "paymentContext")
	private PaymentContext paymentContext;
	@Resource(name = "acceleratorCheckoutFacade")
	private CustomAcceleratorCheckoutFacade checkoutFacade;


	@Resource(name = "loyaltyWsDTOValidator")
	private Validator loyaltyPointsValidator;



	@Resource(name = "threeDSResultForPaymentDeviceValidator")
	private Validator threeDSResultForPaymentDeviceValidator;

	@Resource(name = "threeDSResultValidator")
	private Validator threeDSResultValidator;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "timeSlotFacade")
	private TimeSlotFacade timeSlotFacade;
	@Resource(name = "paymentTransactionInfoService")
	private PaymentTransactionInfoService paymentTransactionInfoService;

	/** The payment provider service. */
	@Resource(name = "mpgsService")
	private MpgsService mpgsService;



	/**
	 * @return the customUserFacade
	 */
	public CustomUserFacade getCustomUserFacade()
	{
		return customUserFacade;
	}


	/**
	 * @return the paymentModeValidator
	 */
	protected Validator getPaymentModeValidator()
	{
		return paymentModeValidator;
	}

	/**
	 * @return the timeSlotInfoValidator
	 */
	protected Validator getTimeSlotInfoValidator()
	{
		return timeSlotInfoValidator;
	}

	/**
	 * @return the storeCreditValidator
	 */
	protected Validator getStoreCreditValidator()
	{
		return storeCreditValidator;
	}

	@Resource(name = "storeCreditWsDTOValidator")
	private Validator storeCreditValidator;

	/**
	 * @return the messageSource
	 */
	protected MessageSource getMessageSource()
	{
		return messageSource;
	}

	/**
	 * @return the loyaltyPointsValidator
	 */
	protected Validator getLoyaltyPointsValidator()
	{
		return loyaltyPointsValidator;
	}


	/**
	 * @return the cmsSiteService
	 */
	protected CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}




	/**
	 * @return the commerceCheckoutService
	 */
	public CommerceCheckoutService getCommerceCheckoutService()
	{
		return commerceCheckoutService;
	}


	/**
	 * @param commerceCheckoutService
	 *           the commerceCheckoutService to set
	 */
	public void setCommerceCheckoutService(final CommerceCheckoutService commerceCheckoutService)
	{
		this.commerceCheckoutService = commerceCheckoutService;
	}


	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}


	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}


	/**
	 * @return the timeSlotFacade
	 */
	protected TimeSlotFacade getTimeSlotFacade()
	{
		return timeSlotFacade;
	}


	protected static CartModificationData mergeCartModificationData(final CartModificationData cmd1,
			final CartModificationData cmd2)
	{
		if ((cmd1 == null) && (cmd2 == null))
		{
			return new CartModificationData();
		}
		if (cmd1 == null)
		{
			return cmd2;
		}
		if (cmd2 == null)
		{
			return cmd1;
		}
		final CartModificationData cmd = new CartModificationData();
		cmd.setDeliveryModeChanged(Boolean
				.valueOf(Boolean.TRUE.equals(cmd1.getDeliveryModeChanged()) || Boolean.TRUE.equals(cmd2.getDeliveryModeChanged())));
		cmd.setEntry(cmd2.getEntry());
		cmd.setQuantity(cmd2.getQuantity());
		cmd.setQuantityAdded(cmd1.getQuantityAdded() + cmd2.getQuantityAdded());
		cmd.setStatusCode(cmd2.getStatusCode());
		return cmd;
	}

	protected static OrderEntryData getCartEntryForNumber(final CartData cart, final long number)
	{
		final List<OrderEntryData> entries = cart.getEntries();
		if (entries != null && !entries.isEmpty())
		{
			final Integer requestedEntryNumber = Integer.valueOf((int) number);
			for (final OrderEntryData entry : entries)
			{
				if (entry != null && requestedEntryNumber.equals(entry.getEntryNumber()))
				{
					return entry;
				}
			}
		}
		throw new CartEntryException("Entry not found", CartEntryException.NOT_FOUND, String.valueOf(number));
	}

	protected static OrderEntryData getCartEntry(final CartData cart, final String productCode, final String pickupStore)
	{
		for (final OrderEntryData oed : cart.getEntries())
		{
			if (oed.getProduct().getCode().equals(productCode))
			{
				if (pickupStore == null && oed.getDeliveryPointOfService() == null)
				{
					return oed;
				}
				else if (pickupStore != null && oed.getDeliveryPointOfService() != null
						&& pickupStore.equals(oed.getDeliveryPointOfService().getName()))
				{
					return oed;
				}
			}
		}
		return null;
	}

	protected static void validateForAmbiguousPositions(final CartData currentCart, final OrderEntryData currentEntry,
			final String newPickupStore)
	{
		final OrderEntryData entryToBeModified = getCartEntry(currentCart, currentEntry.getProduct().getCode(), newPickupStore);
		if (entryToBeModified != null && !entryToBeModified.getEntryNumber().equals(currentEntry.getEntryNumber()))
		{
			throw new CartEntryException(
					"Ambiguous cart entries! Entry number " + currentEntry.getEntryNumber()
							+ " after change would be the same as entry " + entryToBeModified.getEntryNumber(),
					CartEntryException.AMBIGIOUS_ENTRY, entryToBeModified.getEntryNumber().toString());
		}
	}

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCarts", value = "Get all customer carts.", notes = "Lists all customer carts.")
	@ApiBaseSiteIdAndUserIdParam
	public CartListWsDTO getCarts(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields,
			@ApiParam(value = "Optional parameter. If the parameter is provided and its value is true, only saved carts are returned.")
			@RequestParam(defaultValue = "false")
			final boolean savedCartsOnly,
			@ApiParam(value = "Optional pagination parameter in case of savedCartsOnly == true. Default value 0.")
			@RequestParam(defaultValue = DEFAULT_CURRENT_PAGE)
			final int currentPage,
			@ApiParam(value = "Optional {@link PaginationData} parameter in case of savedCartsOnly == true. Default value 20.")
			@RequestParam(defaultValue = DEFAULT_PAGE_SIZE)
			final int pageSize, @ApiParam(value = "Optional sort criterion in case of savedCartsOnly == true. No default value.")
			@RequestParam(required = false)
			final String sort)
	{
		if (getUserFacade().isAnonymousUser())
		{
			throw new AccessDeniedException("Access is denied");
		}

		final CartDataList cartDataList = new CartDataList();

		final PageableData pageableData = new PageableData();
		pageableData.setCurrentPage(currentPage);
		pageableData.setPageSize(pageSize);
		pageableData.setSort(sort);
		final List<CartData> allCarts = new ArrayList<>(
				saveCartFacade.getSavedCartsForCurrentUser(pageableData, null).getResults());
		if (!savedCartsOnly)
		{
			allCarts.addAll(getCartFacade().getCartsForCurrentUser());
		}
		cartDataList.setCarts(allCarts);

		return getDataMapper().map(cartDataList, CartListWsDTO.class, fields);
	}

	@RequestMapping(value = "/{cartId}", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCart", value = "Get a cart with a given identifier.", notes = "Returns the cart with a given identifier.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartWsDTO getCart(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{

		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		Optional<TimeSlotData> timeSlot = Optional.empty();
		try
		{

			if (TimeSlotConfigType.BY_AREA.equals(cmsSiteService.getCurrentSite().getTimeSlotConfigType())
					&& Objects.nonNull(cartData.getDeliveryAddress()) && Objects.nonNull(cartData.getDeliveryAddress().getArea()))
			{
				timeSlot = timeSlotFacade.getTimeSlotDataByArea(cartData.getDeliveryAddress().getArea().getCode());
			}
			else if (TimeSlotConfigType.BY_DELIVERYMODE.equals(cmsSiteService.getCurrentSite().getTimeSlotConfigType())
					&& Objects.nonNull(cartData.getDeliveryMode()))
			{
				timeSlot = timeSlotFacade.getTimeSlotData(cartData.getDeliveryMode().getCode());

			}
		}
		catch (final TimeSlotException e)
		{
			LOG.error(e.getMessage(), e);
		}

		if (timeSlot.isEmpty())
		{
			return getDataMapper().map(getSessionCart(), CartWsDTO.class, fields);
		}

		final TimeSlotInfoData firstAvailableTimeSlotInfoData = getFirstAvailableTimeSlotInfoData(timeSlot.get());

		if (Objects.nonNull(firstAvailableTimeSlotInfoData) && cartData.getTimeSlotInfoData() == null)
		{
			getCheckoutFacade().setTimeSlot(firstAvailableTimeSlotInfoData);
		}
		// CartMatchingFilter sets current cart based on cartId, so we can return cart from the session
		return getDataMapper().map(getSessionCart(), CartWsDTO.class, fields);
	}

	/**
	 * @param timeSlotData
	 */
	private TimeSlotInfoData getFirstAvailableTimeSlotInfoData(final TimeSlotData timeSlotData)
	{
		TimeSlotDayData selectedTimeSlot = null;
		PeriodData selectedPeriod = null;

		outter: for (final TimeSlotDayData timeSlot : timeSlotData.getTimeSlotDays())
		{
			for (final PeriodData period : timeSlot.getPeriods())
			{
				if (period.isEnabled())
				{
					selectedTimeSlot = timeSlot;
					selectedPeriod = period;
					break outter;
				}
			}
		}

		if (selectedPeriod == null)
		{
			return null;
		}
		final TimeSlotInfoData data = new TimeSlotInfoData();
		data.setPeriodCode(selectedPeriod.getCode());
		data.setStart(selectedPeriod.getStart());
		data.setEnd(selectedPeriod.getEnd());
		data.setDay(selectedTimeSlot.getDay());
		data.setDate(selectedTimeSlot.getDate());
		return data;
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "createCart", value = "Creates or restore a cart for a user.", notes = "Creates a new cart or restores an anonymous cart as a user's cart (if an old Cart Id is given in the request).")
	@ApiBaseSiteIdAndUserIdParam
	public CartWsDTO createCart(@ApiParam(value = "Anonymous cart GUID.")
	@RequestParam(required = false)
	final String oldCartId, @ApiParam(value = "The GUID of the user's cart that will be merged with the anonymous cart.")
	@RequestParam(required = false)
	final String toMergeCartGuid, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		LOG.debug("createCart");
		String evaluatedToMergeCartGuid = toMergeCartGuid;
		if (StringUtils.isNotEmpty(oldCartId))
		{
			if (getUserFacade().isAnonymousUser())
			{
				throw new CartException("Anonymous user is not allowed to copy cart!");
			}

			if (!isCartAnonymous(oldCartId))
			{
				throw new CartException("Cart is not anonymous", CartException.CANNOT_RESTORE, oldCartId);
			}

			if (StringUtils.isEmpty(evaluatedToMergeCartGuid))
			{
				evaluatedToMergeCartGuid = getSessionCart().getGuid();
			}
			else
			{
				if (!isUserCart(evaluatedToMergeCartGuid))
				{
					throw new CartException("Cart is not current user's cart", CartException.CANNOT_RESTORE, evaluatedToMergeCartGuid);
				}
			}

			try
			{
				getCartFacade().restoreAnonymousCartAndMerge(oldCartId, evaluatedToMergeCartGuid);
				return getDataMapper().map(getSessionCart(), CartWsDTO.class, fields);
			}
			catch (final CommerceCartMergingException e)
			{
				throw new CartException("Couldn't merge carts", CartException.CANNOT_MERGE, e);
			}
			catch (final CommerceCartRestorationException e)
			{
				throw new CartException("Couldn't restore cart", CartException.CANNOT_RESTORE, e);
			}
		}
		else
		{
			if (StringUtils.isNotEmpty(evaluatedToMergeCartGuid))
			{
				if (!isUserCart(evaluatedToMergeCartGuid))
				{
					throw new CartException("Cart is not current user's cart", CartException.CANNOT_RESTORE, evaluatedToMergeCartGuid);
				}

				try
				{
					getCartFacade().restoreSavedCart(evaluatedToMergeCartGuid);
					return getDataMapper().map(getSessionCart(), CartWsDTO.class, fields);
				}
				catch (final CommerceCartRestorationException e)
				{
					throw new CartException("Couldn't restore cart", CartException.CANNOT_RESTORE, oldCartId, e);
				}

			}
			return getDataMapper().map(getSessionCart(), CartWsDTO.class, fields);
		}
	}

	protected boolean isUserCart(final String toMergeCartGuid)
	{
		if (getCartFacade() instanceof CommerceWebServicesCartFacade)
		{
			final CommerceWebServicesCartFacade commerceWebServicesCartFacade = (CommerceWebServicesCartFacade) getCartFacade();
			return commerceWebServicesCartFacade.isCurrentUserCart(toMergeCartGuid);
		}
		return true;
	}

	protected boolean isCartAnonymous(final String cartGuid)
	{
		if (getCartFacade() instanceof CommerceWebServicesCartFacade)
		{
			final CommerceWebServicesCartFacade commerceWebServicesCartFacade = (CommerceWebServicesCartFacade) getCartFacade();
			return commerceWebServicesCartFacade.isAnonymousUserCart(cartGuid);
		}
		return true;
	}

	@RequestMapping(value = "/{cartId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "removeCart", value = "Deletes a cart with a given cart id.", notes = "Deletes a cart with a given cart id.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void removeCart()
	{
		getCartFacade().removeSessionCart();
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/email", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "replaceCartGuestUser", value = "Assigns an email to the cart.", notes = "Assigns an email to the cart. This step is required to make a guest checkout.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void replaceCartGuestUser(
			@ApiParam(value = "Email of the guest user. It will be used during the checkout process.", required = true)
			@RequestParam
			final String email) throws DuplicateUidException
	{
		LOG.debug("replaceCartGuestUser: email={}", sanitize(email));
		if (!EmailValidator.getInstance().isValid(email))
		{
			throw new RequestParameterException("Email [" + sanitize(email) + "] is not a valid e-mail address!",
					RequestParameterException.INVALID, "login");
		}

		customerFacade.createGuestUserForAnonymousCheckout(email, "guest");
	}

	@RequestMapping(value = "/{cartId}/entries", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCartEntries", value = "Get cart entries.", notes = "Returns cart entries.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public OrderEntryListWsDTO getCartEntries(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		LOG.debug("getCartEntries");
		final OrderEntryDataList dataList = new OrderEntryDataList();
		dataList.setOrderEntries(getSessionCart().getEntries());
		return getDataMapper().map(dataList, OrderEntryListWsDTO.class, fields);
	}

	/**
	 * @deprecated since 2005. Please use {@link CartsController#createCartEntry(String, OrderEntryWsDTO, String)}
	 *             instead.
	 */
	@Deprecated(since = "2005", forRemoval = true)
	@RequestMapping(value = "/{cartId}/entries", method = RequestMethod.POST)
	@ResponseBody
	@SiteChannelRestriction(allowedSiteChannelsProperty = API_COMPATIBILITY_B2C_CHANNELS)
	@ApiOperation(hidden = true, value = "Adds a product to the cart.", notes = "Adds a product to the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartModificationWsDTO createCartEntry(@PathVariable
	final String baseSiteId,
			@ApiParam(value = "Code of the product to be added to cart. Product look-up is performed for the current product catalog version.", required = true)
			@RequestParam
			final String code, @ApiParam(value = "Quantity of product.")
			@RequestParam(defaultValue = "1")
			final long qty,
			@ApiParam(value = "Name of the store where product will be picked. Set only if want to pick up from a store.")
			@RequestParam(required = false)
			final String pickupStore, @ApiFieldsParam
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws CommerceCartModificationException
	{
		LOG.debug("createCartEntry: {}, {}, {}", logParam("code", code), logParam("qty", qty), logParam(PICKUP_STORE, pickupStore));

		if (StringUtils.isNotEmpty(pickupStore))
		{
			validate(pickupStore, PICKUP_STORE, pointOfServiceValidator);
		}

		return addCartEntryInternal(baseSiteId, code, qty, pickupStore, fields);
	}

	protected CartModificationWsDTO addCartEntryInternal(final String baseSiteId, final String code, final long qty,
			final String pickupStore, final String fields) throws CommerceCartModificationException
	{
		final CartModificationData cartModificationData;
		if (StringUtils.isNotEmpty(pickupStore))
		{
			validateIfProductIsInStockInPOS(baseSiteId, code, pickupStore, null);
			cartModificationData = getCartFacade().addToCart(code, qty, pickupStore);
		}
		else
		{
			validateIfProductIsInStockOnline(baseSiteId, code, null);
			cartModificationData = getCartFacade().addToCart(code, qty);
		}
		return getDataMapper().map(cartModificationData, CartModificationWsDTO.class, fields);
	}

	@PostMapping(value = "/{cartId}/entrygroups/{entryGroupNumber}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	@ApiOperation(nickname = "addToCartEntryGroup", value = "Add a product to a cart entry group.", notes = "Adds a product to a cart entry group.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartModificationWsDTO addToCartEntryGroup(@PathVariable
	final String baseSiteId,
			@ApiParam(value = "Each entry group in a cart has a specific entry group number. Entry group numbers are integers starting at one. They are defined in ascending order.", required = true)
			@PathVariable
			final Integer entryGroupNumber,
			@ApiParam(value = "Request body parameter that contains details such as the product code (product.code) and the quantity of product (quantity).", required = true)
			@RequestBody
			final OrderEntryWsDTO entry, @ApiFieldsParam
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws CommerceCartModificationException
	{
		if (entry.getQuantity() == null)
		{
			entry.setQuantity(Long.valueOf(DEFAULT_PRODUCT_QUANTITY));
		}

		validate(entry, ENTRY, addToCartEntryGroupValidator);
		validate(entryGroupNumber, "entryGroupNumber", greaterThanZeroValidator);

		validateIfProductIsInStockOnline(baseSiteId, entry.getProduct().getCode(), null);

		final AddToCartParams params = new AddToCartParams();
		params.setStoreId(baseSiteId);
		params.setProductCode(entry.getProduct().getCode());
		params.setQuantity(entry.getQuantity());
		params.setEntryGroupNumbers(Set.of(entryGroupNumber));
		final CartModificationData cartModificationData = getCartFacade().addToCart(params);

		return getDataMapper().map(cartModificationData, CartModificationWsDTO.class, fields);
	}

	@RequestMapping(value = "/{cartId}/entries", method = RequestMethod.POST, consumes =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	@SiteChannelRestriction(allowedSiteChannelsProperty = API_COMPATIBILITY_B2C_CHANNELS)
	@ApiOperation(nickname = "createCartEntry", value = "Adds a product to the cart.", notes = "Adds a product to the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartModificationWsDTO createCartEntry(@PathVariable
	final String baseSiteId,
			@ApiParam(value = "Request body parameter that contains details such as the product code (product.code), the quantity of product (quantity), and the pickup store name (deliveryPointOfService.name).\n\nThe DTO is in XML or .json format.", required = true)
			@RequestBody
			final OrderEntryWsDTO entry, @ApiFieldsParam
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws CommerceCartModificationException
	{
		if (entry.getQuantity() == null)
		{
			entry.setQuantity(Long.valueOf(DEFAULT_PRODUCT_QUANTITY));
		}

		validate(entry, ENTRY, orderEntryCreateValidator);

		final String pickupStore = entry.getDeliveryPointOfService() == null ? null : entry.getDeliveryPointOfService().getName();
		return addCartEntryInternal(baseSiteId, entry.getProduct().getCode(), entry.getQuantity().longValue(), pickupStore, fields);
	}

	@RequestMapping(value = "/{cartId}/entries/{entryNumber}", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCartEntry", value = "Get the details of the cart entries.", notes = "Returns the details of the cart entries.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public OrderEntryWsDTO getCartEntry(
			@ApiParam(value = "The entry number. Each entry in a cart has an entry number. Cart entries are numbered in ascending order, starting with zero (0).", required = true)
			@PathVariable
			final long entryNumber, @ApiFieldsParam
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{
		LOG.debug("getCartEntry: {}", logParam(ENTRY_NUMBER, entryNumber));
		final OrderEntryData orderEntry = getCartEntryForNumber(getSessionCart(), entryNumber);
		return getDataMapper().map(orderEntry, OrderEntryWsDTO.class, fields);
	}

	/**
	 * @deprecated since 2005. Please use {@link CartsController#replaceCartEntry(String, long, OrderEntryWsDTO, String)}
	 *             instead.
	 */
	@Deprecated(since = "2005", forRemoval = true)
	@RequestMapping(value = "/{cartId}/entries/{entryNumber}", method = RequestMethod.PUT)
	@ResponseBody
	@SiteChannelRestriction(allowedSiteChannelsProperty = API_COMPATIBILITY_B2C_CHANNELS)
	@ApiOperation(hidden = true, value = "Set quantity and store details of a cart entry.", notes = "Updates the quantity of a single cart entry and the details of the store where the cart entry will be picked up. Attributes not provided in request will be defined again (set to null or default)")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartModificationWsDTO replaceCartEntry(@PathVariable
	final String baseSiteId,
			@ApiParam(value = "The entry number. Each entry in a cart has an entry number. Cart entries are numbered in ascending order, starting with zero (0).", required = true)
			@PathVariable
			final long entryNumber, @ApiParam(value = "Quantity of product.", required = true)
			@RequestParam
			final Long qty,
			@ApiParam(value = "Name of the store where product will be picked. Set only if want to pick up from a store.")
			@RequestParam(required = false)
			final String pickupStore, @ApiFieldsParam
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws CommerceCartModificationException
	{
		LOG.debug("replaceCartEntry: {}, {}, {}", logParam(ENTRY_NUMBER, entryNumber), logParam("qty", qty),
				logParam(PICKUP_STORE, pickupStore));
		final CartData cart = getSessionCart();
		final OrderEntryData orderEntry = getCartEntryForNumber(cart, entryNumber);
		if (!StringUtils.isEmpty(pickupStore))
		{
			validate(pickupStore, PICKUP_STORE, pointOfServiceValidator);
		}

		return updateCartEntryInternal(baseSiteId, cart, orderEntry, qty, pickupStore, fields, true);
	}

	protected CartModificationWsDTO updateCartEntryInternal(final String baseSiteId, final CartData cart,
			final OrderEntryData orderEntry, final Long qty, final String pickupStore, final String fields, final boolean putMode)
			throws CommerceCartModificationException
	{
		final long entryNumber = orderEntry.getEntryNumber().longValue();
		final String productCode = orderEntry.getProduct().getCode();
		final PointOfServiceData currentPointOfService = orderEntry.getDeliveryPointOfService();

		CartModificationData cartModificationData1 = null;
		CartModificationData cartModificationData2 = null;

		if (!StringUtils.isEmpty(pickupStore))
		{
			if (currentPointOfService == null || !currentPointOfService.getName().equals(pickupStore))
			{
				//was 'shipping mode' or store is changed
				validateForAmbiguousPositions(cart, orderEntry, pickupStore);
				validateIfProductIsInStockInPOS(baseSiteId, productCode, pickupStore, Long.valueOf(entryNumber));
				cartModificationData1 = getCartFacade().updateCartEntry(entryNumber, pickupStore);
			}
		}
		else if (putMode && currentPointOfService != null)
		{
			//was 'pickup in store', now switch to 'shipping mode'
			validateForAmbiguousPositions(cart, orderEntry, pickupStore);
			validateIfProductIsInStockOnline(baseSiteId, productCode, Long.valueOf(entryNumber));
			cartModificationData1 = getCartFacade().updateCartEntry(entryNumber, pickupStore);
		}

		if (qty != null)
		{
			cartModificationData2 = getCartFacade().updateCartEntry(entryNumber, qty.longValue());
		}

		return getDataMapper().map(mergeCartModificationData(cartModificationData1, cartModificationData2),
				CartModificationWsDTO.class, fields);
	}

	@RequestMapping(value = "/{cartId}/entries/{entryNumber}", method = RequestMethod.PUT, consumes =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	@ApiOperation(nickname = "replaceCartEntry", value = "Set quantity and store details of a cart entry.", notes = "Updates the quantity of a single cart entry and the details of the store where the cart entry will be picked up. "
			+ "Attributes not provided in request will be defined again (set to null or default)")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartModificationWsDTO replaceCartEntry(@PathVariable
	final String baseSiteId,
			@ApiParam(value = "The entry number. Each entry in a cart has an entry number. Cart entries are numbered in ascending order, starting with zero (0).", required = true)
			@PathVariable
			final long entryNumber,
			@ApiParam(value = "Request body parameter that contains details such as the quantity of product (quantity), and the pickup store name (deliveryPointOfService.name)\n\nThe DTO is in XML or .json format.", required = true)
			@RequestBody
			final OrderEntryWsDTO entry, @ApiFieldsParam
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws CommerceCartModificationException
	{
		final CartData cart = getSessionCart();
		final OrderEntryData orderEntry = getCartEntryForNumber(cart, entryNumber);
		final String pickupStore = entry.getDeliveryPointOfService() == null ? null : entry.getDeliveryPointOfService().getName();

		validateCartEntryForReplace(orderEntry, entry);

		return updateCartEntryInternal(baseSiteId, cart, orderEntry, entry.getQuantity(), pickupStore, fields, true);
	}

	protected void validateCartEntryForReplace(final OrderEntryData oryginalEntry, final OrderEntryWsDTO entry)
	{
		final String productCode = oryginalEntry.getProduct().getCode();
		final Errors errors = new BeanPropertyBindingResult(entry, ENTRY);
		if (entry.getProduct() != null && entry.getProduct().getCode() != null && !entry.getProduct().getCode().equals(productCode))
		{
			errors.reject("cartEntry.productCodeNotMatch");
			throw new WebserviceValidationException(errors);
		}

		validate(entry, ENTRY, orderEntryReplaceValidator);
	}

	/**
	 * @deprecated since 2005. Please use {@link CartsController#updateCartEntry(String, long, OrderEntryWsDTO, String)}
	 *             instead.
	 */
	@Deprecated(since = "2005", forRemoval = true)
	@RequestMapping(value = "/{cartId}/entries/{entryNumber}", method = RequestMethod.PATCH)
	@ResponseBody
	@ApiOperation(hidden = true, value = "Update quantity and store details of a cart entry.", notes = "Updates the quantity of a single cart entry and the details of the store where the cart entry will be picked up.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartModificationWsDTO updateCartEntry(@PathVariable
	final String baseSiteId,
			@ApiParam(value = "The entry number. Each entry in a cart has an entry number. Cart entries are numbered in ascending order, starting with zero (0).", required = true)
			@PathVariable
			final long entryNumber, @ApiParam(value = "Quantity of product.")
			@RequestParam(required = false)
			final Long qty,
			@ApiParam(value = "Name of the store where product will be picked. Set only if want to pick up from a store.")
			@RequestParam(required = false)
			final String pickupStore, @ApiFieldsParam
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws CommerceCartModificationException
	{
		LOG.debug("updateCartEntry: {}, {}, {}", logParam(ENTRY_NUMBER, entryNumber), logParam("qty", qty),
				logParam(PICKUP_STORE, pickupStore));
		final CartData cart = getSessionCart();
		final OrderEntryData orderEntry = getCartEntryForNumber(cart, entryNumber);

		if (qty == null && StringUtils.isEmpty(pickupStore))
		{
			throw new RequestParameterException("At least one parameter (qty,pickupStore) should be set!",
					RequestParameterException.MISSING);
		}

		if (qty != null)
		{
			validate(qty, "quantity", greaterThanZeroValidator);
		}

		if (pickupStore != null)
		{
			validate(pickupStore, PICKUP_STORE, pointOfServiceValidator);
		}

		return updateCartEntryInternal(baseSiteId, cart, orderEntry, qty, pickupStore, fields, false);
	}

	@RequestMapping(value = "/{cartId}/entries/{entryNumber}", method = RequestMethod.PATCH, consumes =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	@ApiOperation(nickname = "updateCartEntry", value = "Update quantity and store details of a cart entry.", notes = "Updates the quantity of a single cart entry and the details of the store where the cart entry will be picked up.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartModificationWsDTO updateCartEntry(@PathVariable
	final String baseSiteId,
			@ApiParam(value = "The entry number. Each entry in a cart has an entry number. Cart entries are numbered in ascending order, starting with zero (0).", required = true)
			@PathVariable
			final long entryNumber,
			@ApiParam(value = "Request body parameter that contains details such as the quantity of product (quantity), and the pickup store name (deliveryPointOfService.name)\n\nThe DTO is in XML or .json format.", required = true)
			@RequestBody
			final OrderEntryWsDTO entry, @ApiFieldsParam
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws CommerceCartModificationException
	{
		final CartData cart = getSessionCart();
		final OrderEntryData orderEntry = getCartEntryForNumber(cart, entryNumber);

		final String productCode = orderEntry.getProduct().getCode();
		final Errors errors = new BeanPropertyBindingResult(entry, ENTRY);
		if (entry.getProduct() != null && entry.getProduct().getCode() != null && !entry.getProduct().getCode().equals(productCode))
		{
			errors.reject("cartEntry.productCodeNotMatch");
			throw new WebserviceValidationException(errors);
		}

		if (entry.getQuantity() == null)
		{
			entry.setQuantity(orderEntry.getQuantity());
		}

		validate(entry, ENTRY, orderEntryUpdateValidator);

		final String pickupStore = entry.getDeliveryPointOfService() == null ? null : entry.getDeliveryPointOfService().getName();
		return updateCartEntryInternal(baseSiteId, cart, orderEntry, entry.getQuantity(), pickupStore, fields, false);
	}

	@RequestMapping(value = "/{cartId}/entries/{entryNumber}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "removeCartEntry", value = "Deletes cart entry.", notes = "Deletes cart entry.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void removeCartEntry(
			@ApiParam(value = "The entry number. Each entry in a cart has an entry number. Cart entries are numbered in ascending order, starting with zero (0).", required = true)
			@PathVariable
			final long entryNumber) throws CommerceCartModificationException
	{
		LOG.debug("removeCartEntry: {}", logParam(ENTRY_NUMBER, entryNumber));
		final CartData cart = getSessionCart();
		getCartEntryForNumber(cart, entryNumber);
		getCartFacade().updateCartEntry(entryNumber, 0);
	}

	@DeleteMapping(value = "/{cartId}/entrygroups/{entryGroupNumber}")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "removeCartEntryGroup", value = "Delete an entry group.", notes = "Removes an entry group from an associated cart. The entry group is identified by an entryGroupNumber. The cart is identified by the cartId.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void removeEntryGroup(
			@ApiParam(value = "Each entry group in a cart has a specific entry group number. Entry group numbers are integers starting at one. They are defined in ascending order.", required = true)
			@PathVariable
			final int entryGroupNumber) throws CommerceCartModificationException
	{
		validate(entryGroupNumber, "entryGroupNumber", greaterThanZeroValidator);

		final CartModificationData result = getCartFacade().removeEntryGroup(entryGroupNumber);

		if (StringUtils.equals(CommerceCartModificationStatus.INVALID_ENTRY_GROUP_NUMBER, result.getStatusCode()))
		{
			throw new CartEntryGroupException("Entry group not found", CartEntryGroupException.NOT_FOUND,
					String.valueOf(entryGroupNumber));
		}
	}

	/**
	 * @deprecated since 2005. Please use {@link CartsController#createCartDeliveryAddress(AddressWsDTO, String)}
	 *             instead.
	 */
	@Deprecated(since = "1905", forRemoval = true)
	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_GUEST", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/addresses/delivery", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(hidden = true, value = "Creates a delivery address for the cart.", notes = "Creates an address and assigns it to the cart as the delivery address.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public AddressWsDTO createCartDeliveryAddress(final HttpServletRequest request, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		LOG.debug("createCartDeliveryAddress");
		final AddressData addressData = super.createAddressInternal(request);
		final String addressId = addressData.getId();
		super.setCartDeliveryAddressInternal(addressId);
		return getDataMapper().map(addressData, AddressWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_GUEST", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/addresses/delivery", method = RequestMethod.POST, consumes =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "createCartDeliveryAddress", value = "Creates a delivery address for the cart.", notes = "Creates an address and assigns it to the cart as the delivery address.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public AddressWsDTO createCartDeliveryAddress(
			@ApiParam(value = "Request body parameter that contains details such as the customer's first name (firstName), the customer's last name (lastName), the customer's title (titleCode), the customer's phone (phone), "
					+ "the country (country.isocode), the first part of the address (line1), the second part of the address (line2), the town (town), the postal code (postalCode), and the region (region.isocode).\n\nThe DTO is in XML or .json format.", required = true)
			@RequestBody
			final AddressWsDTO address, @ApiFieldsParam
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{
		LOG.debug("createCartDeliveryAddress");
		validate(address, OBJECT_NAME_ADDRESS, getAddressDTOValidator());
		AddressData addressData = getDataMapper().map(address, AddressData.class, ADDRESS_MAPPING);
		addressData = createAddressInternal(addressData);
		setCartDeliveryAddressInternal(addressData.getId());
		return getDataMapper().map(addressData, AddressWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/addresses/delivery", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	@SiteChannelRestriction(allowedSiteChannelsProperty = API_COMPATIBILITY_B2C_CHANNELS)
	@ApiOperation(nickname = "replaceCartDeliveryAddress", value = "Sets a delivery address for the cart.", notes = "Sets a delivery address for the cart. The address country must be placed among the delivery countries of the current base store.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void replaceCartDeliveryAddress(@ApiParam(value = "Address identifier", required = true)
	@RequestParam
	final String addressId)
	{
		super.setCartDeliveryAddressInternal(addressId);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/addresses/delivery", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "removeCartDeliveryAddress", value = "Deletes the delivery address from the cart.", notes = "Deletes the delivery address from the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void removeCartDeliveryAddress()
	{
		LOG.debug("removeCartDeliveryAddress");
		if (!getCheckoutFacade().removeDeliveryAddress())
		{
			throw new CartException("Cannot reset address!", CartException.CANNOT_RESET_ADDRESS);
		}
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/deliverymode", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCartDeliveryMode", value = "Get the delivery mode selected for the cart.", notes = "Returns the delivery mode selected for the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public DeliveryModeWsDTO getCartDeliveryMode(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		LOG.debug("getCartDeliveryMode");
		return getDataMapper().map(getSessionCart().getDeliveryMode(), DeliveryModeWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/deliverymode", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "replaceCartDeliveryMode", value = "Sets the delivery mode for a cart.", notes = "Sets the delivery mode with a given identifier for the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void replaceCartDeliveryMode(@ApiParam(value = "Delivery mode identifier (code)", required = true)
	@RequestParam(required = true)
	final String deliveryModeId) throws UnsupportedDeliveryModeException
	{
		super.setCartDeliveryModeInternal(deliveryModeId);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/deliverymode", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "removeCartDeliveryMode", value = "Deletes the delivery mode from the cart.", notes = "Deletes the delivery mode from the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void removeCartDeliveryMode()
	{
		LOG.debug("removeCartDeliveryMode");
		if (!getCheckoutFacade().removeDeliveryMode())
		{
			throw new CartException("Cannot reset delivery mode!", CartException.CANNOT_RESET_DELIVERYMODE);
		}
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/deliverymodes", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCartDeliveryModes", value = "Get all delivery modes for the current store and delivery address.", notes = "Returns all delivery modes supported for the "
			+ "current base store and cart delivery address. A delivery address must be set for the cart, otherwise an empty list will be returned.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public DeliveryModeListWsDTO getCartDeliveryModes(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		LOG.debug("getCartDeliveryModes");
		final DeliveryModesData deliveryModesData = new DeliveryModesData();
		deliveryModesData.setDeliveryModes(getCheckoutFacade().getSupportedDeliveryModes());

		return getDataMapper().map(deliveryModesData, DeliveryModeListWsDTO.class, fields);
	}

	/**
	 * @deprecated since 2005. Please use {@link CartsController#createCartPaymentDetails(PaymentDetailsWsDTO, String)}
	 *             instead.
	 */
	@Deprecated(since = "2005", forRemoval = true)
	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/paymentdetails", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(hidden = true, value = "Defines and assigns details of a new credit card payment to the cart.", notes = "Defines the details of a new credit card, and assigns this payment option to the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public PaymentDetailsWsDTO createCartPaymentDetails(final HttpServletRequest request, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields) throws InvalidPaymentInfoException, NoCheckoutCartException, UnsupportedRequestException
	{
		paymentProviderRequestSupportedStrategy.checkIfRequestSupported("addPaymentDetails");
		final CCPaymentInfoData paymentInfoData = super.addPaymentDetailsInternal(request).getPaymentInfo();
		return getDataMapper().map(paymentInfoData, PaymentDetailsWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/payment/info", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getPaymentInfo", value = "Get getPaymentInfo for the current store and delivery address.", notes = "Returns payment info")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public PaymentRequestWsDTO getPaymentInfo(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws PaymentException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("getPaymentInfo");
		}
		if (!cartService.hasSessionCart())
		{
			throw new PaymentException("No cart found!", null);
		}
		refreshMobileCartCalculation(cartService.getSessionCart());

		final Optional<PaymentRequestData> paymentDataByCurrentStore = paymentContext
				.getPaymentDataByCurrentStore(cartService.getSessionCart());

		if (!paymentDataByCurrentStore.isPresent())
		{
			throw new PaymentException("Empty payment data", null);
		}

		if (paymentDataByCurrentStore.get().getPaymentProviderData() instanceof MpgsPaymentRequestData)
		{
			final MpgsPaymentRequestData data = (MpgsPaymentRequestData) paymentDataByCurrentStore.get().getPaymentProviderData();


			savePaymentTransactionInfo("PaymentInfo-After", data);


			return getDataMapper().map(data, PaymentRequestWsDTO.class, fields);
		}
		return new PaymentRequestWsDTO();
	}


	private void refreshMobileCartCalculation(final CartModel cartModel)
	{

		cartModel.setCalculated(Boolean.FALSE);
		getModelService().save(cartModel);
		getCommerceCheckoutService().calculateCart(cartModel);
		getModelService().save(cartModel);
	}

	/**
	 * @param operationType
	 * @param data
	 * @param order
	 */
	private void savePaymentTransactionInfo(final String operationType, final MpgsPaymentRequestData data)
	{

		if (!cartService.hasSessionCart())
		{
			return;
		}

		final CartModel order = cartService.getSessionCart();
		final PaymentTransactionInfoModel savePaymentTransactionInfo = paymentTransactionInfoService
				.savePaymentTransactionInfo("PaymentInfo-After", data.getSessionId(), data.getMerchantId(), data.getApiKey(), order);

		String sessionIdInfoStr = "";
		String error = "";
		final MpgsConstants mpgsConstants = new MpgsConstants(data.getMerchantId(), data.getApiKey(), data.getVersion(),
				data.getScriptSrc(), data.getBaseURL());


		Optional<RetrieveSessionBean> sessionInfo = Optional.empty();
		try
		{
			sessionInfo = mpgsService.getSessionInfo(data.getSessionId(), mpgsConstants);
		}
		catch (final MpgsException e1)
		{
			LOG.error(e1.getMessage());

		}
		if (sessionInfo.isEmpty())
		{
			error = PaymentExceptionType.INVALID_SESSION_ID.getMessage();
			paymentTransactionInfoService.updatePaymentTransactionInfo(savePaymentTransactionInfo, sessionIdInfoStr, 0, error);
			LOG.error(error);
			return;
		}

		//		sessionIdInfoStr = GSON.toJson(sessionInfo.get(), RetrieveSessionBean.class);
		sessionIdInfoStr = sessionInfo.get().toString();


		if (sessionInfo.get().getOrder() == null || StringUtils.isBlank(sessionInfo.get().getOrder().getAmount()))
		{
			error = PaymentExceptionType.INVALID_SESSION_ID.getMessage() + ":[" + sessionIdInfoStr + "]";
			paymentTransactionInfoService.updatePaymentTransactionInfo(savePaymentTransactionInfo, sessionIdInfoStr, 0, error);
			LOG.error(error);
			return;
		}



		double sessionInfoAmount = 0.0d;
		double totalPriceWithTax = 0.0d;

		try
		{
			sessionInfoAmount = Double.parseDouble(sessionInfo.get().getOrder().getAmount());
		}
		catch (final NumberFormatException e)
		{
			sessionInfoAmount = 0.0d;
		}
		try
		{
			totalPriceWithTax = Double.parseDouble(savePaymentTransactionInfo.getTotalPriceWithTax());
		}
		catch (final NumberFormatException e)
		{
			totalPriceWithTax = 0.0d;
		}



		if (sessionInfoAmount != totalPriceWithTax)
		{
			error = PaymentExceptionType.INVALID_SESSION_AMOUNT.getMessage() + ":order ammount[" + totalPriceWithTax
					+ "],sessionInfoAmount[" + sessionInfoAmount + "]";
			paymentTransactionInfoService.updatePaymentTransactionInfo(savePaymentTransactionInfo, sessionIdInfoStr,
					sessionInfoAmount, error);

			LOG.error(error);
			return;
		}
		paymentTransactionInfoService.updatePaymentTransactionInfo(savePaymentTransactionInfo, sessionIdInfoStr, sessionInfoAmount,
				null);

	}


	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/payment/3ds-check/initiate", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(nickname = "init3DSecure", value = " init3DSecure for the current store and delivery address.", notes = "Returns payment transaction")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public Map init3DSecure(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields, @ApiParam(required = true)
			@RequestParam(required = true)
			final String sessionId) throws PaymentException
	{
		if (!cartService.hasSessionCart())
		{
			throw new PaymentException("No cart found!", null);
		}
		final Optional<PaymentResponseData> auth3DSResult = paymentContext
				.initiate3DSecureCheckByCurrentStore(cartService.getSessionCart(), sessionId);
		if (auth3DSResult.isEmpty())
		{
			throw new MpgsException(MpgsExceptionType.UNKNOWN, sessionId, null);
		}
		return auth3DSResult.get().getResponseData();
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/payment/3ds-check/authenticate", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(nickname = "auth3DSecure", value = "authenticate for the current store and delivery address.", notes = "Returns payment transaction")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public Map auth3DSecure(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields, @ApiParam(required = true)
			@RequestParam(required = true)
			final String sessionId, @ApiParam(required = true)
			@RequestParam(required = true)
			final String secureId) throws PaymentException
	{
		if (!cartService.hasSessionCart())
		{
			throw new PaymentException("No cart found!", null);
		}
		final Optional<PaymentResponseData> auth3DSResult = paymentContext
				.authenticate3DSecurePayerByCurrentStore(cartService.getSessionCart(), sessionId, secureId);
		if (auth3DSResult.isEmpty())
		{
			throw new MpgsException(MpgsExceptionType.UNKNOWN, sessionId, null);
		}
		return auth3DSResult.get().getResponseData();
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/payment/pay", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(nickname = "payOrder", value = "Pay the order and Set getPaymentTransaction for the current store and delivery address.", notes = "Returns payment transaction")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public Map payOrder(
			@ApiParam(value = "3DS response from the webview needed to proceed with the payment  required if paymentdevice is false", required = false)
			@RequestBody
			final Payment3DSResultWsDTO result,
			@ApiParam(value = "Was the order paid via a device", required = false, defaultValue = "false")
			@RequestParam(required = false)
			final boolean paymentDevice) throws PaymentException
	{
		if (paymentDevice)
		{
			result.setTransactionId(null);
			result.setFullResponse(null);
			result.setResponseRecommendation(null);
			result.setResult(null);
		}


		if (paymentDevice == false && (result == null || result.getTransactionId() == null || result.getTransactionId().isBlank()
				|| "undefined".equals(result.getTransactionId()) || "string".equals(result.getTransactionId())))
		{

			throw new IllegalArgumentException("Must provide transactionId if result is false.");
		}
		if (!cartService.hasSessionCart())
		{
			throw new PaymentException("No cart found!", null);
		}

		validate(result, "result", paymentDevice ? threeDSResultForPaymentDeviceValidator : threeDSResultValidator);

		if (!paymentDevice)
		{
			MpgsResultEnum resultEnum = MpgsResultEnum.UNKNOWN;
			try
			{
				resultEnum = MpgsResultEnum.valueOf(result.getResult());
			}
			catch (final Exception e)
			{
				throw new PaymentException(e.getMessage(), null);
			}
			if (!MpgsResultEnum.SUCCESS.equals(resultEnum))
			{
				throw new PaymentException("Result status is not successful, cannot proceed with the payment", null);
			}
		}

		final Optional<PaymentResponseData> payment = paymentContext.payOrderByCurrentStore(cartService.getSessionCart(),
				result.getSessionId(), result.getTransactionId(), result.getFullResponse());

		if (payment.isEmpty())
		{
			throw new PaymentException("Cannot proceed with the payment due to failure transaction", null);
		}

		return payment.get().getResponseData();
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/payment/transaction", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(nickname = "setPaymentTransactionForPaymentDevice", value = "Pay the order and Set getPaymentTransaction for Payment Device  for the current store and delivery address.", notes = "Returns payment transaction")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public PaymentDetailsWsDTO setPaymentTransactionForPaymentDevice() throws PaymentException
	{
		final CartModel cartModel = cartService.getSessionCart();

		LOG.info("start payment/transaction  cart code[{}] , order code [{}] ", cartModel.getCode(), cartModel.getOrderCode());

		PaymentSubscriptionResultData paymentOrderStatusResponseData;
		try
		{
			paymentOrderStatusResponseData = checkoutFacade.completePaymentTransactionForOCC(null);
			LOG.info("payment/transaction -> after completePaymentTransactionForOCC  cart code[{}] , order code [{}] ",
					cartModel.getCode(), cartModel.getOrderCode());

		}
		catch (InvalidCartException | PaymentException e)
		{
			throw new PaymentException(e.getMessage(), null);
		}

		if (paymentOrderStatusResponseData == null)
		{
			throw new PaymentException("Could not complete the payment transaction!", null);
		}

		final CCPaymentInfoData storedCard = paymentOrderStatusResponseData.getStoredCard();
		return getDataMapper().map(storedCard, PaymentDetailsWsDTO.class);
	}


	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/payment/transaction/{transactionId}", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(nickname = "setPaymentTransaction", value = "Pay the order and Set getPaymentTransaction for the current store and delivery address.", notes = "Returns payment transaction")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public PaymentDetailsWsDTO setPaymentTransaction(
			@ApiParam(value = "3DS response from the webview needed to proceed with the payment", required = true)
			@PathVariable(name = "transactionId")
			final String transactionId) throws PaymentException
	{
		PaymentSubscriptionResultData paymentOrderStatusResponseData;
		try
		{
			paymentOrderStatusResponseData = checkoutFacade.completePaymentTransactionForOCC(transactionId);
		}
		catch (InvalidCartException | PaymentException e)
		{
			throw new PaymentException(e.getMessage(), null);
		}

		if (paymentOrderStatusResponseData == null)
		{
			throw new PaymentException("Could not complete the payment transaction!", null);
		}

		final CCPaymentInfoData storedCard = paymentOrderStatusResponseData.getStoredCard();
		return getDataMapper().map(storedCard, PaymentDetailsWsDTO.class);
	}


	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/paymentdetails", method = RequestMethod.POST, consumes =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "createCartPaymentDetails", value = "Defines and assigns details of a new credit card payment to the cart.", notes = "Defines the details of a new credit card, and assigns this payment option to the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public PaymentDetailsWsDTO createCartPaymentDetails(
			@ApiParam(value = "Request body parameter that contains details such as the name on the card (accountHolderName), the card number (cardNumber), the card type (cardType.code), "
					+ "the month of the expiry date (expiryMonth), the year of the expiry date (expiryYear), whether the payment details should be saved (saved), whether the payment details "
					+ "should be set as default (defaultPaymentInfo), and the billing address (billingAddress.firstName, billingAddress.lastName, billingAddress.titleCode, billingAddress.country.isocode, "
					+ "billingAddress.line1, billingAddress.line2, billingAddress.town, billingAddress.postalCode, billingAddress.region.isocode)\n\nThe DTO is in XML or .json format.", required = true)
			@RequestBody
			final PaymentDetailsWsDTO paymentDetails, @ApiFieldsParam
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws InvalidPaymentInfoException, NoCheckoutCartException, UnsupportedRequestException
	{
		paymentProviderRequestSupportedStrategy.checkIfRequestSupported("addPaymentDetails");
		validatePayment(paymentDetails);
		final String copiedfields = "accountHolderName,cardNumber,cardType,cardTypeData(code),expiryMonth,expiryYear,issueNumber,startMonth,startYear,subscriptionId,defaultPaymentInfo,saved,billingAddress(titleCode,firstName,lastName,line1,line2,town,postalCode,country(isocode),region(isocode),defaultAddress)";
		CCPaymentInfoData paymentInfoData = getDataMapper().map(paymentDetails, CCPaymentInfoData.class, copiedfields);
		paymentInfoData = addPaymentDetailsInternal(paymentInfoData).getPaymentInfo();
		return getDataMapper().map(paymentInfoData, PaymentDetailsWsDTO.class, fields);
	}

	protected void validatePayment(final PaymentDetailsWsDTO paymentDetails) throws NoCheckoutCartException
	{
		if (!getCheckoutFacade().hasCheckoutCart())
		{
			throw new NoCheckoutCartException("Cannot add PaymentInfo. There was no checkout cart created yet!");
		}
		validate(paymentDetails, "paymentDetails", getPaymentDetailsDTOValidator());
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/paymentdetails", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "replaceCartPaymentDetails", value = "Sets credit card payment details for the cart.", notes = "Sets credit card payment details for the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void replaceCartPaymentDetails(@ApiParam(value = "Payment details identifier.", required = true)
	@RequestParam
	final String paymentDetailsId) throws InvalidPaymentInfoException
	{
		super.setPaymentDetailsInternal(paymentDetailsId);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_CLIENT", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/promotions", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCartPromotions", value = "Get information about promotions applied on cart.", notes = "Returns information about the promotions applied on the cart. "
			+ "Requests pertaining to promotions have been developed for the previous version of promotions and vouchers, and as a result, some of them "
			+ "are currently not compatible with the new promotions engine.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public PromotionResultListWsDTO getCartPromotions(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		LOG.debug("getCartPromotions");
		final List<PromotionResultData> appliedPromotions = new ArrayList<>();
		final List<PromotionResultData> orderPromotions = getSessionCart().getAppliedOrderPromotions();
		final List<PromotionResultData> productPromotions = getSessionCart().getAppliedProductPromotions();
		appliedPromotions.addAll(orderPromotions);
		appliedPromotions.addAll(productPromotions);

		final PromotionResultDataList dataList = new PromotionResultDataList();
		dataList.setPromotions(appliedPromotions);
		return getDataMapper().map(dataList, PromotionResultListWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_CLIENT", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/promotions/{promotionId}", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCartPromotion", value = "Get information about promotion applied on cart.", notes = "Returns information about a promotion (with a specific promotionId), that has "
			+ "been applied on the cart. Requests pertaining to promotions have been developed for the previous version of promotions and vouchers, and as a result, some "
			+ "of them are currently not compatible with the new promotions engine.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public PromotionResultListWsDTO getCartPromotion(@ApiParam(value = "Promotion identifier (code)", required = true)
	@PathVariable
	final String promotionId, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		LOG.debug("getCartPromotion: promotionId = {}", sanitize(promotionId));
		final List<PromotionResultData> appliedPromotions = new ArrayList<>();
		final List<PromotionResultData> orderPromotions = getSessionCart().getAppliedOrderPromotions();
		final List<PromotionResultData> productPromotions = getSessionCart().getAppliedProductPromotions();
		for (final PromotionResultData prd : orderPromotions)
		{
			if (prd.getPromotionData().getCode().equals(promotionId))
			{
				appliedPromotions.add(prd);
			}
		}
		for (final PromotionResultData prd : productPromotions)
		{
			if (prd.getPromotionData().getCode().equals(promotionId))
			{
				appliedPromotions.add(prd);
			}
		}

		final PromotionResultDataList dataList = new PromotionResultDataList();
		dataList.setPromotions(appliedPromotions);
		return getDataMapper().map(dataList, PromotionResultListWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/promotions", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "doApplyCartPromotion", value = "Enables promotions based on the promotionsId of the cart.", notes = "Enables a promotion for the order based on the promotionId defined for the cart. "
			+ "Requests pertaining to promotions have been developed for the previous version of promotions and vouchers, and as a result, some of them are currently not compatible "
			+ "with the new promotions engine.", authorizations =
	{ @Authorization(value = "oauth2_client_credentials") })
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void doApplyCartPromotion(@ApiParam(value = "Promotion identifier (code)", required = true)
	@RequestParam(required = true)
	final String promotionId) throws CommercePromotionRestrictionException
	{
		LOG.debug("doApplyCartPromotion: promotionId = {}", sanitize(promotionId));
		commercePromotionRestrictionFacade.enablePromotionForCurrentCart(promotionId);
	}

	@Secured(
	{ "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/promotions/{promotionId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "removeCartPromotion", value = "Disables the promotion based on the promotionsId of the cart.", notes = "Disables the promotion for the order based on the promotionId defined for the cart. "
			+ "Requests pertaining to promotions have been developed for the previous version of promotions and vouchers, and as a result, some of them are currently not compatible with "
			+ "the new promotions engine.", authorizations =
	{ @Authorization(value = "oauth2_client_credentials") })
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void removeCartPromotion(@ApiParam(value = "Promotion identifier (code)", required = true)
	@PathVariable
	final String promotionId) throws CommercePromotionRestrictionException
	{
		LOG.debug("removeCartPromotion: promotionId = {}", sanitize(promotionId));
		commercePromotionRestrictionFacade.disablePromotionForCurrentCart(promotionId);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_CUSTOMERGROUP", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_GUEST" })
	@RequestMapping(value = "/{cartId}/vouchers", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCartVouchers", value = "Get a list of vouchers applied to the cart.", notes = "Returns a list of vouchers applied to the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public VoucherListWsDTO getCartVouchers(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		LOG.debug("getVouchers");
		final VoucherDataList dataList = new VoucherDataList();
		dataList.setVouchers(voucherFacade.getVouchersForCart());
		return getDataMapper().map(dataList, VoucherListWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_CUSTOMERGROUP", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_GUEST" })
	@RequestMapping(value = "/{cartId}/vouchers", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "doApplyCartVoucher", value = "Applies a voucher based on the voucherId defined for the cart.", notes = "Applies a voucher based on the voucherId defined for the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void doApplyCartVoucher(@ApiParam(value = "Voucher identifier (code)", required = true)
	@RequestParam
	final String voucherId) throws NoCheckoutCartException, VoucherOperationException
	{
		super.applyVoucherForCartInternal(voucherId);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_CUSTOMERGROUP", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_GUEST" })
	@RequestMapping(value = "/{cartId}/vouchers/{voucherId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "removeCartVoucher", value = "Deletes a voucher defined for the current cart.", notes = "Deletes a voucher based on the voucherId defined for the current cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void removeCartVoucher(@ApiParam(value = "Voucher identifier (code)", required = true)
	@PathVariable
	final String voucherId) throws NoCheckoutCartException, VoucherOperationException
	{
		LOG.debug("release voucher : voucherCode = {}", sanitize(voucherId));
		if (!getCheckoutFacade().hasCheckoutCart())
		{
			throw new NoCheckoutCartException("Cannot realese voucher. There was no checkout cart created yet!");
		}
		getVoucherFacade().releaseVoucher(voucherId);
	}

	@PostMapping(path = "/{cartId}/validate")
	@ResponseBody
	@ApiOperation(nickname = "validateCart", value = "Validates the cart", notes = "Runs a cart validation and returns the result.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartModificationListWsDTO validateCart(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields) throws CommerceCartModificationException
	{
		LOG.debug("validateCart");
		final CartModificationDataList cartModificationDataList = new CartModificationDataList();

		final List<CartVoucherValidationData> invalidVouchers = getCartVoucherValidator()
				.validate(getSessionCart().getAppliedVouchers());
		// when a voucher is invalid validateCartData removes it from a cart
		final List<CartModificationData> cartValidationResults = getCartFacade().validateCartData();

		cartModificationDataList.setCartModificationList(replaceVouchersValidationResults(cartValidationResults, invalidVouchers));
		return getDataMapper().map(cartModificationDataList, CartModificationListWsDTO.class, fields);
	}

	protected List<CartModificationData> replaceVouchersValidationResults(final List<CartModificationData> cartModifications,
			final List<CartVoucherValidationData> inValidVouchers)
	{
		if (CollectionUtils.isEmpty(inValidVouchers))
		{
			// do not replace
			return cartModifications;
		}

		final Predicate<CartModificationData> isNotVoucherModification = modification -> !COUPON_STATUS_CODE
				.equals(modification.getStatusCode()) && !VOUCHER_STATUS_CODE.equals(modification.getStatusCode());

		return Collections.unmodifiableList(Stream.concat( //
				cartModifications.stream().filter(isNotVoucherModification), //
				inValidVouchers.stream().map(this::createCouponValidationResult) //
		).collect(Collectors.toList()));
	}

	protected CartModificationData createCouponValidationResult(final CartVoucherValidationData voucherValidationData)
	{
		final CartModificationData cartModificationData = new CartModificationData();
		cartModificationData.setStatusCode(COUPON_STATUS_CODE);
		cartModificationData.setStatusMessage(voucherValidationData.getSubject());
		return cartModificationData;
	}

	protected void validateIfProductIsInStockInPOS(final String baseSiteId, final String productCode, final String storeName,
			final Long entryNumber)
	{
		if (!commerceStockFacade.isStockSystemEnabled(baseSiteId))
		{
			throw new StockSystemException("Stock system is not enabled on this site", StockSystemException.NOT_ENABLED, baseSiteId);
		}
		final StockData stock = commerceStockFacade.getStockDataForProductAndPointOfService(productCode, storeName);
		if (stock != null && stock.getStockLevelStatus().equals(StockLevelStatus.OUTOFSTOCK))
		{
			if (entryNumber != null)
			{
				throw new LowStockException("Product [" + sanitize(productCode) + "] is currently out of stock",
						LowStockException.NO_STOCK, String.valueOf(entryNumber));
			}
			else
			{
				throw new ProductLowStockException("Product [" + sanitize(productCode) + "] is currently out of stock",
						LowStockException.NO_STOCK, productCode);
			}
		}
		else if (stock != null && stock.getStockLevelStatus().equals(StockLevelStatus.LOWSTOCK))
		{
			if (entryNumber != null)
			{
				throw new LowStockException("Not enough product in stock", LowStockException.LOW_STOCK, String.valueOf(entryNumber));
			}
			else
			{
				throw new ProductLowStockException("Not enough product in stock", LowStockException.LOW_STOCK, productCode);
			}
		}
	}

	protected void validateIfProductIsInStockOnline(final String baseSiteId, final String productCode, final Long entryNumber)
	{
		if (!commerceStockFacade.isStockSystemEnabled(baseSiteId))
		{
			throw new StockSystemException("Stock system is not enabled on this site", StockSystemException.NOT_ENABLED, baseSiteId);
		}
		final StockData stock = commerceStockFacade.getStockDataForProductAndBaseSite(productCode, baseSiteId);
		if (stock != null && stock.getStockLevelStatus().equals(StockLevelStatus.OUTOFSTOCK))
		{
			if (entryNumber != null)
			{
				throw new LowStockException("Product [" + sanitize(productCode) + "] cannot be shipped - out of stock online",
						LowStockException.NO_STOCK, String.valueOf(entryNumber));
			}
			else
			{
				throw new ProductLowStockException("Product [" + sanitize(productCode) + "] cannot be shipped - out of stock online",
						LowStockException.NO_STOCK, productCode);
			}
		}
	}

	@RequestMapping(value = "/{cartId}/verification", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(nickname = "getCartVerification", value = "Get CartVerification for the current store and delivery address.", notes = "Returns valid flag and errors if not valid")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public CartCheckOutValidatorWsDTO getCartVerification(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws InvalidCartException, WebserviceValidationException, NoCheckoutCartException
	{

		if (LOG.isDebugEnabled())
		{
			LOG.debug("getCartVerification");
		}

		validateCart();

		final CartCheckOutValidatorData cartCheckOutValidatorData = new CartCheckOutValidatorData();
		cartCheckOutValidatorData.setValid(true);
		final List<String> errors = new LinkedList<>();


		final boolean isValidTime = checkoutFacade.validateCartTimeSlot();
		if (!isValidTime)
		{
			cartCheckOutValidatorData.setValid(false);
			errors.add("Time slot not valid.");

		}



		//******* Max and Min Validation **********//
		try
		{

			final boolean validateCartMaxAmount = cartValidationService.validateCartMaxAmountByCurrentCart();

		}
		catch (final CartValidationException ex)
		{
			if (CartExceptionType.MAX_AMOUNT.equals(ex.getCartExceptionType()))
			{
				final String[] errorParm = ex.getErrorParm();
				cartCheckOutValidatorData.setValid(false);

				errors.add(getMessageSource().getMessage("max2.cart.amount.value.error", errorParm,
						"Please note, The maximum order amount is {0} {1}", null));




			}
		}
		try
		{

			final boolean validateCartMaxAmount = cartValidationService.validateCartMinAmountByCurrentCart();

		}
		catch (final CartValidationException ex)
		{
			if (CartExceptionType.MIN_AMOUNT.equals(ex.getCartExceptionType()))
			{
				final String[] errorParm = ex.getErrorParm();
				cartCheckOutValidatorData.setValid(false);

				errors.add(getMessageSource().getMessage("min2.cart.CustomAcceleratorCheckoutFacade.value.error", errorParm,
						"Min order amount is {0} {1}, please add {4} {1} more to continue with checkout.", null));

			}
		}
		try
		{
			final boolean validateCartDeliveryTypeByCurrentCart = cartValidationService.validateCartDeliveryTypeByCurrentCart();
		}
		catch (final CartValidationException ex)
		{
			if (CartExceptionType.SHIPPINGMETHOD.equals(ex.getCartExceptionType()))
			{
				cartCheckOutValidatorData.setValid(false);
				errors.add(getMessageSource().getMessage("cart.validation.contains.both.delivery.and.pickup", null,
						"Your cart cannot contain pick up from store and shipping items", null));
			}
		}

		if (!cartCheckOutValidatorData.getValid())
		{


			cartCheckOutValidatorData.setErrors(errors);
		}

		return getDataMapper().map(cartCheckOutValidatorData, CartCheckOutValidatorWsDTO.class, fields);
	}


	/**
	 * @param sessionCart
	 * @return
	 * @throws ParseException
	 */
	private boolean validateCartTimeSlot(final CartModel cart) throws ParseException
	{


		final TimeSlotInfoModel timeslot = cart.getTimeSlotInfo();
		final Date currentDate = new Date();
		final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
		final Date timesSlotDate = dateFormatter.parse(timeslot.getDate());
		final String[] hoursAndMinutes = timeslot.getEnd().split(":");
		timesSlotDate.setHours(Integer.valueOf(hoursAndMinutes[0]));
		timesSlotDate.setMinutes(Integer.valueOf(hoursAndMinutes[1]));
		return currentDate.before(timesSlotDate);

	}


	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/timeSlot", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCartTimeSlot", value = "Get timeSlot for the current store.", notes = "Returns timeSlot "
			+ "current base store and cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public TimeSlotWsDTO getCartTimeSlot(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws TimeSlotException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("getCartTimeSlot");
		}

		final Optional<TimeSlotData> supportedTimeSlot = getCheckoutFacade().getSupportedTimeSlot();

		TimeSlotData timeSlotData = null;
		if (supportedTimeSlot.isPresent())
		{
			timeSlotData = supportedTimeSlot.get();
		}
		else
		{
			throw new TimeSlotException(TimeSlotExceptionType.NO_TIMESLOTS_FOUND,
					"Timeslot requires a missing attribute to be selected.");
		}

		return getDataMapper().map(timeSlotData, TimeSlotWsDTO.class, fields);
	}




	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/timeSlot", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "setTimeSlot", value = "Defines and assigns timeSlot to the cart.", notes = "Defines and assigns timeSlot to the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void setTimeSlot(final HttpServletRequest request, //NOSONAR
			@RequestBody
			final TimeSlotInfoWsDTO timeSlotInfos, //NOSONAR
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws InvalidPaymentInfoException, NoCheckoutCartException, UnsupportedRequestException
	{

		validateTimeSlot(timeSlotInfos);

		final TimeSlotInfoData data = new TimeSlotInfoData();
		data.setDate(timeSlotInfos.getDate());
		data.setDay(timeSlotInfos.getDay());
		data.setEnd(timeSlotInfos.getEnd());
		data.setStart(timeSlotInfos.getStart());
		data.setPeriodCode(timeSlotInfos.getPeriodCode());

		getCheckoutFacade().setTimeSlot(data);
	}

	protected void validateTimeSlot(final TimeSlotInfoWsDTO timeSlotInfos) throws NoCheckoutCartException
	{
		if (!getCheckoutFacade().hasCheckoutCart())
		{
			throw new NoCheckoutCartException("Cannot add timeSlotInfos. There was no checkout cart created yet!");
		}
		validate(timeSlotInfos, "timeSlotInfos", getTimeSlotInfoValidator());
	}

	private SalesApplication getSalesApplication(final String applicationType)
	{
		if (StringUtils.isBlank(applicationType))
		{
			return SalesApplication.WEBMOBILE;
		}
		switch (applicationType)
		{
			case "MOBILE":
				return SalesApplication.WEBMOBILE;
			case "WEB":
				return SalesApplication.WEB;
			case "WHATSAPP":
				return SalesApplication.WHATSAPP;
			case "IOS":
				return SalesApplication.IOS;
			case "ANDROID":
				return SalesApplication.ANDROID;
			default:
				return SalesApplication.WEBMOBILE;
		}
	}



	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/paymentmodes", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCartPaymentModes", value = "Get all payment modes for the current store.", notes = "Returns all payment modes supported for the "
			+ "current base store and cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public PaymentModeListWsDTO getCartPaymentModes(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields,
			@ApiParam(value = "Application type if its mobile or other parties. Examples:MOBILE, WEB, WHATSAPP, IOS, ANDROID", allowableValues = "MOBILE, WEB, WHATSAPP, IOS, ANDROID")
			@RequestParam(required = false, defaultValue = "MOBILE")
			final String applicationType


	)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("getCartPaymentModes");
		}

		final SalesApplication salesApplication = getSalesApplication(applicationType);
		final List<PaymentModeData> supportedPaymentModes = getCheckoutFacade().getSupportedPaymentModes(true, false,
				salesApplication);
		final PaymentModesData paymentModesData = new PaymentModesData();
		paymentModesData.setPaymentModes(supportedPaymentModes);
		//		if (CollectionUtils.isEmpty(supportedPaymentModes)&& includeApplePay && includeGooglePay)
		//		{
		//			paymentModesData.setPaymentModes(supportedPaymentModes.get());
		//		}
		//		else if (supportedPaymentModes.isPresent() && includeGooglePay)
		//		{
		//			paymentModesData.setPaymentModes(
		//					supportedPaymentModes.get().stream().filter(mode -> !"apple".equals(mode.getCode())).collect(Collectors.toList()));
		//		}
		//
		//		else if (supportedPaymentModes.isPresent() && includeApplePay)
		//		{
		//			paymentModesData.setPaymentModes(supportedPaymentModes.get().stream().filter(mode -> !"google".equals(mode.getCode()))
		//					.collect(Collectors.toList()));
		//		}
		//
		//				else if (supportedPaymentModes.isPresent())
		//				{
		//					paymentModesData.setPaymentModes(supportedPaymentModes.get().stream().filter(mode -> !"google".equals(mode.getCode())).filter(mode -> !"apple".equals(mode.getCode()))
		//							.collect(Collectors.toList()));
		//				}
		return getDataMapper().map(paymentModesData, PaymentModeListWsDTO.class, fields);
	}



	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/paymentmodes", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "setPaymentMode", value = "Defines and assigns details of a new credit card payment to the cart.", notes = "Defines the details of a new credit card, and assigns this payment option to the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public PaymentModeWsDTO setPaymentMode(final HttpServletRequest request, //NOSONAR
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields, @RequestParam(required = true)
			final String paymentModeCode) throws InvalidPaymentInfoException, NoCheckoutCartException, UnsupportedRequestException
	{
		validate(paymentModeCode, "paymentModeCode", getPaymentModeValidator());
		getCheckoutFacade().setPaymentMode(paymentModeCode);
		final CartData cart = getSessionCart();
		AddressData address = cart.getDeliveryAddress();
		getCheckoutFacade().resetPaymentInfo();

		switch (paymentModeCode.toLowerCase())
		{
			case "pis": // NOSONAR
				address = getBillingAddressForPickup(cart);
				if (address != null)
				{
					address.setTitleCode("mr");
				}
			case "cod":
			case "ccod":
			case "continue":
				final NoCardPaymentInfoData noCardPaymentInfoData = new NoCardPaymentInfoData();
				final NoCardTypeData noCardTypeData = new NoCardTypeData();
				noCardTypeData.setCode(paymentModeCode.toUpperCase());
				noCardPaymentInfoData.setNoCardTypeData(noCardTypeData);
				noCardPaymentInfoData.setDefaultPaymentInfo(true);
				noCardPaymentInfoData.setBillingAddress(address);
				noCardPaymentInfoData.setSaved(true);

				final Optional<NoCardPaymentInfoData> createPaymentSubscription = getCheckoutFacade()
						.createPaymentSubscription(noCardPaymentInfoData);
				setPaymentSubscription(createPaymentSubscription.orElse(null));
				break;

			default:
				break;


		}
		getCheckoutFacade().saveBillingAddress(address);

		final Optional<List<PaymentModeData>> supportedPaymentModes = getCheckoutFacade().getSupportedPaymentModes();
		PaymentModeData paymentModeData = null;
		if (supportedPaymentModes.isPresent())
		{
			final List<PaymentModeData> supportedPaymentModesList = supportedPaymentModes.get();
			paymentModeData = supportedPaymentModesList.stream().filter(pm -> paymentModeCode.equals(pm.getCode())).findFirst()
					.orElse(null);
		}
		return getDataMapper().map(paymentModeData, PaymentModeWsDTO.class, fields);
	}

	private AddressData getBillingAddressForPickup(final CartData cartData)
	{
		if (!CollectionUtils.isEmpty(cartData.getEntries()) && isSinglePOS(cartData.getEntries()))
		{
			final Optional<OrderEntryData> findFirst = cartData.getEntries().stream()
					.filter(e -> e.getDeliveryPointOfService() != null).filter(e -> e.getDeliveryPointOfService().getAddress() != null)
					.findFirst();
			return findFirst.isPresent() ? findFirst.get().getDeliveryPointOfService().getAddress() : getDefaultDeliveryOfOrigin();
		}
		return getDefaultDeliveryOfOrigin();
	}

	private AddressData getDefaultDeliveryOfOrigin()
	{
		final PointOfServiceModel defaultDeliveryOrigin = baseStoreService.getCurrentBaseStore().getDefaultDeliveryOrigin();
		return defaultDeliveryOrigin != null && defaultDeliveryOrigin.getAddress() != null
				? addressConverter.convert(defaultDeliveryOrigin.getAddress())
				: null;
	}

	private boolean isSinglePOS(final List<OrderEntryData> entries)
	{
		final List<String> pointsOfServiceNames = entries.stream().filter(e -> e.getDeliveryPointOfService() != null)
				.map(e -> e.getDeliveryPointOfService().getName()).collect(Collectors.toList());
		return verifyAllEqualUsingStream(pointsOfServiceNames);
	}

	private boolean verifyAllEqualUsingStream(final List<String> values)
	{
		return values.stream().distinct().count() <= 1;
	}

	protected boolean setPaymentSubscription(final NoCardPaymentInfoData newPaymentSubscription)
	{
		if (newPaymentSubscription != null && StringUtils.isNotBlank(newPaymentSubscription.getId()))
		{
			final Optional<List<NoCardPaymentInfoData>> noCardPaymentInfos = getCustomUserFacade().getNoCardPaymentInfos(true);
			if (noCardPaymentInfos.isPresent() && noCardPaymentInfos.get().size() <= 1)
			{
				getCustomUserFacade().setDefaultPaymentInfo(newPaymentSubscription);
			}
			getCheckoutFacade().setGeneralPaymentDetails(newPaymentSubscription.getId());
		}
		return true;
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/storecreditmodes", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "setStoreCreditMode", value = "Defines and assigns details of a new credit card payment to the cart.", notes = "Defines the details of a new credit card, and assigns this payment option to the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void setStoreCreditMode(final HttpServletRequest request, //NOSONAR
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields, @RequestParam(required = true)
			final String storeCreditModeTypeCode, @RequestParam(required = false)
			final Double amount) throws InvalidPaymentInfoException, NoCheckoutCartException, UnsupportedRequestException
	{
		final StoreCreditDetailsWsDTO storeCreditDetailsWsDTO = new StoreCreditDetailsWsDTO();
		storeCreditDetailsWsDTO.setAmount(amount);
		storeCreditDetailsWsDTO.setStoreCreditModeTypeCode(storeCreditModeTypeCode);
		validate(storeCreditDetailsWsDTO, "storeCreditModeTypeCode", getStoreCreditValidator());


		getCheckoutFacade().setStoreCreditMode(storeCreditModeTypeCode, amount);

	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/storecreditmodes", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getStoreCreditModes", value = "Get all payment modes for the current store.", notes = "Returns all payment modes supported for the "
			+ "current base store and cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public StoreCreditModeListWsDTO getStoreCreditModes(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("getStoreCreditModes");
		}
		final Optional<List<StoreCreditModeData>> supportedStoreCreditModes = getCheckoutFacade().getSupportedStoreCreditModes();
		final StoreCreditModesData storeCreditModesData = new StoreCreditModesData();
		if (supportedStoreCreditModes.isPresent())
		{
			storeCreditModesData.setStoreCreditModes(supportedStoreCreditModes.get());
		}
		return getDataMapper().map(storeCreditModesData, StoreCreditModeListWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/min-amount-label", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getMinAmountLabel", value = "Get Min Amount Label.", hidden = true)
	@ApiBaseSiteIdUserIdAndCartIdParam
	public String getMinAmountLabel()
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("getMinAmountLabel");
		}
		final Optional<String> cartMinAmountLabel = cartValidationService.getCartMinAmountLabelByCurrentCart();
		return cartMinAmountLabel.isPresent() ? cartMinAmountLabel.get() : Strings.EMPTY;
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/max-amount-label", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getMaxAmountLabel", value = "Get Max Amount Label.", hidden = true)
	@ApiBaseSiteIdUserIdAndCartIdParam
	public String getMaxAmountLabel()
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("getMaxAmountLabel");
		}
		final Optional<String> cartMaxAmountLabel = cartValidationService.getCartMaxAmountLabelByCurrentCart();
		return cartMaxAmountLabel.isPresent() ? cartMaxAmountLabel.get() : Strings.EMPTY;
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/loyalty/loyaltypaymentmodes", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getLoyaltyPaymentModes", value = "Get all loyalty payment modes for the current store.", notes = "Returns all payment modes supported for the "
			+ "current base store and cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public LoyaltyPaymentModeListWsDTO getLoyaltyPaymentModes(
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("getLoyaltyPaymentModes");
		}
		final List<LoyaltyPaymentModeData> loyaltyPaymentModes = getCheckoutFacade().getLoyaltyPaymentModes();
		final LoyaltyPaymentModesData loyaltyPaymentModesData = new LoyaltyPaymentModesData();
		if (!CollectionUtils.isEmpty(loyaltyPaymentModes))
		{
			loyaltyPaymentModesData.setLoyaltyPaymentModes(loyaltyPaymentModes);
		}
		return getDataMapper().map(loyaltyPaymentModesData, LoyaltyPaymentModeListWsDTO.class, fields);
	}


	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_GUEST", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/{cartId}/loyalty/loyaltypaymentmodes", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "setLoyaltyPaymentMode", value = "Defines and assigns details of a new loyalty payment to the cart.", notes = "Defines the details of a new loyalty, and assigns this payment option to the cart.")
	@ApiBaseSiteIdUserIdAndCartIdParam
	public void setLoyaltyPaymentMode(final HttpServletRequest request, //NOSONAR
			@ApiParam(value = "Response configuration. This is the list of fields that should be returned in the response body.", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields, @RequestParam(required = true)
			final String loyaltyPaymentModeTypeCode, @RequestParam(required = false)
			final Double amount) throws InvalidPaymentInfoException, NoCheckoutCartException, UnsupportedRequestException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("setLoyaltyPaymentMode");
		}
		final LoyaltyPaymentDetailsWsDTO loyaltyDetailsWsDTO = new LoyaltyPaymentDetailsWsDTO();
		loyaltyDetailsWsDTO.setAmount(amount);
		loyaltyDetailsWsDTO.setLoyaltyPaymentModeTypeCode(loyaltyPaymentModeTypeCode);
		validate(loyaltyDetailsWsDTO, "loyaltyPaymentModeTypeCode", getLoyaltyPointsValidator());
		getCheckoutFacade().setLoyaltyPaymentMode(loyaltyPaymentModeTypeCode, amount);

	}



}
