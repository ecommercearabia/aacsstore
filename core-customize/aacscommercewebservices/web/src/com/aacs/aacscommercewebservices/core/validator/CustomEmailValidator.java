package com.aacs.aacscommercewebservices.core.validator;

import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


public class CustomEmailValidator implements Validator
{
	private static final Logger LOG = Logger.getLogger(CustomEmailValidator.class);
	private static final String INVALID_EMAIL_MESSAGE_ID = "field.register.email.invalid";
	private static final String INVALID_EMAIL_USER_IS_EXISTS_MESSAGE_ID = "field.register.email.userExists";
	private String fieldPath;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return true;
	}

	@Override
	public void validate(final Object o, final Errors errors)
	{
		Assert.notNull(errors, "Errors object must not be null");
		final String fieldValue = (String) errors.getFieldValue(this.fieldPath);
		if (!org.apache.commons.validator.routines.EmailValidator.getInstance().isValid(fieldValue))
		{
			errors.rejectValue(this.fieldPath, INVALID_EMAIL_MESSAGE_ID, new String[]
			{ this.fieldPath }, "This field is not a valid email addresss.");
		}
		else
		{
			try
			{
				if (userFacade.getUserUID(fieldValue) != null)
				{
					errors.rejectValue(this.fieldPath, INVALID_EMAIL_USER_IS_EXISTS_MESSAGE_ID, new String[]
					{ this.fieldPath }, "An account already exists for this email address.");
				}
			}
			catch (final UnknownIdentifierException e)
			{
				LOG.error(e.getMessage());
			}


		}
	}

	public String getFieldPath()
	{
		return this.fieldPath;
	}

	public void setFieldPath(final String fieldPath)
	{
		this.fieldPath = fieldPath;
	}
}
