package com.aacs.aacscommercewebservices.core.validator;

import de.hybris.platform.commercewebservicescommons.dto.order.LoyaltyPaymentDetailsWsDTO;

import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.aacs.aacsloyaltyprogramprovider.enums.LoyaltyPaymentModeType;



public class LoyaltyWsDTOValidator implements Validator
{

	private static final String LOYALTYPOINTS_LOYALTY_PAYMENT_CODE_INVALID = "loyaltypoints.loyaltyPaymentCode.invalid";
	private static final String LOYALTY_PAYMENT_CODE = "loyaltyPaymentCode";
	private static final String LOYALTYPOINTS_LOYALTY_PAYMENT_AMOUNT_INVALID = "loyaltypoints.loyaltyPaymentAmount.invalid";
	private static final String LOYALTY_PAYMENT_AMOUNT = "loyaltyPaymentAmount";

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(final Class<?> arg0)
	{
		// YTODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
	 */
	@Override
	public void validate(final Object loyaltyPaymentDetailsWsDTO, final Errors errors)
	{
		final String loyaltyPaymentCode = ((LoyaltyPaymentDetailsWsDTO) loyaltyPaymentDetailsWsDTO).getLoyaltyPaymentModeTypeCode();
		final Double loyaltyPaymentAmount = ((LoyaltyPaymentDetailsWsDTO) loyaltyPaymentDetailsWsDTO).getAmount();
		if (StringUtils.isEmpty(loyaltyPaymentCode))
		{
			errors.rejectValue(LOYALTY_PAYMENT_CODE, LOYALTYPOINTS_LOYALTY_PAYMENT_CODE_INVALID);
		}

		try
		{
			LoyaltyPaymentModeType.valueOf(loyaltyPaymentCode);
		}
		catch (final Exception e)
		{

			errors.rejectValue(LOYALTY_PAYMENT_CODE, LOYALTYPOINTS_LOYALTY_PAYMENT_CODE_INVALID);
		}

		if (LoyaltyPaymentModeType.REDEEM_SPECIFIC_AMOUNT.getCode().equals(loyaltyPaymentCode))
		{
			if (Objects.isNull(loyaltyPaymentAmount))
			{
				errors.rejectValue(LOYALTY_PAYMENT_AMOUNT, LOYALTYPOINTS_LOYALTY_PAYMENT_AMOUNT_INVALID);
			}

			double scAmountValue = 0.0;
			try
			{
				scAmountValue = loyaltyPaymentAmount == null ? 0.0 : loyaltyPaymentAmount.doubleValue();
			}
			catch (final Exception ex)
			{
				errors.rejectValue(LOYALTY_PAYMENT_AMOUNT, LOYALTYPOINTS_LOYALTY_PAYMENT_AMOUNT_INVALID);
			}
			if (scAmountValue < 0)
			{
				errors.rejectValue(LOYALTY_PAYMENT_AMOUNT, LOYALTYPOINTS_LOYALTY_PAYMENT_AMOUNT_INVALID);
			}
		}

	}
}
