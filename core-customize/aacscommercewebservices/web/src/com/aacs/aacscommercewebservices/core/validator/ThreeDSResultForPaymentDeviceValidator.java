/**
 *
 */
package com.aacs.aacscommercewebservices.core.validator;

import de.hybris.platform.commercewebservicescommons.dto.payment.data.Payment3DSResultWsDTO;

import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * @author monzer
 *
 */
public class ThreeDSResultForPaymentDeviceValidator implements Validator
{

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return clazz.isAssignableFrom(Payment3DSResultWsDTO.class);
	}

	@Override
	public void validate(final Object result, final Errors errors)
	{
		if (result == null)
		{
			errors.reject("3DSResult cannot be empty", "payment.3ds.result.empty");
			return;
		}
		if (!(result instanceof Payment3DSResultWsDTO))
		{
			errors.reject("3DSResult is suppported", "payment.3ds.result.notsupported");
			return;
		}
		final Payment3DSResultWsDTO res = (Payment3DSResultWsDTO) result;


		if (StringUtils.isBlank(res.getSessionId()))
		{
			errors.reject("Result in 3DSResult cannot be empty", "payment.3ds.result.sessioinId.empty");
		}

	}


}
