/**
 *
 */
package com.aacs.backoffice.oms.widgets.returns.createreturnrequest;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.omsbackoffice.widgets.returns.createreturnrequest.CreateReturnRequestController;
import de.hybris.platform.omsbackoffice.widgets.returns.dtos.ReturnEntryToCreateDto;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Row;


/**
 * @author husam.dababaneh@erbia.com
 *
 */
public class CustomCreateReturnRequestController extends CreateReturnRequestController
{
	private static final Logger LOG = LoggerFactory.getLogger(CustomCreateReturnRequestController.class);

	@Override
	protected void calculateRowAmount(final Row myRow, final ReturnEntryToCreateDto myReturnEntry, final int qtyEntered)
	{
		final BigDecimal newAmount = getNewAmount(myReturnEntry, qtyEntered);
		myReturnEntry.setQuantityToReturn(qtyEntered);
		myReturnEntry.getRefundEntry().setAmount(newAmount);
		final double doubleValue = newAmount.setScale(2, RoundingMode.HALF_EVEN).doubleValue();
		this.applyToRow(doubleValue, COLUMN_INDEX_RETURN_AMOUNT, myRow);
		this.calculateIndividualTaxEstimate(myReturnEntry);
		this.calculateTotalRefundAmount();
	}

	private BigDecimal getNewAmount(final ReturnEntryToCreateDto myReturnEntry, final int qtyEntered)
	{
		return BigDecimal.valueOf(getBasePriceForEntry(myReturnEntry) * qtyEntered);
	}

	private Double getBasePriceForEntry(final ReturnEntryToCreateDto myReturnEntry)
	{
		final AbstractOrderEntryModel orderEntry = myReturnEntry.getRefundEntry().getOrderEntry();
		double basePrice = orderEntry.getBasePrice();
		if (myReturnEntry.isDiscountApplied())
		{
			basePrice = orderEntry.getTotalPrice() / orderEntry.getQuantity();
		}

		return basePrice;
	}

	@Override
	protected void calculateTotalRefundAmount()
	{
		this.calculateEstimatedTax();

		final Checkbox refundDeliveryCost = getPrivateField("refundDeliveryCost");
		final Set<ReturnEntryToCreateDto> returnEntriesToCreate = getPrivateField("returnEntriesToCreate");
		final Doublebox estimatedTax = getPrivateField("estimatedTax");

		Double calculatedRefundAmount = refundDeliveryCost.isChecked() ? this.getOrder().getDeliveryCost() : 0.0D;
		calculatedRefundAmount = calculatedRefundAmount + returnEntriesToCreate.stream().map((entry) -> {
			return entry.getRefundEntry().getAmount();
		}).reduce(BigDecimal.ZERO, BigDecimal::add).doubleValue();


		final OrderModel order = getOrder();
		final Boolean net = order.getNet();
		double doubleValue = calculatedRefundAmount;
		if (net.booleanValue())
		{
			doubleValue += estimatedTax.doubleValue();
		}

		//		setPrivateField<Doublebox>("totalRefundAmount", doubleValue);
		this.<Doublebox> setPrivateField("totalRefundAmount", doubleValue);
		//		this.totalRefundAmount.setValue(doubleValue);
	}

	//	@Override
	//	protected void calculateEstimatedTax()
	//	{
	//		final Set<ReturnEntryToCreateDto> returnEntriesToCreate = getPrivateField("returnEntriesToCreate");
	//
	//		final Checkbox refundDeliveryCost = getPrivateField("applied");
	//
	//		BigDecimal totalTax = returnEntriesToCreate.stream().filter((returnEntryToCreate) -> {
	//			return returnEntryToCreate.getQuantityToReturn() > 0 && returnEntryToCreate.getTax() != null;
	//		}).map(ReturnEntryToCreateDto::getTax).reduce(BigDecimal.ZERO, BigDecimal::add);
	//
	//		if (refundDeliveryCost.isChecked())
	//		{
	//			final BigDecimal deliveryCostTax = BigDecimal
	//					.valueOf(this.getOrder().getTotalTax() - this.getOrder().getEntries().stream().filter((entry) -> {
	//						return !entry.getTaxValues().isEmpty();
	//					}).mapToDouble((entry) -> {
	//						return entry.getTaxValues().stream().findFirst().get().getValue();
	//					}).sum());
	//			totalTax = totalTax.add(deliveryCostTax);
	//		}
	//		setPrivateField("estimatedTax", totalTax.doubleValue());
	//		//      this.estimatedTax.setValue(totalTax.doubleValue());
	//		//		super.calculateEstimatedTax();
	//	}
	//
	private <T> T getPrivateField(final String fieldName)
	{
		try
		{
			final Field f = getClass().getSuperclass().getDeclaredField(fieldName);
			f.setAccessible(true);
			return (T) f.get(this);
		}
		catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e)
		{
			LOG.error(e.getMessage(), e);
		}
		return null;
	}

	private <T> void setPrivateField(final String fieldName, final Object value)
	{
		final T fieldObj = getPrivateField(fieldName);
		if (fieldObj instanceof Doublebox)
		{
			final Doublebox field = (Doublebox) fieldObj;
			field.setValue((Double) value);
		}
	}
}
