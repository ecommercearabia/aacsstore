/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.test.constants;

/**
 * 
 */
public class AacsTestConstants extends GeneratedAacsTestConstants
{

	public static final String EXTENSIONNAME = "aacstest";

}
