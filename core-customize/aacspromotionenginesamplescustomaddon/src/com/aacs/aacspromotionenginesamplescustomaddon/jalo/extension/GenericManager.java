package com.aacs.aacspromotionenginesamplescustomaddon.jalo.extension;

import com.aacs.aacspromotionenginesamplescustomaddon.constants.AacspromotionenginesamplescustomaddonConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class GenericManager extends GeneratedGenericManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( GenericManager.class.getName() );
	
	public static final GenericManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (GenericManager) em.getExtension(AacspromotionenginesamplescustomaddonConstants.EXTENSIONNAME);
	}
	
}
