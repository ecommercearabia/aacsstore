/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsmessagecentercscustomocc.constants;

/**
 * Global class for all aacsmessagecentercscustomocc constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings(
{ "deprecation", "squid:CallToDeprecatedMethod" })
public final class AacsmessagecentercscustomoccConstants extends GeneratedAacsmessagecentercscustomoccConstants
{
	public static final String EXTENSIONNAME = "aacsmessagecentercscustomocc";

	private AacsmessagecentercscustomoccConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
