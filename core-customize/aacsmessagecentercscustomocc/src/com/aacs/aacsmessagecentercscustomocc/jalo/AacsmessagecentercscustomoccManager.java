/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsmessagecentercscustomocc.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.aacs.aacsmessagecentercscustomocc.constants.AacsmessagecentercscustomoccConstants;
import org.apache.log4j.Logger;

public class AacsmessagecentercscustomoccManager extends GeneratedAacsmessagecentercscustomoccManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( AacsmessagecentercscustomoccManager.class.getName() );
	
	public static final AacsmessagecentercscustomoccManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AacsmessagecentercscustomoccManager) em.getExtension(AacsmessagecentercscustomoccConstants.EXTENSIONNAME);
	}
	
}
