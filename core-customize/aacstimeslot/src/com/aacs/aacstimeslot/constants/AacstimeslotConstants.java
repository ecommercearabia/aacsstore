/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacstimeslot.constants;

/**
 * Global class for all Aacstimeslot constants. You can add global constants for your extension into this class.
 */
public final class AacstimeslotConstants extends GeneratedAacstimeslotConstants
{
	public static final String EXTENSIONNAME = "aacstimeslot";

	private AacstimeslotConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
