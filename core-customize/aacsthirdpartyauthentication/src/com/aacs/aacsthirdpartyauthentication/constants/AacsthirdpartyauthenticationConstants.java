package com.aacs.aacsthirdpartyauthentication.constants;

@SuppressWarnings({"deprecation","squid:CallToDeprecatedMethod"})
public class AacsthirdpartyauthenticationConstants extends GeneratedAacsthirdpartyauthenticationConstants
{
	public static final String EXTENSIONNAME = "aacsthirdpartyauthentication";
	
	private AacsthirdpartyauthenticationConstants()
	{
		//empty
	}
	
	
}
