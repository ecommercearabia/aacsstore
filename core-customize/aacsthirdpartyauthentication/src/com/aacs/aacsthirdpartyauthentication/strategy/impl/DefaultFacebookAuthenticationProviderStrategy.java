/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsthirdpartyauthentication.strategy.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.aacs.aacsthirdpartyauthentication.model.FacebookAuthenticationProviderModel;
import com.aacs.aacsthirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;
import com.aacs.aacsthirdpartyauthentication.service.ThirdPartyAuthenticationProviderService;
import com.aacs.aacsthirdpartyauthentication.strategy.AuthenticationProviderStrategy;


/**
 *
 */
public class DefaultFacebookAuthenticationProviderStrategy implements AuthenticationProviderStrategy
{

	@Resource(name = "thirdPartyAuthenticationProviderService")
	private ThirdPartyAuthenticationProviderService thirdPartyAuthenticationProviderService;

	@Override
	public Optional<ThirdPartyAuthenticationProviderModel> getActiveProvider(final String cmsSiteUid)
	{
		return getThirdPartyAuthenticationProviderService().get(cmsSiteUid, FacebookAuthenticationProviderModel.class);
	}

	@Override
	public Optional<ThirdPartyAuthenticationProviderModel> getActiveProvider(final CMSSiteModel cmsSiteModel)
	{
		return getThirdPartyAuthenticationProviderService().getActive(cmsSiteModel, FacebookAuthenticationProviderModel.class);
	}

	@Override
	public Optional<ThirdPartyAuthenticationProviderModel> getActiveProviderByCurrentSite()
	{
		return getThirdPartyAuthenticationProviderService().getActiveByCurrentSite(FacebookAuthenticationProviderModel.class);
	}

	public ThirdPartyAuthenticationProviderService getThirdPartyAuthenticationProviderService()
	{
		return thirdPartyAuthenticationProviderService;
	}

}

