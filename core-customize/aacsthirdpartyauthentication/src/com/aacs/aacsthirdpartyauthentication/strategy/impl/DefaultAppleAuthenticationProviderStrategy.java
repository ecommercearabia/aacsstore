/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsthirdpartyauthentication.strategy.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.aacs.aacsthirdpartyauthentication.model.AppleAuthenticationProviderModel;
import com.aacs.aacsthirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;
import com.aacs.aacsthirdpartyauthentication.service.ThirdPartyAuthenticationProviderService;
import com.aacs.aacsthirdpartyauthentication.strategy.AuthenticationProviderStrategy;


/**
 * @author monzer
 */
public class DefaultAppleAuthenticationProviderStrategy implements AuthenticationProviderStrategy
{
	@Resource(name = "thirdPartyAuthenticationProviderService")
	private ThirdPartyAuthenticationProviderService thirdPartyAuthenticationProviderService;

	@Override
	public Optional<ThirdPartyAuthenticationProviderModel> getActiveProvider(final String cmsSiteUid)
	{
		return thirdPartyAuthenticationProviderService.get(cmsSiteUid, AppleAuthenticationProviderModel.class);
	}

	@Override
	public Optional<ThirdPartyAuthenticationProviderModel> getActiveProvider(final CMSSiteModel cmsSiteModel)
	{
		return thirdPartyAuthenticationProviderService.getActive(cmsSiteModel, AppleAuthenticationProviderModel.class);
	}

	@Override
	public Optional<ThirdPartyAuthenticationProviderModel> getActiveProviderByCurrentSite()
	{
		return thirdPartyAuthenticationProviderService.getActiveByCurrentSite(AppleAuthenticationProviderModel.class);
	}

}
