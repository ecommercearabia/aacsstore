/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsthirdpartyauthentication.strategy;

import java.util.Optional;

import com.aacs.aacsthirdpartyauthentication.data.ThirdPartyAuthenticationProviderData;
import com.aacs.aacsthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.aacs.aacsthirdpartyauthentication.entry.TwitterFormData;
import com.aacs.aacsthirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.aacs.aacsthirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;


/**
 *
 */
public interface ThirdPartyAuthenticationStrategy
{
	Optional<ThirdPartyAuthenticationProviderData> getThirdPartyAuthenticationProviderData(
			ThirdPartyAuthenticationProviderModel provider) throws ThirdPartyAuthenticationException;

	Optional<ThirdPartyAuthenticationUserData> getThirdPartyUserData(Object data, ThirdPartyAuthenticationProviderModel provider)
			throws ThirdPartyAuthenticationException;

	Optional<ThirdPartyAuthenticationUserData> getThirdPartyUserRegistrationData(Object data,
			ThirdPartyAuthenticationProviderModel provider) throws ThirdPartyAuthenticationException;

	Optional<TwitterFormData> getFormData(Object data, ThirdPartyAuthenticationProviderModel provider, String callbackUrl)
			throws ThirdPartyAuthenticationException;

	boolean verifyAccessTokenWithThirdParty(Object data, Object token, ThirdPartyAuthenticationProviderModel provider)
			throws ThirdPartyAuthenticationException;
}
