/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsthirdpartyauthentication.dao.impl;

import com.aacs.aacsthirdpartyauthentication.dao.ThirdPartyAuthenticationProviderDao;
import com.aacs.aacsthirdpartyauthentication.model.TwitterAuthenticationProviderModel;


/**
 *
 */
public class DefaultTwitterAuthenticationProviderDao extends DefaultThirdPartyAuthenticationProviderDao
		implements ThirdPartyAuthenticationProviderDao
{


	/**
	 *
	 */
	public DefaultTwitterAuthenticationProviderDao()
	{
		super(TwitterAuthenticationProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return TwitterAuthenticationProviderModel._TYPECODE;
	}


}
