/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsthirdpartyauthentication.dao.impl;

import com.aacs.aacsthirdpartyauthentication.dao.ThirdPartyAuthenticationProviderDao;
import com.aacs.aacsthirdpartyauthentication.model.FacebookAuthenticationProviderModel;

/**
 *
 */
public class DefaultFacebookAuthenticationProviderDao extends DefaultThirdPartyAuthenticationProviderDao
		implements ThirdPartyAuthenticationProviderDao
{

	/**
	 *
	 */
	public DefaultFacebookAuthenticationProviderDao()
	{
		super(FacebookAuthenticationProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return FacebookAuthenticationProviderModel._TYPECODE;
	}

}
