/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsthirdpartyauthentication.dao.impl;

import com.aacs.aacsthirdpartyauthentication.dao.ThirdPartyAuthenticationProviderDao;
import com.aacs.aacsthirdpartyauthentication.model.AppleAuthenticationProviderModel;


/**
 * @author monzer
 */
public class DefaultAppleAuthenticationProviderDao extends DefaultThirdPartyAuthenticationProviderDao
		implements ThirdPartyAuthenticationProviderDao
{

	/**
	*
	*/
	public DefaultAppleAuthenticationProviderDao()
	{
		super(AppleAuthenticationProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return AppleAuthenticationProviderModel._TYPECODE;
	}

}
