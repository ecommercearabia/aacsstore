/**
 *
 */
package com.aacs.aacsthirdpartyauthentication.facades.facade;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commercefacades.user.data.CustomerData;

import java.util.Optional;

import com.aacs.aacsthirdpartyauthentication.data.ThirdPartyUserData;
import com.aacs.aacsthirdpartyauthentication.enums.ThirdPartyAuthenticationType;
import com.aacs.aacsthirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.aacs.thirdpartyauthentication.provider.ThirdPartyAuthenticationProviderListData;
import com.aacs.thirdpartyauthentication.provider.apple.AppleAuthenticationProviderData;
import com.aacs.thirdpartyauthentication.provider.facebook.FacebookAuthenticationProviderData;
import com.aacs.thirdpartyauthentication.provider.google.GoogleAuthenticationProviderData;


/**
 * @author monzer
 *
 */
public interface ThirdPartyAuthenticationProviderFacade
{

	Optional<FacebookAuthenticationProviderData> getFacebookAuthenticationProviderByCurrentSite();

	Optional<FacebookAuthenticationProviderData> getFacebookAuthenticationProviderBySiteUid(String siteUid);

	Optional<FacebookAuthenticationProviderData> getFacebookAuthenticationProviderBySite(CMSSiteModel site);

	Optional<GoogleAuthenticationProviderData> getGoogleAuthenticationProviderByCurrentSite();

	Optional<GoogleAuthenticationProviderData> getGoogleAuthenticationProviderBySiteUid(String siteUid);

	Optional<GoogleAuthenticationProviderData> getGoogleAuthenticationProviderBySite(CMSSiteModel site);

	Optional<AppleAuthenticationProviderData> getAppleAuthenticationProviderByCurrentSite();

	Optional<AppleAuthenticationProviderData> getAppleAuthenticationProviderBySiteUid(String siteUid);

	Optional<AppleAuthenticationProviderData> getAppleAuthenticationProviderBySite(CMSSiteModel site);

	Optional<ThirdPartyAuthenticationProviderListData> getAllThirdPartyProvidersByCurrentSite();

	Optional<ThirdPartyAuthenticationProviderListData> getAllThirdPartyProvidersBySiteUid(String siteUid);

	Optional<ThirdPartyAuthenticationProviderListData> getAllThirdPartyProvidersBySite(CMSSiteModel site);

	Optional<ThirdPartyUserData> getThirdPartyUserData(Object authData, ThirdPartyAuthenticationType type, CMSSiteModel cmsSite)
			throws ThirdPartyAuthenticationException;

	Optional<ThirdPartyUserData> getThirdPartyUserDataByCurrentSite(Object authData, ThirdPartyAuthenticationType type)
			throws ThirdPartyAuthenticationException;

	boolean verifyThirdPartyToken(Object token, ThirdPartyAuthenticationType type, CMSSiteModel site)
			throws ThirdPartyAuthenticationException;

	boolean verifyThirdPartyTokenByCurrentSite(Object token, ThirdPartyAuthenticationType type)
			throws ThirdPartyAuthenticationException;

	void saveUser(CustomerData customer);

	Optional<ThirdPartyUserData> getThirdPartyUserRegistrationData(Object authData, ThirdPartyAuthenticationType type,
			CMSSiteModel cmsSite) throws ThirdPartyAuthenticationException;

	Optional<ThirdPartyUserData> getThirdPartyUserRegistrationDataByCurrentSite(Object authData, ThirdPartyAuthenticationType type)
			throws ThirdPartyAuthenticationException;

}
