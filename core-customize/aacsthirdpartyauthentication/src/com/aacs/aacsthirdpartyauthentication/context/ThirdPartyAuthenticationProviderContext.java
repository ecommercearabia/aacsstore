/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsthirdpartyauthentication.context;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Optional;

import com.aacs.aacsthirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;


/**
 *
 */
public interface ThirdPartyAuthenticationProviderContext
{
	public Optional<ThirdPartyAuthenticationProviderModel> getThirdPartyAuthenticationProviderByCurrentSite(
			Class<?> providerClass);

	public Optional<ThirdPartyAuthenticationProviderModel> getThirdPartyAuthenticationProvider(CMSSiteModel cmsSite,
			Class<?> providerClass);

}
