/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsthirdpartyauthentication.context.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.aacs.aacsthirdpartyauthentication.context.ThirdPartyAuthenticationContext;
import com.aacs.aacsthirdpartyauthentication.context.ThirdPartyAuthenticationProviderContext;
import com.aacs.aacsthirdpartyauthentication.data.ThirdPartyAuthenticationProviderData;
import com.aacs.aacsthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.aacs.aacsthirdpartyauthentication.data.ThirdPartyUserData;
import com.aacs.aacsthirdpartyauthentication.entry.TwitterFormData;
import com.aacs.aacsthirdpartyauthentication.enums.ThirdPartyAuthenticationType;
import com.aacs.aacsthirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.aacs.aacsthirdpartyauthentication.exception.enums.ThirdPartyAuthenticationExceptionType;
import com.aacs.aacsthirdpartyauthentication.model.AppleAuthenticationProviderModel;
import com.aacs.aacsthirdpartyauthentication.model.FacebookAuthenticationProviderModel;
import com.aacs.aacsthirdpartyauthentication.model.GoogleAuthenticationProviderModel;
import com.aacs.aacsthirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;
import com.aacs.aacsthirdpartyauthentication.model.TwitterAuthenticationProviderModel;
import com.aacs.aacsthirdpartyauthentication.service.ThirdPartyUserService;
import com.aacs.aacsthirdpartyauthentication.strategy.ThirdPartyAuthenticationStrategy;
import com.aacs.thirdpartyauthentication.profile.ThirdPartyProfileData;
import com.google.common.base.Preconditions;


/**
 * The Class DefaultThirdPartyAuthenticationContext.
 *
 * @author monzer
 */
public class DefaultThirdPartyAuthenticationContext implements ThirdPartyAuthenticationContext
{

	/** The Constant THIRDPARTY_AUTHENTICATION_STRATEGY_NOT_FOUND. */
	private static final String THIRDPARTY_AUTHENTICATION_STRATEGY_NOT_FOUND = "strategy not found";

	/** The Constant CMSSITE_NOT_FOUND. */
	private static final String CMSSITE_NOT_FOUND = "cmsSite is null";

	/** The Constant CALLBACK_URL_NOT_FOUND. */
	private static final String CALLBACK_URL_NOT_FOUND = "callbackUrl is null";

	/** The Constant DATA_IS_NULL. */
	private static final String DATA_IS_NULL = "data is null";

	/** The Constant THIRDPARTY_AUTHENTICATION_TYPE_IS_NULL. */
	private static final String THIRDPARTY_AUTHENTICATION_TYPE_IS_NULL = "thirdPartyAuthenticationType is null";


	/** The third party authentication strategy map. */
	@Resource(name = "thirdPartyAuthenticationStrategyMap")
	private Map<Class<?>, ThirdPartyAuthenticationStrategy> thirdPartyAuthenticationStrategyMap;

	/** The third party authentication provider context. */
	@Resource(name = "thirdPartyAuthenticationProviderContext")
	private ThirdPartyAuthenticationProviderContext thirdPartyAuthenticationProviderContext;

	/** The cms site service. */
	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "thirdPartyUserService")
	private ThirdPartyUserService thirdPartyUserService;


	/**
	 * Gets the supported third party authentication provider data.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return the supported third party authentication provider data
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	@Override
	public List<ThirdPartyAuthenticationProviderData> getSupportedThirdPartyAuthenticationProviderData(
			final CMSSiteModel cmsSiteModel) throws ThirdPartyAuthenticationException
	{

		Preconditions.checkArgument(cmsSiteModel != null, CMSSITE_NOT_FOUND);
		final List<ThirdPartyAuthenticationType> supportedProvider = cmsSiteModel.getSupportedProviders();
		if (CollectionUtils.isEmpty(supportedProvider))
		{
			return Collections.emptyList();
		}
		final List<ThirdPartyAuthenticationProviderData> list = new ArrayList<>();
		for (final ThirdPartyAuthenticationType type : supportedProvider)
		{
			Optional<ThirdPartyAuthenticationProviderModel> thirdPartyAuthenticationProvider = Optional.empty();
			switch (type)
			{
				case FACEBOOK:
					thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
							.getThirdPartyAuthenticationProvider(cmsSiteModel, FacebookAuthenticationProviderModel.class);
					break;
				case GOOGLE:
					thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
							.getThirdPartyAuthenticationProvider(cmsSiteModel, GoogleAuthenticationProviderModel.class);
					break;
				case TWITTER:
					thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
							.getThirdPartyAuthenticationProvider(cmsSiteModel, TwitterAuthenticationProviderModel.class);
					break;
				case APPLE:
					thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
							.getThirdPartyAuthenticationProvider(cmsSiteModel, AppleAuthenticationProviderModel.class);
					break;
				default:
					continue;

			}
			if (thirdPartyAuthenticationProvider.isPresent())
			{
				fillSupportedThirdPartyAuthenticationProviderData(thirdPartyAuthenticationProvider.get(), list);
			}
		}
		return list;
	}

	/**
	 * @throws ThirdPartyAuthenticationException
	 *
	 */
	private void fillSupportedThirdPartyAuthenticationProviderData(
			final ThirdPartyAuthenticationProviderModel thirdPartyAuthenticationProvider,
			final List<ThirdPartyAuthenticationProviderData> list) throws ThirdPartyAuthenticationException
	{
		final Optional<ThirdPartyAuthenticationStrategy> thirdPartyAuthenticationStratergy = getStrategy(
				thirdPartyAuthenticationProvider.getClass());
		if (thirdPartyAuthenticationStratergy.isPresent())
		{
			final Optional<ThirdPartyAuthenticationProviderData> data = thirdPartyAuthenticationStratergy.get()
					.getThirdPartyAuthenticationProviderData(thirdPartyAuthenticationProvider);
			if (data.isPresent())
			{
				list.add(data.get());
			}
		}
	}

	/**
	 * Gets the third party user data.
	 *
	 * @param data
	 *           the data
	 * @param type
	 *           the type
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return the third party user data
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	@Override
	public Optional<ThirdPartyAuthenticationUserData> getThirdPartyUserData(final Object data,
			final ThirdPartyAuthenticationType type, final CMSSiteModel cmsSiteModel) throws ThirdPartyAuthenticationException
	{
		Preconditions.checkArgument(data != null, DATA_IS_NULL);
		Preconditions.checkArgument(type != null, THIRDPARTY_AUTHENTICATION_TYPE_IS_NULL);
		Preconditions.checkArgument(cmsSiteModel != null, CMSSITE_NOT_FOUND);
		Optional<ThirdPartyAuthenticationProviderModel> thirdPartyAuthenticationProvider = Optional.empty();
		switch (type)
		{
			case FACEBOOK:
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSiteModel, FacebookAuthenticationProviderModel.class);
				break;

			case GOOGLE:
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSiteModel, GoogleAuthenticationProviderModel.class);
				break;

			case TWITTER:
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSiteModel, TwitterAuthenticationProviderModel.class);
				break;

			case APPLE:
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSiteModel, AppleAuthenticationProviderModel.class);
				break;

			default:
				throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER,
						ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER.getMsg());
		}
		if (thirdPartyAuthenticationProvider.isPresent())
		{
			final Optional<ThirdPartyAuthenticationStrategy> thirdPartyAuthenticationStratergy = getStrategy(
					thirdPartyAuthenticationProvider.get().getClass());
			if (thirdPartyAuthenticationStratergy.isPresent())
			{
				return thirdPartyAuthenticationStratergy.get().getThirdPartyUserData(data, thirdPartyAuthenticationProvider.get());
			}
		}
		return Optional.empty();
	}

	/**
	 * Gets the strategy.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the strategy
	 */
	protected Optional<ThirdPartyAuthenticationStrategy> getStrategy(final Class<?> providerClass)
	{
		final ThirdPartyAuthenticationStrategy strategy = getThirdPartyAuthenticationStrategyMap().get(providerClass);
		Preconditions.checkArgument(strategy != null, THIRDPARTY_AUTHENTICATION_STRATEGY_NOT_FOUND);
		return Optional.ofNullable(strategy);
	}

	/**
	 * Gets the third party authentication strategy map.
	 *
	 * @return the thirdPartyAuthenticationStrategyMap
	 */
	public Map<Class<?>, ThirdPartyAuthenticationStrategy> getThirdPartyAuthenticationStrategyMap()
	{
		return thirdPartyAuthenticationStrategyMap;
	}

	/**
	 * Gets the third party authentication provider context.
	 *
	 * @return the thirdPartyAuthenticationProviderContext
	 */
	public ThirdPartyAuthenticationProviderContext getThirdPartyAuthenticationProviderContext()
	{
		return thirdPartyAuthenticationProviderContext;
	}

	/**
	 * Gets the supported third party authentication provider data by current store.
	 *
	 * @return the supported third party authentication provider data by current store
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	@Override
	public List<ThirdPartyAuthenticationProviderData> getSupportedThirdPartyAuthenticationProviderDataByCurrentStore()
			throws ThirdPartyAuthenticationException
	{
		final CMSSiteModel cmsSiteModel = cmsSiteService.getCurrentSite();
		return getSupportedThirdPartyAuthenticationProviderData(cmsSiteModel);
	}

	/**
	 * Gets the third party user data by current store.
	 *
	 * @param data
	 *           the data
	 * @param type
	 *           the type
	 * @return the third party user data by current store
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	@Override
	public Optional<ThirdPartyAuthenticationUserData> getThirdPartyUserDataByCurrentStore(final Object data,
			final ThirdPartyAuthenticationType type) throws ThirdPartyAuthenticationException
	{
		final CMSSiteModel cmsSiteModel = cmsSiteService.getCurrentSite();
		return getThirdPartyUserData(data, type, cmsSiteModel);
	}

	/**
	 * Gets the third party form data.
	 *
	 * @param data
	 *           the data
	 * @param type
	 *           the type
	 * @param cmsSiteModel
	 *           the cms site model
	 * @param callbackUrl
	 *           the callback url
	 * @return the third party form data
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	@Override
	public Optional<TwitterFormData> getThirdPartyFormData(final Object data, final ThirdPartyAuthenticationType type,
			final CMSSiteModel cmsSiteModel, final String callbackUrl) throws ThirdPartyAuthenticationException
	{
		Preconditions.checkArgument(data != null, DATA_IS_NULL);
		Preconditions.checkArgument(type != null, THIRDPARTY_AUTHENTICATION_TYPE_IS_NULL);
		Preconditions.checkArgument(cmsSiteModel != null, CMSSITE_NOT_FOUND);
		Optional<ThirdPartyAuthenticationProviderModel> thirdPartyAuthenticationProvider = Optional.empty();
		switch (type)
		{
			case FACEBOOK:
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSiteModel, FacebookAuthenticationProviderModel.class);
				break;
			case GOOGLE:
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSiteModel, GoogleAuthenticationProviderModel.class);
				break;

			case TWITTER:
				Preconditions.checkArgument(callbackUrl != null, CALLBACK_URL_NOT_FOUND);
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSiteModel, TwitterAuthenticationProviderModel.class);
				break;
			default:
				throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER,
						ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER.getMsg());

		}
		if (thirdPartyAuthenticationProvider.isPresent())
		{
			final Optional<ThirdPartyAuthenticationStrategy> thirdPartyAuthenticationStratergy = getStrategy(
					thirdPartyAuthenticationProvider.get().getClass());
			if (thirdPartyAuthenticationStratergy.isPresent())
			{
				return thirdPartyAuthenticationStratergy.get().getFormData(data, thirdPartyAuthenticationProvider.get(), callbackUrl);
			}
		}
		return Optional.empty();
	}

	/**
	 * Gets the third party form data by current store.
	 *
	 * @param data
	 *           the data
	 * @param type
	 *           the type
	 * @param callbackUrl
	 *           the callback url
	 * @return the third party form data by current store
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	@Override
	public Optional<TwitterFormData> getThirdPartyFormDataByCurrentStore(final Object data,
			final ThirdPartyAuthenticationType type, final String callbackUrl) throws ThirdPartyAuthenticationException
	{
		final CMSSiteModel cmsSiteModel = cmsSiteService.getCurrentSite();
		return getThirdPartyFormData(data, type, cmsSiteModel, callbackUrl);
	}

	/**
	 * Verify access token with third party by current store.
	 *
	 * @param data
	 *           the data
	 * @param token
	 *           the token
	 * @param type
	 *           the type
	 * @param callbackUrl
	 *           the callback url
	 * @return true, if successful
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	@Override
	public boolean verifyAccessTokenWithThirdPartyByCurrentSite(final Object data, final Object token,
			final ThirdPartyAuthenticationType type, final String callbackUrl) throws ThirdPartyAuthenticationException
	{
		final CMSSiteModel cmsSiteModel = cmsSiteService.getCurrentSite();
		return verifyAccessTokenWithThirdParty(data, token, type, cmsSiteModel, callbackUrl);
	}

	/**
	 * Verify access token with third party.
	 *
	 * @param data
	 *           the data
	 * @param token
	 *           the token
	 * @param type
	 *           the type
	 * @param cmsSiteModel
	 *           the cms site model
	 * @param callbackUrl
	 *           the callback url
	 * @return true, if successful
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	@Override
	public boolean verifyAccessTokenWithThirdParty(final Object data, final Object token, final ThirdPartyAuthenticationType type,
			final CMSSiteModel cmsSiteModel, final String callbackUrl) throws ThirdPartyAuthenticationException
	{
		Preconditions.checkArgument(data != null, DATA_IS_NULL);
		Preconditions.checkArgument(type != null, THIRDPARTY_AUTHENTICATION_TYPE_IS_NULL);
		Preconditions.checkArgument(cmsSiteModel != null, CMSSITE_NOT_FOUND);
		Optional<ThirdPartyAuthenticationProviderModel> thirdPartyAuthenticationProvider = Optional.empty();
		switch (type)
		{
			case FACEBOOK:
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSiteModel, FacebookAuthenticationProviderModel.class);
				break;

			case GOOGLE:
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSiteModel, GoogleAuthenticationProviderModel.class);
				break;

			case TWITTER:
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSiteModel, TwitterAuthenticationProviderModel.class);
				break;
			case APPLE:
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSiteModel, AppleAuthenticationProviderModel.class);
				break;
			default:
				throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER,
						ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER.getMsg());

		}
		if (thirdPartyAuthenticationProvider.isPresent())
		{
			final Optional<ThirdPartyAuthenticationStrategy> thirdPartyAuthenticationStratergy = getStrategy(
					thirdPartyAuthenticationProvider.get().getClass());
			if (thirdPartyAuthenticationStratergy.isPresent())
			{
				return thirdPartyAuthenticationStratergy.get().verifyAccessTokenWithThirdParty(data, token,
						thirdPartyAuthenticationProvider.get());
			}
		}
		return false;
	}

	@Override
	public Optional<ThirdPartyUserData> getThirdPartyProfileByCurrentSite(final Object data,
			final ThirdPartyAuthenticationType type) throws ThirdPartyAuthenticationException
	{
		final CMSSiteModel cmsSiteModel = cmsSiteService.getCurrentSite();
		return getThirdPartyProfileBySite(data, type, cmsSiteModel);
	}

	@Override
	public Optional<ThirdPartyUserData> getThirdPartyProfileBySite(final Object data, final ThirdPartyAuthenticationType type,
			final CMSSiteModel cmsSite) throws ThirdPartyAuthenticationException
	{
		Preconditions.checkArgument(data != null, DATA_IS_NULL);
		Preconditions.checkArgument(type != null, THIRDPARTY_AUTHENTICATION_TYPE_IS_NULL);
		Preconditions.checkArgument(cmsSite != null, CMSSITE_NOT_FOUND);

		final Optional<ThirdPartyAuthenticationUserData> thirdPartyUserData = this.getThirdPartyUserData(data, type, cmsSite);
		if (thirdPartyUserData.isEmpty())
		{
			return Optional.empty();
		}

		return createThirPartyUserData(thirdPartyUserData);
	}

	private Optional<ThirdPartyUserData> createThirPartyUserData(
			final Optional<ThirdPartyAuthenticationUserData> thirdPartyUserData)
	{
		if (thirdPartyUserData.isEmpty())
		{
			return Optional.empty();
		}
		final Boolean userExist = thirdPartyUserService.isUserExist(thirdPartyUserData.get().getId());
		final ThirdPartyUserData user = new ThirdPartyUserData();
		user.setExists(userExist);
		user.setUserData(populateThirdPartyProfileData(thirdPartyUserData.get()));
		return Optional.ofNullable(user);
	}

	/**
	 *
	 */
	private ThirdPartyProfileData populateThirdPartyProfileData(final ThirdPartyAuthenticationUserData userData)
	{
		final ThirdPartyProfileData profile = new ThirdPartyProfileData();
		profile.setBirthDate(userData.getDateOfBirth());
		profile.setEmail(userData.getEmail());
		profile.setFirstName(userData.getFirstName());
		profile.setLastName(userData.getLastName());
		profile.setId(userData.getId());
		profile.setMaritalStatusCode(userData.getMaritalStatus() == null ? null : userData.getMaritalStatus().getCode());
		profile.setMobileNumber(userData.getMobileNumber());
		profile.setNationality(userData.getNationality());
		profile.setNationalityId(userData.getNationalityId());
		return profile;
	}

	@Override
	public Optional<ThirdPartyUserData> getThirdPartyRegistrationProfileByCurrentSite(final Object data,
			final ThirdPartyAuthenticationType type) throws ThirdPartyAuthenticationException
	{
		final CMSSiteModel cmsSiteModel = cmsSiteService.getCurrentSite();
		return getThirdPartyRegistrationProfileBySite(data, type, cmsSiteModel);
	}

	@Override
	public Optional<ThirdPartyUserData> getThirdPartyRegistrationProfileBySite(final Object data,
			final ThirdPartyAuthenticationType type, final CMSSiteModel cmsSite) throws ThirdPartyAuthenticationException
	{
		Preconditions.checkArgument(data != null, DATA_IS_NULL);
		Preconditions.checkArgument(type != null, THIRDPARTY_AUTHENTICATION_TYPE_IS_NULL);
		Preconditions.checkArgument(cmsSite != null, CMSSITE_NOT_FOUND);
		Optional<ThirdPartyAuthenticationProviderModel> thirdPartyAuthenticationProvider;
		switch (type)
		{
			case FACEBOOK:
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSite, FacebookAuthenticationProviderModel.class);
				break;

			case GOOGLE:
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSite, GoogleAuthenticationProviderModel.class);
				break;

			case TWITTER:
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSite, TwitterAuthenticationProviderModel.class);
				break;
			case APPLE:
				thirdPartyAuthenticationProvider = thirdPartyAuthenticationProviderContext
						.getThirdPartyAuthenticationProvider(cmsSite, AppleAuthenticationProviderModel.class);
				break;
			default:
				throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER,
						ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER.getMsg());

		}
		if (thirdPartyAuthenticationProvider.isPresent())
		{
			final Optional<ThirdPartyAuthenticationStrategy> thirdPartyAuthenticationStratergy = getStrategy(
					thirdPartyAuthenticationProvider.get().getClass());
			if (thirdPartyAuthenticationStratergy.isPresent())
			{
				final Optional<ThirdPartyAuthenticationUserData> registrationData = thirdPartyAuthenticationStratergy.get()
						.getThirdPartyUserRegistrationData(data, thirdPartyAuthenticationProvider.get());


				return createThirPartyUserData(registrationData);
			}
		}
		return Optional.empty();
	}

}
