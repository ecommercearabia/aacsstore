/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsthirdpartyauthentication.service;

import java.util.Optional;

import com.aacs.aacsthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.aacs.aacsthirdpartyauthentication.exception.ThirdPartyAuthenticationException;

/**
 *
 */
public interface AppleService
{

	public Optional<ThirdPartyAuthenticationUserData> getData(String authorizationCode, String jwtToken, String clientId)
			throws ThirdPartyAuthenticationException;

	public Optional<ThirdPartyAuthenticationUserData> getRegisterData(String refreshToken, String jwtToken, String clientId)
			throws ThirdPartyAuthenticationException;

	public boolean verifyThirdPartyAccessToken(String authorizationCode, String jwtToken, String clientId, String id)
			throws ThirdPartyAuthenticationException;

}
