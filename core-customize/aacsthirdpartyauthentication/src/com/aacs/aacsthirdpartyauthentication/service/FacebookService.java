/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsthirdpartyauthentication.service;

import java.util.Optional;

import com.aacs.aacsthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.aacs.aacsthirdpartyauthentication.exception.ThirdPartyAuthenticationException;

/**
 * The Interface FacebookService.
 */
public interface FacebookService
{

	/**
	 * Gets the data.
	 *
	 * @param token
	 *           the token
	 * @param appSecret
	 *           the app secret
	 * @return the data
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	public Optional<ThirdPartyAuthenticationUserData> getData(String token, String appSecret)
			throws ThirdPartyAuthenticationException;

	/**
	 * Verify third party access token.
	 *
	 * @param id
	 *           the id
	 * @param token
	 *           the token
	 * @param appSecret
	 *           the app secret
	 * @return true, if successful
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	public boolean verifyThirdPartyAccessToken(String id, String token, String appSecret)
			throws ThirdPartyAuthenticationException;
}
