/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsthirdpartyauthentication.service.impl;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.aacs.aacsthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.aacs.aacsthirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.aacs.aacsthirdpartyauthentication.exception.enums.ThirdPartyAuthenticationExceptionType;
import com.aacs.aacsthirdpartyauthentication.service.GoogleBasicService;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;


/**
 * The Class DefaultGoogleBasicService.
 */
public class DefaultGoogleBasicService implements GoogleBasicService
{


	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultGoogleBasicService.class);

	/**
	 * Gets the data.
	 *
	 * @param token
	 *           the token
	 * @param clientId
	 *           the client id
	 * @return the data
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	@Override
	public Optional<ThirdPartyAuthenticationUserData> getData(final String token, final String clientId)
			throws ThirdPartyAuthenticationException
	{
		LOG.info("GoogleBasicUserServiceImpl getData()");
		if (StringUtils.isEmpty(token) || StringUtils.isEmpty(clientId))
		{
			LOG.error("token or clientId is null");
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}

		final GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(),
				JacksonFactory.getDefaultInstance()).setAudience(Collections.singletonList(clientId)).build();

		GoogleIdToken idToken = null;
		try
		{
			idToken = verifier.verify(token);
		}
		catch (GeneralSecurityException | IOException e)
		{
			LOG.error("Not Authorized");
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED,
					ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED.getMsg());
		}
		if (idToken == null)
		{
			LOG.error("Not Authorized");
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED,
					ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED.getMsg());
		}

		final Payload payload = idToken.getPayload();

		final String userId = payload.getSubject();
		final String email = payload.getEmail();
		final String name = (String) payload.get("name");
		final String familyName = (String) payload.get("family_name");
		final String givenName = (String) payload.get("given_name");

		final ThirdPartyAuthenticationUserData user = new ThirdPartyAuthenticationUserData();
		user.setId(userId);
		user.setEmail(email);
		user.setName(name);
		user.setLastName(familyName);
		user.setFirstName(givenName);

		return Optional.ofNullable(user);

	}

	/**
	 * Verify third party access token.
	 *
	 * @param id
	 *           the id
	 * @param token
	 *           the token
	 * @param appSecret
	 *           the app secret
	 * @return true, if successful
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	@Override
	public boolean verifyThirdPartyAccessToken(final String id, final String token, final String appSecret)
			throws ThirdPartyAuthenticationException
	{
		if (StringUtils.isEmpty(id) || StringUtils.isEmpty(token) || StringUtils.isEmpty(appSecret))
		{
			LOG.error("token or appSecret is null");
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}
		final Optional<ThirdPartyAuthenticationUserData> data = this.getData(token, appSecret);
		if (data.isEmpty())
		{
			return false;
		}
		return data.get().getId().equals(id);
	}

}
