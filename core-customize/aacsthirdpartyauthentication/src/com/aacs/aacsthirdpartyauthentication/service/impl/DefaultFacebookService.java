/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsthirdpartyauthentication.service.impl;

import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.aacs.aacsthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.aacs.aacsthirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.aacs.aacsthirdpartyauthentication.exception.enums.ThirdPartyAuthenticationExceptionType;
import com.aacs.aacsthirdpartyauthentication.service.FacebookService;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.Version;
import com.restfb.types.User;


/**
 * The Class DefaultFacebookService.
 */
public class DefaultFacebookService implements FacebookService
{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultFacebookService.class);

	/** The Constant ATTRIBUTES. */
	private static final String ATTRIBUTES = "name,first_name,last_name,email";

	/**
	 * Gets the data.
	 *
	 * @param token
	 *           the token
	 * @param appSecret
	 *           the app secret
	 * @return the data
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	@Override
	public Optional<ThirdPartyAuthenticationUserData> getData(final String token, final String appSecret)
			throws ThirdPartyAuthenticationException
	{
		LOG.info("FacebookUserServiceImpl getData()");
		if (StringUtils.isEmpty(token) || StringUtils.isEmpty(appSecret))
		{
			LOG.error("token or appSecret is null");
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}

		final FacebookClient facebookClient = new DefaultFacebookClient(token, appSecret, Version.LATEST);
		final User user = facebookClient.fetchObject("me", User.class, Parameter.with("fields", ATTRIBUTES));
		if (user == null)
		{
			LOG.error("Not Authorized");
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED,
					ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED.getMsg());
		}

		final String firstName = user.getFirstName();
		final String lastName = user.getLastName();
		final String email = user.getEmail();
		final String name = user.getName();
		final String gender = user.getGender();
		final String id = user.getId();
		final String birthday = user.getBirthday();

		final ThirdPartyAuthenticationUserData userData = new ThirdPartyAuthenticationUserData();
		userData.setId(id);
		userData.setFirstName(firstName);
		userData.setLastName(lastName);
		userData.setName(name);
		userData.setEmail(email);
		userData.setDateOfBirth(birthday);

		return Optional.ofNullable(userData);
	}

	/**
	 * Verify third party access token.
	 *
	 * @param id
	 *           the id
	 * @param token
	 *           the token
	 * @param appSecret
	 *           the app secret
	 * @return true, if successful
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	@Override
	public boolean verifyThirdPartyAccessToken(final String id, final String token, final String appSecret)
			throws ThirdPartyAuthenticationException
	{
		if (StringUtils.isEmpty(id) || StringUtils.isEmpty(token) || StringUtils.isEmpty(appSecret))
		{
			LOG.error("token or appSecret is null");
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}
		final Optional<ThirdPartyAuthenticationUserData> data = this.getData(token, appSecret);
		if (data.isEmpty())
		{
			return false;
		}
		return data.get().getId().equals(id);
	}

}
