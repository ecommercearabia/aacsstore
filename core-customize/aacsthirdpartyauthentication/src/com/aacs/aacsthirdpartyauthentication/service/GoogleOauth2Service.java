/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsthirdpartyauthentication.service;

import java.util.Optional;

import com.aacs.aacsthirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.aacs.aacsthirdpartyauthentication.exception.ThirdPartyAuthenticationException;


/**
 * The Interface GoogleOauth2Service.
 */
public interface GoogleOauth2Service
{

	/**
	 * Gets the data.
	 *
	 * @param code
	 *           the code
	 * @param clientId
	 *           the client id
	 * @param clientSecret
	 *           the client secret
	 * @return the data
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	public Optional<ThirdPartyAuthenticationUserData> getData(String code, String clientId, String clientSecret)
			throws ThirdPartyAuthenticationException;

	/**
	 * Verify third party access token.
	 *
	 * @param id
	 *           the id
	 * @param appId
	 *           the app id
	 * @param appSecret
	 *           the app secret
	 * @return true, if successful
	 * @throws ThirdPartyAuthenticationException
	 *            the third party authentication exception
	 */
	public boolean verifyThirdPartyAccessToken(String id, String appId, String appSecret)
			throws ThirdPartyAuthenticationException;
}
