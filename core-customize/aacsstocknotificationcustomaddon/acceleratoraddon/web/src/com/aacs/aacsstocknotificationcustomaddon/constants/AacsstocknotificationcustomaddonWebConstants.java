/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsstocknotificationcustomaddon.constants;

/**
 * Global class for all Aacsstocknotificationcustomaddon web constants. You can add global constants for your extension into this class.
 */
public final class AacsstocknotificationcustomaddonWebConstants // NOSONAR
{
	private AacsstocknotificationcustomaddonWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
