/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsstocknotificationcustomaddon.controllers;

/**
 */
public interface AacsstocknotificationcustomaddonControllerConstants
{
	String ADDON_PREFIX = "addon:/aacsstocknotificationcustomaddon/";

	interface Pages
	{
		String AddNotificationPage = ADDON_PREFIX + "pages/addStockNotification";
		String AddNotificationFromInterestPage = ADDON_PREFIX + "pages/addStockNotificationFromInterest";
	}
}
