/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsstocknotificationcustomaddon.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.aacs.aacsstocknotificationcustomaddon.constants.AacsstocknotificationcustomaddonConstants;
import org.apache.log4j.Logger;

public class AacsstocknotificationcustomaddonManager extends GeneratedAacsstocknotificationcustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( AacsstocknotificationcustomaddonManager.class.getName() );
	
	public static final AacsstocknotificationcustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AacsstocknotificationcustomaddonManager) em.getExtension(AacsstocknotificationcustomaddonConstants.EXTENSIONNAME);
	}
	
}
