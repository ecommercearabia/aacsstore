/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.aacs.fulfilmentprocess.constants.AacsFulfilmentProcessConstants;

public class AacsFulfilmentProcessManager extends GeneratedAacsFulfilmentProcessManager
{
	public static final AacsFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AacsFulfilmentProcessManager) em.getExtension(AacsFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
