/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.fulfilmentprocess.test.actions;

/**
 * Test counterpart for
 * {@link com.aacs.fulfilmentprocess.actions.order.PrepareOrderForManualCheckAction}
 */
public class PrepareOrderForManualCheck extends TestActionTemp
{
	//EMPTY
}
