/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscustomercouponcustomaddon.constants;

/**
 * Aacscustomercouponcustomaddon constants
 */
public final class AacscustomercouponcustomaddonConstants extends GeneratedAacscustomercouponcustomaddonConstants
{
	public static final String EXTENSIONNAME = "aacscustomercouponcustomaddon";

	private AacscustomercouponcustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}
}
