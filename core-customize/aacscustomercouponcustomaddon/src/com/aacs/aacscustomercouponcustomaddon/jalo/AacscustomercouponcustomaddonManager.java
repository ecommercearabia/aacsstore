/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscustomercouponcustomaddon.jalo;

import com.aacs.aacscustomercouponcustomaddon.constants.AacscustomercouponcustomaddonConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class AacscustomercouponcustomaddonManager extends GeneratedAacscustomercouponcustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( AacscustomercouponcustomaddonManager.class.getName() );
	
	public static final AacscustomercouponcustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AacscustomercouponcustomaddonManager) em.getExtension(AacscustomercouponcustomaddonConstants.EXTENSIONNAME);
	}
	
}
