/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.aacs.aacsthirdpartyauthenticationbackoffice.constants;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
public final class AacsthirdpartyauthenticationbackofficeConstants extends GeneratedAacsthirdpartyauthenticationbackofficeConstants
{
	public static final String EXTENSIONNAME = "aacsthirdpartyauthenticationbackoffice";

	private AacsthirdpartyauthenticationbackofficeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
