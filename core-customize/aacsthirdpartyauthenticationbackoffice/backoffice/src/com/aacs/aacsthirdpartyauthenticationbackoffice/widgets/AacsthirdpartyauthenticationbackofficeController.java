/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.aacs.aacsthirdpartyauthenticationbackoffice.widgets;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Label;

import com.hybris.cockpitng.util.DefaultWidgetController;

import com.aacs.aacsthirdpartyauthenticationbackoffice.services.AacsthirdpartyauthenticationbackofficeService;


public class AacsthirdpartyauthenticationbackofficeController extends DefaultWidgetController
{
	private static final long serialVersionUID = 1L;
	private Label label;

	@WireVariable
	private transient AacsthirdpartyauthenticationbackofficeService aacsthirdpartyauthenticationbackofficeService;

	@Override
	public void initialize(final Component comp)
	{
		super.initialize(comp);
		label.setValue(aacsthirdpartyauthenticationbackofficeService.getHello() + " AacsthirdpartyauthenticationbackofficeController");
	}
}
