/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.aacs.aacsthirdpartyauthenticationbackoffice.services;

/**
 * Hello World AacsthirdpartyauthenticationbackofficeService
 */
public class AacsthirdpartyauthenticationbackofficeService
{
	public String getHello()
	{
		return "Hello";
	}
}
