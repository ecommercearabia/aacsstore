/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsacceleratorcustomocc.controllers;

public interface AacsacceleratorcustomoccControllerConstants
{
	// implement here controller constants used by this extension
}
