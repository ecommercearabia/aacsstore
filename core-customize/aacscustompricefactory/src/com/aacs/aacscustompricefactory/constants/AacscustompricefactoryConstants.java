/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscustompricefactory.constants;

/**
 * Global class for all Aacscustompricefactory constants. You can add global constants for your extension into this class.
 */
public final class AacscustompricefactoryConstants extends GeneratedAacscustompricefactoryConstants
{
	public static final String EXTENSIONNAME = "aacscustompricefactory";

	private AacscustompricefactoryConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
