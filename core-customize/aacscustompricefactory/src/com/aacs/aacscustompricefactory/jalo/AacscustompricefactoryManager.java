package com.aacs.aacscustompricefactory.jalo;

import com.aacs.aacscustompricefactory.constants.AacscustompricefactoryConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class AacscustompricefactoryManager extends GeneratedAacscustompricefactoryManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( AacscustompricefactoryManager.class.getName() );
	
	public static final AacscustompricefactoryManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AacscustompricefactoryManager) em.getExtension(AacscustompricefactoryConstants.EXTENSIONNAME);
	}
	
}
