/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscustompricefactory.catalog.jalo;

import de.hybris.platform.europe1.channel.strategies.RetrieveChannelStrategy;
import de.hybris.platform.europe1.constants.Europe1Tools;
import de.hybris.platform.europe1.enums.PriceRowChannel;
import de.hybris.platform.europe1.jalo.PriceRow;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.C2LManager;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.product.Unit;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.StandardDateRange;
import de.hybris.platform.util.TaxValue;
import de.hybris.platform.util.localization.Localization;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aacs.aacscustompricefactory.jalo.AacscustompricefactoryManager;


/**
 *
 */
public class CustomEurope1PriceFactory extends AacscustompricefactoryManager
{

	private static final Logger LOG = LoggerFactory.getLogger(CustomEurope1PriceFactory.class);

	@Resource(name = "retrieveChannelStrategy")
	private RetrieveChannelStrategy retrieveChannelStrategy;

	@Resource(name = "modelService")
	private ModelService modelService;

	private static boolean isFirstTime = true;

	@Override
	public String getName()
	{
		// Note(Husam): I know this solution is not scalable or in any way elegant but this works for our needs
		//					 and this way we do not exclude Europe1 or CustomEurope1PriceFactory from initialization
		if (isFirstTime())
		{
			setFirstTime(false);
			return "europe1";
		}
		// Try to find a way to distinguish which bean actually was called here
		return super.getName();
	}

	@Override
	public PriceValue getBasePrice(final AbstractOrderEntry entry) throws JaloPriceFactoryException
	{
		LOG.info("getBasePrice For {}", entry.getProduct().getCode());
		final SessionContext ctx = this.getSession().getSessionContext();
		final AbstractOrder order = entry.getOrder(ctx);
		final Product product = entry.getProduct();
		final boolean giveAwayMode = entry.isGiveAway(ctx);
		final boolean entryIsRejected = entry.isRejected(ctx);
		final Currency currency = order.getCurrency(ctx);
		final EnumerationValue productGroup = this.getPPG(ctx, product);
		final User user = order.getUser();
		final EnumerationValue userGroup = this.getUPG(ctx, user);
		final Unit unit = entry.getUnit(ctx);
		final long quantity = entry.getQuantity(ctx);
		final boolean net = order.isNet();
		final Date date = new Date();
		PriceRow row;
		if (giveAwayMode && entryIsRejected)
		{
			row = null;
		}
		else
		{
			row = this.matchPriceRowForPrice(ctx, product, productGroup, user, userGroup, quantity, unit, currency, date, net,
					giveAwayMode);
		}

		if (row != null)
		{
			final Currency rowCurr = row.getCurrency();
			double price;
			if (currency.equals(rowCurr))
			{
				price = row.getPriceAsPrimitive() / row.getUnitFactorAsPrimitive();
			}
			else
			{
				price = rowCurr.convert(currency, row.getPriceAsPrimitive() / row.getUnitFactorAsPrimitive());
			}

			final Unit priceUnit = row.getUnit();
			final Unit entryUnit = entry.getUnit();
			final double convertedPrice = priceUnit.convertExact(entryUnit, price);
			return new PriceValue(currency.getIsoCode(), convertedPrice, row.isNetAsPrimitive());
		}
		else if (giveAwayMode)
		{
			return new PriceValue(order.getCurrency(ctx).getIsoCode(), 0.0D, order.isNet());
		}
		else
		{
			final String msg = Localization
					.getLocalizedString("exception.europe1pricefactory.getbaseprice.jalopricefactoryexception1", new Object[]
					{ product, productGroup, user, userGroup, Long.toString(quantity), unit, currency, date, Boolean.toString(net) });
			throw new JaloPriceFactoryException(msg, 0);
		}
		//		return super.getBasePrice(entry);
	}

	@Override
	protected List<PriceRow> filterPriceRows(final List<PriceRow> priceRows)
	{
		if (priceRows.isEmpty())
		{
			return Collections.emptyList();
		}

		Unit lastUnit = null;
		long lastMin = -1L;

		final ArrayList<PriceRow> ret = new ArrayList<>(priceRows);
		for (final ListIterator<PriceRow> it = ret.listIterator(); it.hasNext();)
		{

			final PriceRow row = it.next();

			final long min = row.getMinQuantity();
			final Unit unit = row.getUnit();


			if (lastUnit != null && lastUnit.equals(unit) && lastMin == min)
			{

				it.remove();

				continue;
			}
			lastUnit = unit;
			lastMin = min;

		}
		return ret;
	}

	@Override
	public List<PriceRow> matchPriceRowsForInfo(final SessionContext ctx, final Product product,
			final EnumerationValue productGroup, final User user, final EnumerationValue userGroup, final Currency currency,
			final Date date, final boolean net) throws JaloPriceFactoryException
	{
		if (product == null && productGroup == null)
		{
			throw new JaloPriceFactoryException(
					"cannot match price info without product and product group - at least one must be present", 0);
		}

		if (user == null && userGroup == null)
		{
			throw new JaloPriceFactoryException("cannot match price info without user and user group - at least one must be present",
					0);
		}

		if (currency == null)
		{
			throw new JaloPriceFactoryException("cannot match price info without currency", 0);
		}

		if (date == null)
		{
			throw new JaloPriceFactoryException("cannot match price info without date", 0);
		}

		final Collection<PriceRow> rows = queryPriceRows4Price(ctx, product, productGroup, user, userGroup, date, currency, false);

		if (rows.isEmpty())
		{
			return Collections.emptyList();
		}


		final PriceRowChannel channel = this.retrieveChannelStrategy.getChannel(ctx);
		final List<PriceRow> ret = new ArrayList<>(rows);
		if (ret.size() > 1)
		{
			ret.sort(new PriceRowInfoComparator(currency, net));
		}

		return filterPriceRows4Info(ret, currency, date, channel);
	}


	public List<PriceRowModel> getPriceRowModels(final SessionContext ctx, final Product product, final boolean net,
			final Date date) throws JaloPriceFactoryException
	{

		final EnumerationValue productGroup = this.getPPG(ctx, product);
		final User user = ctx.getUser();
		final EnumerationValue userGroup = getUPG(ctx, ctx.getUser());
		final Currency curr = ctx.getCurrency();
		final Collection<PriceRow> priceRows = matchPriceRowsForInfo(ctx, product, productGroup, user, userGroup, curr, date, net);


		final List<PriceRowModel> result = new ArrayList<>();

		for (final PriceRow row : priceRows)
		{
			final PriceRowModel priceRowModel = modelService.get(row);
			if (priceRowModel != null)
			{
				result.add(priceRowModel);
			}
		}
		return result;
	}

	@Override
	protected List<PriceInformation> getPriceInformations(final SessionContext ctx, final Product product,
			final EnumerationValue productGroup, final User user, final EnumerationValue userGroup, final Currency curr,
			final boolean net, final Date date, final Collection taxValues) throws JaloPriceFactoryException
	{
		final Collection<PriceRow> priceRows = this
				.filterPriceRows(this.matchPriceRowsForInfo(ctx, product, productGroup, user, userGroup, curr, date, net));
		final List<PriceInformation> priceInfos = new ArrayList<>(priceRows.size());
		Collection<TaxValue> theTaxValues = taxValues;
		final List<PriceInformation> defaultPriceInfos = new ArrayList<>(priceRows.size());
		final PriceRowChannel channel = this.retrieveChannelStrategy.getChannel(ctx);
		final Iterator<PriceRow> var16 = priceRows.iterator();

		while (true)
		{
			while (var16.hasNext())
			{
				final PriceRow row = var16.next();
				PriceInformation pInfo = Europe1Tools.createPriceInformation(row, curr);
				if (pInfo.getPriceValue().isNet() != net)
				{
					if (theTaxValues == null)
					{
						theTaxValues = Europe1Tools.getTaxValues(
								this.getTaxInformations(product, this.getPTG(ctx, product), user, this.getUTG(ctx, user), date));
					}

					pInfo = new PriceInformation(pInfo.getQualifiers(), pInfo.getPriceValue().getOtherPrice(theTaxValues));
				}

				if (row.getChannel() == null)
				{
					defaultPriceInfos.add(pInfo);
				}

				if ((channel == null && row.getChannel() == null) || (channel != null && row.getChannel() != null
						&& row.getChannel().getCode().equalsIgnoreCase(channel.getCode())))
				{
					priceInfos.add(pInfo);
				}
			}

			if (priceInfos.isEmpty())
			{
				return defaultPriceInfos;
			}

			return priceInfos;
		}
	}

	@Override
	protected List<PriceRow> filterPriceRows4Info(final Collection<PriceRow> rows, final Currency curr, final Date date,
			final PriceRowChannel channel)
	{
		if (rows.isEmpty())
		{
			return Collections.emptyList();
		}


		final Currency base = curr.isBase().booleanValue() ? null : C2LManager.getInstance().getBaseCurrency();

		final List<PriceRow> ret = new ArrayList<>(rows);
		boolean hasChannelRowMatching = false;
		ListIterator<PriceRow> it;
		for (it = ret.listIterator(); it.hasNext();)
		{

			final PriceRow priceRow = it.next();
			final Currency currency = priceRow.getCurrency();
			if (!curr.equals(currency) && (base == null || !base.equals(currency)))
			{

				it.remove();
				continue;
			}
			final StandardDateRange standardDateRange = priceRow.getDateRange();
			if (standardDateRange != null && !standardDateRange.encloses(date))
			{

				it.remove();
				continue;
			}
			if (priceRow.isGiveAwayPriceAsPrimitive())
			{

				it.remove();







				continue;
			}
			if (channel != null && priceRow.getChannel() != null
					&& !priceRow.getChannel().getCode().equalsIgnoreCase(channel.getCode()))
			{

				it.remove();

				continue;
			}
			if (channel != null && priceRow.getChannel() != null
					&& priceRow.getChannel().getCode().equalsIgnoreCase(channel.getCode()))
			{
				hasChannelRowMatching = true;
			}



		}
		if (hasChannelRowMatching && ret.size() > 1)
		{
			for (it = ret.listIterator(); it.hasNext();)
			{

				final PriceRow priceRow = it.next();
				if (priceRow.getChannel() == null)
				{
					it.remove();
				}



			}
		}
		return ret;
	}

	@Override
	public PriceRow matchPriceRowForPrice(final SessionContext ctx, final Product product, final EnumerationValue productGroup,
			final User user, final EnumerationValue userGroup, final long qtd, final Unit unit, final Currency currency,
			final Date date, final boolean net, final boolean giveAwayMode) throws JaloPriceFactoryException
	{
		if (product == null && productGroup == null)
		{
			throw new JaloPriceFactoryException(
					"cannot match price without product and product group - at least one must be present", 0);
		}

		if (user == null && userGroup == null)
		{
			throw new JaloPriceFactoryException("cannot match price without user and user group - at least one must be present", 0);
		}

		if (currency == null)
		{
			throw new JaloPriceFactoryException("cannot match price without currency", 0);
		}

		if (date == null)
		{
			throw new JaloPriceFactoryException("cannot match price without date", 0);
		}

		if (unit == null)
		{
			throw new JaloPriceFactoryException("cannot match price without unit", 0);
		}


		final Collection<PriceRow> rows = queryPriceRows4Price(ctx, product, productGroup, user, userGroup, date, currency,
				giveAwayMode);
		if (!rows.isEmpty())
		{

			final PriceRowChannel channel = this.retrieveChannelStrategy.getChannel(ctx);
			final List<PriceRow> list = filterPriceRows4Price(rows, qtd, unit, currency, date, giveAwayMode, channel);

			if (list.isEmpty())
			{
				return null;
			}

			if (list.size() == 1)
			{
				return list.get(0);
			}



			list.sort(new PriceRowMatchComparator(currency, net, unit));
			return list.get(0);



		}
		return null;
	}



	/**
	 * @return the isFirstTime
	 */
	public static boolean isFirstTime()
	{
		return isFirstTime;
	}

	/**
	 * @param isFirstTime
	 *           the isFirstTime to set
	 */
	public static void setFirstTime(final boolean isFirstTime)
	{
		CustomEurope1PriceFactory.isFirstTime = isFirstTime;
	}



	protected class PriceRowMatchComparator implements Comparator<PriceRow>
	{
		private static final String PROMOTION_STRING = "promotion";

		private final Currency curr;

		private final boolean net;

		private final Unit unit;

		protected PriceRowMatchComparator(final Currency curr, final boolean net, final Unit unit)
		{
			this.curr = curr;
			this.net = net;
			this.unit = unit;
		}

		public int compare(final PriceRow row1, final PriceRow row2)
		{
			final int matchValue1 = row1.getMatchValueAsPrimitive();
			final int matchValue2 = row2.getMatchValueAsPrimitive();
			if (matchValue1 != matchValue2)
			{
				return matchValue2 - matchValue1;
			}



			final boolean c1Match = this.curr.equals(row1.getCurrency());
			final boolean c2Match = this.curr.equals(row2.getCurrency());
			if (c1Match != c2Match)
			{
				return c1Match ? -1 : 1;
			}



			final boolean n1Match = (this.net == row1.isNetAsPrimitive());
			final boolean n2Match = (this.net == row2.isNetAsPrimitive());
			if (n1Match != n2Match)
			{
				return n1Match ? -1 : 1;
			}



			final boolean u1Match = this.unit.equals(row1.getUnit());
			final boolean u2Match = this.unit.equals(row2.getUnit());
			if (u1Match != u2Match)
			{
				return u1Match ? -1 : 1;
			}



			final long min1 = row1.getMinqtdAsPrimitive();
			final long min2 = row2.getMinqtdAsPrimitive();
			if (min1 != min2)
			{
				return (min1 > min2) ? -1 : 1;
			}



			final boolean row1Range = (row1.getStartTime() != null);
			final boolean row2Range = (row2.getStartTime() != null);

			if (row1Range != row2Range)
			{
				return row1Range ? -1 : 1;
			}

			final boolean isPromotion1 = (boolean) (row1.getProperty(PROMOTION_STRING));
			final boolean isPromotion2 = (boolean) (row2.getProperty(PROMOTION_STRING));

			if (isPromotion1 != isPromotion2)
			{
				return isPromotion1 ? -1 : 1;
			}

			//Europe1PriceFactory.LOG.warn("found ambigous price rows " + row1 + " and " + row2 + " - using PK to distinguish");

			return row1.getPK().compareTo(row2.getPK());
		}
	}



	protected class PriceRowInfoComparator implements Comparator<PriceRow>
	{
		private static final String PROMOTION_STRING = "promotion";

		private final Currency curr;

		private final boolean net;

		protected PriceRowInfoComparator(final Currency curr, final boolean net)
		{
			this.curr = curr;
			this.net = net;
		}

		public int compare(final PriceRow row1, final PriceRow row2)
		{
			final long u1Match = row1.getUnit().getPK().getLongValue();
			final long u2Match = row2.getUnit().getPK().getLongValue();
			if (u1Match != u2Match)
			{
				return (u1Match < u2Match) ? -1 : 1;
			}


			final long min1 = row1.getMinqtdAsPrimitive();
			final long min2 = row2.getMinqtdAsPrimitive();
			if (min1 != min2)
			{
				return (min1 > min2) ? -1 : 1;
			}


			final int matchValue1 = row1.getMatchValueAsPrimitive();
			final int matchValue2 = row2.getMatchValueAsPrimitive();
			if (matchValue1 != matchValue2)
			{
				return matchValue2 - matchValue1;
			}


			final boolean c1Match = this.curr.equals(row1.getCurrency());
			final boolean c2Match = this.curr.equals(row2.getCurrency());
			if (c1Match != c2Match)
			{
				return c1Match ? -1 : 1;
			}


			final boolean n1Match = (this.net == row1.isNetAsPrimitive());
			final boolean n2Match = (this.net == row2.isNetAsPrimitive());
			if (n1Match != n2Match)
			{
				return n1Match ? -1 : 1;
			}


			final boolean row1Range = (row1.getStartTime() != null);
			final boolean row2Range = (row2.getStartTime() != null);

			if (row1Range != row2Range)
			{
				return row1Range ? -1 : 1;
			}

			final boolean isPromotion1 = row1.getProperty(PROMOTION_STRING) != null
					&& (boolean) (row1.getProperty(PROMOTION_STRING));
			final boolean isPromotion2 = row2.getProperty(PROMOTION_STRING) != null
					&& (boolean) (row2.getProperty(PROMOTION_STRING));

			if (isPromotion1 != isPromotion2)
			{
				return isPromotion1 ? -1 : 1;
			}

			return row1.getPK().compareTo(row2.getPK());
		}
	}


}
