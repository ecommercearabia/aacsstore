/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscmscustomocc.beans;

import de.hybris.platform.webservicescommons.dto.error.ErrorWsDTO;

import java.io.Serializable;
import java.util.List;

import com.aacs.aacscmscustomocc.jaxb.adapters.components.ComponentAdapterUtil.ComponentAdaptedData;


/**
 * @author monzer
 */
public class ComponentResultDTO implements Serializable
{

	public enum ResultEnum
	{
		SUCCESS, ERROR;
	}

	private ResultEnum status;
	private List<ErrorWsDTO> errors;
	private ComponentAdaptedData component;


	/**
	 * @return the errors
	 */
	public List<ErrorWsDTO> getErrors()
	{
		return errors;
	}

	/**
	 * @param errors
	 *           the errors to set
	 */
	public void setErrors(final List<ErrorWsDTO> errors)
	{
		this.errors = errors;
	}

	/**
	 * @return the component
	 */
	public ComponentAdaptedData getComponent()
	{
		return component;
	}

	/**
	 * @param component
	 *           the component to set
	 */
	public void setComponent(final ComponentAdaptedData component)
	{
		this.component = component;
	}

	/**
	 * @return the status
	 */
	public ResultEnum getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *           the status to set
	 */
	public void setStatus(final ResultEnum status)
	{
		this.status = status;
	}

}
