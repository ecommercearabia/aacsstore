/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscmscustomocc.controllers;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.contents.components.SimpleCMSComponentModel;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel;
import de.hybris.platform.cmsfacades.data.AbstractCMSComponentData;
import de.hybris.platform.cmsfacades.exception.ValidationException;
import de.hybris.platform.cmsfacades.items.ComponentItemFacade;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.request.mapping.annotation.ApiVersion;
import de.hybris.platform.commercewebservices.core.product.data.CategoryDataList;
import de.hybris.platform.commercewebservicescommons.dto.product.CategoryListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.product.ProductListWsDTO;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.servicelayer.data.SearchPageData;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.dto.PaginationWsDTO;
import de.hybris.platform.webservicescommons.dto.SortWsDTO;
import de.hybris.platform.webservicescommons.dto.error.ErrorWsDTO;
import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;
import de.hybris.platform.webservicescommons.pagination.WebPaginationUtils;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdParam;
import de.hybris.platform.webservicescommons.util.YSanitizer;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aacs.aacscmscustomocc.beans.ComponentResultDTO;
import com.aacs.aacscmscustomocc.beans.ComponentResultDTO.ResultEnum;
import com.aacs.aacscmscustomocc.beans.ComponentResultListDTO;
import com.aacs.aacscmscustomocc.data.ComponentIDListWsDTO;
import com.aacs.aacscmscustomocc.data.ComponentListWsDTO;
import com.aacs.aacscmscustomocc.data.ComponentWsDTO;
import com.aacs.aacscmscustomocc.jaxb.adapters.components.ComponentAdapterUtil.ComponentAdaptedData;
import com.aacs.aacscmscustomocc.jaxb.adapters.components.ComponentListWsDTOAdapter;
import com.aacs.aacscmscustomocc.jaxb.adapters.components.ComponentListWsDTOAdapter.ListAdaptedComponents;
import com.aacs.aacscmscustomocc.jaxb.adapters.components.ComponentWsDTOAdapter;
import com.aacs.aacscmscustomocc.mapping.CMSDataMapper;
import com.aacs.aacscommercewebservices.core.product.data.ProductDataList;
import com.aacs.facades.facade.CategoryFacade;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


/**
 * Default Controller for CMS Component. This controller is used for all CMS components that don\"t have a specific
 * controller to handle them.
 */
@Controller
@RequestMapping(value = "/{baseSiteId}/cms")
@CacheControl(directive = CacheControlDirective.NO_CACHE)
@ApiVersion("v2")
@Api(tags = "Components")
public class ComponentController
{
	public static final String DEFAULT_CURRENT_PAGE = "0";
	public static final String DEFAULT_PAGE_SIZE = "10";
	private static final Logger LOGGER = LoggerFactory.getLogger(ComponentController.class);

	@Resource(name = "componentItemFacade")
	private ComponentItemFacade componentItemFacade;

	@Resource(name = "cmsDataMapper")
	private CMSDataMapper dataMapper;

	@Resource(name = "webPaginationUtils")
	private WebPaginationUtils webPaginationUtils;

	@Resource(name = "cwsProductFacade")
	private ProductFacade productFacade;


	@Resource(name = "cmsComponentService")
	private CMSComponentService cmsComponentService;


	@Resource(name = "categoryFacade")
	private CategoryFacade categoryFacade;


	private static final EnumSet<ProductOption> PRODUCT_OPTIONS_SET = EnumSet.allOf(ProductOption.class);


	@GetMapping(value = "/components/{componentId}")
	@ResponseBody
	@ApiOperation(value = "Get component data by id", notes = "Given a component identifier, return cms component data.", nickname = "getComponentById")
	@ApiBaseSiteIdParam
	public ComponentAdaptedData getComponentById(@ApiParam(value = "Component identifier", required = true)
	@PathVariable
	final String componentId, @ApiParam(value = "Catalog code")
	@RequestParam(required = false)
	final String catalogCode, @ApiParam(value = "Product code")
	@RequestParam(required = false)
	final String productCode, @ApiParam(value = "Category code")
	@RequestParam(required = false)
	final String categoryCode,
			@ApiParam(value = "Response configuration (list of fields, which should be returned in response)", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = "DEFAULT")
			final String fields) throws CMSItemNotFoundException
	{
		try
		{
			final AbstractCMSComponentData componentData = getComponentItemFacade().getComponentById(componentId, categoryCode,
					productCode, catalogCode);
			final ComponentWsDTO data = (ComponentWsDTO) getDataMapper().map(componentData, fields);

			final ComponentWsDTOAdapter adapter = new ComponentWsDTOAdapter();
			return adapter.marshal(data);
		}
		catch (final ValidationException e)
		{
			LOGGER.info("Validation exception", e);
			throw new WebserviceValidationException(e.getValidationObject());
		}
	}

	@GetMapping(value = "/components/{componentId}/products")
	@CacheControl(directive = CacheControlDirective.PRIVATE, maxAge = 120)
	@Cacheable(value = "productCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(true,true,#componentId,#fields)")
	@ResponseBody
	@ApiOperation(value = "Get products component data by id", notes = "Given a component identifier, return products component data.", nickname = "getProductsComponentById")
	@ApiBaseSiteIdParam
	public ProductListWsDTO getProductsComponentById(@ApiParam(value = "Component identifier", required = true)
	@PathVariable
	final String componentId,
			@ApiParam(value = "Response configuration (list of fields, which should be returned in response)", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = "DEFAULT")
			final String fields) throws CMSItemNotFoundException
	{
		try
		{
			LOGGER.debug("getProductsComponentById: componentId={} | options={}", sanitize(componentId), PRODUCT_OPTIONS_SET);

			final List<ProductData> products = getProductsByComponentId(componentId);

			return getDataMapper().map(convertResultset(products), ProductListWsDTO.class, fields);
			//			final ProductData product = productFacade.getProductForCodeAndOptions(productCode, PRODUCT_OPTIONS_SET);
		}
		catch (final ValidationException e)
		{
			LOGGER.info("Validation exception", e);
			throw new WebserviceValidationException(e.getValidationObject());
		}
	}

	/**
	 * @throws CMSItemNotFoundException
	 *
	 */
	private List<ProductData> getProductsByComponentId(final String componentId) throws CMSItemNotFoundException
	{
		final AbstractCMSComponentData componentData = getComponentItemFacade().getComponentById(componentId, null, null, null);

		final List<ProductData> list = new LinkedList<>();
		if (componentData != null && "ProductCarouselComponent".equalsIgnoreCase(componentData.getTypeCode()))
		{

			final SimpleCMSComponentModel simpleCMSComponent = cmsComponentService.getSimpleCMSComponent(componentId);
			if (!(simpleCMSComponent instanceof ProductCarouselComponentModel))
			{
				return list;
			}
			final ProductCarouselComponentModel componentModel = (ProductCarouselComponentModel) simpleCMSComponent;


			final List<ProductModel> productModels = componentModel.getProducts();
			for (final ProductModel product : productModels)
			{
				try
				{
					list.add(productFacade.getProductForCodeAndOptions(product.getCode(), PRODUCT_OPTIONS_SET));
				}
				catch (final UnknownIdentifierException ex)
				{
					LOGGER.warn("product code [" + product.getCode() + "] not found");
				}
			}
		}

		return list;
	}

	protected ProductDataList convertResultset(final List<ProductData> products)
	{
		final ProductDataList result = new ProductDataList();
		if (!org.springframework.util.CollectionUtils.isEmpty(products))
		{
			final int pageSize = products.size();
			result.setProducts(products);
			if (pageSize > 0)
			{
				result.setTotalPageCount(
						((products.size() % pageSize) == 0) ? (products.size() / pageSize) : ((products.size() / pageSize) + 1));
			}
			result.setCurrentPage(0);
			result.setTotalProductCount(products.size());
			result.setCatalog(null);
			result.setVersion(null);
		}

		return result;
	}

	protected static String sanitize(final String input)
	{
		return YSanitizer.sanitize(input);
	}

	/**
	 * This endpoint will trigger CORS preflight requests by the browser in a cross-origin setup, which is the most
	 * common cases. You can use the GET endpoint
	 * ({@link #findComponentsByIds(List, String, String, String, String, int, int, String)}) to easily circumvent the
	 * browser's preflight requests.
	 * <p>
	 * However, when requesting for a very large number of components using the GET endpoint, you may eventually reach
	 * the maximum number of characters allowed in a URL request. For example: <br>
	 * {@code Request Method: GET} <br>
	 * {@code Request URL: /cms/components?componentIds=uid1,uid2,uid3,...,uid1000&catalogCode=...}
	 * <p>
	 * This POST endpoint allows you to request for such large number of components, but you will have to address the
	 * preflight requests issues in a cross-origin setup.
	 *
	 * @deprecated since 1905, please use
	 *             {@link #findComponentsByIds(List, String, String, String, String, int, int, String)} instead.
	 */
	@PostMapping(value = "/components")
	@ResponseBody
	@ApiOperation(value = "Get components' data by id given in body", notes = "Given a list of component identifiers in body, return cms component data.", nickname = "searchComponentsByIds")
	@ApiBaseSiteIdParam
	@Deprecated(since = "1905", forRemoval = true)
	public ListAdaptedComponents searchComponentsByIds(@ApiParam(value = "List of Component identifiers", required = true)
	@RequestBody
	final ComponentIDListWsDTO componentIdList, @ApiParam(value = "Catalog code")
	@RequestParam(required = false)
	final String catalogCode, @ApiParam(value = "Product code")
	@RequestParam(required = false)
	final String productCode, @ApiParam(value = "Category code")
	@RequestParam(required = false)
	final String categoryCode,
			@ApiParam(value = "Response configuration (list of fields, which should be returned in response)", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = "DEFAULT")
			final String fields, @ApiParam(value = "Optional pagination parameter. Default value 0.")
			@RequestParam(required = false, defaultValue = DEFAULT_CURRENT_PAGE)
			final int currentPage, @ApiParam(value = "Optional pagination parameter. Default value 10.")
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_SIZE)
			final int pageSize, @ApiParam(value = "Optional sort criterion. No default value.")
			@RequestParam(required = false)
			final String sort)
	{
		// Validate for componentIdList, if componentIdList from body is null or empty, throw an IllegalArgumentException
		if (componentIdList == null || CollectionUtils.isEmpty(componentIdList.getIdList()))
		{
			throw new IllegalArgumentException("idList in the request body should contain component ID(s)");
		}

		return getComponentsByIds(componentIdList.getIdList(), catalogCode, productCode, categoryCode, fields, currentPage,
				pageSize, sort);
	}

	@GetMapping(value = "/components")
	@ResponseBody
	@ApiOperation(value = "Get component data", notes = "Finds cms components by the specified IDs. When none is provided, this will retrieve all components"
			+ "\nThe components list will be filtered by the given catalog, product or category restrictions, as well as by the "
			+ "pagination information. The result will be sorted in the specified order.", nickname = "getComponentsByIds")
	@ApiBaseSiteIdParam
	public ListAdaptedComponents findComponentsByIds( //
			@ApiParam(value = "List of Component identifiers")
			@RequestParam(required = false)
			final List<String> componentIds, @ApiParam(value = "Catalog code")
			@RequestParam(required = false)
			final String catalogCode, @ApiParam(value = "Product code")
			@RequestParam(required = false)
			final String productCode, @ApiParam(value = "Category code")
			@RequestParam(required = false)
			final String categoryCode,
			@ApiParam(value = "Response configuration (list of fields, which should be returned in response)", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = "DEFAULT")
			final String fields, @ApiParam(value = "Optional pagination parameter. Default value 0.")
			@RequestParam(required = false, defaultValue = DEFAULT_CURRENT_PAGE)
			final int currentPage, @ApiParam(value = "Optional pagination parameter. Default value 10.")
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_SIZE)
			final int pageSize, @ApiParam(value = "Optional sort criterion. No default value.")
			@RequestParam(required = false)
			final String sort)
	{
		return getComponentsByIds(componentIds, catalogCode, productCode, categoryCode, fields, currentPage, pageSize, sort);
	}

	/**
	 * Finds cms components by the specified IDs. When none is provided, this will retrieve all components. The
	 * components list will be filtered by the given catalog, product or category restrictions, as well as by the
	 * pagination information. The result will be sorted in the specified order.
	 *
	 * @param componentIds
	 *           - the list of component uid's
	 * @param catalogCode
	 *           - the catalog code determining the restriction to apply
	 * @param productCode
	 *           - the product code determining the restriction to apply
	 * @param categoryCode
	 *           - the category code determining the restriction to apply
	 * @param fields
	 *           - the response configuration determining which fields to include in the response
	 * @param currentPage
	 *           - the pagination information determining the page index
	 * @param pageSize
	 *           - the pagination information determining the page size
	 * @param sort
	 *           - the sorting information determining which field to sort on and the ordering direction (ASC or DESC)
	 * @return a list of cms component data
	 */
	protected ListAdaptedComponents getComponentsByIds(final List<String> componentIds, final String catalogCode,
			final String productCode, final String categoryCode, final String fields, final int currentPage, final int pageSize,
			final String sort)
	{
		try
		{
			// Creates a SearchPageData object contains requested pagination and sorting information
			final SearchPageData<AbstractCMSComponentData> searchPageDataInput = getWebPaginationUtils().buildSearchPageData(sort,
					currentPage, pageSize, true);

			// Get search result in a SearchPageData object contains search results with applied pagination and sorting information
			final SearchPageData<AbstractCMSComponentData> searchPageDataResult = getComponentItemFacade()
					.getComponentsByIds(componentIds, categoryCode, productCode, catalogCode, searchPageDataInput);

			return formatSearchPageDataResult(fields, searchPageDataResult);
		}
		catch (final ValidationException e)
		{
			LOGGER.info("Validation exception", e);
			throw new WebserviceValidationException(e.getValidationObject());
		}
	}

	/**
	 * Transforms the {@code SearchPageData} containing the list of cms component data into a
	 * {@code ListAdaptedComponents} object.
	 *
	 * @param fields
	 *           - the response configuration determining which fields to include in the response
	 * @param searchPageDataResult
	 *           - the configuration containing the pagination and sorting information
	 * @return a list of components of type {@link ListAdaptedComponents}
	 */
	protected ListAdaptedComponents formatSearchPageDataResult(final String fields,
			final SearchPageData<AbstractCMSComponentData> searchPageDataResult)
	{
		// Map the results into a ComponentListWsDTO which is an intermediate WsDTO
		final ComponentListWsDTO componentListWsDTO = new ComponentListWsDTO();
		final List<ComponentWsDTO> componentWsDTOList = getDataMapper().mapAsList(searchPageDataResult.getResults(),
				ComponentWsDTO.class, fields);
		componentListWsDTO.setComponent(componentWsDTOList);

		// Marshal the ComponentListWsDTO into ListAdaptedComponents
		final ComponentListWsDTOAdapter adapter = new ComponentListWsDTOAdapter();
		final ListAdaptedComponents listAdaptedComponent = adapter.marshal(componentListWsDTO);

		// Convert pagination and sorting data into ListAdaptedComponents' WsDTO
		final PaginationWsDTO paginationWsDTO = getWebPaginationUtils().buildPaginationWsDto(searchPageDataResult.getPagination());
		final List<SortWsDTO> sortWsDTOList = getWebPaginationUtils().buildSortWsDto(searchPageDataResult.getSorts());
		listAdaptedComponent.setPagination(paginationWsDTO);
		listAdaptedComponent.setSorts(sortWsDTOList);

		return listAdaptedComponent;
	}

	@GetMapping(value = "/components/ids")
	@ResponseBody
	@ApiOperation(value = "Get component data", notes = "Finds cms components by the specified IDs. When none is provided, this will retrieve all components"
			+ "\nThe components list will be filtered by the given catalog, product or category restrictions, as well as by the "
			+ "pagination information. The result will be sorted in the specified order.", nickname = "getComponentsByIds")
	@ApiBaseSiteIdParam
	public ComponentResultListDTO getComponentsByIds( //
			@ApiParam(value = "List of Component identifiers")
			@RequestParam(required = true)
			final List<String> componentIds, @ApiParam(value = "Catalog code")
			@RequestParam(required = false)
			final String catalogCode, @ApiParam(value = "Product code")
			@RequestParam(required = false)
			final String productCode, @ApiParam(value = "Category code")
			@RequestParam(required = false)
			final String categoryCode,
			@ApiParam(value = "Response configuration (list of fields, which should be returned in response)", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = "DEFAULT")
			final String fields) throws CMSItemNotFoundException
	{
		final List<ComponentResultDTO> componentList = new ArrayList();
		final ComponentResultListDTO componentsWsDTO = new ComponentResultListDTO();
		componentIds.stream().forEach(id -> {
			final ComponentResultDTO result = new ComponentResultDTO();
			final List<ErrorWsDTO> errorList = new ArrayList();
			try
			{
				final AbstractCMSComponentData componentData = getComponentItemFacade().getComponentById(id, categoryCode,
						productCode, catalogCode);
				final ComponentWsDTO data = (ComponentWsDTO) getDataMapper().map(componentData, fields);
				final ComponentWsDTOAdapter adapter = new ComponentWsDTOAdapter();
				result.setComponent(adapter.marshal(data));
				result.setStatus(ResultEnum.SUCCESS);
			}
			catch (final ValidationException e)
			{
				errorList.add(handleErrorInternal(ValidationException.class.getSimpleName(), e.getMessage()));
			}
			catch (final CMSItemNotFoundException e)
			{
				errorList.add(handleErrorInternal(CMSItemNotFoundException.class.getSimpleName(), e.getMessage()));
			}
			result.setErrors(errorList);
			result.setStatus(errorList == null || errorList.isEmpty() ? ResultEnum.SUCCESS : ResultEnum.ERROR);
			componentList.add(result);
		});

		componentsWsDTO.setComponents(componentList);
		return componentsWsDTO;
	}


	@GetMapping(value = "/components/{componentId}/categories")
	@CacheControl(directive = CacheControlDirective.PRIVATE, maxAge = 120)
	@Cacheable(value = "productCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(true,true,#componentId,#fields)")
	@ResponseBody
	@ApiOperation(value = "Get categories component data by id", notes = "Given a component identifier, return categories component data.", nickname = "getCategoriesComponentById")
	@ApiBaseSiteIdParam
	public CategoryListWsDTO getCategoriesComponentById(@ApiParam(value = "Component identifier", required = true)
	@PathVariable
	final String componentId,
			@ApiParam(value = "Response configuration (list of fields, which should be returned in response)", allowableValues = "BASIC, DEFAULT, FULL")
			@RequestParam(defaultValue = "DEFAULT")
			final String fields) throws CMSItemNotFoundException
	{
		try
		{
			LOGGER.debug("getCategoriesComponentById: componentId={} | options={}", sanitize(componentId), PRODUCT_OPTIONS_SET);

			final List<CategoryData> categories = getCategoriesComponentById(componentId);

			return getDataMapper().map(convertResultsetForCategories(categories), CategoryListWsDTO.class, fields);
			//			final ProductData product = productFacade.getProductForCodeAndOptions(productCode, PRODUCT_OPTIONS_SET);
		}
		catch (final ValidationException e)
		{
			LOGGER.info("Validation exception", e);
			throw new WebserviceValidationException(e.getValidationObject());
		}
	}

	protected CategoryDataList convertResultsetForCategories(final List<CategoryData> categories)
	{
		final CategoryDataList result = new CategoryDataList();
		if (!org.springframework.util.CollectionUtils.isEmpty(categories))
		{
			final int pageSize = categories.size();
			result.setCategories(categories);
			if (pageSize > 0)
			{
				result.setTotalPageCount(
						((categories.size() % pageSize) == 0) ? (categories.size() / pageSize) : ((categories.size() / pageSize) + 1));
			}
			result.setCurrentPage(0);
			result.setTotalProductCount(categories.size());
			result.setCatalog(null);
			result.setVersion(null);
		}

		return result;
	}

	private List<CategoryData> getCategoriesComponentById(final String componentId) throws CMSItemNotFoundException
	{
		final AbstractCMSComponentData componentData = getComponentItemFacade().getComponentById(componentId, null, null, null);

		final List<CategoryData> list = new LinkedList<>();
		if (componentData != null && "CategoryCarouselComponent".equalsIgnoreCase(componentData.getTypeCode())
				&& componentData.getOtherProperties() != null && componentData.getOtherProperties().get("categories") != null
				&& componentData.getOtherProperties().get("categories") instanceof List)
		{
			final List<String> codes = (List<String>) componentData.getOtherProperties().get("categories");
			for (final String code : codes)
			{
				try
				{
					final Optional<CategoryData> category = categoryFacade.getCategoryForCode(code);
					if (category.isPresent())
					{
						list.add(category.get());
					}
				}
				catch (final UnknownIdentifierException ex)
				{
					LOGGER.warn("Category code [" + code + "] not found");
				}
			}
		}

		return list;
	}

	/**
	 *
	 */
	private ErrorWsDTO handleErrorInternal(final String type, final String message)
	{
		final ErrorWsDTO error = new ErrorWsDTO();
		error.setType(type.replace("Exception", "Error"));
		error.setMessage(sanitize(message));
		return error;
	}

	public ComponentItemFacade getComponentItemFacade()
	{
		return componentItemFacade;
	}

	public void setComponentItemFacade(final ComponentItemFacade componentItemFacade)
	{
		this.componentItemFacade = componentItemFacade;
	}

	public CMSDataMapper getDataMapper()
	{
		return dataMapper;
	}

	public void setDataMapper(final CMSDataMapper dataMapper)
	{
		this.dataMapper = dataMapper;
	}

	public WebPaginationUtils getWebPaginationUtils()
	{
		return webPaginationUtils;
	}

	public void setWebPaginationUtils(final WebPaginationUtils webPaginationUtils)
	{
		this.webPaginationUtils = webPaginationUtils;
	}
}
