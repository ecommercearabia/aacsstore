/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscmscustomocc.constants;

/**
 * Global class for all aacscmscustomocc constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings(
{ "deprecation", "squid:CallToDeprecatedMethod" })
public final class AacscmscustomoccConstants extends GeneratedAacscmscustomoccConstants
{
	public static final String EXTENSIONNAME = "aacscmscustomocc";

	public static final String PAGE_LABEL_ID = "pageLabelOrId";
	public static final String PAGE_ID = "pageId";
	public static final String PAGE_TYPE = "pageType";
	public static final String CODE = "code";

	private AacscmscustomoccConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
