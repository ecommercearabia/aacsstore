/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.aacs.aacspaymentbackoffice.services;

/**
 * Hello World AacspaymentbackofficeService
 */
public class AacspaymentbackofficeService
{
	public String getHello()
	{
		return "Hello";
	}
}
