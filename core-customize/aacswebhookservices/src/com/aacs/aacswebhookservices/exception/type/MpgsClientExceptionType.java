/**
 *
 */
package com.aacs.aacswebhookservices.exception.type;

/**
 * @author husam.dababneh@erabia.com
 *
 */
public enum MpgsClientExceptionType
{
	DECRYPTION_ERROR, PROVIDER_NOT_FOUND, BAD_REQUEST;

}
