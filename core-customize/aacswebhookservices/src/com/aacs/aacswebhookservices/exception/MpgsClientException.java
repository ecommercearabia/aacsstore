/**
 *
 */
package com.aacs.aacswebhookservices.exception;

import com.aacs.aacswebhookservices.exception.type.MpgsClientExceptionType;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class MpgsClientException extends Exception
{
	private final Object data;

	private final MpgsClientExceptionType type;

	/**
	 * @param message
	 */
	public MpgsClientException(final MpgsClientExceptionType type, final String message, final Object data)
	{
		super(message);
		this.data = data;
		this.type = type;
	}

	/**
	 * @return the data
	 */
	public Object getData()
	{
		return data;
	}

	/**
	 * @return the type
	 */
	public MpgsClientExceptionType getType()
	{
		return type;
	}

}
