package com.aacs.aacswebhookservices.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;

import org.apache.log4j.Logger;

import com.aacs.aacswebhookservices.constants.AacswebhookservicesConstants;


public class AacswebhookservicesManager extends GeneratedAacswebhookservicesManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(AacswebhookservicesManager.class.getName());

	public static final AacswebhookservicesManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AacswebhookservicesManager) em.getExtension(AacswebhookservicesConstants.EXTENSIONNAME);
	}

}
