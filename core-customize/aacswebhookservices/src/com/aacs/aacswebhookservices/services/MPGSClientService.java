/**
 *
 */
package com.aacs.aacswebhookservices.services;

import com.aacs.aacswebhookservices.enums.MpgsPayBrands;
import com.aacs.aacswebhookservices.exception.MpgsClientException;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public interface MPGSClientService
{
	public void updatePayment(final String data, final String secret, final MpgsPayBrands brand) throws MpgsClientException;

}
