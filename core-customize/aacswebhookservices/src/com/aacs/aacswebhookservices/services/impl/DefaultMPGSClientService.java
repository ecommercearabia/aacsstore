/**
 *
 */
package com.aacs.aacswebhookservices.services.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aacs.aacspayment.context.PaymentContext;
import com.aacs.aacspayment.model.MpgsPaymentProviderModel;
import com.aacs.aacspayment.model.PaymentProviderModel;
import com.aacs.aacspayment.mpgs.beans.incoming.AuthenticatePayerResponseBean;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.MpgsOrderStatus;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.MpgsTransactionAuthenticaStatus;
import com.aacs.aacspayment.mpgs.beans.incoming.helperbeans.MpgsTransactionType;
import com.aacs.aacspayment.service.PaymentProviderService;
import com.aacs.aacswebhookservices.enums.MpgsPayBrands;
import com.aacs.aacswebhookservices.exception.MpgsClientException;
import com.aacs.aacswebhookservices.exception.type.MpgsClientExceptionType;
import com.aacs.aacswebhookservices.services.MPGSClientService;
import com.aacs.core.model.MerchantTransactionModel;
import com.aacs.core.service.CustomCartService;
import com.aacs.core.service.CustomOrderService;
import com.aacs.core.service.MerchantTransactionService;
import com.aacs.facades.facade.CustomAcceleratorCheckoutFacade;
import com.google.common.base.Preconditions;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;



/**
 * @author husam.dababneh@erabia.com
 *
 */
public class DefaultMPGSClientService implements MPGSClientService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultMPGSClientService.class);
	private static final Gson GSON = new GsonBuilder().create();
	private static final String BASE_STORE_CODE = "alain";


	@Resource(name = "paymentProviderService")
	private PaymentProviderService paymentProviderService;

	@Resource(name = "paymentContext")
	private PaymentContext paymentContext;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "customCartService")
	private CustomCartService customCartService;

	@Resource(name = "acceleratorCheckoutFacade")
	private CustomAcceleratorCheckoutFacade customAcceleratorCheckoutFacade;

	@Resource(name = "customOrderService")
	private CustomOrderService customOrderService;

	@Resource(name = "merchantTransactionService")
	private MerchantTransactionService merchantTransactionService;

	@Override
	public synchronized void updatePayment(final String data, final String secret, final MpgsPayBrands brand)
			throws MpgsClientException
	{
		Preconditions.checkArgument(Strings.isNotBlank(data), "data is empty");
		Preconditions.checkArgument(Strings.isNotBlank(secret), "secret is empty");
		Preconditions.checkNotNull(Strings.isNotBlank(secret), "Brand is null");

		final MpgsPaymentProviderModel paymentProvider = getProviderByBrand(brand);
		validateProvider(paymentProvider, brand);

		if (!paymentProvider.getNotificationSecret().equals(secret))
		{
			throw new IllegalArgumentException("Payment Provider Notification secret and webhook secret does not match");
		}


		final AuthenticatePayerResponseBean response = GSON.fromJson(data, AuthenticatePayerResponseBean.class);

		final String merchantTransactionId = response.getOrder().getId();
		final String orderCode = merchantTransactionId.split("_")[0];

		if (Strings.isBlank(merchantTransactionId))
		{
			throw new MpgsClientException(MpgsClientExceptionType.BAD_REQUEST, "Merchant Transaction ID(order id) is blank", data);
		}

		final MpgsTransactionType type = response.getTransaction().getType();
		final MpgsTransactionAuthenticaStatus transactionStatus = response.getTransaction().getAuthenticationStatus();

		if (MpgsTransactionType.AUTHENTICATION.equals(type)
				&& MpgsTransactionAuthenticaStatus.isFailedAuthentication(transactionStatus))
		{

			LOG.warn("Payment for Order[{}] authentication failed with status:[{}] and message:[{}]", orderCode,
					transactionStatus.getCode(), transactionStatus.getDesc());
			return;
		}

		if (!MpgsTransactionType.PAYMENT.equals(type) && !MpgsTransactionType.AUTHORIZATION.equals(type))
		{
			LOG.info("Recived Webhook transaction notification for Order:[{}], type:[{}] and status[{}], nothing to do", orderCode,
					type.getCode(), transactionStatus == null ? "null" : transactionStatus.getCode());
			return;
		}


		final MpgsOrderStatus orderStatus = response.getOrder().getStatus();
		LOG.info("Order[{}] Status Is [{}]", orderCode, orderStatus.getCode());
		if (!MpgsOrderStatus.isValueOneOf(orderStatus, MpgsOrderStatus.CAPTURED, MpgsOrderStatus.AUTHORIZED))
		{
			LOG.info("Since the OrderStatus of the [{}] transaction is [{}] skipping the notification", orderCode, orderStatus);
			return;
		}

		final MerchantTransactionModel merchantTransaction = trySaveMerchantTransaction(merchantTransactionId, orderCode);
		if (merchantTransaction == null)
		{
			LOG.info("Order[{}] is being proccessed by StoreFront", orderCode);
			return;
		}

		final List<CartModel> cartByMerchantTransactionId = getCustomCartService()
				.getCartByMerchantTransactionId(merchantTransaction.getId());

		if (CollectionUtils.isEmpty(cartByMerchantTransactionId))
		{
			LOG.info("Cannot Find cart with the orderCode[{}]", orderCode);
			// Check if cart already changed into order
			checkIfOrderIsPlace(orderCode);
			return;
		}
		if (cartByMerchantTransactionId.size() > 1)
		{
			LOG.warn("Found Multiple Carts[{}] with the same merchantTransactionId[{}], taking the first one",
					cartByMerchantTransactionId.stream().map(AbstractOrderModel::getCode).collect(Collectors.toList()),
					merchantTransactionId);
		}

		final CartModel cartModel = cartByMerchantTransactionId.get(0);
		try
		{
			final Type empMapType = new TypeToken<Map<String, Object>>()
			{
			}.getType();

			final Map<String, Object> fromJson = GSON.fromJson(data, empMapType);
			cartModel.setOrderPlacedByChannel("WEBHOOK");
			getModelService().save(cartModel);
			getCustomAcceleratorCheckoutFacade().completeCardOrderModelUsingWebhook(cartModel, fromJson);
		}
		catch (final InvalidCartException e)
		{
			LOG.error("{}", e.getStackTrace());
			throw new MpgsClientException(MpgsClientExceptionType.BAD_REQUEST, e.getMessage(), data);
		}


	}

	private void checkIfOrderIsPlace(final String orderCode)
	{
		final OrderModel orderForCode = getCustomOrderService().getOrderForCode(orderCode);
		if (orderForCode == null)
		{
			LOG.error("Nor the cart nor the order can be found ");
			return;
		}

		LOG.info("Order with code {} is placed", orderCode);
	}

	/**
	 * @param paymentProvider
	 */
	private void validateProvider(final MpgsPaymentProviderModel paymentProvider, final MpgsPayBrands brand)
	{
		Preconditions.checkNotNull(paymentProvider, "Payment Provider is null for brand [" + brand.getName() + "]");
		Preconditions.checkArgument(Strings.isNotBlank(paymentProvider.getNotificationSecret()),
				"Notification Secret on provider is empty");

	}




	/**
	 * @param brand
	 * @return
	 * @throws HyperpayClientException
	 */
	private MpgsPaymentProviderModel getProviderByBrand(final MpgsPayBrands brand) throws MpgsClientException
	{
		switch (brand)
		{
			case APPLEPAY:
				return null;
			case CC:
				return getPaymentProvider(MpgsPaymentProviderModel.class);
			case MADA:
				return null;
			default:
				break;
		}
		return null;
	}

	private MerchantTransactionModel getMerchantTransactionIfExistById(final String merchantTransactionId)
	{
		return getMerchantTransactionService().getMerchantTransactionById(merchantTransactionId);
	}

	private MerchantTransactionModel trySaveMerchantTransaction(final String merchantTransactionId, final String orderCode)
	{
		final MerchantTransactionModel merchantTransactionIfExist = getMerchantTransactionIfExistById(orderCode);
		if (merchantTransactionIfExist != null && (merchantTransactionIfExist.getVersion() == null
				|| !merchantTransactionIfExist.getVersion().equalsIgnoreCase(merchantTransactionId)))
		{
			getModelService().remove(merchantTransactionIfExist);
		}

		try
		{
			final MerchantTransactionModel merchantTrasnaction = getModelService().create(MerchantTransactionModel.class);
			merchantTrasnaction.setId(orderCode);
			merchantTrasnaction.setSource("WEBHOOK");
			merchantTrasnaction.setVersion(merchantTransactionId);
			getModelService().save(merchantTrasnaction);
			return merchantTrasnaction;
		}
		catch (final Exception e)
		{
			LOG.info(e.toString());
			LOG.warn("MerchantTransactionModel already exist for {}", merchantTransactionId);
		}
		return null;
	}

	private <T> T getPaymentProvider(final Class<T> clazz) throws MpgsClientException
	{
		final Optional<PaymentProviderModel> paymentProvider = getPaymentProviderService().getActive(BASE_STORE_CODE, clazz);

		if (paymentProvider.isEmpty())
		{
			throw new MpgsClientException(MpgsClientExceptionType.PROVIDER_NOT_FOUND, "provider not found", null);
		}
		return (T) paymentProvider.get();
	}

	/**
	 * @return the paymentProviderService
	 */
	protected PaymentProviderService getPaymentProviderService()
	{
		return paymentProviderService;
	}



	/**
	 * @return the paymentContext
	 */
	public PaymentContext getPaymentContext()
	{
		return paymentContext;
	}


	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}



	/**
	 * @return the customCartService
	 */
	public CustomCartService getCustomCartService()
	{
		return customCartService;
	}


	/**
	 * @return the customAcceleratorCheckoutFacade
	 */
	public CustomAcceleratorCheckoutFacade getCustomAcceleratorCheckoutFacade()
	{
		return customAcceleratorCheckoutFacade;
	}


	/**
	 * @return the customOrderService
	 */
	public CustomOrderService getCustomOrderService()
	{
		return customOrderService;
	}

	/**
	 * @return the merchantTransactionService
	 */
	public MerchantTransactionService getMerchantTransactionService()
	{
		return merchantTransactionService;
	}


}
