
package com.aacs.aacswebhookservices.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Card
{

	@SerializedName("bin")
	@Expose
	private String bin;
	@SerializedName("last4Digits")
	@Expose
	private String last4Digits;
	@SerializedName("holder")
	@Expose
	private String holder;
	@SerializedName("expiryMonth")
	@Expose
	private String expiryMonth;
	@SerializedName("expiryYear")
	@Expose
	private String expiryYear;

	public String getBin()
	{
		return bin;
	}

	public void setBin(final String bin)
	{
		this.bin = bin;
	}

	public String getLast4Digits()
	{
		return last4Digits;
	}

	public void setLast4Digits(final String last4Digits)
	{
		this.last4Digits = last4Digits;
	}

	public String getHolder()
	{
		return holder;
	}

	public void setHolder(final String holder)
	{
		this.holder = holder;
	}

	public String getExpiryMonth()
	{
		return expiryMonth;
	}

	public void setExpiryMonth(final String expiryMonth)
	{
		this.expiryMonth = expiryMonth;
	}

	public String getExpiryYear()
	{
		return expiryYear;
	}

	public void setExpiryYear(final String expiryYear)
	{
		this.expiryYear = expiryYear;
	}

}
