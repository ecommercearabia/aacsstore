/**
 *
 */
package com.aacs.aacswebhookservices.enums;

/**
 * @author core
 *
 */
public enum MpgsPayBrands
{
	APPLEPAY("ApplePay"), CC("Credit Card"), MADA("Mada");

	private final String name;

	/**
	 *
	 */
	private MpgsPayBrands(final String name)
	{
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}


}
