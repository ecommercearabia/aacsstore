/**
 *
 */
package com.aacs.aacswebhookservices.controllers;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.aacs.aacswebhookservices.enums.MpgsPayBrands;
import com.aacs.aacswebhookservices.exception.MpgsClientException;
import com.aacs.aacswebhookservices.services.MPGSClientService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @author mbaker
 *
 */
@Controller
@RequestMapping(value = "/")
@Api(tags = "Mpgs Payment")
public class MpgsController
{

	private static final Logger LOG = LoggerFactory.getLogger(MpgsController.class);
	private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	private static final String MPGS_SECRET_HEADER = "x-notification-secret";


	@Resource(name = "mpgsClientService")
	private MPGSClientService mpgsClientService;

	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value = "/notification/cc", method =
	{ RequestMethod.POST, RequestMethod.GET })
	@ApiOperation(value = "Payment Notification", notes = "Payment Notification", produces = "application/json", consumes = "text/plain")
	public ResponseEntity<String> notification(@RequestBody(required = false)
	final String body, final HttpServletRequest httpServletRequest)
	{
		logRequestInfo(body, httpServletRequest);
		final String notificationSecret = httpServletRequest.getHeader(MPGS_SECRET_HEADER);
		return callUpdatePayment(body, notificationSecret, MpgsPayBrands.CC);
	}






	private ResponseEntity<String> callUpdatePayment(final String body, final String sceret, final MpgsPayBrands brand)
	{
		try
		{
			getMpgsClientService().updatePayment(body, sceret, brand);
		}
		catch (final IllegalArgumentException e)
		{
			LOG.error(e.getMessage());
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		catch (final MpgsClientException e)
		{
			LOG.error(e.getMessage());
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	private void logRequestInfo(final String body, final HttpServletRequest httpServletRequest)
	{
		final String parameterMap = GSON.toJson(httpServletRequest.getParameterMap());
		LOG.info("Request Param Map = {}", parameterMap);
		logHeaders(httpServletRequest);
		LOG.info("body = {}", body);
	}

	private void logHeaders(final HttpServletRequest httpServletRequest)
	{
		final Enumeration<String> headerNames = httpServletRequest.getHeaderNames();
		final Map<String, String> map = new HashMap<>();
		while (headerNames.hasMoreElements())
		{
			final String key = headerNames.nextElement();
			final String value = httpServletRequest.getHeader(key);
			map.put(key, value);
		}

		final String json = GSON.toJson(map);
		LOG.info("Headers: {}", json);
	}


	/**
	 * @return the mpgsClientService
	 */
	public MPGSClientService getMpgsClientService()
	{
		return mpgsClientService;
	}


}
