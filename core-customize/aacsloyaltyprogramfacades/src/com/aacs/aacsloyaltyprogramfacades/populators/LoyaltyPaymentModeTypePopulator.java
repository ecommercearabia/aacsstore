/**
 *
 */
package com.aacs.aacsloyaltyprogramfacades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.type.TypeService;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.aacs.aacsloyaltyprogramfacades.data.LoyaltyPaymentModeTypeData;
import com.aacs.aacsloyaltyprogramprovider.enums.LoyaltyPaymentModeType;



/**
 * @author amjad.shati@erabia.com
 *
 */
public class LoyaltyPaymentModeTypePopulator implements Populator<LoyaltyPaymentModeType, LoyaltyPaymentModeTypeData>
{
	@Resource(name = "typeService")
	private TypeService typeService;

	@Override
	public void populate(final LoyaltyPaymentModeType source, final LoyaltyPaymentModeTypeData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setCode(source.getCode());
		target.setName(typeService.getEnumerationValue(source).getName());
	}
}

