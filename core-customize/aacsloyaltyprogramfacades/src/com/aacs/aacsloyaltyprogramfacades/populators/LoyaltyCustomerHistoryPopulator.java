/**
 *
 */
package com.aacs.aacsloyaltyprogramfacades.populators;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.math.BigDecimal;
import java.util.Objects;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.aacs.aacsloyaltyprogramfacades.data.LoyaltyCustomerHistoryData;
import com.aacs.aacsloyaltyprogramprovider.beans.CustomerHistory;



/**
 * @author amjad.shati@erabia.com
 *
 */
public class LoyaltyCustomerHistoryPopulator implements Populator<CustomerHistory, LoyaltyCustomerHistoryData>
{
	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Override
	public void populate(final CustomerHistory source, final LoyaltyCustomerHistoryData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setCreated(source.getCreated());
		target.setExpireDate(source.getExpireDate());
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		if (!Objects.isNull(currentBaseStore))
		{
			target.setFinalAmount(getPriceData(BigDecimal.valueOf(source.getFinalAmount()), PriceDataType.BUY,
					currentBaseStore.getDefaultCurrency()));
			target.setInitialAmount(getPriceData(BigDecimal.valueOf(source.getInitialAmount()), PriceDataType.BUY,
					currentBaseStore.getDefaultCurrency()));
		}

		target.setPoints(source.getPoints());
		if (source.getMetaData() != null && source.getMetaData().getTransaction() != null)
		{
			target.setOrderNumber(source.getMetaData().getTransaction().getId());
		}
		target.setType(source.getType());
	}

	private PriceData getPriceData(final BigDecimal value, final PriceDataType priceType, final CurrencyModel currency)
	{
		if (currency == null)
		{
			return null;
		}
		return priceDataFactory.create(priceType, value, currency);
	}

}
