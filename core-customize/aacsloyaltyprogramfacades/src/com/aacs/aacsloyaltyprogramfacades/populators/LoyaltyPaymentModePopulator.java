/**
 *
 */
package com.aacs.aacsloyaltyprogramfacades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Objects;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.aacs.aacsloyaltyprogramfacades.data.LoyaltyPaymentModeData;
import com.aacs.aacsloyaltyprogramfacades.data.LoyaltyPaymentModeTypeData;
import com.aacs.aacsloyaltyprogramprovider.enums.LoyaltyPaymentModeType;
import com.aacs.aacsloyaltyprogramprovider.model.LoyaltyPaymentModeModel;




/**
 * @author amjad.shati@erabia.com
 *
 */
public class LoyaltyPaymentModePopulator implements Populator<LoyaltyPaymentModeModel, LoyaltyPaymentModeData>
{
	@Resource(name = "loyaltyPaymentModeTypeConverter")
	private Converter<LoyaltyPaymentModeType, LoyaltyPaymentModeTypeData> loyaltyPaymentModeTypeConverter;

	@Override
	public void populate(final LoyaltyPaymentModeModel source, final LoyaltyPaymentModeData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setDescription(source.getDescription());
		if (!Objects.isNull(source.getLoyaltyPaymentModeType()))
		{
			target.setLoyaltyPaymentModeType(loyaltyPaymentModeTypeConverter.convert(source.getLoyaltyPaymentModeType()));
		}
		target.setName(source.getDisplayName());
	}
}

