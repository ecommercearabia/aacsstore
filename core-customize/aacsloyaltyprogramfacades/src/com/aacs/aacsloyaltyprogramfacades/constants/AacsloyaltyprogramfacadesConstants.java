/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramfacades.constants;

/**
 * Global class for all Aacsloyaltyprogramfacades constants. You can add global constants for your extension into this class.
 */
public final class AacsloyaltyprogramfacadesConstants extends GeneratedAacsloyaltyprogramfacadesConstants
{
	public static final String EXTENSIONNAME = "aacsloyaltyprogramfacades";

	private AacsloyaltyprogramfacadesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "aacsloyaltyprogramfacadesPlatformLogo";
}
