/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramfacades.facades;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.aacs.aacsloyaltyprogramfacades.data.LoyaltyBalanceData;
import com.aacs.aacsloyaltyprogramfacades.data.LoyaltyCustomerInfoData;
import com.aacs.aacsloyaltyprogramfacades.data.LoyaltyCustomerQrData;
import com.aacs.aacsloyaltyprogramfacades.data.LoyaltyPaginationData;
import com.aacs.aacsloyaltyprogramfacades.data.LoyaltyUsablePointData;
import com.aacs.aacsloyaltyprogramprovider.exception.AACSLoyaltyException;


/**
 *
 */
public interface LoyaltyPaymentFacade
{
	public Optional<LoyaltyUsablePointData> getLoyaltyUsablePoints(CartModel cart) throws AACSLoyaltyException;

	public Optional<LoyaltyUsablePointData> getLoyaltyUsablePointsByCurrentCart() throws AACSLoyaltyException;

	public Optional<LoyaltyBalanceData> getBalance(final CustomerModel customerModel, final BaseStoreModel baseStoreModel)
			throws AACSLoyaltyException;

	public Optional<LoyaltyBalanceData> getBalanceByCurrentBaseStore(final CustomerModel customerModel)
			throws AACSLoyaltyException;

	public Optional<LoyaltyBalanceData> getBalanceByCurrentBaseStoreAndCustomer() throws AACSLoyaltyException;

	public Optional<LoyaltyCustomerInfoData> getLoyaltyCustomer(CustomerModel customer, BaseStoreModel baseStoreModel,
			LoyaltyPaginationData paginationData) throws AACSLoyaltyException;

	public Optional<LoyaltyCustomerInfoData> getLoyaltyCustomerByCurrentBaseStore(CustomerModel customer,
			LoyaltyPaginationData paginationData) throws AACSLoyaltyException;

	public Optional<LoyaltyCustomerInfoData> getLoyaltyCustomerByCurrentBaseStoreAndCurrentCustomer(
			LoyaltyPaginationData paginationData) throws AACSLoyaltyException;

	public Optional<LoyaltyCustomerQrData> getLoyaltyCustomerQrCode(CustomerModel customer, BaseStoreModel baseStoreModel)
			throws AACSLoyaltyException;

	public Optional<LoyaltyCustomerQrData> getLoyaltyCustomerQrCodeByCurrentBaseStore(CustomerModel customer)
			throws AACSLoyaltyException;

	public Optional<LoyaltyCustomerQrData> getLoyaltyCustomerQrCodeByCurrentBaseStoreAndCustomer() throws AACSLoyaltyException;

	public boolean isEnabledForCustomer(CustomerModel customer, BaseStoreModel baseStoreModel) throws AACSLoyaltyException;

	public boolean isEnabledForCustomerByCurrentBaseStore(CustomerModel customer) throws AACSLoyaltyException;

	public boolean isEnabledForCustomerByCurrentBaseStoreAndCurrentCustomer() throws AACSLoyaltyException;

	public boolean isEnabledOnStore(BaseStoreModel baseStoreModel);

	public boolean isEnabledOnStoreByCurrentBaseStore();





}
