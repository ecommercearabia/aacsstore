/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacswebserviceapi.constants;

/**
 * Global class for all Aacswebserviceapi constants. You can add global constants for your extension into this class.
 */
public final class AacswebserviceapiConstants extends GeneratedAacswebserviceapiConstants
{
	public static final String EXTENSIONNAME = "aacswebserviceapi";

	private AacswebserviceapiConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
