/**
 *
 */
package com.aacs.aacsexport.services;

import de.hybris.platform.impex.model.ImpExMediaModel;

import com.aacs.aacsexport.bean.EmailBean;



/**
 * @author jaafarNaddaf
 *
 */
public interface ErabiaExportService
{
	/**
	 *
	 * @param code
	 * @param data
	 * @param folderName
	 * @return ImpExMediaModel
	 */
	public ImpExMediaModel impExToMedia(String code, StringBuilder data, String folderName);

	/**
	 *
	 * @param mail
	 * @return boolean
	 */
	public boolean sendMail(EmailBean mail);

	/**
	 *
	 * @param catalogId
	 */
	public void synchronize(final String catalogId);
}
