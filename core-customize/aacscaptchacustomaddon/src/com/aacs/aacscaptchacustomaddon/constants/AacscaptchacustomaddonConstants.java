/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscaptchacustomaddon.constants;

/**
 * Global class for all Aacscaptchacustomaddon constants. You can add global constants for your extension into this class.
 */
public final class AacscaptchacustomaddonConstants extends GeneratedAacscaptchacustomaddonConstants
{
	public static final String EXTENSIONNAME = "aacscaptchacustomaddon";

	private AacscaptchacustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
