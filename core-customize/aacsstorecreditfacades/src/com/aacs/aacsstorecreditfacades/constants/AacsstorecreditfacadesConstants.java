/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsstorecreditfacades.constants;

/**
 * Global class for all Aacsstorecreditfacades constants. You can add global constants for your extension into this class.
 */
public final class AacsstorecreditfacadesConstants extends GeneratedAacsstorecreditfacadesConstants
{
	public static final String EXTENSIONNAME = "aacsstorecreditfacades";

	private AacsstorecreditfacadesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "aacsstorecreditfacadesPlatformLogo";
}
