/**
 *
 */
package com.aacs.aacsstorecreditfacades.facade.impl;

import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.aacs.aacsstorecredit.enums.StoreCreditModeType;
import com.aacs.aacsstorecredit.model.StoreCreditModeModel;
import com.aacs.aacsstorecredit.service.StoreCreditModeService;
import com.aacs.aacsstorecreditfacades.data.StoreCreditModeData;
import com.aacs.aacsstorecreditfacades.facade.StoreCreditModeFacade;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultStoreCreditModeFacade implements StoreCreditModeFacade
{
	@Resource(name = "storeCreditModeService")
	private StoreCreditModeService storeCreditModeService;

	@Resource(name = "storeCreditModeConverter")
	private Converter<StoreCreditModeModel, StoreCreditModeData> storeCreditModeConverter;

	@Override
	public Optional<StoreCreditModeData> getStoreCreditMode(final String storeCreditModeTypeCode)
	{
		if (storeCreditModeTypeCode == null)
		{
			throw new IllegalArgumentException("storeCreditModeTypeCode cannot must be null");
		}
		final StoreCreditModeType storeCreditModeType = StoreCreditModeType.valueOf(storeCreditModeTypeCode);
		final StoreCreditModeModel storeCreditMode = storeCreditModeService.getStoreCreditMode(storeCreditModeType);
		if (storeCreditMode == null)
		{
			return Optional.empty();
		}

		return Optional.ofNullable(storeCreditModeConverter.convert(storeCreditMode));
	}


	@Override
	public Optional<List<StoreCreditModeData>> getSupportedStoreCreditModesCurrentBaseStore()
	{

		final List<StoreCreditModeModel> storeCreditModeList = storeCreditModeService
				.getSupportedStoreCreditModesCurrentBaseStore();
		if (storeCreditModeList == null)
		{
			return Optional.empty();
		}

		//		final Iterator<StoreCreditModeModel> itr = storeCreditModeList.iterator();
		//		while (itr.hasNext())
		//		{
		//			final StoreCreditModeModel storeCreditModeModel = itr.next();
		//			if (storeCreditModeModel.getActive() == null || Boolean.FALSE.equals(storeCreditModeModel.getActive()))
		//			{
		//				itr.remove();
		//			}
		//		}
		final List<StoreCreditModeModel> collect = storeCreditModeList.stream()
				.filter(creditMode -> creditMode != null && creditMode.getActive())
				.sorted(Comparator.comparingInt(StoreCreditModeModel::getPriority)).collect(Collectors.toList());

		return Optional.ofNullable(storeCreditModeConverter.convertAll(collect));
	}


	@Override
	public boolean isStoreCreditModeSupportedByCurrentBaseStore(final String StoreCreditTypeCode)
	{
		final Optional<List<StoreCreditModeData>> supportedStoreCreditModes = getSupportedStoreCreditModesCurrentBaseStore();

		if (!supportedStoreCreditModes.isPresent())
		{
			return false;
		}
		final List<String> storeCreditModeCodes = new ArrayList();
		for (final StoreCreditModeData storeCreditMode : supportedStoreCreditModes.get())
		{
			storeCreditModeCodes.add(storeCreditMode.getStoreCreditModeType().getCode());
		}
		return storeCreditModeCodes.contains(StoreCreditTypeCode);
	}


}
