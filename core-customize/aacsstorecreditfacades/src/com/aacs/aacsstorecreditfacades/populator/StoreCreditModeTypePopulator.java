/**
 *
 */
package com.aacs.aacsstorecreditfacades.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.type.TypeService;

import javax.annotation.Resource;

import com.aacs.aacsstorecredit.enums.StoreCreditModeType;
import com.aacs.aacsstorecreditfacades.data.StoreCreditModeTypeData;



/**
 * @author amjad.shati@erabia.com
 *
 */
public class StoreCreditModeTypePopulator implements Populator<StoreCreditModeType, StoreCreditModeTypeData>
{
	@Resource(name = "typeService")
	private TypeService typeService;

	@Override
	public void populate(final StoreCreditModeType source, final StoreCreditModeTypeData target)
	{
		target.setCode(source.getCode());
		target.setName(typeService.getEnumerationValue(source).getName());
	}
}
