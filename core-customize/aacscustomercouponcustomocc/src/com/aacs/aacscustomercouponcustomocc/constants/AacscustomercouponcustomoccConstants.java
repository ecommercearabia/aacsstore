/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscustomercouponcustomocc.constants;

/**
 * Aacscustomercouponcustomocc constants
 */
@SuppressWarnings(
{ "deprecation", "squid:CallToDeprecatedMethod" })
public final class AacscustomercouponcustomoccConstants extends GeneratedAacscustomercouponcustomoccConstants
{
	public static final String EXTENSIONNAME = "aacscustomercouponcustomocc"; //NOSONAR

	private AacscustomercouponcustomoccConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
