/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscustomercouponcustomocc.jalo;

import com.aacs.aacscustomercouponcustomocc.constants.AacscustomercouponcustomoccConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class AacscustomercouponcustomoccManager extends GeneratedAacscustomercouponcustomoccManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( AacscustomercouponcustomoccManager.class.getName() );
	
	public static final AacscustomercouponcustomoccManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AacscustomercouponcustomoccManager) em.getExtension(AacscustomercouponcustomoccConstants.EXTENSIONNAME);
	}
	
}
