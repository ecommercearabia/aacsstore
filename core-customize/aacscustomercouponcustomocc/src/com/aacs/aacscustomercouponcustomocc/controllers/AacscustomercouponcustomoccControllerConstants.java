/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscustomercouponcustomocc.controllers;

/**
 */
public interface AacscustomercouponcustomoccControllerConstants
{
	// implement here controller constants used by this extension
}
