/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsconsignmenttrackingcustomaddon.constants;

/**
 * Global class for all Aacsconsignmenttrackingcustomaddon constants. You can add global constants for your extension into this class.
 */
public final class AacsconsignmenttrackingcustomaddonConstants extends GeneratedAacsconsignmenttrackingcustomaddonConstants
{
	public static final String EXTENSIONNAME = "aacsconsignmenttrackingcustomaddon";

	private AacsconsignmenttrackingcustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
