/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsconsignmenttrackingcustomaddon.jalo;

import com.aacs.aacsconsignmenttrackingcustomaddon.constants.AacsconsignmenttrackingcustomaddonConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class AacsconsignmenttrackingcustomaddonManager extends GeneratedAacsconsignmenttrackingcustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( AacsconsignmenttrackingcustomaddonManager.class.getName() );
	
	public static final AacsconsignmenttrackingcustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AacsconsignmenttrackingcustomaddonManager) em.getExtension(AacsconsignmenttrackingcustomaddonConstants.EXTENSIONNAME);
	}
	
}
