/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.wishlistfacade.constants;

/**
 * Global class for all Aacswishlistfacadewishlistfacade constants. You can add global constants for your extension into this class.
 */
public final class AacswishlistfacadewishlistfacadeConstants extends GeneratedAacswishlistfacadewishlistfacadeConstants
{
	public static final String EXTENSIONNAME = "aacswishlistfacade";

	private AacswishlistfacadewishlistfacadeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "aacswishlistfacadePlatformLogo";
}
