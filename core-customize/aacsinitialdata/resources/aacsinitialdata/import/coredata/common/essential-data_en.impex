# -----------------------------------------------------------------------
# Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
# -----------------------------------------------------------------------
#
# Import essential data for the Accelerator
#

# Language
$lang=en

# Languages
UPDATE Language;isocode[unique=true];name[lang=$lang]
 ;en;"English"
 ;ar;"Arabic"
# Currencies
UPDATE Currency;isocode[unique=true];name[lang=$lang]
 ;AED ;"United Arab Emirates dirham"
 ;USD;"US Dollar"


# Titles
UPDATE Title;code[unique=true];name[lang=$lang]
 ;miss;"Miss"
 ;mr;"Mr."
 ;mrs;"Mrs."
 ;ms;"Ms."

# Credit / Debit Cards
UPDATE CreditCardType;code[unique=true];name[lang=$lang]
 ;amex;"American Express"
 ;diners;"Diner's Club"
 ;maestro;"Maestro"
 ;master;"Mastercard"
 ;mastercard_eurocard;"Mastercard/Eurocard"
 ;switch;"Switch"
 ;visa;"Visa"

# DistanceUnits for Storelocator 
UPDATE DistanceUnit;code[unique=true];name[lang=$lang]
 ;km;"km"
 ;miles;"miles"

# MediaFormats
UPDATE MediaFormat;qualifier[unique=true];name[lang=$lang];
 ;1200Wx1200H;"1200Wx1200H"
 ;300Wx300H;"300Wx300H"
 ;30Wx30H;"30Wx30H"
 ;365Wx246H;"365Wx246H"
 ;515Wx515H;"515Wx515H"
 ;65Wx65H;"65Wx65H"
 ;96Wx96H;"96Wx96H"


# User Tax Groups
UPDATE UserTaxGroup;code[unique=true];name[lang=$lang]
 ;alain-taxes;"ALAIN Taxes"

# Product Tax Groups
UPDATE ProductTaxGroup;code[unique=true];name[lang=$lang]
 ;alain-vat-full;"ALAIN Full Tax rate"

# Tax
UPDATE Tax;code[unique=true];name[lang=$lang]
 ;alain-vat-full;"ALAIN VAT"


#####Unit
UPDATE Unit;unitType[unique=true];code[unique=true];name[lang=$lang]
;BAG;BAG;"Bag"
;CT1;CT1;"Carton 1"
;CT2;CT2;"Carton 2"
;CT3;CT3;"Carton 3"
;CT4;CT4;"Carton 4"
;CT5;CT5;"Carton 5"
;CT6;CT6;"Carton 6"
;CT7;CT7;"Carton 7"
;DR;DR;"Drum"
;DZ;DZ;"Dozen"
;EA;EA;"each"
;G;G;"Gram"
;GAL;GAL;"US Gallon"
;HP0;HP0;"Higher Pack 0"
;HP2;HP2;"Higher Pack 2"
;HP3;HP3;"Higher Pack 3"
;HP4;HP4;"Higher Pack 4"
;HP5;HP5;"Higher Pack 5"
;HP6;HP6;"Higher Pack 6"
;HP7;HP7;"Higher Pack 7"
;HP8;HP8;"Higher Pack 8"
;HP9;HP9;"Higher Pack 9"
;CAN;CAN;"Canister"
;CAR;CAR;"Carton"
;KG;KG;"KG"
;L;L;"Liter"
;LB;LB;"Pound"
;LP0;LP0;"Lower Pack 0"
;LP1;LP1;"Lower Pack 1"
;LP2;LP2;"Lower Pack 2"
;LP3;LP3;"Lower Pack 3"
;LP4;LP4;"Lower Pack 4"
;LP5;LP5;"Lower Pack 5"
;LP6;LP6;"Lower Pack 6"
;LP7;LP7;"Lower Pack 7"
;LP8;LP8;"Lower Pack 8"
;LP9;LP9;"Lower Pack 9"
;M;M;"Meter"
;ML;ML;"Milliliter"
;MON;MON;"Months"
;OT1;OT1;"Outer1"
;OT2;OT2;"Outer 2"
;OT3;OT3;"Outer 3"
;OUT;OUT;"Outer"
;OZ;OZ;"Ounce"
;PAA;PAA;"Pair"
;PAC;PAC;"Pack"
;ROL;ROL;"Roll"
;SO1;SO1;"Special Offer 1"
;SO2;SO2;"Special Offer 2"
;SO3;SO3;"Special Offer 3"
;PC;PC;"Piece"
;TON;TON;"US Ton"
;YD;YD;"Yards"



Localized
# Tax
UPDATE Tax;code[unique=true];name[lang=en]
 ; S5  ; S5
 ; S0  ; S0 

 # User Tax Group
UPDATE UserTaxGroup;code[unique=true];name[lang=en]
 ; alain-taxes  ; Taxes 5%
 ; alain-taxes-zero   ; Taxes 0% 
 
   # Product Tax Group
UPDATE ProductTaxGroup;code[unique=true];name[lang=en]
 ; alain-vat-full  ; Tax rate 5%
 ; S0  ; Tax rate 0% 

INSERT_UPDATE UnitFactorRange;code[unique=true];startUnitValue;unitValueStep;endUnitValue
;5KGram_250gScale_Range;250;250;5000
;5KGram_500gScale_Range;500;500;5000
;5KGram_100gScale_Range;100;100;1000         
         

# Consginment statuses
UPDATE ConsignmentStatus;code[unique=true];name[lang=$lang]
 ;CANCELLED;"Cancelled"
 ;PICKPACK;"Pickpack"
 ;PICKUP_COMPLETE;"Pickup complete"
 ;READY;"Ready"
 ;READY_FOR_PICKUP;"Ready for pickup"
 ;SHIPPED;"Shipped"
 ;WAITING;"Waiting"
