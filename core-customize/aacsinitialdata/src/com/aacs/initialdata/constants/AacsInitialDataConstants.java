/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.initialdata.constants;

/**
 * Global class for all AacsInitialData constants.
 */
public final class AacsInitialDataConstants extends GeneratedAacsInitialDataConstants
{
	public static final String EXTENSIONNAME = "aacsinitialdata";

	private AacsInitialDataConstants()
	{
		//empty
	}
}
