/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.dao;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.aacs.aacsscpiservices.model.SCPIServiceProviderModel;


/**
 *
 */
public interface SCPIProviderDao
{
	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	public Optional<SCPIServiceProviderModel> get(String code);

	/**
	 * Gets the active.
	 *
	 * @param baseStoreUid
	 *           the base store uid
	 * @return the active
	 */
	public Optional<SCPIServiceProviderModel> getActive(String baseStoreUid);

	/**
	 * Gets the active.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @return the active
	 */
	public Optional<SCPIServiceProviderModel> getActive(BaseStoreModel baseStoreModel);

	/**
	 * Gets the active by current base store.
	 *
	 * @return the active by current base store
	 */
	public Optional<SCPIServiceProviderModel> getActiveByCurrentBaseStore();
}
