/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.dao.impl;

import com.aacs.aacsscpiservices.model.SCPISalesOrderServiceProviderModel;


/**
 *
 */
public class DefaultSCPISalesOrderProviderDao extends DefaultSCPIProviderDao
{

	public DefaultSCPISalesOrderProviderDao()
	{
		super(SCPISalesOrderServiceProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return SCPISalesOrderServiceProviderModel._TYPECODE;
	}

}
