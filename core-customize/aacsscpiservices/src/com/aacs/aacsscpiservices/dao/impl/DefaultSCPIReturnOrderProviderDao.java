/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.dao.impl;

import com.aacs.aacsscpiservices.model.SCPIReturnOrderServiceProviderModel;


/**
 *
 */
public class DefaultSCPIReturnOrderProviderDao extends DefaultSCPIProviderDao
{

	public DefaultSCPIReturnOrderProviderDao()
	{
		super(SCPIReturnOrderServiceProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return SCPIReturnOrderServiceProviderModel._TYPECODE;
	}

}
