/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.service;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.returns.model.ReturnRequestModel;

import com.aacs.aacsscpiservices.exception.SCPIException;


/**
 * @author Husam Dababneh
 */
public interface SCPIService
{

	public void sendSCPISalesOrder(ConsignmentModel consignment) throws SCPIException;


	public void sendSCPIReturnOrder(ReturnRequestModel returnRequest) throws SCPIException;

}
