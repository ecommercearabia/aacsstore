/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.service.impl;

import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import com.aacs.aacsscpiservices.dao.SCPIProviderDao;
import com.aacs.aacsscpiservices.model.SCPIServiceProviderModel;
import com.aacs.aacsscpiservices.service.SCPIProviderService;
import com.google.common.base.Preconditions;


/**
 * @author Husam Dababneh
 */
public class DefaultSCPIProviderService implements SCPIProviderService
{

	private static final String STORE_MUST_NOT_BE_NULL = "Store Must Not Be NULL";
	private static final String PROVIDER_DAO_NOT_FOUND = "dao not found";
	private static final String PROVIDER_CLASS_MUSTN_T_BE_NULL = "providerClass mustn't be null";

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	/** The SCPI Provider dao map. */
	@Resource(name = "scpiProviderDaoMap")
	private Map<Class<?>, SCPIProviderDao> scpiProviderDaoMap;


	@Override
	public Optional<SCPIServiceProviderModel> getSCPIProvider(final BaseStoreModel store, final Class<?> providerClass)
	{
		Preconditions.checkArgument(store != null, STORE_MUST_NOT_BE_NULL);
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<SCPIProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().getActive(store);
	}

	@Override
	public Optional<SCPIServiceProviderModel> getSCPIProviderByCurrentStore(final Class<?> providerClass)
	{
		return getSCPIProvider(getBaseStoreService().getCurrentBaseStore(), providerClass);
	}

	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	/**
	 * @return the scpiProviderDaoMap
	 */
	public Map<Class<?>, SCPIProviderDao> getScpiProviderDaoMap()
	{
		return scpiProviderDaoMap;
	}

	/**
	 * Gets the dao.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the dao
	 */
	protected Optional<SCPIProviderDao> getDao(final Class<?> providerClass)
	{
		final SCPIProviderDao dao = getScpiProviderDaoMap().get(providerClass);

		return Optional.ofNullable(dao);
	}


}
