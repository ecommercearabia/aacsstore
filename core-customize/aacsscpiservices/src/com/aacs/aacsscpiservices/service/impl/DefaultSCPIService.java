/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.service.impl;

import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aacs.aacsscpiservices.beans.returnorder.ReturnOrderBean;
import com.aacs.aacsscpiservices.beans.salesorder.SalesOrderBean;
import com.aacs.aacsscpiservices.enums.SCPIActionType;
import com.aacs.aacsscpiservices.exception.SCPIException;
import com.aacs.aacsscpiservices.exception.type.SCPIExceptionType;
import com.aacs.aacsscpiservices.gsonadapters.ListAdapter;
import com.aacs.aacsscpiservices.model.SCPIActionHistoryEntryModel;
import com.aacs.aacsscpiservices.model.SCPIReturnOrderServiceProviderModel;
import com.aacs.aacsscpiservices.model.SCPISalesOrderServiceProviderModel;
import com.aacs.aacsscpiservices.model.SCPIServiceProviderModel;
import com.aacs.aacsscpiservices.service.SCPIProviderService;
import com.aacs.aacsscpiservices.service.SCPIService;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;


/**
 * @author Husam Dababneh
 */
public class DefaultSCPIService implements SCPIService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultSCPIService.class);

	private static final String CLIENT_ERROR_SCPI_PROVIDER_IS_NULL = "Client Error: SCPI Provider is null";
	private static final String RETURNREQUEST_MUST_NOT_BE_EMPTY = "Return Request Must not be null";
	private static final String CONSIGNMENT_MUST_NOT_BE_EMPTY = "Consignment Must not be null";
	private static final String ORDER_MUST_BE_ASSOSIATED_WITH_A_BASESTORE_MODEL = "Order must be assosiated with a basestore model";
	private static final String ORDER_MUST_NOT_BE_EMPTY = "Order Must not be null";
	private static final String AUTHORIZATION = "Authorization";

	final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().registerTypeAdapter(List.class, new ListAdapter())
			.create();


	@Resource(name = "scpiProviderService")
	private SCPIProviderService scpiProviderService;

	@Resource(name = "warehouseService")
	private WarehouseService warehouseService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "orderConverter")
	private Converter<OrderModel, OrderData> orderConverter;

	@Resource(name = "ordermanagementConsignmentConverter")
	private Converter<ConsignmentModel, ConsignmentData> consignmentConverter;

	@Resource(name = "scpiConsignmentToSalesOrderConverter")
	private Converter<ConsignmentModel, SalesOrderBean> consignmentToSalesOrderConverter;

	@Resource(name = "scpiProviderToSalesOrderConverter")
	private Converter<SCPISalesOrderServiceProviderModel, SalesOrderBean> scpiProviderToSalesOrderConverter;


	@Resource(name = "scpiReturnRequestToReturnOrderConverter")
	private Converter<ReturnRequestModel, ReturnOrderBean> returnRequestToSalesOrderConverter;

	@Resource(name = "scpiProviderToReturnOrderConverter")
	private Converter<SCPIReturnOrderServiceProviderModel, ReturnOrderBean> scpiProviderToReturnOrderConverter;


	private boolean isResponseSuccessful(final int status)
	{
		return status >= 200 && status <= 299;
	}

	@Override
	public void sendSCPISalesOrder(final ConsignmentModel consignment) throws SCPIException
	{
		Preconditions.checkArgument(consignment != null, CONSIGNMENT_MUST_NOT_BE_EMPTY);
		final AbstractOrderModel order = consignment.getOrder();
		Preconditions.checkArgument(order != null, ORDER_MUST_NOT_BE_EMPTY);
		final BaseStoreModel store = order.getStore();
		Preconditions.checkArgument(store != null, ORDER_MUST_BE_ASSOSIATED_WITH_A_BASESTORE_MODEL);

		final Optional<SCPIServiceProviderModel> scpiProvider = getScpiProviderService().getSCPIProvider(store,
				SCPISalesOrderServiceProviderModel.class);
		if (scpiProvider.isEmpty())
		{
			throw new SCPIException(SCPIExceptionType.CLIENT_ERROR, CLIENT_ERROR_SCPI_PROVIDER_IS_NULL);
		}

		final SCPISalesOrderServiceProviderModel salesOrderProvider = (SCPISalesOrderServiceProviderModel) scpiProvider.get();
		final Map<String, String> headers = createRequestHeaders(salesOrderProvider);
		final String baseUrl = scpiProvider.get().getBaseUrl();
		final SalesOrderBean data = prepareSalesOrderData(consignment, salesOrderProvider);

		//		LOG.info(gson.toJson(data));

		String response = null;
		try
		{
			final HttpResponse<String> httpResponse = Unirest.post(baseUrl).headers(headers).body(gson.toJson(data)).asString();
			response = httpResponse.getBody();

			if (!isResponseSuccessful(httpResponse.getStatus()))
			{
				saveActionInHistory(order, SCPIActionType.SALESORDER, gson.toJson(data), httpResponse.getBody());
				throw new SCPIException(SCPIExceptionType.SERVER_ERROR, httpResponse.getBody());
			}
		}
		catch (final UnirestException e)
		{
			saveActionInHistory(order, SCPIActionType.SALESORDER, gson.toJson(data), e.getMessage());
			throw new SCPIException(SCPIExceptionType.SERVER_ERROR, e.getMessage());
		}

		saveActionInHistory(order, SCPIActionType.SALESORDER, gson.toJson(data), response);

	}

	@Override
	public void sendSCPIReturnOrder(final ReturnRequestModel returnRequest) throws SCPIException
	{
		Preconditions.checkArgument(returnRequest != null, RETURNREQUEST_MUST_NOT_BE_EMPTY);
		final AbstractOrderModel order = returnRequest.getOrder();
		Preconditions.checkArgument(order != null, ORDER_MUST_NOT_BE_EMPTY);
		final BaseStoreModel store = order.getStore();
		Preconditions.checkArgument(store != null, ORDER_MUST_BE_ASSOSIATED_WITH_A_BASESTORE_MODEL);

		final Optional<SCPIServiceProviderModel> scpiProvider = getScpiProviderService().getSCPIProvider(store,
				SCPIReturnOrderServiceProviderModel.class);
		if (scpiProvider.isEmpty())
		{
			throw new SCPIException(SCPIExceptionType.CLIENT_ERROR, CLIENT_ERROR_SCPI_PROVIDER_IS_NULL);
		}

		final SCPIReturnOrderServiceProviderModel returnOrderProvider = (SCPIReturnOrderServiceProviderModel) scpiProvider.get();
		final Map<String, String> headers = createRequestHeaders(returnOrderProvider);
		final String baseUrl = scpiProvider.get().getBaseUrl();
		final ReturnOrderBean data = prepareReturnOrderData(returnRequest, returnOrderProvider);

		//		LOG.info(gson.toJson(data));


		String response = null;
		try
		{
			final HttpResponse<String> httpResponse = Unirest.post(baseUrl).headers(headers).body(gson.toJson(data)).asString();
			response = httpResponse.getBody();

			if (!isResponseSuccessful(httpResponse.getStatus()))
			{
				saveActionInHistory(order, SCPIActionType.RETURNORDER, gson.toJson(data), httpResponse.getBody());
				throw new SCPIException(SCPIExceptionType.SERVER_ERROR, httpResponse.getBody());
			}
		}
		catch (final UnirestException e)
		{
			saveActionInHistory(order, SCPIActionType.RETURNORDER, gson.toJson(data), e.getMessage());
			throw new SCPIException(SCPIExceptionType.SERVER_ERROR, e.getMessage());
		}

		saveActionInHistory(order, SCPIActionType.RETURNORDER, gson.toJson(data), response);
	}





	private void saveActionInHistory(final AbstractOrderModel order, final SCPIActionType type, final String request,
			final String response)
	{
		final List<SCPIActionHistoryEntryModel> history = new ArrayList<>();
		if (order.getScpiActionHistory() != null)
		{
			history.addAll(order.getScpiActionHistory());
		}
		final SCPIActionHistoryEntryModel historyEntry = modelService.create(SCPIActionHistoryEntryModel.class);
		historyEntry.setType(type);
		historyEntry.setRequest(request);
		historyEntry.setResponse(response);
		historyEntry.setOrder(order);
		history.add(historyEntry);
		modelService.save(historyEntry);
		order.setScpiActionHistory(history);
		modelService.save(order);
		modelService.refresh(order);
	}



	/**
	 *
	 */
	private ReturnOrderBean prepareReturnOrderData(final ReturnRequestModel returnRequest,
			final SCPIReturnOrderServiceProviderModel returnOrderProvider)
	{
		final ReturnOrderBean returnOrder = getReturnRequestToSalesOrderConverter().convert(returnRequest);
		getScpiProviderToReturnOrderConverter().convert(returnOrderProvider, returnOrder);
		return returnOrder;
	}


	/**
	 *
	 */
	private SalesOrderBean prepareSalesOrderData(final ConsignmentModel consignment,
			final SCPISalesOrderServiceProviderModel salesOrderProvider)
	{
		final SalesOrderBean salesOrder = consignmentToSalesOrderConverter.convert(consignment);
		scpiProviderToSalesOrderConverter.convert(salesOrderProvider, salesOrder);
		return salesOrder;
	}



	private Map<String, String> createRequestHeaders(final SCPIServiceProviderModel provider)
	{
		final Map<String, String> headers = new HashMap<>();
		headers.put(AUTHORIZATION, DefaultSCPIService.getBasicAuthHeader(provider.getUsername(), provider.getPassword()));
		return headers;
	}

	private static String getBasicAuthHeader(final String apiUsername, final String apiPassword)
	{
		final String auth = apiUsername + ":" + apiPassword;
		final byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes());
		return "Basic " + new String(encodedAuth);
	}

	/**
	 * @return the scpiProviderService
	 */
	protected SCPIProviderService getScpiProviderService()
	{
		return scpiProviderService;
	}


	/**
	 * @return the warehouseService
	 */
	protected WarehouseService getWarehouseService()
	{
		return warehouseService;
	}


	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}


	/**
	 * @return the orderConverter
	 */
	protected Converter<OrderModel, OrderData> getOrderConverter()
	{
		return orderConverter;
	}


	/**
	 * @return the consignmentConverter
	 */
	protected Converter<ConsignmentModel, ConsignmentData> getConsignmentConverter()
	{
		return consignmentConverter;
	}

	/**
	 * @return the returnRequestToSalesOrderConverter
	 */
	public Converter<ReturnRequestModel, ReturnOrderBean> getReturnRequestToSalesOrderConverter()
	{
		return returnRequestToSalesOrderConverter;
	}

	/**
	 * @return the scpiProviderToReturnOrderConverter
	 */
	public Converter<SCPIReturnOrderServiceProviderModel, ReturnOrderBean> getScpiProviderToReturnOrderConverter()
	{
		return scpiProviderToReturnOrderConverter;
	}





}
