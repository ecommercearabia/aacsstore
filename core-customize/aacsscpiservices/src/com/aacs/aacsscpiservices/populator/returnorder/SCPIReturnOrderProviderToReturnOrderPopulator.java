/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.populator.returnorder;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aacs.aacsscpiservices.beans.returnorder.ReturnOrderBean;
import com.aacs.aacsscpiservices.beans.returnorder.ReturnOrderEntryBean;
import com.aacs.aacsscpiservices.beans.returnorder.ReturnOrderEntryPromotionBean;
import com.aacs.aacsscpiservices.beans.returnorder.ReturnOrderEntryTaxBean;
import com.aacs.aacsscpiservices.model.SCPIReturnOrderServiceProviderModel;


/**
 * @author husam.dababneh@erabia.com
 */
public class SCPIReturnOrderProviderToReturnOrderPopulator
		implements Populator<SCPIReturnOrderServiceProviderModel, ReturnOrderBean>
{

	private static final Logger LOG = LoggerFactory.getLogger(SCPIReturnOrderProviderToReturnOrderPopulator.class);


	@Override
	public void populate(final SCPIReturnOrderServiceProviderModel source, final ReturnOrderBean target) throws ConversionException
	{
		final String posNumber = String.valueOf(source.getWorkstationID());

		target.setPosNumber(posNumber);
		target.setOperatorQualifier(source.getOperatorQualifier() != null ? String.valueOf(source.getOperatorQualifier()) : "");
		target.setOperatorID(String.valueOf(source.getOperatorID()));
		target.setTransactionCurrency(source.getTransactionCurrency() == null ? "" : source.getTransactionCurrency().getIsocode());

		for (final ReturnOrderEntryBean salesOrderEntryBean : target.getReturnOrderEntries())
		{
			salesOrderEntryBean.setPosNumber(posNumber);
			salesOrderEntryBean.setItemIDEntryMethodCode(String.valueOf(source.getItemIDEntryMethodCode()));
		}
		for (final ReturnOrderEntryTaxBean salesOrderEntryTaxBean : target.getReturnOrderEntriesTaxes())
		{
			salesOrderEntryTaxBean.setPosNumber(posNumber);
		}
		for (final ReturnOrderEntryPromotionBean salesOrderEntryPromotionBean : target.getReturnOrderEntriesPromotions())
		{
			salesOrderEntryPromotionBean.setPosNumber(posNumber);
		}

	}

}
