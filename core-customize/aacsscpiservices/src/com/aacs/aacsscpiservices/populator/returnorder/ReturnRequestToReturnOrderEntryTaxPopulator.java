/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.populator.returnorder;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.returns.model.ReturnEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.TaxValue;
import de.hybris.platform.warehousing.returns.strategy.RestockWarehouseSelectionStrategy;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aacs.aacsscpiservices.beans.returnorder.ReturnOrderEntryTaxBean;
import com.google.common.base.Preconditions;


/**
 * @author husam.dababneh@erabia.com
 */
public class ReturnRequestToReturnOrderEntryTaxPopulator implements Populator<ReturnRequestModel, List<ReturnOrderEntryTaxBean>>
{
	private static final Logger LOG = LoggerFactory.getLogger(ReturnRequestToReturnOrderEntryTaxPopulator.class);

	@Resource(name = "restockWarehouseSelectionStrategy")
	private RestockWarehouseSelectionStrategy restockWarehouseSelectionStrategy;

	@Override
	public void populate(final ReturnRequestModel returnRequest, List<ReturnOrderEntryTaxBean> target) throws ConversionException
	{
		if (target == null)
		{
			target = new ArrayList<>();
		}
		Preconditions.checkNotNull(returnRequest, "Return Request Is null");
		Preconditions.checkNotNull(returnRequest.getOrder(), "Order Is null for [" + returnRequest.getCode() + "] Return Request");
		Preconditions.checkNotNull(returnRequest.getOrder().getUser(),
				"User Is null for [" + returnRequest.getCode() + "] Return Request");

		final WarehouseModel returnWarehouse = getReturnWarehouse(returnRequest);
		Preconditions.checkNotNull(returnWarehouse, "Warehouse Is null for [" + returnRequest.getCode() + "] Return Request");


		final AbstractOrderModel order = returnRequest.getOrder();
		final CustomerModel customer = (CustomerModel) order.getUser();

		final SimpleDateFormat orderDateFormat = new SimpleDateFormat("yyyyMMdd");

		final List<ReturnEntryModel> arrayList = returnRequest.getReturnEntries().stream()
				.sorted((arg0, arg1) -> Integer.compare(arg0.getOrderEntry().getEntryNumber(), arg1.getOrderEntry().getEntryNumber()))
				.collect(Collectors.toList());

		for (final ReturnEntryModel source : arrayList)
		{
			final ReturnOrderEntryTaxBean taxBean = new ReturnOrderEntryTaxBean();
			final double taxValue = source.getOrderEntry().getTaxValues().stream().mapToDouble(TaxValue::getAppliedValue).sum();
			taxBean.setWarehouseID(returnWarehouse.getCode());
			taxBean.setOrderDate(orderDateFormat.format(order.getDate()));
			taxBean.setTransactionType(customer.isAacsCustomer() ? "1001" : "1004");
			taxBean.setTransactionNumber(String.valueOf(order.getCode()));
			taxBean.setSalesItemNumber(String.valueOf(source.getOrderEntry().getEntryNumber()));
			taxBean.setTaxSequenceNumber(String.valueOf(target.size()));
			taxBean.setTaxTypeCode(taxValue > 0.0 ? "1301" : "2301");
			taxBean.setTaxAmount(String.valueOf(taxValue == 0.0 ? taxValue : taxValue * -1));

			target.add(taxBean);
		}
	}

	/**
	 * Finds the {@link WarehouseModel}, which can accept the returned good(s)
	 *
	 * @param returnRequest
	 *           the {@link ReturnRequestModel} for which goods need to be put back in stock
	 * @return the {@link WarehouseModel} which can accept the returned good(s) from the given
	 *         {@link ReturnRequestModel}.
	 */
	protected WarehouseModel getReturnWarehouse(final ReturnRequestModel returnRequest)
	{
		WarehouseModel returnWarehouse = returnRequest.getReturnWarehouse();
		if (returnWarehouse == null)
		{
			LOG.info(
					"No return warehouse set for the Return Request: [{}], applying RestockWarehouseSelectionStrategy to find the warehouse",
					returnRequest.getRMA());
			returnWarehouse = getRestockWarehouseSelectionStrategy().performStrategy(returnRequest);
		}
		return returnWarehouse;
	}

	/**
	 * @return the restockWarehouseSelectionStrategy
	 */
	public RestockWarehouseSelectionStrategy getRestockWarehouseSelectionStrategy()
	{
		return restockWarehouseSelectionStrategy;
	}

}
