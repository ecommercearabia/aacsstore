/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.populator.returnorder;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.returns.model.ReturnEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.warehousing.returns.strategy.RestockWarehouseSelectionStrategy;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aacs.aacsscpiservices.beans.returnorder.ReturnOrderEntryPromotionBean;
import com.google.common.base.Preconditions;


/**
 * @author husam.dababneh@erabia.com
 */
public class ReturnRequestToReturnOrderEntryPromotionPopulator
		implements Populator<ReturnRequestModel, List<ReturnOrderEntryPromotionBean>>
{

	private static final Logger LOG = LoggerFactory.getLogger(ReturnRequestToReturnOrderEntryPromotionPopulator.class);

	@Resource(name = "restockWarehouseSelectionStrategy")
	private RestockWarehouseSelectionStrategy restockWarehouseSelectionStrategy;

	final SimpleDateFormat orderDateFormat = new SimpleDateFormat("yyyyMMdd");

	@Override
	public void populate(final ReturnRequestModel returnRequest, List<ReturnOrderEntryPromotionBean> target)
			throws ConversionException
	{
		if (target == null)
		{
			target = new ArrayList<>();
		}
		Preconditions.checkNotNull(returnRequest, "Return Request Is null");
		final AbstractOrderModel order = returnRequest.getOrder();
		Preconditions.checkNotNull(order, "Order Is null for [" + returnRequest.getCode() + "] ReturnRequest");
		Preconditions.checkNotNull(order.getUser(), "User Is null for [" + returnRequest.getCode() + "] ReturnRequest");
		final CustomerModel customer = (CustomerModel) order.getUser();
		final WarehouseModel returnWarehouse = getReturnWarehouse(returnRequest);
		Preconditions.checkNotNull(order, "Warehouse Is null for [" + returnRequest.getCode() + "] ReturnRequest");


		final List<ReturnEntryModel> arrayList = returnRequest.getReturnEntries().stream()
				.sorted((arg0, arg1) -> Integer.compare(arg0.getOrderEntry().getEntryNumber(), arg1.getOrderEntry().getEntryNumber()))
				.collect(Collectors.toList());

		for (final ReturnEntryModel source : arrayList)
		{
			final AbstractOrderEntryModel orderEntry = source.getOrderEntry();

			for (int i = 0; i < orderEntry.getDiscountValues().size(); i++)
			{
				final DiscountValue discountValue = orderEntry.getDiscountValues().get(i);
				final ReturnOrderEntryPromotionBean promotionEntry = new ReturnOrderEntryPromotionBean();
				promotionEntry.setWarehouseID(returnWarehouse.getCode());
				promotionEntry.setOrderDate(orderDateFormat.format(order.getDate()));
				promotionEntry.setTransactionType(customer.isAacsCustomer() ? "1001" : "1004");
				promotionEntry.setTransactionNumber(String.valueOf(order.getCode()));
				promotionEntry.setRetailSequenceNumber(String.valueOf(orderEntry.getEntryNumber()));

				promotionEntry.setDiscountAmount(String.valueOf(discountValue.getAppliedValue()));
				promotionEntry.setDiscountTypeCode("ZPDI");
				promotionEntry.setDiscountSequenceNumber(String.valueOf(target.size()));
				promotionEntry.setBonusBuyID(String.valueOf(discountValue.getCode()));
				promotionEntry.setOriginalPrice(String.valueOf(orderEntry.getBasePrice()));

				target.add(promotionEntry);
			}

			if (orderEntry.isPriceRowPromotion())
			{
				populatePromotionEntryForProduct(orderEntry, returnRequest, returnWarehouse, target);
			}

		}

	}

	private void populatePromotionEntryForProduct(final AbstractOrderEntryModel orderEntry, final ReturnRequestModel source,
			final WarehouseModel warehouse, final List<ReturnOrderEntryPromotionBean> target)
	{
		final AbstractOrderModel order = orderEntry.getOrder();
		final ReturnOrderEntryPromotionBean result = new ReturnOrderEntryPromotionBean();
		result.setWarehouseID(warehouse.getCode());
		result.setOrderDate(orderDateFormat.format(order.getDate()));
		result.setTransactionType(((CustomerModel) order.getUser()).isAacsCustomer() ? "1001" : "1004");
		result.setTransactionNumber(String.valueOf(order.getCode()));
		result.setRetailSequenceNumber(String.valueOf(orderEntry.getEntryNumber()));
		result.setDiscountTypeCode("ZPDI");
		result.setDiscountSequenceNumber(String.valueOf(target.size()));
		result.setBonusBuyID("Promotion");
		result.setDiscountAmount(
				String.valueOf(orderEntry.getPriceRowPromotionOriginalPrice() - orderEntry.getPriceRowPromotionPrice()));

		target.add(result);


	}

	/**
	 * Finds the {@link WarehouseModel}, which can accept the returned good(s)
	 *
	 * @param returnRequest
	 *           the {@link ReturnRequestModel} for which goods need to be put back in stock
	 * @return the {@link WarehouseModel} which can accept the returned good(s) from the given
	 *         {@link ReturnRequestModel}.
	 */
	protected WarehouseModel getReturnWarehouse(final ReturnRequestModel returnRequest)
	{
		WarehouseModel returnWarehouse = returnRequest.getReturnWarehouse();
		if (returnWarehouse == null)
		{
			LOG.info(
					"No return warehouse set for the Return Request: [{}], applying RestockWarehouseSelectionStrategy to find the warehouse",
					returnRequest.getRMA());
			returnWarehouse = getRestockWarehouseSelectionStrategy().performStrategy(returnRequest);
		}
		return returnWarehouse;
	}

	/**
	 * @return the restockWarehouseSelectionStrategy
	 */
	public RestockWarehouseSelectionStrategy getRestockWarehouseSelectionStrategy()
	{
		return restockWarehouseSelectionStrategy;
	}


}
