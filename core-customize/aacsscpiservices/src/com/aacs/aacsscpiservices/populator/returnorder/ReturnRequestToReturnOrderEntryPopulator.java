/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.populator.returnorder;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.returns.model.ReturnEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.warehousing.returns.strategy.RestockWarehouseSelectionStrategy;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aacs.aacsscpiservices.beans.returnorder.ReturnOrderEntryBean;
import com.google.common.base.Preconditions;


/**
 * @author husam.dababneh@erabia.com
 */
public class ReturnRequestToReturnOrderEntryPopulator implements Populator<ReturnRequestModel, List<ReturnOrderEntryBean>>
{

	private static final Logger LOG = LoggerFactory.getLogger(ReturnRequestToReturnOrderEntryPopulator.class);

	@Resource(name = "restockWarehouseSelectionStrategy")
	private RestockWarehouseSelectionStrategy restockWarehouseSelectionStrategy;

	private final SimpleDateFormat orderDateFormat = new SimpleDateFormat("yyyyMMdd");
	private final SimpleDateFormat timestampFormat = new SimpleDateFormat("yyyyMMddhhmmss");


	@Override
	public void populate(final ReturnRequestModel source, final List<ReturnOrderEntryBean> target) throws ConversionException
	{

		Preconditions.checkNotNull(source, "ReturnRequest Is null");
		Preconditions.checkNotNull(source.getOrder(), "Order Is null for [" + source.getCode() + "] ReturnRequest");
		Preconditions.checkNotNull(source.getOrder().getUser(), "User Is null for [" + source.getCode() + "] ReturnRequest");
		Preconditions.checkNotNull(source.getOrder().getUser(), "User Is null for [" + source.getCode() + "] ReturnRequest");
		final AbstractOrderModel order = source.getOrder();
		final CustomerModel customer = (CustomerModel) order.getUser();
		final WarehouseModel returnWarehouse = getReturnWarehouse(source);
		Preconditions.checkNotNull(source.getOrder(), "Warehouse Is null for [" + source.getCode() + "] ReturnRequest");

		for (final ReturnEntryModel returnEntry : source.getReturnEntries())
		{
			final AbstractOrderEntryModel orderEntry = returnEntry.getOrderEntry();
			final ProductModel product = orderEntry.getProduct();

			final ReturnOrderEntryBean bean = new ReturnOrderEntryBean();
			bean.setWarehouseID(returnWarehouse.getCode());
			bean.setOrderDate(orderDateFormat.format(order.getDate()));
			bean.setTransactionType(customer.isAacsCustomer() ? "1001" : "1004");
			bean.setTransactionNumber(String.valueOf(order.getCode()));
			bean.setSalesItemNumber(String.valueOf(orderEntry.getEntryNumber()));
			bean.setSalesItemType(customer.isAacsCustomer() ? "2003" : "2804");
			bean.setItemIDQualifier("1");
			bean.setBasePrice(String.valueOf(orderEntry.getBasePrice()));
			bean.setOriginalPrice(String.valueOf(orderEntry.getPriceRowPromotionOriginalPrice()));
			bean.setItemID(getProductID(product));
			bean.setSapBaseUnitConversion(
					product.getSapBaseUnitConversion() != null ? String.valueOf(product.getSapBaseUnitConversion()) : "");
			bean.setQuantity(String.valueOf(returnEntry.getReceivedQuantity() * -1));
			bean.setUom(String.valueOf(product.getUnit() == null ? "" : product.getUnit().getCode()));
			bean.setSalesAmount(String.valueOf(orderEntry.getBasePrice() * returnEntry.getReceivedQuantity() * -1));
			bean.setScanTime(timestampFormat.format(order.getDate()));
			bean.setPromitionID(orderEntry.getPriceRowPromotionId());

			target.add(bean);
		}

		addDeliveryCharges(source, customer, returnWarehouse, target);
	}


	/**
	 * Finds the {@link WarehouseModel}, which can accept the returned good(s)
	 *
	 * @param returnRequest
	 *           the {@link ReturnRequestModel} for which goods need to be put back in stock
	 * @return the {@link WarehouseModel} which can accept the returned good(s) from the given
	 *         {@link ReturnRequestModel}.
	 */
	protected WarehouseModel getReturnWarehouse(final ReturnRequestModel returnRequest)
	{
		WarehouseModel returnWarehouse = returnRequest.getReturnWarehouse();
		if (returnWarehouse == null)
		{
			LOG.info(
					"No return warehouse set for the Return Request: [{}], applying RestockWarehouseSelectionStrategy to find the warehouse",
					returnRequest.getRMA());
			returnWarehouse = getRestockWarehouseSelectionStrategy().performStrategy(returnRequest);
		}
		return returnWarehouse;
	}


	private void addDeliveryCharges(final ReturnRequestModel returnRequest, final CustomerModel customer,
			final WarehouseModel returnWarehouse, final List<ReturnOrderEntryBean> target)
	{
		final ReturnOrderEntryBean bean = new ReturnOrderEntryBean();

		final OrderModel order = returnRequest.getOrder();

		bean.setWarehouseID(String.valueOf(returnWarehouse.getCode()));
		bean.setOrderDate(orderDateFormat.format(order.getDate()));
		bean.setTransactionType(customer.isAacsCustomer() ? "1001" : "1004");
		bean.setTransactionNumber(String.valueOf(order.getCode()));
		bean.setSalesItemNumber(String.valueOf(target.size()));
		bean.setSalesItemType(customer.isAacsCustomer() ? "2003" : "2804");
		bean.setItemIDQualifier("1");
		bean.setBasePrice("");
		bean.setOriginalPrice("");
		bean.setSapBaseUnitConversion(String.valueOf(1.0));
		bean.setQuantity(String.valueOf(1));
		bean.setUom("EA");
		bean.setSalesAmount(
				String.valueOf(order.getDeliveryCost() == 0.0 ? order.getDeliveryCost() : order.getDeliveryCost() * -1));
		bean.setScanTime(timestampFormat.format(order.getDate()));
		bean.setItemID("");
		bean.setPromitionID("");

		target.add(bean);
	}


	/**
	 *
	 */
	private String getProductID(final ProductModel product)
	{
		if (product == null || Strings.isBlank(product.getCode()))
		{
			return "";
		}

		final String[] split = product.getCode().split("_");
		return split[0];
	}


	/**
	 * @return the restockWarehouseSelectionStrategy
	 */
	public RestockWarehouseSelectionStrategy getRestockWarehouseSelectionStrategy()
	{
		return restockWarehouseSelectionStrategy;
	}

}

