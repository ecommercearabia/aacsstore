/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.populator.returnorder;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.warehousing.returns.strategy.RestockWarehouseSelectionStrategy;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aacs.aacsscpiservices.beans.returnorder.ReturnOrderBean;
import com.aacs.aacsscpiservices.beans.returnorder.ReturnOrderEntryBean;
import com.aacs.aacsscpiservices.beans.returnorder.ReturnOrderEntryPromotionBean;
import com.aacs.aacsscpiservices.beans.returnorder.ReturnOrderEntryTaxBean;
import com.aacs.aacsscpiservices.beans.returnorder.ReturnOrderTenderBean;
import com.google.common.base.Preconditions;


/**
 * @author husam.dababneh@erabia.com
 */
public class ReturnRequestToReturnOrderPopulator implements Populator<ReturnRequestModel, ReturnOrderBean>
{
	private static final Logger LOG = LoggerFactory.getLogger(ReturnRequestToReturnOrderPopulator.class);

	@Resource(name = "restockWarehouseSelectionStrategy")
	private RestockWarehouseSelectionStrategy restockWarehouseSelectionStrategy;

	@Resource(name = "returnRequestToReturnOrderEntryPopulator")
	private Populator<ReturnRequestModel, List<ReturnOrderEntryBean>> returnRequestToReturnOrderEntry;

	@Resource(name = "returnRequestToReturnOrderEntryPromotionPopulator")
	private Populator<ReturnRequestModel, List<ReturnOrderEntryPromotionBean>> returnRequestToReturnOrderPromotionEntries;

	@Resource(name = "returnRequestToReturnOrderTenderPopulator")
	private Populator<ReturnRequestModel, List<ReturnOrderTenderBean>> returnRequestToReturnOrderTenderPopulator;

	@Resource(name = "returnRequestToReturnOrderEntryTaxPopulator")
	private Populator<ReturnRequestModel, List<ReturnOrderEntryTaxBean>> returnRequestToReturnOrderTaxEntry;

	@Override
	public void populate(final ReturnRequestModel returnRequest, final ReturnOrderBean target) throws ConversionException
	{
		Preconditions.checkNotNull(returnRequest, "Return Request Is null");
		final AbstractOrderModel order = returnRequest.getOrder();
		Preconditions.checkNotNull(order, "Order Is null for [" + returnRequest.getCode() + "] Return Request");
		Preconditions.checkNotNull(order.getUser(), "User Is null for [" + returnRequest.getCode() + "] Return Request");
		final CustomerModel customer = (CustomerModel) order.getUser();

		final SimpleDateFormat orderDateFormat = new SimpleDateFormat("yyyyMMdd");
		final SimpleDateFormat timestampFormat = new SimpleDateFormat("yyyyMMddHHmmss");

		final WarehouseModel returnWarehouse = getReturnWarehouse(returnRequest);
		Preconditions.checkNotNull(returnWarehouse, "returnWarehouse Is null for [" + returnRequest.getCode() + "] Return Request");

		target.setWarehouseID(returnWarehouse.getCode());
		target.setOrderDate(orderDateFormat.format(order.getDate()));
		target.setTransactionType(customer.isAacsCustomer() ? "1001" : "1004");
		target.setTransactionNumber(String.valueOf(order.getCode()));
		target.setBeginDateTimestamp(timestampFormat.format(order.getDate()));
		target.setEndDateTimestamp(timestampFormat.format(order.getDate()));
		target.setOperatorQualifier("");
		target.setPartnerQualifier(customer.isAacsCustomer() ? "1" : "");
		target.setPartnerID(customer.isAacsCustomer() ? customer.getCustomerID() : "");


		final List<ReturnOrderEntryBean> returnOrderEntries = new ArrayList<>();
		getConsignmentToReturnOrderEntry().populate(returnRequest, returnOrderEntries);
		target.getReturnOrderEntries().addAll(returnOrderEntries);

		final List<ReturnOrderEntryPromotionBean> promotionEntries = new ArrayList<>();
		getConsignmentToReturnOrderPromotionEntries().populate(returnRequest, promotionEntries);
		target.getReturnOrderEntriesPromotions().addAll(promotionEntries);

		final List<ReturnOrderEntryTaxBean> taxEntries = new ArrayList<>();
		getConsignmentToReturnOrderTaxEntry().populate(returnRequest, taxEntries);
		target.getReturnOrderEntriesTaxes().addAll(taxEntries);

		final List<ReturnOrderTenderBean> tenderEntries = new ArrayList<>();
		getConsignmentToReturnOrderTenderPopulator().populate(returnRequest, tenderEntries);
		target.getReturnOrderTenders().addAll(tenderEntries);


		// Sort The arrays
		Collections.sort(target.getReturnOrderEntries(),
				(e1, e2) -> Integer.compare(Integer.valueOf(e1.getSalesItemNumber()), Integer.valueOf(e2.getSalesItemNumber())));

		Collections.sort(target.getReturnOrderEntriesPromotions(), (e1, e2) -> Integer
				.compare(Integer.valueOf(e1.getRetailSequenceNumber()), Integer.valueOf(e1.getRetailSequenceNumber())));

		Collections.sort(target.getReturnOrderEntriesTaxes(),
				(e1, e2) -> Integer.compare(Integer.valueOf(e1.getSalesItemNumber()), Integer.valueOf(e2.getSalesItemNumber())));

	}

	/**
	 * @return the consignmentEntryToReturnOrderPromotionEntry
	 */
	protected Populator<ReturnRequestModel, List<ReturnOrderEntryPromotionBean>> getConsignmentToReturnOrderPromotionEntries()
	{
		return returnRequestToReturnOrderPromotionEntries;
	}

	/**
	 * @return the consignmentEntryToReturnOrderEntry
	 */
	protected Populator<ReturnRequestModel, List<ReturnOrderEntryBean>> getConsignmentToReturnOrderEntry()
	{
		return returnRequestToReturnOrderEntry;
	}

	/**
	 * @return the consignmentEntryToReturnOrderTaxEntry
	 */
	protected Populator<ReturnRequestModel, List<ReturnOrderEntryTaxBean>> getConsignmentToReturnOrderTaxEntry()
	{
		return returnRequestToReturnOrderTaxEntry;
	}

	/**
	 * @return the consignmentEntryToReturnOrderTenderPopulator
	 */
	protected Populator<ReturnRequestModel, List<ReturnOrderTenderBean>> getConsignmentToReturnOrderTenderPopulator()
	{
		return returnRequestToReturnOrderTenderPopulator;
	}


	/**
	 * Finds the {@link WarehouseModel}, which can accept the returned good(s)
	 *
	 * @param returnRequest
	 *           the {@link ReturnRequestModel} for which goods need to be put back in stock
	 * @return the {@link WarehouseModel} which can accept the returned good(s) from the given
	 *         {@link ReturnRequestModel}.
	 */
	protected WarehouseModel getReturnWarehouse(final ReturnRequestModel returnRequest)
	{
		WarehouseModel returnWarehouse = returnRequest.getReturnWarehouse();
		if (returnWarehouse == null)
		{
			LOG.info(
					"No return warehouse set for the Return Request: [{}], applying RestockWarehouseSelectionStrategy to find the warehouse",
					returnRequest.getRMA());
			returnWarehouse = getRestockWarehouseSelectionStrategy().performStrategy(returnRequest);
		}
		return returnWarehouse;
	}

	/**
	 * @return the restockWarehouseSelectionStrategy
	 */
	public RestockWarehouseSelectionStrategy getRestockWarehouseSelectionStrategy()
	{
		return restockWarehouseSelectionStrategy;
	}
}
