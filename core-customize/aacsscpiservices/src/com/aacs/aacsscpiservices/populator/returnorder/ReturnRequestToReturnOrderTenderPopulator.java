/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.populator.returnorder;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.warehousing.returns.strategy.RestockWarehouseSelectionStrategy;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aacs.aacsloyaltyprogramprovider.enums.LoyaltyPaymentStatus;
import com.aacs.aacsloyaltyprogramprovider.enums.LoyaltyPaymentType;
import com.aacs.aacsscpiservices.beans.returnorder.ReturnOrderTenderBean;
import com.google.common.base.Preconditions;


/**
 * @author husam.dababneh@erabia.com
 */
public class ReturnRequestToReturnOrderTenderPopulator implements Populator<ReturnRequestModel, List<ReturnOrderTenderBean>>
{

	private static final Logger LOG = LoggerFactory.getLogger(ReturnRequestToReturnOrderTenderPopulator.class);

	@Resource(name = "restockWarehouseSelectionStrategy")
	private RestockWarehouseSelectionStrategy restockWarehouseSelectionStrategy;


	final SimpleDateFormat orderDateFormat = new SimpleDateFormat("yyyyMMdd");

	@Override
	public void populate(final ReturnRequestModel returnRequest, List<ReturnOrderTenderBean> target) throws ConversionException
	{

		if (target == null)
		{
			target = new ArrayList<>();
		}

		Preconditions.checkNotNull(returnRequest, "Return Request Is null");
		Preconditions.checkNotNull(returnRequest.getOrder(), "Order Is null for [" + returnRequest.getCode() + "] Return Request");
		Preconditions.checkNotNull(returnRequest.getOrder().getUser(),
				"User Is null for [" + returnRequest.getCode() + "] Return Request");

		final WarehouseModel returnWarehouse = getReturnWarehouse(returnRequest);
		Preconditions.checkNotNull(returnWarehouse, "returnWarehouse Is null for [" + returnRequest.getCode() + "] Return Request");

		final AbstractOrderModel order = returnRequest.getOrder();


		populateDefaultPaymentMode(target, returnRequest, order, returnWarehouse);
		populateStoreCreditMode(target, returnRequest, order, returnWarehouse);
		populateLoyaltyMode(target, returnRequest, order, returnWarehouse);


	}

	/**
	 * @param returnRequest
	 * @param returnWarehouse
	 *
	 */
	private void populateDefaultPaymentMode(final List<ReturnOrderTenderBean> target, final ReturnRequestModel returnRequest,
			final AbstractOrderModel order, final WarehouseModel returnWarehouse)
	{

		final String typeCode = order.getPaymentMode() == null || order.getPaymentMode().getCode() == null ? ""
				: order.getPaymentMode().getCode();

		final ReturnOrderTenderBean tender = new ReturnOrderTenderBean();
		tender.setTenderSequenceNumber(String.valueOf(target.size()));


		switch (typeCode.toLowerCase())
		{
			case "continue":
				return;
			case "cod":
				tender.setTenderTypeCode("ZTCS");
				break;
			case "card":
				tender.setTenderTypeCode("ZTVI");
				break;
			case "ccod":
				tender.setTenderTypeCode("ZTCS");
				break;
			default:
				throw new IllegalArgumentException(
						"Unknown Payment Mode[" + typeCode.toLowerCase() + "] in Order[" + order.getCode() + "]");
		}

		tender.setWarehouseID(returnWarehouse.getCode());
		tender.setOrderDate(orderDateFormat.format(order.getDate()));
		tender.setTenderAmount(String.valueOf(order.getTotalPrice() * -1));
		tender.setTenderCurrency(String.valueOf(order.getCurrency() == null ? "" : order.getCurrency().getIsocode()));
		tender.setReferenceID("");
		tender.setTransactionNumber(String.valueOf(order.getCode()));
		target.add(tender);

	}

	private void populateLoyaltyMode(final List<ReturnOrderTenderBean> target, final ReturnRequestModel returnRequest,
			final AbstractOrderModel order, final WarehouseModel returnWarehouse)
	{
		if (order.getLoyaltyAmount() != 0.0 && LoyaltyPaymentType.CAPTURED.equals(order.getLoyaltyPaymentType())
				&& LoyaltyPaymentStatus.SUCCESS.equals(order.getLoyaltyPaymentStatus()))
		{
			final ReturnOrderTenderBean tender = new ReturnOrderTenderBean();
			tender.setTenderSequenceNumber(String.valueOf(target.size()));
			tender.setTenderTypeCode("ZLOP");
			tender.setTenderAmount(String.valueOf(order.getLoyaltyAmount() * -1));
			tender.setTenderCurrency(String.valueOf(order.getCurrency() == null ? "" : order.getCurrency().getIsocode()));
			tender.setWarehouseID(returnWarehouse.getCode());
			tender.setOrderDate(orderDateFormat.format(order.getDate()));
			tender.setTransactionNumber(String.valueOf(order.getCode()));
			tender.setReferenceID("");
			target.add(tender);
		}
	}

	private void populateStoreCreditMode(final List<ReturnOrderTenderBean> target, final ReturnRequestModel returnRequest,
			final AbstractOrderModel order, final WarehouseModel returnWarehouse)
	{
		if (order.getStoreCreditAmount() != 0.0)
		{
			final ReturnOrderTenderBean tender = new ReturnOrderTenderBean();
			tender.setTenderSequenceNumber(String.valueOf(target.size()));
			tender.setTenderTypeCode("ZTBL");
			tender.setTenderAmount(String.valueOf(order.getStoreCreditAmount().doubleValue() * -1));
			tender.setTenderCurrency(String.valueOf(order.getCurrency() == null ? "" : order.getCurrency().getIsocode()));
			tender.setWarehouseID(returnWarehouse.getCode());
			tender.setOrderDate(orderDateFormat.format(order.getDate()));
			tender.setTransactionNumber(String.valueOf(order.getCode()));
			tender.setReferenceID("");
			target.add(tender);
		}
	}


	/**
	 * Finds the {@link WarehouseModel}, which can accept the returned good(s)
	 *
	 * @param returnRequest
	 *           the {@link ReturnRequestModel} for which goods need to be put back in stock
	 * @return the {@link WarehouseModel} which can accept the returned good(s) from the given
	 *         {@link ReturnRequestModel}.
	 */
	protected WarehouseModel getReturnWarehouse(final ReturnRequestModel returnRequest)
	{
		WarehouseModel returnWarehouse = returnRequest.getReturnWarehouse();
		if (returnWarehouse == null)
		{
			LOG.info(
					"No return warehouse set for the Return Request: [{}], applying RestockWarehouseSelectionStrategy to find the warehouse",
					returnRequest.getRMA());
			returnWarehouse = getRestockWarehouseSelectionStrategy().performStrategy(returnRequest);
		}
		return returnWarehouse;
	}

	/**
	 * @return the restockWarehouseSelectionStrategy
	 */
	public RestockWarehouseSelectionStrategy getRestockWarehouseSelectionStrategy()
	{
		return restockWarehouseSelectionStrategy;
	}

}
