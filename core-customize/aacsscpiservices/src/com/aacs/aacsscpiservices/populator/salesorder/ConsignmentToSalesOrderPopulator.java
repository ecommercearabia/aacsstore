/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.populator.salesorder;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.util.Strings;

import com.aacs.aacsscpiservices.beans.salesorder.SalesOrderBean;
import com.aacs.aacsscpiservices.beans.salesorder.SalesOrderEntryBean;
import com.aacs.aacsscpiservices.beans.salesorder.SalesOrderEntryPromotionBean;
import com.aacs.aacsscpiservices.beans.salesorder.SalesOrderEntryTaxBean;
import com.aacs.aacsscpiservices.beans.salesorder.SalesOrderTenderBean;
import com.google.common.base.Preconditions;


/**
 * @author husam.dababneh@erabia.com
 */
public class ConsignmentToSalesOrderPopulator implements Populator<ConsignmentModel, SalesOrderBean>
{

	@Resource(name = "consignmentToSalesOrderEntryPopulator")
	private Populator<ConsignmentModel, List<SalesOrderEntryBean>> consignmentToSalesOrderEntry;

	@Resource(name = "consignmentToSalesOrderPromotionEntriesPopulator")
	private Populator<ConsignmentModel, List<SalesOrderEntryPromotionBean>> consignmentToSalesOrderPromotionEntries;

	@Resource(name = "consignmentToSalesOrderTenderPopulator")
	private Populator<ConsignmentModel, List<SalesOrderTenderBean>> consignmentToSalesOrderTenderPopulator;

	@Resource(name = "consignmentToSalesOrderTaxEntryPopulator")
	private Populator<ConsignmentModel, List<SalesOrderEntryTaxBean>> consignmentToSalesOrderTaxEntry;

	@Override
	public void populate(final ConsignmentModel consignment, final SalesOrderBean target) throws ConversionException
	{
		Preconditions.checkNotNull(consignment, "Consignment Is null");
		Preconditions.checkNotNull(consignment.getWarehouse(), "Warehouse Is null");
		final AbstractOrderModel order = consignment.getOrder();
		Preconditions.checkNotNull(order, "Order Is null for [" + consignment.getCode() + "] consignemnt");
		Preconditions.checkNotNull(order.getUser(), "User Is null for [" + consignment.getCode() + "] consignemnt");
		final CustomerModel customer = (CustomerModel) order.getUser();

		final SimpleDateFormat orderDateFormat = new SimpleDateFormat("yyyyMMdd");
		final SimpleDateFormat timestampFormat = new SimpleDateFormat("yyyyMMddHHmmss");

		target.setWarehouseID(String.valueOf(consignment.getWarehouse().getCode()));
		target.setOrderDate(orderDateFormat.format(order.getDate()));
		target.setTransactionType(customer.isAacsCustomer() ? "1001" : "1004");
		target.setTransactionNumber(String.valueOf(order.getCode()));
		target.setBeginDateTimestamp(timestampFormat.format(order.getDate()));
		target.setEndDateTimestamp(timestampFormat.format(order.getDate()));
		target.setOperatorQualifier("");
		target.setPartnerQualifier(customer.isAacsCustomer() ? "1" : "");
		target.setPartnerID(customer.isAacsCustomer() ? customer.getCustomerID() : "");
		target.setDiscount(getOrderDiscounts(order));
		target.setCoupons(getOrderCoupons(order));

		final List<SalesOrderEntryBean> salesOrderEntries = new ArrayList<>();
		getConsignmentToSalesOrderEntry().populate(consignment, salesOrderEntries);
		target.getSalesOrderEntries().addAll(salesOrderEntries);

		final List<SalesOrderEntryPromotionBean> promotionEntries = new ArrayList<>();
		getConsignmentToSalesOrderPromotionEntries().populate(consignment, promotionEntries);
		target.getSalesOrderEntriesPromotions().addAll(promotionEntries);

		final List<SalesOrderEntryTaxBean> taxEntries = new ArrayList<>();
		getConsignmentToSalesOrderTaxEntry().populate(consignment, taxEntries);
		target.getSalesOrderEntriesTaxes().addAll(taxEntries);

		final List<SalesOrderTenderBean> tenderEntries = new ArrayList<>();
		getConsignmentToSalesOrderTenderPopulator().populate(consignment, tenderEntries);
		target.getSalesOrderTenders().addAll(tenderEntries);


		// Sort The arrays
		Collections.sort(target.getSalesOrderEntries(),
				(e1, e2) -> Integer.compare(Integer.valueOf(e1.getSalesItemNumber()), Integer.valueOf(e2.getSalesItemNumber())));

		Collections.sort(target.getSalesOrderEntriesPromotions(), (e1, e2) -> Integer
				.compare(Integer.valueOf(e1.getRetailSequenceNumber()), Integer.valueOf(e1.getRetailSequenceNumber())));

		Collections.sort(target.getSalesOrderEntriesTaxes(),
				(e1, e2) -> Integer.compare(Integer.valueOf(e1.getSalesItemNumber()), Integer.valueOf(e2.getSalesItemNumber())));

		updateSalesOrderEntriesSalesAmount(target.getSalesOrderEntries(), target.getSalesOrderEntriesPromotions());

	}

	/**
	 *
	 */
	private void updateSalesOrderEntriesSalesAmount(final List<SalesOrderEntryBean> salesOrderEntries,
			final List<SalesOrderEntryPromotionBean> salesOrderEntriesPromotions)
	{

		if (CollectionUtils.isEmpty(salesOrderEntriesPromotions))
		{
			return;
		}

		for (final SalesOrderEntryBean entry : salesOrderEntries)
		{
			final SalesOrderEntryPromotionBean promotion = salesOrderEntriesPromotions.stream()
					.filter(e -> e.getItemID().equals(entry.getItemID())).findFirst().orElse(null);

			if (promotion == null || Strings.isBlank(entry.getItemID()))
			{
				continue;
			}

			final double originalPriceDiscount = getOriginalPriceDiscount(entry);
			entry.setSalesAmount(entry.getSalesAmount() - promotion.getDiscountAmount() + originalPriceDiscount);
		}

	}

	private double getOriginalPriceDiscount(final SalesOrderEntryBean entry)
	{
		double discountPricePer1Qty = entry.getOriginalPrice() - entry.getBasePrice();
		if ("KG".equals(entry.getUom()))
		{
			discountPricePer1Qty = (entry.getOriginalPrice() - (entry.getBasePrice() * 1000)) / 1000;
		}
		return discountPricePer1Qty * entry.getQuantity();
	}

	/**
	 *
	 */
	private String getOrderCoupons(final AbstractOrderModel order)
	{
		final Collection<String> appliedCouponCodes = order.getAppliedCouponCodes();
		if (CollectionUtils.isEmpty(appliedCouponCodes))
		{
			return "";
		}
		return String.join(", ", appliedCouponCodes);
	}

	/**
	 *
	 */
	private Double getOrderDiscounts(final AbstractOrderModel order)
	{
		return order.getTotalDiscounts() == null ? 0.0d : order.getTotalDiscounts().doubleValue();
	}

	/**
	 * @return the consignmentEntryToSalesOrderPromotionEntry
	 */
	protected Populator<ConsignmentModel, List<SalesOrderEntryPromotionBean>> getConsignmentToSalesOrderPromotionEntries()
	{
		return consignmentToSalesOrderPromotionEntries;
	}

	/**
	 * @return the consignmentEntryToSalesOrderEntry
	 */
	protected Populator<ConsignmentModel, List<SalesOrderEntryBean>> getConsignmentToSalesOrderEntry()
	{
		return consignmentToSalesOrderEntry;
	}

	/**
	 * @return the consignmentEntryToSalesOrderTaxEntry
	 */
	protected Populator<ConsignmentModel, List<SalesOrderEntryTaxBean>> getConsignmentToSalesOrderTaxEntry()
	{
		return consignmentToSalesOrderTaxEntry;
	}

	/**
	 * @return the consignmentEntryToSalesOrderTenderPopulator
	 */
	protected Populator<ConsignmentModel, List<SalesOrderTenderBean>> getConsignmentToSalesOrderTenderPopulator()
	{
		return consignmentToSalesOrderTenderPopulator;
	}

}
