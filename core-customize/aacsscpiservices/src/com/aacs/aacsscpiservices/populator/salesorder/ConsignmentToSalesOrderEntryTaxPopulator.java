/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.populator.salesorder;

import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.TaxValue;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.aacs.aacsscpiservices.beans.salesorder.SalesOrderEntryTaxBean;
import com.google.common.base.Preconditions;


/**
 * @author husam.dababneh@erabia.com
 */
public class ConsignmentToSalesOrderEntryTaxPopulator implements Populator<ConsignmentModel, List<SalesOrderEntryTaxBean>>
{

	@Resource(name = "ordermanagementConsignmentEntryConverter")
	private Converter<ConsignmentEntryModel, ConsignmentEntryData> consignmentEntryConverter;

	@Override
	public void populate(final ConsignmentModel consignment, List<SalesOrderEntryTaxBean> target) throws ConversionException
	{
		if (target == null)
		{
			target = new ArrayList<>();
		}
		Preconditions.checkNotNull(consignment, "Consignment Is null");
		Preconditions.checkNotNull(consignment.getWarehouse(), "Warehouse Is null");
		Preconditions.checkNotNull(consignment.getOrder(), "Order Is null for [" + consignment.getCode() + "] consignemnt");
		Preconditions.checkNotNull(consignment.getOrder().getUser(),
				"User Is null for [" + consignment.getCode() + "] consignemnt");

		final AbstractOrderModel order = consignment.getOrder();
		final CustomerModel customer = (CustomerModel) order.getUser();

		final SimpleDateFormat orderDateFormat = new SimpleDateFormat("yyyyMMdd");

		final List<ConsignmentEntryModel> arrayList = consignment.getConsignmentEntries().stream()
				.sorted((arg0, arg1) -> Integer.compare(arg0.getOrderEntry().getEntryNumber(), arg1.getOrderEntry().getEntryNumber()))
				.collect(Collectors.toList());

		for (final ConsignmentEntryModel source : arrayList)
		{
			final SalesOrderEntryTaxBean taxBean = new SalesOrderEntryTaxBean();
			final double taxValue = source.getOrderEntry().getTaxValues().stream().mapToDouble(TaxValue::getAppliedValue).sum();
			taxBean.setWarehouseID(String.valueOf(consignment.getWarehouse().getCode()));
			taxBean.setOrderDate(orderDateFormat.format(order.getDate()));
			taxBean.setTransactionType(customer.isAacsCustomer() ? "1001" : "1004");
			taxBean.setTransactionNumber(String.valueOf(order.getCode()));
			taxBean.setSalesItemNumber(String.valueOf(source.getOrderEntry().getEntryNumber()));
			taxBean.setTaxSequenceNumber(String.valueOf(target.size()));
			taxBean.setTaxTypeCode(taxValue > 0.0 ? "1301" : "2301");
			taxBean.setTaxAmount(String.valueOf(taxValue));

			target.add(taxBean);
		}
	}

	/**
	 * @return the consignmentEntryConverter
	 */
	protected Converter<ConsignmentEntryModel, ConsignmentEntryData> getConsignmentEntryConverter()
	{
		return consignmentEntryConverter;
	}



}
