/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.populator.salesorder;

import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;

import com.aacs.aacsscpiservices.beans.salesorder.SalesOrderEntryBean;
import com.google.common.base.Preconditions;


/**
 * @author husam.dababneh@erabia.com
 */
public class ConsignmentToSalesOrderEntryPopulator implements Populator<ConsignmentModel, List<SalesOrderEntryBean>>
{

	private final SimpleDateFormat orderDateFormat = new SimpleDateFormat("yyyyMMdd");
	private final SimpleDateFormat timestampFormat = new SimpleDateFormat("yyyyMMddhhmmss");

	@Resource(name = "ordermanagementConsignmentEntryConverter")
	private Converter<ConsignmentEntryModel, ConsignmentEntryData> consignmentEntryConverter;


	@Override
	public void populate(final ConsignmentModel source, final List<SalesOrderEntryBean> target) throws ConversionException
	{
		final ConsignmentModel consignment = source;
		Preconditions.checkNotNull(consignment, "Consignment Is null");
		Preconditions.checkNotNull(consignment.getWarehouse(), "Warehouse Is null");
		Preconditions.checkNotNull(consignment.getOrder(), "Order Is null for [" + consignment.getCode() + "] consignemnt");
		Preconditions.checkNotNull(consignment.getOrder().getUser(),
				"User Is null for [" + consignment.getCode() + "] consignemnt");
		final AbstractOrderModel order = consignment.getOrder();
		final CustomerModel customer = (CustomerModel) order.getUser();


		final List<ConsignmentEntryModel> arrayList = consignment.getConsignmentEntries().stream()
				.sorted((arg0, arg1) -> Integer.compare(arg0.getOrderEntry().getEntryNumber(), arg1.getOrderEntry().getEntryNumber()))
				.collect(Collectors.toList());

		int salesItemNumber = 0;
		for (final ConsignmentEntryModel consignmentEntry : arrayList)
		{
			final ConsignmentEntryData consignmentEntryData = consignmentEntryConverter.convert(consignmentEntry);
			final AbstractOrderEntryModel orderEntry = consignmentEntry.getOrderEntry();
			final ProductModel product = orderEntry.getProduct();
			final SalesOrderEntryBean bean = new SalesOrderEntryBean();
			bean.setWarehouseID(String.valueOf(consignment.getWarehouse().getCode()));
			bean.setOrderDate(orderDateFormat.format(order.getDate()));
			bean.setTransactionType(customer.isAacsCustomer() ? "1001" : "1004");
			bean.setTransactionNumber(String.valueOf(order.getCode()));
			bean.setSalesItemNumber(String.valueOf(salesItemNumber));
			bean.setSalesItemType(customer.isAacsCustomer() ? "2001" : "2101");
			bean.setItemIDQualifier("1");
			bean.setBasePrice(getBasePrice(consignmentEntryData));
			bean.setOriginalPrice(orderEntry.getPriceRowPromotionOriginalPrice());
			bean.setItemID(getProductID(product));
			bean.setSapBaseUnitConversion(
					product.getSapBaseUnitConversion() != null ? String.valueOf(product.getSapBaseUnitConversion()) : "");
			bean.setQuantity(consignmentEntry.getQuantity());
			bean.setUom(String.valueOf(product.getUnit() == null ? "" : product.getUnit().getCode()));
			bean.setSalesAmount(orderEntry.getTotalPrice());
			bean.setScanTime(timestampFormat.format(order.getDate()));
			bean.setPromitionID(orderEntry.getPriceRowPromotionId());
			target.add(bean);
			salesItemNumber += 1;
		}

		addDeliveryCharges(source, customer, target, salesItemNumber);
	}

	/**
	 *
	 */
	private void addDeliveryCharges(final ConsignmentModel consignment, final CustomerModel customer,
			final List<SalesOrderEntryBean> target, final int salesItemNumber)
	{
		final SalesOrderEntryBean bean = new SalesOrderEntryBean();
		bean.setWarehouseID(String.valueOf(consignment.getWarehouse().getCode()));
		bean.setOrderDate(orderDateFormat.format(consignment.getOrder().getDate()));
		bean.setTransactionType(customer.isAacsCustomer() ? "1001" : "1004");
		bean.setTransactionNumber(String.valueOf(consignment.getOrder().getCode()));
		bean.setSalesItemNumber(String.valueOf(salesItemNumber));
		bean.setSalesItemType(customer.isAacsCustomer() ? "2001" : "2101");
		bean.setItemIDQualifier("1");
		bean.setBasePrice(0.0d);
		bean.setOriginalPrice(0.0d);
		bean.setSapBaseUnitConversion(String.valueOf(1.0));
		bean.setQuantity(1);
		bean.setUom("EA");
		bean.setSalesAmount(getOrderDeliveryCost(consignment));
		bean.setScanTime(timestampFormat.format(consignment.getOrder().getDate()));
		bean.setItemID("");
		bean.setPromitionID("");

		target.add(bean);
	}

	private double getOrderDeliveryCost(final ConsignmentModel consignment)
	{
		return consignment.getOrder().getDeliveryCost() == null ? 0.0d : consignment.getOrder().getDeliveryCost();
	}

	/**
	 *
	 */
	private double getBasePrice(final ConsignmentEntryData consignmentEntryData)
	{
		if (consignmentEntryData == null || consignmentEntryData.getOrderEntry() == null
				|| consignmentEntryData.getOrderEntry().getBasePrice() == null)
		{
			return 0.0d;
		}

		return consignmentEntryData.getOrderEntry().getBasePrice().getValue().doubleValue();
	}

	/**
	 *
	 */
	private String getProductID(final ProductModel product)
	{
		if (product == null || Strings.isBlank(product.getCode()))
		{
			return "";
		}

		final String[] split = product.getCode().split("_");
		return split[0];
	}


}

