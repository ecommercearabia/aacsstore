/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.populator.salesorder;

import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.commerceservices.strategies.NetGrossStrategy;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.product.PriceService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.util.DiscountValue;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.util.Strings;

import com.aacs.aacsscpiservices.beans.salesorder.SalesOrderEntryPromotionBean;
import com.aacs.core.service.CustomCommercePriceService;
import com.google.common.base.Preconditions;


/**
 * @author husam.dababneh@erabia.com
 */
public class ConsignmentToSalesOrderEntryPromotionPopulator
		implements Populator<ConsignmentModel, List<SalesOrderEntryPromotionBean>>
{

	final SimpleDateFormat orderDateFormat = new SimpleDateFormat("yyyyMMdd");

	@Resource(name = "netGrossStrategy")
	private NetGrossStrategy netGrossStrategy;

	/**
	 * @return the netGrossStrategy
	 */
	protected NetGrossStrategy getNetGrossStrategy()
	{
		return netGrossStrategy;
	}

	/**
	 * @return the timeService
	 */
	protected TimeService getTimeService()
	{
		return timeService;
	}

	@Resource(name = "timeService")
	private TimeService timeService;

	@Resource(name = "ordermanagementConsignmentEntryConverter")
	private Converter<ConsignmentEntryModel, ConsignmentEntryData> consignmentEntryConverter;

	@Resource(name = "priceService")
	private PriceService priceService;

	@Resource(name = "customCommercePriceService")
	private CustomCommercePriceService customCommercePriceService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public void populate(final ConsignmentModel consignment, List<SalesOrderEntryPromotionBean> target) throws ConversionException
	{
		if (target == null)
		{
			target = new ArrayList<>();
		}
		Preconditions.checkNotNull(consignment, "Consignment Is null");
		Preconditions.checkNotNull(consignment.getWarehouse(), "Warehouse Is null");
		final AbstractOrderModel order = consignment.getOrder();
		Preconditions.checkNotNull(order, "Order Is null for [" + consignment.getCode() + "] consignemnt");
		Preconditions.checkNotNull(order.getUser(), "User Is null for [" + consignment.getCode() + "] consignemnt");
		final CustomerModel customer = (CustomerModel) order.getUser();


		final List<ConsignmentEntryModel> arrayList = consignment.getConsignmentEntries().stream()
				.sorted((arg0, arg1) -> Integer.compare(arg0.getOrderEntry().getEntryNumber(), arg1.getOrderEntry().getEntryNumber()))
				.collect(Collectors.toList());

		int count = 0;
		for (final ConsignmentEntryModel source : arrayList)
		{
			final ConsignmentEntryData consignmentData = consignmentEntryConverter.convert(source);
			final AbstractOrderEntryModel orderEntry = source.getOrderEntry();
			final ProductModel product = orderEntry.getProduct();
			final double totalDiscountValue = calculateDiscounts(source, orderEntry);
			if (totalDiscountValue == 0.0d)
			{
				continue;
			}
			final SalesOrderEntryPromotionBean promotionEntry = new SalesOrderEntryPromotionBean();
			promotionEntry.setWarehouseID(String.valueOf(consignment.getWarehouse().getCode()));
			promotionEntry.setOrderDate(orderDateFormat.format(order.getDate()));
			promotionEntry.setTransactionType(customer.isAacsCustomer() ? "1001" : "1004");
			promotionEntry.setItemID(getProductID(product));
			promotionEntry.setTransactionNumber(String.valueOf(order.getCode()));
			promotionEntry.setRetailSequenceNumber(String.valueOf(count));
			promotionEntry.setDiscountAmount(totalDiscountValue);
			promotionEntry.setDiscountTypeCode("ZPDI");
			promotionEntry.setDiscountSequenceNumber(String.valueOf(target.size()));
			promotionEntry.setBonusBuyID("Discount");
			promotionEntry.setOriginalPrice(getBasePrice(consignmentData));
			target.add(promotionEntry);
			count += 1;
		}

	}

	/**
	 *
	 */
	private String getProductID(final ProductModel product)
	{
		if (product == null || Strings.isBlank(product.getCode()))
		{
			return "";
		}

		final String[] split = product.getCode().split("_");
		return split[0];
	}

	private double calculateDiscounts(final ConsignmentEntryModel source, final AbstractOrderEntryModel orderEntry)
	{
		double totalDiscountValue = 0.0d;
		totalDiscountValue += addEntryDiscount(orderEntry);
		totalDiscountValue += addOriginalPricePromotion(source, orderEntry);
		//		totalDiscountValue += addOrderDiscount(source);

		return totalDiscountValue;
	}


	private static double formatDouble(final double value)
	{
		final DecimalFormat df = new DecimalFormat("#.00");
		return Double.valueOf(df.format(value));
	}

	private double addOrderDiscount(final ConsignmentEntryModel source)
	{
		final ConsignmentModel consignment = source.getConsignment();
		final double orderDiscount = getOrderDiscount(consignment);
		if (orderDiscount == 0.0d)
		{
			return 0.d;
		}


		final double totalValueOfEntries = consignmentEntryConverter.convertAll(consignment.getConsignmentEntries()).stream()
				.map(e -> e.getOrderEntry().getTotalPrice().getValue().doubleValue()).reduce(0.0d, Double::sum).doubleValue();

		final double discountRate = orderDiscount / totalValueOfEntries;

		return discountRate * source.getOrderEntry().getTotalPrice();
	}

	private double addOriginalPricePromotion(final ConsignmentEntryModel source, final AbstractOrderEntryModel orderEntry)
	{
		if (!orderEntry.isPriceRowPromotion())
		{
			return 0.0d;
		}

		final double qty = orderEntry.getUnit() != null && "KG".equals(orderEntry.getUnit().getCode())
				? source.getQuantity() / 1000.d
				: source.getQuantity().doubleValue();

		return (orderEntry.getPriceRowPromotionOriginalPrice() - orderEntry.getPriceRowPromotionPrice()) * qty;
	}

	private double addEntryDiscount(final AbstractOrderEntryModel orderEntry)
	{
		if (!CollectionUtils.isEmpty(orderEntry.getDiscountValues()))
		{
			return 0.0d;
		}
		return orderEntry.getDiscountValues().stream().map(DiscountValue::getAppliedValue).reduce(0.0d, Double::sum).doubleValue();
	}

	/**
	 *
	 */
	private double getOrderDiscount(final ConsignmentModel consignment)
	{
		return consignment.getOrder().getTotalDiscounts() == null ? 0.0d : consignment.getOrder().getTotalDiscounts().doubleValue();
	}

	private String getBasePrice(final ConsignmentEntryData consignmentEntryData)
	{
		if (consignmentEntryData == null || consignmentEntryData.getOrderEntry() == null
				|| consignmentEntryData.getOrderEntry().getBasePrice() == null)
		{
			return "0";
		}

		return String.valueOf(consignmentEntryData.getOrderEntry().getBasePrice().getValue());
	}

	/**
	 * @return the customPriceService
	 */
	public PriceService getCustomPriceService()
	{
		return priceService;
	}

	/**
	 * @return the customCommercePriceService
	 */
	public CustomCommercePriceService getCustomCommercePriceService()
	{
		return customCommercePriceService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

}
