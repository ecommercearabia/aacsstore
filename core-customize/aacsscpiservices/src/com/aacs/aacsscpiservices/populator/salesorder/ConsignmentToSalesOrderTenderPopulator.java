/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.populator.salesorder;

import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.aacs.aacsloyaltyprogramprovider.enums.LoyaltyPaymentStatus;
import com.aacs.aacsloyaltyprogramprovider.enums.LoyaltyPaymentType;
import com.aacs.aacsscpiservices.beans.salesorder.SalesOrderTenderBean;
import com.google.common.base.Preconditions;


/**
 * @author husam.dababneh@erabia.com
 */
public class ConsignmentToSalesOrderTenderPopulator implements Populator<ConsignmentModel, List<SalesOrderTenderBean>>
{

	@Resource(name = "ordermanagementConsignmentEntryConverter")
	private Converter<ConsignmentModel, ConsignmentEntryData> consignmentEntryConverter;

	final SimpleDateFormat orderDateFormat = new SimpleDateFormat("yyyyMMdd");


	@Override
	public void populate(final ConsignmentModel consignment, List<SalesOrderTenderBean> target) throws ConversionException
	{

		if (target == null)
		{
			target = new ArrayList<>();
		}

		Preconditions.checkNotNull(consignment, "Consignment Is null");
		Preconditions.checkNotNull(consignment.getWarehouse(), "Warehouse Is null");
		Preconditions.checkNotNull(consignment.getOrder(), "Order Is null for [" + consignment.getCode() + "] consignemnt");
		Preconditions.checkNotNull(consignment.getOrder().getUser(),
				"User Is null for [" + consignment.getCode() + "] consignemnt");



		final AbstractOrderModel order = consignment.getOrder();


		populateDefaultPaymentMode(target, consignment, order);
		populateStoreCreditMode(target, consignment, order);
		populateLoyaltyMode(target, consignment, order);
		populateLoyaltyModeRedemedPoints(target, consignment, order);
		populateTotalDiscounts(target, consignment, order);

	}

	/**
	 * @param consignment
	 *
	 */
	private void populateDefaultPaymentMode(final List<SalesOrderTenderBean> target, final ConsignmentModel consignment,
			final AbstractOrderModel order)
	{

		final String typeCode = order.getPaymentMode() == null || order.getPaymentMode().getCode() == null ? ""
				: order.getPaymentMode().getCode();

		final SalesOrderTenderBean tender = new SalesOrderTenderBean();
		tender.setTenderSequenceNumber(String.valueOf(target.size()));


		switch (typeCode.toLowerCase())
		{
			case "continue":
				return;
			case "cod":
				tender.setTenderTypeCode("ZTCS");
				break;
			case "card":
				tender.setTenderTypeCode("ZTVI");
				break;
			case "ccod":
				tender.setTenderTypeCode("ZTCS");
				break;
			default:
				throw new IllegalArgumentException(
						"Unknown Payment Mode[" + typeCode.toLowerCase() + "] in Order[" + order.getCode() + "]");
		}


		tender.setWarehouseID(String.valueOf(consignment.getWarehouse().getCode()));
		tender.setOrderDate(orderDateFormat.format(order.getDate()));
		tender.setTenderAmount(String.valueOf(order.getTotalPrice()));
		tender.setTenderCurrency(String.valueOf(order.getCurrency() == null ? "" : order.getCurrency().getIsocode()));
		tender.setReferenceID("");
		tender.setTransactionNumber(String.valueOf(order.getCode()));
		target.add(tender);

	}

	private void populateLoyaltyMode(final List<SalesOrderTenderBean> target, final ConsignmentModel consignment,
			final AbstractOrderModel order)
	{
		if (order.getLoyaltyAmount() != 0.0 && LoyaltyPaymentType.CAPTURED.equals(order.getLoyaltyPaymentType())
				&& LoyaltyPaymentStatus.SUCCESS.equals(order.getLoyaltyPaymentStatus()))
		{
			final SalesOrderTenderBean tender = new SalesOrderTenderBean();
			tender.setTenderSequenceNumber(String.valueOf(target.size()));
			tender.setTenderTypeCode("ZLOP");
			tender.setTenderAmount(String.valueOf(order.getLoyaltyAmount()));
			tender.setTenderCurrency(String.valueOf(order.getCurrency() == null ? "" : order.getCurrency().getIsocode()));
			tender.setWarehouseID(String.valueOf(consignment.getWarehouse().getCode()));
			tender.setOrderDate(orderDateFormat.format(order.getDate()));
			tender.setTransactionNumber(String.valueOf(order.getCode()));
			tender.setReferenceID("");
			target.add(tender);
		}
	}


	private void populateTotalDiscounts(final List<SalesOrderTenderBean> target, final ConsignmentModel consignment,
			final AbstractOrderModel order)
	{
		if (order.getTotalDiscounts() == null || order.getTotalDiscounts() == 0.0d)
		{
			return;
		}
		final SalesOrderTenderBean tender = new SalesOrderTenderBean();
		tender.setTenderSequenceNumber(String.valueOf(target.size()));
		tender.setTenderTypeCode("ZCOU");
		tender.setTenderAmount(String.valueOf(order.getTotalDiscounts()));
		tender.setTenderCurrency(String.valueOf(order.getCurrency() == null ? "" : order.getCurrency().getIsocode()));
		tender.setWarehouseID(String.valueOf(consignment.getWarehouse().getCode()));
		tender.setOrderDate(orderDateFormat.format(order.getDate()));
		tender.setTransactionNumber(String.valueOf(order.getCode()));
		tender.setReferenceID("");
		target.add(tender);
	}

	private void populateLoyaltyModeRedemedPoints(final List<SalesOrderTenderBean> target, final ConsignmentModel consignment,
			final AbstractOrderModel order)
	{
		//		if (order.getExpectedAddedPoint() != null && order.getExpectedAddedPoint() != 0.0
		//				&& LoyaltyPaymentType.CAPTURED.equals(order.getLoyaltyPaymentType())
		//				&& LoyaltyPaymentStatus.SUCCESS.equals(order.getLoyaltyPaymentStatus()))
		//		{
		//			final SalesOrderTenderBean tender = new SalesOrderTenderBean();
		//			tender.setTenderSequenceNumber(String.valueOf(target.size()));
		//			tender.setTenderTypeCode("ZPL2");
		//			tender.setTenderAmount(String.valueOf(order.getExpectedAddedPoint()));
		//			tender.setTenderCurrency(String.valueOf(order.getCurrency() == null ? "" : order.getCurrency().getIsocode()));
		//			tender.setWarehouseID(String.valueOf(consignment.getWarehouse().getCode()));
		//			tender.setOrderDate(orderDateFormat.format(order.getDate()));
		//			tender.setTransactionNumber(String.valueOf(order.getCode()));
		//			tender.setReferenceID("");
		//			target.add(tender);
		//		}
	}

	private void populateStoreCreditMode(final List<SalesOrderTenderBean> target, final ConsignmentModel consignment,
			final AbstractOrderModel order)
	{
		if (order.getStoreCreditAmount() != 0.0)
		{
			final SalesOrderTenderBean tender = new SalesOrderTenderBean();
			tender.setTenderSequenceNumber(String.valueOf(target.size()));
			tender.setTenderTypeCode("ZTBL");
			tender.setTenderAmount(String.valueOf(order.getStoreCreditAmount().doubleValue()));
			tender.setTenderCurrency(String.valueOf(order.getCurrency() == null ? "" : order.getCurrency().getIsocode()));
			tender.setWarehouseID(String.valueOf(consignment.getWarehouse().getCode()));
			tender.setOrderDate(orderDateFormat.format(order.getDate()));
			tender.setTransactionNumber(String.valueOf(order.getCode()));
			tender.setReferenceID("");
			target.add(tender);
		}
	}



}
