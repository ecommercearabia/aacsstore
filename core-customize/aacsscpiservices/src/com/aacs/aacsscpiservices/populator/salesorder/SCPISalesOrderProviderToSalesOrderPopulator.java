/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.populator.salesorder;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.aacs.aacsscpiservices.beans.salesorder.SalesOrderBean;
import com.aacs.aacsscpiservices.beans.salesorder.SalesOrderEntryBean;
import com.aacs.aacsscpiservices.beans.salesorder.SalesOrderEntryPromotionBean;
import com.aacs.aacsscpiservices.beans.salesorder.SalesOrderEntryTaxBean;
import com.aacs.aacsscpiservices.model.SCPISalesOrderServiceProviderModel;


/**
 * @author husam.dababneh@erabia.com
 */
public class SCPISalesOrderProviderToSalesOrderPopulator implements Populator<SCPISalesOrderServiceProviderModel, SalesOrderBean>
{

	@Override
	public void populate(final SCPISalesOrderServiceProviderModel source, final SalesOrderBean target) throws ConversionException
	{
		final String posNumber = String.valueOf(source.getWorkstationID());

		target.setPosNumber(posNumber);
		target.setOperatorQualifier(source.getOperatorQualifier() != null ? String.valueOf(source.getOperatorQualifier()) : "");
		target.setOperatorID(String.valueOf(source.getOperatorID()));
		target.setTransactionCurrency(source.getTransactionCurrency() == null ? "" : source.getTransactionCurrency().getIsocode());

		for (final SalesOrderEntryBean salesOrderEntryBean : target.getSalesOrderEntries())
		{
			salesOrderEntryBean.setPosNumber(posNumber);
			salesOrderEntryBean.setItemIDEntryMethodCode(String.valueOf(source.getItemIDEntryMethodCode()));
		}
		for (final SalesOrderEntryTaxBean salesOrderEntryTaxBean : target.getSalesOrderEntriesTaxes())
		{
			salesOrderEntryTaxBean.setPosNumber(posNumber);
		}
		for (final SalesOrderEntryPromotionBean salesOrderEntryPromotionBean : target.getSalesOrderEntriesPromotions())
		{
			salesOrderEntryPromotionBean.setPosNumber(posNumber);
		}

	}

}
