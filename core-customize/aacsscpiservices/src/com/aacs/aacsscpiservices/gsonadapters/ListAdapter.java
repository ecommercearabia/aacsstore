/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.gsonadapters;

import java.lang.reflect.Type;
import java.util.List;

import org.apache.poi.ss.formula.functions.T;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;


/**
 *
 */
public class ListAdapter implements JsonSerializer<List<T>>
{

	@Override
	public JsonElement serialize(final List<T> data, final Type type, final JsonSerializationContext context)
	{
		if (data == null || data.isEmpty())
		{
			return null;
		}
		final JsonArray array = new JsonArray();

		for (final Object child : data)
		{
			final JsonElement element = context.serialize(child);
			array.add(element);
		}

		return array;


	}

}
