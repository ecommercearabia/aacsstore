/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.constants;

/**
 * Global class for all Aacsscpiservices constants. You can add global constants for your extension into this class.
 */
public final class AacsscpiservicesConstants extends GeneratedAacsscpiservicesConstants
{
	public static final String EXTENSIONNAME = "aacsscpiservices";

	private AacsscpiservicesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
