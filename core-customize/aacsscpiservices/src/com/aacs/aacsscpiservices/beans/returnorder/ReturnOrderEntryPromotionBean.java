/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.beans.returnorder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class ReturnOrderEntryPromotionBean
{
	@Expose
	@SerializedName("warehouseID")
	private String warehouseID;

	@Expose
	@SerializedName("orderDate")
	private String orderDate;

	@Expose
	@SerializedName("transactionType")
	private String transactionType;

	@Expose
	@SerializedName("posNumber")
	private String posNumber;

	@Expose
	@SerializedName("transactionNumber")
	private String transactionNumber;

	@Expose
	@SerializedName("retailsequencenumber")
	private String retailSequenceNumber;

	@Expose
	@SerializedName("discountSequenceNumber")
	private String discountSequenceNumber;

	@Expose
	@SerializedName("discountTypeCode")
	private String discountTypeCode;

	@Expose
	@SerializedName("discountAmount")
	private String discountAmount;

	@Expose
	@SerializedName("bonusBuyID")
	private String bonusBuyID;

	@Expose
	@SerializedName("basePrice")
	private String basePrice;

	@Expose
	@SerializedName("originalPrice")
	private String originalPrice;

	/**
	 * @return the warehouseID
	 */
	public String getWarehouseID()
	{
		return warehouseID;
	}

	/**
	 * @param warehouseID
	 *           the warehouseID to set
	 */
	public void setWarehouseID(final String warehouseID)
	{
		this.warehouseID = warehouseID;
	}

	/**
	 * @return the orderDate
	 */
	public String getOrderDate()
	{
		return orderDate;
	}

	/**
	 * @param orderDate
	 *           the orderDate to set
	 */
	public void setOrderDate(final String orderDate)
	{
		this.orderDate = orderDate;
	}

	/**
	 * @return the transactionType
	 */
	public String getTransactionType()
	{
		return transactionType;
	}

	/**
	 * @param transactionType
	 *           the transactionType to set
	 */
	public void setTransactionType(final String transactionType)
	{
		this.transactionType = transactionType;
	}

	/**
	 * @return the posNumber
	 */
	public String getPosNumber()
	{
		return posNumber;
	}

	/**
	 * @param posNumber
	 *           the posNumber to set
	 */
	public void setPosNumber(final String posNumber)
	{
		this.posNumber = posNumber;
	}

	/**
	 * @return the transactionNumber
	 */
	public String getTransactionNumber()
	{
		return transactionNumber;
	}

	/**
	 * @param transactionNumber
	 *           the transactionNumber to set
	 */
	public void setTransactionNumber(final String transactionNumber)
	{
		this.transactionNumber = transactionNumber;
	}



	/**
	 * @return the retailSequenceNumber
	 */
	public String getRetailSequenceNumber()
	{
		return retailSequenceNumber;
	}

	/**
	 * @param retailSequenceNumber
	 *           the retailSequenceNumber to set
	 */
	public void setRetailSequenceNumber(final String retailSequenceNumber)
	{
		this.retailSequenceNumber = retailSequenceNumber;
	}

	/**
	 * @return the discountSequenceNumber
	 */
	public String getDiscountSequenceNumber()
	{
		return discountSequenceNumber;
	}

	/**
	 * @param discountSequenceNumber
	 *           the discountSequenceNumber to set
	 */
	public void setDiscountSequenceNumber(final String discountSequenceNumber)
	{
		this.discountSequenceNumber = discountSequenceNumber;
	}

	/**
	 * @return the discountTypeCode
	 */
	public String getDiscountTypeCode()
	{
		return discountTypeCode;
	}

	/**
	 * @param discountTypeCode
	 *           the discountTypeCode to set
	 */
	public void setDiscountTypeCode(final String discountTypeCode)
	{
		this.discountTypeCode = discountTypeCode;
	}

	/**
	 * @return the discountAmount
	 */
	public String getDiscountAmount()
	{
		return discountAmount;
	}

	/**
	 * @param discountAmount
	 *           the discountAmount to set
	 */
	public void setDiscountAmount(final String discountAmount)
	{
		this.discountAmount = discountAmount;
	}

	/**
	 * @return the bonusBuyID
	 */
	public String getBonusBuyID()
	{
		return bonusBuyID;
	}

	/**
	 * @param bonusBuyID
	 *           the bonusBuyID to set
	 */
	public void setBonusBuyID(final String bonusBuyID)
	{
		this.bonusBuyID = bonusBuyID;
	}

	/**
	 * @return the basePrice
	 */
	public String getBasePrice()
	{
		return basePrice;
	}

	/**
	 * @param basePrice
	 *           the basePrice to set
	 */
	public void setBasePrice(final String basePrice)
	{
		this.basePrice = basePrice;
	}

	/**
	 * @return the originalPrice
	 */
	public String getOriginalPrice()
	{
		return originalPrice;
	}

	/**
	 * @param originalPrice
	 *           the originalPrice to set
	 */
	public void setOriginalPrice(final String originalPrice)
	{
		this.originalPrice = originalPrice;
	}



}
