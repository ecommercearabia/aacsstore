/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.beans.returnorder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class ReturnOrderTenderBean
{
	@Expose
	@SerializedName("warehouseID")
	private String warehouseID;

	@Expose
	@SerializedName("orderDate")
	private String orderDate;

	@Expose
	@SerializedName("tenderSequenceNumber")
	private String tenderSequenceNumber;

	@Expose
	@SerializedName("tenderTypeCode")
	private String tenderTypeCode;

	@Expose
	@SerializedName("tenderAmount")
	private String tenderAmount;

	@Expose
	@SerializedName("tenderCurrency")
	private String tenderCurrency;

	@Expose
	@SerializedName("referenceID")
	private String referenceID;

	@Expose
	@SerializedName("transactionNumber")
	private String transactionNumber;

	/**
	 * @return the tenderSequenceNumber
	 */
	public String getTenderSequenceNumber()
	{
		return tenderSequenceNumber;
	}

	/**
	 * @param tenderSequenceNumber
	 *           the tenderSequenceNumber to set
	 */
	public void setTenderSequenceNumber(final String tenderSequenceNumber)
	{
		this.tenderSequenceNumber = tenderSequenceNumber;
	}

	/**
	 * @return the tenderTypeCode
	 */
	public String getTenderTypeCode()
	{
		return tenderTypeCode;
	}

	/**
	 * @param tenderTypeCode
	 *           the tenderTypeCode to set
	 */
	public void setTenderTypeCode(final String tenderTypeCode)
	{
		this.tenderTypeCode = tenderTypeCode;
	}

	/**
	 * @return the tenderAmount
	 */
	public String getTenderAmount()
	{
		return tenderAmount;
	}

	/**
	 * @param tenderAmount
	 *           the tenderAmount to set
	 */
	public void setTenderAmount(final String tenderAmount)
	{
		this.tenderAmount = tenderAmount;
	}

	/**
	 * @return the tenderCurrency
	 */
	public String getTenderCurrency()
	{
		return tenderCurrency;
	}

	/**
	 * @param tenderCurrency
	 *           the tenderCurrency to set
	 */
	public void setTenderCurrency(final String tenderCurrency)
	{
		this.tenderCurrency = tenderCurrency;
	}

	/**
	 * @return the referenceID
	 */
	public String getReferenceID()
	{
		return referenceID;
	}

	/**
	 * @param referenceID
	 *           the referenceID to set
	 */
	public void setReferenceID(final String referenceID)
	{
		this.referenceID = referenceID;
	}

	/**
	 * @return the warehouseID
	 */
	public String getWarehouseID()
	{
		return warehouseID;
	}

	/**
	 * @param warehouseID
	 *           the warehouseID to set
	 */
	public void setWarehouseID(final String warehouseID)
	{
		this.warehouseID = warehouseID;
	}

	/**
	 * @return the orderDate
	 */
	public String getOrderDate()
	{
		return orderDate;
	}

	/**
	 * @param orderDate
	 *           the orderDate to set
	 */
	public void setOrderDate(final String orderDate)
	{
		this.orderDate = orderDate;
	}

	/**
	 * @return the transactionNumber
	 */
	public String getTransactionNumber()
	{
		return transactionNumber;
	}

	/**
	 * @param transactionNumber
	 *           the transactionNumber to set
	 */
	public void setTransactionNumber(final String transactionNumber)
	{
		this.transactionNumber = transactionNumber;
	}



}
