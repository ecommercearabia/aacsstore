/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.beans.returnorder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class ReturnOrderEntryBean
{
	@Expose
	@SerializedName("warehouseID")
	private String warehouseID;

	@Expose
	@SerializedName("orderDate")
	private String orderDate;

	@Expose
	@SerializedName("transactionType")
	private String transactionType;

	@Expose
	@SerializedName("posNumber")
	private String posNumber;

	@Expose
	@SerializedName("transactionNumber")
	private String transactionNumber;

	@Expose
	@SerializedName("salesItemNumber")
	private String salesItemNumber;

	@Expose
	@SerializedName("salesItemType")
	private String salesItemType;

	@Expose
	@SerializedName("itemIDQualifier")
	private String itemIDQualifier;

	@Expose
	@SerializedName("itemID")
	private String itemID;

	@Expose
	@SerializedName("quantity")
	private String quantity;

	@Expose
	@SerializedName("uom")
	private String uom;

	@Expose
	@SerializedName("sapBaseUnitConversion")
	private String sapBaseUnitConversion;

	@Expose
	@SerializedName("salesAmount")
	private String salesAmount;

	@Expose
	@SerializedName("itemIDEntryMethodCode")
	private String itemIDEntryMethodCode;

	@Expose
	@SerializedName("scanTime")
	private String scanTime;

	@Expose
	@SerializedName("promitionID")
	private String promitionID;


	@Expose
	@SerializedName("basePrice")
	private String basePrice;


	@Expose
	@SerializedName("originalPrice")
	private String originalPrice;



	/**
	 * @return the warehouseID
	 */
	public String getWarehouseID()
	{
		return warehouseID;
	}

	/**
	 * @return the uom
	 */
	public String getUom()
	{
		return uom;
	}

	/**
	 * @return the sapBaseUnitConversion
	 */
	public String getSapBaseUnitConversion()
	{
		return sapBaseUnitConversion;
	}

	/**
	 * @param sapBaseUnitConversion
	 *           the sapBaseUnitConversion to set
	 */
	public void setSapBaseUnitConversion(final String sapBaseUnitConversion)
	{
		this.sapBaseUnitConversion = sapBaseUnitConversion;
	}

	/**
	 * @param uom
	 *           the uom to set
	 */
	public void setUom(final String uom)
	{
		this.uom = uom;
	}

	/**
	 * @param warehouseID
	 *           the warehouseID to set
	 */
	public void setWarehouseID(final String warehouseID)
	{
		this.warehouseID = warehouseID;
	}

	/**
	 * @return the orderDate
	 */
	public String getOrderDate()
	{
		return orderDate;
	}

	/**
	 * @param orderDate
	 *           the orderDate to set
	 */
	public void setOrderDate(final String orderDate)
	{
		this.orderDate = orderDate;
	}

	/**
	 * @return the transactionType
	 */
	public String getTransactionType()
	{
		return transactionType;
	}

	/**
	 * @param transactionType
	 *           the transactionType to set
	 */
	public void setTransactionType(final String transactionType)
	{
		this.transactionType = transactionType;
	}

	/**
	 * @return the posNumber
	 */
	public String getPosNumber()
	{
		return posNumber;
	}

	/**
	 * @param posNumber
	 *           the posNumber to set
	 */
	public void setPosNumber(final String posNumber)
	{
		this.posNumber = posNumber;
	}

	/**
	 * @return the transactionNumber
	 */
	public String getTransactionNumber()
	{
		return transactionNumber;
	}

	/**
	 * @param transactionNumber
	 *           the transactionNumber to set
	 */
	public void setTransactionNumber(final String transactionNumber)
	{
		this.transactionNumber = transactionNumber;
	}

	/**
	 * @return the salesItemNumber
	 */
	public String getSalesItemNumber()
	{
		return salesItemNumber;
	}

	/**
	 * @param salesItemNumber
	 *           the salesItemNumber to set
	 */
	public void setSalesItemNumber(final String salesItemNumber)
	{
		this.salesItemNumber = salesItemNumber;
	}

	/**
	 * @return the salesItemType
	 */
	public String getSalesItemType()
	{
		return salesItemType;
	}

	/**
	 * @param salesItemType
	 *           the salesItemType to set
	 */
	public void setSalesItemType(final String salesItemType)
	{
		this.salesItemType = salesItemType;
	}

	/**
	 * @return the itemIDQualifier
	 */
	public String getItemIDQualifier()
	{
		return itemIDQualifier;
	}

	/**
	 * @param itemIDQualifier
	 *           the itemIDQualifier to set
	 */
	public void setItemIDQualifier(final String itemIDQualifier)
	{
		this.itemIDQualifier = itemIDQualifier;
	}

	/**
	 * @return the itemID
	 */
	public String getItemID()
	{
		return itemID;
	}

	/**
	 * @param itemID
	 *           the itemID to set
	 */
	public void setItemID(final String itemID)
	{
		this.itemID = itemID;
	}

	/**
	 * @return the quantity
	 */
	public String getQuantity()
	{
		return quantity;
	}

	/**
	 * @param quantity
	 *           the quantity to set
	 */
	public void setQuantity(final String quantity)
	{
		this.quantity = quantity;
	}

	/**
	 * @return the salesAmount
	 */
	public String getSalesAmount()
	{
		return salesAmount;
	}

	/**
	 * @param salesAmount
	 *           the salesAmount to set
	 */
	public void setSalesAmount(final String salesAmount)
	{
		this.salesAmount = salesAmount;
	}

	/**
	 * @return the itemIDEntryMethodCode
	 */
	public String getItemIDEntryMethodCode()
	{
		return itemIDEntryMethodCode;
	}

	/**
	 * @param itemIDEntryMethodCode
	 *           the itemIDEntryMethodCode to set
	 */
	public void setItemIDEntryMethodCode(final String itemIDEntryMethodCode)
	{
		this.itemIDEntryMethodCode = itemIDEntryMethodCode;
	}

	/**
	 * @return the scanTime
	 */
	public String getScanTime()
	{
		return scanTime;
	}

	/**
	 * @param scanTime
	 *           the scanTime to set
	 */
	public void setScanTime(final String scanTime)
	{
		this.scanTime = scanTime;
	}

	/**
	 * @return the promitionID
	 */
	public String getPromitionID()
	{
		return promitionID;
	}

	/**
	 * @param promitionID
	 *           the promitionID to set
	 */
	public void setPromitionID(final String promitionID)
	{
		this.promitionID = promitionID;
	}

	/**
	 * @return the basePrice
	 */
	public String getBasePrice()
	{
		return basePrice;
	}

	/**
	 * @param basePrice
	 *           the basePrice to set
	 */
	public void setBasePrice(final String basePrice)
	{
		this.basePrice = basePrice;
	}

	/**
	 * @return the originalPrice
	 */
	public String getOriginalPrice()
	{
		return originalPrice;
	}

	/**
	 * @param originalPrice
	 *           the originalPrice to set
	 */
	public void setOriginalPrice(final String originalPrice)
	{
		this.originalPrice = originalPrice;
	}



}
