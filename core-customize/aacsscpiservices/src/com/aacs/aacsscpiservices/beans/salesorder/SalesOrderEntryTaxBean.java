/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.beans.salesorder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class SalesOrderEntryTaxBean
{
	@Expose
	@SerializedName("warehouseID")
	private String warehouseID;

	@Expose
	@SerializedName("orderDate")
	private String orderDate;

	@Expose
	@SerializedName("transactionType")
	private String transactionType;

	@Expose
	@SerializedName("posNumber")
	private String posNumber;

	@Expose
	@SerializedName("transactionNumber")
	private String transactionNumber;

	@Expose
	@SerializedName("salesItemNumber")
	private String salesItemNumber;

	@Expose
	@SerializedName("taxSequenceNumber")
	private String taxSequenceNumber;

	@Expose
	@SerializedName("taxTypeCode")
	private String taxTypeCode;

	@Expose
	@SerializedName("taxAmount")
	private String taxAmount;

	/**
	 * @return the warehouseID
	 */
	public String getWarehouseID()
	{
		return warehouseID;
	}

	/**
	 * @param warehouseID
	 *           the warehouseID to set
	 */
	public void setWarehouseID(final String warehouseID)
	{
		this.warehouseID = warehouseID;
	}

	/**
	 * @return the orderDate
	 */
	public String getOrderDate()
	{
		return orderDate;
	}

	/**
	 * @param orderDate
	 *           the orderDate to set
	 */
	public void setOrderDate(final String orderDate)
	{
		this.orderDate = orderDate;
	}

	/**
	 * @return the transactionType
	 */
	public String getTransactionType()
	{
		return transactionType;
	}

	/**
	 * @param transactionType
	 *           the transactionType to set
	 */
	public void setTransactionType(final String transactionType)
	{
		this.transactionType = transactionType;
	}

	/**
	 * @return the posNumber
	 */
	public String getPosNumber()
	{
		return posNumber;
	}

	/**
	 * @param posNumber
	 *           the posNumber to set
	 */
	public void setPosNumber(final String posNumber)
	{
		this.posNumber = posNumber;
	}

	/**
	 * @return the transactionNumber
	 */
	public String getTransactionNumber()
	{
		return transactionNumber;
	}

	/**
	 * @param transactionNumber
	 *           the transactionNumber to set
	 */
	public void setTransactionNumber(final String transactionNumber)
	{
		this.transactionNumber = transactionNumber;
	}

	/**
	 * @return the salesItemNumber
	 */
	public String getSalesItemNumber()
	{
		return salesItemNumber;
	}

	/**
	 * @param salesItemNumber
	 *           the salesItemNumber to set
	 */
	public void setSalesItemNumber(final String salesItemNumber)
	{
		this.salesItemNumber = salesItemNumber;
	}

	/**
	 * @return the taxSequenceNumber
	 */
	public String getTaxSequenceNumber()
	{
		return taxSequenceNumber;
	}

	/**
	 * @param taxSequenceNumber
	 *           the taxSequenceNumber to set
	 */
	public void setTaxSequenceNumber(final String taxSequenceNumber)
	{
		this.taxSequenceNumber = taxSequenceNumber;
	}

	/**
	 * @return the taxTypeCode
	 */
	public String getTaxTypeCode()
	{
		return taxTypeCode;
	}

	/**
	 * @param taxTypeCode
	 *           the taxTypeCode to set
	 */
	public void setTaxTypeCode(final String taxTypeCode)
	{
		this.taxTypeCode = taxTypeCode;
	}

	/**
	 * @return the taxAmount
	 */
	public String getTaxAmount()
	{
		return taxAmount;
	}

	/**
	 * @param taxAmount
	 *           the taxAmount to set
	 */
	public void setTaxAmount(final String taxAmount)
	{
		this.taxAmount = taxAmount;
	}



}
