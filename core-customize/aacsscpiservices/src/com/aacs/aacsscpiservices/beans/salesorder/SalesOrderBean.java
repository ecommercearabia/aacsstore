/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsscpiservices.beans.salesorder;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class SalesOrderBean
{
	@Expose
	@SerializedName("warehouseID")
	private String warehouseID;

	@Expose
	@SerializedName("orderDate")
	private String orderDate;

	@Expose
	@SerializedName("transactionType")
	private String transactionType;

	@Expose
	@SerializedName("posNumber")
	private String posNumber;

	@Expose
	@SerializedName("transactionNumber")
	private String transactionNumber;

	@Expose
	@SerializedName("beginDateTimestamp")
	private String beginDateTimestamp;

	@Expose
	@SerializedName("endDateTimestamp")
	private String endDateTimestamp;

	@Expose
	@SerializedName("operatorQualifier")
	private String operatorQualifier;

	@Expose
	@SerializedName("operatorID")
	private String operatorID;

	@Expose
	@SerializedName("transactionCurrency")
	private String transactionCurrency;

	@Expose
	@SerializedName("partnerQualifier")
	private String partnerQualifier;

	@Expose
	@SerializedName("partnerID")
	private String partnerID;


	@Expose
	@SerializedName("salesOrderEntries")
	private final List<SalesOrderEntryBean> salesOrderEntries;

	@Expose
	@SerializedName("salesOrderEntriesPromotions")
	private final List<SalesOrderEntryPromotionBean> salesOrderEntriesPromotions;

	@Expose
	@SerializedName("salesOrderEntriesTaxes")
	private final List<SalesOrderEntryTaxBean> salesOrderEntriesTaxes;


	@Expose
	@SerializedName("salesOrderTenders")
	private final List<SalesOrderTenderBean> salesOrderTenders;

	@Expose
	@SerializedName("discountAmount")
	private Double discount;

	@Expose
	@SerializedName("couponCode")
	private String coupons;

	/**
	 *
	 */
	public SalesOrderBean()
	{
		super();
		salesOrderEntries = new ArrayList<>();
		salesOrderEntriesPromotions = new ArrayList<>();
		salesOrderEntriesTaxes = new ArrayList<>();
		this.salesOrderTenders = new ArrayList<>();
	}




	/**
	 * @return the coupons
	 */
	public String getCoupons()
	{
		return coupons;
	}




	/**
	 * @param coupons
	 *           the coupons to set
	 */
	public void setCoupons(final String coupons)
	{
		this.coupons = coupons;
	}




	/**
	 * @return the discount
	 */
	public Double getDiscount()
	{
		return discount;
	}


	/**
	 * @param discount
	 *           the discount to set
	 */
	public void setDiscount(final Double discount)
	{
		this.discount = discount;
	}


	/**
	 * @return the salesOrderTenders
	 */
	public List<SalesOrderTenderBean> getSalesOrderTenders()
	{
		return salesOrderTenders;
	}





	/**
	 * @return the warehouseID
	 */
	public String getWarehouseID()
	{
		return warehouseID;
	}

	/**
	 * @param warehouseID
	 *           the warehouseID to set
	 */
	public void setWarehouseID(final String warehouseID)
	{
		this.warehouseID = warehouseID;
	}

	/**
	 * @return the orderDate
	 */
	public String getOrderDate()
	{
		return orderDate;
	}

	/**
	 * @param orderDate
	 *           the orderDate to set
	 */
	public void setOrderDate(final String orderDate)
	{
		this.orderDate = orderDate;
	}

	/**
	 * @return the transactionType
	 */
	public String getTransactionType()
	{
		return transactionType;
	}

	/**
	 * @param transactionType
	 *           the transactionType to set
	 */
	public void setTransactionType(final String transactionType)
	{
		this.transactionType = transactionType;
	}

	/**
	 * @return the posNumber
	 */
	public String getPosNumber()
	{
		return posNumber;
	}

	/**
	 * @param posNumber
	 *           the posNumber to set
	 */
	public void setPosNumber(final String posNumber)
	{
		this.posNumber = posNumber;
	}

	/**
	 * @return the transactionNumber
	 */
	public String getTransactionNumber()
	{
		return transactionNumber;
	}

	/**
	 * @param transactionNumber
	 *           the transactionNumber to set
	 */
	public void setTransactionNumber(final String transactionNumber)
	{
		this.transactionNumber = transactionNumber;
	}

	/**
	 * @return the beginDateTimestamp
	 */
	public String getBeginDateTimestamp()
	{
		return beginDateTimestamp;
	}

	/**
	 * @param beginDateTimestamp
	 *           the beginDateTimestamp to set
	 */
	public void setBeginDateTimestamp(final String beginDateTimestamp)
	{
		this.beginDateTimestamp = beginDateTimestamp;
	}

	/**
	 * @return the endDateTimestamp
	 */
	public String getEndDateTimestamp()
	{
		return endDateTimestamp;
	}

	/**
	 * @param endDateTimestamp
	 *           the endDateTimestamp to set
	 */
	public void setEndDateTimestamp(final String endDateTimestamp)
	{
		this.endDateTimestamp = endDateTimestamp;
	}

	/**
	 * @return the operatorQualifier
	 */
	public String getOperatorQualifier()
	{
		return operatorQualifier;
	}

	/**
	 * @param operatorQualifier
	 *           the operatorQualifier to set
	 */
	public void setOperatorQualifier(final String operatorQualifier)
	{
		this.operatorQualifier = operatorQualifier;
	}

	/**
	 * @return the operatorID
	 */
	public String getOperatorID()
	{
		return operatorID;
	}

	/**
	 * @param operatorID
	 *           the operatorID to set
	 */
	public void setOperatorID(final String operatorID)
	{
		this.operatorID = operatorID;
	}

	/**
	 * @return the transactionCurrency
	 */
	public String getTransactionCurrency()
	{
		return transactionCurrency;
	}

	/**
	 * @param transactionCurrency
	 *           the transactionCurrency to set
	 */
	public void setTransactionCurrency(final String transactionCurrency)
	{
		this.transactionCurrency = transactionCurrency;
	}

	/**
	 * @return the partnerQualifier
	 */
	public String getPartnerQualifier()
	{
		return partnerQualifier;
	}

	/**
	 * @param partnerQualifier
	 *           the partnerQualifier to set
	 */
	public void setPartnerQualifier(final String partnerQualifier)
	{
		this.partnerQualifier = partnerQualifier;
	}

	/**
	 * @return the partnerID
	 */
	public String getPartnerID()
	{
		return partnerID;
	}

	/**
	 * @param partnerID
	 *           the partnerID to set
	 */
	public void setPartnerID(final String partnerID)
	{
		this.partnerID = partnerID;
	}

	/**
	 * @return the salesOrderEntries
	 */
	public List<SalesOrderEntryBean> getSalesOrderEntries()
	{
		return salesOrderEntries;
	}

	/**
	 * @return the salesOrderEntriesPromotions
	 */
	public List<SalesOrderEntryPromotionBean> getSalesOrderEntriesPromotions()
	{
		return salesOrderEntriesPromotions;
	}


	/**
	 * @return the salesOrderEntriesTaxes
	 */
	public List<SalesOrderEntryTaxBean> getSalesOrderEntriesTaxes()
	{
		return salesOrderEntriesTaxes;
	}



}
