/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacscomponentsfacades.constants;

/**
 * Global class for all Aacscomponentsfacades constants. You can add global constants for your extension into this class.
 */
public final class AacscomponentsfacadesConstants extends GeneratedAacscomponentsfacadesConstants
{
	public static final String EXTENSIONNAME = "aacscomponentsfacades";

	private AacscomponentsfacadesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "aacscomponentsfacadesPlatformLogo";
}
