/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.paymentredirect.service;

public interface AacspaymentredirectService
{
	String getHybrisLogoUrl(String logoCode);

	void createLogo(String logoCode);
}
