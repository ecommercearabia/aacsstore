/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.paymentredirect.setup;

import static com.aacs.paymentredirect.constants.AacspaymentredirectConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.aacs.paymentredirect.constants.AacspaymentredirectConstants;
import com.aacs.paymentredirect.service.AacspaymentredirectService;


@SystemSetup(extension = AacspaymentredirectConstants.EXTENSIONNAME)
public class AacspaymentredirectSystemSetup
{
	private final AacspaymentredirectService aacspaymentredirectService;

	public AacspaymentredirectSystemSetup(final AacspaymentredirectService aacspaymentredirectService)
	{
		this.aacspaymentredirectService = aacspaymentredirectService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		aacspaymentredirectService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return AacspaymentredirectSystemSetup.class.getResourceAsStream("/aacspaymentredirect/sap-hybris-platform.png");
	}
}
