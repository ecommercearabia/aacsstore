/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsnotificationcustomocc.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.aacs.aacsnotificationcustomocc.constants.AacsnotificationcustomoccConstants;
import org.apache.log4j.Logger;

public class AacsnotificationcustomoccManager extends GeneratedAacsnotificationcustomoccManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( AacsnotificationcustomoccManager.class.getName() );
	
	public static final AacsnotificationcustomoccManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AacsnotificationcustomoccManager) em.getExtension(AacsnotificationcustomoccConstants.EXTENSIONNAME);
	}
	
}
