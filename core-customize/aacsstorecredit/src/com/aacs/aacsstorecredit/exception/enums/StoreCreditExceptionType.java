/**
 *
 */
package com.aacs.aacsstorecredit.exception.enums;

/**
 * @author yhammad
 *
 */
public enum StoreCreditExceptionType
{
	NOT_AVAILABLE, EMPTY;
}
