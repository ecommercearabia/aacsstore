/**
 *
 */
package com.aacs.aacsstorecredit.exception;

import com.aacs.aacsstorecredit.exception.enums.StoreCreditExceptionType;


/**
 * @author yhammad
 *
 */
public class StoreCreditException extends RuntimeException
{

	StoreCreditExceptionType storeCreditExceptionType;


	public StoreCreditException(final String message, final StoreCreditExceptionType type)
	{
		super(message);
		this.storeCreditExceptionType = type;
	}

	/**
	 * @return the type
	 */
	public StoreCreditExceptionType getStoreCreditExceptionType()
	{
		return storeCreditExceptionType;
	}
}
