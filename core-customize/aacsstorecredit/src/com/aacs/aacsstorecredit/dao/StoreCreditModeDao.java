/**
 *
 */
package com.aacs.aacsstorecredit.dao;


import com.aacs.aacsstorecredit.enums.StoreCreditModeType;
import com.aacs.aacsstorecredit.model.StoreCreditModeModel;

/**
 * @author mnasro
 *
 */
public interface StoreCreditModeDao
{
	public StoreCreditModeModel getStoreCreditMode(StoreCreditModeType storeCreditModeType);
}
