/**
 *
 */
package com.aacs.aacsstorecredit.service;

import de.hybris.platform.store.BaseStoreModel;

import java.util.List;

import com.aacs.aacsstorecredit.enums.StoreCreditModeType;
import com.aacs.aacsstorecredit.model.StoreCreditModeModel;


/**
 * @author mnasro
 *
 */
public interface StoreCreditModeService
{
	public StoreCreditModeModel getStoreCreditMode(String storeCreditModeTypeCode);

	public StoreCreditModeModel getStoreCreditMode(StoreCreditModeType storeCreditModeType);

	public List<StoreCreditModeModel> getSupportedStoreCreditModes(BaseStoreModel baseStoreModel);

	public List<StoreCreditModeModel> getSupportedStoreCreditModesCurrentBaseStore();
}
