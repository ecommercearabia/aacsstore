/**
 *
 */
package com.aacs.aacsloyaltyprogramprovider.exception;

import com.aacs.aacsloyaltyprogramprovider.exception.type.LoyaltyPaymentExceptionType;


/**
 * @author yhammad
 *
 */
public class LoyaltyPaymentException extends RuntimeException
{

	private final LoyaltyPaymentExceptionType loyaltyPaymentExceptionType;


	public LoyaltyPaymentException(final String message, final LoyaltyPaymentExceptionType type)
	{
		super(message);
		this.loyaltyPaymentExceptionType = type;
	}


	/**
	 * @return the loyaltyPaymentExceptionType
	 */
	public LoyaltyPaymentExceptionType getLoyaltyPaymentExceptionType()
	{
		return loyaltyPaymentExceptionType;
	}

}
