/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramprovider.exception;

import com.aacs.aacsloyaltyprogramprovider.exception.type.AACSLoyaltyExceptionType;


/**
 *
 */
public class AACSLoyaltyException extends Exception
{
	private final Object data;

	private final AACSLoyaltyExceptionType exceptionType;

	/**
	 *
	 */
	public AACSLoyaltyException(final String msg, final Object data, final AACSLoyaltyExceptionType exceptionType)
	{
		super(msg);
		this.data = data;
		this.exceptionType = exceptionType;
	}

	/**
	 * @return the data
	 */
	public Object getData()
	{
		return data;
	}

	/**
	 * @return the exceptionType
	 */
	public AACSLoyaltyExceptionType getExceptionType()
	{
		return exceptionType;
	}

}
