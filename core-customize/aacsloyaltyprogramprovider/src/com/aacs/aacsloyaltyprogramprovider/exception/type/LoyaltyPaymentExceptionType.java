/**
 *
 */
package com.aacs.aacsloyaltyprogramprovider.exception.type;

/**
 * @author yhammad
 *
 */
public enum LoyaltyPaymentExceptionType
{
	NOT_AVAILABLE, EMPTY;
}
