/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramprovider.exception.type;

/**
 *
 */
public enum AACSLoyaltyExceptionType
{

	// @formatter:off
	GIIFT("Giift Exception"),
	LOYALTY_IS_NOT_ENABLED("loyalty is not enabled"),
	ERROR_WHILE_PROCESSING("Request Was not successful"),
	CUSTOMER_NOT_INVOLVED("customer is not involved in loyalty program");
	// @formatter:on

	private final String msg;

	/**
	 *
	 */
	private AACSLoyaltyExceptionType(final String msg)
	{
		this.msg = msg;
	}

	/**
	 * @return the msg
	 */
	public String getMsg()
	{
		return msg;
	}



}
