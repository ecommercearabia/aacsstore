/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramprovider.dao.impl;

import com.aacs.aacsloyaltyprogramprovider.dao.LoyaltyProgramProviderDao;
import com.aacs.aacsloyaltyprogramprovider.model.LoyaltyProgramProviderModel;


/**
 * The Class DefaultGiiftLoyaltyProgramProviderDao.
 */
public class DefaultGiiftLoyaltyProgramProviderDao extends DefaultLoyaltyProgramProviderDao implements LoyaltyProgramProviderDao
{



	/**
	 * Instantiates a new default giift loyalty program provider dao.
	 */
	public DefaultGiiftLoyaltyProgramProviderDao()
	{
		super(LoyaltyProgramProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return LoyaltyProgramProviderModel._TYPECODE;
	}

}
