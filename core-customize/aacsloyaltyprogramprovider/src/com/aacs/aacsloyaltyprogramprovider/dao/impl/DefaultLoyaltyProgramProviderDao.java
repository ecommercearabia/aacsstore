/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramprovider.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.aacs.aacsloyaltyprogramprovider.dao.LoyaltyProgramProviderDao;
import com.aacs.aacsloyaltyprogramprovider.model.LoyaltyProgramProviderModel;
import com.google.common.base.Preconditions;


/**
 * The Class DefaultLoyaltyProgramProviderDao.
 */
public abstract class DefaultLoyaltyProgramProviderDao extends DefaultGenericDao<LoyaltyProgramProviderModel>
		implements LoyaltyProgramProviderDao
{

	/** The base store service. */
	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	/** The Constant CODE_MUSTN_T_BE_NULL. */
	private static final String CODE_MUSTN_T_BE_NULL = "code mustn't be null or empty";

	/** The Constant STORES. */
	private static final String STORES = "stores";

	/** The Constant CODE. */
	private static final String CODE = "code";

	/** The Constant ACTIVE. */
	private static final String ACTIVE = "active";

	/**
	 * Instantiates a new default loyalty program provider dao.
	 *
	 * @param typecode the typecode
	 */
	public DefaultLoyaltyProgramProviderDao(final String typecode)
	{
		super(typecode);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	protected abstract String getModelName();

	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the optional
	 */
	@Override
	public Optional<LoyaltyProgramProviderModel> get(final String code)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(code), CODE_MUSTN_T_BE_NULL);
		final Map<String, Object> params = new HashMap<>();
		params.put(CODE, code);
		final List<LoyaltyProgramProviderModel> find = find(params);
		final Optional<LoyaltyProgramProviderModel> findFirst = find.stream().findFirst();

		return findFirst.isPresent() ? Optional.ofNullable(findFirst.get()) : Optional.ofNullable(null);
	}

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreUid the base store uid
	 * @return the active provider
	 */
	@Override
	public Optional<LoyaltyProgramProviderModel> getActiveProvider(final String baseStoreUid)
	{
		return getActiveProvider(baseStoreService.getBaseStoreForUid(baseStoreUid));
	}

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreModel the base store model
	 * @return the active provider
	 */
	@Override
	public Optional<LoyaltyProgramProviderModel> getActiveProvider(final BaseStoreModel baseStoreModel)
	{
		Preconditions.checkArgument(baseStoreModel != null, "baseStoreModel must not be null");
		final Optional<Collection<LoyaltyProgramProviderModel>> find = find(Arrays.asList(baseStoreModel), Boolean.TRUE, 1);

		return find.isPresent() && !find.get().isEmpty() ? Optional.ofNullable(find.get().iterator().next())
				: Optional.ofNullable(null);
	}

	/**
	 * Gets the active provider by current base store.
	 *
	 * @return the active provider by current base store
	 */
	@Override
	public Optional<LoyaltyProgramProviderModel> getActiveProviderByCurrentBaseStore()
	{
		return getActiveProvider(baseStoreService.getCurrentBaseStore());
	}



	/**
	 * Find.
	 *
	 * @param stores the stores
	 * @param active the active
	 * @param countRecords the count records
	 * @return the optional
	 */
	protected Optional<Collection<LoyaltyProgramProviderModel>> find(final List<BaseStoreModel> stores, final Boolean active,
			final Integer countRecords)
	{
		final Map<String, Object> params = new HashMap<>();
		final StringBuilder query = new StringBuilder();
		query.append("SELECT {fp.PK} ");
		query.append("FROM ");
		query.append("{ ");
		query.append(getModelName()).append(" AS fp ");


		if (CollectionUtils.isNotEmpty(stores))
		{
			query.append("JOIN BSFPRelation AS bsfpRel ON {bsfpRel.target}={fp.PK} ");
			query.append("JOIN BaseStore AS bs ON {bsfpRel.source}={bs.PK} AND {bs.PK} IN (?stores) ");
			params.put(STORES, stores);
		}

		query.append("} ");

		int activeValue = 0;
		if (active == null || Boolean.TRUE.equals(active))
		{
			activeValue = 1;
		}
		query.append("WHERE {fp.active}=?active ");
		params.put(ACTIVE, activeValue);

		query.append("ORDER BY {fp.creationtime} DESC");

		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query.toString());
		flexibleSearchQuery.getQueryParameters().putAll(params);
		if (countRecords != null && countRecords > 0)
		{
			flexibleSearchQuery.setCount(countRecords);
		}

		final SearchResult<LoyaltyProgramProviderModel> result = getFlexibleSearchService().search(flexibleSearchQuery);

		return result != null ? Optional.ofNullable(result.getResult()) : Optional.ofNullable(Collections.emptyList());
	}

}
