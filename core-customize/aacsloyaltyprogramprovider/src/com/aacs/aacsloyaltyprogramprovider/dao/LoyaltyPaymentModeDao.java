/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramprovider.dao;

import com.aacs.aacsloyaltyprogramprovider.enums.LoyaltyPaymentModeType;
import com.aacs.aacsloyaltyprogramprovider.model.LoyaltyPaymentModeModel;


/**
 *
 */
public interface LoyaltyPaymentModeDao
{
	public LoyaltyPaymentModeModel getLoyaltyPaymentMode(LoyaltyPaymentModeType loyaltyPaymentModeType);

}
