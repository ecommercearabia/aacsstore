/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramprovider.service.impl;

import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;
import org.springframework.util.CollectionUtils;

import com.aacs.aacsloyaltyprogramprovider.dao.LoyaltyPaymentModeDao;
import com.aacs.aacsloyaltyprogramprovider.enums.LoyaltyPaymentModeType;
import com.aacs.aacsloyaltyprogramprovider.exception.LoyaltyPaymentException;
import com.aacs.aacsloyaltyprogramprovider.exception.type.LoyaltyPaymentExceptionType;
import com.aacs.aacsloyaltyprogramprovider.model.LoyaltyPaymentModeModel;
import com.aacs.aacsloyaltyprogramprovider.service.LoyaltyPaymentModeService;


/**
 *
 */
public class DefaultLoyaltyPaymentModeService implements LoyaltyPaymentModeService
{

	private static final String NOT_ENABLED_MSG = "Loyalty is not enabled";

	@Resource(name = "loyaltyPaymentModeDao")
	private LoyaltyPaymentModeDao loyaltyPaymentModeDao;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;


	@Override
	public Optional<LoyaltyPaymentModeModel> getLoyaltyPaymentMode(final String loyaltyPaymentModeTypeCode)
	{
		if (Strings.isBlank(loyaltyPaymentModeTypeCode))
		{
			throw new IllegalArgumentException("loyaltyPaymentModeTypeCode can't be null or empty");
		}
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();

		if (!currentBaseStore.isLoyaltyProgramEnabled())
		{
			throw new LoyaltyPaymentException(NOT_ENABLED_MSG, LoyaltyPaymentExceptionType.NOT_AVAILABLE);
		}

		final LoyaltyPaymentModeType loyaltyPaymentModeType = LoyaltyPaymentModeType.valueOf(loyaltyPaymentModeTypeCode);
		if (loyaltyPaymentModeType == null)
		{
			return Optional.empty();
		}
		return getLoyaltyPaymentMode(loyaltyPaymentModeType);

	}

	@Override
	public Optional<LoyaltyPaymentModeModel> getLoyaltyPaymentMode(final LoyaltyPaymentModeType loyaltyPaymentModeType)
	{
		if (loyaltyPaymentModeType == null)
		{
			throw new IllegalArgumentException("loyaltyPaymentModeType can't be null");
		}
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();

		if (!currentBaseStore.isLoyaltyProgramEnabled())
		{
			throw new LoyaltyPaymentException(NOT_ENABLED_MSG, LoyaltyPaymentExceptionType.NOT_AVAILABLE);
		}
		return Optional.ofNullable(loyaltyPaymentModeDao.getLoyaltyPaymentMode(loyaltyPaymentModeType));
	}

	@Override
	public List<LoyaltyPaymentModeModel> getSupportedLoyaltyPaymentModes(final BaseStoreModel baseStoreModel)
	{
		if (baseStoreModel == null)
		{
			throw new IllegalArgumentException("baseStoreModel can't be null");
		}
		else if (!baseStoreModel.isLoyaltyProgramEnabled())
		{
			throw new LoyaltyPaymentException(NOT_ENABLED_MSG, LoyaltyPaymentExceptionType.NOT_AVAILABLE);
		}

		return new ArrayList<>(baseStoreModel.getSupportedLoyaltyPaymentModes());
	}

	@Override
	public List<LoyaltyPaymentModeModel> getSupportedLoyaltyPaymentModesCurrentBaseStore()
	{
		return getSupportedLoyaltyPaymentModes(baseStoreService.getCurrentBaseStore());
	}

	@Override
	public Optional<LoyaltyPaymentModeModel> getLoyaltyPaymentModeByStore(final LoyaltyPaymentModeType loyaltyPaymentModeType,
			final BaseStoreModel baseStoreModel)
	{
		final List<LoyaltyPaymentModeModel> supportedLoyaltyPaymentModes = getSupportedLoyaltyPaymentModes(baseStoreModel);
		final Optional<LoyaltyPaymentModeModel> loyaltyPaymentMode = getLoyaltyPaymentMode(loyaltyPaymentModeType);

		if (loyaltyPaymentMode.isEmpty() || CollectionUtils.isEmpty(supportedLoyaltyPaymentModes))
		{
			return Optional.empty();
		}

		return supportedLoyaltyPaymentModes.contains(loyaltyPaymentMode.get()) ? loyaltyPaymentMode : Optional.empty();

	}

	@Override
	public Optional<LoyaltyPaymentModeModel> getLoyaltyPaymentModeByCurrentBaseStore(
			final LoyaltyPaymentModeType loyaltyPaymentModeType)
	{
		return getLoyaltyPaymentModeByStore(loyaltyPaymentModeType, baseStoreService.getCurrentBaseStore());
	}

}
