/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramprovider.service.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Objects;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aacs.aacsloyaltyprogramprovider.exception.AACSLoyaltyException;
import com.aacs.aacsloyaltyprogramprovider.exception.type.AACSLoyaltyExceptionType;
import com.aacs.aacsloyaltyprogramprovider.service.LoyaltyValidationService;
import com.google.common.base.Preconditions;


/**
 *
 */
public class DefaultLoyaltyValidationService implements LoyaltyValidationService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultLoyaltyValidationService.class);
	private static final String ORDER_NULL_MSG = "order is null";
	private static final String CUSTOMER_NULL_MSG = "customer is null";
	private static final String BASE_STORE_NULL_MSG = "baseStore is null";

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "userService")
	private UserService userService;

	@Override
	public void validateLoyaltyEnabled(final CustomerModel customer, final BaseStoreModel baseStore) throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(baseStore), BASE_STORE_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		if (!baseStore.isLoyaltyProgramEnabled())
		{
			throw new AACSLoyaltyException(AACSLoyaltyExceptionType.LOYALTY_IS_NOT_ENABLED.getMsg(),
					null, AACSLoyaltyExceptionType.LOYALTY_IS_NOT_ENABLED);
		}

		if (!customer.isInvolvedInLoyaltyProgram())
		{
			throw new AACSLoyaltyException(AACSLoyaltyExceptionType.CUSTOMER_NOT_INVOLVED.getMsg(),
					null, AACSLoyaltyExceptionType.CUSTOMER_NOT_INVOLVED);
		}

	}

	@Override
	public void validateLoyaltyEnabledByCurrentStore(final CustomerModel customer) throws AACSLoyaltyException
	{
		validateLoyaltyEnabled(customer, baseStoreService.getCurrentBaseStore());

	}

	@Override
	public void validateLoyaltyEnabledByCurrentStoreAndCurrentCustomer() throws AACSLoyaltyException
	{
		validateLoyaltyEnabledByCurrentStore(getCurrentCustomer());
	}

	private CustomerModel getCurrentCustomer()
	{
		final UserModel currentUser = userService.getCurrentUser();
		if (!Objects.isNull(currentUser) && currentUser instanceof CustomerModel)
		{
			return (CustomerModel) currentUser;
		}
		return null;
	}

	@Override
	public void validateLoyaltyEnabledByOrder(final AbstractOrderModel order) throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(order), ORDER_NULL_MSG);
		final UserModel user = order.getUser();
		if (!Objects.isNull(user) && user instanceof CustomerModel)
		{
			validateLoyaltyEnabled((CustomerModel) user, order.getStore());
		}
		else
		{
			validateLoyaltyEnabled(null, order.getStore());
		}


	}


}
