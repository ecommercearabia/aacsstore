/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramprovider.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.store.BaseStoreModel;

import com.aacs.aacsloyaltyprogramprovider.exception.AACSLoyaltyException;


/**
 *
 */
public interface LoyaltyValidationService
{
	public void validateLoyaltyEnabled(final CustomerModel customer, final BaseStoreModel baseStore) throws AACSLoyaltyException;

	public void validateLoyaltyEnabledByCurrentStore(final CustomerModel customer) throws AACSLoyaltyException;

	public void validateLoyaltyEnabledByCurrentStoreAndCurrentCustomer() throws AACSLoyaltyException;

	public void validateLoyaltyEnabledByOrder(final AbstractOrderModel order) throws AACSLoyaltyException;



}
