/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramprovider.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Objects;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;
import org.springframework.util.Assert;

import com.aacs.aacsloyaltyprogramprovider.giift.beans.metadata.GiiftTransaction;


/**
 *
 */
public class GiiftMetaInfoTransactionPopulator implements Populator<AbstractOrderModel, GiiftTransaction>
{
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Override
	public void populate(final AbstractOrderModel source, final GiiftTransaction target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		if (!Objects.isNull(source.getCreationtime()))
		{
			//target.setDate(source.getCreationtime().toLocaleString());
		}

		target.setId(source.getCode());
		if (Strings.isNotBlank(source.getCode()))
		{
			target.setPos("POS" + source.getCode());
			target.setSeq("SEQ" + source.getCode());

		}
		else
		{
			target.setPos(Strings.EMPTY);
			target.setSeq(Strings.EMPTY);
		}

		final double total = getOrderTotalWithTax(source)
				+ (source.getExpectedLoyaltyRedeemAmount() != null ? source.getExpectedLoyaltyRedeemAmount() : 0d);
		target.setTotal(formatDouble(total));

	}

	private double calcTotalWithTax(final AbstractOrderModel source)
	{
		if (source == null)
		{
			throw new IllegalArgumentException("source order must not be null");
		}
		if (source.getTotalPrice() == null)
		{
			return 0.0d;
		}

		BigDecimal totalPrice = BigDecimal.valueOf(source.getTotalPrice().doubleValue());

		// Add the taxes to the total price if the cart is net; if the total was null taxes should be null as well
		if (Boolean.TRUE.equals(source.getNet()) && totalPrice.compareTo(BigDecimal.ZERO) != 0 && source.getTotalTax() != null)
		{
			totalPrice = totalPrice.add(BigDecimal.valueOf(source.getTotalTax().doubleValue()));
		}
		return totalPrice.doubleValue();
	}

	/**
	 * Gets the total price.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the total price
	 */
	protected Double getOrderTotalWithTax(final AbstractOrderModel abstractOrderModel)
	{
		final CurrencyModel cartCurrency = abstractOrderModel.getCurrency();
		final CurrencyModel baseCurrency = commonI18NService.getBaseCurrency();
		double calcTotalWithTax = calcTotalWithTax(abstractOrderModel);
		if (Double.NaN == calcTotalWithTax)
		{
			calcTotalWithTax = 0.0d;
		}
		return commonI18NService.convertAndRoundCurrency(cartCurrency.getConversion().doubleValue(),
				baseCurrency.getConversion().doubleValue(), cartCurrency.getDisplayDigits(), calcTotalWithTax);
	}

	protected double formatDouble(final double val)
	{
		final DecimalFormat df = new DecimalFormat("#.00");
		final String format = df.format(val);
		return Double.valueOf(format);
	}
}
