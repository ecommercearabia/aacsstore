/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramprovider.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.text.DecimalFormat;
import java.util.Objects;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.aacs.aacsloyaltyprogramprovider.giift.beans.Customer;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.Transaction;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.metadata.GiiftMetaInfo;


/**
 *
 */
public class GiiftTransactionPopulator implements Populator<AbstractOrderModel, Transaction>
{

	@Resource(name = "giiftCustomerConverter")
	private Converter<CustomerModel, Customer> customerConverter;

	@Resource(name = "giiftMetaInfoConverter")
	private Converter<AbstractOrderModel, GiiftMetaInfo> giiftMetaInfoConverter;

	@Override
	public void populate(final AbstractOrderModel source, final Transaction target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setFinalAmount(source.getTotalPriceAfterRedeem());
		target.setInitialAmount(source.getTotalPrice());

		if (!Objects.isNull(source.getLoyaltyAmountSelected()) && source.getLoyaltyExchangeRate() > 0)
		{
			target.setPointAmount(formatDouble(source.getLoyaltyAmountSelected() / source.getLoyaltyExchangeRate()));
		}
		if (!Objects.isNull(source.getUser()) && (source.getUser() instanceof CustomerModel))
		{
			target.setCustomer(customerConverter.convert((CustomerModel) source.getUser()));
		}
		target.setValidateId(source.getGiiftValidateId());
		if (!Objects.isNull(source.getGiiftValidateId()))
		{
		target.setTransactionId(String.format("P%s", source.getGiiftValidateId()));
		}


		target.setMetaData(giiftMetaInfoConverter.convert(source));
	}

	protected double formatDouble(final double val)
	{
		final DecimalFormat df = new DecimalFormat("#.00");
		final String format = df.format(val);
		return Double.valueOf(format);
	}


}
