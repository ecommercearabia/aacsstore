/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramprovider.context;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.aacs.aacsloyaltyprogramprovider.beans.LoyaltyBalance;
import com.aacs.aacsloyaltyprogramprovider.beans.LoyaltyCustomerCode;
import com.aacs.aacsloyaltyprogramprovider.beans.LoyaltyCustomerInfo;
import com.aacs.aacsloyaltyprogramprovider.beans.LoyaltyPagination;
import com.aacs.aacsloyaltyprogramprovider.beans.LoyaltyUsablePoints;
import com.aacs.aacsloyaltyprogramprovider.beans.ValidateTransactionResult;
import com.aacs.aacsloyaltyprogramprovider.exception.AACSLoyaltyException;



/**
 *
 */
public interface LoyaltyProgramContext
{

	public boolean isRegister(CustomerModel customer, BaseStoreModel baseStore) throws AACSLoyaltyException;

	public boolean isRegisterByCurrentBaseStore(CustomerModel customer) throws AACSLoyaltyException;

	public boolean isRegisterByCurrentBaseStoreAndCustomer() throws AACSLoyaltyException;

	public boolean registerCustomer(CustomerModel customer, BaseStoreModel baseStore) throws AACSLoyaltyException;

	public boolean registerCustomerByCurrentBaseStore(CustomerModel customer) throws AACSLoyaltyException;

	public boolean registerCustomerByCurrentBaseStoreAndCustomer() throws AACSLoyaltyException;


	public Optional<LoyaltyUsablePoints> getUsablePoints(AbstractOrderModel order, double orderTotalAmount)
			throws AACSLoyaltyException;

	public Optional<LoyaltyUsablePoints> getUsablePointsByOrder(AbstractOrderModel order) throws AACSLoyaltyException;

	public Optional<ValidateTransactionResult> validateTransaction(AbstractOrderModel order) throws AACSLoyaltyException;

	public boolean createTransaction(AbstractOrderModel order) throws AACSLoyaltyException;

	public Optional<LoyaltyCustomerInfo> getLoyaltyCustomerInfo(CustomerModel customer, LoyaltyPagination pagination,
			BaseStoreModel baseStore) throws AACSLoyaltyException;

	public Optional<LoyaltyCustomerInfo> getLoyaltyCustomerInfoByCurrentBaseStore(CustomerModel customer,
			LoyaltyPagination pagination) throws AACSLoyaltyException;

	public Optional<LoyaltyCustomerInfo> getLoyaltyCustomerInfoByCurrentBaseStoreAndCustomer(LoyaltyPagination pagination)
			throws AACSLoyaltyException;


	public Optional<LoyaltyCustomerCode> getLoyaltyCustomerCode(CustomerModel customer, BaseStoreModel baseStore)
			throws AACSLoyaltyException;

	public Optional<LoyaltyCustomerCode> getLoyaltyCustomerCodeByCurrentBaseStore(CustomerModel customer)
			throws AACSLoyaltyException;

	public Optional<LoyaltyCustomerCode> getLoyaltyCustomerCodeByCurrentBaseStoreAndCustomer() throws AACSLoyaltyException;

	public boolean cancelTransaction(AbstractOrderModel order) throws AACSLoyaltyException;

	public Optional<LoyaltyBalance> getLoyaltyBalance(CustomerModel customer, BaseStoreModel baseStore)
			throws AACSLoyaltyException;

	public Optional<LoyaltyBalance> getLoyaltyBalanceByCurrentBaseStore(CustomerModel customer) throws AACSLoyaltyException;

	public Optional<LoyaltyBalance> getLoyaltyBalanceByCurrentBaseStoreAndCustomer() throws AACSLoyaltyException;

	public void reserve(AbstractOrderModel order, double orderTotalAmount) throws AACSLoyaltyException;

	public boolean isLoyaltyEnabled(AbstractOrderModel order) throws AACSLoyaltyException;

	public boolean isLoyaltyEnabled(BaseStoreModel baseStoreModel, CustomerModel customer) throws AACSLoyaltyException;

	public boolean isLoyaltyEnabledByCurrentBaseStore(CustomerModel customer) throws AACSLoyaltyException;

	public boolean isLoyaltyEnabledByCurrentBaseStoreAndCurrentCustomer() throws AACSLoyaltyException;

	public void resetLoyalty(AbstractOrderModel order);

	public boolean fullyRefundLoyalty(final OrderModel order);

	public boolean refundLoyalty(final ReturnRequestModel order);

	public boolean refundLoyaltyAmount(final OrderModel order, final double amount);


}
