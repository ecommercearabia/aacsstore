/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramprovider.context.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.aacs.aacsloyaltyprogramprovider.context.LoyaltyProgramProviderContext;
import com.aacs.aacsloyaltyprogramprovider.model.GiiftLoyaltyProgramProviderModel;
import com.aacs.aacsloyaltyprogramprovider.model.LoyaltyProgramProviderModel;
import com.aacs.aacsloyaltyprogramprovider.strategy.LoyaltyProgramProviderStrategy;
import com.google.common.base.Preconditions;


/**
 * The Class DefaultLoyaltyProgramProviderContext.
 */
public class DefaultLoyaltyProgramProviderContext implements LoyaltyProgramProviderContext
{

	/** The loyalty program provider map. */
	@Resource(name = "loyaltyProgramProviderStrategyMap")
	private Map<Class<?>, LoyaltyProgramProviderStrategy> loyaltyProgramProviderMap;

	/** The Constant PROVIDER_CLASS_MUSTN_T_BE_NULL. */
	private static final String PROVIDER_CLASS_MUSTN_T_BE_NULL = "strategy mustn't be null";

	/** The Constant BASESTORE_UID_MUSTN_T_BE_NULL. */
	private static final String BASESTORE_UID_MUSTN_T_BE_NULL = "baseStoreUid mustn't be null";

	/** The Constant BASESTORE_MODEL_MUSTN_T_BE_NULL. */
	private static final String BASESTORE_MODEL_MUSTN_T_BE_NULL = "baseStoreModel mustn't be null";

	/** The Constant PROVIDER_STRATEGY_NOT_FOUND. */
	private static final String PROVIDER_STRATEGY_NOT_FOUND = "strategy not found";

	/** The Constant BASE_STORE_DEFAULT_PROVIDER. */
	//	private static final String BASE_STORE_DEFAULT_PROVIDER = "GiiftLoyalityProgramProvider";


	private static final String GIIFT_PROVIDER = "GiiftLoyaltyProgramProvider";

	/**
	 * Gets the loyalty program provider map.
	 *
	 * @return the loyalty program provider map
	 */
	public Map<Class<?>, LoyaltyProgramProviderStrategy> getLoyaltyProgramProviderMap()
	{
		return loyaltyProgramProviderMap;
	}

	/**
	 * Gets the provider.
	 *
	 * @param providerClass the provider class
	 * @return the provider
	 */
	@Override
	public Optional<LoyaltyProgramProviderModel> getProvider(final Class<?> providerClass)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<LoyaltyProgramProviderStrategy> strategy = getStrategy(providerClass);
		Preconditions.checkArgument(strategy.isPresent(), PROVIDER_STRATEGY_NOT_FOUND);

		return strategy.get().getActiveProviderByCurrentBaseStore();
	}

	/**
	 * Gets the provider.
	 *
	 * @param baseStoreUid the base store uid
	 * @param providerClass the provider class
	 * @return the provider
	 */
	@Override
	public Optional<LoyaltyProgramProviderModel> getProvider(final String baseStoreUid, final Class<?> providerClass)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(baseStoreUid != null, BASESTORE_UID_MUSTN_T_BE_NULL);
		final Optional<LoyaltyProgramProviderStrategy> strategy = getStrategy(providerClass);
		Preconditions.checkArgument(strategy.isPresent(), PROVIDER_STRATEGY_NOT_FOUND);

		return strategy.get().getActiveProvider(baseStoreUid);
	}

	/**
	 * Gets the provider.
	 *
	 * @param baseStoreModel the base store model
	 * @param providerClass the provider class
	 * @return the provider
	 */
	@Override
	public Optional<LoyaltyProgramProviderModel> getProvider(final BaseStoreModel baseStoreModel, final Class<?> providerClass)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_MODEL_MUSTN_T_BE_NULL);
		final Optional<LoyaltyProgramProviderStrategy> strategy = getStrategy(providerClass);
		Preconditions.checkArgument(strategy.isPresent(), PROVIDER_STRATEGY_NOT_FOUND);

		return strategy.get().getActiveProvider(baseStoreModel);
	}

	/**
	 * Gets the provider.
	 *
	 * @param baseStoreModel the base store model
	 * @return the provider
	 */
	@Override
	public Optional<LoyaltyProgramProviderModel> getProvider(final BaseStoreModel baseStoreModel)
	{
		if (StringUtils.isBlank(baseStoreModel.getLoyaltyProgramProvider())
				|| StringUtils.isEmpty(baseStoreModel.getLoyaltyProgramProvider())
		)
		{
			return Optional.empty();
		}

		if (GIIFT_PROVIDER.equalsIgnoreCase(baseStoreModel.getLoyaltyProgramProvider()))
		{
			return getProvider(baseStoreModel.getUid(), GiiftLoyaltyProgramProviderModel.class);
		}

		return getProvider(baseStoreModel.getUid(), LoyaltyProgramProviderModel.class);
	}

	/**
	 * Gets the strategy.
	 *
	 * @param providerClass the provider class
	 * @return the strategy
	 */
	protected Optional<LoyaltyProgramProviderStrategy> getStrategy(final Class<?> providerClass)
	{
		final LoyaltyProgramProviderStrategy strategy = getLoyaltyProgramProviderMap().get(providerClass);

		return Optional.ofNullable(strategy);
	}
}
