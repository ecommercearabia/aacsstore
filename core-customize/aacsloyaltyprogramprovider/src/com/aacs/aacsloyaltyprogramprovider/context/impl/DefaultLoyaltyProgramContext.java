/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramprovider.context.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aacs.aacsloyaltyprogramprovider.beans.LoyaltyBalance;
import com.aacs.aacsloyaltyprogramprovider.beans.LoyaltyCustomerCode;
import com.aacs.aacsloyaltyprogramprovider.beans.LoyaltyCustomerInfo;
import com.aacs.aacsloyaltyprogramprovider.beans.LoyaltyPagination;
import com.aacs.aacsloyaltyprogramprovider.beans.LoyaltyUsablePoints;
import com.aacs.aacsloyaltyprogramprovider.beans.ValidateTransactionResult;
import com.aacs.aacsloyaltyprogramprovider.context.LoyaltyProgramContext;
import com.aacs.aacsloyaltyprogramprovider.context.LoyaltyProgramProviderContext;
import com.aacs.aacsloyaltyprogramprovider.exception.AACSLoyaltyException;
import com.aacs.aacsloyaltyprogramprovider.model.LoyaltyProgramProviderModel;
import com.aacs.aacsloyaltyprogramprovider.service.LoyaltyValidationService;
import com.aacs.aacsloyaltyprogramprovider.strategy.LoyaltyProgramStrategy;
import com.aacs.aacsloyaltyprogramprovider.strategy.impl.DefaultGiiftLoyaltyStrategy;
import com.google.common.base.Preconditions;




/**
 *
 */
public class DefaultLoyaltyProgramContext implements LoyaltyProgramContext
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultGiiftLoyaltyStrategy.class);
	private static final String LOYALTY_STRATEGY_NOT_FOUND = "strategy not found";
	private static final String BASESTORE_MUSTN_T_BE_NULL = "basestore mustn't be null";
	private static final String LOYALTY_PROGRAM_PROVIDER_NULL_MSG = "loyalty program provider is null";
	private static final String ABSTRACT_ORDER_NULL_MSG = "order is null";
	private static final String CUSTOMER_NULL_MSG = "customer  is null";
	private static final String RETURN_REQUEST_NULL_MSG = "return request is null";



	@Resource(name = "loyaltyProgramProviderContext")
	private LoyaltyProgramProviderContext loyaltyProgramProviderContext;

	@Resource(name = "loyaltyProgramStrategyMap")
	private Map<Class<?>, LoyaltyProgramStrategy> loyaltyProgramStrategyMap;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "loyaltyValidationService")
	private LoyaltyValidationService loyaltyValidationService;


	protected Map<Class<?>, LoyaltyProgramStrategy> getLoyaltyProgramStrategy()
	{
		return loyaltyProgramStrategyMap;
	}

	protected Optional<LoyaltyProgramStrategy> getStrategy(final Class<?> providerClass)
	{
		final LoyaltyProgramStrategy strategy = getLoyaltyProgramStrategy().get(providerClass);
		Preconditions.checkArgument(strategy != null, LOYALTY_STRATEGY_NOT_FOUND);
		return Optional.ofNullable(strategy);
	}

	@Override
	public boolean isRegister(final CustomerModel customer, final BaseStoreModel baseStore) throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(baseStore), BASESTORE_MUSTN_T_BE_NULL);
		loyaltyValidationService.validateLoyaltyEnabled(customer, baseStore);
		final Optional<LoyaltyProgramProviderModel> provider = loyaltyProgramProviderContext.getProvider(baseStore);
		Preconditions.checkArgument(provider.isPresent(), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final Optional<LoyaltyProgramStrategy> strategy = getStrategy(provider.get().getClass());
		Preconditions.checkArgument(strategy.isPresent(), LOYALTY_STRATEGY_NOT_FOUND);


		try
		{
			return strategy.get().isRegister(customer, provider.get());
		}
		catch (final AACSLoyaltyException e)
		{
			LOG.error(e.getMessage());
			throw e;
		}
	}


	@Override
	public boolean isRegisterByCurrentBaseStore(final CustomerModel customer) throws AACSLoyaltyException
	{
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();

		return isRegister(customer, currentBaseStore);
	}


	@Override
	public boolean isRegisterByCurrentBaseStoreAndCustomer() throws AACSLoyaltyException
	{
		return isRegisterByCurrentBaseStore(getCurrentCustomer());
	}


	@Override
	public boolean registerCustomer(final CustomerModel customer, final BaseStoreModel baseStore) throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(baseStore), BASESTORE_MUSTN_T_BE_NULL);
		loyaltyValidationService.validateLoyaltyEnabled(customer, baseStore);
		final Optional<LoyaltyProgramProviderModel> provider = loyaltyProgramProviderContext.getProvider(baseStore);
		Preconditions.checkArgument(provider.isPresent(), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final Optional<LoyaltyProgramStrategy> strategy = getStrategy(provider.get().getClass());
		Preconditions.checkArgument(strategy.isPresent(), LOYALTY_STRATEGY_NOT_FOUND);


		return strategy.get().registerCustomer(customer, provider.get());
	}


	@Override
	public boolean registerCustomerByCurrentBaseStore(final CustomerModel customer) throws AACSLoyaltyException
	{
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();

		return registerCustomer(customer, currentBaseStore);
	}


	@Override
	public boolean registerCustomerByCurrentBaseStoreAndCustomer() throws AACSLoyaltyException
	{
		final CustomerModel currentCustomer = getCurrentCustomer();
		return registerCustomerByCurrentBaseStore(currentCustomer);
	}


	@Override
	public Optional<LoyaltyUsablePoints> getUsablePoints(final AbstractOrderModel order, final double orderTotalAmount)
			throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(order), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(order.getStore()), BASESTORE_MUSTN_T_BE_NULL);
		loyaltyValidationService.validateLoyaltyEnabledByOrder(order);
		final Optional<LoyaltyProgramProviderModel> provider = loyaltyProgramProviderContext.getProvider(order.getStore());
		Preconditions.checkArgument(provider.isPresent(), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final Optional<LoyaltyProgramStrategy> strategy = getStrategy(provider.get().getClass());
		Preconditions.checkArgument(strategy.isPresent(), LOYALTY_STRATEGY_NOT_FOUND);


		return strategy.get().getUsablePoints(order, orderTotalAmount, provider.get());
	}

	@Override
	public Optional<LoyaltyUsablePoints> getUsablePointsByOrder(final AbstractOrderModel order) throws AACSLoyaltyException
	{
		return getUsablePoints(order, order.getTotalPrice());
	}

	@Override
	public Optional<ValidateTransactionResult> validateTransaction(final AbstractOrderModel order) throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(order), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(order.getStore()), BASESTORE_MUSTN_T_BE_NULL);
		loyaltyValidationService.validateLoyaltyEnabledByOrder(order);
		final Optional<LoyaltyProgramProviderModel> provider = loyaltyProgramProviderContext.getProvider(order.getStore());
		Preconditions.checkArgument(provider.isPresent(), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final Optional<LoyaltyProgramStrategy> strategy = getStrategy(provider.get().getClass());
		Preconditions.checkArgument(strategy.isPresent(), LOYALTY_STRATEGY_NOT_FOUND);


		return strategy.get().validateTransactionByOrder(order, provider.get());

	}

	@Override
	public boolean createTransaction(final AbstractOrderModel order) throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(order), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(order.getStore()), BASESTORE_MUSTN_T_BE_NULL);
		loyaltyValidationService.validateLoyaltyEnabledByOrder(order);
		final Optional<LoyaltyProgramProviderModel> provider = loyaltyProgramProviderContext.getProvider(order.getStore());
		Preconditions.checkArgument(provider.isPresent(), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final Optional<LoyaltyProgramStrategy> strategy = getStrategy(provider.get().getClass());
		Preconditions.checkArgument(strategy.isPresent(), LOYALTY_STRATEGY_NOT_FOUND);

		return strategy.get().createTransaction(order, provider.get());

	}

	@Override
	public Optional<LoyaltyCustomerInfo> getLoyaltyCustomerInfo(final CustomerModel customer, final LoyaltyPagination pagination,
			final BaseStoreModel baseStore) throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(baseStore), BASESTORE_MUSTN_T_BE_NULL);
		loyaltyValidationService.validateLoyaltyEnabled(customer, baseStore);

		final Optional<LoyaltyProgramProviderModel> provider = loyaltyProgramProviderContext.getProvider(baseStore);
		Preconditions.checkArgument(provider.isPresent(), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final Optional<LoyaltyProgramStrategy> strategy = getStrategy(provider.get().getClass());
		Preconditions.checkArgument(strategy.isPresent(), LOYALTY_STRATEGY_NOT_FOUND);

		return strategy.get().getLoyaltyCustomerInfo(customer, pagination, provider.get());
	}


	@Override
	public Optional<LoyaltyCustomerInfo> getLoyaltyCustomerInfoByCurrentBaseStore(final CustomerModel customer,
			final LoyaltyPagination pagination) throws AACSLoyaltyException
	{
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		return getLoyaltyCustomerInfo(customer, pagination, currentBaseStore);
	}


	@Override
	public Optional<LoyaltyCustomerInfo> getLoyaltyCustomerInfoByCurrentBaseStoreAndCustomer(final LoyaltyPagination pagination)
			throws AACSLoyaltyException
	{
		return getLoyaltyCustomerInfoByCurrentBaseStore(getCurrentCustomer(), pagination);
	}


	@Override
	public Optional<LoyaltyCustomerCode> getLoyaltyCustomerCode(final CustomerModel customer, final BaseStoreModel baseStore)
			throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(baseStore), BASESTORE_MUSTN_T_BE_NULL);
		loyaltyValidationService.validateLoyaltyEnabled(customer, baseStore);
		final Optional<LoyaltyProgramProviderModel> provider = loyaltyProgramProviderContext.getProvider(baseStore);
		Preconditions.checkArgument(provider.isPresent(), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final Optional<LoyaltyProgramStrategy> strategy = getStrategy(provider.get().getClass());
		Preconditions.checkArgument(strategy.isPresent(), LOYALTY_STRATEGY_NOT_FOUND);


		return strategy.get().getLoyaltyCustomerCode(customer, provider.get());
	}


	@Override
	public Optional<LoyaltyCustomerCode> getLoyaltyCustomerCodeByCurrentBaseStore(final CustomerModel customer)
			throws AACSLoyaltyException
	{
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		return getLoyaltyCustomerCode(customer, currentBaseStore);
	}


	@Override
	public Optional<LoyaltyCustomerCode> getLoyaltyCustomerCodeByCurrentBaseStoreAndCustomer() throws AACSLoyaltyException
	{
		return getLoyaltyCustomerCodeByCurrentBaseStore(getCurrentCustomer());
	}

	private CustomerModel getCurrentCustomer()
	{
		final UserModel currentUser = userService.getCurrentUser();
		if (!Objects.isNull(currentUser) && currentUser instanceof CustomerModel)
		{
			return (CustomerModel) currentUser;
		}
		return null;
	}

	@Override
	public boolean cancelTransaction(final AbstractOrderModel order) throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(order), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(order.getStore()), BASESTORE_MUSTN_T_BE_NULL);
		loyaltyValidationService.validateLoyaltyEnabledByOrder(order);
		final Optional<LoyaltyProgramProviderModel> provider = loyaltyProgramProviderContext.getProvider(order.getStore());
		Preconditions.checkArgument(provider.isPresent(), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final Optional<LoyaltyProgramStrategy> strategy = getStrategy(provider.get().getClass());
		Preconditions.checkArgument(strategy.isPresent(), LOYALTY_STRATEGY_NOT_FOUND);

		return strategy.get().cancelTranscation(order, provider.get());

	}

	@Override
	public Optional<LoyaltyBalance> getLoyaltyBalance(final CustomerModel customer, final BaseStoreModel baseStore)
			throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(baseStore), BASESTORE_MUSTN_T_BE_NULL);
		loyaltyValidationService.validateLoyaltyEnabled(customer, baseStore);
		final Optional<LoyaltyProgramProviderModel> provider = loyaltyProgramProviderContext.getProvider(baseStore);
		Preconditions.checkArgument(provider.isPresent(), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final Optional<LoyaltyProgramStrategy> strategy = getStrategy(provider.get().getClass());
		Preconditions.checkArgument(strategy.isPresent(), LOYALTY_STRATEGY_NOT_FOUND);

		return strategy.get().getLoyaltyBalance(customer, provider.get());

	}

	@Override
	public Optional<LoyaltyBalance> getLoyaltyBalanceByCurrentBaseStore(final CustomerModel customer) throws AACSLoyaltyException
	{
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		return getLoyaltyBalance(customer, currentBaseStore);

	}

	@Override
	public Optional<LoyaltyBalance> getLoyaltyBalanceByCurrentBaseStoreAndCustomer() throws AACSLoyaltyException
	{
		return getLoyaltyBalanceByCurrentBaseStore(getCurrentCustomer());
	}

	@Override
	public void reserve(final AbstractOrderModel order, final double orderTotalAmount) throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(order), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(order.getStore()), BASESTORE_MUSTN_T_BE_NULL);
		loyaltyValidationService.validateLoyaltyEnabledByOrder(order);

		final Optional<LoyaltyProgramProviderModel> provider = loyaltyProgramProviderContext.getProvider(order.getStore());
		Preconditions.checkArgument(provider.isPresent(), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final Optional<LoyaltyProgramStrategy> strategy = getStrategy(provider.get().getClass());
		Preconditions.checkArgument(strategy.isPresent(), LOYALTY_STRATEGY_NOT_FOUND);


		strategy.get().reserve(order, orderTotalAmount, provider.get());

	}


	public boolean isLoyaltyEnabled(final BaseStoreModel baseStoreModel, final CustomerModel customer) throws AACSLoyaltyException
	{

		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(baseStoreModel), BASESTORE_MUSTN_T_BE_NULL);


		final Optional<LoyaltyProgramProviderModel> provider = loyaltyProgramProviderContext.getProvider(baseStoreModel);
		Preconditions.checkArgument(provider.isPresent(), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final Optional<LoyaltyProgramStrategy> strategy = getStrategy(provider.get().getClass());
		Preconditions.checkArgument(strategy.isPresent(), LOYALTY_STRATEGY_NOT_FOUND);

		return strategy.get().isLoyaltyEnabled(provider.get(), baseStoreModel, customer);
	}

	public boolean isLoyaltyEnabledByCurrentBaseStore(final CustomerModel customer) throws AACSLoyaltyException
	{

		return isLoyaltyEnabled(baseStoreService.getCurrentBaseStore(), customer);
	}

	public boolean isLoyaltyEnabledByCurrentBaseStoreAndCurrentCustomer() throws AACSLoyaltyException
	{
		return isLoyaltyEnabledByCurrentBaseStore(getCurrentCustomer());
	}

	@Override
	public boolean isLoyaltyEnabled(final AbstractOrderModel order) throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(order), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(order.getUser()), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(order.getStore()), BASESTORE_MUSTN_T_BE_NULL);

		if (!(order.getUser() instanceof CustomerModel))
		{
			return false;
		}

		return isLoyaltyEnabled(order.getStore(), (CustomerModel) order.getUser());
	}

	@Override
	public void resetLoyalty(final AbstractOrderModel order)
	{
		Preconditions.checkArgument(!Objects.isNull(order), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(order.getStore()), BASESTORE_MUSTN_T_BE_NULL);

		final Optional<LoyaltyProgramProviderModel> provider = loyaltyProgramProviderContext.getProvider(order.getStore());
		Preconditions.checkArgument(provider.isPresent(), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final Optional<LoyaltyProgramStrategy> strategy = getStrategy(provider.get().getClass());
		Preconditions.checkArgument(strategy.isPresent(), LOYALTY_STRATEGY_NOT_FOUND);

		try
		{
			strategy.get().resetLoyalty(order, provider.get());
		}
		catch (final AACSLoyaltyException e)
		{
			LOG.error(e.getMessage());
		}

	}

	@Override
	public boolean fullyRefundLoyalty(final OrderModel order)
	{
		Preconditions.checkArgument(!Objects.isNull(order), ABSTRACT_ORDER_NULL_MSG);
		return refundLoyaltyAmount(order, order.getTotalPriceAfterRedeem());
	}

	@Override
	public boolean refundLoyalty(final ReturnRequestModel returnRequest)
	{
		Preconditions.checkArgument(!Objects.isNull(returnRequest), RETURN_REQUEST_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(returnRequest.getOrder()), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(returnRequest.getOrder().getStore()), BASESTORE_MUSTN_T_BE_NULL);

		final LoyaltyProgramProviderModel provider = loyaltyProgramProviderContext.getProvider(returnRequest.getOrder().getStore())
				.orElse(null);
		Preconditions.checkNotNull(provider, LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final LoyaltyProgramStrategy strategy = getStrategy(provider.getClass()).orElse(null);
		Preconditions.checkNotNull(strategy, LOYALTY_STRATEGY_NOT_FOUND);

		try
		{
			strategy.refundTransaction(provider, returnRequest.getOrder().getStore(), returnRequest);
			return true;
		}
		catch (final AACSLoyaltyException e)
		{
			LOG.error(e.getMessage());
		}

		return false;
	}

	@Override
	public boolean refundLoyaltyAmount(final OrderModel order, final double amount)
	{
		Preconditions.checkArgument(!Objects.isNull(order), ABSTRACT_ORDER_NULL_MSG);
		final BaseStoreModel store = order.getStore();
		Preconditions.checkArgument(!Objects.isNull(store), BASESTORE_MUSTN_T_BE_NULL);
		final LoyaltyProgramProviderModel provider = loyaltyProgramProviderContext.getProvider(store).orElse(null);
		Preconditions.checkNotNull(provider, LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final LoyaltyProgramStrategy strategy = getStrategy(provider.getClass()).orElse(null);
		Preconditions.checkNotNull(strategy, LOYALTY_STRATEGY_NOT_FOUND);

		try
		{
			strategy.refundTransaction(provider, store, order, amount);
			return true;
		}
		catch (final AACSLoyaltyException e)
		{
			LOG.error(e.getMessage());
		}

		return false;
	}


}
