/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramprovider.beans;

/**
 *
 */
public class LoyaltyBalance
{
	private double points;

	private double value;

	private double pointExchangeRate;

	/**
	 * @return the points
	 */
	public double getPoints()
	{
		return points;
	}

	/**
	 * @param points
	 *           the points to set
	 */
	public void setPoints(final double points)
	{
		this.points = points;
	}

	/**
	 * @return the value
	 */
	public double getValue()
	{
		return value;
	}

	/**
	 * @param value
	 *           the value to set
	 */
	public void setValue(final double value)
	{
		this.value = value;
	}

	/**
	 * @return the pointExchangeRate
	 */
	public double getPointExchangeRate()
	{
		return pointExchangeRate;
	}

	/**
	 * @param pointExchangeRate
	 *           the pointExchangeRate to set
	 */
	public void setPointExchangeRate(final double pointExchangeRate)
	{
		this.pointExchangeRate = pointExchangeRate;
	}


}
