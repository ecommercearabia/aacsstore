/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramprovider.beans;

/**
 *
 */
public class LoyaltyUsablePoints
{
	private double redeemAmount;

	private double redeemValue;

	private double issueAmount;

	private double issueValue;

	private double balancePoints;

	private double exhcangeRate;

	/**
	 * @return the redeemAmount
	 */
	public double getRedeemAmount()
	{
		return redeemAmount;
	}

	/**
	 * @param redeemAmount
	 *           the redeemAmount to set
	 */
	public void setRedeemAmount(final double redeemAmount)
	{
		this.redeemAmount = redeemAmount;
	}

	/**
	 * @return the redeemValue
	 */
	public double getRedeemValue()
	{
		return redeemValue;
	}

	/**
	 * @param redeemValue
	 *           the redeemValue to set
	 */
	public void setRedeemValue(final double redeemValue)
	{
		this.redeemValue = redeemValue;
	}

	/**
	 * @return the issueAmount
	 */
	public double getIssueAmount()
	{
		return issueAmount;
	}

	/**
	 * @param issueAmount
	 *           the issueAmount to set
	 */
	public void setIssueAmount(final double issueAmount)
	{
		this.issueAmount = issueAmount;
	}

	/**
	 * @return the issueValue
	 */
	public double getIssueValue()
	{
		return issueValue;
	}

	/**
	 * @param issueValue
	 *           the issueValue to set
	 */
	public void setIssueValue(final double issueValue)
	{
		this.issueValue = issueValue;
	}

	/**
	 * @return the balancePoints
	 */
	public double getBalancePoints()
	{
		return balancePoints;
	}

	/**
	 * @param balancePoints
	 *           the balancePoints to set
	 */
	public void setBalancePoints(final double balancePoints)
	{
		this.balancePoints = balancePoints;
	}

	/**
	 * @return the exhcangeRate
	 */
	public double getExhcangeRate()
	{
		return exhcangeRate;
	}

	/**
	 * @param exhcangeRate
	 *           the exhcangeRate to set
	 */
	public void setExhcangeRate(final double exhcangeRate)
	{
		this.exhcangeRate = exhcangeRate;
	}


}
