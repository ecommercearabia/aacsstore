/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramprovider.strategy.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;
import javax.ws.rs.NotSupportedException;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aacs.aacsloyaltyprogramprovider.beans.CustomerHistory;
import com.aacs.aacsloyaltyprogramprovider.beans.LoyaltyBalance;
import com.aacs.aacsloyaltyprogramprovider.beans.LoyaltyCustomerCode;
import com.aacs.aacsloyaltyprogramprovider.beans.LoyaltyCustomerInfo;
import com.aacs.aacsloyaltyprogramprovider.beans.LoyaltyPagination;
import com.aacs.aacsloyaltyprogramprovider.beans.LoyaltyUsablePoints;
import com.aacs.aacsloyaltyprogramprovider.beans.ValidateTransactionResult;
import com.aacs.aacsloyaltyprogramprovider.enums.LoyaltyPaymentModeType;
import com.aacs.aacsloyaltyprogramprovider.enums.LoyaltyPaymentStatus;
import com.aacs.aacsloyaltyprogramprovider.enums.LoyaltyPaymentType;
import com.aacs.aacsloyaltyprogramprovider.exception.AACSLoyaltyException;
import com.aacs.aacsloyaltyprogramprovider.exception.type.AACSLoyaltyExceptionType;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.Customer;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.CustomerPointLogs;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.CustomerPointSummary;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.CustomerQRCode;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.ExchangeRate;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.GiiftCredential;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.Log;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.Pagination;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.Points;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.RefundTransactionResponse;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.Transaction;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.TransactionResponse;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.VaildateTransactionResponse;
import com.aacs.aacsloyaltyprogramprovider.giift.enums.CancelStatusOption;
import com.aacs.aacsloyaltyprogramprovider.giift.exception.GiiftException;
import com.aacs.aacsloyaltyprogramprovider.giift.service.GiiftCustomerService;
import com.aacs.aacsloyaltyprogramprovider.model.GiiftLoyaltyProgramProviderModel;
import com.aacs.aacsloyaltyprogramprovider.model.LoyaltyPaymentModeModel;
import com.aacs.aacsloyaltyprogramprovider.model.LoyaltyProgramProviderModel;
import com.aacs.aacsloyaltyprogramprovider.service.LoyaltyPaymentModeService;
import com.aacs.aacsloyaltyprogramprovider.service.LoyaltyValidationService;
import com.aacs.aacsloyaltyprogramprovider.strategy.LoyaltyProgramStrategy;
import com.google.common.base.Preconditions;




/**
 *
 */
public class DefaultGiiftLoyaltyStrategy implements LoyaltyProgramStrategy
{
	private static final String CUSTOMER_SHALL_NOT_BE_NULL = "Customer Shall not be null";
	private static final Logger LOG = LoggerFactory.getLogger(DefaultGiiftLoyaltyStrategy.class);
	private static final String LOYALTY_PROGRAM_PROVIDER_NULL_MSG = "loyalty program provider is null";
	private static final String ABSTRACT_ORDER_NULL_MSG = "order is null";
	private static final String CUSTOMER_ID_EMPTY_MSG = "customerId  is null or empty";
	private static final String TRANSACTION_VAILDATE_ID = "transactionVaildateId  is null or empty";
	private static final String BASE_STORE_NULL_MSG = "baseStore  is null";

	private static final String CUSTOMER_NULL_MSG = "customer  is null";
	private static final String LOG_ERROR_MSG = "[DefaultGiiftLoyaltyStrategy]: {}";


	@Resource(name = "giiftCustomerService")
	private GiiftCustomerService giiftCustomerService;

	@Resource(name = "giiftCustomerConverter")
	private Converter<CustomerModel, Customer> customerConverter;

	@Resource(name = "giiftTransactionConverter")
	private Converter<AbstractOrderModel, Transaction> transactionConverter;

	@Resource(name = "giiftCustomerHistoryLogConverter")
	private Converter<Log, CustomerHistory> customerHistoryLogConverter;

	@Resource(name = "loyaltyValidationService")
	private LoyaltyValidationService loyaltyValidationService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "loyaltyPaymentModeService")
	private LoyaltyPaymentModeService loyaltyPaymentModeService;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;



	@Override
	public boolean isRegister(final CustomerModel customer, final LoyaltyProgramProviderModel provider) throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		loyaltyValidationService.validateLoyaltyEnabledByCurrentStore(customer);
		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);
		final Customer giiftCustomer = customerConverter.convert(customer);
		return giiftCustomerService.isCustomerRegistered(getGiiftCredential(giiftProvider), giiftCustomer.getId());
	}

	/**
	 *
	 */
	private GiiftCredential getGiiftCredential(final GiiftLoyaltyProgramProviderModel giiftProvider)
	{
		final GiiftCredential giiftCredential = new GiiftCredential();
		giiftCredential.setBaseURL(giiftProvider.getBaseUrl());
		giiftCredential.setKey(giiftProvider.getKey());
		giiftCredential.setVector(giiftProvider.getVector());
		giiftCredential.setMerchantId(giiftProvider.getMerchantId());

		final BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();
		if (Objects.nonNull(currentBaseStore))
		{
			giiftCredential.setTimeout(currentBaseStore.getLoyaltyProgramTimeoutLimit());
			giiftCredential.setTimeoutEnabled(currentBaseStore.isLoyaltyProgramTimeoutEnabled());
		}



		return giiftCredential;

	}

	@Override
	public boolean registerCustomer(final CustomerModel customer, final LoyaltyProgramProviderModel provider)
			throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_ID_EMPTY_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);
		loyaltyValidationService.validateLoyaltyEnabledByCurrentStore(customer);
		final Customer giiftCustomer = customerConverter.convert(customer);

		return giiftCustomerService.registerCustomer(getGiiftCredential(giiftProvider), giiftCustomer);
	}

	@Override
	public Optional<LoyaltyUsablePoints> getUsablePoints(final AbstractOrderModel order, final double orderTotalAmount,
			final LoyaltyProgramProviderModel provider) throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(order), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(order.getUser()), CUSTOMER_ID_EMPTY_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);
		Preconditions.checkArgument(!Objects.isNull(order.getStore()), BASE_STORE_NULL_MSG);
		loyaltyValidationService.validateLoyaltyEnabledByOrder(order);


		final Transaction transaction = transactionConverter.convert(order);
		if (equalToZero(getOrderTotalWithTax(order)))
		{
			transaction.setInitialAmount(getOrderTotalWithTax(order)
					+ (order.getExpectedLoyaltyRedeemAmount() != null ? order.getExpectedLoyaltyRedeemAmount() : 0d));
		}
		else
		{
			transaction.setInitialAmount(getOrderTotalWithTax(order));
		}
		final Optional<Points> usablePoints = giiftCustomerService.getUsablePoints(getGiiftCredential(giiftProvider), transaction);
		final Optional<ExchangeRate> pointExchangeRate = giiftCustomerService
				.getPointExchangeRate(getGiiftCredential(giiftProvider));


		if (usablePoints.isEmpty())
		{
			return Optional.empty();
		}
		if (pointExchangeRate.isPresent())
		{
			order.setLoyaltyExchangeRate(pointExchangeRate.get().getPointValue());
		}

		validateUsablePoints(order, giiftProvider, usablePoints.get());
		modelService.save(order);
		return Optional.ofNullable(getLoyaltyUsablePoints(usablePoints.get()));
	}

	private void validateUsablePoints(final AbstractOrderModel order, final GiiftLoyaltyProgramProviderModel giiftProvider,
			final Points usablePoints) throws AACSLoyaltyException
	{
		final Optional<LoyaltyBalance> loyaltyBalance = getLoyaltyBalance((CustomerModel) order.getUser(), giiftProvider);

		if (loyaltyBalance.isPresent() && equalToZero(usablePoints.getRedeemAmount()) && order.getLoyaltyAmount() > 0.0
				&& loyaltyBalance.get().getPoints() > order.getLoyaltyAmount())
		{
			order.setUsableLoyaltyRedeemPoints(loyaltyBalance.get().getPoints() - order.getRedeemedLoyaltyPoints());
			order.setUsableLoyaltyRedeemAmount(loyaltyBalance.get().getValue() - order.getLoyaltyAmount());
		}
		else if (equalToZero(getOrderTotalWithTax(order)) && order.getLoyaltyAmount() > 0)
		{
			order.setUsableLoyaltyRedeemAmount(0d);
			order.setUsableLoyaltyRedeemPoints(0d);
		}
		else
		{
			order.setUsableLoyaltyRedeemAmount(usablePoints.getRedeemValue());
			order.setUsableLoyaltyRedeemPoints(usablePoints.getRedeemAmount());
		}
	}

	private boolean equalToZero(final double value)
	{
		final double epsilon = 0.0000001;
		return value < epsilon;
	}

	@Override
	public Optional<ValidateTransactionResult> validateTransaction(final AbstractOrderModel order, final double orderTotalAmount,
			final LoyaltyProgramProviderModel provider) throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(order), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(order.getUser()), CUSTOMER_ID_EMPTY_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(order.getStore()), BASE_STORE_NULL_MSG);
		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);
		loyaltyValidationService.validateLoyaltyEnabledByOrder(order);


		if (Strings.isNotBlank(order.getGiiftValidateId()))
		{
			cancelTranscation(order, giiftProvider);
		}

		Optional<VaildateTransactionResponse> validateTransaction;
		final Transaction transaction = transactionConverter.convert(order);
		transaction.setInitialAmount(orderTotalAmount);
		try
		{
			order.setLoyaltyPaymentType(LoyaltyPaymentType.AUTHORIZE);
			validateTransaction = giiftCustomerService.validateTransaction(getGiiftCredential(giiftProvider), transaction);
			order.setLoyaltyPaymentStatus(LoyaltyPaymentStatus.SUCCESS);
		}
		catch (final GiiftException e)
		{
			order.setExpectedAddedPoint(0.0);
			order.setLoyaltyPaymentStatus(LoyaltyPaymentStatus.FAILED);
			modelService.save(order);
			throw new AACSLoyaltyException(e.getMessage(), e.getData(), AACSLoyaltyExceptionType.GIIFT);
		}
		if (validateTransaction.isEmpty() || validateTransaction.get().getPoints() == null)
		{
			return Optional.empty();
		}

		order.setGiiftValidateId(validateTransaction.get().getValidateID());
		final Points points = validateTransaction.get().getPoints();
		order.setExpectedAddedPoint(points.getIssueAmount());
		order.setExpectedLoyaltyRedeemAmount(points.getRedeemValue());
		order.setRedeemedLoyaltyPoints(points.getRedeemAmount());
		order.setLoyaltyAmount(points.getRedeemValue());
		modelService.save(order);
		return Optional.ofNullable(getValidateTransactionResult(validateTransaction.get()));
	}

	@Override
	public Optional<ValidateTransactionResult> validateTransactionByOrder(final AbstractOrderModel order,
			final LoyaltyProgramProviderModel provider) throws AACSLoyaltyException
	{

		return validateTransaction(order, getOrderTotalWithTax(order), provider);
	}

	protected double formatDouble(final double val)
	{
		final DecimalFormat df = new DecimalFormat("#.00");
		final String format = df.format(val);
		return Double.valueOf(format);
	}

	@Override
	public boolean createTransaction(final AbstractOrderModel order, final LoyaltyProgramProviderModel provider)
			throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(order), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(order.getUser()), CUSTOMER_ID_EMPTY_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);
		Preconditions.checkArgument(!Objects.isNull(order.getStore()), BASE_STORE_NULL_MSG);
		loyaltyValidationService.validateLoyaltyEnabledByOrder(order);

		final Optional<TransactionResponse> createTransaction;
		final double orderTotalAmount = formatDouble(getOrderTotalWithTax(order)
				+ (order.getExpectedLoyaltyRedeemAmount() != null ? order.getExpectedLoyaltyRedeemAmount() : 0d));
		final Optional<ValidateTransactionResult> validateTransaction = validateTransaction(order, orderTotalAmount, giiftProvider);
		if (validateTransaction.isEmpty())
		{
			return false;
		}

		final ValidateTransactionResult validateTransactionResult = validateTransaction.get();
		order.setTotalPriceAfterRedeem(validateTransactionResult.getFinalAmount());
		if (!Objects.isNull(validateTransactionResult.getPoints()))
		{
			order.setRedeemedLoyaltyPoints(validateTransactionResult.getPoints().getRedeemAmount());
			order.setExpectedLoyaltyRedeemAmount(validateTransactionResult.getPoints().getRedeemValue());
		}
		order.setGiiftValidateId(validateTransactionResult.getValidateID());
		final Transaction transaction = transactionConverter.convert(order);
		transaction.setInitialAmount(orderTotalAmount);
		try
		{
			createTransaction = giiftCustomerService.createTransaction(getGiiftCredential(giiftProvider), transaction);
		}
		catch (final GiiftException e)
		{
			order.setLoyaltyFailureReason(e.getMessage());
			order.setLoyaltyPaymentStatus(LoyaltyPaymentStatus.FAILED);
			modelService.save(order);
			throw e;
		}

		if (createTransaction.isPresent())
		{
			order.setGiiftTransactionId(createTransaction.get().getTransactionId());
			order.setLoyaltyPaymentStatus(LoyaltyPaymentStatus.SUCCESS);
			modelService.save(order);
			return true;
		}
		return false;
	}

	@Override
	public Optional<LoyaltyCustomerInfo> getLoyaltyCustomerInfo(final CustomerModel customer, final LoyaltyPagination pagination,
			final LoyaltyProgramProviderModel provider) throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		loyaltyValidationService.validateLoyaltyEnabledByCurrentStore(customer);
		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);
		final Customer giiftCustomer = customerConverter.convert(customer);

		Optional<LoyaltyBalance> loyaltyBalance = Optional.empty();
		try
		{
			loyaltyBalance = getLoyaltyBalance(customer, giiftProvider);
		}
		catch (final AACSLoyaltyException ex)
		{
			LOG.warn(ex.getMessage());
		}

		Optional<CustomerPointLogs> customerPointLogs = null;
		try
		{
			customerPointLogs = giiftCustomerService.getCustomerPointLogs(getGiiftCredential(giiftProvider), giiftCustomer.getId(),
					getGiiftPagintation(pagination));
		}
		catch (final GiiftException ex)
		{
			LOG.warn(ex.getMessage());
			throw new AACSLoyaltyException(ex.getMessage(), ex, AACSLoyaltyExceptionType.GIIFT);
		}

		final LoyaltyCustomerInfo loyaltyCustomerInfoBean = getLoyaltyCustomerInfoBean(customerPointLogs, loyaltyBalance);
		try
		{

			final Optional<LoyaltyCustomerCode> loyaltyCustomerCode = getLoyaltyCustomerCode(customer, giiftProvider);
			if (loyaltyCustomerCode.isPresent())
			{
				loyaltyCustomerInfoBean.setCustomerId(loyaltyCustomerCode.get().getCustomerCode());
			}
		}
		catch (final AACSLoyaltyException ex)
		{
			LOG.warn(ex.getMessage());
			throw ex;
		}


		return Optional.ofNullable(loyaltyCustomerInfoBean);
	}

	/**
	 *
	 */
	private Pagination getGiiftPagintation(final LoyaltyPagination pagination)
	{
		if (pagination == null)
		{
			return null;
		}
		final Pagination giifPagination = new Pagination();
		giifPagination.setPageIndex(pagination.getPageIndex());
		giifPagination.setPageSize(pagination.getPageSize());
		return giifPagination;
	}

	/**
	 *
	 */
	private LoyaltyCustomerInfo getLoyaltyCustomerInfoBean(final Optional<CustomerPointLogs> customerPointLogs,
			final Optional<LoyaltyBalance> loyaltyBalance)
	{
		final LoyaltyCustomerInfo customerInfo = new LoyaltyCustomerInfo();
		if (loyaltyBalance.isPresent())
		{
			customerInfo.setBalance(loyaltyBalance.get());
		}

		if (customerPointLogs.isPresent())
		{
			final CustomerPointLogs pointLogs = customerPointLogs.get();
			final List<Log> logs = pointLogs.getLogs();
			customerInfo.setHistories(customerHistoryLogConverter.convertAll(logs));
			customerInfo.setPageIndex(pointLogs.getPageIndex());
			customerInfo.setPageSize(pointLogs.getPageSize());
			customerInfo.setTotalCount(pointLogs.getTotalCount());
			customerInfo.setTotalPage(pointLogs.getTotalPage());

		}

		return customerInfo;
	}

	@Override
	public Optional<LoyaltyCustomerCode> getLoyaltyCustomerCode(final CustomerModel customer,
			final LoyaltyProgramProviderModel provider) throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		loyaltyValidationService.validateLoyaltyEnabledByCurrentStore(customer);

		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);
		Optional<CustomerQRCode> customerQRCode;
		final Customer giiftCustomer = customerConverter.convert(customer);

		customerQRCode = giiftCustomerService.getCustomerQRCode(getGiiftCredential(giiftProvider), giiftCustomer.getId());


		return Optional.ofNullable(getCustomerCode(customerQRCode));
	}


	/**
	 *
	 */
	private LoyaltyCustomerCode getCustomerCode(final Optional<CustomerQRCode> customerQRCode)
	{
		final LoyaltyCustomerCode customerCode = new LoyaltyCustomerCode();
		if (customerQRCode.isPresent())
		{
			customerCode.setCustomerCode(customerQRCode.get().getCustomerCode());
			customerCode.setCustomerID(customerQRCode.get().getCustomerID());
			customerCode.setQrCode(customerQRCode.get().getQrCode());
		}
		return customerCode;

	}

	private GiiftLoyaltyProgramProviderModel getGiiftProvider(final LoyaltyProgramProviderModel providerModel)
	{
		if (!(providerModel instanceof GiiftLoyaltyProgramProviderModel))
		{
			throw new NotSupportedException("");
		}
		return (GiiftLoyaltyProgramProviderModel) providerModel;
	}

	protected LoyaltyUsablePoints getLoyaltyUsablePoints(final Points points)
	{
		final LoyaltyUsablePoints loyaltyUsablePoints = new LoyaltyUsablePoints();
		loyaltyUsablePoints.setBalancePoints(points.getBalancePoints());
		loyaltyUsablePoints.setIssueAmount(points.getIssueAmount());
		loyaltyUsablePoints.setIssueValue(points.getIssueValue());
		loyaltyUsablePoints.setRedeemAmount(points.getRedeemAmount());
		loyaltyUsablePoints.setRedeemValue(points.getRedeemValue());
		return loyaltyUsablePoints;
	}

	protected ValidateTransactionResult getValidateTransactionResult(final VaildateTransactionResponse response)
	{
		final ValidateTransactionResult result = new ValidateTransactionResult();
		result.setCustomerId(response.getCustomerId());
		result.setFinalAmount(response.getFinalAmount());
		result.setInitialAmount(response.getInitialAmount());
		result.setValidateID(response.getValidateID());
		result.setPoints(getLoyaltyUsablePoints(response.getPoints()));
		return result;
	}

	@Override
	public boolean cancelTranscation(final AbstractOrderModel order, final LoyaltyProgramProviderModel provider)
			throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(order), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(Strings.isNotBlank(order.getGiiftValidateId()), TRANSACTION_VAILDATE_ID);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		loyaltyValidationService.validateLoyaltyEnabledByOrder(order);
		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);

		try
		{
			final boolean cancelTransaction = giiftCustomerService.cancelTransaction(getGiiftCredential(giiftProvider),
					order.getGiiftValidateId(), CancelStatusOption.CANCEL);

			if (cancelTransaction)
			{
				order.setGiiftValidateId(Strings.EMPTY);
				modelService.save(order);
			}

			return cancelTransaction;

		}
		catch (final GiiftException e)
		{
			LOG.error(LOG_ERROR_MSG, e.getMessage());
			return false;
		}

	}

	@Override
	public Optional<LoyaltyBalance> getLoyaltyBalance(final CustomerModel customer, final LoyaltyProgramProviderModel provider)
			throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);
		loyaltyValidationService.validateLoyaltyEnabledByCurrentStore(customer);
		final Customer giiftCustomer = customerConverter.convert(customer);

		final Optional<CustomerPointSummary> customerPointSummary = giiftCustomerService
				.getCustomerPointSummary(getGiiftCredential(giiftProvider), giiftCustomer.getId());

		return customerPointSummary.isPresent() ? Optional.ofNullable(getBalanceBean(customerPointSummary.get()))
				: Optional.empty();
	}

	/**
	 *
	 */
	private LoyaltyBalance getBalanceBean(final CustomerPointSummary customerPointSummary)
	{
		final LoyaltyBalance balance = new LoyaltyBalance();
		balance.setPoints(customerPointSummary.getPoints());
		balance.setValue(customerPointSummary.getValue());
		balance.setPointExchangeRate(customerPointSummary.getPointExchangeRate());
		return balance;
	}

	@Override
	public void reserve(final AbstractOrderModel order, final double orderTotalAmount, final LoyaltyProgramProviderModel provider)
			throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(order), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(order.getUser()), CUSTOMER_ID_EMPTY_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);
		Preconditions.checkArgument(!Objects.isNull(order.getStore()), BASE_STORE_NULL_MSG);
		loyaltyValidationService.validateLoyaltyEnabledByOrder(order);

		if (LoyaltyPaymentType.CAPTURED.equals(order.getLoyaltyPaymentType()))
		{
			return;
		}
		try
		{
			cancelTranscation(order, giiftProvider);
		}
		catch (final Exception ex)
		{
			LOG.warn(ex.getMessage());
		}

		validateAmount(order, orderTotalAmount, provider);

		validateTransaction(order, orderTotalAmount, giiftProvider);
		modelService.refresh(order);
		modelService.save(order);

	}

	private void validateAmount(final AbstractOrderModel order, final double orderTotalAmount,
			final LoyaltyProgramProviderModel provider) throws AACSLoyaltyException
	{
		final boolean isSpecific = (order.getLoyaltyPaymentMode() != null
				&& LoyaltyPaymentModeType.REDEEM_SPECIFIC_AMOUNT.equals(order.getLoyaltyPaymentMode().getLoyaltyPaymentModeType()));
		final boolean isFull = (order.getLoyaltyPaymentMode() != null
				&& LoyaltyPaymentModeType.REDEEM_FULL_AMOUNT.equals(order.getLoyaltyPaymentMode().getLoyaltyPaymentModeType()));

		if (orderTotalAmount == 0.0)
		{
			redeemNon(order);
		}

		final Optional<LoyaltyUsablePoints> usablePoints = getUsablePoints(order, orderTotalAmount, provider);
		if (usablePoints.isEmpty() || (isSpecific && equalToZero(order.getLoyaltyAmountSelected()))
				|| order.getLoyaltyPaymentMode() == null
				|| LoyaltyPaymentModeType.REDEEM_NONE.equals(order.getLoyaltyPaymentMode().getLoyaltyPaymentModeType()))
		{
			redeemNon(order);
		}
		else if ((order.getLoyaltyAmountSelected() >= usablePoints.get().getRedeemValue() && isSpecific)
				|| (isFull && equalToZero(order.getLoyaltyAmountSelected())))
		{
			redeemFull(order, usablePoints.get());
		}
		else if (isFull)
		{
			redeemFull(order, usablePoints.get());
		}
	}

	private void redeemNon(final AbstractOrderModel order)
	{
		order.setLoyaltyAmountSelected(0d);
		order.setLoyaltyAmount(0d);
		final Optional<LoyaltyPaymentModeModel> loyaltyPaymentMode = loyaltyPaymentModeService
				.getLoyaltyPaymentModeByCurrentBaseStore(LoyaltyPaymentModeType.REDEEM_NONE);

		order.setLoyaltyPaymentMode(loyaltyPaymentMode.isPresent() ? loyaltyPaymentMode.get() : null);
	}

	private void redeemFull(final AbstractOrderModel order, final LoyaltyUsablePoints usablePoints)
	{
		order.setLoyaltyAmountSelected(usablePoints.getRedeemValue());
		final Optional<LoyaltyPaymentModeModel> loyaltyPaymentMode = loyaltyPaymentModeService
				.getLoyaltyPaymentModeByCurrentBaseStore(LoyaltyPaymentModeType.REDEEM_FULL_AMOUNT);
		order.setLoyaltyPaymentMode(loyaltyPaymentMode.isPresent() ? loyaltyPaymentMode.get() : null);
	}


	@Override
	public void resetLoyalty(final AbstractOrderModel order, final LoyaltyProgramProviderModel provider)
			throws AACSLoyaltyException
	{
		Preconditions.checkArgument(Objects.nonNull(order), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(Objects.nonNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);

		if (Strings.isNotBlank(order.getGiiftValidateId()))
		{
			cancelTranscation(order, provider);
		}
		resetLoyaltyOnOrder(order);
	}

	/**
	 * @param order
	 */
	protected void resetLoyaltyOnOrder(final AbstractOrderModel order)
	{

		order.setUsableLoyaltyRedeemAmount(null);
		order.setLoyaltyExchangeRate(0);
		order.setUsableLoyaltyRedeemPoints(null);
		order.setLoyaltyAmount(0);
		order.setLoyaltyExchangeRate(0);
		order.setLoyaltyFailureReason(null);
		order.setLoyaltyAmountSelected(0);
		order.setLoyaltyPaymentMode(null);
		order.setLoyaltyPaymentType(null);
		order.setRedeemedLoyaltyPoints(0);
		order.setTotalPriceAfterRedeem(0);
		order.setExpectedAddedPoint(null);
		order.setExpectedLoyaltyRedeemAmount(null);
		modelService.save(order);
	}

	private double calcTotalWithTax(final AbstractOrderModel source)
	{
		if (source == null)
		{
			throw new IllegalArgumentException("source order must not be null");
		}
		if (source.getTotalPrice() == null)
		{
			return 0.0d;
		}

		BigDecimal totalPrice = BigDecimal.valueOf(source.getTotalPrice().doubleValue());

		// Add the taxes to the total price if the cart is net; if the total was null taxes should be null as well
		if (Boolean.TRUE.equals(source.getNet()) && totalPrice.compareTo(BigDecimal.ZERO) != 0 && source.getTotalTax() != null)
		{
			totalPrice = totalPrice.add(BigDecimal.valueOf(source.getTotalTax().doubleValue()));
		}
		return totalPrice.doubleValue();
	}

	/**
	 * Gets the total price.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the total price
	 */
	protected Double getOrderTotalWithTax(final AbstractOrderModel abstractOrderModel)
	{
		final CurrencyModel cartCurrency = abstractOrderModel.getCurrency();
		final CurrencyModel baseCurrency = commonI18NService.getBaseCurrency();
		final double calcTotalWithTax = calcTotalWithTax(abstractOrderModel);

		return commonI18NService.convertAndRoundCurrency(cartCurrency.getConversion().doubleValue(),
				baseCurrency.getConversion().doubleValue(), cartCurrency.getDisplayDigits(), calcTotalWithTax);
	}

	/**
	 * @return the baseStoreService
	 */
	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	@Override
	public boolean isLoyaltyEnabled(final LoyaltyProgramProviderModel provider, final BaseStoreModel baseStoreModel,
			final CustomerModel customer) throws AACSLoyaltyException
	{

		Preconditions.checkArgument(Objects.nonNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(Objects.nonNull(baseStoreModel), BASE_STORE_NULL_MSG);
		Preconditions.checkArgument(Objects.nonNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);

		if (!baseStoreModel.isLoyaltyProgramEnabled() || !customer.isInvolvedInLoyaltyProgram())
		{
			return false;
		}

		if (customer.isInvolvedInLoyaltyProgram() && !isRegister(customer, provider, baseStoreModel))
		{
			registerCustomer(customer, provider, baseStoreModel);
		}

		return true;
	}

	protected boolean registerCustomer(final CustomerModel customer, final LoyaltyProgramProviderModel provider,
			final BaseStoreModel baseStoreModel) throws AACSLoyaltyException
	{
		validate(customer, provider, baseStoreModel);
		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);
		loyaltyValidationService.validateLoyaltyEnabled(customer, baseStoreModel);
		final Customer giiftCustomer = customerConverter.convert(customer);
		return giiftCustomerService.registerCustomer(getGiiftCredential(giiftProvider), giiftCustomer);
	}

	protected boolean isRegister(final CustomerModel customer, final LoyaltyProgramProviderModel provider,
			final BaseStoreModel baseStoreModel) throws AACSLoyaltyException
	{
		validate(customer, provider, baseStoreModel);
		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);
		final Customer giiftCustomer = customerConverter.convert(customer);
		return giiftCustomerService.isCustomerRegistered(getGiiftCredential(giiftProvider), giiftCustomer.getId());
	}

	private void validate(final CustomerModel customer, final LoyaltyProgramProviderModel provider,
			final BaseStoreModel baseStoreModel) throws AACSLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(baseStoreModel), BASE_STORE_NULL_MSG);
		loyaltyValidationService.validateLoyaltyEnabled(customer, baseStoreModel);
	}


	@Override
	public void refundTransaction(final LoyaltyProgramProviderModel provider, final BaseStoreModel baseStoreModel,
			final ReturnRequestModel returnRequest) throws AACSLoyaltyException
	{
		Preconditions.checkNotNull(returnRequest, "Return Request Shall not be null");
		Preconditions.checkNotNull(baseStoreModel, BASE_STORE_NULL_MSG);
		Preconditions.checkNotNull(provider, LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		Preconditions.checkNotNull(returnRequest.getOrder(), "Order Shall not be null");
		Preconditions.checkNotNull(returnRequest.getOrder().getUser(), CUSTOMER_SHALL_NOT_BE_NULL);

		final OrderModel order = returnRequest.getOrder();
		final double amount = calculateRefundAmount(returnRequest);
		final String giiftTransactionId = order.getGiiftTransactionId();
		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);
		final RefundTransactionResponse refundTransaction = giiftCustomerService
				.refundTransaction(getGiiftCredential(giiftProvider), giiftTransactionId, amount).orElse(null);

		if (refundTransaction == null)
		{
			LOG.error("RefundTransaction is null, please check the logs");
			throw new AACSLoyaltyException("RefundTransaction is null, please check the logs", returnRequest,
					AACSLoyaltyExceptionType.GIIFT);
		}

	}

	private double calculateRefundAmount(final ReturnRequestModel returnRequest)
	{
		double doubleValue = returnRequest.getSubtotal().doubleValue();
		if (returnRequest.getRefundDeliveryCost() != null && returnRequest.getRefundDeliveryCost().booleanValue())
		{
			doubleValue += returnRequest.getOrder().getDeliveryCost() == null ? 0.0d
					: returnRequest.getOrder().getDeliveryCost().doubleValue();
		}

		return doubleValue;
	}

	@Override
	public void refundTransaction(final LoyaltyProgramProviderModel provider, final BaseStoreModel baseStoreModel,
			final OrderModel orderModel, final double amount) throws AACSLoyaltyException
	{
		Preconditions.checkNotNull(orderModel, "Order Shall not be null");
		Preconditions.checkNotNull(baseStoreModel, BASE_STORE_NULL_MSG);
		Preconditions.checkNotNull(provider, LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		Preconditions.checkNotNull(orderModel.getUser(), CUSTOMER_SHALL_NOT_BE_NULL);

		final String giiftTransactionId = orderModel.getGiiftTransactionId();
		final GiiftLoyaltyProgramProviderModel giiftProvider = getGiiftProvider(provider);
		final RefundTransactionResponse refundTransaction = giiftCustomerService
				.refundTransaction(getGiiftCredential(giiftProvider), giiftTransactionId, amount).orElse(null);

		if (refundTransaction == null)
		{
			LOG.error("RefundTransaction is null, please check the logs");
			throw new AACSLoyaltyException("RefundTransaction is null, please check the logs", null, AACSLoyaltyExceptionType.GIIFT);
		}


	}
}
