/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramprovider.strategy.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.aacs.aacsloyaltyprogramprovider.model.GiiftLoyaltyProgramProviderModel;
import com.aacs.aacsloyaltyprogramprovider.model.LoyaltyProgramProviderModel;
import com.aacs.aacsloyaltyprogramprovider.service.LoyaltyProgramProviderService;
import com.aacs.aacsloyaltyprogramprovider.strategy.LoyaltyProgramProviderStrategy;


/**
 * The Class DefaultGiiftLoyaltyProgramProviderStrategy.
 */
public class DefaultGiiftLoyaltyProgramProviderStrategy implements LoyaltyProgramProviderStrategy
{
	
	/** The loyalty program provider service. */
	@Resource(name = "loyaltyProgramProviderService")
	private LoyaltyProgramProviderService loyaltyProgramProviderService;

	/**
	 * Gets the loyalty program provider service.
	 *
	 * @return the loyalty program provider service
	 */
	public LoyaltyProgramProviderService getLoyaltyProgramProviderService()
	{
		return loyaltyProgramProviderService;
	}


	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreUid the base store uid
	 * @return the active provider
	 */
	@Override
	public Optional<LoyaltyProgramProviderModel> getActiveProvider(final String baseStoreUid)
	{
		return getLoyaltyProgramProviderService().getActiveProvider(baseStoreUid, GiiftLoyaltyProgramProviderModel.class);
	}

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreModel the base store model
	 * @return the active provider
	 */
	@Override
	public Optional<LoyaltyProgramProviderModel> getActiveProvider(final BaseStoreModel baseStoreModel)
	{

		return getLoyaltyProgramProviderService().getActiveProvider(baseStoreModel, GiiftLoyaltyProgramProviderModel.class);

	}

	/**
	 * Gets the active provider by current base store.
	 *
	 * @return the active provider by current base store
	 */
	@Override
	public Optional<LoyaltyProgramProviderModel> getActiveProviderByCurrentBaseStore()
	{
		return getLoyaltyProgramProviderService().getActiveProviderByCurrentBaseStore(GiiftLoyaltyProgramProviderModel.class);
	}


}
