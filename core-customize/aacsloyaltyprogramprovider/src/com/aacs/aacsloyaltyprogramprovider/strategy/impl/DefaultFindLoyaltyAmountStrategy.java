package com.aacs.aacsloyaltyprogramprovider.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.util.PriceValue;

import java.util.Objects;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.aacs.aacsloyaltyprogramprovider.context.LoyaltyProgramContext;
import com.aacs.aacsloyaltyprogramprovider.enums.LoyaltyPaymentStatus;
import com.aacs.aacsloyaltyprogramprovider.exception.AACSLoyaltyException;
import com.aacs.aacsloyaltyprogramprovider.service.LoyaltyPaymentModeService;
import com.aacs.aacsloyaltyprogramprovider.strategy.FindLoyaltyAmountStrategy;


/**
 * @author mohammedbaker
 *
 *
 */
public class DefaultFindLoyaltyAmountStrategy extends AbstractBusinessService implements FindLoyaltyAmountStrategy
{

	private static final Logger LOG = Logger.getLogger(DefaultFindLoyaltyAmountStrategy.class);

	@Resource(name = "loyaltyPaymentModeService")
	private LoyaltyPaymentModeService loyaltyPaymentModeService;

	@Resource(name = "loyaltyProgramContext")
	private LoyaltyProgramContext loyaltyProgramContext;





	public PriceValue getLoyaltyAmount(final AbstractOrderModel order, final double finalTotalAmount)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("order", order);

		try
		{
			if (!loyaltyProgramContext.isLoyaltyEnabled(order) || Objects.isNull(order.getLoyaltyPaymentMode()))
			{
				order.setShouldCalculateLoyalty(false);
				return null;
			}
		}
		catch (final AACSLoyaltyException e1)
		{
			order.setShouldCalculateLoyalty(false);
			return null;
		}

		double loyaltyAmount = 0.0;


		try
		{
			loyaltyProgramContext.reserve(order, finalTotalAmount);
			getModelService().refresh(order);
			if (LoyaltyPaymentStatus.SUCCESS.equals(order.getLoyaltyPaymentStatus()))
			{
				loyaltyAmount = order.getLoyaltyAmount();
			}
		}
		catch (final Exception e)
		{
			LOG.warn("Could not find loyaltyAmount for order [" + order.getCode() + "] due to : " + e + "... skipping!");
		}

		order.setShouldCalculateLoyalty(false);
		getModelService().save(order);

		return new PriceValue(order.getCurrency().getIsocode(), loyaltyAmount, order.getNet().booleanValue());
	}



}
