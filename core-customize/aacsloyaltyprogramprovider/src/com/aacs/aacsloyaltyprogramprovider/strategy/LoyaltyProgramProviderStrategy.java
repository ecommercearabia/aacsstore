/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramprovider.strategy;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.aacs.aacsloyaltyprogramprovider.model.LoyaltyProgramProviderModel;


/**
 * The Interface LoyaltyProgramProviderStrategy.
 */
public interface LoyaltyProgramProviderStrategy
{
	
	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreUid the base store uid
	 * @return the active provider
	 */
	public Optional<LoyaltyProgramProviderModel> getActiveProvider(String baseStoreUid);

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreModel the base store model
	 * @return the active provider
	 */
	public Optional<LoyaltyProgramProviderModel> getActiveProvider(BaseStoreModel baseStoreModel);

	/**
	 * Gets the active provider by current base store.
	 *
	 * @return the active provider by current base store
	 */
	public Optional<LoyaltyProgramProviderModel> getActiveProviderByCurrentBaseStore();
}
