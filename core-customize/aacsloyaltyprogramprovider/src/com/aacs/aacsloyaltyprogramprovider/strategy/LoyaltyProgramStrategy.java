/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramprovider.strategy;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.aacs.aacsloyaltyprogramprovider.beans.LoyaltyBalance;
import com.aacs.aacsloyaltyprogramprovider.beans.LoyaltyCustomerCode;
import com.aacs.aacsloyaltyprogramprovider.beans.LoyaltyCustomerInfo;
import com.aacs.aacsloyaltyprogramprovider.beans.LoyaltyPagination;
import com.aacs.aacsloyaltyprogramprovider.beans.LoyaltyUsablePoints;
import com.aacs.aacsloyaltyprogramprovider.beans.ValidateTransactionResult;
import com.aacs.aacsloyaltyprogramprovider.exception.AACSLoyaltyException;
import com.aacs.aacsloyaltyprogramprovider.model.LoyaltyProgramProviderModel;


/**
 *
 */
public interface LoyaltyProgramStrategy
{

	public boolean isRegister(CustomerModel customer, LoyaltyProgramProviderModel provider) throws AACSLoyaltyException;

	public boolean registerCustomer(CustomerModel customer, LoyaltyProgramProviderModel provider) throws AACSLoyaltyException;

	public Optional<LoyaltyUsablePoints> getUsablePoints(AbstractOrderModel order, double orderTotalAmount,
			LoyaltyProgramProviderModel provider) throws AACSLoyaltyException;

	public Optional<ValidateTransactionResult> validateTransactionByOrder(AbstractOrderModel order,
			LoyaltyProgramProviderModel provider) throws AACSLoyaltyException;

	public boolean createTransaction(AbstractOrderModel order, LoyaltyProgramProviderModel provider) throws AACSLoyaltyException;

	public Optional<LoyaltyCustomerInfo> getLoyaltyCustomerInfo(CustomerModel customer, LoyaltyPagination pagination,
			LoyaltyProgramProviderModel provider) throws AACSLoyaltyException;

	public Optional<LoyaltyCustomerCode> getLoyaltyCustomerCode(CustomerModel customer, LoyaltyProgramProviderModel provider)
			throws AACSLoyaltyException;

	public boolean cancelTranscation(AbstractOrderModel order, LoyaltyProgramProviderModel provider) throws AACSLoyaltyException;

	public Optional<LoyaltyBalance> getLoyaltyBalance(CustomerModel customer, LoyaltyProgramProviderModel provider)
			throws AACSLoyaltyException;

	public void reserve(AbstractOrderModel order, double orderTotalAmount, LoyaltyProgramProviderModel loyaltyProgramProviderModel)
			throws AACSLoyaltyException;

	Optional<ValidateTransactionResult> validateTransaction(AbstractOrderModel order, double orderTotalAmount,
			LoyaltyProgramProviderModel provider) throws AACSLoyaltyException;

	public void resetLoyalty(AbstractOrderModel order, LoyaltyProgramProviderModel provider) throws AACSLoyaltyException;

	public boolean isLoyaltyEnabled(LoyaltyProgramProviderModel provider, BaseStoreModel baseStoreModel, CustomerModel customer)
			throws AACSLoyaltyException;

	public void refundTransaction(LoyaltyProgramProviderModel provider, BaseStoreModel baseStoreModel,
			ReturnRequestModel returnRequest) throws AACSLoyaltyException;

	public void refundTransaction(LoyaltyProgramProviderModel provider, BaseStoreModel baseStoreModel, OrderModel orderModel,
			double amount) throws AACSLoyaltyException;

}
