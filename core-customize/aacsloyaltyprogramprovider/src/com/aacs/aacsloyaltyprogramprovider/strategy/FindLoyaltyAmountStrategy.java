package com.aacs.aacsloyaltyprogramprovider.strategy;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.util.PriceValue;


public interface FindLoyaltyAmountStrategy
{


	PriceValue getLoyaltyAmount(AbstractOrderModel order, double finalTotalAmount);
}
