package com.aacs.aacsloyaltyprogramprovider.giift.constant;

public interface GiiftConstants {

	public static final String BASE_URL = "https://acs-stage.giift.com";
	public static final String SUCCESS_CODE = "1";
	public static final String ERROR_CODE = "0";

}
