package com.aacs.aacsloyaltyprogramprovider.giift.exception;

import com.aacs.aacsloyaltyprogramprovider.exception.AACSLoyaltyException;
import com.aacs.aacsloyaltyprogramprovider.exception.type.AACSLoyaltyExceptionType;
import com.aacs.aacsloyaltyprogramprovider.giift.exception.type.GiiftExceptionType;


public class GiiftException extends AACSLoyaltyException
{

	/**
	 *
	 */
	private static final long serialVersionUID = 73715348954434564L;

	private final GiiftExceptionType type;


	public GiiftException(final GiiftExceptionType type, final String msg, final Object data)
	{
		super(msg, data, AACSLoyaltyExceptionType.GIIFT);
		this.type = type;

	}

	public GiiftExceptionType getType()
	{
		return type;
	}


}
