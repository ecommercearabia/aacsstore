package com.aacs.aacsloyaltyprogramprovider.giift.exception.type;

import java.util.HashMap;
import java.util.Map;

public enum GiiftExceptionType {

	ENCRYPTION_EXCEPTION("0", "encryption exception"), EMPTY_ERROR_RESPONSE_EXCEPTION("1", "error result is null"),
	UNKOWN_ERROR_CODE("2", "unkown error code"), EMPTY_CAMPIGN_EXCEPTION("3", "campaign is null"),
	EMPTY_USEABLE_REWARDS_EXCEPTION("4", "rewards list is null"), EMPTY_USEABLE_POINTS_EXCEPTION("5", "points is null"),
	SYSTEM_ERROR_EXCEPTION("10000", "System error"), REMOTE_SERVICE_ERROR_EXCEPTION("10001", "Remote service error"),
	PERMISSION_DENIED_EXCEPTION("10002", "Permission denied"),
	SMS_PLATFORM_ERROR_EXCEPTION("10003", "SMS platform error"),
	UNSUPPORTED_REQUEST_TYPE_EXCEPTION("10101", "Unsupported request type"),
	INVALIDATE_REQUEST_MODEL_EXCEPTION("10102", "Invalidate request model"),
	REQUIRED_REQUEST_PARAMETER_IS_NULL_OR_EMPTY_EXCEPTION("10103", "Required request parameter is null or empty"),
	REQUEST_PARAMETER_VALUE_INVALID_EXCEPTION("10104", "Request parameter value invalid"),
	REQUEST_PARAMETER_TIMESTAMP_EXPIRED_EXCEPTION("10105", "Request parameter timestamp expired"),
	REQUEST_PARAMETER_INVALID_EXCEPTION("10106", "Request parameter invalid"),
	INVALID_OPERATION_EXCEPTION("10107", "Invalid operation"), SERVICE_ERROR_EXCEPTION("20000", "Service error"),
	DATA_ENCRYPT_FAILED_EXCEPTION("20001", "Data encrypt failed"),
	DATA_DECRYPT_FAILED_EXCEPTION("20002", "Data decrypt failed"),
	CREATE_MERCHANT_ERROR_EXCEPTION("20100", "Create merchant error"),
	MERCHANT_TIERS_ARE_NOT_SET_EXCEPTION("20101", "Merchant tiers are not set"),
	POINT_EXCHANGE_RATE_IS_NOT_SET_EXCEPTION("20102", "Point exchange rate is not set"),
	MERCHANT_IS_NOT_FOUND_EXCEPTION("20103", "Merchant is not found"),
	MERCHANT_ALREADY_EXIST_EXCEPTION("20104", "Merchant already exist"),
	MERCHANT_ACCOUNT_IS_NOT_FOUND_EXCEPTION("20105", "Merchant account is not found"),
	CUSTOMER_ACCOUNT_IS_NOT_FOUND_EXCEPTION("20106", "Customer account is not found"),
	PLATFORM_USERNAME_ALREADY_IN_USE_EXCEPTION("20107", "Platform username already in use"),
	MERCHANT_TYPE_INVALID_EXCEPTION("20108", "Merchant type invalid"),
	MERCHANT_TIER_IS_NOT_FOUND_EXCEPTION("20109", "Merchant tier is not found"),
	MERCHANT_TIER_IN_USE_EXCEPTION("20110", "Merchant tier in use"),
	POINT_EXCHANGE_RATE_IS_NOT_FOUND_EXCEPTION("20111", "Point exchange rate is not found"),
	POINT_REDEEM_LIMITATION_IS_NOT_FOUND_EXCEPTION("20112", "Point redeem limitation is not found"),
	PROVINCE_IS_NOT_FOUND_EXCEPTION("20113", "Province is not found"),
	MERCHANT_ACCOUNT_ALREADY_EXIST_EXCEPTION("20114", "Merchant account already exist"),
	CUSTOMER_ACCOUNT_ALREADY_EXIST_EXCEPTION("20115", "Customer account already exist"),
	USERNAME_OR_PASSWORD_IS_INVALID_EXCEPTION("20120", "Username or password is invalid"),
	USERNAME_OR_VERIFICATION_CODE_IS_INVALID_EXCEPTION("20121", "Username or verification code is invalid"),
	LOGIN_FAILED_EXCEPTION("20122", "Login failed"), LOGOUT_FAILED_EXCEPTION("20123", "Logout failed"),
	USER_INVALID_EXCEPTION("20124", "User invalid"),
	USER_RETRIEVE_VERIFICATION_CODE_FAILED_EXCEPTION("20125", "User retrieve verification code failed"),
	CHANGE_PASSWORD_FAILED_EXCEPTION("20126", "Change password failed"),
	RED_PACKET_CAMPAIGN_IS_NOT_FOUND_EXCEPTION("20201", "Red packet campaign is not found"),
	LUCKY_DRAW_CAMPAIGN_IS_NOT_FOUND_EXCEPTION("20202", "Lucky draw campaign is not found"),
	COUPON_CAMPAIGN_IS_NOT_FOUND_EXCEPTION("20203", "Coupon campaign is not found"),
	LOYALTY_POINTS_CAMPAIGN_IS_NOT_FOUND_EXCEPTION("20204", "Loyalty points campaign is not found"),
	MERCHANT_ALREADY_SET_A_REWARDS_CAMPAIGN_EXCEPTION("20300", "Merchant already set a rewards campaign"),
	MERCHANT_ALREADY_SET_A_RED_PACKET_CAMPAIGN_EXCEPTION("20301", "Merchant already set a red packet campaign"),
	MERCHANT_ALREADY_SET_A_LUCKY_DRAW_CAMPAIGN_EXCEPTION("20302", "Merchant already set a lucky draw campaign"),
	MERCHANT_ALREADY_SET_A_COUPON_CAMPAIGN_EXCEPTION("20303", "Merchant already set a coupon campaign"),
	MERCHANT_ALREADY_SET_A_LOYALTY_POINTS_CAMPAIGN_EXCEPTION("20304", "Merchant already set a loyalty points campaign"),
	REWARD_IS_NOT_FOUND_EXCEPTION("20400", "Reward is not found"),
	RED_PACKET_IS_NOT_FOUND_EXCEPTION("20401", "Red packet is not found"),
	LUCKY_DRAW_PRIZE_IS_NOT_FOUND_EXCEPTION("20402", "Lucky draw prize is not found"),
	COUPON_IS_NOT_FOUND_EXCEPTION("20403", "Coupon is not found"),
	LOYALTY_POINTS_IS_NOT_ENOUGH_EXCEPTION("20404", "Loyalty points is not enough"),
	CUSTOMER_CANNOT_PARTICIPATE_NOW_EXCEPTION("20405", "Customer cannot participate now"),
	REWARD_ERROR_EXCEPTION("20500", "Reward error"), RED_PACKET_ERROR_EXCEPTION("20501", "Red packet error"),
	LUCKY_DRAW_ERROR_EXCEPTION("20502", "Lucky draw error"), COUPON_ERROR_EXCEPTION("20503", "Coupon error"),
	LOYALTY_POINTS_ERROR_EXCEPTION("20504", "Loyalty points error"),
	REWARD_REDEEM_LIMIT_EXCEPTION("20505", "Reward redeem limit"),
	TRANSACTION_INVALID_EXCEPTION("20600", "Transaction invalid"),
	REWARD_CANNOT_REDEEM_IN_THIS_TRANSACTION_EXCEPTION("20601", "Reward cannot redeem in this transaction"),
	LOYALTY_POINTS_AMOUNT_INVALID_IN_THIS_TRANSACTION_EXCEPTION("20602",
			"Loyalty points amount invalid in this transaction"),
	POINT_STRATEGY_SETTING_ERROR_EXCEPTION("21000", "Point strategy setting error"),
	POINT_ISSUE_STRATEGY_ERROR_EXCEPTION("21001", "Point issue strategy error"),
	POINT_REDEEM_STRATEGY_ERROR_EXCEPTION("21002", "Point redeem strategy error"),
	POINT_CONVERSION_STRATEGY_ERROR_EXCEPTION("21003", "Point conversion strategy error"),
	POINT_EXPIRE_STRATEGY_ERROR_EXCEPTION("21004", "Point expire strategy error"),
	UPLOAD_FILE_READ_FAILED_EXCEPTION("22001", "Upload file read failed"),
	DOWNLOAD_FILE_FAILED_EXCEPTION("22002", "Download file failed"),
	DATABASE_ERROR_EXCEPTION("30000", "Database error"),
	PAGINATION_QUERY_PARAMETER_CANNOT_BE_NULL_EXCEPTION("30001", "Pagination query parameter cannot be null");

	private final String code;

	private final String msg;

	private static Map<String, GiiftExceptionType> map = new HashMap<>();

	static {
		for (final GiiftExceptionType giiftExceptionType : GiiftExceptionType.values()) {
			map.put(giiftExceptionType.code, giiftExceptionType);
		}
	}

	private GiiftExceptionType(final String code, final String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

	public static GiiftExceptionType getExceptionFromCode(final String code)
	{
		final GiiftExceptionType giiftExceptionType = map.get(code);
		return giiftExceptionType == null ? UNKOWN_ERROR_CODE: giiftExceptionType;
	}

}
