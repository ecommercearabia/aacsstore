package com.aacs.aacsloyaltyprogramprovider.giift.beans;

import com.aacs.aacsloyaltyprogramprovider.giift.beans.metadata.GiiftMetaInfo;


public class Transaction {

	private Customer customer;

	private double initialAmount;

	private double finalAmount;

	private String validateId;

	private String transactionId;

	private double pointAmount;

	private GiiftMetaInfo metaData;

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(final Customer customer) {
		this.customer = customer;
	}

	public double getInitialAmount() {
		return initialAmount;
	}

	public void setInitialAmount(final double initialAmount) {
		this.initialAmount = initialAmount;
	}

	public double getFinalAmount() {
		return finalAmount;
	}

	public void setFinalAmount(final double finalAmount) {
		this.finalAmount = finalAmount;
	}

	public String getValidateId() {
		return validateId;
	}

	public void setValidateId(final String validateId) {
		this.validateId = validateId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(final String transactionId) {
		this.transactionId = transactionId;
	}

	public double getPointAmount() {
		return pointAmount;
	}

	public void setPointAmount(final double pointAmount) {
		this.pointAmount = pointAmount;
	}

	public GiiftMetaInfo getMetaData()
	{
		return metaData;
	}

	public void setMetaData(final GiiftMetaInfo metaData)
	{
		this.metaData = metaData;
	}

}
