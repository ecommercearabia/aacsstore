package com.aacs.aacsloyaltyprogramprovider.giift.beans;

import java.util.Date;

import com.google.gson.annotations.SerializedName;

public class Log {

	@SerializedName("ID")
	private String id;

	@SerializedName("Created")
	private Date created;

	@SerializedName("ExpireAt")
	private Date expireDate;

	@SerializedName("Type")
	private String type;

	@SerializedName("MerchantName")
	private String merchantName;

	@SerializedName("MerchantID")
	private String merchantID;

	@SerializedName("TillNumber")
	private String tillNumber;

	@SerializedName("ItemName")
	private String itemName;

	@SerializedName("ItemImage")
	private String itemImage;

	@SerializedName("QRCode")
	private String qrCode;

	@SerializedName("QRCodeText")
	private String qrCodeText;

	@SerializedName("WithQrCode")
	private boolean withQrCode;

	@SerializedName("TransactionID")
	private String transactionID;

	@SerializedName("InitialAmount")
	private double initialAmount;

	@SerializedName("FinalAmount")
	private double finalAmount;

	@SerializedName("Fee")
	private double fee;

	@SerializedName("Points")
	private double points;

	@SerializedName("Metadata")
	private String metaData;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(final Date created) {
		this.created = created;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(final Date expireDate) {
		this.expireDate = expireDate;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(final String merchantName) {
		this.merchantName = merchantName;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(final String merchantID) {
		this.merchantID = merchantID;
	}

	public String getTillNumber() {
		return tillNumber;
	}

	public void setTillNumber(final String tillNumber) {
		this.tillNumber = tillNumber;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(final String itemName) {
		this.itemName = itemName;
	}

	public String getItemImage() {
		return itemImage;
	}

	public void setItemImage(final String itemImage) {
		this.itemImage = itemImage;
	}

	public String getQrCode() {
		return qrCode;
	}

	public void setQrCode(final String qrCode) {
		this.qrCode = qrCode;
	}

	public String getQrCodeText() {
		return qrCodeText;
	}

	public void setQrCodeText(final String qrCodeText) {
		this.qrCodeText = qrCodeText;
	}

	public boolean isWithQrCode() {
		return withQrCode;
	}

	public void setWithQrCode(final boolean withQrCode) {
		this.withQrCode = withQrCode;
	}

	public String getTransactionID() {
		return transactionID;
	}

	public void setTransactionID(final String transactionID) {
		this.transactionID = transactionID;
	}

	public double getInitialAmount() {
		return initialAmount;
	}

	public void setInitialAmount(final double initialAmount) {
		this.initialAmount = initialAmount;
	}

	public double getFinalAmount() {
		return finalAmount;
	}

	public void setFinalAmount(final double finalAmount) {
		this.finalAmount = finalAmount;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(final double fee) {
		this.fee = fee;
	}

	public double getPoints() {
		return points;
	}

	public void setPoints(final double points) {
		this.points = points;
	}

	/**
	 * @return the metaData
	 */
	public String getMetaData()
	{
		return metaData;
	}

	/**
	 * @param metaData
	 *           the metaData to set
	 */
	public void setMetaData(final String metaData)
	{
		this.metaData = metaData;
	}


}
