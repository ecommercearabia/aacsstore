package com.aacs.aacsloyaltyprogramprovider.giift.beans;

import com.google.gson.annotations.SerializedName;

public class ExchangeRate {

	@SerializedName("PointValue")
	private double pointValue;

	@SerializedName("Denomination")
	private double denomination;

	@SerializedName("DenominationValue")
	private double denominationValue;

	public double getPointValue()
	{
		return pointValue;
	}

	public void setPointValue(final double pointValue)
	{
		this.pointValue = pointValue;
	}

	public double getDenomination() {
		return denomination;
	}

	public void setDenomination(final double denomination) {
		this.denomination = denomination;
	}

	public double getDenominationValue()
	{
		return denominationValue;
	}

	public void setDenominationValue(final double denominationValue)
	{
		this.denominationValue = denominationValue;
	}

}
