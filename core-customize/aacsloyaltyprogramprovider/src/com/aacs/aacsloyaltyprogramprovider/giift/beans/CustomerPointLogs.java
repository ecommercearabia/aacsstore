package com.aacs.aacsloyaltyprogramprovider.giift.beans;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class CustomerPointLogs {

	@SerializedName("Logs")
	private List<Log> logs;

	@SerializedName("TotalPage")
	private int totalPage;

	@SerializedName("TotalCount")
	private int totalCount;

	@SerializedName("PageIndex")
	private int pageIndex;

	@SerializedName("PageSize")
	private int pageSize;

	public List<Log> getLogs() {
		return logs;
	}

	public void setLogs(final List<Log> logs) {
		this.logs = logs;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(final int totalPage) {
		this.totalPage = totalPage;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(final int totalCount) {
		this.totalCount = totalCount;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(final int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(final int pageSize) {
		this.pageSize = pageSize;
	}
}
