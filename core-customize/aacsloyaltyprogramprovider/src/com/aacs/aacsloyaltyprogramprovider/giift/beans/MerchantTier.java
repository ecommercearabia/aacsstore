package com.aacs.aacsloyaltyprogramprovider.giift.beans;

import java.math.BigDecimal;

import com.google.gson.annotations.SerializedName;

public class MerchantTier {
	@SerializedName("TierID")
	private String tierID;
	@SerializedName("TierName")
	private String tierName;
	@SerializedName("MinimumDiscount")
	private BigDecimal minimumDiscount;
	@SerializedName("MaximumDiscount")
	private BigDecimal maximumDiscount;

	public String getTierID() {
		return tierID;
	}

	public void setTierID(final String tierID) {
		this.tierID = tierID;
	}

	public String getTierName() {
		return tierName;
	}

	public void setTierName(final String tierName) {
		this.tierName = tierName;
	}

	public BigDecimal getMinimumDiscount() {
		return minimumDiscount;
	}

	public void setMinimumDiscount(final BigDecimal minimumDiscount) {
		this.minimumDiscount = minimumDiscount;
	}

	public BigDecimal getMaximumDiscount() {
		return maximumDiscount;
	}

	public void setMaximumDiscount(final BigDecimal maximumDiscount) {
		this.maximumDiscount = maximumDiscount;
	}

}
