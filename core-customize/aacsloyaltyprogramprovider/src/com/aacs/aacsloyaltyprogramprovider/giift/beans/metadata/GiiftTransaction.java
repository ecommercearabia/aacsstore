/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramprovider.giift.beans.metadata;

import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class GiiftTransaction
{

	@SerializedName("TransactionID")
	private String id;

	@SerializedName("POSNo")
	private String pos;

	@SerializedName("SeqNo")
	private String seq;

	@SerializedName("TransactionDate")
	private String date;

	@SerializedName("TransactionTotal")
	private double total;

	/**
	 * @return the id
	 */
	public String getId()
	{
		return id;
	}

	/**
	 * @param id
	 *           the id to set
	 */
	public void setId(final String id)
	{
		this.id = id;
	}

	/**
	 * @return the pos
	 */
	public String getPos()
	{
		return pos;
	}

	/**
	 * @param pos
	 *           the pos to set
	 */
	public void setPos(final String pos)
	{
		this.pos = pos;
	}

	/**
	 * @return the seq
	 */
	public String getSeq()
	{
		return seq;
	}

	/**
	 * @param seq
	 *           the seq to set
	 */
	public void setSeq(final String seq)
	{
		this.seq = seq;
	}

	/**
	 * @return the date
	 */
	public String getDate()
	{
		return date;
	}

	/**
	 * @param date
	 *           the date to set
	 */
	public void setDate(final String date)
	{
		this.date = date;
	}

	/**
	 * @return the total
	 */
	public double getTotal()
	{
		return total;
	}

	/**
	 * @param total
	 *           the total to set
	 */
	public void setTotal(final double total)
	{
		this.total = total;
	}
}
