package com.aacs.aacsloyaltyprogramprovider.giift.beans;

import com.google.gson.annotations.SerializedName;

public class CustomerQRCode {

	@SerializedName("CustomerID")
	private String customerID;

	@SerializedName("QrCode")
	private String qrCode;

	@SerializedName("CustomerCode")
	private String customerCode;

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(final String customerID) {
		this.customerID = customerID;
	}

	public String getQrCode() {
		return qrCode;
	}

	public void setQrCode(final String qrCode) {
		this.qrCode = qrCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(final String customerCode) {
		this.customerCode = customerCode;
	}



}
