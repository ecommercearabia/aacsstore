package com.aacs.aacsloyaltyprogramprovider.giift.beans;

import com.google.gson.annotations.SerializedName;

public class MerchantDataResponse {

	@SerializedName("ResultCode")
	private String code;

	@SerializedName("Result")
	private MerchantData result;

	@SerializedName("ErrorResult")
	private ErrorResult errorResult;

	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

	public MerchantData getResult() {
		return result;
	}

	public void setResult(final MerchantData result) {
		this.result = result;
	}

	public ErrorResult getErrorResult() {
		return errorResult;
	}

	public void setErrorResult(final ErrorResult errorResult) {
		this.errorResult = errorResult;
	}

}
