package com.aacs.aacsloyaltyprogramprovider.giift.beans;

import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class CustomerResponse {

	@SerializedName("ResultCode")
	private String code;

	@SerializedName("Result")
	private Map<String, Object> result;

	@SerializedName("ErrorResult")
	private ErrorResult errorResult;

	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

	public Map<String, Object> getResult() {
		return result;
	}

	public void setResult(final Map<String, Object> result) {
		this.result = result;
	}

	public ErrorResult getErrorResult() {
		return errorResult;
	}

	public void setErrorResult(final ErrorResult errorResult) {
		this.errorResult = errorResult;
	}

}
