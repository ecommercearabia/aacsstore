package com.aacs.aacsloyaltyprogramprovider.giift.beans;

import java.math.BigDecimal;

public class MerchantData {
	private String merchantID;
	private String tillNumber;
	private String merchantName;
	private String merchantType;
	private String category;
	private String province;
	private String address;
	private String phoneNumber;
	private String email;
	private String contactPerson;
	private String contactPersonPhoneNumber;
	private String parentID;
	private String tier;
	private BigDecimal discount;

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(final String merchantID) {
		this.merchantID = merchantID;
	}

	public String getTillNumber() {
		return tillNumber;
	}

	public void setTillNumber(final String tillNumber) {
		this.tillNumber = tillNumber;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(final String merchantName) {
		this.merchantName = merchantName;
	}

	public String getMerchantType() {
		return merchantType;
	}

	public void setMerchantType(final String merchantType) {
		this.merchantType = merchantType;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(final String category) {
		this.category = category;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(final String province) {
		this.province = province;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(final String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(final String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(final String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getContactPersonPhoneNumber() {
		return contactPersonPhoneNumber;
	}

	public void setContactPersonPhoneNumber(final String contactPersonPhoneNumber) {
		this.contactPersonPhoneNumber = contactPersonPhoneNumber;
	}

	public String getParentID() {
		return parentID;
	}

	public void setParentID(final String parentID) {
		this.parentID = parentID;
	}

	public String getTier() {
		return tier;
	}

	public void setTier(final String tier) {
		this.tier = tier;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(final BigDecimal discount) {
		this.discount = discount;
	}

}
