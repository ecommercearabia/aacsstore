/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramprovider.giift.beans;

import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class QueryRefundPoints
{
	@SerializedName("MerchantID")
	private String merchantID;
	@SerializedName("MerchantName")
	private String merchantName;
	@SerializedName("CustomerID")
	private String customerID;
	@SerializedName("RefundAmount")
	private double refundAmount;
	@SerializedName("FinalRefundAmount")
	private double finalRefundAmount;
	@SerializedName("ValidateID")
	private String validateID;
	@SerializedName("TransactionID")
	private String transactionID;
	@SerializedName("Points")
	private Points points;

	/**
	 * @return the merchantID
	 */
	public String getMerchantID()
	{
		return merchantID;
	}

	/**
	 * @param merchantID
	 *           the merchantID to set
	 */
	public void setMerchantID(final String merchantID)
	{
		this.merchantID = merchantID;
	}

	/**
	 * @return the merchantName
	 */
	public String getMerchantName()
	{
		return merchantName;
	}

	/**
	 * @param merchantName
	 *           the merchantName to set
	 */
	public void setMerchantName(final String merchantName)
	{
		this.merchantName = merchantName;
	}

	/**
	 * @return the customerID
	 */
	public String getCustomerID()
	{
		return customerID;
	}

	/**
	 * @param customerID
	 *           the customerID to set
	 */
	public void setCustomerID(final String customerID)
	{
		this.customerID = customerID;
	}

	/**
	 * @return the refundAmount
	 */
	public double getRefundAmount()
	{
		return refundAmount;
	}

	/**
	 * @param refundAmount
	 *           the refundAmount to set
	 */
	public void setRefundAmount(final double refundAmount)
	{
		this.refundAmount = refundAmount;
	}

	/**
	 * @return the finalRefundAmount
	 */
	public double getFinalRefundAmount()
	{
		return finalRefundAmount;
	}

	/**
	 * @param finalRefundAmount
	 *           the finalRefundAmount to set
	 */
	public void setFinalRefundAmount(final double finalRefundAmount)
	{
		this.finalRefundAmount = finalRefundAmount;
	}

	/**
	 * @return the validateID
	 */
	public String getValidateID()
	{
		return validateID;
	}

	/**
	 * @param validateID
	 *           the validateID to set
	 */
	public void setValidateID(final String validateID)
	{
		this.validateID = validateID;
	}

	/**
	 * @return the transactionID
	 */
	public String getTransactionID()
	{
		return transactionID;
	}

	/**
	 * @param transactionID
	 *           the transactionID to set
	 */
	public void setTransactionID(final String transactionID)
	{
		this.transactionID = transactionID;
	}

	/**
	 * @return the points
	 */
	public Points getPoints()
	{
		return points;
	}

	/**
	 * @param points
	 *           the points to set
	 */
	public void setPoints(final Points points)
	{
		this.points = points;
	}



}
