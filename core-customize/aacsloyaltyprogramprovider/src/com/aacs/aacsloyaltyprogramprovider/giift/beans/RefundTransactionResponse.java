package com.aacs.aacsloyaltyprogramprovider.giift.beans;

import java.util.Date;

import com.google.gson.annotations.SerializedName;


public class RefundTransactionResponse
{

	@SerializedName("Created")
	private Date created;

	@SerializedName("MerchantID")
	private String merchantId;

	@SerializedName("MerchantName")
	private String merchantName;

	@SerializedName("CustomerID")
	private String customerId;

	@SerializedName("RefundAmount")
	private double refundAmount;

	@SerializedName("FinalRefundAmount")
	private double finalRefundAmount;

	@SerializedName("ValidateID")
	private String validateId;

	@SerializedName("TransactionID")
	private String transactionId;

	@SerializedName("Points")
	private Points points;

	/**
	 * @return the created
	 */
	public Date getCreated()
	{
		return created;
	}

	/**
	 * @param created
	 *           the created to set
	 */
	public void setCreated(final Date created)
	{
		this.created = created;
	}

	/**
	 * @return the merchantId
	 */
	public String getMerchantId()
	{
		return merchantId;
	}

	/**
	 * @param merchantId
	 *           the merchantId to set
	 */
	public void setMerchantId(final String merchantId)
	{
		this.merchantId = merchantId;
	}

	/**
	 * @return the merchantName
	 */
	public String getMerchantName()
	{
		return merchantName;
	}

	/**
	 * @param merchantName
	 *           the merchantName to set
	 */
	public void setMerchantName(final String merchantName)
	{
		this.merchantName = merchantName;
	}

	/**
	 * @return the customerId
	 */
	public String getCustomerId()
	{
		return customerId;
	}

	/**
	 * @param customerId
	 *           the customerId to set
	 */
	public void setCustomerId(final String customerId)
	{
		this.customerId = customerId;
	}

	/**
	 * @return the refundAmount
	 */
	public double getRefundAmount()
	{
		return refundAmount;
	}

	/**
	 * @param refundAmount
	 *           the refundAmount to set
	 */
	public void setRefundAmount(final double refundAmount)
	{
		this.refundAmount = refundAmount;
	}

	/**
	 * @return the finalRefundAmount
	 */
	public double getFinalRefundAmount()
	{
		return finalRefundAmount;
	}

	/**
	 * @param finalRefundAmount
	 *           the finalRefundAmount to set
	 */
	public void setFinalRefundAmount(final double finalRefundAmount)
	{
		this.finalRefundAmount = finalRefundAmount;
	}

	/**
	 * @return the validateId
	 */
	public String getValidateId()
	{
		return validateId;
	}

	/**
	 * @param validateId
	 *           the validateId to set
	 */
	public void setValidateId(final String validateId)
	{
		this.validateId = validateId;
	}

	/**
	 * @return the transactionId
	 */
	public String getTransactionId()
	{
		return transactionId;
	}

	/**
	 * @param transactionId
	 *           the transactionId to set
	 */
	public void setTransactionId(final String transactionId)
	{
		this.transactionId = transactionId;
	}

	/**
	 * @return the points
	 */
	public Points getPoints()
	{
		return points;
	}

	/**
	 * @param points
	 *           the points to set
	 */
	public void setPoints(final Points points)
	{
		this.points = points;
	}



}
