/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsloyaltyprogramprovider.giift.beans.metadata;

import java.util.List;

import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class GiiftMetaInfo
{
	@SerializedName("Transaction")
	private GiiftTransaction transaction;

	@SerializedName("Items")
	private List<GiiftItem> items;

	/**
	 * @return the transaction
	 */
	public GiiftTransaction getTransaction()
	{
		return transaction;
	}

	/**
	 * @param transaction
	 *           the transaction to set
	 */
	public void setTransaction(final GiiftTransaction transaction)
	{
		this.transaction = transaction;
	}

	/**
	 * @return the items
	 */
	public List<GiiftItem> getItems()
	{
		return items;
	}

	/**
	 * @param items
	 *           the items to set
	 */
	public void setItems(final List<GiiftItem> items)
	{
		this.items = items;
	}

}
