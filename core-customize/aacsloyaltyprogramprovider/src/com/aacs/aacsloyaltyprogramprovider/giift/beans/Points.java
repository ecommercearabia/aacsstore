package com.aacs.aacsloyaltyprogramprovider.giift.beans;

import com.google.gson.annotations.SerializedName;

public class Points {

	@SerializedName("PointRedeemAmount")
	private double redeemAmount;

	@SerializedName("PointRedeemValue")
	private double redeemValue;

	@SerializedName("PointIssueAmount")
	private double issueAmount;

	@SerializedName("PointIssueValue")
	private double issueValue;

	@SerializedName("BalancePoints")
	private double balancePoints;

	public double getRedeemAmount()
	{
		return redeemAmount;
	}

	public void setRedeemAmount(final double redeemAmount)
	{
		this.redeemAmount = redeemAmount;
	}

	public double getRedeemValue()
	{
		return redeemValue;
	}

	public void setRedeemValue(final double redeemValue)
	{
		this.redeemValue = redeemValue;
	}

	public double getIssueAmount()
	{
		return issueAmount;
	}

	public void setIssueAmount(final double issueAmount)
	{
		this.issueAmount = issueAmount;
	}

	public double getIssueValue()
	{
		return issueValue;
	}

	public void setIssueValue(final double issueValue)
	{
		this.issueValue = issueValue;
	}

	public double getBalancePoints()
	{
		return balancePoints;
	}

	public void setBalancePoints(final double balancePoints)
	{
		this.balancePoints = balancePoints;
	}

}
