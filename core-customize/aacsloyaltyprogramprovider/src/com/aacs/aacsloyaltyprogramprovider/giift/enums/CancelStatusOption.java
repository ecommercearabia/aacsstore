package com.aacs.aacsloyaltyprogramprovider.giift.enums;

public enum CancelStatusOption {

	CANCEL("Cancel"),FAIL("Fail");

	private final String value;

	private CancelStatusOption(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
