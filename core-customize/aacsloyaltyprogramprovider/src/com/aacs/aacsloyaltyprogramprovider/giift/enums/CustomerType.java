package com.aacs.aacsloyaltyprogramprovider.giift.enums;

import com.google.gson.annotations.SerializedName;

public enum CustomerType {

	@SerializedName("Non_Shareholder")
	NON_SHAREHOLDER("Non_Shareholder"),
	@SerializedName("Shareholder")
	SHAREHOLDER("Shareholder"),
	@SerializedName("Staff")
	STAFF("Staff");

	private CustomerType(String value) {
		this.value = value;
	}

	private final String value;

	public String getValue() {
		return value;
	}

}
