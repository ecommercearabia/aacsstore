package com.aacs.aacsloyaltyprogramprovider.giift.enums;

public enum CampaignType {

	RED_PACKET("RedPacket"),LUCKY_DRAW("LuckyDraw"),COUPON("Coupon");

	private String name;

	private CampaignType(final String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
