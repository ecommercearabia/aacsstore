package com.aacs.aacsloyaltyprogramprovider.giift.util;

import java.util.Base64;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AESHelper {
	public static boolean isValid(final String key, final String initVector)
	{
		if (initVector == null || key == null)
		{
			return false;
		}
		if (initVector.length() != 16 || (key.length() != 16 && key.length() != 24 && key.length() != 32))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	public static String aesEncrypt(final String plainStr, final String key, final String initVector) throws Exception {
		if (!isValid(key, initVector))
		{
			return null;
		}
		if (plainStr == null)
		{
			return null;
		}
		final SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes("utf-8"), "AES");
		final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		final IvParameterSpec ivParameterSpec = new IvParameterSpec(initVector.getBytes("utf-8"));
		cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
		final byte[] bytes = cipher.doFinal(plainStr.getBytes("utf-8"));
		return Base64.getEncoder().encodeToString(bytes);
	}

	public static String aesDecrypt(final String plainStr, final String key, final String initVector) throws Exception {
		if (!isValid(key, initVector))
		{
			return null;
		}
		if (plainStr == null)
		{
			return null;
		}
		final SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes("utf-8"), "AES");
		final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		final IvParameterSpec ivParameterSpec = new IvParameterSpec(initVector.getBytes("utf-8"));
		cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
		byte[] bytes = Base64.getDecoder().decode(plainStr);
		bytes = cipher.doFinal(bytes);
		return new String(bytes,"utf-8");
	}
	public static void testSelf() throws Exception
	{
		final String input = "This is input";
		final String key = "edaf3c284a4feaad";//AESHelper.getRandomString(16);
		final String iv = "e9803653e8244e04";//AESHelper.getRandomString(16);
		System.out.println(key);
		System.out.println(iv);

		final String afterEncrypt = AESHelper.aesEncrypt(input, key, iv);
		final String afterDecrypt = AESHelper.aesDecrypt("gbdh7l+CWFCW31em9TdEpP/IRe7+x52dI/0mtqrBnH3HDLMsrtp3Fa9qVbVHt3p40zQffzvHMYUz54y30sF9T2Izi58d1ZuwvjIWJ6vEEcVxMnktPaZ4+4Dkn1Udc4BUM3QPxf1GFKfph05bMuhYjGhsUUT/k+QPTORo5Qxa2i28FlD6PgOz+X5/MLh/ajNmy02Q1Hpw8qtYq0LXHM8Qk0TTfsdGhXOcTA3qtdAaoLe21Ks1Qnzs31ZsvMh9LikTttmMt9ZXpDM+afZ8TC3jYQ==", key, iv);

		System.out.println(input);
		System.out.println(afterEncrypt);
		System.out.println(afterDecrypt);
	}
	public static void main(final String[] args) {
		try {
			AESHelper.testSelf();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
	public static String getRandomString(final int length){
		final StringBuilder strs = new StringBuilder("1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ,./<>?;':[]{}|~!@#$%^&*()_+-=");
        final int range = strs.length();
		final char[] key = new char[length];
        final Random ran = new Random(System.currentTimeMillis());
        for (int i = 0; i < length; i++)
        {
            final int RandKey = ran.nextInt(range);
            key[i] = strs.charAt(RandKey);
        }
        return new String(key);
	 }
}
