package com.aacs.aacsloyaltyprogramprovider.giift.util;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.aacs.aacsloyaltyprogramprovider.giift.beans.EncryptedResponse;
import com.aacs.aacsloyaltyprogramprovider.giift.exception.GiiftException;
import com.aacs.aacsloyaltyprogramprovider.giift.exception.type.GiiftExceptionType;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class GiiftWebServiceApiUtil
{
	private static final Logger LOG = LoggerFactory.getLogger(GiiftWebServiceApiUtil.class);
	private static final String TIME_STAMP_KEY = "TimeStamp";

	private GiiftWebServiceApiUtil()
	{
	}

	public static <T> T httpPOST(final String url, Map<String, Object> params, final String key, final String vector,
			final Class<T> clazz, final int timeout, final boolean timeoutEnabled) throws GiiftException
	{
		final HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		httpRequestFactory.setConnectionRequestTimeout(timeout * 1000);
		httpRequestFactory.setConnectTimeout(timeout * 1000);
		httpRequestFactory.setReadTimeout(timeout * 1000);
		RestTemplate restTemplate = null;
		if (timeoutEnabled)
		{
			restTemplate = new RestTemplate(httpRequestFactory);
		}
		else
		{
			restTemplate = new RestTemplate();
		}

		if (params == null)
		{
			params = new LinkedHashMap<>();
		}

		final long unixTime = System.currentTimeMillis() / 1000L;
		params.put(TIME_STAMP_KEY, unixTime);

		String requestString = mapToJSONString(params);
		requestString = requestString.replaceAll("\\s+", "");

		params.clear();
		LOG.info("Giift Request:{},\n Giift url:{}", requestString, url);
		final String encryptReqString = encryptReqString(requestString, key, vector);
		params.put("Data", encryptReqString);

		requestString = mapToJSONString(params);
		ResponseEntity<?> stage2Response = null;
		try
		{
			stage2Response = restTemplate.postForEntity(url, requestString, EncryptedResponse.class);
		}
		catch (final ResourceAccessException e)
		{
			LOG.error(e.getMessage());
			throw new GiiftException(GiiftExceptionType.SERVICE_ERROR_EXCEPTION, e.getMessage(), e);
		}
		if (!stage2Response.getStatusCode().is2xxSuccessful())
		{
			LOG.warn("Body: {}", stage2Response.getBody());
			return null;
		}

		final EncryptedResponse responseResult2 = (EncryptedResponse) stage2Response.getBody();
		if (Objects.isNull(responseResult2))
		{
			return null;
		}
		return decryptResponse(responseResult2.getData(), key, vector, clazz);

	}

	private static <T> T decryptResponse(final String resString, final String key, final String vector, final Class<T> clazz)
			throws GiiftException
	{
		try
		{
			final String aesDecrypt = AESHelper.aesDecrypt(resString, key, vector);
			LOG.info("Giift DEC Response: {}", aesDecrypt);
			return new Gson().fromJson(aesDecrypt, clazz);
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage());
			throw new GiiftException(GiiftExceptionType.ENCRYPTION_EXCEPTION, GiiftExceptionType.ENCRYPTION_EXCEPTION.getMsg(), e);
		}
	}

	private static String mapToJSONString(final Map<String, Object> map)
	{
		final Gson gson = new GsonBuilder().create();
		return gson.toJson(map);
	}

	private static String encryptReqString(final String reqString, final String key, final String vector) throws GiiftException
	{
		try
		{
			return AESHelper.aesEncrypt(reqString, key, vector);
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage());
			throw new GiiftException(GiiftExceptionType.ENCRYPTION_EXCEPTION, GiiftExceptionType.ENCRYPTION_EXCEPTION.getMsg(), e);
		}
	}

}
