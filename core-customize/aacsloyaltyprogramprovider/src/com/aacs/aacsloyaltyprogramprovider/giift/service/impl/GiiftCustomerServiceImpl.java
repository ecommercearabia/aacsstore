package com.aacs.aacsloyaltyprogramprovider.giift.service.impl;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.collections.MapUtils;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aacs.aacsloyaltyprogramprovider.giift.beans.Customer;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.CustomerPointLogs;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.CustomerPointSummary;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.CustomerQRCode;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.CustomerResponse;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.CustomerStatus;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.ExchangeRate;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.GiiftCredential;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.Pagination;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.Points;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.QueryRefundPoints;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.RefundTransactionResponse;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.Transaction;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.TransactionResponse;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.VaildateTransactionResponse;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.metadata.GiiftMetaInfo;
import com.aacs.aacsloyaltyprogramprovider.giift.constant.GiiftConstants;
import com.aacs.aacsloyaltyprogramprovider.giift.enums.CancelStatusOption;
import com.aacs.aacsloyaltyprogramprovider.giift.exception.GiiftException;
import com.aacs.aacsloyaltyprogramprovider.giift.exception.type.GiiftExceptionType;
import com.aacs.aacsloyaltyprogramprovider.giift.service.GiiftCustomerService;
import com.aacs.aacsloyaltyprogramprovider.giift.util.ErrorHandlingUtil;
import com.aacs.aacsloyaltyprogramprovider.giift.util.GiiftWebServiceApiUtil;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * @author mohammed.baker@erabia.com
 * @editor husam.dababneh@erabia.com
 */
public class GiiftCustomerServiceImpl implements GiiftCustomerService
{



	private static final Logger LOG = LoggerFactory.getLogger(GiiftCustomerServiceImpl.class);

	private static final String MERCHANT_ID_KEY = "MerchantID";
	private static final String CUSTOMER_KEY = "Customer";
	private static final String INITIAL_AMOUNT_KEY = "InitialAmount";
	private static final String POINT_AMOUNT_KEY = "PointAmount";
	private static final String VALIDATE_ID_KEY = "ValidateID";
	private static final String TRANSACTION_ID_KEY = "TransactionID";
	private static final String FINAL_AMOUNT_KEY = "FinalAmount";
	private static final String CUSTOMRE_ID_KEY = "CustomerID";
	private static final String PAGINATION_KEY = "Pagination";
	private static final String CANCEL_STATUS_OPTION_KEY = "Status";
	private static final String POINTS_MAPPING_KEY = "Points";
	private static final String METADATA_KEY = "Metadata";
	private static final String REFUND_AMOUNT_KEY = "RefundAmount";


	private static final String CUSTOMER_ID_NULL_MESSAGE = "customerId is null";
	private static final String GIIFT_CREDENTIAL_NULL_MESSAGE = "giiftCredential is null";
	private static final String BASE_URL_NULL_MESSAGE = "baseUrl is null";
	private static final String VALIDATION_ID_NULL_MESSAGE = "validationId is null";
	private static final String TRANSACTION_NULL_MESSAGE = "transaction is null";

	private static final String USEABLE_POINTS_URL = "/api/Transaction/QueryUseablePoints";
	private static final String EXCHANGE_RATE_URL = "/api/Transaction/QueryPointExchangeRate";
	private static final String VALIDATE_TRANSACTION_URL = "/api/Transaction/ValidateTransaction";
	private static final String CREATE_TRANSACTION_URL = "/api/Transaction/CreateTransaction";
	private static final String CUSTOMER_SUMMARY_URL = "/api/Member/CustomerPointsSummary";
	private static final String CUSTOMER_POINT_LOGS_URL = "/api/Member/CustomerPointsLog";
	private static final String CUSTOMER_QR_CODE_URL = "/api/Member/CustomerQrcode";
	private static final String IS_REGISTER_URL = "/api/Member/CheckRegister";
	private static final String CUSTOMER_REGISTER_URL = "/api/Member/CustomerRegister";
	private static final String CANCEL_TRANSACTION_URL = "/api/Transaction/TransactionNotification";

	private static final String QUERY_REFUND_TRANSACTION_URL = "/api/Transaction/QueryRefundPoints";
	private static final String REFUND_TRANSACTION_URL = "/api/Transaction/RefundTransaction";


	private static final Gson GSON;

	static
	{
		GSON = new GsonBuilder().serializeSpecialFloatingPointValues().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();
	}

	@Override
	public Optional<Points> getUsablePoints(final GiiftCredential giiftCredential, final Transaction transaction)
			throws GiiftException
	{
		validate(giiftCredential);
		validate(giiftCredential.getKey(), giiftCredential.getVector(), giiftCredential.getMerchantId());
		Preconditions.checkArgument(!Objects.isNull(transaction), TRANSACTION_NULL_MESSAGE);
		validate(transaction.getCustomer());
		final Map<String, Object> params = new LinkedHashMap<>();
		params.put(MERCHANT_ID_KEY, giiftCredential.getMerchantId());
		params.put(CUSTOMER_KEY, transaction.getCustomer());
		params.put(INITIAL_AMOUNT_KEY, transaction.getInitialAmount());
		params.put(METADATA_KEY, getJSONString(transaction.getMetaData()));


		final Map<String, Object> result = getResult(giiftCredential.getBaseURL(), giiftCredential.getKey(),
				giiftCredential.getVector(), params, USEABLE_POINTS_URL, giiftCredential.getTimeout(),
				giiftCredential.isTimeoutEnabled());

		if (MapUtils.isEmpty(result) || !result.containsKey(POINTS_MAPPING_KEY))
		{
			return Optional.empty();
		}

		final String json = GSON.toJson(result.get(POINTS_MAPPING_KEY));
		final Points points = GSON.fromJson(json, Points.class);
		return Optional.ofNullable(points);
	}

	/**
	 *
	 */
	private void validate(final GiiftCredential giiftCredential)
	{
		Preconditions.checkArgument(!Objects.isNull(giiftCredential), GIIFT_CREDENTIAL_NULL_MESSAGE);
		Preconditions.checkArgument(Strings.isNotBlank(giiftCredential.getBaseURL()), BASE_URL_NULL_MESSAGE);

	}

	private Map<String, Object> getResult(final String baseURL, final String key, final String vector,
			final Map<String, Object> params, final String path, final int timeout, final boolean timeoutEnabled)
			throws GiiftException
	{
		final CustomerResponse response = GiiftWebServiceApiUtil.httpPOST(baseURL.concat(path), params, key, vector,
				CustomerResponse.class, timeout, timeoutEnabled);
		validate(response);
		if (Objects.isNull(response.getResult()))
		{
			LOG.error("response result is null");
			return Collections.emptyMap();
		}
		return response.getResult();
	}

	@Override
	public Optional<ExchangeRate> getPointExchangeRate(final GiiftCredential giiftCredential) throws GiiftException
	{
		validate(giiftCredential);
		validate(giiftCredential.getKey(), giiftCredential.getVector(), giiftCredential.getMerchantId());
		final Map<String, Object> params = new LinkedHashMap<>();
		params.put(MERCHANT_ID_KEY, giiftCredential.getMerchantId());

		final Map<String, Object> result = getResult(giiftCredential.getBaseURL(), giiftCredential.getKey(),
				giiftCredential.getVector(), params, EXCHANGE_RATE_URL, giiftCredential.getTimeout(),
				giiftCredential.isTimeoutEnabled());
		if (MapUtils.isEmpty(result))
		{
			return Optional.empty();
		}
		final ExchangeRate exchangeRate = GSON.fromJson(GSON.toJson(result), ExchangeRate.class);
		return Optional.ofNullable(exchangeRate);

	}

	@Override
	public Optional<VaildateTransactionResponse> validateTransaction(final GiiftCredential giiftCredential,
			final Transaction transaction) throws GiiftException
	{
		validate(giiftCredential);
		validate(giiftCredential.getKey(), giiftCredential.getVector(), giiftCredential.getMerchantId());
		Preconditions.checkArgument(!Objects.isNull(transaction), TRANSACTION_NULL_MESSAGE);
		validate(transaction.getCustomer());
		final Map<String, Object> params = new LinkedHashMap<>();
		params.put(MERCHANT_ID_KEY, giiftCredential.getMerchantId());
		params.put(CUSTOMER_KEY, transaction.getCustomer());
		params.put(INITIAL_AMOUNT_KEY, transaction.getInitialAmount());
		params.put(POINT_AMOUNT_KEY, transaction.getPointAmount());
		params.put(METADATA_KEY, getJSONString(transaction.getMetaData()));


		final Map<String, Object> result = getResult(giiftCredential.getBaseURL(), giiftCredential.getKey(),
				giiftCredential.getVector(), params, VALIDATE_TRANSACTION_URL, giiftCredential.getTimeout(),
				giiftCredential.isTimeoutEnabled());
		if (MapUtils.isEmpty(result))
		{
			return Optional.empty();
		}
		final String json = GSON.toJson(result);
		LOG.info("Validate Transaction Response: {}", json);
		final var vaildateTransactionResponse = GSON.fromJson(json, VaildateTransactionResponse.class);
		return Optional.ofNullable(vaildateTransactionResponse);
	}

	@Override
	public Optional<TransactionResponse> createTransaction(final GiiftCredential giiftCredential, final Transaction transaction)
			throws GiiftException
	{
		validate(giiftCredential);
		validate(giiftCredential.getKey(), giiftCredential.getVector(), giiftCredential.getMerchantId());
		validate(transaction);
		final Map<String, Object> params = getTransactionMap(transaction);
		params.put(MERCHANT_ID_KEY, giiftCredential.getMerchantId());
		params.put(METADATA_KEY, getJSONString(transaction.getMetaData()));

		final Map<String, Object> result = getResult(giiftCredential.getBaseURL(), giiftCredential.getKey(),
				giiftCredential.getVector(), params, CREATE_TRANSACTION_URL, giiftCredential.getTimeout(),
				giiftCredential.isTimeoutEnabled());
		if (MapUtils.isEmpty(result))
		{
			return Optional.empty();
		}

		final String json = GSON.toJson(result);
		LOG.info("Create Transaction Response: {}", json);
		final var transactionResponse = GSON.fromJson(json, TransactionResponse.class);

		return Optional.ofNullable(transactionResponse);

	}

	private void validate(final Transaction transaction)
	{

		Preconditions.checkArgument(!Objects.isNull(transaction), TRANSACTION_NULL_MESSAGE);

		if (Double.compare(transaction.getInitialAmount(), transaction.getFinalAmount()) != 0
				&& Objects.isNull(transaction.getValidateId()))
		{
			final IllegalArgumentException ex = new IllegalArgumentException(VALIDATION_ID_NULL_MESSAGE);
			LOG.error(ex.getMessage());
			throw ex;
		}
		validate(transaction.getCustomer());
	}

	private Map<String, Object> getTransactionMap(final Transaction transaction)
	{
		final Map<String, Object> params = new LinkedHashMap<>();
		params.put(CUSTOMER_KEY, transaction.getCustomer());
		params.put(INITIAL_AMOUNT_KEY, transaction.getInitialAmount());
		params.put(POINT_AMOUNT_KEY, transaction.getPointAmount());
		params.put(VALIDATE_ID_KEY, transaction.getValidateId());
		params.put(TRANSACTION_ID_KEY, transaction.getTransactionId());
		params.put(FINAL_AMOUNT_KEY, transaction.getFinalAmount());
		return params;

	}

	private void validate(final String key, final String vector)
	{

		Preconditions.checkArgument(Strings.isNotBlank(key), "key is null or empty");
		Preconditions.checkArgument(Strings.isNotBlank(vector), "vector is null or empty");
	}


	private void validate(final String key, final String vector, final String merchantId)
	{
		validate(key, vector);
		Preconditions.checkArgument(Strings.isNotBlank(merchantId), "merchantId is null or empty");
	}




	private void validate(final Customer customer)
	{
		Preconditions.checkArgument(!Objects.isNull(customer), "customer is null");
		Preconditions.checkArgument(Strings.isNotBlank(customer.getId()), CUSTOMER_ID_NULL_MESSAGE);
		Preconditions.checkArgument(Strings.isNotBlank(customer.getName()), "customer name is null");
		Preconditions.checkArgument(Strings.isNotBlank(customer.getMobileNumber()), "customer mobile number is null");
		Preconditions.checkArgument(Strings.isNotBlank(customer.getProvince()), "customer province is null");
		Preconditions.checkArgument(Strings.isNotBlank(customer.getAge()), "customer age is null");
		Preconditions.checkArgument(Strings.isNotBlank(customer.getOccupation()), "customer occupation is null");
	}

	private void validate(final CustomerResponse response) throws GiiftException
	{
		if (Objects.isNull(response))
		{
			final GiiftException ex = new GiiftException(GiiftExceptionType.EMPTY_ERROR_RESPONSE_EXCEPTION,
					GiiftExceptionType.EMPTY_ERROR_RESPONSE_EXCEPTION.getMsg(), null);
			LOG.error(ex.getMessage());
			throw ex;
		}

		if (response.getCode().equals(GiiftConstants.ERROR_CODE))
		{
			final GiiftException ex = ErrorHandlingUtil.getException(response.getErrorResult());
			LOG.error(ex.getMessage());
			throw ex;
		}
	}

	@Override
	public Optional<CustomerPointSummary> getCustomerPointSummary(final GiiftCredential giiftCredential, final String customerId)
			throws GiiftException
	{
		validate(giiftCredential);
		validate(giiftCredential.getKey(), giiftCredential.getVector());
		Preconditions.checkArgument(Strings.isNotBlank(customerId), CUSTOMER_ID_NULL_MESSAGE);

		final Map<String, Object> params = new LinkedHashMap<>();
		params.put(CUSTOMRE_ID_KEY, customerId);

		final Map<String, Object> result = getResult(giiftCredential.getBaseURL(), giiftCredential.getKey(),
				giiftCredential.getVector(), params, CUSTOMER_SUMMARY_URL, giiftCredential.getTimeout(),
				giiftCredential.isTimeoutEnabled());
		if (MapUtils.isEmpty(result))
		{
			return Optional.empty();
		}
		final CustomerPointSummary customerPointSummary = GSON.fromJson(GSON.toJson(result), CustomerPointSummary.class);

		return Optional.ofNullable(customerPointSummary);
	}

	@Override
	public Optional<CustomerPointLogs> getCustomerPointLogs(final GiiftCredential giiftCredential, final String customerId,
			final Pagination pagination) throws GiiftException
	{
		validate(giiftCredential);
		validate(giiftCredential.getKey(), giiftCredential.getVector());

		Preconditions.checkArgument(Strings.isNotBlank(customerId), CUSTOMER_ID_NULL_MESSAGE);
		final Map<String, Object> params = new LinkedHashMap<>();
		params.put(CUSTOMRE_ID_KEY, customerId);
		params.put(PAGINATION_KEY, pagination);

		final Map<String, Object> result = getResult(giiftCredential.getBaseURL(), giiftCredential.getKey(),
				giiftCredential.getVector(), params, CUSTOMER_POINT_LOGS_URL, giiftCredential.getTimeout(),
				giiftCredential.isTimeoutEnabled());
		if (MapUtils.isEmpty(result))
		{
			return Optional.empty();
		}
		final CustomerPointLogs customerPointLogs = GSON.fromJson(GSON.toJson(result), CustomerPointLogs.class);
		return Optional.ofNullable(customerPointLogs);
	}

	@Override
	public Optional<CustomerQRCode> getCustomerQRCode(final GiiftCredential giiftCredential, final String customerId)
			throws GiiftException
	{
		validate(giiftCredential);
		validate(giiftCredential.getKey(), giiftCredential.getVector());
		Preconditions.checkArgument(Strings.isNotBlank(customerId), CUSTOMER_ID_NULL_MESSAGE);

		final Map<String, Object> params = new LinkedHashMap<>();
		params.put(CUSTOMRE_ID_KEY, customerId);

		final Map<String, Object> result = getResult(giiftCredential.getBaseURL(), giiftCredential.getKey(),
				giiftCredential.getVector(), params, CUSTOMER_QR_CODE_URL, giiftCredential.getTimeout(),
				giiftCredential.isTimeoutEnabled());
		if (MapUtils.isEmpty(result))
		{
			return Optional.empty();
		}
		final CustomerQRCode customerQRCode = GSON.fromJson(GSON.toJson(result), CustomerQRCode.class);

		return Optional.ofNullable(customerQRCode);
	}

	@Override
	public boolean isCustomerRegistered(final GiiftCredential giiftCredential, final String customerId) throws GiiftException
	{
		validate(giiftCredential);
		validate(giiftCredential.getKey(), giiftCredential.getVector());
		Preconditions.checkArgument(Strings.isNotBlank(customerId), CUSTOMER_ID_NULL_MESSAGE);

		final Map<String, Object> params = new LinkedHashMap<>();
		params.put(CUSTOMRE_ID_KEY, customerId);

		final Map<String, Object> result = getResult(giiftCredential.getBaseURL(), giiftCredential.getKey(),
				giiftCredential.getVector(), params, IS_REGISTER_URL, giiftCredential.getTimeout(),
				giiftCredential.isTimeoutEnabled());
		if (MapUtils.isEmpty(result))
		{
			return false;
		}

		final String json = GSON.toJson(result);
		final CustomerStatus customerStatus = GSON.fromJson(json, CustomerStatus.class);

		if (Objects.isNull(customerStatus))
		{
			LOG.error("Could Not Desierialize [{}]", json);
			return false;
		}

		LOG.info("Customer [{}] registration status is : {}", customerStatus.getCustomerID(), customerStatus.isRegister());
		return customerStatus.isRegister();
	}

	@Override
	public boolean registerCustomer(final GiiftCredential giiftCredential, final Customer customer) throws GiiftException
	{
		validate(giiftCredential);
		validate(giiftCredential.getKey(), giiftCredential.getVector());
		validate(customer);

		final Map<String, Object> params = new LinkedHashMap<>();
		params.put(CUSTOMER_KEY, customer);

		final Map<String, Object> result = getResult(giiftCredential.getBaseURL(), giiftCredential.getKey(),
				giiftCredential.getVector(), params, CUSTOMER_REGISTER_URL, giiftCredential.getTimeout(),
				giiftCredential.isTimeoutEnabled());
		if (MapUtils.isEmpty(result))
		{
			return false;
		}
		final CustomerStatus customerStatus = GSON.fromJson(GSON.toJson(result), CustomerStatus.class);

		if (Objects.isNull(customerStatus))
		{
			return false;
		}
		return customerStatus.isRegister();
	}

	@Override
	public boolean cancelTransaction(final GiiftCredential giiftCredential, final String validateId,
			final CancelStatusOption cancelStatusOption) throws GiiftException
	{
		validate(giiftCredential);
		validate(giiftCredential.getKey(), giiftCredential.getVector());
		Preconditions.checkArgument(Strings.isNotBlank(validateId), VALIDATION_ID_NULL_MESSAGE);
		Preconditions.checkArgument(!Objects.isNull(cancelStatusOption), "cancelStatusOption is null");

		final Map<String, Object> params = new LinkedHashMap<>();
		params.put(CANCEL_STATUS_OPTION_KEY, cancelStatusOption);
		params.put(VALIDATE_ID_KEY, validateId);
		final Map<String, Object> result = getResult(giiftCredential.getBaseURL(), giiftCredential.getKey(),
				giiftCredential.getVector(), params, CANCEL_TRANSACTION_URL, giiftCredential.getTimeout(),
				giiftCredential.isTimeoutEnabled());
		return !MapUtils.isEmpty(result);
	}

	private String getJSONString(final GiiftMetaInfo metaInfo)
	{
		String jsonString = null;
		try
		{
			jsonString = GSON.toJson(metaInfo);
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage());
			logJSONStringWithNaNValues(metaInfo);
		}
		return jsonString;
	}

	private void logJSONStringWithNaNValues(final Object obj)
	{
		final Gson gson = new GsonBuilder().serializeNulls().serializeSpecialFloatingPointValues().create();
		String jsonString = null;
		try
		{
			jsonString = gson.toJson(obj);
		}
		catch (final Exception e)
		{
			LOG.error("Could not serialize metaInfo object, Ignoring the data");
			return;
		}
		LOG.info(jsonString);

	}

	@Override
	public Optional<QueryRefundPoints> queryRefundPoints(final GiiftCredential giiftCredential, final String transactionId,
			final double refundAmount) throws GiiftException
	{
		validate(giiftCredential);
		validate(giiftCredential.getKey(), giiftCredential.getVector());
		Preconditions.checkArgument(Strings.isNotBlank(transactionId), TRANSACTION_NULL_MESSAGE);
		Preconditions.checkArgument(refundAmount > 0, "RefundAmount must be greater than 0");

		final Map<String, Object> params = new LinkedHashMap<>();
		params.put(TRANSACTION_ID_KEY, transactionId);
		params.put(REFUND_AMOUNT_KEY, refundAmount);

		final Map<String, Object> result = getResult(giiftCredential.getBaseURL(), giiftCredential.getKey(),
				giiftCredential.getVector(), params, QUERY_REFUND_TRANSACTION_URL, giiftCredential.getTimeout(),
				giiftCredential.isTimeoutEnabled());

		final QueryRefundPoints refundPointsQuery = GSON.fromJson(GSON.toJson(result), QueryRefundPoints.class);

		return Optional.of(refundPointsQuery);
	}

	@Override
	public Optional<RefundTransactionResponse> refundTransaction(final GiiftCredential giiftCredential, final String transactionId,
			final double refundAmount) throws GiiftException
	{
		validate(giiftCredential);
		validate(giiftCredential.getKey(), giiftCredential.getVector());
		Preconditions.checkArgument(Strings.isNotBlank(transactionId), TRANSACTION_NULL_MESSAGE);
		Preconditions.checkArgument(refundAmount > 0, "RefundAmount must be greater than 0");

		final Optional<QueryRefundPoints> queryRefundPointsOpt = queryRefundPoints(giiftCredential, transactionId, refundAmount);

		if (queryRefundPointsOpt.isEmpty())
		{
			throw new GiiftException(GiiftExceptionType.SERVICE_ERROR_EXCEPTION, "Could Not Query Refund Points", null);
		}

		final QueryRefundPoints queryRefundPoints = queryRefundPointsOpt.get();

		final Map<String, Object> params = new LinkedHashMap<>();
		params.put(TRANSACTION_ID_KEY, transactionId);
		params.put(REFUND_AMOUNT_KEY, refundAmount);
		params.put(VALIDATE_ID_KEY, queryRefundPoints.getValidateID());

		final Map<String, Object> result = getResult(giiftCredential.getBaseURL(), giiftCredential.getKey(),
				giiftCredential.getVector(), params, REFUND_TRANSACTION_URL, giiftCredential.getTimeout(),
				giiftCredential.isTimeoutEnabled());

		final RefundTransactionResponse refundPointsQuery = GSON.fromJson(GSON.toJson(result), RefundTransactionResponse.class);

		return Optional.of(refundPointsQuery);

	}
}
