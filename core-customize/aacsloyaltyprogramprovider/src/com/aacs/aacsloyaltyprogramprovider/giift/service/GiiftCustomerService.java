package com.aacs.aacsloyaltyprogramprovider.giift.service;

import java.util.Optional;

import com.aacs.aacsloyaltyprogramprovider.giift.beans.Customer;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.CustomerPointLogs;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.CustomerPointSummary;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.CustomerQRCode;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.ExchangeRate;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.GiiftCredential;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.Pagination;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.Points;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.QueryRefundPoints;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.RefundTransactionResponse;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.Transaction;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.TransactionResponse;
import com.aacs.aacsloyaltyprogramprovider.giift.beans.VaildateTransactionResponse;
import com.aacs.aacsloyaltyprogramprovider.giift.enums.CancelStatusOption;
import com.aacs.aacsloyaltyprogramprovider.giift.exception.GiiftException;


public interface GiiftCustomerService
{

	public Optional<Points> getUsablePoints(GiiftCredential giiftCredential, Transaction transaction) throws GiiftException;

	public Optional<ExchangeRate> getPointExchangeRate(GiiftCredential giiftCredential) throws GiiftException;

	public Optional<VaildateTransactionResponse> validateTransaction(GiiftCredential giiftCredential, Transaction transaction)
			throws GiiftException;

	public Optional<TransactionResponse> createTransaction(GiiftCredential giiftCredential, Transaction transaction)
			throws GiiftException;

	public Optional<CustomerPointSummary> getCustomerPointSummary(GiiftCredential giiftCredential, String customerId)
			throws GiiftException;

	public Optional<CustomerPointLogs> getCustomerPointLogs(GiiftCredential giiftCredential, String customerId,
			final Pagination pagination) throws GiiftException;

	public Optional<CustomerQRCode> getCustomerQRCode(GiiftCredential giiftCredential, String customerId) throws GiiftException;

	public boolean isCustomerRegistered(GiiftCredential giiftCredential, String customerId) throws GiiftException;

	public boolean registerCustomer(GiiftCredential giiftCredential, Customer customer) throws GiiftException;

	public boolean cancelTransaction(GiiftCredential giiftCredential, String validateId, CancelStatusOption cancelStatusOption)
			throws GiiftException;

	public Optional<QueryRefundPoints> queryRefundPoints(GiiftCredential giiftCredential, String transactionId,
			double refundAmount) throws GiiftException;

	public Optional<RefundTransactionResponse> refundTransaction(GiiftCredential giiftCredential, String transactionId,
			double refundAmount) throws GiiftException;
}
