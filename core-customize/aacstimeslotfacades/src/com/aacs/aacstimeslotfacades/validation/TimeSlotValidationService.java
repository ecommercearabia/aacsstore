/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacstimeslotfacades.validation;

import com.aacs.aacstimeslotfacades.TimeSlotInfoData;
import com.aacs.aacstimeslotfacades.exception.TimeSlotException;


/**
 * @author amjad.shati@erabia.com
 */
public interface TimeSlotValidationService
{
	public boolean validate(final TimeSlotInfoData timeSlotInfo) throws TimeSlotException;
}
