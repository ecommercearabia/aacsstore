/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacstimeslotfacades.facade;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.time.format.DateTimeFormatter;
import java.util.Optional;

import com.aacs.aacstimeslotfacades.PeriodData;
import com.aacs.aacstimeslotfacades.TimeSlotData;
import com.aacs.aacstimeslotfacades.exception.TimeSlotException;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface TimeSlotFacade
{
	/**
	 * Retrieves TimeSlotData by the related ZoneDeliveryMode.
	 *
	 * @param zoneDeliveryModeCode,
	 *           String
	 * @return timeSlotData, Optional<TimeSlotData>
	 */
	public Optional<TimeSlotData> getTimeSlotData(String zoneDeliveryModeCode) throws TimeSlotException;

	/**
	 * Retrieves TimeSlotData by the related Area.
	 *
	 * @param areaCode,
	 *           String
	 * @return timeSlotData, Optional<TimeSlotData>
	 */
	public Optional<TimeSlotData> getTimeSlotDataByArea(String areaCode) throws TimeSlotException;

	/**
	 * Returns true if the TimeSlot related to the ZoneDeliveryMode code is active.
	 *
	 * @param zoneDeliveryModeCode,
	 *           String
	 * @return timeSlotEnabled, boolean
	 */
	public boolean isTimeSlotEnabled(String zoneDeliveryModeCode);

	/**
	 * Returns true if the TimeSlot related to the Area code is active.
	 *
	 * @param areaCode,
	 *           String
	 * @return timeSlotEnabled, boolean
	 */
	public boolean isTimeSlotEnabledByArea(String areaCode);

	public DateTimeFormatter getDateTimeFormatter(String zoneDeliveryModeCode);


	public boolean isTimeSlotEnabled(CMSSiteModel cmsSiteService) throws TimeSlotException;

	public boolean isTimeSlotEnabledByCurrentSite() throws TimeSlotException;

	public Optional<PeriodData> getFirstDeliverySlotForArea(String areaCode) throws TimeSlotException;

}
