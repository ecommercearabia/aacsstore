/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.aacs.aacsassistedservicecustomerinterestscustomaddon.jalo;

import com.aacs.aacsassistedservicecustomerinterestscustomaddon.constants.AacsassistedservicecustomerinterestscustomaddonConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class AacsassistedservicecustomerinterestscustomaddonManager extends GeneratedAacsassistedservicecustomerinterestscustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( AacsassistedservicecustomerinterestscustomaddonManager.class.getName() );
	
	public static final AacsassistedservicecustomerinterestscustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (AacsassistedservicecustomerinterestscustomaddonManager) em.getExtension(AacsassistedservicecustomerinterestscustomaddonConstants.EXTENSIONNAME);
	}
	
}
