/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */

package com.aacs.ordermanagement.actions.returns;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.returns.model.ReturnProcessModel;


/**
 * Creates the packing label for the {@link de.hybris.platform.returns.model.ReturnRequestModel}
 */
public class PrintPackingLabelAction extends AbstractProceduralAction<ReturnProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(PrintPackingLabelAction.class);

	@Override
	public void executeAction(ReturnProcessModel process)
	{
		LOG.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());
	}
}
