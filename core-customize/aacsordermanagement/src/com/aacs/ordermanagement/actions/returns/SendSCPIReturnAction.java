/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.aacs.ordermanagement.actions.returns;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aacs.aacsscpiservices.exception.SCPIException;
import com.aacs.aacsscpiservices.service.SCPIService;

import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.returns.model.OrderReturnRecordModel;
import de.hybris.platform.returns.model.ReturnProcessModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.store.BaseStoreModel;


/**
 * Update the {@link ReturnRequestModel} status to COMPLETED and finalize the corresponding
 * {@link OrderReturnRecordModel} for this {@link ReturnRequestModel}<br/>
 */
public class SendSCPIReturnAction extends AbstractSimpleDecisionAction<ReturnProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(SendSCPIReturnAction.class);


	@Resource(name = "defaultSCPIService")
	private SCPIService defaultSCPIService;


	@Override
	public Transition executeAction(final ReturnProcessModel process)
	{
		LOG.debug("Process: {} in step {}", process.getCode(), getClass().getSimpleName());

		BaseStoreModel baseStore = getStore(process);
		if (baseStore == null)
		{
			LOG.error("BaseStore For ReturnRequest [{}] Is null", process.getReturnRequest().getCode());
			return Transition.NOK;
		}

		if (baseStore.isEnableSCPIReturnOrder())
		{
			try
			{
				defaultSCPIService.sendSCPIReturnOrder(process.getReturnRequest());
			}
			catch (SCPIException e)
			{
				return Transition.NOK;
			}
		}
		return Transition.OK;
	}


	private BaseStoreModel getStore(ReturnProcessModel process)
	{
		if (process == null || process.getReturnRequest() == null || process.getReturnRequest().getOrder() == null
				|| process.getReturnRequest().getOrder().getStore() == null)
			return null;

		return process.getReturnRequest().getOrder().getStore();
	}

}
