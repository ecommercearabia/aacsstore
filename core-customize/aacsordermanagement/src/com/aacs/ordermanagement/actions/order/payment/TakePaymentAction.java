/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.aacs.ordermanagement.actions.order.payment;

import java.util.Optional;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aacs.aacspayment.context.PaymentContext;
import com.aacs.aacspayment.entry.PaymentResponseData;
import com.aacs.aacspayment.enums.PaymentResponseStatus;
import com.aacs.ordermanagement.actions.consignment.TakePaymentConsignmentAction;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;


/**
 * The TakePayment step captures the payment transaction.
 */
public class TakePaymentAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(TakePaymentConsignmentAction.class);

	@Resource(name = "paymentContext")
	private PaymentContext paymentContext;

	@Override
	public Transition executeAction(final OrderProcessModel process)
	{
		LOGGER.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());

		final OrderModel order = process.getOrder();
		if (order== null)
		{
			LOGGER.error("order is null!");
			return Transition.NOK;
		}
		if (order.getPaymentInfo() == null)
		{
			LOGGER.error("order [{}] PaymentInfo is null!", order.getCode());
			return Transition.NOK;
		}
		if (!(order.getPaymentInfo() instanceof CreditCardPaymentInfoModel))
		{
			LOGGER.info("order [{}] PaymentInfo [{}] is not CreditCardPaymentInfo  ", order.getCode(),
					order.getPaymentInfo().getClass());
			return Transition.OK;
		}

		if (paymentContext.isCaptured(order))
		{
			LOGGER.info("order [{}] PaymentInfo [{}] is Captured  ", order.getCode(),
					order.getPaymentInfo().getClass());
			return Transition.OK;
		}
		try
		{
			LOGGER.info("order [{}] PaymentInfo [{}] has not been captured,  getting captured now ",
					order.getCode(), order.getPaymentInfo().getClass());
			Optional<PaymentResponseData> response = paymentContext.captureOrder(order);
			
			if (response.isEmpty() ||response.get().getStatus().equals(PaymentResponseStatus.FAILURE))
			{
				LOGGER.error("A problem has occured during capturing consignment! Status is: " + response.get().getStatus());
				return Transition.NOK;
			}
			
			return Transition.OK;

		}
		catch (Exception e)
		{
			LOGGER.info("An error has occured while capturing order [{}] with PaymentInfo [{}], error message is [{}]",
					order.getCode(), order.getPaymentInfo().getClass(), e.getMessage());
			return Transition.NOK;
		}
	}

}
