/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.aacs.ordermanagement.actions.order.loyalty;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aacs.aacsloyaltyprogramprovider.context.LoyaltyProgramContext;
import com.aacs.aacsloyaltyprogramprovider.enums.LoyaltyPaymentStatus;
import com.aacs.aacsloyaltyprogramprovider.enums.LoyaltyPaymentType;
import com.aacs.aacsloyaltyprogramprovider.exception.AACSLoyaltyException;
import com.aacs.aacsloyaltyprogramprovider.exception.type.AACSLoyaltyExceptionType;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;


/**
 * 
 * 
 */
public class LoyaltyPaymentAction extends AbstractProceduralAction<OrderProcessModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(LoyaltyPaymentAction.class);

	@Resource(name = "loyaltyProgramContext")
	private LoyaltyProgramContext loyaltyProgramContext;

	@Override
	public void executeAction(final OrderProcessModel process)
	{
		LOG.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());

		final OrderModel order = process.getOrder();

		try
		{
			if (!getLoyaltyProgramContext().isLoyaltyEnabled(order) || isLoyaltyAlreadyCaptured(order))
			{
				return;
			}

			if (!getLoyaltyProgramContext().createTransaction(order))
			{
				throw new AACSLoyaltyException("Transaction Was not created please check the logs", order,
						AACSLoyaltyExceptionType.ERROR_WHILE_PROCESSING);
			}

			getModelService().refresh(order);
			order.setLoyaltyPaymentType(LoyaltyPaymentType.CAPTURED);
			getModelService().save(order);

		}
		catch (AACSLoyaltyException e)
		{
			throw new IllegalStateException(e.getMessage());
		}


	}

	private boolean isLoyaltyAlreadyCaptured(OrderModel order) throws AACSLoyaltyException
	{
		return LoyaltyPaymentType.CAPTURED.equals(order.getLoyaltyPaymentType())
				&& LoyaltyPaymentStatus.SUCCESS.equals(order.getLoyaltyPaymentStatus());
	}

	public LoyaltyProgramContext getLoyaltyProgramContext()
	{
		return loyaltyProgramContext;
	}

}
