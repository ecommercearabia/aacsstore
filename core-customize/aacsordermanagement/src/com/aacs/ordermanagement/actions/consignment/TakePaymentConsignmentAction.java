package com.aacs.ordermanagement.actions.consignment;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aacs.aacspayment.context.PaymentContext;
import com.aacs.aacspayment.entry.PaymentResponseData;
import com.aacs.aacspayment.enums.PaymentResponseStatus;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;


/**
 * Checks the state of the consignment to performStrategy changes (e.g., setting the consignment status) according to
 * the consignment's state changes resulted from the actions performed on it.
 */
public class TakePaymentConsignmentAction extends AbstractAction<ConsignmentProcessModel>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(TakePaymentConsignmentAction.class);

	@Resource(name = "paymentContext")
	private PaymentContext paymentContext;

	@Override
	public String execute(final ConsignmentProcessModel consignmentProcessModel) throws Exception
	{
		LOGGER.info("Process: {} in step {}", consignmentProcessModel.getCode(), getClass().getSimpleName());
		final ConsignmentModel consignment = consignmentProcessModel.getConsignment();

		if (consignment.getOrder() == null)
		{
			LOGGER.error("consignment [{}] order is null!", consignment.getCode());
			return Transition.NOK.toString();
		}
		if (consignment.getOrder().getPaymentInfo() == null)
		{
			LOGGER.error("order [{}] PaymentInfo is null!", consignment.getOrder().getCode());
			return Transition.NOK.toString();
		}
		if (!(consignment.getOrder().getPaymentInfo() instanceof CreditCardPaymentInfoModel))
		{
			LOGGER.info("order [{}] PaymentInfo [{}] is not CreditCardPaymentInfo  ", consignment.getOrder().getCode(),
					consignment.getOrder().getPaymentInfo().getClass());
			return Transition.OK.toString();
		}

		if (paymentContext.isCaptured(consignment.getOrder()))
		{
			LOGGER.info("order [{}] PaymentInfo [{}] is Captured  ", consignment.getOrder().getCode(),
					consignment.getOrder().getPaymentInfo().getClass());
			return Transition.OK.toString();
		}
		try
		{
			LOGGER.info("order [{}] PaymentInfo [{}] has not been captured,  getting captured now ",
					consignment.getOrder().getCode(), consignment.getOrder().getPaymentInfo().getClass());
			Optional<PaymentResponseData> response = paymentContext.captureOrder(consignment);
			
			if (response.isEmpty() ||response.get().getStatus().equals(PaymentResponseStatus.FAILURE))
			{
				LOGGER.error("A problem has occured during capturing consignment! Status is: " + response.get().getStatus());
				return Transition.NOK.toString();
			}
			
			return Transition.OK.toString();

		}
		catch (Exception e)
		{
			LOGGER.info("An error has occured while capturing order [{}] with PaymentInfo [{}], error message is [{}]",
					consignment.getOrder().getCode(), consignment.getOrder().getPaymentInfo().getClass(), e.getMessage());
			return Transition.NOK.toString();
		}

	}

	/**
	 * Updates the {@link ConsignmentModel#STATUS} to given {@link ConsignmentStatus}
	 *
	 * @param consignmentProcessModel
	 *                                   the given {@link ConsignmentProcessModel}
	 * @param status
	 *                                   The {@link ConsignmentStatus} to be set on the {@link ConsignmentModel}
	 */
	protected void updateConsignmentStatus(final ConsignmentProcessModel consignmentProcessModel, final ConsignmentStatus status)
	{
		final ConsignmentModel consignment = consignmentProcessModel.getConsignment();
		consignment.setStatus(status);
		getModelService().save(consignment);
	}

	@Override
	public Set<String> getTransitions()
	{
		return Transition.getStringValues();
	}

	protected enum Transition
	{
		OK, NOK;

		public static Set<String> getStringValues()
		{
			final Set<String> res = new HashSet<>();

			for (final Transition transition : Transition.values())
			{
				res.add(transition.toString());
			}
			return res;
		}
	}

}