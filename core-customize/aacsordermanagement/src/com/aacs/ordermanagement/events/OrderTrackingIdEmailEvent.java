package com.aacs.ordermanagement.events;

import de.hybris.platform.orderprocessing.events.ConsignmentProcessingEvent;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;



/**
 *@author monzer
 */
public class OrderTrackingIdEmailEvent extends ConsignmentProcessingEvent
{

	/**
	 * Instantiates a new payment failed notification email event.
	 *
	 * @param process the process
	 */
	public OrderTrackingIdEmailEvent(ConsignmentProcessModel process)
	{
		super(process);
	}
	

	
}